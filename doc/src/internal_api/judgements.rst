Judgements
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http

      GET /assessments/assessments/assessment_uuid/judgements/
   **Example response**:

   .. sourcecode:: http

   [
     {
          "uuid": "FMfp3hLqSPYWvnJE3Jw8ZH",
          "question": "7M2ObvUoRAJVMFoVlmarhH",
          "value": "yes",
          "justification": ""
      },
      {
         "uuid": "9gKYzgWDWauV4dsGkqxNxA",
         "question": "7oGGpvdu4L1D9s2b0ncTHb",
         "value": "yes",
         "justification": "Zie bijlage"
     }
   ]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST /assessments/assessments/assessment_uuid/judgements/

      [{
        "value": "no",
        "justification": "",
        "question": "SWJqrkFwtRLcuFWKELrmwc"
      }, ...]

   **Example response**:

   .. sourcecode:: http

   [{
     "uuid": "9gKYzgWDWauV4dsGkqxNxA",
     "value": "no",
     "justification": "",
     "question": "SWJqrkFwtRLcuFWKELrmwc"
   }, ...]

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]

UPDATE
________

**Example request**:

   .. sourcecode:: http

      PUT assessments/assessments/assessment_uuid/judgements/judgements_uuid/

      {
        "uuid": "qfSG6dyyUHJ2h7MJNZhCYn",
        "value": "no",
        "justification": "",
        "question": "SWJqrkFwtRLcuFWKELrmwc"
      }

   **Example response**:

   .. sourcecode:: http

   {
     "uuid": "qfSG6dyyUHJ2h7MJNZhCYn",
     "value": "no",
     "justification": "",
     "question": "SWJqrkFwtRLcuFWKELrmwc"
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


DELETE
--------
**Example request**:

   .. sourcecode:: http

      DELETE assessments/assessments/assessment_uuid/judgements/judgements_uuid/


   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


