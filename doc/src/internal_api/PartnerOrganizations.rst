Partner Organizations
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/organizations/{organization_pk}/partners/?
      page=<page_number>&
      page_size=<page_size>&
      organization_name=<text>

   **Example response**:

   .. sourcecode:: http

   {
      "count": 0,
      "results": [
        {
            "organization_uuid": "YVfskjgGMEfyWByzHz7xw4",
            "organization_name": " Johan Zwinkels cv",
            "organization_slug": "johan-zwinkels-cv",
            "organization_city": "London",
            "organization_type": "Producer/Farmer",
            "membership_uuid": "PoDb9rJMBXr4oRLcULYkeC"
        },
        {
            "organization_uuid": "ABfskjgGMEfyWByzHz7x23",
            "organization_name": " Johan Doe",
            "organization_slug": "johan-doe",
            "organization_city": "Amsterdam",
            "organization_type": "Producer/Farmer",
            "membership_uuid": "Aw3oDb9rJMBXr4oRLcULYks23"
        }
        ...
      ],
    }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]
