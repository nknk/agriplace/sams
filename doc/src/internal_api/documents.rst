Documents Internal API
=======================

This API provides access to data related to documents.


.. http:get:: /documents/types/

    List of document types.

    :reqheader Authorization: optional token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                    header of request
    :statuscode 200: you win


.. http:get:: /documents/org/(organization_pk)/documents/

    List of documents of given organization.

    :reqheader Authorization: optional token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                header of request
    :statuscode 200: you win
    :statuscode 404: unknown organization


.. http:get:: /documents/(document_pk)/

    Details for given document.

    :reqheader Authorization: optional token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                        header of request
    :statuscode 200: you win
    :statuscode 404: unknown organization or document


