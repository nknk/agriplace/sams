Agreements (user data)
----------------------

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST v2/assessments/assessment-types/${workflowContextUUID}/agreement/

        {
            "form_data": {*any form data*}
        }

   **Example response**:

   .. sourcecode:: http

   {
       "form_data": {*any form data*}
   }

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]
