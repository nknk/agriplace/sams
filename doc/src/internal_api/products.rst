Products (user data)
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http

      GET v2/organizations/organization_uuid/crops/
   **Example response**:

   .. sourcecode:: http

    [
      {
          "uuid": "6AVYdEYRaPu63WsYNBjHNG",
          "product_type_name": "Komkommer",
          "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
          "area": 20.0,
          "location": "Utrecht",
          "comment": "",
          "harvest_year": 2016,
          "traders": [
              {
                  "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
                  "contract_number": "098765"
              }
          ]
      },...
    ]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

   **Example request**:

   .. sourcecode:: http

      GET v2/organizations/organization_uuid/products/product_uuid/
   **Example response**:

   .. sourcecode:: http

   {
       "uuid": "6AVYdEYRaPu63WsYNBjHNG",
       "product_type_name": "Komkommer",
       "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
       "area": 20.0,
       "location": "Utrecht",
       "comment": "",
       "harvest_year": 2016,
       "traders": [
           {
               "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
               "contract_number": "098765"
           }
       ]
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST v2/organizations/organization_uuid/products/

      [
        {
            "product_type_name": "Komkommer",
            "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
            "area": 20.0,
            "location": "Utrecht",
            "comment": "",
            "harvest_year": 2016,
            "traders": [
                {
                    "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
                    "contract_number": "098765"
                }
            ]
        },...
      ]

   **Example response**:

   .. sourcecode:: http

    [
      {
          "uuid": "6AVYdEYRaPu63WsYNBjHNG",
          "product_type_name": "Komkommer",
          "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
          "area": 20.0,
          "location": "Utrecht",
          "comment": "",
          "harvest_year": 2016,
          "traders": [
              {
                  "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
                  "contract_number": "098765"
              }
          ]
      },...
    ]

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]

UPDATE
________

**Example request**:

   .. sourcecode:: http

      PUT v2/organizations/organization_uuid/products/product_uuid/

      {
          "uuid": "6AVYdEYRaPu63WsYNBjHNG",
          "product_type_name": "Komkommer",
          "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
          "area": 20.0,
          "location": "Utrecht",
          "comment": "",
          "harvest_year": 2016,
          "traders": [
              {
                  "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
                  "contract_number": "098765"
              }
          ]
      }

   **Example response**:

   .. sourcecode:: http

   {
       "uuid": "6AVYdEYRaPu63WsYNBjHNG",
       "product_type_name": "Komkommer",
       "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
       "area": 20.0,
       "location": "Utrecht",
       "comment": "",
       "harvest_year": 2016,
       "traders": [
           {
               "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
               "contract_number": "098765"
           }
       ]
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


DELETE
--------
**Example request**:

   .. sourcecode:: http

      DELETE v2/organizations/organization_uuid/products/product_uuid/

     **Example response**:

     .. sourcecode:: http

     {
         "uuid": "6AVYdEYRaPu63WsYNBjHNG",
         "product_type_name": "Komkommer",
         "product_type_uuid": "3xG2jcyo6hPvdxoncwdZJa",
         "area": 20.0,
         "location": "Utrecht",
         "comment": "",
         "harvest_year": 2016,
         "traders": [
             {
                 "organization_uuid": "6FPJWwVOcI5mikQwTIPGG7",
                 "contract_number": "098765"
             }
         ]
     }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


