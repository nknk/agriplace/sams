Detail
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http

      GET /assessments/assessments/assessment_uuid/detail/
   **Example response**:

   .. sourcecode:: http

   {
        "uuid": "FMfp3hLqSPYWvnJE3Jw8ZH",
        "created_time": "2015-06-02T16:30:49.349",
        "created_by": "Super User",
        "modified_time": "2015-07-02T16:30:49.349",
        "modified_by": "Another Super User",
        "shared_time": "2015-08-02T16:30:49.349",

        "preferred_month": "02",
        "audit_date": "2015-08-02",
        "auditor": "Super Auditor",
    }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]
