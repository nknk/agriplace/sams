Dashboard
---------

GET
---

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/memberships/<membership_pk>/dashboard/?page=<pageNumber>&year=<year_number>&year_start=<start_date>&year_end=<end_date>&assessment_types=<assessment_types>&assessment_states=<assessment_states>&inspectors=<internal_inspectors_ids>&search_str=<org_or_rel_query_string>
   **Example response**:

   .. sourcecode:: http

   {
    "first": /internal-api/memberships/<membership_id>/dashboard/?page=1&year=<year_number>&year_start=<start_date>&year_end=<end_date>&assessment_types=<assessment_types>&assessment_states=<assessment_states>&inspectors=<internal_inspectors_ids>&search_str=<org_or_rel_query_string>,
    "next": /internal-api/memberships/<membership_id>/dashboard/?page=5&year=<year_number>&year_start=<start_date>&year_end=<end_date>&assessment_types=<assessment_types>&assessment_states=<assessment_states>&inspectors=<internal_inspectors_ids>&search_str=<org_or_rel_query_string>,
    "previous": /internal-api/memberships/<membership_id>/dashboard/?page=3&year=<year_number>&year_start=<start_date>&year_end=<end_date>&assessment_types=<assessment_types>&assessment_states=<assessment_states>&inspectors=<internal_inspectors_ids>&search_str=<org_or_rel_query_string>,
    "count": 100,
    "result": [{
        "workflow_uuid": "AESD1232DSFS32",
        "assessment": {
                "status": "Internal audit done",
                "type": "global_gap",
                "label": "Global gap",
                "uuid": "SDFS232D233234234",
                "url": "/AS3DSF4RWESFSF/our/assessments/SDFS232D233234234/detail",
            },
        "organization_name": "MY FARM",
        "organization_url": "#",
        "producer_number": "SA112",
        "audit_date_actual": "2012-12-23",
        "audit_date_planned": "2012-12-22",
        "preferred_month": "1",
        "auditor": "12",
    }]
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


----------------------

GET
---

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/memberships/<membership_pk>/auditors/?year=<year number/All>
   **Example response**:

   .. sourcecode:: http

    [{
        "id": 12,
        "first_name": "MY FARM",
        "last_name": "#",
        "work_flow_count": 12,
    },
    {
        "id": 21,
        "first_name": "MY FARM",
        "last_name": "#",
        "work_flow_count": 4,
    }
    ]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

----------------------


GET
---

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/memberships/<membership_pk>/assessment-types/workflow-counts/?year=<year number/All>
   **Example response**:

   .. sourcecode:: http

    [{
        "uuid": "2Eja9QpUfbZUcnXSgsCKLH",
        "code": "",
        "name": "Tesco Nurture",
        "description": "",
        "kind": "assessment",
        "has_logo": true,
        "has_product_selection": true,
        "generic_form": null,
        "sharing_conditions": "",
        "edition": 1,
        "is_public": true,
        "parent_assessment_type": null,
        "assessment_sections": "overview,evidence,questionnaires,results,sharing",
        "available_languages": "en,nl,es",
        "logo_small": null,
        "logo_large": null,
        "work_flow_count": 0
    },
    {
        "uuid": "RLQxxaGwtn84QNMMiqy4pC",
        "code": "BRC-V7",
        "name": "BRC 7",
        "description": "BRC Global Standards is a leading safety and quality certification programme, with certification issued through a worldwide network of accredited certification bodies. The checklist covers each of the requirements of the BRC Standard and may be used to check your site&rsquo;s compliance with each of these requirements. The checklist also allows you to add comments or identify areas of improvement in the empty box provided at the bottom of each question. Please note that the checklist should not be considered as evidence of an internal audit and will not be accepted by auditors during an audit.",
        "kind": "assessment",
        "has_logo": true,
        "has_product_selection": true,
        "generic_form": null,
        "sharing_conditions": "",
        "version": 1,
        "is_public": true,
        "parent_assessment_type": null,
        "assessment_sections": "overview,evidence,questionnaires,results,sharing",
        "available_languages": "en,es",
        "logo_small": "http://0.0.0.0:3000/media/assessment-type-logos/Logo-BRC-small.jpg",
        "logo_large": "http://0.0.0.0:3000/media/assessment-type-logos/Logo-BRC-large.jpg",
        "work_flow_count": 24
    }]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

----------------------


GET
---

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/memberships/<membership_pk>/assessments/statuses/?year=<year number/All>
   **Example response**:

   .. sourcecode:: http

    [{
        "code": "internal_audit_request",
        "value": "Internal audit request",
        "work_flow_count": 12
    },
    {
        "code": "audit_hzpc",
        "value": "Internal audit pending",
        "work_flow_count": 32
    },
    {
        "code": "self_managed",
        "value": "Self managed",
        "work_flow_count": 2
    },
    {
        "code": "internal_audit_open",
        "value": "Internal audit open",
        "work_flow_count": 1
    }]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]
