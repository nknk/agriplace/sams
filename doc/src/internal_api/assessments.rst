Assessments Internal API
=========================

This API works with data related to assessments (assessments, questionnaires, etc.).

All URLs are included by the `internal-api` application with the root URL prefix ``/assessments/``.


assessments/assessments/rw7L7n2y7vKFWHHoabsnEH/full/?format=json

        {
            "questionnaire_element_states": {},
            "assessment": {
                "uuid": "rw7L7n2y7vKFWHHoabsnEH",
                "name": "GLOBALG.A.P. IFA / 2014-12-09",
                "assessment_type": "uaZHkcWBdNp0tW0S_IpzJ",
                "organization": "wcD5zskiRp8i78WzF29zu",
                "created_by": 1,
                "state": "open",
                "products": [{
                  "uuid": "Yr9UCf6DRhgfDWxabiPiuf",
                  "name": "Amaranth",
                  "product_type": "jdgTLg3hPiLeR3FnUv8CmU",
                  "harvest_year": 2016,
                  "area": 3.0,
                  "location": null,
                  "comment": null,
                  "partners": [],
                  "used_in_assessments": [{
                    "uuid": "3TCjyLL6MmkNiMac2ZK6QE",
                    "name": "GLOBALG.A.P. IFA / 2016-06-01-12"
                  }],
                  "used_in_certificates": []
                }],
                "judgement_mode": "not_available"  # 3 types of values ('read', 'write', 'not_available')
                "shares": []
            },
            "attachment_links": {
                "1UHLMed35ttSI7wUFPGRFR": [{
                    "attachment": {
                        "uuid": "Y2BM8eumKc29VdAn4Ru5E5",
                        "title": "Zie management programma\n",
                        "organization": "wcD5zskiRp8i78WzF29zu",
                        "attachment_type": "FormAttachment",
                        "generic_form_data": {
                          "uuid": "X5EQG8HTiGfjXJHsT2USAZ",
                          "json_data":{"test":1},
                          "generic_form": "Y2BM8eumKc30VdAn4Ru5E5"
                        },
                        "modified_time": "2015-06-02T16:30:49.349",
                        "used_in_assessments": [],
                        "is_frozen": false
                    },
                    "id": 937,
                    "assessment": "rw7L7n2y7vKFWHHoabsnEH",
                    "document_type_attachment": "1UHLMed35ttSI7wUFPGRFR",
                    "is_done": false,
                    "author": null,
                    "timestamp": "2015-04-30T14:38:44.922"
                }]
            },
            "answers": {
                "7M2ObvUoRAJVMFoVlmarhH": {
                    "uuid": "FMfp3hLqSPYWvnJE3Jw8ZH",
                    "question": "7M2ObvUoRAJVMFoVlmarhH",
                    "value": "yes",
                    "justification": ""
                },
                "7oGGpvdu4L1D9s2b0ncTHb": {
                    "uuid": "9gKYzgWDWauV4dsGkqxNxA",
                    "question": "7oGGpvdu4L1D9s2b0ncTHb",
                    "value": "yes",
                    "justification": "Zie bijlage"
                }
            },
            "shares": []
        }


assessments/assessment-types/uaZHkcWBdNp0tW0S_IpzJ/full/?format=json

"assessment_type": {
    "assessment_sections": "overview,evidence,questionnaires,results,sharing",
    "code": "globalgap_ifa",
    "description": "GlobalG.A.P. staat voor de eisen die in mondiaal verband aan land- en tuinbouwbedrijven worden gesteld i.v.m. voedselveiligheid, duurzaamheid en kwaliteit. \r\n\r\nDit protocol cre\u00ebert het kader voor Goede Agrarische Praktijken (GAP) op het bedrijfsniveau. Het bepaalt de minimumnormen waaraan de productie moet voldoen om te worden aanvaard door de grote supermarktketens. Het GlobalG.A.P. protocol is opgebouwd uit meerdere modules, afhankelijk van het soort product en de productie methode. ",
    "has_logo": true,
    "has_product_selection": true,
    "is_public": true,
    "kind": "assessment",
    "name": "GLOBALG.A.P. IFA",
    "edition": 1,
    "generic_form": null,
    "uuid": "uaZHkcWBdNp0tW0S_IpzJ",
    "questionnaires": {..,},
    document_types: {
        1UHLMed35ttSI7wUFPGRFR: {
            uuid: "1UHLMed35ttSI7wUFPGRFR",
            code: "EV1",
            name: "Proof of purchase crop protection products",…}
            assessment_type: "5UHLMed35ttSI7wUFPGRFR"
            assessment_types_where_optional: []
            code: "EV1"
            description: "Proof of purchase such as invoices, delivery notes or a supplier list."
            generic_form: {
              slug: "EV55"
              uuid: "Qnopn8vrzFzPvPUoo8DH4N"
            }
            level_object_dict: {uaZHkcWBdNp0tW0S_IpzJ: "2rDaxliQwxEaXKMFe2IWEz"}
            name: "Proof of purchase crop protection products"
            template: ""
            uuid: "1UHLMed35ttSI7wUFPGRFR"
            }
        }
    }

Assessment additional endpoints
-------------------------------

assessments/assessments/rw7L7n2y7vKFWHHoabsnEH/auditor_overview/

{
    "auditor_organization": {
        "name": "test_organization",
        "profile_url": "http://agriplace.com/test_organization/profile"
    },
    "auditor_user" = {
        "name": "Martijn",
        "profile_url": "http://agriplace.com/Martijn/profile"
    }
    audit_date_plan = "12/12/2010"
    audit_date_fact = "12/12/2010"
    audit_results = {
        decision = "Negative",  # 3 possible values
        reaudit = "true"  # boolean field
    }
    judgements = [],  # list of judgements with "no" value
    overview_description = "",
    internal_description = ""
}

