Products
This API links Products to Aseessment
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http

      GET /assessments/assessments/nAMZUtvNjCajsGhw5GedRh/products/
   **Example response**:

   .. sourcecode:: http

   ['Yr9UCf6DRhgfDWxabiPiuf', '79e3055cd639427996ecd2d0919b73ed', '79e3055cd639427996ecd2d0919b73ed']
   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST /assessments/assessments/nAMZUtvNjCajsGhw5GedRh/products/

      ['12cf9925d8ae4a36beb7699e6991cf89']

   **Example response**:

   .. sourcecode:: http

   {
     "errors": [],
     "data": ['12cf9925d8ae4a36beb7699e6991cf89'],
     "success": true
   }

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]


DELETE
--------
**Example request**:

   .. sourcecode:: http

      DELETE assessments/assessments/nAMZUtvNjCajsGhw5GedRh/products/12cf9925d8ae4a36beb7699e6991cf89/


   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


