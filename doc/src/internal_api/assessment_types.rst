Assessment types version
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/assessment-standard/editions/

   **Example response**:

   .. sourcecode:: http

   [
    {
        "2Eja9QpUfbZUcnXSgsCKLH": {
        "edition": 15,
        "language": "en"
        }
    },
    {
        "RLQxxaGwtn84QNMMiqy4pC": {
        "edition": 12,
        "language": "es"
        }
    },
    {
        "7w4uidcF5hN89cRNtbxdkS": {
        "edition": 1,
        "language": "en,es"
        }
    },
    {
        "eFnqyWoQ6gUqJbyNfavBvL": {
        "edition": 1,
        "language": "en"
        }
    },
     ...
   ]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


----------------------

GET
--------
.. sourcecode:: http
      GET /internal-api/assessment-standard/<pk>/<lang>/

   **Example response**:

   .. sourcecode:: http

   /internal-api/assessment-standard/BVkBxcZVCw2NKQ9AYvsqBW/en/

   {
    "uuid": "BVkBxcZVCw2NKQ9AYvsqBW",
    "code": "globalgap_ifa_v5",
    "name": "GLOBALG.A.P. IFA v.5.0-2",
    "description": "The GLOBALG.A.P. standard covers the certification of the whole agricultural production proces. The standard provides a framework for farmers worlwide for Good Agricultural Practice (GAP). The standard has been initiated by leading retailers and sets the minimum criteria for food safety, sustainability and workers welfare. The standard consists of different modules, depending on type of produce and the production proces.",
    "kind": "assessment",
    "has_logo": true,
    "has_product_selection": true,
    "generic_form": null,
    "edition": 1,
    "is_public": true,
    "parent_assessment_type": null,
    "available_languages": "en,nl,es",
    "logo_small": "https://agriplace-staging-media.s3.amazonaws.com/assessment-type-logos/logo-globalgap_ifa_v5-small.jpg?Signature=UZG55SfEVDtUDjefmbt8VQWup5Q%3D&Expires=1492758773&AWSAccessKeyId=AKIAJLDENWUZCCFIIRBA",
    "logo_large": "https://agriplace-staging-media.s3.amazonaws.com/assessment-type-logos/logo-globalgap_ifa_v5-large.jpg?Signature=4SM%2BICN3hTVrKtw4xr8XZy5VeEw%3D&Expires=1492758773&AWSAccessKeyId=AKIAJLDENWUZCCFIIRBA",
    "questionnaires": {
        "dvsaj5Lgkmt25yKUZSiym": {
            "uuid": "dvsaj5Lgkmt25yKUZSiym",
            "code": "AOSHZPC",
            "order_index": 5,
            "name": "Add-on Suikerbieten HZPC",
            "requires_signature": false,
            "requires_reference": false,
            "completeness_check": false,
            "available_languages": "nl",
            "assessment_type_code": "globalgap_ifa_v5",
            "question_levels": {
                "H5XJwwK8pYPyUWZQEASf5N": {
                    "uuid": "H5XJwwK8pYPyUWZQEASf5N",
                    "code": "GG-V5-L1-Suikerbieten",
                    "title": "Suikerbieten",
                    "order_index": 4,
                    "color": "#D9FF96",
                    "requires_justification": false,
                    "requires_attachment": false,
                    "assessment_type": "BVkBxcZVCw2NKQ9AYvsqBW"
                },
                .....
            },
            "help_document": "",
            "elements": [
                {
                    "uuid": "zQxT9nhNUwx8MJzJfKGM4H",
                    "code": "AOSHZPC-h-1",
                    "element_type": "heading",
                    "guidance": "",
                    "is_initially_visible": true,
                    "order_index": 0,
                    "parent_element": null,
                    "questionnaire": "dvsaj5Lgkmt25yKUZSiym",
                    "text": "1. Perceelskeuze",
                    "level": 1
                },
                ...
            ]
        },
        ....
    },
    "document_types": {
        "aYj46CCr35ASTMgD6UREkH": {
            "uuid": "aYj46CCr35ASTMgD6UREkH",
            "code": "OEV-GG-V5-FV-q-5.4.9",
            "name": "Optional attachment",
            "assessment_type": null,
            "generic_form": null,
            "template": "",
            "description": "",
            "level_object_dict": {
                "BVkBxcZVCw2NKQ9AYvsqBW": "BARzEDcoCbEAXac9uCXdrh"
            },
            "assessment_types_where_optional": [],
            "is_optional": true
        },
        .......
    },
    "triggers": {
        "mcuRja2QTtYVpjmSgsWQGC": {
            "uuid": "mcuRja2QTtYVpjmSgsWQGC",
            "target_element": "FNm2SMNHiiQe2tbxpkrLXD",
            "target_question": "FNm2SMNHiiQe2tbxpkrLXD",
            "target_justification": "No combinable crops are stored on the farm.",
            "target_value": "not_applicable",
            "target_visibility": true,
            "conditions": [
                {
                    "uuid": "np5up8CMbrHCRtBupXdXFP",
                    "source_property_name": "value",
                    "source_value": "no",
                    "trigger": "mcuRja2QTtYVpjmSgsWQGC",
                    "source_question": "WUP6L89Bn6dtXni8MCPpHJ"
                }
            ]
        },
        ........
    },
    "sections": [
        "overview",
        "products",
        "evidence",
        "questionnaires",
        "sharing"
    ],
    "sharing_conditions": "Plse note that there are pre-conditions to make the off-site auditing effective: 1. Mutually agree with your auditing company on the off-site document audit. 2. Off -site audible documents need to be completed and uploaded. At least the risk assessments and procedures. 3. Share your assessment maximum 10 days and minimum 5 days before the actual audit visit."
}

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]
