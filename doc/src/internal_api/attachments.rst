Attachments
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http
      GET /attachments/?page=<pageNumber>&modified_range_start=<modified_date>&modified_range_end=<modified_date>&title=<title>&used_in_standar=<standard_id>&usable_for_standard=<standard_id>&attachment_type=<attachment_type>&evidence_type=<evidence_type>&include_optional_evidence=<1/0>
   **Example response**:

   .. sourcecode:: http

   [{
     "uuid": "P7cCzPxzDQZnvYyTY7bZc",
     "title": "^CCD751851F0BD1598299652DB099B9CC08E9E3A4222639DFF4^pimgpsh_fullsize_distr",
     "organization": "kmraoShKxLaivXvphNcN43",
     "file": "https://agriplace-staging-media.s3.amazonaws.com/kmraoShKxLaivXvphNcN43/attachments/488535da-af36-47f4-886c-d4122017840e/010f8b61-074.jpeg",
     "original_file_name": "^CCD751851F0BD1598299652DB099B9CC08E9E3A4222639DFF4^pimgpsh_fullsize_distr.jpg",
     "expiration_date": null,
     "attachment_type": "FileAttachment",
     "modified_time": "2016-06-02",
     "used_in_document_types": [],    // current : "used_in": "",
     "used_in_assessments": [],       // current : new field
     "usable_for_assessments": [],    // current : new field
     "is_frozen": false
   }, {
     "uuid": "rtkbfECF7AtFvMYNPBPc25",
     "title": "IMG_20160523_164830",
     "organization": "kmraoShKxLaivXvphNcN43",
     "file": "https://agriplace-staging-media.s3.amazonaws.com/kmraoShKxLaivXvphNcN43/attachments/15389452-d8a1-4064-bac3-d42288c664ab/3c50d418-0dc.jpeg",
     "original_file_name": "IMG_20160523_164830.jpg",
     "expiration_date": null,
     "attachment_type": "FileAttachment",
     "modified_time": "2016-06-02",
     "used_in_document_types": [],    // format: [{uuid: '123', name: 'foo', code:'EV5'}, {…}], current : "used_in": "",
     "used_in_assessments": [],       // format: [{uuid: '123', name: 'foo', closed: False, url: '/grower/'}, {…}], current : new field
     "usable_for_assessments": [],    // format: [{uuid: '123', name: 'foo'}, {…}], current : new field
     "is_frozen": false
   }]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST /attachments/


   **Example response**:

   .. sourcecode:: http


    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]

DELETE
--------
**Example request**:

   .. sourcecode:: http

      DELETE attachments/nAMZUtvNjCajsGhw5GedRh


   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

