DISCLAIMER

this file is in developing mode.
feel free to suggest/modify


DocTypeEvidences
----------------------

Get
--------

   **Example request**:

   .. sourcecode:: http

      GET /assessments/assessments/nAMZUtvNjCajsGhw5GedRh/document-types/sandfjh4kjb5n34bBSg/attachments
   **Example response**:

   .. sourcecode:: http

   [
    {
      "attachment": {
          "uuid": "Y2BM8eumKc29VdAn4Ru5E5",
          "title": "Attachment 1",
          "organization": "wcD5zskiRp8i78WzF29zu",
          "attachment_type": "FormAttachment",
          "generic_form_data": {
            "uuid": "X5EQG8HTiGfjXJHsT2USAZ",
            "json_data":{"test":1},
            "generic_form": "Y2BM8eumKc30VdAn4Ru5E5"
          },
          "modified_time": "2015-06-02T16:30:49.349",
          "used_in_assessments": [],
          "is_frozen": false
      },
      "id": 937,
      "assessment": "nAMZUtvNjCajsGhw5GedRh",
      "document_type_attachment": "sandfjh4kjb5n34bBSg",
      "is_done": false,
      "author": null,
      "timestamp": "2015-04-30T14:38:44.922"
    }
    {
      "attachment": {
          "uuid": "Y2BM8eumKc29VdAn4Ru5E5",
          "title": "Attachment 2",
          "organization": "wcD5zskiRp8i78WzF29zu",
          "attachment_type": "TextReferenceAttachment",
          "generic_form_data": {
            "uuid": "X5EQG8HTiGfjXJHsT2USAZ",
            "json_data":{"test":1},
            "generic_form": "Y2BM8eumKc30VdAn4Ru5E5"
          },
          "modified_time": "2015-06-02T16:30:49.349",
          "used_in_assessments": [],
          "is_frozen": false
      },
      "id": 937,
      "assessment": "nAMZUtvNjCajsGhw5GedRh",
      "document_type_attachment": "sandfjh4kjb5n34bBSg",
      "is_done": false,
      "author": null,
      "timestamp": "2015-04-30T14:38:44.922"
    }
   ]
   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST /assessments/assessments/nAMZUtvNjCajsGhw5GedRh/document-types/sandfjh4kjb5n34bBSg/attachments

      {"document_links": [937, 938]}

   **Example response**:

   .. sourcecode:: http

   {
     "errors": [],
     "data":    [
    {
      "attachment": {
          "uuid": "Y2BM8eumKc29VdAn4Ru5E5",
          "title": "Attachment 1",
          "organization": "wcD5zskiRp8i78WzF29zu",
          "attachment_type": "FormAttachment",
          "generic_form_data": {
            "uuid": "X5EQG8HTiGfjXJHsT2USAZ",
            "json_data":{"test":1},
            "generic_form": "Y2BM8eumKc30VdAn4Ru5E5"
          },
          "modified_time": "2015-06-02T16:30:49.349",
          "used_in_assessments": [],
          "is_frozen": false
      },
      "id": 1000,
      "assessment": "nAMZUtvNjCajsGhw5GedRh",
      "document_type_attachment": "sandfjh4kjb5n34bBSg",
      "is_done": false,
      "author": null,
      "timestamp": "2015-10-30T14:38:44.922"
    }
    {
      "attachment": {
          "uuid": "Y2BM8eumKc29VdAn4Ru5E5",
          "title": "Attachment 2",
          "organization": "wcD5zskiRp8i78WzF29zu",
          "attachment_type": "TextReferenceAttachment",
          "generic_form_data": {
            "uuid": "X5EQG8HTiGfjXJHsT2USAZ",
            "json_data":{"test":1},
            "generic_form": "Y2BM8eumKc30VdAn4Ru5E5"
          },
          "modified_time": "2015-06-02T16:30:49.349",
          "used_in_assessments": [],
          "is_frozen": false
      },
      "id": 1001,
      "assessment": "nAMZUtvNjCajsGhw5GedRh",
      "document_type_attachment": "sandfjh4kjb5n34bBSg",
      "is_done": false,
      "author": null,
      "timestamp": "2015-10-30T14:38:44.922"
    }
   ],
     "success": true
   }

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]

