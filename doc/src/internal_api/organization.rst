Organization Internal API
==========================


----------------------

GET
---

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/organizations/pWx5uUXVTpVor5ZuRXmgqi/detail-for-auditor
   **Example response**:

   .. sourcecode:: http

    {
        "uuid": "pWx5uUXVTpVor5ZuRXmgqi",
        "name": "Orange Farm Ltd.",
        "slug": "orange-farm-ltd-almeria-es",
        "logo": null,
        "identification_number": "",
        "parent_organization": "foFjw8cNV29xPaJHErhHFa",
        "type": "producer",
        "description": "",
        "mailing_address1": "Camino de la luz 89",
        "mailing_address2": "",
        "mailing_city": "Almeria",
        "mailing_province": "",
        "mailing_postal_code": "",
        "mailing_country": "ES",
        "map_latitude": "0.00000",
        "map_longitude": "0.00000",
        "map_zoom": 3,
        "email": "",
        "url": "",
        "phone": "",
        "mobile": "",
        "ggn_number": "",
        "producer_number": "3",
        "gnl_number": "",
        "theme": "PNrMYqj2BoNT5jqSNUppuN",
        "evidence_sharing": true
    }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


Product types (content)
----------------------

.. http:get:: /organization/product-types/

    List of product types, with product groups.

   :reqheader Authorization: optional token to authenticate
   :resheader Content-Type: this depends on :mailheader:`Accept` header of request
   :statuscode 200: you win

     Returns an object with two lists: `{product_types, product_type_groups}`.