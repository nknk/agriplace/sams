Share Evidence
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http
      GET /internal-api/organizations/{organization_pk}/shared_attachments/share/?attachment_uuids=uuid1,uuid2,...]

   **Example response**:

   .. sourcecode:: http

   [
     {
        "organization_uuid": "ABfskjgGMEfyWByzHz7x23",
        "organization_name": " Johan Doe",
        "organization_slug": "johan-doe",
        "organization_city": "Amsterdam",
        "organization_type": "Producer/Farmer",
        "membership_uuid": "Aw3oDb9rJMBXr4oRLcULYks23",
     }, {
        "organization_uuid": "ABf234234GMEfyWByzHz7x23",
        "organization_name": " Johan Wick",
        "organization_slug": "johan-wick",
        "organization_city": "Amsterdam",
        "organization_type": "Producer/Farmer",
        "membership_uuid": "Aw3oDb9rJMBXr4o34534LYks23",
     },
     ...
   ]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST /internal-api/organizations/{organization_pk}/shared_attachments/share

      { 
        attachment_uuids : [ "akuwhdasbcjnzn", "akuwhdasbcjnzn", ... ],
        membership_uuids : [ "Bkuwhdasbcja2dd", "BDakuwhdasbcjnwe3", ... ]
      }


   **Example response**:

   .. sourcecode:: http

    <same as GET response>

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]


