Internal API
================

The internal API provides (RESTful) access to data models.

The internal API differs from the external API through the fact that it is intended only for internal (AgriPlace) use, and not communicated externally, so it can be updated more often with only changing internal apps/libraries.

The root URL is ``/internal-api``, included from the ``api.internal`` application. It then includes needed API URL modules from the other applications.

Different endpoints of the API are implemented in the various Django applications, split by functionality (assessments, documents, organization, etc.).


.. toctree::
    :maxdepth: 1
    :glob:

    *



