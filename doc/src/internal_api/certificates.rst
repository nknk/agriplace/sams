Certificates
------------

GET
---

   **Example request**:

   .. sourcecode:: http

      GET /organizations/<organization-slug>/certifications/
      
   **Example response**:

   .. sourcecode:: http

   [
     {
          "uuid": "FMfp3hLqSPYWvnJE3Jw8ZH",
          "certificate_type": {
            "uuid": "FMfp3hLqSPYWvnJE3Jw12J",
            "name": "AB Logo",
          },
          "certificate_number": "00412570",
          "issue_date": "2016-01-23",
          "expiry_date": "2017-01-23",
          "auditor": "Auditor Name",
          "crops":[
              {
                "uuid":"FMfp3hLqSPYWvnJE3Jw12G",
                "product_type":"lkfp3hLqSPYWvnJE3Jwsdh23",
                "name": "Almond",
                "total_surface":20.5,
              },
              ...
          ],
          "certificate_files":[
               {
                    uuid : "yMciwV8Nq4hDfJXs3whbRa",
                    title : "certificate_doc",
                    file : "/media/kmraoShKxLaivXvphNcN43/attachments/e0157a72-6626-4b6a-9bd3-c6a6bf0d4e3c/6c515340-184.pdf",
                    original_file_name : "certificate_doc.pdf",
               },
               ...
          ],
          "audit_report_files":[
               {
                    uuid : "yMciwV8Nq4hDfJXs3whbRa",
                    title : "audit_report",
                    file : "/media/kmraoShKxLaivXvphNcN43/attachments/e0157a72-6626-4b6a-9bd3-c6a6bf0d4e3c/6c515340-185.pdf",
                    original_file_name : "audit_report.pdf",
               },
               ...
          ],
      },
      ...
   ]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
------

   **Example request**:

   .. sourcecode:: http

      POST /organizations/<organization-slug>/certifications/

      {
        "certificate_type": "FMfp3hLqSPYWvnJE3Jw12J",
        "certificate_number": "00412570",
        "issue_date": "2016-01-23",
        "expiry_date": "2017-01-23",
        "auditor": "Auditor name",
        "crops":[
              {
                "product_type":"lkfp3hLqSPYWvnJE3Jwsdh23",
                "total_surface":20.4,
              },
              ...
        ],
        "certificate_files":[
               {
                    uuid : "yMciwV8Nq4hDfJXs3whbRa",
                    title : "certificate_doc",
                    file : "/media/kmraoShKxLaivXvphNcN43/attachments/e0157a72-6626-4b6a-9bd3-c6a6bf0d4e3c/6c515340-184.pdf",
                    original_file_name : "certificate_doc.pdf",
               },
               ...
          ],
          "audit_report_files":[
               {
                    uuid : "yMciwV8Nq4hDfJXs3whbRa",
                    title : "audit_report",
                    file : "/media/kmraoShKxLaivXvphNcN43/attachments/e0157a72-6626-4b6a-9bd3-c6a6bf0d4e3c/6c515340-185.pdf",
                    original_file_name : "audit_report.pdf",
               },
               ...
          ],
      }

   **Example response**:

   .. sourcecode:: http

   {
     "uuid": "9gKYzgWDWauV4dsGkqxNxA",
     ...
   }

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]

UPDATE
______

**Example request**:

   .. sourcecode:: http

      PUT /organizations/<organization-slug>/certifications/uuid

      {
        "uuid": "qfSG6dyyUHJ2h7MJNZhCYn",
        <params in create>
        ...
      }

   **Example response**:

   .. sourcecode:: http

   {
     "uuid": "qfSG6dyyUHJ2h7MJNZhCYn",
     ...
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

GET
---

   **Example request**:

   .. sourcecode:: http

      GET /organizations/<organization-slug>/certifications/uuid

   **Example response**:

   .. sourcecode:: http

   {
      "uuid": "FMfp3hLqSPYWvnJE3Jw8ZH",
      <params in create>
   },

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

DELETE
------
**Example request**:

   .. sourcecode:: http

      DELETE /organizations/<organization-slug>/certifications/uuid


   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


