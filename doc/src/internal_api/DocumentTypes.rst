DocuementTypes
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http
      GET /attachments/document_types/
   **Example response**:
'uuid',
            'code',
            'name',
            'assessment_type',
            'is_optional'
   .. sourcecode:: http

   [{
     "uuid": "P7cCzPxzDQZnvYyTY7bZc",
     "name": "The cleanliness solution",
     "code": "EV71",
     "assessment_type": "O23AJkjnas0a9sasjnnas",
     "is_optional": True

   }, {
     "uuid": "12P7cCzPxzDQZnvYyTY7bZ12",
     "name": "The security policy",
     "code": "EF73",
     "assessment_type": "PISO23AJkjnas0a9sasjnnas",
     "is_optional": False

   }]

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]
