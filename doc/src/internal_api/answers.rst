Answers
----------------------

GET
--------

   **Example request**:

   .. sourcecode:: http

      GET /assessments/assessments/nAMZUtvNjCajsGhw5GedRh/answers/
   **Example response**:

   .. sourcecode:: http

   {
     "7M2ObvUoRAJVMFoVlmarhH": {
         "uuid": "FMfp3hLqSPYWvnJE3Jw8ZH",
         "question": "7M2ObvUoRAJVMFoVlmarhH",
         "value": "yes",
         "justification": ""
     },
     "7oGGpvdu4L1D9s2b0ncTHb": {
         "uuid": "9gKYzgWDWauV4dsGkqxNxA",
         "question": "7oGGpvdu4L1D9s2b0ncTHb",
         "value": "yes",
         "justification": "Zie bijlage"
     }, ...
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]

CREATE
--------

   **Example request**:

   .. sourcecode:: http

      POST /assessments/assessments/nAMZUtvNjCajsGhw5GedRh/answers/

      [{
        "value": "no",
        "justification": "",
        "question": "SWJqrkFwtRLcuFWKELrmwc",
        "dirtyCounter": 1
      }, ...]

   **Example response**:

   .. sourcecode:: http

   {
     "errors": [],
     "data": {
       "SWJqrkFwtRLcuFWKELrmwc": {
         "uuid": "qfSG6dyyUHJ2h7MJNZhCYn",
         "justification": "",
         "question": "SWJqrkFwtRLcuFWKELrmwc",
         "value": "no",
         "dirtyCounter": 1
       }
     },
     "success": true
   }

    :statuscode 2xx: no error
    :statuscode 4xx 5xx: response with [errors]

UPDATE
________

**Example request**:

   .. sourcecode:: http

      PUT assessments/assessments/nAMZUtvNjCajsGhw5GedRh/answers/qfSG6dyyUHJ2h7MJNZhCYn/

      {
        "value": "yes",
        "justification": "",
        "question": "SWJqrkFwtRLcuFWKELrmwc",
        "dirtyCounter": 1,
        "uuid": "qfSG6dyyUHJ2h7MJNZhCYn"
      }

   **Example response**:

   .. sourcecode:: http

   {
      justification: "",
      question: "SWJqrkFwtRLcuFWKELrmwc",
      uuid: "qfSG6dyyUHJ2h7MJNZhCYn",
      value: "yes"
   }

   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


DELETE
--------
**Example request**:

   .. sourcecode:: http

      DELETE assessments/assessments/nAMZUtvNjCajsGhw5GedRh/answers/qfSG6dyyUHJ2h7MJNZhCYn/


   :statuscode 2xx: no error
   :statuscode 4xx 5xx: response with [errors]


