Welcome to the AgriPlace code documentation!
=================================================

This is the internal documentation for AgriPlace developers.

.. toctree::
    :maxdepth: 9

    getting_started
    deployment
    architecture
    internal_api/index
    apps/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

