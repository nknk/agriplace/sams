
In this document I try to explain the way translations are done in the Agriplace webapp.


Translations are implemented with gettext.

The frontend and the backend have seperate translation files, they are located in webapp/frontend/locale/[ language ]/LC_MESSAGES/
and webapp/locale/[ language ]/LC_MESSAGES/ respectively.



																							BACKEND/DJANGO


Translations of static fields in the Django backend can be done with the Rosetta web manage tool.

To update the webapp/locale/ translations so it reflects the current codebase do the following:

	Go into the virtual environment, from there:

		1) in sams folder run
			pip install -r requirements.txt

		2) extract the strings from source-code into .po:
				in webapp folder run:
					./manage.py makemessages --all

		3) compile .po files created by makemessages to .mo files for use with the builtin gettext support:
			in webapp folder run:
				python manage.py compilemessages


																							FRONTEND/REACT


Frontend translation templates are stored in webapp/frontend/locale/.

The frontend translation templates are generated manually for now. The script to generate a .pot template from the
React frontend source files is: webapp/gulp/inactive-tasks/js2po.js. The script can be run from it's
directory with node js: node js2po The script actually does a little more than only generating a .pot file.

Running js2po.js does the following:

	- it creates a __temp file in webapp/gulp/inactive-tasks/ to store temporary files

	- it goes recursively through all source files in webapp/frontend/react/ to convert all .jsx files to .js
		while putting them in the same directory structure, but now in __temp

	- a backup of webapp/frontend/locale/djangojs.pot is created or updated

	- it goes recursively through all directories in __temp to find in each .js file the '_t' function identifiers
		used for translation. Every '_t' found will be put in the webapp/frontend/locale/djangojs.pot template file,
		including a reference from which file it originates

	- after the .pot file has been generated, the __temp directory will be removed entirely

	- backups will be made or updated from webapp/frontend/locale/**/LC_MESSAGES/djangojs.po

	- webapp/frontend/locale/dangojs.pot will be merged into webapp/frontend/locale/**/LC_MESSAGES/djangojs.po
		(mind the glob, it will do all languages!)



To have the translations working in the frontend, yet another (final) script has to run, which is: webapp/gulp/tasks/po2json.js

po2json.js generates webapp/frontend/static/frontend/locale/nl.json

nl.json is being used by the i18next-client library, to finally translate the frontend _t texts to dutch in this case.

Later po2json.js needs to be upgraded to convert multiple languages in webapp/frontend/locale/**/LC_MESSAGES/ to all.json,
so all (future) languages are available in the frontend bundle.


Dependencies for the frontend translations need to be installed with npm:

	gulp
	i18next
	i18next-conv -g ( needs to be installed globally )
	i18next-client
	jsxgettext-recursive
	walker