Heroku:

  Maintenance mode:
    heroku maintenance:on -a agriplace-production

  Install heroku toolbelt:
    wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh


  Install heroku-pg-transfer:
    heroku plugins:install https://github.com/ddollar/heroku-pg-transfer

  Get current config:
    heroku config -a agriplace-testing


  Upgrading the local content database to the latest one on Heroku:

    from the command line as user:
      heroku pg:transfer --from DATABASE_URL --to postgresql://agriplace@localhost:5432/agriplace --app agriplace-content
    from webapp/
      ./manage.py migrate


