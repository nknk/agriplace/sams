Deployment
=============

Deploying to Heroku
-----------------------

AgriPlace reads the following envvars:

* ``DEVEL`` - if set to `on`, load the debug toolbar, be verbose in logging
* ``DEBUG`` - if set to `on`, set Django's debug flag
* ``SECRET_KEY`` - the Django secret key
* ``DATABASE_URL`` - database connection
* ``SMTP`` - email server, in the format ``user:password@host:port``. Prepend ``ssl::`` to use SSL; this is required by Amazon SES.
* ``SENTRY_DSN`` - sentry access URL
* ``AWS_ACCESS_KEY_ID`` - Amazon Access Key ID
* ``AWS_SECRET_ACCESS_KEY`` - Amazon Secret Access Key
* ``AWS_STORAGE_BUCKET_NAME`` - Amazon S3 bucket for user uploads
* ``MAINTENANCE_PAGE_URL`` - maintenance page; we use ``http://maintenance.transip-1.agriplace.com/``.
* ``PIWIK_URL`` - report traffic to Piwik instance; we use ``http://piwik.agriplace.com/piwik.php?idsite=1`` for the production website.
* ``URL_REDIRECT`` - perform redirection of entire site, e.g. ``http://agriplace.com|http://www.agriplace.com`` will redirect users to the ``www`` site if they visit the non-www domain.
* ``URL_PROXY`` - proxy requests to a backend server, e.g. ``/en/|http://cms.agriplace.com/en/`` will proxy ``/en/*`` requests to ``cms.agriplace.com``.

To deploy, just push the `sams` repository to Heroku::

    % git remote add heroku-testing git@heroku.com:agriplace-testing.git
    % git push heroku-testing HEAD:master

Then initialize the database::

    % heroku run bash --app agriplace-testing
    $ webapp/manage.py syncdb

To view activity logs, use the Heroku toolbelt::

    % heroku logs -t --app agriplace-testing

Panic button – you can switch the site to `maintenance mode`_::

    % heroku maintenance:on --app agriplace-testing

.. _`maintenance mode`: https://devcenter.heroku.com/articles/maintenance-mode


Deploying to a Linux Server
-------------------------------

The following instructions assume a Debian or Ubuntu system, with the
installation folder at `/home/webapps/ap-test`, running as the user
`ap-test`, using the local port `12828`. Feel free to change them as you see
fit.

Setting up a new installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As `root`, install dependencies, then create the installation folder and
database::

  apt-get install -y postgresql python-virtualenv nginx supervisor git
  apt-get install -y build-essential python2.7-dev postgresql-server-dev-all
  apt-get install -y libpng-dev libjpeg-dev libxml2-dev libxslt1-dev

  mkdir -p /home/webapps/ap-test
  chown ap-test: /home/webapps/ap-test

  sudo -u postgres createuser ap-test
  sudo -u postgres createdb ap-test -O ap-test

As the `ap-test` user, set up the web application::

  cd /home/webapps/ap-test
  git clone https://bitbucket.org/people4earth/sams.git src
  virtualenv venv
  venv/bin/pip install -r src/webapp/requirements/devel.txt
  mkdir var var/static var/media var/logs

Create `src/webapp/sams_project/settings/local_settings.py`::

  import dj_database_url
  from .production import *

  SECRET_KEY = '--- random string here ---'

  # Files
  VAR = '/home/webapps/ap-test/var'
  STATIC_ROOT = VAR + '/static'
  MEDIA_ROOT = VAR + '/media'
  LOG_ROOT = VAR + '/logs'

  # Database
  DATABASE_URL = 'postgres:///ap-test'
  DATABASES = {
      'default': dj_database_url.parse(DATABASE_URL),
  }

Create a script that runs the server in `venv/bin/runserver`::

  #!/bin/bash
  cd /home/webapps/ap-test
  source venv/bin/activate
  cd src/webapp
  exec waitress-serve --host=127.0.0.1 --port=12828 sams_project.wsgi:application

Make the `runserver` script executable::

  chmod +x venv/bin/runserver

Initialize the database and prepare static files::

  source venv/bin/activate
  src/webapp/manage.py syncdb
  src/webapp/manage.py collectstatic --noinput

As root, create a supervisor configuration file at `/etc/supervisor/conf.d/ap-test.conf`::

  [program:ap-test]
  command = /home/webapps/ap-test/venv/bin/runserver
  user = ap-test
  stdout_logfile = /home/webapps/ap-test/var/logs/app.log
  redirect_stderr = true

As root, reload supervisor::

  supervisorctl update

As root, create an nginx configuration file at `/etc/nginx/sites-available/ap-test`::

  server {
    server_name test.agriplace.com;

    location /static/ {
      alias /home/webapps/ap-test/var/static/;
    }

    location /media/ {
      alias /home/webapps/ap-test/var/media/;
    }

    location / {
      proxy_pass http://127.0.0.1:12828;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $host;
      proxy_redirect off;
    }
  }

As root, activate the site and reload nginx::

  cd /etc/nginx/sites-enabled
  ln -s ../sites-available/ap-test
  service nginx reload

Updating an existing installation to the latest version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As the `ap-test` user, run these commands::

  cd /home/webapps/ap-test/src
  git pull
  cd ..
  source venv/bin/activate
  src/webapp/manage.py syncdb
  src/webapp/manage.py collectstatic --noinput

As `root`, restart the web process::

  supervisorctl restart ap-test


Manifest of services
--------------------

Heroku
~~~~~~
We run the following apps on Heroku:

* ``agriplace-testing``: code from the ``devel`` branch; database may be reset
  at any time; accessible at http://testing.in.agriplace.com and
  http://agriplace-testing.herokuapp.com.
* ``agriplace-content``: code from the ``content`` branch; used by content
  editors to prepare content without disrupting the production setup;
  accessible at http://content.in.agriplace.com and
  http://agriplace-content.herokuapp.com.
* ``agriplace-production``: the production app, accessible at
  https://www.agriplace.com and http://agriplace-production.herokuapp.com;
  https is provided via the heroku addon.

WebFaction
~~~~~~~~~~
WebFaction hosts the CMS (cms.agriplace.com) which serves the front-page and
other content. This content is proxied through the main app to be displayed
to the user.

WebFaction also hosts DNS records for all domains except ``agriplace.com``.

TransIP
~~~~~~~
There is one VPS at TransIP, ``transip-1.agriplace.com``, it hosts a number of
things:

* The ``agriplace.com`` apex website (without a leading ``www.``), which
  redirects to ``www.agriplace.com``, and also proxies some requests to
  Piwik at ``https://agriplace.com/__piwik/*``, to keep browsers happy about
  loading resources via https.

* Piwik (analytics) at ``http://piwik.agriplace.com``. It uses a local MySQL
  database.

* Sentry (error logging) at ``http://sentry.agriplace.com``. It uses a local
  PostgreSQL database, and a cron job that deletes data older than 30 days.

* Monit (service monitoring) which pings our websites and emails us when they
  are down.

* Backup scripts that run hourly and upload data to Tarsnap:

  * for production, it downloads the content of the S3 bucket, dumps the
    Postgres database, and makes a backup;

  * for content, it downloads the content of the S3 bucket, dumps the
    Postgres database, and makes a backup;

  * for piwik, it dumps the MySQL database, and makes a backup.

Amazon AWS
~~~~~~~~~~
* S3: we store user-uploaded media; each heroku app has its own bucket.

* Route 53: serves the DNS records for the ``agriplace.com`` domain.

* SES: all our various services send email via SES.
