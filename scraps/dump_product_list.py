#!/usr/bin/env python

import sys
import simplejson as json
import xlrd


def dump_products(sheet):
    for n_row in xrange(sheet.nrows):
        if not sheet.cell(n_row, 0).value:
            continue
        standards = []
        flags = [(8, 'FV'),
                 (9, 'SAN-RA'),
                 (10, 'FT-FFr'),
                 (11, 'FT-Fveg'),
                 (12, 'All FT')]
        for n_col, name in flags:
            val = sheet.cell(n_row, n_col).value
            if isinstance(val, float) and val > 0:
                standards.append(name)
        record = {
            'name': sheet.cell(n_row, 1).value,
            'latin_name': sheet.cell(n_row, 2).value,
            'synonyms': sheet.cell(n_row, 3).value,
            'source': sheet.cell(n_row, 4).value,
            'helptext': sheet.cell(n_row, 5).value,
            'standards': ', '.join(standards),
        }
        name_2 = sheet.cell(n_row, 6).value
        helptext_2 = sheet.cell(n_row, 7).value
        if name_2:
            record['name'] += " (%s)" % name_2
        if helptext_2:
            record['helptext'] += "; " + helptext_2

        #standards_agg = sheet.cell(n_row, 13).value
        #computed_standards_agg = ', '.join(standards)
        #if standards_agg != record['standards']:
        #    print "mismatch: %r != %r" % (standards_agg, record['standards'])

        print json.dumps(record, sort_keys=True)


def main():
    [filename] = sys.argv[1:]
    wb = xlrd.open_workbook(filename)
    for sheet in wb.sheets():
        if "Product Taxonomy" in sheet.name:
            dump_products(sheet)


if __name__ == '__main__':
    main()
