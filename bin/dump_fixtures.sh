#!/bin/sh

MODEL_LIST="\
core.assessmenttype \
core.possibleanswer \
core.question \
core.questionnaire \
core.questionnaireheading \
tools.sai.saiquestion \
tools.sai.saiquestionnaire"

webapp/manage.py dumpdata $MODEL_LIST --indent=2 > webapp/db/questionnaires.json
