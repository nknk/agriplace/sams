#!/bin/bash

###############################################################################
# Git Hook Chaining, Dale Anderson, 2015-02-17
# based on code found at: http://stackoverflow.com/a/8734391/267455
# and: https://github.com/henrik/dotfiles/blob/master/git_template/hooks/pre-commit
#------------------------------------------------------------------------------
#
# To install:
#
#    1) Copy this file in to your git hooks dir, and make it executable.
#
#    2) If you have an existing hook, rename it as "hook-type.action"
#       e.g:  $ mv post-receive post-receive.email-developers
#
#    3) Create your primary hook as a symlink to this file.
#       e.g:  $ ln -s hook-chain.sh post-receive
#
# That's it. Now this script will execute every chained hook it finds.
#
# You can create as many more executable "hook-type.some-action" files you want,
# and they'll all get executed in alphabetical order.
#
# Repeat steps 2 and 3 for every hook you need to run multiple scripts for. All
# your hooks can be symlinked to the same master hook-chain script, and they'll
# all behave the same way.
#
#------------------------------------------------------------------------------



# Whatever this script was called as (pre-receive, post-receive, update, etc),
# that's the pattern of file names we'll be looking for to execute
# (post-receive.something, pre-receive.something, etc)
hookname=$(basename "$0")

# Capture what came from STDIN so it can be passed through to our chained hooks
# data=$(cat)

# Keep track of exit codes so we can return an error if any hook failed.
exitcodes=()

# Process all the 'hookname.action' files we find, and store the exit codes.
for hook in "hooks/$hookname."*; do
  if test -x "$hook"; then
    # This next line is how we impersonate the hook.
    # We pipe anything we got from stdin, for hooks like pre-receive and post-receive.
    # We forward any command line arguments we got after it, for hooks like update.
    # Between both of these, this file can successfully impersonate any type of hook.
    echo "$data" | "$hook" "$@"

    STATUS=$?
    exitcodes+=($STATUS)
    # Let the user know if a chained hook exited with an error.
    [ $STATUS -eq 0 ] || echo "### Warning ### $hookname chained hook '$hook' exited with status $STATUS"
  fi
done

# If any exit code isn't 0, exit with that code.
for ((i=0; i < ${#exitcodes[@]}; i++)); do
  CODE=${exitcodes[$i]}
  [ "$CODE" -eq 0 ] || exit "$CODE"
done