from django.db import models
from django.utils.translation import ugettext as _

from core.models import UuidModel

import json_field


class FormSchema(UuidModel):
    json = json_field.JSONField(
      help_text='Form schema',
      blank=True,
      default={},
    )

    class Meta:
        db_table = 'FormSchema'


class FormData(UuidModel):
    schema = models.ForeignKey(FormSchema, related_name='forms')
    json = json_field.JSONField(
      help_text='Form data',
      blank=True,
      default={},
    )

    class Meta:
        db_table = 'FormData'
        permissions = (
            ('view_formdata', 'Can view form data'),
        )


class GenericForm(UuidModel):
    name = models.CharField(_("Form name"), max_length=200)
    slug = models.SlugField(_("Slug"), max_length=200, unique=True)

    class Meta:
        db_table = 'GenericForm'


class GenericFormData(UuidModel):
    generic_form = models.ForeignKey(GenericForm, related_name='form_data')
    json_data = json_field.JSONField(
      help_text='Form data',
      blank=True,
      default={},
    )

    class Meta:
        db_table = 'GenericFormData'
