# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django_extensions.db.fields
import json_field.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FormData',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('json', json_field.fields.JSONField(default={}, help_text=b'Form data', blank=True)),
            ],
            options={
                'db_table': 'FormData',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormSchema',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('json', json_field.fields.JSONField(default={}, help_text=b'Form schema', blank=True)),
            ],
            options={
                'db_table': 'FormSchema',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GenericForm',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=200, verbose_name='Form name')),
                ('slug', models.SlugField(unique=True, max_length=200, verbose_name='Slug')),
            ],
            options={
                'db_table': 'GenericForm',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GenericFormData',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('json_data', json_field.fields.JSONField(default={}, help_text=b'Form data', blank=True)),
                ('generic_form', models.ForeignKey(related_name='form_data', to='form_builder.GenericForm')),
            ],
            options={
                'db_table': 'GenericFormData',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='formdata',
            name='schema',
            field=models.ForeignKey(related_name='forms', to='form_builder.FormSchema'),
            preserve_default=True,
        ),
    ]
