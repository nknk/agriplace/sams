# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form_builder', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='formdata',
            options={'permissions': (('view_formdata', 'Can view form data'),)},
        ),
    ]
