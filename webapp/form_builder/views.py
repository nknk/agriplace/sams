from collections import OrderedDict

from django.contrib.auth.decorators import login_required
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import View

from ap_extras.item_list import ItemListModel, ItemListColumn
from app_settings.utils import get_help_text_block
from organization.views import get_organization_context
from sams.constants import ICONS, PAGINATION_ITEMS_PER_PAGE
from sams.decorators import session_organization_required
from sams.views import get_common_context
from .forms import FormTemplateCreateForm
from .models import (
    FormSchema,
    FormData
)


@login_required
@session_organization_required
def list(request, *args, **kwargs):
    context, organization = get_organization_context(request, kwargs)

    context.update({
        'page_title': _("Form templates"),
    })

    context.pop("page_title_url", None)

    paginator = Paginator(
        FormSchema.objects.order_by("-created_time"),
        PAGINATION_ITEMS_PER_PAGE)
    page = request.GET.get('page')
    try:
        forms = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        forms = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page
        # of results.
        forms = paginator.page(paginator.num_pages)

    # Tree view
    context["list"] = {
        "items": forms,
        "rows": []
    }
    for form in forms:
        context["list"]["rows"].append({
            "cells": [
                {
                    "icon": ICONS["form"],
                    "text": form,
                    "url": "/"  # reverse("my_assessment_detail", args=[organization.slug, assessment.pk])
                },
                {
                    "text": naturaltime(form.created_time)
                }
            ]
        })

    # Form templates item list model
    forms_model = ItemListModel()
    forms_model.id = "active"
    forms_model.items = FormSchema.objects.all()

    forms_model.columns = [
        ItemListColumn(
            header=_("Name"),
            text=lambda form: form.json['title'] if 'title' in form.json else _('<no name specified>'),
            url=lambda form: reverse("form_builder", args=[form.uuid]),
            icon_class=lambda form: ICONS['form']
        ),
        ItemListColumn(
            header=_("Actions"),
            text=lambda form: _('Create'),
            url=lambda form: reverse("form_instantiate", args=[form.uuid])
        )
    ]

    forms_model.create_url = reverse("form_create")
    forms_model.create_text = _("Create a new form")
    forms_model.no_items_text = _("No forms available.")
    forms_model.refresh()
    context['forms_model'] = forms_model
    context['help_link_url'] = get_help_text_block('forms')

    return render(request, "forms/list.html", context)


class FormTemplateCreator(View):
    target_type = FormSchema
    form_class = FormTemplateCreateForm

    def get_context(self, form):
        context, organization = get_organization_context(self.request, self.kwargs)
        context['form'] = form
        return context

    def render(self, request, form):
        return render(request, "forms/create.html", self.get_context(form))

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return self.render(request, form)

    def post(self, request, *args, **kwargs):
        form = self.form_class(
            data=request.POST
        )

        if form.is_valid():
            form_template = FormSchema(
                json=OrderedDict(
                    title=form.cleaned_data['name'],
                    schema={
                        'type': 'object',
                        'properties': {
                            'firstName': {
                                'type': 'string',
                                "enum": ["Tree", "Potato", 'Agriplace']
                            },
                            'lastName': {
                                'type': 'boolean'
                            },
                            'age': {
                                'description': 'Age in years',
                                'type': 'integer',
                                'minimum': 0
                            }
                        },
                        'required': ['firstName', 'lastName']
                    },
                    options={},
                    layout=''
                )
            )
            form_template.save()
            return redirect("form_builder", form_template.uuid)
        else:
            messages.warning(request, _("Input is not valid."))
        return self.render(request, form)


@login_required
def builder(request, schema_pk, *args, **kwargs):
    """
    form builder
    """

    context = get_common_context(request)

    schema = get_object_or_404(FormSchema, pk=schema_pk)

    context.update({
        'page_title': _("Form templates"),
        "page_title_url": reverse("form_list"),
        'schema_pk': schema.uuid,
        'ICONS': ICONS
    })

    return render(request, 'forms/builder.html', context)


@login_required
def create(request, schema_pk, *args, **kwargs):
    try:
        schema = FormSchema.objects.get(pk=schema_pk)
    except FormSchema.DoesNotExist:
        raise Http404

    form = FormData(schema=schema, json={})
    form.save()

    return redirect("form_runner", form.pk)


@login_required
def runner(request, form_pk, *args, **kwargs):
    """
    form runner
    """
    context = get_common_context(request)

    form = get_object_or_404(FormData, pk=form_pk)

    context.update({
        'page_title': form.schema.json['title'] if 'title' in form.schema.json else _('Untitled form'),
        "page_title_url": '',
        'form_pk': form.pk,
        'schema_pk': form.schema.pk,
        'ICONS': ICONS
    })

    return render(request, 'forms/runner.html', context)
