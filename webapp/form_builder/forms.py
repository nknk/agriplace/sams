
from django import forms

from django.utils.translation import ugettext as _

from .models import (
    FormSchema,
    FormData
)

class FormTemplateCreateForm(forms.Form):
    name = forms.CharField(
        required=True,
        label=_("Name")
    )
