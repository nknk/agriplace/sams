import json

from django.http import Http404
from jsonschema import Draft3Validator
from rest_framework import status, generics
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.generics import UpdateAPIView, CreateAPIView
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.schemas import SchemaGenerator
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer

from form_builder.api.internal.serializers import GenericFormDataSerializer
from form_builder.models import *


@api_view()
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer, CoreJSONRenderer])
def schema_view(request):
    """
    Core API schema for RBAC API
    :param request:
    :return:
    """
    generator = SchemaGenerator(title='AGRIPLACE API')
    return Response(generator.get_schema(request=request))


@api_view(['GET'])
def internal_api_form_builder_root(request, format=None):
    """
    Root for Form Builder Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'generic-form-data-list': reverse('generic-form-data-list', request=request, format=format),
    })


class SchemaUpdateView(UpdateAPIView):
    def get_serializer(self, *args, **kwargs):
        pass

    def get_queryset(self):
        pass

    def put(self, request, *args, **kwargs):
        schema_pk = kwargs.get('schema_pk', '')

        try:
            schema_json = json.loads(request.body)
        except ValueError:
            return Response({'success': False, 'message': 'Invalid JSON'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            schema = FormSchema.objects.get(pk=schema_pk)
        except FormSchema.DoesNotExist:
            schema = FormSchema(pk=schema_pk, json=schema_json)

        schema.json = schema_json

        try:
            schema.save()
        except Exception as ex:
            return Response(
                {'success': False, 'message': "Unable to save schema: {0}" % ex},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

        return Response({'success': True}, status=status.HTTP_201_CREATED)


class SchemaCreateView(CreateAPIView):
    def get_serializer(self, *args, **kwargs):
        pass

    def get_queryset(self):
        pass

    def post(self, request, *args, **kwargs):
        try:
            schema_pk = kwargs.get('schema_pk', '')
            schema = FormSchema.objects.get(pk=schema_pk)
        except FormSchema.DoesNotExist:
            raise Http404()

        form = FormData(schema=schema, json={})
        form.save()

        return Response({'form_id': form.pk})


class FormUpdateView(UpdateAPIView):
    def get_serializer(self, *args, **kwargs):
        pass
    
    def get_queryset(self):
        pass

    def put(self, request, *args, **kwargs):

        form_pk = kwargs.get('form_pk', '')
        try:
            form_json = json.loads(request.body)
        except ValueError:
            return Response({'success': False, 'message': 'Invalid JSON'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            form = FormData.objects.get(pk=form_pk)
        except FormData.DoesNotExist:
            return Response({'success': False, 'message': "Specified form does not exists"},
                            status=status.HTTP_400_BAD_REQUEST)

        errors = list(Draft3Validator(form.schema.json).iter_errors(form_json))

        if errors:
            json_errors = map(
                lambda error: {'message': error.message, 'context': error.context, 'path': list(error.path),
                               'schema_path': list(error.schema_path)}, errors)
            return Response({'errors': json_errors}, safe=False)

        form.json = form_json

        try:
            form.save()
        except Exception as ex:
            return Response({'success': False, 'message': "Unable to save form: {0}" % ex},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response([], safe=False)


class GenericFormDataListView(generics.ListCreateAPIView):
    """
    List and create react form data
    """
    serializer_class = GenericFormDataSerializer
    model = GenericFormData

    def get_queryset(self):
        return GenericFormData.objects.all()


class GenericFormDataDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update and delete react form data
    """
    serializer_class = GenericFormDataSerializer
    model = GenericFormData
