from rest_framework_bulk import BulkSerializerMixin
from rest_framework import serializers

from api.serializers import UuidModelSerializer

from form_builder.models import GenericFormData, GenericForm


class GenericFormSerializer(BulkSerializerMixin, UuidModelSerializer):
    class Meta:
        model = GenericForm
        fields = ('uuid', 'slug')


class GenericFormDataSerializer(BulkSerializerMixin, UuidModelSerializer):
    generic_form = serializers.PrimaryKeyRelatedField(
        queryset=GenericForm.objects.all()
    )

    class Meta:
        model = GenericFormData
        fields = ('uuid', 'json_data', 'generic_form',)

    json_data = serializers.ReadOnlyField()
