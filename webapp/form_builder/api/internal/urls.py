from django.conf.urls import patterns, url
from form_builder.api.internal.views import (
    GenericFormDataListView, GenericFormDataDetailsView, SchemaUpdateView, SchemaCreateView, FormUpdateView
)

urlpatterns = patterns(
    "form_builder.api.internal.views",
    url(
        r'^api-root/$',
        'internal_api_form_builder_root',
        name='form_builder-app-api-list'
    ),
    url(
        r"^schema/(?P<schema_pk>.+)/$",
        SchemaUpdateView.as_view(),
        name="schema-update-view"
    ),
    url(
        r"^schema/(?P<schema_pk>.+)/create$",
        SchemaCreateView.as_view(),
        name="schema-data-create-view"
    ),
    url(
        r"^form/(?P<form_pk>.+)/$",
        FormUpdateView.as_view(),
        name="form-update-view"
    ),
    url(
        r"^generic_form/$",
        GenericFormDataListView.as_view(),
        name="generic-form-data-list"
    ),
    url(
        r"^generic_form/(?P<pk>.+)/$",
        GenericFormDataDetailsView.as_view(),
        name="generic-form-data-details"
    ),
)
