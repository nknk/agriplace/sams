import json
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse
from form_builder.models import GenericFormData, GenericForm


# TODO: investigate why this tests takes more than 10 minutes
# class TestFormSchemaAPI(APITestCase):
#     test_schema_id = '1234567890'
#
#     test_form_schema_url = "/internal-api/forms/schema/%s/" % test_schema_id
#
#     def test_save_load(self):
#         #
#         # Save a schema
#         #
#         schema = {
#             "title": "test",
#             "name": {
#                 "type": "string"
#             }
#         }
#
#         response = self.client.put(self.test_form_schema_url, data=schema, format='json')
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.content)
#
#         #
#         # Load a schema
#         #
#         response = self.client.get(self.test_form_schema_url)
#         self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
#
#         data = json.loads(response.content)
#
#         self.assertTrue(type(data) is dict, msg='A response to a form schema query must be an Object')
#         self.assertNotEqual(data, {}, msg='A form schema must be non-empty')
#
#
# class TestFormAPI(APITestCase):
#     test_schema_id = '1234567890'
#
#     test_form_schema_url = "/internal-api/forms/schema/%s/" % test_schema_id
#     test_form_schema_create_url = "/internal-api/forms/schema/%s/create" % test_schema_id
#
#     test_form_id = '1234567890'
#
#     def test_save_load(self):
#         #
#         # Create a schema
#         #
#         schema = {
#             "title": "test",
#             "type": "object",
#             "properties": {
#                 "name": {
#                     "type": "string"
#                 },
#             }
#         }
#
#         response = self.client.put(self.test_form_schema_url, data=schema, format='json')
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.content)
#
#         response = self.client.get(self.test_form_schema_create_url, format='json')
#         data = json.loads(response.content)
#
#         test_form_url = "/internal-api/forms/form/%s/" % data['form_id']
#
#         #
#         # Save a form
#         #
#         form = {'name': "Val"}
#
#         response = self.client.put(test_form_url, data=form, format='json')
#         self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
#
#         #
#         # Load a form
#         #
#         response = self.client.get(test_form_url)
#         self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
#
#         data = json.loads(response.content)
#         self.assertTrue(type(data) is dict, msg='A response to a form query must be an Object')
#
#
# class TestGenericFormApi(APITestCase):
#     def test_generic_form_data_create(self):
#         user = User.objects.create_user('test', 'test@agriplace.com')
#         generic_form = GenericForm.objects.create(
#             name='generic_test_form',
#             slug='generic_test_form'
#         )
#         url = reverse('generic_form_data_list')
#         self.client.force_authenticate(user=user)
#         json_data = {'data': 'contains form data'}
#         response = self.client.post(
#             url,
#             data={
#                 'json_data': json_data,
#                 'generic_form': generic_form.uuid
#             },
#             format='json'
#         )
#         data = json.loads(response.content)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertDictContainsSubset(
#             {
#                 'json_data': json_data
#             },
#             data
#         )
#         self.assertTrue(GenericFormData.objects.filter(uuid=data['uuid']).exists())
#
#     def test_generic_form_data_retrieve(self):
#         user = User.objects.create_user('test', 'test@agriplace.com')
#         generic_form = GenericForm.objects.create(
#             name='generic_test_form',
#             slug='generic_test_form'
#         )
#         json_data = {'data': 'contains form data'}
#         generic_form_data = GenericFormData.objects.create(
#             generic_form=generic_form,
#             json_data=json_data
#         )
#         url = reverse('generic_form_data_details', kwargs={'pk': generic_form_data.uuid})
#         self.client.force_authenticate(user=user)
#         response = self.client.get(url, format='json')
#         data = json.loads(response.content)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertDictContainsSubset(
#             {
#                 'uuid': generic_form_data.uuid,
#                 'json_data': json_data
#             },
#             data
#         )
#
#     def test_generic_form_data_update(self):
#         user = User.objects.create_user('test', 'test@agriplace.com')
#         generic_form = GenericForm.objects.create(
#             name='generic_test_form',
#             slug='generic_test_form'
#         )
#         json_data = {'data': 'contains form data'}
#         generic_form_data = GenericFormData.objects.create(
#             generic_form=generic_form,
#             json_data=json_data
#         )
#         url = reverse('generic_form_data_details', kwargs={'pk': generic_form_data.uuid})
#         self.client.force_authenticate(user=user)
#         json_data_update = {'data': 'contains form data update'}
#         response = self.client.put(
#             url,
#             data={
#                 'json_data': json_data_update,
#                 'generic_form': generic_form.uuid
#             },
#             format='json'
#         )
#         data = json.loads(response.content)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#         self.assertDictContainsSubset(
#             {
#                 'generic_form': generic_form.uuid,
#                 'json_data': json_data_update
#             },
#             data
#         )
#         generic_form = GenericFormData.objects.get(uuid=data['uuid'])
#         self.assertEqual(json_data_update, generic_form.json_data)

    # TODO: Test delete and list if nessecary
