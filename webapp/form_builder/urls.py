from django.conf.urls import patterns, url

from .views import FormTemplateCreator

urlpatterns = patterns(
    "form_builder.views",

    # Form templates
    url(r"^$", "list", name="form_list"),
    url(r"^new$", FormTemplateCreator.as_view(), name="form_create"),
    url(r"^builder/(?P<schema_pk>.+)/$", "builder", name="form_builder"),

    # Forms
    url(r"^create/(?P<schema_pk>.+)/$", "create", name="form_instantiate"),
    url(r"^(?P<form_pk>.+)/$", "runner", name="form_runner"),
)
