from django.contrib import admin

from core.admin import BaseAdmin
from .models import FeedbackMessage


class FeedbackMessageAdmin(BaseAdmin):
    pass
admin.site.register(FeedbackMessage, FeedbackMessageAdmin)

