from django import forms

from .models import FeedbackMessage


class FeedbackMessageForm(forms.ModelForm):
    """
    Form for user feedback message

    Kwargs:
        user: request.user
        referrer: Originating page
    """
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(FeedbackMessageForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        feedback_message = super(FeedbackMessageForm, self).save(commit=False)
        feedback_message.author = self.user
        feedback_message.save()

    class Meta:
        model = FeedbackMessage
        fields = ['referrer', 'text']
