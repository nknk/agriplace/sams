from django.conf.urls import patterns, url

urlpatterns = patterns(
    'feedback.views',
    url(r'^$', 'feedback_form', name='feedback_form'),
    url(r'^thanks$', 'feedback_thanks', name='feedback_thanks'),
)
