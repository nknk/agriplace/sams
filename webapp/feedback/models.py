from datetime import datetime

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class FeedbackMessage(models.Model):
    """
    User feedback message model
    """
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='feedback_messages', verbose_name=_("Author"))
    timestamp = models.DateTimeField(verbose_name=_("Date"))
    text = models.TextField(verbose_name=_("Message"))
    referrer = models.CharField(max_length=1000, blank=True, verbose_name=_("Referrer"))

    class Meta:
        permissions = (
            ('view_feedbackmessage', 'Can view feedback message'),
        )

    def __unicode__(self):
        return u"{timestamp} {author}".format(
            timestamp=self.timestamp,
            author=self.author
        )

    def save(self, *args, **kwargs):
        # Update timestamp
        if not self.pk:
            self.timestamp = datetime.today()
        return super(FeedbackMessage, self).save(*args, **kwargs)
