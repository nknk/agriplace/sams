from django import views
from django.contrib import messages
from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from sams import constants
from sams.views import get_common_context

from .forms import FeedbackMessageForm
from app_settings.utils import get_text_blocks


def feedback_form(request, *args, **kwargs):
    """
    User feedback form
    """
    referrer=request.META.get('HTTP_REFERER')
    context = get_common_context(request)
    context['page_title'] = _("Feedback")
    from_email = getattr(settings, "DEFAULT_FROM_EMAIL", 'support@agriplace.com')
    to_email = getattr(settings, "FEEDBACK_EMAILS", ('support@agriplace.com',))

    if request.method == 'POST':
        if hasattr(request, 'layout_context') and request.layout_context.get('layout_name') == 'hzpc':
            from_email = getattr(settings, "HZPC_DEFAULT_FROM_EMAIL", 'globalgap@hzpc.nl')
            to_email = getattr(settings, "HZPC_FEEDBACK_EMAILS", ('globalgap@hzpc.nl',))

        form = FeedbackMessageForm(request.POST, user=request.user)
        if form.is_valid():
            # Save message to db
            form.save()
            form_referrer = form.cleaned_data['referrer']

            # Send message to support@agriplace.com
            subject = "Feedback"
            message = form.cleaned_data.get('text') + '\n\n' + 'user: ' + unicode(request.user) + '\n\n' + form_referrer
            send_mail(subject, message, from_email, to_email)

            # Redirect back where we came from
            url = reverse('feedback_thanks')
            return redirect("{url}?next={referrer}".format(url=url, referrer=form_referrer))
    else:
        form = FeedbackMessageForm(user=request.user)
    context.update({
        'form': form,
        'referrer': referrer
    })

    context['text_blocks'] = get_text_blocks(('feedback_form_intro',))

    return render(request, 'feedback/feedback_form.html', context)


def feedback_thanks(request, *args, **kwargs):
    """
    User feedback form thanks page.
    """
    context = get_common_context(request)
    next_url = request.GET.get('next')
    if not next_url:
        next_url = reverse('root')
    context['next_url'] = next_url
    return render(request, 'feedback/feedback_thanks.html', context)

