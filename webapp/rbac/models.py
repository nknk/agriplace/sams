# Use modern Python
from __future__ import unicode_literals, absolute_import, print_function

# Standard Library Imports
import time
import weakref

# Django imports
from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, Group

# External Library imports
import json_field

# Local imports
from rbac.fields import StringListField, StringSetField
from rbac.managers import GrantManager

__all__ = [
    'Role',
    'Grant',
    'RoleInstance',
    'UserRole',
]


# Managers for using natural keys (for nice dumps)
class RoleManager(models.Manager):
    def get_by_natural_key(self, slug, *args):
        return self.get(slug=slug)


class DataObjectStateManager(models.Manager):
    def get_by_natural_key(self, name, *args):
        return self.get(name=name)

    def get_data_object_state_names(self):
        cache_key = self.model.cache_key_name()
        object_state_names = cache.get(cache_key)
        if object_state_names is None:
            object_state_names = list(self.values_list('name', flat=True))
            cache.set(cache_key, object_state_names, None)
        return object_state_names


class GroupRoleManager(models.Manager):
    def get_by_natural_key(self, group, role):
        return self.get(group=group, role=role)


class DataObjectManager(models.Manager):
    def get_by_natural_key(self, name, state):
        return self.get(name=name, state=state)

    def get_data_objects_names_by_state_name(self, state_name):
        cache_key = self.model.cache_key_state_name(state_name)
        data_objects = cache.get(cache_key)
        if data_objects is None:
            data_objects = list(self.filter(state__name=state_name).values_list('name', flat=True))
            cache.set(cache_key, data_objects, None)
        return data_objects


class ValidatingModel(object):

    def save(self, force_insert=False, force_update=False, **kwargs):
        if not (force_insert or force_update):
            self.full_clean() # Will raise ValidationError if needed
        super(ValidatingModel, self).save(force_insert, force_update, **kwargs)


class Role(ValidatingModel, models.Model):
    """
    A RBAC role, aka a Role parameterized by a set of named variables. Roles
    also model privileges:  They differ only in that privileges only refer
    to real-world consequences when all parameters are instantiated.
    """

    PRIVILEGES_BY_SLUG = "RBAC_PRIVELEGES"
    ROLES_BY_ID = "RBAC_ROLES"
    _default_instance = lambda s:None

    objects = RoleManager()


    # Databaes fields
    # ---------------

    slug = models.CharField(
        max_length=256,
        help_text='The formal slug for this role, which should be unique',
        unique=True,
    )

    name = models.CharField(
        max_length=256,
        help_text='The friendly name for this role to present to users; this need not be unique.',
    )

    description = models.TextField(
        help_text='A long-form description of the intended semantics of this role.',
        blank=True,
        default='',
    )

    parameters = StringSetField(
        help_text='A set of strings which are the parameters for this role. Entered as a JSON list.',
        blank=True,
        default=set,
    )

    privelege = models.BooleanField(
        help_text='',
        default=False
    )

    # Methods
    # -------

    @classmethod
    def get_cache(cls):
        try:
            cache = cls.cache
        except AttributeError:
            timeout = getattr(settings, 'RBAC_CACHE_TIMEOUT', 60)
            cache = cls.cache = DictCache(timeout)
        return cache

    @classmethod
    def update_cache(cls):
        roles = cls.objects.prefetch_related('memberships_granted').all()
        roles = {role.id: role for role in roles}
        for role in roles.values():
            role._granted_privileges = privileges = []
            # Prevent extra queries by manually linking grants and roles
            # because Django 1.6 isn't smart enough to do this for us
            for membership in role.memberships_granted.all():
                membership.to_role = roles[membership.to_role_id]
                membership.from_role = roles[membership.from_role_id]
                privileges.append(membership.instantiated_to_role({}))
        cache = cls.get_cache()
        cache.set(cls.ROLES_BY_ID, roles)
        cache.set(cls.PRIVILEGES_BY_SLUG,
            {role.slug: role.instantiate({}) for role in roles.values()})

    @classmethod
    def get_privilege(cls, slug, assignment=None):
        """
        Optimized lookup of privilege by slug

        This optimization is specifically geared toward cases where
        `assignments` is empty.
        """
        cache = cls.get_cache()
        if cache.disabled:
            roles = Role.objects.filter(slug=slug)
            if roles:
                return roles[0].instantiate(assignment or {})
            return None
        privileges = cache.get(cls.PRIVILEGES_BY_SLUG)
        if privileges is None:
            cls.update_cache()
            privileges = cache.get(cls.PRIVILEGES_BY_SLUG)
        privilege = privileges.get(slug)
        if privilege is None:
            return None
        if assignment:
            return privilege.role.instantiate(assignment)
        return privilege

    def get_cached_role(self):
        """
        Optimized lookup of role by id
        """
        cache = self.get_cache()
        if cache.disabled:
            return self
        roles = cache.get(self.ROLES_BY_ID)
        if roles is None or self.id not in roles:
            self.update_cache()
            roles = cache.get(self.ROLES_BY_ID)
        return roles.get(self.id, self)

    def get_privileges(self, assignment):
        if not assignment:
            try:
                return self._granted_privileges
            except AttributeError:
                pass
        return [membership.instantiated_to_role(assignment)
                for membership in self.memberships_granted.all()]

    def instantiate(self, assignment):
        """
        An instantiation of this role with some parameters fixed via the provided assignments.
        """
        if assignment:
            filtered_assignment = {key: assignment[key]
                for key in self.parameters & set(assignment.keys())}
        else:
            value = self._default_instance()
            if value is not None:
                return value
            filtered_assignment = assignment
        value = RoleInstance(self, filtered_assignment)
        if not filtered_assignment:
            self._default_instance = weakref.ref(value)
        return value

    def has_privilege(self, privilege):
        """
        Shortcut for checking privileges easily for roles with no params (aka probably users)
        """
        role = self.get_cached_role()
        return role.instantiate({}).has_privilege(privilege)

    @property
    def assignment(self):
        """
        A Role stored in the database always has an empty assignment.
        """
        return {}

    def __repr__(self):
        return 'Role(%r, parameters=%r)' % (self.slug, self.parameters)

    def __unicode__(self):
        return '%s (%s)' % (self.name, self.slug)

    def get_access_rules(self):
        rule_link_list = RoleAccessRuleLink.objects.filter(role=self).values_list("rule", flat=True)
        return AccessRule.objects.filter(id__in=rule_link_list)

    def natural_key(self):
        return (self.slug, )


class Grant(ValidatingModel, models.Model):
    """
    A parameterized membership between a sub-role and super-role.
    The parameters applied to the super-role are all those.
    """


    # Database Fields
    # ---------------

    from_role = models.ForeignKey(
        'Role',
        help_text='The sub-role begin granted membership or permission',
        related_name='memberships_granted',
    )

    to_role = models.ForeignKey(
        'Role',
        help_text='The super-role or permission being given',
        related_name='members',
    )

    assignment = json_field.JSONField(
        help_text='Assignment from parameters (strings) to values (any JSON-compatible value)',
        blank=True,
        default=dict,
        db_index=True
    )

    objects = GrantManager()


    # Methods
    # -------

    def instantiated_to_role(self, assignment):
        """
        Returns the super-role instantiated with the parameters of the membership
        composed with the `parameters` passed in.
        """
        composed_assignment = {}
        if assignment:
            for key in self.to_role.parameters & set(assignment.keys()):
                composed_assignment[key] = assignment[key]
        if self.assignment:
            composed_assignment.update(self.assignment)
        return self.to_role.instantiate(composed_assignment)

    def __repr__(self):
        return 'Grant(from_role=%r, to_role=%r, assignment=%r)' % (self.from_role, self.to_role, self.assignment)

    def natural_key(self):
        return (self.from_role, self.to_role, self.assignment)


class UserRole(ValidatingModel, models.Model):
    """
    A link between a django.contrib.auth.models.User and
    a rbac.models.Role. They are kept to
    one-to-one fields to make their use extremely simple:

    request.user.rbac_role.has_privilege(...)
    """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='rbac_role')
    role = models.OneToOneField(Role, related_name='user_role')

    def has_privilege(self, privilege):
        return self.role.has_privilege(privilege)

    def __eq__(self, other):
        return self.user == other.user and self.role == other.role

    def __repr__(self):
        return 'UserRole(user=%r, role=%r)' % (self.user, self.role)


class GroupRole(ValidatingModel, models.Model):
    """
    A link between a django.contrib.auth.models.Group and
    a rbac.models.Role. They are kept to
    one-to-one fields to make their use extremely simple:

    request.user.groups.first().rbac_role.has_privilege(...)
    """
    objects = GroupRoleManager()

    group = models.OneToOneField(Group, related_name='rbac_role')
    role = models.OneToOneField(Role, related_name='group_role')

    def has_privilege(self, privilege):
        # print("has_privilege %s -> %s = %s" % (self.role, privilege, self.role.has_privilege(privilege)))
        return self.role.has_privilege(privilege)

    def __eq__(self, other):
        return self.group == other.group and self.role == other.role

    def __repr__(self):
        return 'GroupRole(group=%r, role=%r)' % (self.group, self.role)

    def natural_key(self):
        return (self.group, self.role)


class RoleInstance(object):
    """
    A parameterized role along with some parameters that are fixed. Note that this is
    not a model but only a transient Python object.
    """

    def __init__(self, role, assignment):
        self.role = role
        self.assignment = assignment
        self.slug = role.slug
        self.name = role.name
        self.parameters = role.parameters - set(assignment.keys())

    def instantiate(self, assignment):
        """
        This role further instantiated with the additional assignment.
        Note that any parameters that are already fixed are not actually
        available for being assigned, so will _not_ change.
        """
        composed_assignment = {}
        if assignment:
            for key in self.parameters & set(assignment.keys()):
                composed_assignment[key] = assignment[key]
        if self.assignment:
            composed_assignment.update(self.assignment)
        # this seems like a bug (wrong arguments). is this method ever called?
        return RoleInstance(composed_assignment)

    def has_privilege(self, privilege):
        """
        True if this instantiated role is allowed the privilege passed in,
        (which is itself an RoleInstance)
        """

        if self == privilege:
            return True

        return any(p.has_privilege(privilege)
                   for p in self.role.get_privileges(self.assignment))

    def __eq__(self, other):
        return self.slug == other.slug and self.assignment == other.assignment

    def __repr__(self):
        return 'RoleInstance(%r, parameters=%r, assignment=%r)' % (self.slug, self.parameters, self.assignment)


class DictCache(object):
    """A simple in-memory dict cache

    :param timeout: Number of seconds until an item in the cache expires.
    """

    def __init__(self, timeout=60):
        self.timeout = timeout
        self.data = {}

    @property
    def disabled(self):
        return self.timeout == 0

    def get(self, key, default=None):
        now = time.time()
        value, expires = self.data.get(key, (default, now))
        if now > expires:
            self.data.pop(key)
            return default
        return value

    def set(self, key, value):
        self.data[key] = (value, time.time() + self.timeout)

    def clear(self):
        self.data.clear()


#
# Access control list
#

class DataObjectState(models.Model):
    name = models.CharField(max_length=100, unique=True)

    objects = DataObjectStateManager()

    class Meta:
        db_table = 'DataObjectState'
        ordering = ('name',)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        self.clear_cache()
        DataObject.clear_cache(self.name)
        super(DataObjectState, self).save(force_insert=False, force_update=False, using=None,
             update_fields=None)

    def __unicode__(self):
        return u"%s" % (self.name,)

    def natural_key(self):
        return (self.name,)

    @classmethod
    def cache_key_name(cls):
        """Return the name of the key to use to cache the DataObjectState models

        Returns:
            Unicode cache key
        """
        return u"{}.{}".format(cls._meta.app_label, cls._meta.model_name)

    @classmethod
    def clear_cache(cls):
        cache_key = cls.cache_key_name()
        cache.delete(cache_key)


class DataObject(models.Model):
    name = models.CharField(max_length=500)
    state = models.ForeignKey(DataObjectState)

    objects = DataObjectManager()

    class Meta:
        db_table = 'DataObject'
        ordering = ('name',)
        unique_together = ("name", "state")

    def __unicode__(self):
        return u"%s %s" % (self.name, self.state)


    def natural_key(self):
        return (self.name, self.state)

    # def get_permissions(self):
    #     rules = RoleAccessRuleLink.objects.filter(rule__data_object=self).values_list('rule', flat=True)
    #     return Permission.objects.filter(id__in=rules)

    @classmethod
    def cache_key_state_name(cls, state_name):
        return u"{}.{}.{}".format(cls._meta.app_label, cls._meta.model_name, state_name)

    @classmethod
    def clear_cache(cls, state_name):
        cache_key = cls.cache_key_state_name(state_name)
        cache.delete(cache_key)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.clear_cache(self.state.name)
        super(DataObject, self).save(force_insert=False, force_update=False, using=None, update_fields=None)


class Permission(models.Model):
    name = models.CharField(max_length=50)  # read, write, update, delete

    class Meta:
        db_table = 'Permission'
        ordering = ('name',)

    def __unicode__(self):
        return u"%s" % (self.name,)


class AccessRule(models.Model):
    data_object = models.ForeignKey(DataObject)
    permission = models.ForeignKey(Permission)

    class Meta:
        db_table = 'AccessRule'
        ordering = ('data_object',)

    def __unicode__(self):
        return u"%s for %s" % (self.permission.name, self.data_object.name)


class RoleAccessRuleLink(models.Model):
    role = models.ForeignKey(Role)
    rule = models.ForeignKey(AccessRule)

    class Meta:
        db_table = 'RoleAccessRuleLink'
        unique_together = ("role", "rule")
        ordering = ('role',)

    def __unicode__(self):
        return u"%s <-> %s" % (self.role, self.rule)
