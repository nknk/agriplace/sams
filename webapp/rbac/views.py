from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from sams.decorators import session_organization_required

from organization.views_organization import get_organization_overview_context


@login_required
@session_organization_required
def rbac_control_panel(request, *args, **kwargs):

    context, _ = get_organization_overview_context(request, kwargs, "rbac_control_panel")
    context['page_triggers'] += ('rbac_control_panel',)
    return render(request, "rbac/control_panel.html", context)
