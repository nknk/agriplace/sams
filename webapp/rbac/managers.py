from django.db.models import Manager


class GrantManager(Manager):
    def get_queryset(self):
        return super(GrantManager, self).get_queryset()

    def update_grants(self, assignment, privilege_state, role, permission_role):
        if privilege_state:
            self.create(
                from_role=role,
                to_role=permission_role,
                assignment=assignment
            )
        else:
            grants = self.filter(from_role=role, to_role=permission_role)
            for grant in grants:
                grant_assignment = grant.assignment
                if grant_assignment['state'] == assignment['state'] and \
                                grant_assignment['data_object'] == assignment['data_object']:
                    grant.delete()
