from django.core.management.base import BaseCommand
from assessment_workflow.models import ASSESSMENT_STATUSES
from assessment_workflow.managers import (
    AUDIT_JUDGEMENTS, ASSESSMENT_ANSWERS, EVALUATION_ACTIONS, EVIDENCE, AUDIT_DATE, ASSIGN_AUDITOR,
    SHARE_WITH_AUDITOR, SHARE_WITH_COORDINATOR, DOCUMENT_REVIEW, CONFIRM_JUDGEMENT,
    OTHER_REMARKS, INTERNAL_REMARKS, AUDITOR_DECISION, PHYSICAL_REAUDIT
)
from rbac import models as rbac_models


def new_data_object(name, states):
    return [rbac_models.DataObject.objects.get_or_create(
        name=name,
        state=state
    )[0] for state in states]


class Command(BaseCommand):
    help = 'Initialize rbac data'

    def handle(self, *args, **options):
        states = [
            rbac_models.DataObjectState.objects.get_or_create(name=name[0])[0]
            for name in ASSESSMENT_STATUSES]
        print new_data_object(ASSESSMENT_ANSWERS, states)
        print new_data_object(AUDIT_JUDGEMENTS, states)
        print new_data_object(EVALUATION_ACTIONS, states)
        print new_data_object(EVIDENCE, states)
        print new_data_object(AUDIT_DATE, states)
        print new_data_object(ASSIGN_AUDITOR, states)
        print new_data_object(SHARE_WITH_AUDITOR, states)
        print new_data_object(SHARE_WITH_COORDINATOR, states)
        print new_data_object(DOCUMENT_REVIEW, states)
        print new_data_object(CONFIRM_JUDGEMENT, states)
        # --------------------------------------------------------
        print new_data_object(OTHER_REMARKS, states)
        print new_data_object(INTERNAL_REMARKS, states)
        print new_data_object(AUDITOR_DECISION, states)
        print new_data_object(PHYSICAL_REAUDIT, states)
