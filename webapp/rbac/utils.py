# Use modern Python
from __future__ import unicode_literals, absolute_import, print_function
import warnings

# Local Imports
from django.core.exceptions import ObjectDoesNotExist

from rbac.exceptions import PermissionDenied
from rbac.models import Role, UserRole, GroupRole


def has_privilege(request, slug, **assignment):
    """
    Returns true if the request has the privilege specified by slug,
    otherwise false
    """
    if not hasattr(request, 'role'):
        return False

    privilege = Role.get_privilege(slug, assignment)
    if privilege is None:
        return False

    if request.role.has_privilege(privilege):
        return True

    if not hasattr(request, 'user') or not hasattr(request.user, 'rbac_role'):
        return False
    try:
        request.user.prbac_role
    except UserRole.DoesNotExist:
        return False

    return request.user.rbac_role.has_privilege(privilege)


def user_has_privilege(user, permission_name, data_object, state):
    """
    Check if the user has the permission based on the groups the user is in
    """
    result = False
    try:
        group_roles = GroupRole.objects.filter(group=user.groups.all())
        permission_role = Role.objects.get(name=permission_name)
        return any(gr.has_privilege(permission_role.instantiate({"state": state, "data_object": data_object}))
                   for gr in group_roles)
    except ObjectDoesNotExist:
        return result


def membership_has_privilege(membership, permission_name, data_object, state):
    """
    Check if the user has the permission based on the groups the user is in
    """
    result = False
    try:
        group_roles = GroupRole.objects.filter(group=membership.role)
        permission_role = Role.objects.get(name=permission_name)
        return any(gr.has_privilege(permission_role.instantiate({"state": state, "data_object": data_object}))
                   for gr in group_roles)
    except ObjectDoesNotExist:
        return result


def ensure_request_has_privilege(request, slug, **assignment):
    """
    DEPRECATED
    """
    warnings.warn(
        '`ensure_request_has_privilege` is deprecated. You likely want '
        '`has_permission` or one of the `requires_privilege` decorators',
        DeprecationWarning
    )
    if not has_privilege(request, slug, **assignment):
        raise PermissionDenied()
