from django import template
from django.core.exceptions import ObjectDoesNotExist
from rbac.utils import user_has_privilege

register = template.Library()


from rbac.models import UserRole, Role, GroupRole


#@register.assignment_tag
#def user_has_permission(user, permission_name, data_object, state):
#    result = False
#    try:
#        user_role = UserRole.objects.get(user=user)
#        permission_role = Role.objects.get(name=permission_name)
#        return user_role.has_privilege(permission_role.instantiate({"state": state, "data_object": data_object}))
#    except ObjectDoesNotExist:
#        return result


@register.assignment_tag
def user_has_permission(user, permission_name, data_object, state):
    return user_has_privilege(user, permission_name, data_object, state)
