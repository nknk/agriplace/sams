# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import rbac.models
import rbac.fields
from django.conf import settings
import json_field.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AccessRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'ordering': ('data_object',),
                'db_table': 'AccessRule',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataObject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=500)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'DataObject',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DataObjectState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'DataObjectState',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Grant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('assignment', json_field.fields.JSONField(default=dict, help_text='Assignment from parameters (strings) to values (any JSON-compatible value)', blank=True)),
            ],
            options={
            },
            bases=(rbac.models.ValidatingModel, models.Model),
        ),
        migrations.CreateModel(
            name='GroupRole',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group', models.OneToOneField(related_name='rbac_role', to='auth.Group')),
            ],
            options={
            },
            bases=(rbac.models.ValidatingModel, models.Model),
        ),
        migrations.CreateModel(
            name='Permission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'Permission',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.CharField(help_text='The formal slug for this role, which should be unique', unique=True, max_length=256)),
                ('name', models.CharField(help_text='The friendly name for this role to present to users; this need not be unique.', max_length=256)),
                ('description', models.TextField(default='', help_text='A long-form description of the intended semantics of this role.', blank=True)),
                ('parameters', rbac.fields.StringSetField(default=set, help_text='A set of strings which are the parameters for this role. Entered as a JSON list.', blank=True)),
                ('privelege', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(rbac.models.ValidatingModel, models.Model),
        ),
        migrations.CreateModel(
            name='RoleAccessRuleLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.ForeignKey(to='rbac.Role')),
                ('rule', models.ForeignKey(to='rbac.AccessRule')),
            ],
            options={
                'ordering': ('role',),
                'db_table': 'RoleAccessRuleLink',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserRole',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('role', models.OneToOneField(related_name='user_role', to='rbac.Role')),
                ('user', models.OneToOneField(related_name='rbac_role', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(rbac.models.ValidatingModel, models.Model),
        ),
        migrations.AlterUniqueTogether(
            name='roleaccessrulelink',
            unique_together=set([('role', 'rule')]),
        ),
        migrations.AddField(
            model_name='grouprole',
            name='role',
            field=models.OneToOneField(related_name='group_role', to='rbac.Role'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='grant',
            name='from_role',
            field=models.ForeignKey(related_name='memberships_granted', to='rbac.Role', help_text='The sub-role begin granted membership or permission'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='grant',
            name='to_role',
            field=models.ForeignKey(related_name='members', to='rbac.Role', help_text='The super-role or permission being given'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dataobject',
            name='state',
            field=models.ForeignKey(to='rbac.DataObjectState'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='dataobject',
            unique_together=set([('name', 'state')]),
        ),
        migrations.AddField(
            model_name='accessrule',
            name='data_object',
            field=models.ForeignKey(to='rbac.DataObject'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='accessrule',
            name='permission',
            field=models.ForeignKey(to='rbac.Permission'),
            preserve_default=True,
        ),
    ]
