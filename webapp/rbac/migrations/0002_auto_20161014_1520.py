# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import json_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('rbac', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grant',
            name='assignment',
            field=json_field.fields.JSONField(default=dict, help_text='Assignment from parameters (strings) to values (any JSON-compatible value)', db_index=True, blank=True),
        ),
    ]
