# Use modern Python
from __future__ import unicode_literals, absolute_import, print_function

# Django imports
from django.contrib import admin
from django import forms
from django.core import urlresolvers

# External librariess
import simplejson

# Local imports
import rbac.csv
from .models import Role, Grant, UserRole, DataObject, Permission, AccessRule, DataObjectState, RoleAccessRuleLink, \
    GroupRole
from .forms import StringSetInput



__all__ = [
    'RoleAdmin',
    'RoleAdminForm',
    'GrantAdmin',
]

class RoleAdminForm(forms.ModelForm):
    class Meta:
        model = Role
        widgets = {
            'parameters': StringSetInput
        }
        exclude = []


class RoleAdmin(admin.ModelAdmin):

    model = Role
    form = RoleAdminForm

    def parameters__csv(self, instance):
        return rbac.csv.line_to_string(sorted(list(instance.parameters)))
    parameters__csv.short_description = 'Parameters'
    parameters__csv.admin_order_field = 'parameters'

    list_display = [
        'slug',
        'name',
        'parameters__csv',
        'description',
        'privelege'
    ]

    search_fields = [
        'slug',
        'name',
        'parameters',
        'description',
    ]

class GrantAdmin(admin.ModelAdmin):

    model = Grant

    def edit_link(self, instance):
        if instance.id:
            return '<a href="%s">(edit)</a>' % instance.id
        else:
            return ''
    edit_link.allow_tags = True
    edit_link.short_description = 'Actions'

    def assignment__dumps(self, instance):
        return simplejson.dumps(instance.assignment)
    assignment__dumps.short_description = 'Assignment'

    list_display = [
        'edit_link',
        'from_role',
        'to_role',
        'assignment__dumps',
    ]

    search_fields = [
        'from_role__name',
        'from_role__description',
        'to_role__name',
        'to_role__description',
    ]

    def get_queryset(self, request):
        return Grant.objects.select_related('to_role', 'from_role')

admin.site.register(Role, RoleAdmin)
admin.site.register(Grant, GrantAdmin)
admin.site.register(UserRole)
admin.site.register(GroupRole)

# admin.site.register(AccessRule)
# admin.site.register(Permission)
admin.site.register(DataObject)
admin.site.register(DataObjectState)
# admin.site.register(RoleAccessRuleLink)


