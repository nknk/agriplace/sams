from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.generics import ListCreateAPIView
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.schemas import SchemaGenerator
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer

from rbac.api.internal.serializers import RBACSerializer
from rbac.models import Role, Grant, DataObject


@api_view()
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer, CoreJSONRenderer])
def schema_view(request):
    """
    Core API schema for RBAC API
    :param request:
    :return:
    """
    generator = SchemaGenerator(title='AGRIPLACE API')
    return Response(generator.get_schema(request=request))


@api_view(['GET'])
def internal_api_rbac_root(request, format=None):
    """
    Root for Organization Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'rbac-list-create-view': reverse('rbac-list-create-view', request=request, format=format),
    })


class RBACListCreateView(ListCreateAPIView):
    serializer_class = RBACSerializer

    def get_queryset(self):
        return {
            "roles": Role.objects.filter(privelege=False),
            "privileges": Role.objects.filter(privelege=True),
            "grants": Grant.objects.all(),
            "dataobjects": DataObject.objects.all()
        }

    def get(self, request, *args, **kwargs):
        return Response(self.get_serializer(self.get_queryset()).data)

    def post(self, request, *args, **kwargs):
        if not request.data:
            return Response(status.HTTP_400_BAD_REQUEST)
        else:
            result = {
                'success': [],
                'errors': []
            }
            try:
                assignment = request.data['assignment']
                privilege_state = request.data['privilegeState']
                role = Role.objects.get(id=request.data['selectedRole']['id'])
                permission_role = Role.objects.get(id=int(request.data['privilegeId']))

                Grant.objects.update_grants(
                    assignment=assignment, privilege_state=privilege_state, role=role, permission_role=permission_role
                )
            except ObjectDoesNotExist:
                result['errors'].append({'message': 'Role not found'})

        return Response(result)
