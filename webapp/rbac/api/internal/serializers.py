from rest_framework.serializers import ModelSerializer, Serializer, Field

from rbac.models import Role, AccessRule, DataObject, Permission, Grant


class JSONSerializerField(Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        return value


class DataObjectSerializer(ModelSerializer):

    class Meta:
        model = DataObject
        fields = ('id', 'name', 'state')
        depth = 1


class GrantSerializer(ModelSerializer):

    assignment = JSONSerializerField()

    class Meta:
        model = Grant
        fields = ('from_role', 'to_role', 'assignment')


class RoleSerializer(ModelSerializer):

    class Meta:
        model = Role
        fields = ('id', 'name', 'slug', 'description')


class PermissionSerializer(ModelSerializer):

    class Meta:
        model = Permission
        fields = ('id', 'name')


class AccessRuleSerializer(ModelSerializer):
    data_object = DataObjectSerializer(source='data_object')

    class Meta:
        model = AccessRule
        fields = ('id', 'data_object')


class RBACSerializer(Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    roles = RoleSerializer(many=True)
    privileges = RoleSerializer(many=True)
    grants = GrantSerializer(many=True)
    dataobjects = DataObjectSerializer(many=True)

