from django.conf.urls import patterns, url

from rbac.api.internal.views import RBACListCreateView

urlpatterns = patterns(
    'rbac.api.internal.views',
    url(
        r'^api-root/$',
        'internal_api_rbac_root',
        name='rbac-app-api-list'
    ),
    url(
        r'cp/$',
        RBACListCreateView.as_view(),
        name='rbac-list-create-view'
    ),
)
