from django.conf.urls import patterns, url

from .views import rbac_control_panel


urlpatterns = patterns(
    '',
    url(r'^control-panel/$', rbac_control_panel, name='rbac_control_panel'),
)
