//
// Root of main AgriPlace browserify bundle / React version
//

import "babel-polyfill";
window.app							= global.app= {};
window.React						= global.React= require('react');

global._								= require('lodash');
global.Fluxxor					= require('fluxxor');
global.FluxMixin				= Fluxxor.FluxMixin(React);
global.StoreWatchMixin	= Fluxxor.StoreWatchMixin;

global.ToastMixin				= require( '../react/mixins/ToastMixin.js' );
global.GeneralEventsMixin	= require( '../react/mixins/GeneralEventsMixin.js' );

global.Bootstrap				= require('react-bootstrap');

// documentation: https://github.com/phazelift/tools.js
global.tools						= require('../lib/tools.js');

// add ap-specific and other methods to tools
_.extend( tools, require('../lib/ap-tools.js') );

// documentation: https://github.com/phazelift/sortby.js
tools.sortby						= require( 'sortby.js' );

// call tools.log without arguments (or a falsey) to not show log's to the console.
// global.log							= tools.log(true);

var customLog								= require( 'custom-log' );
global.log							= customLog({
	info				: 'INFO: ',
	warning			: 'WARNING: ',
	error				: 'ERROR: '
});

log.disable( 'info' );

global._t								= require( './translator.js' );

global.pageData					= require( '../react/components/Utils/pageData.js' );

// Page-specific apps, will need some of the above globals, so leave at bottom.
// Most components are included because they are required in one of the page apps
require('../react/apps');
