"use strict"

//
// i18next
//

var i18n								= require( 'i18next-client' );


// add new available languages here:
var availableLanguages	= {

	nl: require( '../locale/nl.json' ),
	es: require( '../locale/es.json' )
};


var selectedLanguage		= window.jsContext.LANGUAGE_CODE || 'nl';

var getI18n= function(){

	i18n.init({
		customLoad: function( lng, ns, options, loadComplete ){
				loadComplete( null, availableLanguages[selectedLanguage] );
		}
	});

	return i18n.t;
};


var translatorFunction= ( availableLanguages.hasOwnProperty(selectedLanguage) )
	? getI18n()
	// if selected language is not available, return original (english)
	: function( text ){ return text; };

module.exports= translatorFunction;
