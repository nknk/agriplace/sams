"use strict"

// a no-operation as nop in assembly enhanced with a friendly empty space return value
var nop= ()=> { return ''; };

// taken/translated from my phazelift/strings.js github/npm repo
var REGEXP_SPECIAL_CHARS= ['?', '\\', '[', ']', '(', ')', '*', '+', '.', '/', '|', '^', '$', '<', '>', '-', '&'];

var regEscape= function( string ){
	var string= tools.forceString( string );
	if ( string === '' )
		return string;

	var escaped= '',
			index,
			ch;
	for ( index in string ){
		ch= string[index];
		if ( ~REGEXP_SPECIAL_CHARS.indexOf(ch) )
			escaped+= '\\';
		escaped+= ch;
	}
	return escaped;
};



// objectFilter returns all items if query was not found, or all
// objects matching/starting with query
//
// items is an array of objects,
// query is the value to search for
// props is an array of key-names that should be available in a single item from items,
//
// if props is not given, all item's key's values will be searched for
//
// this method is not refactored yet..
//
var objectFilter= function( items, query, props ){
	if ( ! query || ! items )
		return items

	var filtered= [];

	if ( ! props ){
		props= [];
		_.forEach( items[0], function( value, key ){
			props.push( key );
		});
	}

	// all items
	_.forEach( items, function(item){
		var start= new RegExp( regEscape(query), 'i' );

		// all keys of item
		_.forEach( item, function( value, key ){

			// compare only values from props
			_.forEach( props, function( prop ){
				if ( item[prop] === value )

					// match?
					if ( start.test(value) )

						// avoid duplicates
						if ( filtered.indexOf(item) < 0 )
							filtered.push( item );
			});
		});
	});

	return filtered;
};



// this function returns an array of all keys found in the object
var allObjectKeys= function( object ){
	var keys= [];
	if ( object && (object.length > 0) )
		_.forEach( object[0], function(value, key){
			if ( Object.hasOwnProperty(key) )
				keys.push( key );
		});
	return keys;
};



var toggleListItem= function( list, item ){
  return ( list.indexOf( item ) > -1 )
      ? _.without( list, item )
      : list.concat( item );
};



var toggleCollectionItem= function( collection, item ){
    return ( _.findWhere(collection, item) )
      ? _.reject( collection, item )
      : collection.concat( item );
};



//
//																		MOMENT.JS
//

var moment= require( 'moment' );

var isIso8601= function( string ){
	if ( typeof string == 'string' )
		return ( string.match( /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}$/ ) || string.match( /^\d{4}-\d{2}-\d{2}$/ ) );
};

// require moment.js from our custom module with only the needed translations
var setMomentLang= function( lang ){
	moment= require( './moment-translations.js' )( lang );
}

// moment.js doesn't work as the manual says it seems, made some adjustments
// also the .isValid doesn't work like we need, so I added a quick regexp
// to match the iso 8601 from Agriplace database..
var dateFilter= function( string, format, locale ){
	format = format || 'D MMMM YYYY - HH:mm';
	if ( isIso8601(string) && moment(string).isValid() )
		return moment( string ).format( format );  //  || moment.ISO_8601 ).local();
	return string;
};

//																	END OF MOMENT.JS
//


var translate= function( query, replacement ){
	return ( window.localeTranslations )
		? ( window.localeTranslations[query] || replacement || query )
		: replacement || query;
};


translate.ui= function( query, replacement ){
	return ( window.localeTranslationsUI )
		? ( window.localeTranslationsUI[query] || replacement || query )
		: replacement || query;
};


var numberTail= function( string ){
	var match= string.match( /([0-9]|\.)+$/ );
	return ( match ) ? match[0] : '';
};


// Displaying the question code:
// If the code starts with the prefix,
// remove that part and the separator when displaying code
// otherwise display the entire code
var removePrefixFromString = function(s, separator) {
  separator = separator || '-';
  var shortString = s.split(separator).pop();
  return shortString;
};

//
//	If the item exists in the array, it will be removed, otherwise it will be added.
//
var toggleUnique= function( array, item ){
	var i,
			length= array.length;

	for ( i= 0; i < length; i++ )
		if ( array[i] === item ){
			array.splice( i, 1 );
			return array;
		}

	array.push( item );
	return array;
};


// converts a date string based on the current pageData.dateFormat to abbreviated iso 8601
// if conversion fails, dateToIso8601 will return null
var dateToIso8601= function( date ){
	if (!date) {
		return null;
	}
	else if ( isIso8601(date) ) {
		return date;
	}
	else {
		date= moment( moment(date, pageData.dateFormat).toDate() ).format( 'YYYY-MM-DD' );
		if ( isIso8601(date) )
			return date
		else {
      log.error( 'Error converting date, could not convert to iso-8601 !' );
			return null;
		}
	}
};



var validateDate= function( dateString ){
	return pageData.dateFormatRegExp.test( dateString );
};


var isFutureDate= function( dateString ){
	if ( dateString && validateDate(dateString) ){
		var now		= Date.now(),
				given	= Date.parse( new Date(dateToIso8601( dateString )) );
		return ( now <  given );
	}
};

var isIE = function() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');

  return !!(msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))
};

const getProdutYears = (start = 2015) => {
  let years = [];
  const currentYear = +moment().format('YYYY');
  const endYear = currentYear + 1;

  for ( let year = 2015; year <= endYear; year++) {
    years.push(year);
  }
  return years;
}

module.exports= {
	nop							: nop,
	objectFilter		: objectFilter,
	allObjectKeys		: allObjectKeys,
	toggleListItem	: toggleListItem,
	toggleCollectionItem: toggleCollectionItem,
	setMomentLang		: setMomentLang,
	moment					: moment,
	isIso8601				: isIso8601,
	dateToIso8601		: dateToIso8601,
	validateDate		: validateDate,
	isFutureDate		: isFutureDate,
	dateFilter			: dateFilter,
	translate				: translate,
	numberTail			: numberTail,
	removePrefixFromString: removePrefixFromString,
  toggleUnique    : toggleUnique,
  getProdutYears  : getProdutYears,
	isIE	         	: isIE
};