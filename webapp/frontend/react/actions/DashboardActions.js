import DashboardDAL from '../dal/dashboardDAL.js';
const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid: organizationUuid, membershipUuid } = pageData.context;
const _ = require('lodash');

const dashboardDAL = new DashboardDAL({ apiUrl, organizationUuid, membershipUuid });
const DashboardActions = {

  loadDashboard() {
    dashboardDAL.get().then((response) => {
      this.dispatch('loadDashboard', response);
    });
  },

  changeFilter({ type, value }) {
    this.dispatch('filterChanged', { type, value });
  },

  changeAuditDate({ workflowUuid, date }) {
    dashboardDAL.changeDate({ workflowUuid, date }).then(() => {
      this.dispatch('plannedAuditDateUpdated', { workflowUuid, date });
    });
  },

  changeAuditor({ workflowUuid, auditor }) {
    this.dispatch('auditorChanged', { workflowUuid, auditor });
    auditor = auditor !== 'open' ? auditor : null;
    dashboardDAL.changeAuditor({ workflowUuid, auditor });
  },

  loadAuditors() {
    this.flux.actions.organizations.loadAuditors();
    this.dispatch('auditorsChanged');
  },

  getAssessmentTypesForFilters(year) {
    const assessmentTypes = this.flux.stores.DashboardStore.getAssessmentTypes(year);
    if (!assessmentTypes) {
      this.dispatch('assessmentTypesForFiltersLoading');
      dashboardDAL.getAssessmentTypes(year).then((response) => {
        this.dispatch('assessmentTypesForFiltersFetched', { response, year });
      });
    }
  },

  loadFilteredAssessments(filters = null) {
    if (!filters) {
      const dashboardState = this.flux.stores.DashboardStore.getState();
      filters = _.clone(dashboardState.filters);
    }

    const key = this.flux.stores.DashboardStore.getFilteredKey(filters);
    const allFilteredAssessments = this.flux.stores.DashboardStore.getAllFilteredAssessments();

    if (allFilteredAssessments[key]) {
      this.dispatch('updateFilters', { filters });
    } else {
      this.dispatch('updatingFilteredAssessments');
      dashboardDAL.getFilteredAssessments(filters).then((response) => {
        this.dispatch('filteredAssessmentsFetched', { response, filters });
      });
    }
  },

  applyAssessmentTypesFilter(assessmentTypeUuids) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.assessmentTypes = assessmentTypeUuids;
    filters.pageNumber = 1;
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  getAssessmentStatesForFilters(year) {
    const assessmentStates = this.flux.stores.DashboardStore.getAssessmentStates(year);
    if (!assessmentStates) {
      this.dispatch('assessmentStatesForFiltersLoading');
      dashboardDAL.getAssessmentStates(year).then((response) => {
        this.dispatch('assessmentStatesForFiltersFetched', { response, year });
      });
    }
  },

  applyAssessmentStatesFilter(assessmentStatesUuids) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.assessmentStates = assessmentStatesUuids;
    filters.pageNumber = 1;
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  getInternalInspectorsForFilters(year) {
    const internalInspectors = this.flux.stores.DashboardStore.getInternalInspectors(year);
    if (!internalInspectors) {
      this.dispatch('internalInspectorsForFiltersLoading');
      dashboardDAL.getInternalInspectors(year).then((response) => {
        this.dispatch('internalInspectorsForFiltersFetched', { response, year });
      });
    }
  },

  applySearchFilter(search) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.search = search;
    filters.pageNumber = 1;
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  applyYearFilter(year) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.year = year;
    filters.startDate = null;
    filters.endDate = null;
    filters.pageNumber = 1;
    filters.assessmentStates = [];
    filters.internalInspectors = [];
    filters.assessmentTypes = [];
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  applyDateRangeFilter({ startDate, endDate }) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.startDate = startDate;
    filters.endDate = endDate;
    filters.pageNumber = 1;
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  applyInternalInspectorFilter(inspectorIds) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.internalInspectors = inspectorIds;
    filters.pageNumber = 1;
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  applyPageNumberFilter(pageNumber) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const filters = _.clone(dashboardState.filters);
    filters.pageNumber = pageNumber;
    this.flux.actions.dashboard.loadFilteredAssessments(filters);
  },

  getOrganizationDetails(selectedOrganizationUuid) {
    const dashboardState = this.flux.stores.DashboardStore.getState();
    const organizationDetails = dashboardState.organizationDetails[selectedOrganizationUuid];
    const isFetchingOrganizationDetails = dashboardState.isFetchingOrganizationDetails[selectedOrganizationUuid];
    if (!organizationDetails && !isFetchingOrganizationDetails) {
      this.dispatch('isFetchingOrganizationDetails', { status: true, selectedOrganizationUuid });
      dashboardDAL.getOrganizationDetails(selectedOrganizationUuid).then((details) => {
        this.dispatch('updateOrganizationDetails', { details, selectedOrganizationUuid });
      }).catch((response) => {
        if (response.status === 404) {
          // organization details not found
          this.dispatch('updateOrganizationDetails', { details: {}, selectedOrganizationUuid });
        } else {
          this.dispatch('isFetchingOrganizationDetails', { status: false, selectedOrganizationUuid });
        }
      });
    }
  },
};

export default DashboardActions;
