import HelpLinksDAL from '../dal/helpLinksDAL.js';
const helpLinksDAL = new HelpLinksDAL(window.jsContext.API_URL);

const helpLinkActions = {

  loadHelpLinks(assessmentUuid) {
    helpLinksDAL.get(assessmentUuid).then((response) => {
      this.dispatch('loadHelpLinks', response);
    });
  },
};

export default helpLinkActions;
