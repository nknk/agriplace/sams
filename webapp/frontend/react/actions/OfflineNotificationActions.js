import HeartbeatDal from '../dal/heartbeatDAL.js';
const heartbeatDal = new HeartbeatDal();

import _ from 'lodash';

const OfflineNotificationActions = {

  checkInternetStatus() {
    this.dispatch('checkInternetStatusInProgress');
    const heartBeat = () => {
      heartbeatDal.get().then((status) => {
        this.dispatch('updateInternetStatus', status);
      });
    };

    // add a debounce for spinner to be visible inside the button
    _.debounce(heartBeat, 2000)();
  },

  updateInternetStatus(status) {
    this.dispatch('updateInternetStatus', status);
  },
};

export default OfflineNotificationActions;
