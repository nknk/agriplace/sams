import FormDAL from '../dal/formDAL.js';

const formDAL = new FormDAL(window.jsContext.API_URL);

const formActions = {
  getSchema(uuid) {
    formDAL.getSchema(uuid).then(
      (payload) => {
        this.dispatch('schemaChanged', { id: uuid, formData: payload });
        log.info('getSchema: Form schema has been loaded.', payload);
      },

      (err) => {
        this.dispatch('schemaIOError');
        log.error('formSchemaActions: Could not load form schema: ', err);
      }
   );
  },

  getFormValue(uuid) {
    formDAL.getFormValue(uuid).then(
      (payload) => {
        this.dispatch('formChanged', { id: uuid, form: payload });
      },

      (err) => {
        this.flux.emit('schmeaUpdatedError');
      }
     );
  },

  saveSchema(uuid, formData) {
    this.dispatch('schemaSaveInProgress', { isLoading: true });
    formDAL.saveSchema(uuid, formData).then(
      () => {
        this.dispatch('schemaChanged', { id: uuid, formData });
        this.dispatch('schemaSaveInProgress', { isLoading: false });
        log.info('saveSchema: Form schema has been saved.');
      },

      (err) => {
        this.dispatch('schemaSaveInProgress', { isLoading: false });
        this.dispatch('schemaIOError', { error: `Error saving schema:${err.errorThrown}` });
        log.error('formSchemaActions: Could not save form schema: ', err);
      }
   );
  },

  submit(value, formUuid) {
    this.dispatch('formChanged', { id: formUuid, form: value });
    this.dispatch('formSaveInProgress', { isLoading: true });
    formDAL.submit(value, formUuid).then(
      (payload) => {
        this.dispatch('formSaveInProgress', { isLoading: false });
      },

      (err) => {
        this.dispatch('formIOError', err);
        this.dispatch('formSaveInProgress', { isLoading: false });
      }
   );
  },
};

export default formActions;
