import AssessmentsDAL from '../dal/assessmentsDAL.js';
import AnswersDal from '../dal/answersDAL.js';
const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid: organizationUuid, membershipUuid } = pageData.context;

const dal = new AssessmentsDAL(apiUrl, organizationUuid, membershipUuid);
const answersDal = new AnswersDal({ apiUrl, organizationUuid, membershipUuid });

import _ from 'lodash';

//
// Action creators for assessment related actions
//
const AssessmentsActions = {

  createAnswer({ assessmentUuid, questionUuid, answer }) {
    return new Promise(() => {
      this.dispatch('markAnswerInProgress', { assessmentUuid, questionUuid });
      answer.question = answer.question || questionUuid;
      answersDal.create(answer, assessmentUuid).then(answerReceived => {
        const assessmentsStoreState = this.flux.stores.AssessmentsStore.getState();
        const assessment = assessmentsStoreState.assessments[assessmentUuid];
        assessment.createAnswer(questionUuid, answerReceived);
        const { toCreate, toUpdate, toDelete, all } = assessment.getTriggerredAnswers();
        this.flux.actions.assessments.updateTriggerredAnswers({ toCreate, toUpdate, toDelete, all, assessmentUuid });
        this.dispatch('createAnswerReturn', { assessmentUuid, questionUuid, answerReceived, answerSent: answer });
      }).catch(() => {
        this.dispatch('createAnswerFailed', { assessmentUuid, questionUuid, answerSent: answer });
      });
    });
  },

  updateAnswer({ assessmentUuid, questionUuid, answerUuid, answer }) {
    return new Promise(() => {
      this.dispatch('markAnswerInProgress', { assessmentUuid, questionUuid });
      answer.question = answer.question || questionUuid;
      answer.uuid = answer.uuid || answerUuid;
      answersDal.update(answer, assessmentUuid, answerUuid).then(answerReceived => {
        const assessmentsStoreState = this.flux.stores.AssessmentsStore.getState();
        const assessment = assessmentsStoreState.assessments[assessmentUuid];
        assessment.updateAnswer(questionUuid, answerReceived);
        const { toCreate, toUpdate, toDelete, all } = assessment.getTriggerredAnswers();
        this.flux.actions.assessments.updateTriggerredAnswers({ toCreate, toUpdate, toDelete, all, assessmentUuid });
        this.dispatch('updateAnswerReturn', {
          assessmentUuid,
          questionUuid,
          answerReceived,
          answerSent: answer,
        });
      }).catch(() => {
        this.dispatch('updateAnswerFailed', { assessmentUuid, questionUuid, answerSent: answer });
      });
    });
  },

  deleteAnswer({ assessmentUuid, questionUuid, answerUuid }) {
    return new Promise(() => {
      this.dispatch('markAnswerInProgress', { assessmentUuid, questionUuid });
      answersDal.delete(assessmentUuid, answerUuid).then(() => {
        const assessmentsStoreState = this.flux.stores.AssessmentsStore.getState();
        const assessment = assessmentsStoreState.assessments[assessmentUuid];
        assessment.removeAnswer(questionUuid);
        const { toCreate, toUpdate, toDelete, all } = assessment.getTriggerredAnswers();
        this.flux.actions.assessments.updateTriggerredAnswers({ toCreate, toUpdate, toDelete, all, assessmentUuid });
        this.dispatch('deleteAnswerSuccess', { assessmentUuid, questionUuid });
      }).catch(() => {
        this.dispatch('deleteAnswerFailed', { assessmentUuid, questionUuid });
      });
    });
  },

  //
  // Load assessment
  //

  // This is cumulative action
  // it will load assessment, assessment type and state of the first questionnaire
  loadAssessmentFull(assessmentUuid) {
    const assessment = this.flux.stores.AssessmentsStore.getState().assessments[assessmentUuid];

    if (!assessment) {
      // Full download
      this.flux.actions.assessments.loadAssessment(assessmentUuid).then((loadedAssessment) => {
        this.flux.actions.assessments.loadAssessmentType(
          assessmentUuid, loadedAssessment.assessment.assessment_type
        ).then((assessmentType) => {
          // We need to load questionnaire state in QuestionnaireEditorApp
          // (currently this make value olny for agriforms)
          const questionnaireUuid = _.keys(assessmentType.questionnaires)[0];
          this.flux.actions.assessments.loadAssessmentState(assessmentUuid, questionnaireUuid);
        });
      });
    }
  },

  reloadAssessmentFull(assessmentUuid) {
    this.flux.actions.assessments.reloadAssessment(assessmentUuid).then((loadedAssessment) => {
      this.flux.actions.assessments.loadAssessmentType(
        assessmentUuid, loadedAssessment.assessment.assessment_type
      ).then((assessmentType) => {
        const questionnaireUuid = _.keys(assessmentType.questionnaires)[0];
        this.flux.actions.assessments.loadAssessmentState(assessmentUuid, questionnaireUuid);
      });
    });
  },

  loadAssessment(assessmentUuid) {
    return dal.loadAssessment(assessmentUuid).then((assessment) => {
      this.dispatch('assessmentLoaded', assessment);
      return assessment;
    }, (error) => { log.error('error:', error); });
  },

  reloadAssessment(assessmentUuid) {
    return dal.loadAssessment(assessmentUuid)
      .then((assessment) => {
        this.dispatch('assesmentReloaded', assessment);
        return assessment;
      })
      .catch(() => {
        this.dispatch('assesmentReloadFailed', assessmentUuid);
      });
  },

  loadAssessmentType(assessmentUuid, assessmentTypeUuid) {
    // Maybe we already have this assessmentType?
    const presentAssessmentType = this.flux.stores.AssessmentsStore.getState().assessmentTypes[assessmentTypeUuid];
    if (!presentAssessmentType) {
      return dal.loadAssessmentType(assessmentTypeUuid, assessmentUuid).then(
        (assessmentType) => {
          this.dispatch('assessmentTypeLoaded', { assessmentUuid, assessmentType });
          return assessmentType;
        },
       (error) => { log.error('error:', error); });
    }

    // Anyway, dispatch to store for binding this AssessmentType to proper Assessment
    this.dispatch(
      'assessmentTypeLoaded', { assessmentUuid, assessmentType: presentAssessmentType }
    );
    return new Promise((resolve) => resolve(presentAssessmentType));
  },

  loadPreviousAssessments(assessmentUuid, assessmentTypeUuid) {
    return dal.loadPreviousAssessments(assessmentTypeUuid).then((previousAssessments) => {
      this.dispatch('previousAssessmentsLoaded', { assessmentUuid, previousAssessments });
      return previousAssessments;
    }, (error) => { log.error('error: ', error); });
  },

  loadAssessmentState(assessmentUuid, questionnaireUuid) {
    const assessment = this.flux.stores.AssessmentsStore.getState().assessments[assessmentUuid];
    const questionnaireState = assessment && assessment.questionnaireState[questionnaireUuid];

    if (assessment && !questionnaireState) {
      dal.loadAssessmentState(assessmentUuid, questionnaireUuid).then((assessmentState) => {
        this.dispatch(
          'assessmentStateLoaded', { assessmentUuid, questionnaireUuid, assessmentState }
        );
      });
    }
  },

  //
  // Loads single questionnaire.
  //
  // Params:
  //  questionnaireUuid of questionnaire to load
  //  assessmentUuid of parent assessment
  //
  loadQuestionnaire(questionnaireUuid, assessmentUuid) {
    this.dispatch('loadQuestionnaire', {
      questionnaireUuid,
      assessmentUuid,
    });
  },

  saveAssessmentState(assessmentUuid) {
    const assessments = this.flux.stores.AssessmentsStore.getState().assessments;
    const assessment = assessments[assessmentUuid];
    _.forEach(assessment.questionnaireState, (questionnaireState, questionnaireUuid) => {
      const { isComplete, isSigned } = questionnaireState;
      dal.saveAssessmentState(assessment.uuid, questionnaireUuid, isComplete, isSigned);
    });
  },

  saveAssessmentShares(assessmentUuid, partners) {
    this.dispatch('saveAssessmentShares', { assessmentUuid, partners });
  },

  closeAssessment(assessmentUuid) {
    if (window.ga) {
      window.ga('send', 'event', 'AARRR', 'closed', 'assessment');
    }

    this.dispatch('closeAssessment', { assessmentUuid });
  },

  saveAssessmentSharesAndClose(assessmentUuid, partners) {
    if (window.ga) {
      window.ga('send', 'event', 'AARRR', 'closed', 'assessment');
    }

    this.dispatch('saveAssessmentSharesAndClose', {
      assessmentUuid,
      partners,
    });
  },

  //
  // do not use, for future purpose
  // uncomment if needed, do not forget to uncomment in AssessmentsStore as well
  //
  reopenAssessment(assessmentUuid) {
    if (window.ga) {
      window.ga('send', 'event', 'AARRR', 'closed', 'assessment');
    }

    this.dispatch('reopenAssessment', { assessmentUuid });
  },

  sendRegistrationInfo({ value, workflowContextUUID, callback }) {
    dal.createRegistrationInfo({ payload: { form_data: value }, workflowContextUUID })
    .then(() => {
      if (typeof callback === 'function') {
        callback();
      }
    });
  },

  answerEvidenceReuse(assessmentUuid) {
    dal.setEvidenceReuseAnswer({ assessmentId: assessmentUuid, val: true });
    this.dispatch('answerEvidenceReuseChanged', { assessmentUuid, val: true });
  },

  assessmentReuse(sourceAssessmentUuid, targetAssessmentUuid) {
    this.dispatch('assessmentReuseInProgress');
    dal.assessmentReuse(sourceAssessmentUuid, targetAssessmentUuid).then(() => {
      this.dispatch('assessmentReuseCompleted');
      this.dispatch('assessmentReuseStatusFetched', {
        assessmentUuid: targetAssessmentUuid,
        status: 'pending',
      });
    }).catch(() => {
      this.dispatch('assessmentReuseFailed');
    });
  },

  // deprecated
  closeAssessmentReuseSuccessModal() {
    this.dispatch('closeAssessmentReuseSuccessModal');
  },

  loadAssessmentReuseStatus(assessmentUuid) {
    this.dispatch('assessmentReuseStatusInProgress', true);
    dal.assessmentReuseStatus(assessmentUuid).then((response) => {
      // trigger a function to stop spinner after 1.5 seconds
      setTimeout(() => { this.dispatch('assessmentReuseStatusInProgress', false); }, 1500);

      if (response.status === 'done' || response.status === 'error') {
        // if resue is complete then reload the assessment
        this.flux.actions.assessments.reloadAssessmentFull(assessmentUuid);
      } else {
        this.dispatch('assessmentReuseStatusFetched', { assessmentUuid, status: response.status });
      }
    }).catch(() => {
      this.dispatch('assessmentReuseStatusFailed');
    });
  },

  loadAssessmentReport(assessmentUuid) {
    dal.loadAssessmentReport(assessmentUuid).then((report) => {
      this.dispatch('assessmentReportLoaded', { assessmentUuid, report });
    });
  },

  createAnswers({ assessmentUuid, answers }) {
    return new Promise((resolve) => {
      if (answers && (answers.length > 0)) {
        answersDal.createAnswers(answers, assessmentUuid).then(response => {
          const answersReceived = response.data || {}; // its a dictionary

          const assessmentsStoreState = this.flux.stores.AssessmentsStore.getState();
          const assessment = assessmentsStoreState.assessments[assessmentUuid];

          _.each(answersReceived, (answerReceived) => {
            assessment.createAnswer(answerReceived.question, answerReceived);
          });

          const { toCreate, toUpdate, toDelete, all } = assessment.getTriggerredAnswers();
          this.flux.actions.assessments.updateTriggerredAnswers({ toCreate, toUpdate, toDelete, all, assessmentUuid });

          this.dispatch('createAnswersReturn', { assessmentUuid, answersReceived, answersSent: answers });
          resolve();
        }).catch(() => {
          this.dispatch('createAnswersFailed', { assessmentUuid, answers });
          resolve();
        });
      } else {
        resolve();
      }
    });
  },

  updateAnswers({ assessmentUuid, answers }) {
    return new Promise((resolve) => {
      const assessmentActions = this.flux.actions.assessments;
      _.each(answers, (answer) => {
        const answerUuid = answer.uuid;
        const questionUuid = answer.question;
        assessmentActions.updateAnswer({ assessmentUuid, questionUuid, answerUuid, answer });
      });
      resolve();
    });
  },

  deleteAnswers({ assessmentUuid, answers }) {
    return new Promise((resolve) => {
      const assessmentActions = this.flux.actions.assessments;
      _.each(answers, (answer) => {
        const answerUuid = answer.uuid;
        const questionUuid = answer.question;
        assessmentActions.deleteAnswer({ assessmentUuid, questionUuid, answerUuid });
      });
      resolve();
    });
  },

  updateAnswersForDocType({ assessmentUuid, documentTypeUuid }) {
    const assessmentsStoreState = this.flux.stores.AssessmentsStore.getState();
    const assessment = assessmentsStoreState.assessments[assessmentUuid];
    if (assessment) {
      const { toCreate, toUpdate, toDelete, all } = assessment.getAnswersToUpdate(documentTypeUuid);
      this.dispatch('markAnswersInProgress', { assessmentUuid, answers: all });
      const assessmentActions = this.flux.actions.assessments;
      assessmentActions.createAnswers({ assessmentUuid, answers: toCreate })
        .then(() => { assessmentActions.updateAnswers({ assessmentUuid, answers: toUpdate }); })
        .then(() => { assessmentActions.deleteAnswers({ assessmentUuid, answers: toDelete }); });
    }
  },

  updateTriggerredAnswers({ toCreate, toUpdate, toDelete, all, assessmentUuid }) {
    const assessmentActions = this.flux.actions.assessments;
    this.dispatch('markAnswersInProgress', { assessmentUuid, answers: all });
    assessmentActions.createAnswers({ assessmentUuid, answers: toCreate })
      .then(() => { assessmentActions.updateAnswers({ assessmentUuid, answers: toUpdate }); })
      .then(() => { assessmentActions.deleteAnswers({ assessmentUuid, answers: toDelete }); });
  },
};

export default AssessmentsActions;
