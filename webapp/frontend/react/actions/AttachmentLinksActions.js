import AttachmentLinksDAL from '../dal/attachmentLinksDAL.js';
const attachmentLinksDAL = new AttachmentLinksDAL(window.jsContext.API_URL);

function trackAttachmentLinks(assessment, delta) {
  const ga = window.ga;
  if (!ga) {
    return;
  }

  const numFilledAttachmentTypes = Object.keys(
    assessment.attachmentLinks).reduce((previousValue, attachmentLinkUuid) =>
    previousValue + assessment.attachmentLinks[attachmentLinkUuid].length, delta
  );

  if (numFilledAttachmentTypes > 5) {
    ga('send', 'event', 'AARRR', 'inprogress', 'attachmentLinks');
  }
}

const AttachmentsLinkActions = {

  createAttachmentLink(attachmentLink, assessmentUuid, cb) {
    const documentTypeUuid = attachmentLink[0] && attachmentLink[0].document_type_attachment;
    attachmentLinksDAL.create(attachmentLink, assessmentUuid).then((response) => {
      this.dispatch('createAttachmentLink', response);
      this.flux.actions.assessments.updateAnswersForDocType({ assessmentUuid, documentTypeUuid });

      // Ugly hack to open modal for agriform
      // TODO: rewrite
      if (cb) {
        cb(response[0]);
      }
    });

    trackAttachmentLinks(
      this.flux.stores.AssessmentsStore.getState().assessments[assessmentUuid].getState(), 1
    );
  },

  updateAttachmentLink(attachmentLink, assessmentUuid, attachmentLinkUuid) {
    const documentTypeUuid = attachmentLink.document_type_attachment;
    attachmentLinksDAL.update(
      attachmentLink, assessmentUuid, attachmentLinkUuid
    ).then((response) => {
      this.dispatch('updateAttachmentLink', response);
      this.flux.actions.assessments.updateAnswersForDocType({ assessmentUuid, documentTypeUuid });
    });
  },

  deleteAttachmentLink(assessmentUuid, attachmentLinkUuid) {
    attachmentLinksDAL.delete(assessmentUuid, attachmentLinkUuid).then((response) => {
      const documentTypeUuid = response.document_type_attachment;
      this.dispatch('deleteAttachmentLink', response);
      this.flux.actions.assessments.updateAnswersForDocType({ assessmentUuid, documentTypeUuid });
    });

    trackAttachmentLinks(
      this.flux.stores.AssessmentsStore.getState().assessments[assessmentUuid].getState(), -1
    );
  },
};

export default AttachmentsLinkActions;
