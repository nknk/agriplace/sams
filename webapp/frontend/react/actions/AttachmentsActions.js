
import AttachmentsDAL from '../dal/attachmentsDAL.js';
const attachmentsDAL = new AttachmentsDAL(window.jsContext.API_URL);

const AttachmentsActions = {
  loadAttachmentsOfType(attachmentsType) {
    this.dispatch('loadAttachmentsOfType', { attachmentsType });
  },

  reLoadFilteredAttachments() {
    const attachmentsStore = this.flux.stores.AttachmentsStore;
    // no need to clear filters, clear attachments list only
    attachmentsStore.filteredAttachments = {};
    attachmentsStore.pageCount = {};
    this.flux.actions.attachments.applyPageNumber({ pageNumber: attachmentsStore.pageNumber });
  },

  sortAttachments(key, sortOrder) {
    this.dispatch('sortAttachments', {
      key,
      sortOrder,
    });
  },

  sortFilteredAttachments(key, sortOrder) {
    this.dispatch('sortFilteredAttachments', {
      key,
      sortOrder,
    });
  },

  applyFilters(filters) {
    // get missing pageNumber from store
    const attachmentsStore = this.flux.stores.AttachmentsStore;
    filters.pageNumber = attachmentsStore.filters.pageNumber;
    this.flux.actions.attachments.getFilteredAttachments(filters);
  },

  applyPageNumber(filters) {
    // get missing filters from store
    const attachmentsStore = this.flux.stores.AttachmentsStore;
    filters.startDate = attachmentsStore.filters.startDate;
    filters.endDate = attachmentsStore.filters.endDate;
    filters.title = attachmentsStore.filters.title;
    filters.usedIn = attachmentsStore.filters.usedIn;
    filters.usableFor = attachmentsStore.filters.usableFor;
    filters.attachmentType = attachmentsStore.filters.attachmentType;
    filters.evidenceType = attachmentsStore.filters.evidenceType;
    filters.isOptionalEvidenceIncluded = attachmentsStore.filters.isOptionalEvidenceIncluded;
    this.flux.actions.attachments.getFilteredAttachments(filters);
  },

  getFilteredAttachments(filters) {
    const attachmentsStore = this.flux.stores.AttachmentsStore;
    const filteredKey = attachmentsStore.getFilteredKey(filters);

    if (attachmentsStore.filteredAttachments[filteredKey]) {
      this.dispatch('filteredAttachmentsUpdated', { filters });
    } else {
      this.dispatch('loadingFilteredAttachments');
      attachmentsDAL.applyFilters(filters)
        .then((response) => {
          this.dispatch('filteredAttachmentsUpdated', { response, filters });
        })
        .catch((response) => {
          if (filters.pageNumber > 1) {
            filters.pageNumber = 1;
            this.flux.actions.attachments.getFilteredAttachments(filters);
          }
        });
    }
  },

  saveAttachment(attachment) {
    this.dispatch('saveAttachment', { attachment });
  },

  updateAttachment(attachment) {
    this.dispatch('updateAttachment', { attachment });
  },

  deleteAttachment(attachment) {
    this.dispatch('deleteAttachment', { attachment });
  },

  getEvidenceTypesList() {
    attachmentsDAL.getEvidenceTypesList().then((response) => {
      this.dispatch('evidenceTypesListFetched', response);
    });
  },

  getAttachmentTypesList() {
    attachmentsDAL.getAttachmentTypesList().then((response) => {
      this.dispatch('attachmentTypesListFetched', response);
    });
  },

  getUsedInList() {
    attachmentsDAL.getUsedInList().then((response) => {
      this.dispatch('usedInListFetched', response);
    });
  },

  getUsableForList() {
    attachmentsDAL.getUsableForList().then((response) => {
      this.dispatch('usableForListFetched', response);
    });
  },

};

export default AttachmentsActions;
