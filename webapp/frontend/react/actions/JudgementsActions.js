const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid: organizationUuid, membershipUuid } = pageData.context;
import JudgementsDAL from '../dal/judgementsDAL.js';
const judgementsDAL = new JudgementsDAL({ apiUrl, organizationUuid, membershipUuid });

const judgementsActions = {

  changeJudgement(judgement) {
    this.dispatch('judgementChanged', { judgement });
  },

  load({ assessmentUuid }) {
    judgementsDAL.get({ assessmentUuid }).then(
      judgements => this.dispatch('judgementsLoaded', judgements)
    );
  },

  create({ assessmentUuid, judgement }) {
    this.dispatch('judgementSaving', { questionUuid: judgement.question });
    judgementsDAL.create({ judgements: [judgement], assessmentUuid }).then((createdJudgements) => {
      this.dispatch('createdJudgementSaved', { judgements: createdJudgements });
    });
  },

  delete({ assessmentUuid, judgement }) {
    this.dispatch('judgementSaving', { questionUuid: judgement.question });
    judgementsDAL.delete({ assessmentUuid, judgementUuid: judgement.uuid }).then(() => {
      this.dispatch('deletedJudgementSaved', { judgement });
    });
  },

  update({ assessmentUuid, judgement, judgementUuid }) {
    this.dispatch('judgementSaving', { questionUuid: judgement.question });
    judgementsDAL.update({ assessmentUuid, judgement, judgementUuid }).then((updatedJudgement) => {
      this.dispatch('updatedJudgementSaved', { judgement: updatedJudgement });
    });
  },

  openConfirmAllModal() {
    this.dispatch('openConfirmAllModal');
  },

  closeConfirmAllModal() {
    this.dispatch('closeConfirmAllModal');
  },

  confirmUnansweredJudgements({ assessmentUuid, questionnaireUuid }) {
    this.dispatch('confirmAllInprogress');
    judgementsDAL.confirmUnansweredJudgements({ assessmentUuid, questionnaireUuid })
      .then(judgements => {
        this.dispatch('judgementsLoaded', judgements);
        this.dispatch('unansweredJudgementsUpdated');
      })
      .catch(() => {
        this.dispatch('failedToUpdateUnansweredJudgements');
      });
  },
};

export default judgementsActions;
