import CertificatesDAL from '../dal/certificatesDAL.js';
const pageData = require('../components/Utils/pageData.js');
const { apiUrl } = pageData.context;
const certificatesDAL = new CertificatesDAL(apiUrl);

const CertificatesActions = {

  getCertificatesList(organizationUuid) {
    const certificatesStoreState = this.flux.stores.CertificatesStore.getState();
    const isCertificateListFetched = certificatesStoreState.isFetched || {};

    if (!isCertificateListFetched[organizationUuid]) {
      certificatesDAL.getList(organizationUuid).then((response) => {
        this.dispatch('updateCertificatesList', { organizationUuid, response });
      });
    }
  },

  resetSelectedCertificate() {
    this.dispatch('resetSelectedCertificate');
  },

  getCertificate(organizationUuid, certificateUuid) {
    const certificatesStoreState = this.flux.stores.CertificatesStore.getState();
    const certificateList = certificatesStoreState.certificateList[organizationUuid] || [];
    const certificates = certificatesStoreState.certificates[organizationUuid] || {};

    const certificate = certificates[certificateUuid]
      || certificateList.find(cert => cert.uuid === certificateUuid);

    if (certificate) {
      this.dispatch('getCertificate', { organizationUuid, response: certificate });
    } else {
      certificatesDAL.getCertificate(organizationUuid, certificateUuid).then((response) => {
        this.dispatch('getCertificate', { organizationUuid, response });
      });
    }
  },

  deleteCertificate({ organizationUuid, certificateUuid, cb }) {
    certificatesDAL.delete(organizationUuid, certificateUuid)
      .then(() => {
        if (cb) {
          cb();
        }

        this.dispatch('certificateDeleted', { organizationUuid, certificateUuid });
      })
      .catch(() => {
        certificatesDAL.getList(organizationUuid).then((response) => {
          this.dispatch('updateCertificatesList', { organizationUuid, response });
        });
      });
  },

  getCertificateTypes(organizationUuid) {
    const certificatesStoreState = this.flux.stores.CertificatesStore.getState();
    const _certificateTypes = certificatesStoreState.certificateTypes || {};

    if (!_certificateTypes[organizationUuid]) {
      certificatesDAL.getCertificateTypes(organizationUuid).then(certificateTypes => {
        this.dispatch('certificateTypesUpdated', { organizationUuid, certificateTypes });
      });
    }
  },

  create({ certificate, organizationUuid, cb }) {
    this.dispatch('createOrUpdateCertificateInProgress');
    this.dispatch('createCertificateStarted');

    certificatesDAL.create({ certificate, organizationUuid }).then(certificateCreated => {
      if (cb) { cb(); }
      this.dispatch('createCertificate', { organizationUuid, certificateCreated });
    });
  },

  update({ certificate, organizationUuid, cb }) {
    this.dispatch('createOrUpdateCertificateInProgress');
    const certificateUuid = certificate.uuid;
    certificatesDAL.update({ certificate, organizationUuid }).then(certificateCreated => {
      if (!certificateCreated.uuid) {
        certificateCreated.uuid = certificateUuid;
      }

      if (cb) {
        cb();
      }

      this.dispatch('updateCertificate', { organizationUuid, certificateCreated, certificateUuid });
    });
  },

};

export default CertificatesActions;
