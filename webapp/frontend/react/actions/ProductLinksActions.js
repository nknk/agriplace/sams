import ProductLinksDAL from '../dal/productLinksDAL.js';

const productsLinksDAL = new ProductLinksDAL(window.jsContext.API_URL);
const productLinksActions = {

  loadProductLinks(assessmentUuid) {
    productsLinksDAL.get(assessmentUuid).then(
      (productLinks) => this.dispatch('productLinksLoaded', productLinks)
    );
  },

  // Here we got all selected links
  change(assessmentUuid, updatedProductLinks) {
    this.flux.actions.productLinks.changeAssessment(assessmentUuid, updatedProductLinks);

    const actualProductLinks = this.flux.stores.ProductLinksStore.getState().productLinks.map(
      pl => pl.uuid
    );

    const productsToAdd = updatedProductLinks.filter(upl => !~actualProductLinks.indexOf(upl));
    const productsToRemove = actualProductLinks.filter(apl => !~updatedProductLinks.indexOf(apl));

    this.flux.actions.productLinks.add(assessmentUuid, productsToAdd);
    this.flux.actions.productLinks.remove(assessmentUuid, productsToRemove);
    this.flux.actions.productLinks.addProductDependentQuestionnaires(assessmentUuid, productsToAdd);
    this.flux.actions.productLinks.removeProductDependentQuestionnaires(assessmentUuid, productsToRemove);
  },

  add(assessmentUuid, productLinks) {
    if (!_.isEmpty(productLinks)) {
      this.dispatch('addProductLinksStarted', productLinks);
      productsLinksDAL.create(assessmentUuid, productLinks)
        .then(() => this.dispatch('addProductLinksFinished', productLinks))
        .catch(e => console.error('error:', e));
    }
  },

  remove(assessmentUuid, productLinks) {
    if (!_.isEmpty(productLinks)) {
      this.dispatch('removeProductLinksStarted', productLinks);
      _.forEach(productLinks, link => productsLinksDAL.delete(assessmentUuid, link)
        .then(() => this.dispatch('removeProductLinksFinished', [link]))
        .catch(e => console.error('error:', e))
      );
    }
  },

  changeAssessment(assessmentUuid, updatedProductLinks) {
    const products = this.flux.stores.ProductsStore.getState().products;
    const updatedProducts = updatedProductLinks.map(upl => products.find(p => p.uuid === upl));

    this.dispatch('productsChangedFromTab', { assessmentUuid, updatedProducts });
  },

  addProductDependentQuestionnaires(assessmentId, productIds) {
    if (!_.isEmpty(productIds)) {
      productsLinksDAL.getProductDependentQuestionnaires(assessmentId, productIds)
          .then(questionnaires => this.dispatch('addProductDependentQuestionnairesFinished', questionnaires))
          .catch(e => console.error('error:', e));
    }
  },

  removeProductDependentQuestionnaires(assessmentId, productIds) {
    if (!_.isEmpty(productIds)) {
      productsLinksDAL.getProductDependentQuestionnaires(assessmentId, productIds)
          .then(questionnaires => this.dispatch('removeProductDependentQuestionnairesFinished', questionnaires))
          .catch(e => console.error('error:', e));
    }
  },
};

export default productLinksActions;
