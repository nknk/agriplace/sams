import ProductsDAL from '../dal/productsDAL.js';

const productsDAL = new ProductsDAL(window.jsContext.API_URL);

const productActions = {

  loadProductTypes() {
    productsDAL.getProductTypes().then(
      (payload) => {
        this.dispatch('productTypesUpdated', payload);
      },

      (err) => {
        this.flux.emit('loadProductTypesError', err);
      }
    );
  },

  // use getProductTypes as default as it only loads once from the server if productTypes is not
  // yet available in the product store
  getProductTypes() {
    const productTypes = this.flux.stores.ProductsStore.productTypes;
    if (!productTypes.length) {
      this.flux.actions.products.loadProductTypes();
    }
  },

  loadProducts(uuid) {
    productsDAL.getProducts({ organizationUuid: uuid }).then(
      (payload) => {
        this.dispatch('productsUpdated', { products: payload });
      },

      (err) => {
        this.flux.emit('updateProductsError');
      }
   );
  },

  addProduct(product) {
    this.dispatch('productAdded', { product });
    productsDAL.createProduct({ products: product }).then(
      (payload) => {
        this.dispatch('productUpdated', { product: payload, productUuid: payload.uuid });
      },

      (err) => {
        this.flux.emit('addProductError');
      }
      );
  },

  addProductAndSelect(product, assessmentUuid) {
    this.dispatch('productAdded', { product });
    productsDAL.createProduct({ products: product }).then(
      (payload) => {
        this.dispatch('productUpdated', { product: payload, productUuid: payload.uuid });
        const actualProductLinks = this.flux.stores.ProductLinksStore.getState().productLinks.map(
          pl => pl.uuid
        );

        this.flux.actions.productLinks.change(
          assessmentUuid, actualProductLinks.concat(payload.uuid)
        );
      },

      (err) => {
        this.flux.emit('addProductError');
      });
  },

  deleteProduct({ uuid }) {
    this.dispatch('productDeleted', { uuid });
    productsDAL.deleteProduct({ uuid }).then(
    () => {
    },

    (err) => {
      this.flux.emit('deleteProductError');
    });
  },

  updateProduct({ product, productUuid }) {
    this.dispatch('productUpdated', { product, productUuid });
    productsDAL.updateProduct({ product, productUuid }).then(

      (payload) => {
        // this.dispatch('historyUpdated', {
        //   history: payload, historyUuid: payload.uuid, productUuid,
        // });
      },

      (err) => {
      });
  },

  changeYear({ year }) {
    this.dispatch('yearChanged', { year });
  },
};

export default productActions;
