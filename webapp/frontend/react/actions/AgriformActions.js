const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid: organizationUuid, membershipUuid } = pageData.context;

import AssessmentsDAL from '../dal/assessmentsDAL.js';
const dal = new AssessmentsDAL(apiUrl, organizationUuid, membershipUuid);

//
// Action creators for agriform related actions
//
const AgriformActions = {
  //
  // Creates agriform.
  //
  // - API: create assessment
  // - Dispatch add assessment
  // - Dispatch create attachment
  //
  // Params:
  //  attachmentLink
  //  assessmentUuid of parent assessment
  //
  createAgriformAttachment(attachmentLink, _organizationUuid, cb) {
    const agriformAssessmentTypeUuid = attachmentLink.attachment.agriformAssessmentTypeUuid;

    const createAssessmentPromise = dal.createAssessment(_organizationUuid, {
      assessment_type: agriformAssessmentTypeUuid,
      organization: _organizationUuid,
      name: attachmentLink.attachment.name,
      products: [],  // TODO Products?
    });

    createAssessmentPromise.then(

      // success
      (agriformAssessment) => {
        log.info('[create assessment] success.', agriformAssessment);
        attachmentLink.attachment.agriformUuid = agriformAssessment.uuid;
        this.flux.actions.attachmentLinks.createAttachmentLink([attachmentLink], attachmentLink.assessment, cb);
        const documentTypeUuid = attachmentLink.document_type_attachment;
        this.flux.actions.assessments.updateAnswersForDocType({
          assessmentUuid: agriformAssessment.uuid,
          documentTypeUuid,
        });
        this.flux.actions.assessments.loadAssessmentFull(agriformAssessment.uuid);
      },

      // created agriform assessment failed
      (errors) => {
        log.error('errors:', errors);

        // TODO Tell user agriform create failed
      }

    );
  },

  setSignature(assessmentUuid, questionnaireUuid, isSigned) {
    this.dispatch('signatureStateChanged', { assessmentUuid, questionnaireUuid, isSigned });
  },

};

export default AgriformActions;
