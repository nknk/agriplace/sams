import HistoricalAttachmentDAL from '../dal/historicalAttachmentDAL.js';
const historicalAttachmentDAL = new HistoricalAttachmentDAL(window.jsContext.API_URL);

const DocTypeReuseActions = {
  getHistoricalAttachmentsList(assessmentUuid, documentTypeUuid) {
    this.dispatch('loadHistoricalAttachmentsStarted', documentTypeUuid);

    historicalAttachmentDAL.get(assessmentUuid, documentTypeUuid).then((response) => {
      this.dispatch('loadHistoricalAttachments', response);
    });
  },

  updateHistoricalAttachmentsList(assessmentUuid, documentTypeUuid, attachments, cb) {
    this.dispatch('updateHistoricalAttachmentsStarted', documentTypeUuid);
    historicalAttachmentDAL.create(
      assessmentUuid, documentTypeUuid, attachments
    ).then((response) => {
      this.dispatch('updateHistoricalAttachmentsComplete', documentTypeUuid);
      this.dispatch('createAttachmentLink', response);
      this.flux.actions.assessments.updateAnswersForDocType({ assessmentUuid, documentTypeUuid });
      if (cb) { cb(); }
    });
  },
};

export default DocTypeReuseActions;
