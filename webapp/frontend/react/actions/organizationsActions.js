import OrganizationsDAL from '../dal/organizationsDAL.js';
const organizationsDAL = new OrganizationsDAL(window.jsContext.API_URL);

const organizationsActions = {

  loadOrganizations() {
    const organizations = this.flux.stores.OrganizationsStore.getState().organizations;
    if (_.isEmpty(organizations)) {
      organizationsDAL.loadOrganizations().then(

        (payload) => {
          this.dispatch('organizationsUpdated', payload);
          log.info('organizationsActions: organizations are updated.', payload);
        },

        (err) => {
          this.flux.emit('updateOrganizationsError', err);
          log.error('organizationsActions: Could not update organizations. ', err);
        }

      );
    }
  },

  loadAuditorsByAssessement(assessmentUuid) {
    organizationsDAL.loadAuditorsByAssessement(assessmentUuid).then(

      (payload) => {
        this.dispatch('auditorsUpdated', payload);
        log.info('auditorsActions: organizations are updated.', payload);
      },

      (err) => {
        this.flux.emit('updateOrganizationsError', err);
        log.error('auditorsActions: Could not update auditors. ', err);
      }

    );
  },

  loadAuditors() {
    organizationsDAL.loadAuditors().then(

      (payload) => {
        this.dispatch('auditorsUpdated', payload);
        log.info('auditorsActions: organizations are updated.', payload);
      },

      (err) => {
        this.flux.emit('updateOrganizationsError', err);
        log.error('auditorsActions: Could not update auditors. ', err);
      }

    );
  },

  loadTraders() {
    organizationsDAL.loadTraders().then(

      (payload) => {
        this.dispatch('tradersUpdated', payload);
        log.info('TradersActions: organizations are updated.', payload);
      },

      (err) => {
        this.flux.emit('updateOrganizationsError', err);
        log.error('TradersActions: Could not update auditors. ', err);
      }

    );
  },
};

export default organizationsActions;
