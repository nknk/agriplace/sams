const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid, membershipUuid } = pageData.context;
const organizationUuid = uuid;

import SharedEvidenceDAL from '../dal/sharedEvidenceDAL.js';
const sharedEvidenceDAL = new SharedEvidenceDAL(apiUrl);

const SharedEvidenceActions = {

  getSharingOrganizations() {
    const sharedEvidenceStoreState = this.flux.stores.SharedEvidenceStore.getState();
    const sharingOrganizations = sharedEvidenceStoreState.sharingOrganizations;
    if (!sharingOrganizations.length) {
      sharedEvidenceDAL.getSharingOrganizations(organizationUuid).then(response => {
        this.dispatch('sharingOrganizationsFetched', response);
      });
    }
  },

  sortSharingOrganizations(column) {
    this.dispatch('sortSharingOrganizations', { column });
  },

  sharingOrganizationSelected(selectedOrganizationUuid) {
    this.dispatch('sharingOrganizationSelected', selectedOrganizationUuid);
  },

  getListOfAttachments(selectedOrganizationUuid) {
    const sharedEvidenceStoreState = this.flux.stores.SharedEvidenceStore.getState();
    const listOfAttachments = sharedEvidenceStoreState.sharedAttachments[selectedOrganizationUuid];

    if (!listOfAttachments) {
      sharedEvidenceDAL.getListOfAttachments(organizationUuid, selectedOrganizationUuid).then(response => {
        this.dispatch('listOfAttachmentsFetched', { selectedOrganizationUuid, response: response[0].attachments });
      });
    }
  },

  reLoadListOfAttachments(selectedOrganizationUuid) {
    sharedEvidenceDAL.getListOfAttachments(organizationUuid, selectedOrganizationUuid).then(response => {
      this.dispatch('listOfAttachmentsFetched', { selectedOrganizationUuid, response: response[0].attachments });
    });
  },

  selectSharedAttachment(isSelected, attachmentUuid) {
    this.dispatch('selectSharedAttachment', { isSelected, attachmentUuid });
  },

  selectAllSharedAttachments(isSelected) {
    this.dispatch('selectAllSharedAttachments', isSelected);
  },

  sortSharedAttachments(column) {
    this.dispatch('sortSharedAttachments', { column });
  },

  toggleCopyToArchive() {
    this.dispatch('toggleCopyToArchive');
  },

  openSharedEvidenceConfirmationModal() {
    this.dispatch('openSharedEvidenceConfirmationModal');
  },

  copySharedEvidenceToArchive() {
    this.dispatch('copySharedEvidenceToArchiveInProgress');

    const sharedEvidenceStoreState = this.flux.stores.SharedEvidenceStore.getState();
    const selectedOrganizationUuid = sharedEvidenceStoreState.selectedOrganization.uuid;
    const selectedAttachments = sharedEvidenceStoreState.selectedSharedAttachments[selectedOrganizationUuid];
    const selectedOrganization = sharedEvidenceStoreState.selectedOrganization;

    // used only for calling action copy to assessments
    const selectedOpenAssessmentTypes = sharedEvidenceStoreState.selectedOpenAssessmentTypes;

    const selectedAttachmentUuids = selectedAttachments.map(a => a.uuid);
    sharedEvidenceDAL.copySharedAttachmentsToArchive(organizationUuid, selectedOrganizationUuid,
      selectedAttachmentUuids)
    .then(() => {
      // update the status of organization to in progress
      selectedOrganization.state = _t('In progress');
      this.dispatch('copySharedAttachmentsToArchiveSuccess');

      // if any open assessment types are selected then also call action to copy assessments
      if (selectedOpenAssessmentTypes.length) {
        this.flux.actions.sharedEvidence.copySharedEvidenceToOpenAssessments({
          selectedOrganization,
          selectedOpenAssessmentTypes,
          selectedAttachments,
        });
      }
    })
    .catch(() => {
      this.dispatch('copySharedAttachmentsToArchiveFailure');

      // This is not a good practice, but
      // incase a timeout occurs then still make a call to copy evidences to assessments
      if (selectedOpenAssessmentTypes.length) {
        this.flux.actions.sharedEvidence.copySharedEvidenceToOpenAssessments({
          selectedOrganization,
          selectedOpenAssessmentTypes,
          selectedAttachments,
        });
      }
    });
  },

  copySharedEvidenceCancel() {
    this.dispatch('copySharedEvidenceCancelled');
  },

  getOpenAssessmentTypes() {
    // check if already exists in store
    const sharedEvidenceStoreState = this.flux.stores.SharedEvidenceStore.getState();
    const openAssessmentTypesFetched = sharedEvidenceStoreState.openAssessmentTypesFetched;

    if (!openAssessmentTypesFetched) {
      sharedEvidenceDAL.getOpenAssessmentTypes(membershipUuid).then(response => {
        const openAssessmentTypes = response;
        this.dispatch('openAssessmentTypesFetched', { openAssessmentTypes });
      });
    }
  },

  openAssessmentTypeSelected({ isSelected, assessmentTypeUuid }) {
    this.dispatch('openAssessmentTypeSelected', { isSelected, assessmentTypeUuid });
  },

  copySharedEvidenceToOpenAssessments({
    selectedOrganization = null,
    selectedOpenAssessmentTypes = null,
    selectedAttachments = null,
  }) {
    this.dispatch('copySharedEvidenceToAssessmentsInProgress');

    const sharedEvidenceStoreState = this.flux.stores.SharedEvidenceStore.getState();
    selectedOrganization = selectedOrganization
      || sharedEvidenceStoreState.selectedOrganization;
    const selectedOrganizationUuid = selectedOrganization.uuid;
    selectedAttachments = selectedAttachments
      || sharedEvidenceStoreState.selectedSharedAttachments[selectedOrganizationUuid];
    selectedOpenAssessmentTypes = selectedOpenAssessmentTypes
      || sharedEvidenceStoreState.selectedOpenAssessmentTypes;

    // make post body
    const post = [];

    selectedOpenAssessmentTypes.forEach(assessment => {
      const compatibleAssessments = selectedAttachments.filter(a =>
        a.attachment.usable_for_assessments.find(usableFor =>
          usableFor.uuid === assessment.uuid
        )
      );

      if (compatibleAssessments && compatibleAssessments.length) {
        const item = {
          share_attachment_list_uuids: compatibleAssessments.map(a => a.uuid),
          assessment_type_uuid: assessment.uuid,
        };
        post.push(item);
      }
    });

    if (post.length) {
      sharedEvidenceDAL.copySharedAttachmentsToAssessments(organizationUuid, selectedOrganizationUuid, post)
        .then(() => {
          selectedOrganization.state = _t('In progress');
          this.dispatch('copySharedEvidenceToAssessmentsSuccess');
        })
        .catch(() => { this.dispatch('copySharedEvidenceToAssessmentsFailure'); });
    } else {
      this.dispatch('copySharedEvidenceToAssessmentsSuccess');
    }
  },
};

export default SharedEvidenceActions;
