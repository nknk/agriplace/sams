import RbacDAL from '../dal/rbacDAL.js';
const rbacDAL = new RbacDAL(window.jsContext.API_URL);

const RbacActions = {

  loadData() {
    rbacDAL.get().then(
      (payload) => {
        this.dispatch('downloadRoles', payload);
        this.dispatch('currentRoleChanged', _.min(payload.roles.map((role) => role.id)));
      }
    );
  },

  // payload = {key: value}
  updateFilter(payload) {
    this.dispatch('filterChanged', payload);
  },

  // payload = {key: value}
  updateFilterByPrivilege(payload) {
    this.dispatch('filterByPrivilegeChanged', payload);
  },

  // payload = role_id
  setCurrentRole(payload) {
    this.dispatch('currentRoleChanged', payload);
  },

  updatePermissions(payload) {
    this.dispatch('permissionsUpdated', payload);
    rbacDAL.save(payload).then(
      () => {
        this.dispatch('saveSuccess');
      },

      (error) => {
        this.dispatch('saveFail', error);
        this.flux.actions.rbac.loadData();
      }
    );
  },
};

export default RbacActions;
