import ReviewDAL from '../dal/reviewDAL.js';
import DashboardDAL from '../dal/dashboardDAL.js';
const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid: organizationUuid, membershipUuid } = pageData.context;

const reviewDAL = new ReviewDAL(apiUrl);
const dashboardDAL = new DashboardDAL({ apiUrl, organizationUuid, membershipUuid });
const reviewActions = {

  loadReview(assessmentUuid) {
    reviewDAL.get(assessmentUuid).then((response) => {
      this.dispatch('loadReview', response);
    });
  },

  changeInternalDescription({ workflowUuid, text }) {
    reviewDAL.changeInternalDescription({ workflowUuid, text });
  },

  changeOverviewDescription({ workflowUuid, text }) {
    reviewDAL.changeOverviewDescription({ workflowUuid, text });
  },

  changeDate({ workflowUuid, date }) {
    this.dispatch('auditDateChanged', date);
    if (date) {
      dashboardDAL.changeDate({ workflowUuid, date });
    }
  },

  changeAuditStartDate({ workflowUuid, date }) {
    this.dispatch('auditStartDateChanged', date);
    if (date) {
      dashboardDAL.changeAuditStartDate({ workflowUuid, date });
    }
  },

  changeAuditEndDate({ workflowUuid, date }) {
    this.dispatch('auditEndDateChanged', date);
    if (date) {
      dashboardDAL.changeAuditEndDate({ workflowUuid, date });
    }
  },

  changeDecision({ workflowUuid, decision }) {
    this.dispatch('decisionChanged', decision);
    reviewDAL.changeDecision({ workflowUuid, decision });
  },

  changeReaudit({ workflowUuid, reaudit }) {
    this.dispatch('reauditChanged', reaudit);
    reviewDAL.changeReaudit({ workflowUuid, reaudit });
  },

  onJudgementChange({ assessmentUuid, judgement }) {
    this.dispatch('judgementChanged', judgement);
    this.flux.actions.judgements.update({ assessmentUuid, judgement, judgementUuid: judgement.uuid });
  },
};

export default reviewActions;
