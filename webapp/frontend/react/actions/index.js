import organizationsActions from './organizationsActions.js';
import AgriformActions from './AgriformActions.js';
import AssessmentsActions from './AssessmentsActions.js';
import AttachmentsActions from './AttachmentsActions.js';
import AttachmentLinksActions from './AttachmentLinksActions.js';
import productsActions from './productsActions.js';
import ProductLinksActions from './ProductLinksActions.js';
import HelpLinksActions from './HelpLinksActions.js';
import PlotsActions from './PlotsActions.js';
import RbacActions from './RbacActions.js';
import ReviewActions from './ReviewActions.js';
import JudgementsActions from './JudgementsActions.js';
import DashboardActions from './DashboardActions.js';
import FormActions from './FormActions.js';
import OfflineNotificationActions from './OfflineNotificationActions.js';
import CertificatesActions from './CertificatesActions.js';
import DocTypeReuseActions from './DocTypeReuseActions.js';
import ShareEvidenceActions from './ShareEvidenceActions.js';
import SharedEvidenceActions from './SharedEvidenceActions.js';

export {
  organizationsActions as organizations,
  AgriformActions as agriform,
  AssessmentsActions as assessments,
  AttachmentsActions as attachments,
  AttachmentLinksActions as attachmentLinks,
  productsActions as products,
  ProductLinksActions as productLinks,
  HelpLinksActions as helpLinks,
  PlotsActions as plots,
  RbacActions as rbacRoles,
  ReviewActions as review,
  JudgementsActions as judgements,
  DashboardActions as dashboard,
  FormActions as form,
  OfflineNotificationActions as offlineNotification,
  CertificatesActions as certificates,
  DocTypeReuseActions as docTypeReuse,
  ShareEvidenceActions as shareEvidence,
  SharedEvidenceActions as sharedEvidence,
};
