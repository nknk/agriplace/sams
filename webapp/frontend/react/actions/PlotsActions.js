import PlotsDAL from '../dal/plotsDAL.js';

const plotsDAL = new PlotsDAL('/internal-api');
const helpLinkActions = {

  loadPlots(organizationUuid) {
    plotsDAL.get(organizationUuid).then((response) => {
      this.dispatch('loadPlots', response);
    }, (response) => {
      console.error('error :', response);
    });
  },

  loadRvo(organizationUuid, kvk) {
    this.dispatch('loadRvoInProgress', { isLoading: true });
    this.dispatch('updateKvk', kvk);

    plotsDAL.sendKvk(organizationUuid, kvk).then((responseSuccess) => {
      this.dispatch('loadRvoSuccess', responseSuccess);
      this.dispatch('loadRvoInProgress', { isLoading: false });
      if (responseSuccess.success.plots_fetched > 0) {
        this.flux.actions.plots.loadPlots(organizationUuid);
      }
    }, (responseFail) => {
      if (responseFail) {
        this.dispatch('loadRvoFail', responseFail);
        this.dispatch('loadRvoInProgress', { isLoading: false });
      }
    });
  },
};

module.exports = helpLinkActions;
