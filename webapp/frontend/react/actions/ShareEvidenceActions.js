const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid } = pageData.context;
const organizationUuid = uuid;

import PartnerOrganizationsDAL from '../dal/partnerOrganizationsDAL.js';
const partnerOrganizationsDAL = new PartnerOrganizationsDAL(apiUrl);

import ShareEvidenceDAL from '../dal/shareEvidenceDAL.js';
const shareEvidenceDAL = new ShareEvidenceDAL(apiUrl);

const ShareEvidenceActions = {

  getPartnerOrganizationsSearch(organizationSearchText) {
    const shareEvidenceStoreState = this.flux.stores.ShareEvidenceStore.getState();

    const searchedPartnerOrganizations = shareEvidenceStoreState.searchedPartnerOrganizations;

    if (searchedPartnerOrganizations[organizationSearchText] === undefined) {
      this.dispatch('partnerOrganizationsFetching');
      const pageNumber = 1;
      const pageSize = shareEvidenceStoreState.pageSize;
      partnerOrganizationsDAL.getPartnerOrganizations(organizationUuid, pageNumber,
        pageSize, organizationSearchText).then((response) => {
          this.dispatch('partnerOrganizationsFetched', { response, pageNumber, organizationSearchText });
        });
    } else {
      this.dispatch('updateorganizationSearchText', { organizationSearchText });
    }
  },

  getPartnerOrganizationsPage(pageNumber) {
    const shareEvidenceStoreState = this.flux.stores.ShareEvidenceStore.getState();
    if (pageNumber > shareEvidenceStoreState.pageNumber) {
      this.dispatch('partnerOrganizationsFetching');
      const pageSize = shareEvidenceStoreState.pageSize;
      const organizationSearchText = shareEvidenceStoreState.organizationSearchText;
      partnerOrganizationsDAL.getPartnerOrganizations(organizationUuid, pageNumber,
        pageSize, organizationSearchText).then((response) => {
          this.dispatch('partnerOrganizationsFetched', { response, pageNumber, organizationSearchText });
        });
    }
  },

  loadMorePartnerOrganizations(pageNumber) {
    this.dispatch('loadingMorePartnerOrganizations');
    const shareEvidenceStoreState = this.flux.stores.ShareEvidenceStore.getState();
    const pageSize = shareEvidenceStoreState.pageSize;
    const organizationSearchText = shareEvidenceStoreState.organizationSearchText;
    partnerOrganizationsDAL.getPartnerOrganizations(organizationUuid, pageNumber,
      pageSize, organizationSearchText).then((response) => {
        this.dispatch('partnerOrganizationsFetched', { response, pageNumber, organizationSearchText });
      });
  },

  updateSelectedAttachments(selectedAttachments) {
    this.dispatch('updateSelectedAttachments', selectedAttachments);
  },

  updateSelectedOrganizations(selectedOrganizations) {
    this.dispatch('updateSelectedOrganizations', selectedOrganizations);
  },

  updateSearchOrganizationText(organizationSearchText) {
    this.dispatch('updateSearchOrganizationText', organizationSearchText);
  },

  startSharing() {
    this.dispatch('startSharingEvidence');
  },

  stopSharing() {
    this.dispatch('stopSharingEvidence');
  },

  saveEvidence() {
    this.dispatch('saveSharingEvidence');
  },

  shareConfirmation() {
    this.dispatch('confirmationSharingEvidence');
    const shareEvidenceStoreState = this.flux.stores.ShareEvidenceStore.getState();
    const selectedAttachments = shareEvidenceStoreState.selectedAttachments;
    const selectedOrganizations = shareEvidenceStoreState.selectedOrganizations;

    const attachmentUuids = selectedAttachments.map(a => a.uuid);
    const organizationMembershipUuids = selectedOrganizations.map(a =>
      a.membership_uuid
    );

    shareEvidenceDAL.shareEvidence(organizationUuid, attachmentUuids,
      organizationMembershipUuids)
      .then((response) => {
        this.dispatch('evidenceShared', true);
      })
      .catch((response) => {
        this.dispatch('evidenceShared', false);
      });

    // then dispatch evidenceShared
  },

  getAlreadySharedOrganizations(selectedAttachments) {
    this.dispatch('fetchingAlreadySharedOrganizations');
    const attachmentUuids = selectedAttachments.map(a => a.uuid);
    shareEvidenceDAL.getOrganizations(organizationUuid, attachmentUuids)
      .then((response) => {
        this.dispatch('alreadySharedOrganizationsFetched', response);
      });
  },

};

export default ShareEvidenceActions;
