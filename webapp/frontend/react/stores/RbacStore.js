const RbacStore = Fluxxor.createStore({

  initialize() {
    this.states = [];
    this.roles = [];
    this.privileges = [];
    this.grants = [];
    this.dataobjects = [];
    this.rbacErrors = undefined;
    this.selectedRoleId = undefined;
    this.selectedFilters = {
      role: {},
      dataobjectName: {},
      dataobjectState: {},
      privilege: {},
      privilegeAssigned: false,
    };

    // Actions we are listening for
    this.bindActions(
      'downloadRoles', this.onDownloadRoles,
      'filterChanged', this.onFilterChanged,
      'currentRoleChanged', this.onCurrentRoleChanged,
      'permissionsUpdated', this.onPermissionsUpdated,
      'saveSuccess', this.onSaveSuccess,
      'saveFail', this.onSaveFail
    );
  },

  //
  // Get store state
  //

  getState() {
    return {
      rbacErrors: this.rbacErrors,
    };
  },

  applyFilters(collection, filterObject) {
    const that = this;
    let result = collection;
    _.forEach(filterObject, (value, key) => {
      result = that.filterByValue(result, key, value);
    });
    return result;
  },

  filterByValue(collection, key, value) {
    const that = this;
    return _.filter(collection, (o) =>
      _.includes(that.resolveKey(o, key).toLowerCase(), value.toLowerCase())
    );
  },

  resolveKey(obj, key) {
    return (typeof key === 'function') ? key(obj) : _.get(obj, key);
  },

  applyFilterByPrivilege(collection, privilege) {
    const that = this;
    return _.filter(collection, (dataobject) =>
      _.some(that.getGrants(), {
        from_role: +that.selectedRoleId,
        to_role: +privilege.id,
        assignment: {
          state: dataobject.state.name,
          data_object: dataobject.name,
        },
      })
    );
  },

  applyAssignedOnlyFilter(collection) {
    const that = this;
    return _.filter(collection, (dataobject) =>
      _.some(that.getGrants(), {
        from_role: +that.selectedRoleId,
        assignment: {
          state: dataobject.state.name,
          data_object: dataobject.name,
        },
      })
    );
  },

  getRoles() {
    return this.applyFilters(this.roles, this.selectedFilters.role);
  },

  getDataObjects() {
    let result;
    result = this.applyFilters(this.dataobjects, this.selectedFilters.dataobjectName);
    if (this.selectedFilters.privilegeAssigned) {
      result = this.applyAssignedOnlyFilter(result);
    }
    result = this.applyFilters(result, this.selectedFilters.dataobjectState);

    const privilege = this.selectedFilters.privilege;

    if (_.isEqual(privilege, {}) || +privilege.privilegeId < 1) {
      return result;
    }

    const privilegeObject = _.find(this.privileges, { id: +privilege.privilegeId });
    return this.applyFilterByPrivilege(result, privilegeObject);
  },

  getGrants() {
    return this.grants;
  },

  getPrivileges() {
    return this.privileges;
  },

  getCurrentRole() {
    const role = _.find(this.roles, { id: this.selectedRoleId }) || {};
    return role || {};
  },

  onDownloadRoles(payload) {
    this.roles = payload.roles;
    this.privileges = payload.privileges;
    this.grants = payload.grants;
    this.dataobjects = payload.dataobjects;
    this.states = _.uniq(this.dataobjects.map((dataobject) => dataobject.state), 'name');
    this.emit('change');
  },

  onFilterChanged(payload) {
    const that = this;
    _.forEach(payload, (value, key) => {
      that.selectedFilters[key] = value;
    });
    this.emit('change');
  },

  onCurrentRoleChanged(payload) {
    this.selectedRoleId = payload;
    this.emit('change');
  },

  onPermissionsUpdated(payload) {
    const grantObject = {
      from_role: this.selectedRoleId,
      to_role: parseInt(payload.privilegeId, 10),
      assignment: payload.assignment,
    };
    if (payload.privilegeState) {
      this.grants.push(grantObject);
    } else {
      _.remove(this.grants, grantObject);
    }
    this.emit('change');
  },

  onSaveSuccess() {
    this.rbacErrors = undefined;
    this.emit('change');
  },

  onSaveFail(payload) {
    this.rbacErrors = payload.errors.map((error) => error.msg);
    this.emit('change');
  },

});

module.exports = RbacStore;
