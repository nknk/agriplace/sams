

var HelpLinksStore = Fluxxor.createStore({

  initialize() {
    this.helpLinks = [];

    // Actions we are listening for
    this.bindActions(
      'loadHelpLinks', this.onLoadHelpLinks
    );
  },

  //
  // Get store state
  //
  getState() {
    return { helpLinks: this.helpLinks };
  },


  onLoadHelpLinks(payload) {
    this.helpLinks = payload;

    this.emit('change');
  },
});

module.exports = HelpLinksStore;
