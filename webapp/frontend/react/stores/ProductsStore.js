

const moment = require('moment');
const aptools = require('../../lib/ap-tools.js');

const ProductsStore = Fluxxor.createStore({

  initialize() {
    this.years = aptools.getProdutYears(2015);
    this.selectedYear = +moment().format('YYYY');
    this.isProductsLoaded = false;
    this.products = [];
    this.productTypes = [];

    this.bindActions(
        'yearChanged', this.onYearChanged,
        'productTypesUpdated', this.onProductTypesUpdated,
        'productsUpdated', this.onProductsUpdated,
        'productAdded', this.onProductAdded,
        'productUpdated', this.onProductUpdated,
        'productDeleted', this.onProductDeleted
    );
  },

  makeYears(start = 2015) {
    const years = [];
    const currentYear = +moment().format('YYYY');
    const endYear = currentYear + 1;

    for (let year = 2015; year <= endYear; year++) {
      years.push(year);
    }
    return years;
  },

  getState() {
    return {
      isProductsLoaded: this.isProductsLoaded,
      productTypes: this.productTypes,
      products: this.products,
      years: this.years,
      selectedYear: this.selectedYear,
    };
  },

  onYearChanged({ year }) {
    this.selectedYear = year;

    this.emit('change');
  },

  getProductsByYear() {
    const products = _.clone(this.products.filter(product => product.harvest_year === this.selectedYear));
    return _.sortByOrder(products, ['name'], ['asc']);
  },


  emitChange(eventName) {
    this.emit('change');
    if (eventName) {
      this.emit(eventName);
    }
  },


  onProductTypesUpdated(payload) {
    this.productTypes = payload.productTypes;

    this.emitChange('productTypesUpdated');
  },


  onProductsUpdated({ products }) {
    this.isProductsLoaded = true;
    this.products = products;

    this.emitChange('productsUpdated');
  },


  onProductAdded({ product }) {
    this.products.push(product);

    this.emitChange('productAdded');
  },

  onProductDeleted({ uuid }) {
    this.products = this.products.filter(product => product.uuid !== uuid);

    this.emit('change');
  },

  onProductUpdated({ productUuid, product }) {
    const index = _.indexOf(this.products, _.find(this.products, { uuid: productUuid }));
    this.products.splice(index, 1, product);

    this.emit('change');
  },
});

module.exports = ProductsStore;
