
const SharedEvidenceStore = Fluxxor.createStore({

  initialize() {
    // Organizations which have shared evidence with my organization
    this.sharingOrganizations = [];
    this.sortOrderSharingOrganizations = 'inverse';
    this.selectedColumnSharingOrganizations = 'sharing_date';
    this.selectedOrganization = null;
    this.isConfirmationModalVisible = false;

    this.sharedAttachments = {};
    this.sortOrderSharedAttachments = 'inverse';
    this.selectedColumnSharedAttachments = 'modified_time';

    this.selectedSharedAttachments = {};

    this.copyToArchive = true;
    this.copySharedEvidenceToArchiveInProgress = false;

    this.openAssessmentTypes = [];
    this.selectedOpenAssessmentTypes = [];
    this.copySharedEvidenceToAssessmentsInProgress = false;
    this.openAssessmentTypesFetched = false;

    this.bindActions(
      'sharingOrganizationsFetched', this.onSharingOrganizationsFetched,
      'sortSharingOrganizations', this.onSortSharingOrganizations,
      'sharingOrganizationSelected', this.onSharingOrganizationSelected,
      'listOfAttachmentsFetched', this.onListOfAttachmentsFetched,
      'selectSharedAttachment', this.onSelectSharedAttachment,
      'selectAllSharedAttachments', this.onSelectAllSharedAttachments,
      'sortSharedAttachments', this.onSortSharedAttachments,
      'toggleCopyToArchive', this.onToggleCopyToArchive,
      'copySharedEvidenceToArchiveInProgress', this.onCopySharedEvidenceToArchiveInProgress,
      'copySharedAttachmentsToArchiveSuccess', this.onCopySharedAttachmentsToArchiveSuccess,
      'copySharedAttachmentsToArchiveFailure', this.onCopySharedAttachmentsToArchiveFailure,
      'copySharedEvidenceCancelled', this.onCopySharedEvidenceCancelled,
      'openSharedEvidenceConfirmationModal', this.onOpenSharedEvidenceConfirmationModal,
      'openAssessmentTypesFetched', this.onOpenAssessmentTypesFetched,
      'openAssessmentTypeSelected', this.onOpenAssessmentTypeSelected,
      'copySharedEvidenceToAssessmentsInProgress', this.onCopySharedEvidenceToAssessmentsInProgress,
      'copySharedEvidenceToAssessmentsSuccess', this.onCopySharedEvidenceToAssessmentsSuccess,
      'copySharedEvidenceToAssessmentsFailure', this.onCopySharedEvidenceToAssessmentsFailure
    );
  },

  getState() {
    return {
      sharingOrganizations: this.sharingOrganizations,
      sortOrderSharingOrganizations: this.sortOrderSharingOrganizations,
      selectedColumnSharingOrganizations: this.selectedColumnSharingOrganizations,
      selectedOrganization: this.selectedOrganization,
      isConfirmationModalVisible: this.isConfirmationModalVisible,
      sharedAttachments: this.sharedAttachments,
      sortOrderSharedAttachments: this.sortOrderSharedAttachments,
      selectedColumnSharedAttachments: this.selectedColumnSharedAttachments,
      selectedSharedAttachments: this.selectedSharedAttachments,
      copyToArchive: this.copyToArchive,
      openAssessmentTypes: this.openAssessmentTypes,
      selectedOpenAssessmentTypes: this.selectedOpenAssessmentTypes,
      openAssessmentTypesFetched: this.openAssessmentTypesFetched,
    };
  },

  onSharingOrganizationsFetched(organizations) {
    this.sharingOrganizations = organizations;
    this.onSortSharingOrganizations({
      column: this.selectedColumnSharingOrganizations,
    });
  },

  onSortSharingOrganizations({ column }) {
    this.sortOrderSharingOrganizations = this.sortOrderSharingOrganizations === 'normal' ? 'inverse' : 'normal';
    this.selectedColumnSharingOrganizations = column;

    const requiredSortOrder = this.sortOrderSharingOrganizations === 'inverse' ? [true] : [false];

    this.sharingOrganizations = _.sortByOrder(
      this.sharingOrganizations, (obj) => {
        let key;
        if (column === 'organization_name') {
          key = obj.sharing_organization.name;
        } else {
          key = _.get(obj, column, '');
        }
        return (typeof key === 'string') ? key.toLowerCase() : key;
      }, requiredSortOrder
    );
    this.emit('change');
  },

  onSharingOrganizationSelected(selectedOrganizationUuid) {
    const selectedOrganization = this.sharingOrganizations.find(org =>
      org.uuid === selectedOrganizationUuid
    );
    this.selectedOrganization = selectedOrganization;
    this.copyToArchive = true;
    this.selectedOpenAssessmentTypes = this.openAssessmentTypes;
    this.emit('change');
  },

  onListOfAttachmentsFetched({ selectedOrganizationUuid, response }) {
    this.sharedAttachments[selectedOrganizationUuid] = response;
    this.selectedSharedAttachments[selectedOrganizationUuid] = response;
    this.onSortSharedAttachments({
      column: this.selectedColumnSharedAttachments,
    });
  },

  onSelectSharedAttachment({ isSelected, attachmentUuid }) {
    const selectedOrganizationUuid = this.selectedOrganization.uuid;
    const sharedAttachments = this.sharedAttachments[selectedOrganizationUuid];
    const selectedAttachment = sharedAttachments.find(a => a.uuid === attachmentUuid);
    let selectedSharedAttachments = this.selectedSharedAttachments[selectedOrganizationUuid] || [];

    if (isSelected) {
      selectedSharedAttachments.push(selectedAttachment);
    } else {
      selectedSharedAttachments = selectedSharedAttachments.filter(a =>
        a.uuid !== selectedAttachment.uuid
      );
    }

    this.selectedSharedAttachments[selectedOrganizationUuid] = selectedSharedAttachments;
    this.emit('change');
  },

  onSelectAllSharedAttachments(isSelected) {
    const selectedOrganizationUuid = this.selectedOrganization.uuid;
    const sharedAttachments = this.sharedAttachments[selectedOrganizationUuid];
    let selectedSharedAttachments = [];
    if (isSelected) {
      selectedSharedAttachments = sharedAttachments;
    }
    this.selectedSharedAttachments[selectedOrganizationUuid] = selectedSharedAttachments;
    this.emit('change');
  },

  onSortSharedAttachments({ column }) {
    this.sortOrderSharedAttachments = this.sortOrderSharedAttachments === 'normal' ? 'inverse' : 'normal';
    this.selectedColumnSharedAttachments = column;

    const requiredSortOrder = this.sortOrderSharedAttachments === 'inverse' ? [true] : [false];

    const selectedOrganizationUuid = this.selectedOrganization.uuid;
    let sharedAttachments = this.sharedAttachments[selectedOrganizationUuid];

    sharedAttachments = _.sortByOrder(
      sharedAttachments, (obj) => {
        const attachment = obj.attachment;
        let key = _.get(attachment, column, '');
        if ($.isArray(key)) {
          key = key.reduce((initialValue, nextValue) => (
            initialValue + (nextValue.name ? `${nextValue.name} ` : '')
          ), '');
        }
        return (typeof key === 'string') ? key.toLowerCase() : key;
      }, requiredSortOrder
    );
    this.sharedAttachments[selectedOrganizationUuid] = sharedAttachments;
    this.emit('change');
  },

  onToggleCopyToArchive() {
    this.copyToArchive = !this.copyToArchive;
    this.emit('change');
  },

  onCopySharedEvidenceToArchiveInProgress() {
    this.copySharedEvidenceToArchiveInProgress = true;
  },

  onCopySharedAttachmentsToArchiveSuccess() {
    this.copySharedEvidenceToArchiveInProgress = false;
    this.emit('change');
  },

  onCopySharedAttachmentsToArchiveFailure() {
    this.copySharedEvidenceToArchiveInProgress = false;
  },

  onCopySharedEvidenceCancelled() {
    // reset store to defaults
    // this.sharingOrganizations = [];
    this.sortOrderSharingOrganizations = 'inverse';
    this.selectedColumnSharingOrganizations = 'sharing_date';
    this.selectedOrganization = null;
    this.isConfirmationModalVisible = false;
    // this.sharedAttachments = {};
    this.sortOrderSharedAttachments = 'inverse';
    this.selectedColumnSharedAttachments = 'modified_time';
    // this.selectedSharedAttachments = {};
    this.copyToArchive = true;
    this.copySharedEvidenceToArchiveInProgress = false;
    this.copySharedEvidenceToArchiveInProgress = false;
    this.emit('change');
  },

  onOpenSharedEvidenceConfirmationModal() {
    this.isConfirmationModalVisible = true;
    this.emit('change');
  },

  onOpenAssessmentTypesFetched({ openAssessmentTypes }) {
    this.openAssessmentTypes = openAssessmentTypes;
    this.selectedOpenAssessmentTypes = openAssessmentTypes;
    this.openAssessmentTypesFetched = true;
    this.emit('change');
  },

  onOpenAssessmentTypeSelected({ isSelected, assessmentTypeUuid }) {
    const assessment = this.openAssessmentTypes.find(a => a.uuid === assessmentTypeUuid);
    if (isSelected) {
      this.selectedOpenAssessmentTypes.push(assessment);
    } else {
      this.selectedOpenAssessmentTypes = this.selectedOpenAssessmentTypes.filter(
        a => a.uuid !== assessmentTypeUuid
      );
    }
    this.emit('change');
  },

  onCopySharedEvidenceToAssessmentsInProgress() {
    this.copySharedEvidenceToArchiveInProgress = true;
  },

  onCopySharedEvidenceToAssessmentsSuccess() {
    this.copySharedEvidenceToArchiveInProgress = false;
    this.emit('change');
  },

  onCopySharedEvidenceToAssessmentsFailure() {
    this.copySharedEvidenceToArchiveInProgress = false;
  },

});

module.exports = SharedEvidenceStore;
