
const CertificatesStore = Fluxxor.createStore({

  initialize() {
    // list of certificates of this user
    this.certificateList = {};

    // dictionary of certificates of this user
    // it may not contain all the certificates of user
    // only those which have been edited or selected
    this.certificates = {};

    // dictionary of certificates that are fetched
    this.isFetched = {};

    // dictionary of certificate types
    // used for displaying and fetching certifiicate types in the form
    this.certificateTypes = {};

    // if certificate is being created
    this.isCreating = false;

    /* certificate which is being edited or created */
    this.selectedCertificate = null;

    this.createOrUpdateCertificateInProgress = false;
    /* ** */

    // Actions we are listening for
    this.bindActions(
      'updateCertificatesList', this.onUpdateCertificatesList,
      'createCertificateStarted', this.onCreateCertificateStarted,
      'createCertificate', this.onCreateCertificate,
      'updateCertificate', this.onUpdateCertificate,
      'certificateTypesUpdated', this.onCertificateTypesUpdated,
      'certificateDeleted', this.onCertificateDeleted,
      'getCertificate', this.onGetCertificate,
      'resetSelectedCertificate', this.onResetSelectedCertificate,
      'createOrUpdateCertificateInProgress', this.onCreateOrUpdateCertificateInProgress
    );
  },

  //
  // Get store state
  //
  getState() {
    return {
      certificateList: this.certificateList,
      certificates: this.certificates,
      isFetched: this.isFetched,
      certificateTypes: this.certificateTypes,
      isCreating: this.isCreating,
      selectedCertificate: this.selectedCertificate,
      isLoadingSelectedCertificate: this.isLoadingSelectedCertificate,
      createOrUpdateCertificateInProgress: this.createOrUpdateCertificateInProgress,
    };
  },

  onUpdateCertificatesList({ organizationUuid, response }) {
    this.certificateList[organizationUuid] = response;
    this.isFetched[organizationUuid] = true;
    this.emit('change');
  },

  onResetSelectedCertificate() {
    this.selectedCertificate = null;
    this.createOrUpdateCertificateInProgress = false;
    this.emit('change');
  },

  onGetCertificate({ organizationUuid, response }) {
    this.certificates[organizationUuid] = this.certificates[organizationUuid] || {};
    this.certificates[organizationUuid][response.uuid] = this.certificates[organizationUuid][response.uuid]
      || _.cloneDeep(response);
    this.selectedCertificate = _.cloneDeep(response);
    this.emit('change');
  },

  onCertificateDeleted({ organizationUuid, certificateUuid }) {
    this.certificateList[organizationUuid] = this.certificateList[organizationUuid].filter((certificate) => (
      certificate.uuid !== certificateUuid
    ));
    this.emit('change');
  },

  onCreateCertificateStarted() {
    this.isCreating = true;
    this.emit('change');
  },

  onCreateCertificate({ organizationUuid, certificateCreated }) {
    this.certificateList[organizationUuid] = this.certificateList[organizationUuid] || [];
    this.certificateList[organizationUuid].push(certificateCreated);
    this.isCreating = false;
    this.emit('change');
  },

  onUpdateCertificate({ organizationUuid, certificateCreated, certificateUuid }) {
    this.certificateList[organizationUuid] = this.certificateList[organizationUuid] || [];
    this.certificateList[organizationUuid] = this.certificateList[organizationUuid]
      .filter(c => c.uuid !== certificateUuid);
    this.certificateList[organizationUuid].push(certificateCreated);
    // remove updated certificate individual list
    this.certificates[organizationUuid] = this.certificates[organizationUuid] || {};
    this.certificates[organizationUuid][certificateUuid] = undefined;
    this.emit('change');
  },

  onCertificateTypesUpdated({ organizationUuid, certificateTypes }) {
    this.certificateTypes[organizationUuid] = certificateTypes;
    this.emit('change');
  },

  onCreateOrUpdateCertificateInProgress() {
    this.createOrUpdateCertificateInProgress = true;
    this.emit('change');
  },

});

module.exports = CertificatesStore;
