var FormStore = Fluxxor.createStore({

  initialize() {
    this.schemas = {};
    this.forms = {};
    this.schemaSaveInProgress = false;
    this.errors = {};

    this.bindActions(
            'schemaChanged', this.onSchemaChanged,
            'formChanged', this.onFormChanged,
            'schemaSaveInProgress', this.onSchemaSaveInProgress,
            'formSaveInProgress', this.onFormSaveInProgress,
            'schemaIOError', this.onSchemaIOError,
            'formIOError', this.onFormIOError
        );
  },

  getState() {
    return {
      forms: this.forms,
      schemas: this.schemas,
      schemaSaveInProgress: this.schemaSaveInProgress,
      formSaveInProgress: this.formSaveInProgress,
      errors: this.errors,
    };
  },

  emitChange(eventName) {
    this.emit('change');
    if (eventName) {
      this.emit(eventName);
    }
  },

  onSchemaChanged(payload) {
    this.schemas[payload.id] = payload.formData;
    this.emitChange('schemaChanged');
  },

  onFormChanged(payload) {
    this.forms[payload.id] = payload.form;
    this.emit('change');
  },

  onSchemaSaveInProgress(payload) {
    if (payload.isLoading) {
      this.errors = {};
    }

    this.schemaSaveInProgress = payload.isLoading;
    this.emitChange();
  },

  onFormSaveInProgress(payload) {
    if (payload.isLoading) {
      this.errors = {};
    }

    this.schemaSaveInProgress = payload.isLoading;
    this.emitChange();
  },

  onSchemaIOError(errors) {
    this.errors = errors;
    this.emitChange('schemaIOError');
  },

  onFormIOError(errors) {
    this.errors = errors.map(e => e.message);
    this.emit('change');
  },

});

module.exports = FormStore;
