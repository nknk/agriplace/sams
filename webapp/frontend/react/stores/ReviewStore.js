import moment from 'moment';

const ReviewStore = Fluxxor.createStore({

  initialize() {
    this.review = {};
    this.errors = {
      audit_date_planned: null,
      audit_date_actual_start: null,
      audit_date_actual_end: null,
      has_errors: false,
    };

    this.bindActions(
      'loadReview', this.onLoadReview,
      'auditDateChanged', this.onAuditDateChanged,
      'auditStartDateChanged', this.onAuditStartDateChanged,
      'auditEndDateChanged', this.onAuditEndDateChanged,
      'reauditChanged', this.onReauditChanged,
      'decisionChanged', this.onDecisionChanged,
      'judgementChanged', this.onJudgementChanged
    );

    this.validate(this.review);
  },

  getState() {
    return {
      review: this.review,
      errors: this.errors,
    };
  },

  validate(review) {
    const dateFormat = 'YYYY-MM-DDTHH:mm:ss';

    // remove any validation checks for planned audit date
    this.errors.audit_date_planned = null;

    this.errors.audit_date_actual_start = !moment(review.audit_date_actual_start, dateFormat, true).isValid()
      ? 'Invalid date'
      : null;

    this.errors.audit_date_actual_end = !review.audit_date_actual_end || (
      review.audit_date_actual_end && !moment(review.audit_date_actual_end, dateFormat, true).isValid()
    ) ? 'Invalid date' : null;

    this.errors.end_before_start = !review.audit_date_actual_start && !review.audit_date_actual_end || (
      review.audit_date_actual_start && review.audit_date_actual_end
      && moment(review.audit_date_actual_end, dateFormat, true)
        .isBefore(review.audit_date_actual_start, dateFormat, true)
    );

    this.errors.has_errors = this.errors.audit_date_planned !== null
      || this.errors.audit_date_actual_start !== null
      || this.errors.audit_date_actual_end !== null
      || this.errors.end_before_start;
  },

  onLoadReview(review) {
    this.review = review;
    this.validate(this.review);
    this.emit('change');
  },

  onAuditDateChanged(date) {
    this.review.audit_date_planned = date;
    this.validate(this.review);
    this.emit('change');
  },

  onAuditStartDateChanged(date) {
    this.review.audit_date_actual_start = date;
    this.validate(this.review);
    this.emit('change');
  },

  onAuditEndDateChanged(date) {
    this.review.audit_date_actual_end = date;
    this.validate(this.review);
    this.emit('change');
  },

  onReauditChanged(reaudit) {
    this.review.audit_results = this.review.audit_results || {};
    this.review.audit_results.reaudit = reaudit;
    this.emit('change');
  },

  onDecisionChanged(decision) {
    this.review.audit_results = this.review.audit_results || {};
    this.review.audit_results.decision = decision;
    this.emit('change');
  },

  onJudgementChanged(judgement) {
    this.review.judgements = this.review.judgements || [];
    let existingJudgement = _.find(this.review.judgements, j => (j.uuid === judgement.uuid));

    if (existingJudgement) {
      existingJudgement = judgement;
    } else {
      this.review.judgements.push(judgement);
    }
    this.emit('change');
  },
});

module.exports = ReviewStore;
