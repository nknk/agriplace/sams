import AssessmentsDAL from '../dal/assessmentsDAL.js';
import AssessmentTypeModel from '../models/AssessmentTypeModel';
const AssessmentModel = require('../models/AssessmentModel');
const AttachmentsUtils = require('../components/Utils/attachments.js');
const pageData = require('../components/Utils/pageData.js');
const { apiUrl, uuid: organizationUuid, membershipUuid } = pageData.context;
import _ from 'lodash';

const AssessmentsStore = Fluxxor.createStore({

  initialize() {
    this.assessments = {};
    this.assessmentTypes = {};
    this.previousAssessments = {};
    this.documentTypesDictionary = {};
    this.isAssessmentSaving = false;

    this.dal = new AssessmentsDAL(apiUrl, organizationUuid, membershipUuid);

    this.evidenceAssociatedQuestionsDictionary = {};

    this.assessmentReports = {};

    this.answersInProgress = {};
    this.answersWithErrors = {};

    this.assessmentReuseFailed = false;
    this.assessmentReuseInProgress = false;
    this.assessmentReuseSuccess = false;

    this.assessmentReuseStatusInProgress = false;

    this.attachmentLinksUpdateCounter = 0;

    //
    // Actions we are listening for
    //
    this.bindActions(
      'assessmentLoaded', this.onAssessmentLoaded,
      'assesmentReloaded', this.onAssesmentReloaded,
      'assesmentReloadFailed', this.onAssesmentReloadFailed,
      'assessmentTypeLoaded', this.onAssessmentTypeLoaded,
      'previousAssessmentsLoaded', this.onPreviousAssessmentsLoaded,
      'assessmentStateLoaded', this.onAssessmentStateLoaded,
      'assessmentReportLoaded', this.onAssessmentReportLoaded,

      'saveAssessmentShares', this.onSaveAssessmentShares,
      'saveAssessmentSharesAndClose', this.onSaveAssessmentSharesAndClose,
      'closeAssessment', this.onCloseAssessment,

      // Attachments
      'createAttachmentLink', this.onCreateAttachmentLink,
      'deleteAttachmentLink', this.onDeleteAttachmentLink,
      'updateAttachmentLink', this.onUpdateAttachmentLink,

      'attachmentLinksUpdated', this.onAttachmentLinksUpdated,
      'assessmentReuseFailed', this.onAssessmentReuseFailed,
      'assessmentReuseInProgress', this.onAssessmentReuseInProgress,
      'assessmentReuseCompleted', this.onAssessmentReuseComplete,
      'closeAssessmentReuseSuccessModal', this.onCloseAssessmentReuseSuccessModal,
      'assessmentReuseStatusInProgress', this.onAssessmentReuseStatusInProgress,
      'assessmentReuseStatusFetched', this.onAssessmentReuseStatusFetched,
      'assessmentReuseStatusFailed', this.onAssessmentReuseStatusFailed,
      'answerEvidenceReuseChanged', this.onAnswerEvidenceReuseChanged,
      'signatureStateChanged', this.onSignatureStateChanged,
      'productsChangedFromTab', this.onProductsChangedFromTab,

      'createAnswerReturn', this.onCreateAnswerReturn,
      'createAnswersReturn', this.onCreateAnswersReturn,
      'createAnswerFailed', this.onCreateAnswerFailed,
      'createAnswersFailed', this.onCreateAnswersFailed,
      'updateAnswerReturn', this.onUpdateAnswerReturn,
      'updateAnswerFailed', this.onUpdateAnswerFailed,
      'deleteAnswerSuccess', this.onDeleteAnswerSuccess,
      'deleteAnswerFailed', this.onDeleteAnswerFailed,
      'markAnswerInProgress', this.onMarkAnswerInProgress,
      'markAnswersInProgress', this.onMarkAnswersInProgress
    );
  },

  //
  // Get entire store state
  //
  getState() {
    return {
      assessments: this.assessments,
      assessmentTypes: this.assessmentTypes,
      previousAssessments: this.previousAssessments,
      documentTypesDictionary: this.documentTypesDictionary,
      isAssessmentSaving: this.isAssessmentSaving,
      evidenceAssociatedQuestionsDictionary: this.evidenceAssociatedQuestionsDictionary,
      assessmentReports: this.assessmentReports,
      answersInProgress: this.answersInProgress,
      answersWithErrors: this.answersWithErrors,
      assessmentReuseFailed: this.assessmentReuseFailed,
      assessmentReuseInProgress: this.assessmentReuseInProgress,
      attachmentLinksUpdateCounter: this.attachmentLinksUpdateCounter,
      assessmentReuseSuccess: this.assessmentReuseSuccess,
      assessmentReuseStatusInProgress: this.assessmentReuseStatusInProgress,
    };
  },

  //
  // Get questionnaire by uuid
  //
  getQuestionnaire(assessmentUuid, questionnaireUuid) {
    const assessment = this.assessments[assessmentUuid];
    if (!(assessment && assessment.assessmentType)) {
      return undefined;
    }
    return assessment.assessmentType.questionnaires[questionnaireUuid];
  },

  onAssessmentLoaded(assessment, noEmit) {
    let assessmentModel = this.assessments[assessment.assessment.uuid];

    if (!assessmentModel) {
      assessmentModel = new AssessmentModel({
        assessment: assessment.assessment,
        uuid: assessment.assessment.uuid,
        answers: assessment.answers,
        permissions: assessment.permissions,
        overview: assessment.detail,
        products: assessment.products,
        document_type_reuse_attachments: assessment.document_type_reuse_attachments,
        attachmentLinks: AttachmentsUtils.initAssessmentAttachments(assessment.attachment_links),
        judgementMode: assessment.judgement_mode,
        isEvidenceReuseAnswered: this.dal.getEvidenceReuseAnswer(assessment.assessment.uuid)
          || !!assessment.assessment_reuse_status,
        documentTypeReuseAttachments: assessment.document_type_reuse_attachments,
        isReportVisible: assessment.is_report_visible,
        assessmentReuseStatus: assessment.assessment_reuse_status,
      });
      this.assessments[assessment.assessment.uuid] = assessmentModel;
    }

    if (!noEmit) {
      this.emit('change');
    }
  },

  onAssesmentReloaded(assessment) {
    this.assessments[assessment.assessment.uuid] = null;
    this.onAssessmentLoaded(assessment, true);
  },

  onAssesmentReloadFailed(assessmentUuid) {
    this.assessments[assessmentUuid] = null;
    this.emit('change');
  },

  onAssessmentTypeLoaded(payload) {
    const assessmentType = payload.assessmentType || {};
    const assessmentModel = this.assessments[payload.assessmentUuid];
    const assessmentTypeInstance = new AssessmentTypeModel();

    // we have dat on the assessment type, load it in the model
    assessmentTypeInstance.load(assessmentType);

    // save it into store for furher purposes
    this.assessmentTypes[assessmentType.uuid] = assessmentType;

    // put questions associated with evidences in collection
    this.updateAssociatedQuestions(assessmentType);

    // put document types in collection
    this.updateDocumentTypes(assessmentType);

    assessmentModel.setAssessmentType(assessmentTypeInstance);
    this.emit('change');
  },

  onPreviousAssessmentsLoaded(payload) {
    const previousAssessments = payload.previousAssessments;

    _.forEach(previousAssessments, (assessment) => {
      if (payload.assessmentUuid !== assessment.uuid) {
        let assessmentModel = this.previousAssessments[assessment.uuid];
        if (!assessmentModel) {
          assessmentModel = new AssessmentModel({
            assessment,
            uuid: assessment.uuid,
          });

          this.previousAssessments[assessment.uuid] = assessmentModel;
        }
      }
    });

    this.emit('change');
  },

  onAssessmentStateLoaded(payload) {
    const assessmentModel = this.assessments[payload.assessmentUuid];
    assessmentModel.questionnaireState[payload.questionnaireUuid] = {};
    assessmentModel.questionnaireState[payload.questionnaireUuid].isComplete = payload.assessmentState.isComplete;
    assessmentModel.questionnaireState[payload.questionnaireUuid].isSigned = payload.assessmentState.isSigned;
    assessmentModel.questionnaireState[payload.questionnaireUuid].isDone =
      assessmentModel.isQuestionnaireDone(payload.questionnaireUuid);

    this.emit('change');
  },

  onCreateAttachmentLink(attachmentLink) {
    const assessment = this.assessments[attachmentLink[0].assessment];
    const documentTypeUuid = attachmentLink[0].document_type_attachment;
    const currentAttachmentLinks = assessment.attachmentLinks[documentTypeUuid] || [];
    assessment.attachmentLinks[documentTypeUuid] = currentAttachmentLinks.concat(attachmentLink);
    this.attachmentLinksUpdateCounter = this.attachmentLinksUpdateCounter + 1;
    this.emit('change');
  },

  onUpdateAttachmentLink(attachmentLinkUpdated) {
    const assessment = this.assessments[attachmentLinkUpdated.assessment];
    const documentTypeUuid = attachmentLinkUpdated.document_type_attachment;
    const currentAttachmentLinks = assessment.attachmentLinks[documentTypeUuid];

    const agriformUuid = attachmentLinkUpdated.attachment.agriformUuid;
    if (agriformUuid) {
      const questionnaireUuid = _.first(_.keys(
        this.assessments[agriformUuid].assessmentType.questionnaires
      ));
      const questionnaireState = this.assessments[agriformUuid].questionnaireState[questionnaireUuid];
      attachmentLinkUpdated.is_done = questionnaireState.isDone;
    }

    const indexOfUpdatedAttachmentLink = _.findIndex(
      currentAttachmentLinks, (attachmentLink) => attachmentLink.id === attachmentLinkUpdated.id
    );
    currentAttachmentLinks.splice(indexOfUpdatedAttachmentLink, 1, attachmentLinkUpdated);
    this.attachmentLinksUpdateCounter = this.attachmentLinksUpdateCounter + 1;
    this.emit('change');
  },

  onDeleteAttachmentLink(attachmentLinkUpdated) {
    const assessment = this.assessments[attachmentLinkUpdated.assessment];
    const documentTypeUuid = attachmentLinkUpdated.document_type_attachment;
    const currentAttachmentLinks = assessment.attachmentLinks[documentTypeUuid];

    const indexOfUpdatedAttachmentLink = _.findIndex(
      currentAttachmentLinks, (attachmentLink) => attachmentLink.id === attachmentLinkUpdated.id
    );
    currentAttachmentLinks.splice(indexOfUpdatedAttachmentLink, 1);
    this.attachmentLinksUpdateCounter = this.attachmentLinksUpdateCounter + 1;
    this.emit('change');
  },

  // build the sorted document types list
  // extra params doc_types will contain :
  // questionnaire_order_index, question_order_index
  // question_level => to update UI of question level
  updateDocumentTypes(assessmentType) {
    let documentTypes = _.cloneDeep(assessmentType.document_types) || {};
    const associatedQuestions = this.evidenceAssociatedQuestionsDictionary[assessmentType.uuid];

    // add the needed params in doc types
    _.forEach(documentTypes, (documentType) => {
      // there is only one question level associated with one doc type
      const questionLevelUuid = documentType.level_object_dict && documentType.level_object_dict[assessmentType.uuid];

      // get question level for this document type
      if (questionLevelUuid) {
        _.some(assessmentType.questionnaires, (questionnaire) => {
          const questionLevel = questionnaire.question_levels && questionnaire.question_levels[questionLevelUuid];
          if (questionLevel) {
            documentType.question_level = questionLevel;
          }
          return questionLevel;
        });
      }

      const associatedQuestionsList = associatedQuestions && associatedQuestions[documentType.uuid];
      const highestAssociatedQuestion = associatedQuestionsList && associatedQuestionsList[0];
      if (highestAssociatedQuestion) {
        documentType.questionnaire_order_index = highestAssociatedQuestion.questionnaire_order_index;
        documentType.question_order_index = highestAssociatedQuestion.order_index;
      }
    });

    // desired ordering
    // First by questionnaire order index
    // Second by question order index
    // Third by alphabetical order of doc type name
    documentTypes = _.sortByOrder(documentTypes, [
      'questionnaire_order_index', 'question_order_index', 'name',
    ], ['asc', 'asc', 'asc']);

    this.documentTypesDictionary[assessmentType.uuid] = documentTypes;
  },

  updateAssociatedQuestions(assessmentType) {
    const evidenceAssociatedQuestions = {};

    // extract associated questions
    _.forEach(assessmentType.questionnaires, (questionnaire) => {
      _.forEach(questionnaire.elements, (element) => {
        const documentTypesAttachments = element.document_types_attachments || [];
        _.forEach(documentTypesAttachments, (documentTypesUuid) => {
          const question = _.cloneDeep(element);

          // get relevant questionnaire belonging to question
          const contextQuestionnaire = assessmentType.questionnaires[element.questionnaire];
          question.questionnaire_order_index = contextQuestionnaire.order_index;
          question.questionnaire_code = contextQuestionnaire.code;
          evidenceAssociatedQuestions[documentTypesUuid] = evidenceAssociatedQuestions[documentTypesUuid] || [];
          evidenceAssociatedQuestions[documentTypesUuid].push(question);
        });
      });
    });

    // sort associated questions
    _.forEach(Object.keys(evidenceAssociatedQuestions), (documentTypeUuid) => {
      evidenceAssociatedQuestions[documentTypeUuid] = _.sortByOrder(evidenceAssociatedQuestions[documentTypeUuid], [
        'questionnaire_order_index', 'order_index', 'code',
      ], ['asc', 'asc', 'asc']);
    });

    this.evidenceAssociatedQuestionsDictionary[assessmentType.uuid] = evidenceAssociatedQuestions;
  },

  onSignatureStateChanged(payload) {
    const assessment = this.assessments[payload.assessmentUuid];
    assessment.questionnaireState[payload.questionnaireUuid].isSigned = payload.isSigned;
    assessment.questionnaireState[payload.questionnaireUuid].isDone = assessment.isQuestionnaireDone(
      payload.questionnaireUuid
    );
    this.emit('change');
  },

  updateAssessmentProperty(assessmentUuid, key, value) {
    const assessmentModel = this.assessments[assessmentUuid];
    if (assessmentModel) {
      assessmentModel[key] = value;
      log.info(`AssessmentsStore::updateAssessmentProperty: ${key}: has been set to: ${value}`);
      return true;
    }

    let error = 'AssessmentsStore::updateAssessmentProperty: could not update';
    error = `${error} assessments[${assessmentUuid}].${key} to: ${value}`;
    log.error(error);
    return false;
  },

  //
  // data is an object containing:
  //
  //  assessmentUuid: assessmentUuid,
  //
  //  partners:
  //    [
  //       { partner: organizationUuid },
  //       { partner: organizationUuid },
  //       ...
  //    ]
  //
  onSaveAssessmentShares(data) {
    // name the collection 'shares' and send it as object to the DAL
    const promise = this.dal.genericPatch(data.assessmentUuid, { shares: data.partners });
    promise.then(
      (payload) => {
        log.info('AssessmentsStore::onSaveAssessmentShares: assessment shares have been saved');
        this.updateAssessmentProperty(data.assessmentUuid, 'shares', payload.shares);
        this.emit('shares-saving-success');
        this.emit('change');
      },

      (error) => {
        log.error('AssessmentsStore::onSaveAssessmentShares: an error occured, assessment shares were not saved!');
        this.emit('shares-saving-error');
      }
    );
  },

  //
  // data is an object containing:
  //
  //  assessmentUuid: assessmentUuid,
  //
  //  partners:
  //    [
  //       { partner: organizationUuid },
  //       { partner: organizationUuid },
  //       ...
  //    ]
  //
  onSaveAssessmentSharesAndClose(data) {
    // name the collection 'shares' and send it as object to the DAL
    const promise = this.dal.genericPatch(data.assessmentUuid, { shares: data.partners, state: 'closed' });
    promise.then(
      (payload) => {
        let info = 'AssessmentsStore::onSaveAssessmentSharesAndClose: assessment';
        info = `${info} shares have been saved and the assessment has been closed`;
        log.info(info);
        this.updateAssessmentProperty(data.assessmentUuid, 'shares', payload.shares);
        this.emit('shares-saving-success');
        this.emit('change');
      },

      (error) => {
        let errorText = 'AssessmentsStore::onSaveAssessmentSharesAndClose: an error occured,';
        errorText = `${errorText} assessment shares were not saved and the assessment was not closed!`;
        log.error(errorText);
        this.emit('shares-saving-error');
      }
    );
  },

  //
  // returns a collection of partners as described in onSaveAssessmentShares
  //
  getAssessmentShares(assessmentUuid) {
    const assessmentModel = this.assessments[assessmentUuid];
    if (assessmentModel && assessmentModel.assessment && assessmentModel.assessment.shares) {
      return assessmentModel.assessment.shares;
    }

    return [];
  },

  changeAssessmentState(assessmentUuid, state) {
    const promise = this.dal.genericPatch(assessmentUuid, { state });
    promise.then(
      (payload) => {
        log.info('AssessmentsStore::onChangeAssessmentState: assessment.state has been set to:', state);
        this.updateAssessmentProperty(assessmentUuid, 'state', state);
        this.emit('state-saving-success');
        this.emit('change');
      },

      (error) => {
        log.error('AssessmentsStore::onChangeAssessmentState: an error occured, assessment.state could not be saved!');
        this.emit('state-saving-error');
      }
    );
  },

  onCloseAssessment(data) {
    this.changeAssessmentState(data.assessmentUuid, 'closed');
  },

  onAnswerEvidenceReuseChanged({ assessmentUuid, val }) {
    this.assessments[assessmentUuid].isEvidenceReuseAnswered = val;
    this.emit('change');
  },

  onAttachmentLinksUpdated({ attachmentLinks, assessmentUuid }) {
    const assessment = this.assessments[assessmentUuid];
    const attLinksByDocType = {};
    attachmentLinks.forEach(x => {
      attLinksByDocType[x.document_type_attachment] = attLinksByDocType[x.document_type_attachment] || [];
      attLinksByDocType[x.document_type_attachment].push(x);
    });
    assessment.attachmentLinks = attLinksByDocType;
    this.attachmentLinksUpdateCounter = this.attachmentLinksUpdateCounter + 1;
    this.emit('change');
  },

  getPermissions(assessmentUuid) {
    const assessment = this.assessments[assessmentUuid] || {};
    return assessment.permissions || [];
  },

  onProductsChangedFromTab({ assessmentUuid, updatedProducts }) {
    this.assessments[assessmentUuid].products = updatedProducts;

    this.emit('change');
  },

  onAssessmentReportLoaded({ assessmentUuid, report }) {
    this.assessmentReports[assessmentUuid] = report;
    this.emit('change');
  },

  onMarkAnswerInProgress({ assessmentUuid, questionUuid }) {
    if (!this.answersInProgress[assessmentUuid]) {
      this.answersInProgress[assessmentUuid] = {};
    }

    this.answersInProgress[assessmentUuid][questionUuid] = true;
    this.emit('change');
  },

  onMarkAnswersInProgress({ assessmentUuid, answers }) {
    if (!this.answersInProgress[assessmentUuid]) {
      this.answersInProgress[assessmentUuid] = {};
    }
    _.each(answers, (answer) => {
      this.answersInProgress[assessmentUuid][answer.question] = true;
    });
    this.emit('change');
  },

  onCreateAnswerReturn({ assessmentUuid, questionUuid, answerReceived, answerSent }) {
    this.answersInProgress[assessmentUuid][questionUuid] = false;

    if (!this.answersWithErrors[assessmentUuid]) {
      this.answersWithErrors[assessmentUuid] = {};
    }

    if (answerReceived && answerSent) {
      this.answersWithErrors[assessmentUuid][questionUuid] = false;
      this.emit('answer-saving-success');
    } else {
      this.answersWithErrors[assessmentUuid][questionUuid] = true;
    }

    this.emit('change');
  },

  onCreateAnswersReturn({ assessmentUuid, answersReceived, answersSent }) {
    _.each(answersSent, (answerSent) => {
      const answerReceivedArray = _.filter(answersReceived, (a) => (
         a.question === answerSent.question
      ));
      const answerReceived = answerReceivedArray && answerReceivedArray[0];
      const questionUuid = answersSent.question || answerReceived.question;
      this.onCreateAnswerReturn({ assessmentUuid, questionUuid, answerReceived, answerSent });
    });
  },

  onCreateAnswerFailed({ assessmentUuid, questionUuid }) {
    this.answersInProgress[assessmentUuid][questionUuid] = false;

    if (!this.answersWithErrors[assessmentUuid]) {
      this.answersWithErrors[assessmentUuid] = {};
    }

    this.answersWithErrors[assessmentUuid][questionUuid] = true;
    this.emit('change');
  },

  onCreateAnswersFailed({ assessmentUuid, answers }) {
    _.each(answers, (answer) => {
      const questionUuid = answer.question;
      this.onCreateAnswerFailed({ assessmentUuid, questionUuid });
    });
  },

  onUpdateAnswerReturn({ assessmentUuid, questionUuid, answerReceived, answerSent }) {
    this.answersInProgress[assessmentUuid][questionUuid] = false;

    if (!this.answersWithErrors[assessmentUuid]) {
      this.answersWithErrors[assessmentUuid] = {};
    }

    if (answerReceived && answerSent) {
      this.answersWithErrors[assessmentUuid][questionUuid] = false;
      this.emit('answer-saving-success');
    } else {
      this.answersWithErrors[assessmentUuid][questionUuid] = true;
    }

    this.emit('change');
  },

  onUpdateAnswerFailed({ assessmentUuid, questionUuid }) {
    this.answersInProgress[assessmentUuid][questionUuid] = false;

    if (!this.answersWithErrors[assessmentUuid]) {
      this.answersWithErrors[assessmentUuid] = {};
    }

    this.answersWithErrors[assessmentUuid][questionUuid] = true;
    this.emit('change');
  },

  onDeleteAnswerSuccess({ assessmentUuid, questionUuid }) {
    this.answersInProgress[assessmentUuid][questionUuid] = false;
    this.emit('answer-saving-success');
    this.emit('change');
  },

  onDeleteAnswerFailed({ assessmentUuid, questionUuid }) {
    this.answersInProgress[assessmentUuid][questionUuid] = false;

    if (!this.answersWithErrors[assessmentUuid]) {
      this.answersWithErrors[assessmentUuid] = {};
    }

    this.answersWithErrors[assessmentUuid][questionUuid] = true;
    this.emit('change');
  },

  onAssessmentReuseFailed() {
    this.assessmentReuseInProgress = false;
    this.assessmentReuseFailed = true;
    this.emit('change');
  },

  onAssessmentReuseInProgress() {
    this.assessmentReuseInProgress = true;
    this.emit('change');
  },

  onAssessmentReuseComplete() {
    this.assessmentReuseInProgress = false;
    this.assessmentReuseSuccess = true;
    this.emit('change');
  },

  onCloseAssessmentReuseSuccessModal() {
    this.assessmentReuseSuccess = false;
    this.emit('change');
  },

  onAssessmentReuseStatusInProgress(inProgress) {
    this.assessmentReuseStatusInProgress = inProgress;
    this.emit('change');
  },

  onAssessmentReuseStatusFetched({ assessmentUuid, status }) {
    const assessment = this.assessments[assessmentUuid];
    assessment.assessmentReuseStatus = status;
    this.emit('change');
  },

  onAssessmentReuseStatusFailed() {
    this.assessmentReuseStatusInProgress = false;
    this.emit('change');
  },
});

module.exports = AssessmentsStore;
