
const ShareEvidenceStore = Fluxxor.createStore({

  initialize() {
    this.searchedPartnerOrganizations = {};

    // with whom attachments are already shared
    this.alreadySharedOrganizations = [];

    this.pageNumber = 0;
    this.pageSize = 10;

    // has more items in th elist to be fetched
    this.hasMorePartnerOrganizations = {};

    // loading fresh partners organization
    this.isLoadingPartnerOrganizations = true;

    // loading next items in list
    this.loadingMorePartnerOrganizations = false;

    // sharing in progress after user clicks on confirmation button
    this.evidenceSharingInProgress = false;

    this.isLoadingAlreadySharedOrganizations = true;

    this.selectedAttachments = [];
    this.selectedOrganizations = [];
    this.organizationSearchText = '';
    this.isShareEvidenceModalVisible = false;
    this.isConfirmationModalVisible = false;

    this.isSuccessShareEvidence = false;
    this.isFailureShareEvidence = false;

    // Actions we are listening for
    this.bindActions(
      'partnerOrganizationsFetched', this.onPartnerOrganizationsFetched,
      'partnerOrganizationsFetching', this.onPartnerOrganizationsFetching,
      'updateSelectedAttachments', this.onUpdateSelectedAttachments,
      'startSharingEvidence', this.onStartSharingEvidence,
      'stopSharingEvidence', this.onStopSharingEvidence,
      'updateSelectedOrganizations', this.onUpdateSelectedOrganizations,
      'updateSearchOrganizationText', this.onUpdateSearchOrganizationText,
      'saveSharingEvidence', this.onSaveSharingEvidence,
      'confirmationSharingEvidence', this.onConfirmationSharingEvidence,
      'evidenceShared', this.onEvidenceShared,
      'loadingMorePartnerOrganizations', this.onLoadingMorePartnerOrganizations,
      'alreadySharedOrganizationsFetched', this.onAlreadySharedOrganizationsFetched,
      'updateorganizationSearchText', this.onUpdateorganizationSearchText,
      'fetchingAlreadySharedOrganizations', this.onFetchingAlreadySharedOrganizations
    );
  },

  //
  // Get store state
  //
  getState() {
    return {
      pageSize: this.pageSize,
      pageNumber: this.pageNumber,
      partnerOrganizations: this.getUpdatedPartnerOrganizations({
        pageNumber: this.pageNumber,
        organizationSearchText: this.organizationSearchText,
      }),
      isLoadingPartnerOrganizations: this.isLoadingPartnerOrganizations,
      selectedAttachments: this.selectedAttachments,
      evidenceSharingInProgress: this.evidenceSharingInProgress,
      shareSelectedAttachments: this.shareSelectedAttachments,
      selectedOrganizations: this.selectedOrganizations,
      organizationSearchText: this.organizationSearchText,
      isShareEvidenceModalVisible: this.isShareEvidenceModalVisible,
      isConfirmationModalVisible: this.isConfirmationModalVisible,
      hasMorePartnerOrganizations: this.getUpdatedLoadMoreStatus({
        pageNumber: this.pageNumber,
        organizationSearchText: this.organizationSearchText,
      }),
      loadingMorePartnerOrganizations: this.loadingMorePartnerOrganizations,
      alreadySharedOrganizations: this.alreadySharedOrganizations,
      searchedPartnerOrganizations: this.searchedPartnerOrganizations,
      isLoadingAlreadySharedOrganizations: this.isLoadingAlreadySharedOrganizations,
      isSuccessShareEvidence: this.isSuccessShareEvidence,
      isFailureShareEvidence: this.isFailureShareEvidence,
    };
  },

  onStartSharingEvidence() {
    this.isShareEvidenceModalVisible = true;
    this.isConfirmationModalVisible = false;
    this.emit('change');
  },

  onStopSharingEvidence() {
    this.isShareEvidenceModalVisible = false;
    this.isConfirmationModalVisible = false;
    this.evidenceSharingInProgress = false;
    this.isSuccessShareEvidence = false;
    this.isFailureShareEvidence = false;
    this.selectedOrganizations = [];
    this.alreadySharedOrganizations = [];
    this.emit('change');
  },

  onSaveSharingEvidence() {
    this.isShareEvidenceModalVisible = false;
    this.isConfirmationModalVisible = true;
    this.emit('change');
  },

  onConfirmationSharingEvidence() {
    this.evidenceSharingInProgress = true;
    this.emit('change');
  },

  onEvidenceShared(isSuccess) {
    if (isSuccess) {
      // only empty selected attachments when shared successfully
      this.selectedAttachments = [];
      this.isSuccessShareEvidence = true;
      this.evidenceSharingInProgress = false;
      _.debounce(this.onStopSharingEvidence, 2000)();
    } else {
      this.isFailureShareEvidence = true;
      this.evidenceSharingInProgress = false;
    }
    this.emit('change');
  },

  onFetchingAlreadySharedOrganizations() {
    this.isLoadingAlreadySharedOrganizations = true;
    this.emit('change');
  },

  onAlreadySharedOrganizationsFetched(response) {
    this.isLoadingAlreadySharedOrganizations = false;
    this.alreadySharedOrganizations = response;
    this.emit('change');
  },

  onPartnerOrganizationsFetching() {
    this.isLoadingPartnerOrganizations = true;
    this.emit('change');
  },

  getUpdatedPartnerOrganizations({ pageNumber, organizationSearchText }) {
    let partnerOrganizations = [];
    const orgsDict = this.searchedPartnerOrganizations[organizationSearchText];

    _.forEach(orgsDict, (orgs, page) => {
      if (pageNumber >= page) {
        const pOrgs = this.searchedPartnerOrganizations[organizationSearchText][page];
        partnerOrganizations = _(partnerOrganizations).concat(pOrgs).value();
      }
    });
    return partnerOrganizations;
  },

  getUpdatedLoadMoreStatus({ pageNumber, organizationSearchText }) {
    let hasMore = false;
    if (this.hasMorePartnerOrganizations[organizationSearchText] &&
      this.hasMorePartnerOrganizations[organizationSearchText][pageNumber]) {
      hasMore = this.hasMorePartnerOrganizations[organizationSearchText][pageNumber];
    }
    return hasMore;
  },


  onPartnerOrganizationsFetched({ response, pageNumber, organizationSearchText }) {
    this.pageNumber = pageNumber;
    this.organizationSearchText = organizationSearchText;

    this.searchedPartnerOrganizations[organizationSearchText] =
      this.searchedPartnerOrganizations[organizationSearchText] || {};
    this.hasMorePartnerOrganizations[organizationSearchText] =
      this.hasMorePartnerOrganizations[organizationSearchText] || {};

    this.searchedPartnerOrganizations[organizationSearchText][pageNumber] = response.results;

    const pageCount = Math.ceil(response.count / this.pageSize);

    this.hasMorePartnerOrganizations[organizationSearchText][pageNumber] =
      (pageCount > pageNumber);
    this.isLoadingPartnerOrganizations = false;
    this.loadingMorePartnerOrganizations = false;

    this.emit('change');
  },

  onUpdateorganizationSearchText({ organizationSearchText }) {
    this.organizationSearchText = organizationSearchText;
    this.emit('change');
  },

  onLoadingMorePartnerOrganizations() {
    this.loadingMorePartnerOrganizations = true;
    this.emit('change');
  },

  onUpdateSelectedAttachments(selectedAttachments) {
    this.selectedAttachments = selectedAttachments;
    this.emit('change');
  },

  onUpdateSelectedOrganizations(selectedOrganizations) {
    this.selectedOrganizations = selectedOrganizations;
    this.emit('change');
  },

  onUpdateSearchOrganizationText(organizationSearchText) {
    this.organizationSearchText = organizationSearchText;
    this.emit('change');
  },

});

module.exports = ShareEvidenceStore;
