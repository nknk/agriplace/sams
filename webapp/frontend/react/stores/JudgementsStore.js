export const JudgementsStore = Fluxxor.createStore({

  initialize() {
    this.judgements = [];
    this.questionsInProgress = [];
    this.isLoaded = false;

    this.isConfirmAllModalOpen = false;
    this.isConfirmAllInProgress = false;

    this.bindActions(
      'judgementsLoaded', this.onJudgementsLoaded,
      'judgementSaving', this.onJudgementSaving,
      'updatedJudgementSaved', this.onUpdatedJudgementSaved,
      'createdJudgementSaved', this.onCreatedJudgementSaved,
      'deletedJudgementSaved', this.onDeletedJudgementSaved,
      'openConfirmAllModal', this.onOpenConfirmAllModal,
      'closeConfirmAllModal', this.onCloseConfirmAllModal,
      'confirmAllInprogress', this.onConfirmAllInprogress,
      'failedToUpdateUnansweredJudgements', this.onFailedToUpdateUnansweredJudgements,
      'unansweredJudgementsUpdated', this.onUnansweredJudgementsUpdated
    );
  },

  //
  // Get store state
  //

  getState() {
    return {
      isLoaded: this.isLoaded,
      judgements: _.cloneDeep(this.judgements),
      questionsInProgress: _.cloneDeep(this.questionsInProgress),
      isConfirmAllModalOpen: this.isConfirmAllModalOpen,
      isConfirmAllInProgress: this.isConfirmAllInProgress,
    };
  },

  onJudgementSaving({ questionUuid }) {
    this.questionsInProgress.push(questionUuid);

    this.emit('change');
  },

  onJudgementsLoaded(judgements) {
    this.judgements = judgements;
    this.isLoaded = true;

    this.emit('change');
  },

  unblockQuestion(questionUuid) {
    this.questionsInProgress = this.questionsInProgress.filter(
      currentQuestionUuid => currentQuestionUuid !== questionUuid
    );
  },

  onUpdatedJudgementSaved({ judgement }) {
    const index = _.findIndex(this.judgements, { question: judgement.question });
    this.judgements.splice(index, 1, judgement);
    this.unblockQuestion(judgement.question);

    this.emit('change');
  },

  onCreatedJudgementSaved({ judgements }) {
    const judgement = judgements[0];

    this.judgements.push(judgement);
    this.unblockQuestion(judgement.question);

    this.emit('change');
  },

  onDeletedJudgementSaved({ judgement }) {
    const index = _.findIndex(this.judgements, { uuid: judgement.uuid });
    this.judgements.splice(index, 1);

    this.unblockQuestion(judgement.question);

    this.emit('change');
  },

  onOpenConfirmAllModal() {
    this.isConfirmAllModalOpen = true;
    this.emit('change');
  },

  onCloseConfirmAllModal() {
    this.isConfirmAllModalOpen = false;
    this.emit('change');
  },

  onConfirmAllInprogress() {
    this.isConfirmAllModalOpen = false;
    this.isConfirmAllInProgress = true;
    this.emit('change');
  },

  onFailedToUpdateUnansweredJudgements() {
    this.isConfirmAllModalOpen = true;
    this.isConfirmAllInProgress = false;
    this.emit('change');
  },

  onUnansweredJudgementsUpdated() {
    this.isConfirmAllModalOpen = false;
    this.isConfirmAllInProgress = false;
    this.emit('change');
  },

});

module.exports = JudgementsStore;
