export const HzpcAssessmentStatuses = [
  { code: 'internal_audit_request', label: _t('Internal audit request') },
  { code: 'audit_hzpc', label: _t('Internal audit pending') },
  { code: 'self_managed', label: _t('Self managed') },
  { code: 'internal_audit_open', label: _t('Internal audit open') },
  { code: 'internal_audit_done', label: _t('Internal audit done') },
  { code: 'document_review_done', label: _t('Document review done') },
  { code: 'audit_done', label: _t('Audit done') },
  { code: 'action_required', label: _t('Action required') },
  { code: 'action_done', label: _t('Action done') },
  { code: 'reaudit_required', label: _t('Reaudit required') },
  { code: 'certified', label: _t('Certified') },
  { code: 'rejected', label: _t('Rejected') },
];
