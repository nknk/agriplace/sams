

const OrganizationsStore = require('./OrganizationsStore.js');
const AssessmentsStore = require('./AssessmentsStore.js');
const AttachmentsStore = require('./AttachmentsStore.js');
const ProductsStore = require('./ProductsStore.js');
const ProductLinksStore = require('./ProductLinksStore.js');
const HelpLinksStore = require('./HelpLinksStore.js');
const PlotsStore = require('./PlotsStore.js');
const FormStore = require('./FormStore.js');
const RbacStore = require('./RbacStore.js');
const ReviewStore = require('./ReviewStore.js');
const DashboardStore = require('./DashboardStore.js');
const JudgementsStore = require('./JudgementsStore.js');
const OfflineNotificationStore = require('./OfflineNotificationStore.js');
const CertificatesStore = require('./CertificatesStore.js');
const DocTypeReuseStore = require('./DocTypeReuseStore.js');
const ShareEvidenceStore = require('./ShareEvidenceStore.js');
const SharedEvidenceStore = require('./SharedEvidenceStore.js');

module.exports = {
  OrganizationsStore: new OrganizationsStore(),
  AssessmentsStore: new AssessmentsStore(),
  AttachmentsStore: new AttachmentsStore(),
  HelpLinksStore: new HelpLinksStore(),
  PlotsStore: new PlotsStore(),
  ProductsStore: new ProductsStore(),
  ProductLinksStore: new ProductLinksStore(),
  FormStore: new FormStore(),
  JudgementsStore: new JudgementsStore(),
  ReviewStore: new ReviewStore(),
  DashboardStore: new DashboardStore(),
  RbacStore: new RbacStore(),
  OfflineNotificationStore: new OfflineNotificationStore(),
  CertificatesStore: new CertificatesStore(),
  DocTypeReuseStore: new DocTypeReuseStore(),
  ShareEvidenceStore: new ShareEvidenceStore(),
  SharedEvidenceStore: new SharedEvidenceStore(),
};
