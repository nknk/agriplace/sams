import AttachmentsDAL from '../dal/attachmentsDAL.js';

const AttachmentsStore = Fluxxor.createStore({

  initialize() {
    this.attachments = [];
    this.filteredAttachments = {};

    this.pageCount = {};
    this.isLoading = true;

    // Filters
    this.filters = {
      pageNumber: 1,
      startDate: null,
      endDate: null,
      title: null,
      usedIn: null,
      usableFor: null,
      attachmentType: null,
      evidenceType: null,
      isOptionalEvidenceIncluded: '0',
    };

    // updated on every call to sortAttachments so it reflects the current order
    const defaultSort = { key: 'modified_time', sortOrder: 'normal' };
    this.sortOrder = defaultSort.sortOrder;
    this.selectedColumn = defaultSort.key;

    this.dal = new AttachmentsDAL(window.jsContext.API_URL);

    // Filters drop down data
    this.evidenceTypesList = [];
    this.attachmentTypesList = [];
    this.usedInList = [];
    this.usableForList = [];

    // Actions we are listening for
    this.bindActions(
      'loadAttachmentsOfType', this.loadAttachmentsOfType,
      'sortAttachments', this.sortAttachments,
      'sortFilteredAttachments', this.sortFilteredAttachments,
      'saveAttachment', this.saveAttachment,
      'updateAttachment', this.updateAttachment,
      'deleteAttachment', this.deleteAttachment,
      'evidenceTypesListFetched', this.onEvidenceTypesListFetched,
      'attachmentTypesListFetched', this.onAttachmentTypesListFetched,
      'usedInListFetched', this.onUsedInListFetched,
      'usableForListFetched', this.onUsableForListFetched,
      'filteredAttachmentsUpdated', this.onFilteredAttachmentsUpdated,
      'loadingFilteredAttachments', this.onLoadingFilteredAttachments
    );
  },

  getFilteredKey(filters = null) {
    if (!filters) {
      filters = this.filters;
    }

    const key = [
      filters.pageNumber,
      filters.startDate,
      filters.endDate,
      filters.title,
      filters.usedIn,
      filters.usableFor,
      filters.attachmentType,
      filters.evidenceType,
      filters.isOptionalEvidenceIncluded,
    ];

    return key;
  },

  getFilteredAttachments() {
    const key = this.getFilteredKey();
    return this.filteredAttachments[key];
  },

  setFilteredAttachments(attachments) {
    const key = this.getFilteredKey();
    this.filteredAttachments[key] = attachments;
  },

  getPageCount() {
    const key = this.getFilteredKey();
    return this.pageCount[key];
  },

  setPageCount(pageCount, filters = null) {
    const key = this.getFilteredKey(filters);
    this.pageCount[key] = pageCount;
  },

  //
  // Get store state
  //
  getState() {
    const filteredAttachments = this.getFilteredAttachments();
    const pageCount = this.getPageCount();
    return {
      attachments: this.attachments,
      filteredAttachments,
      pageCount,
      pageNumber: this.filters.pageNumber,
      sortOrder: this.sortOrder,
      selectedColumn: this.selectedColumn,
      evidenceTypesList: this.evidenceTypesList,
      attachmentTypesList: this.attachmentTypesList,
      usedInList: this.usedInList,
      usableForList: this.usableForList,
      isLoading: this.isLoading,
    };
  },

  // Actions
  sortList(data, param = 'name', sortOrder = 'asc') {
    const sortedList = _.sortByOrder(data, (obj) => {
      const key = _.get(obj, param, '');
      return typeof key === 'string' ? key.toLowerCase() : key;
    }, sortOrder);

    return sortedList;
  },

  onEvidenceTypesListFetched(payload) {
    this.evidenceTypesList = this.sortList(payload);
    this.emit('change');
  },

  onAttachmentTypesListFetched(payload) {
    this.attachmentTypesList = this.sortList(payload);
    this.emit('change');
  },

  onUsedInListFetched(payload) {
    this.usedInList = this.sortList(payload);
    this.emit('change');
  },

  onUsableForListFetched(payload) {
    this.usableForList = this.sortList(payload);
    this.emit('change');
  },

  // Loads attachments of some type from server
  loadAttachmentsOfType(payload) {
    this.dal.loadAttachmentsOfType(payload.attachmentsType)
      .then((data) => {
        this.attachments = data;
        this.emit('attachment-saved');
        this.emit('change');
      },

      (err) => {});
  },

  onFilteredAttachmentsUpdated({ response, filters }) {
    // update store filters
    this.filters = filters;

    if (response) {
      const pageCount = Math.ceil(response.count / 10);
      this.setPageCount(pageCount);
      this.setFilteredAttachments(response.results);
    }
    this.isLoading = false;
    this.sortFilteredAttachments();
  },

  onLoadingFilteredAttachments() {
    this.isLoading = true;
    this.emit('change');
  },

  sortAttachments(data) {
    // apply default sort settings for non-arguments call
    data = tools.forceObject(data, { key: 'title', sortOrder: 'normal' });

    if (data.sortOrder === 'toggle') {
      this.sortOrder = (this.sortOrder === 'normal') ? 'inverse' : 'normal';
    } else {
      this.sortOrder = data.sortOrder;
    }

    const sortReversed = this.sortOrder === 'inverse' ? 'desc' : 'asc';
    this.attachments = _.sortByOrder(this.attachments, (obj) => {
      const key = _.get(obj, data.key, '');
      return typeof key === 'string' ? key.toLowerCase() : key;
    }, sortReversed);

    this.emit('change');
  },

  sortFilteredAttachments(data = null) {
    if (data) {
      this.sortOrder = (this.sortOrder === 'normal') ? 'inverse' : 'normal';
      this.selectedColumn = data.key ? data.key : 'modified_time';
    }

    const requiredSortOrder = this.sortOrder === 'inverse' ? [true] : [false];

    this.setFilteredAttachments(_.sortByOrder(
      this.getFilteredAttachments(), (obj) => {
        let key = _.get(obj, this.selectedColumn, '');
        if ($.isArray(key)) {
          key = key.reduce((initialValue, nextValue) => (
            initialValue + (nextValue.name ? `${nextValue.name} ` : '')
          ), '');
        }
        return (typeof key === 'string') ? key.toLowerCase() : key;
      },
      requiredSortOrder
    ));

    this.emit('change');
  },

  saveAttachment(payload) {
    this.dal.saveAttachment(payload.attachment)
      .then((data) => {
        this.attachments.push(data);
        this.emit('attachment-saved');
        this.emit('attachments-modified');
        this.emit('change');
      },

      // error
      (err) => {
        log.error('error on saving attachment', err);
      });
  },

  updateAttachment(payload) {
    this.dal.updateAttachment(payload.attachment)
      .then((data) => {
        this.attachments = _.remove(
          this.attachments, (attachment) => attachment.uuid !== data.uuid
        );
        this.attachments.push(data);
        this.emit('attachments-modified');
        this.emit('change');
      },

      // error
      (err) => {
        log.error('error on updating attachment', err);
      });
  },

  deleteAttachment(payload) {
    this.dal.deleteAttachment(payload.attachment)
      .then((data) => {
        this.attachments = _.remove(
          this.attachments, (attachment) => attachment.uuid !== data.uuid
        );
        this.emit('attachments-modified');
        this.emit('change');
      },

      (err) => {
        log.error('error on deleting attachment', err);
      });
  },

});

module.exports = AttachmentsStore;
