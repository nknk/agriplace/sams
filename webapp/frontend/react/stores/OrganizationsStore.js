

var OrganizationsStore = Fluxxor.createStore({
  initialize() {
    this.organizations = [];
    this.auditors = [];
    this.traders = [];
    this.bindActions(
      'organizationsUpdated', this.onOrganizationsUpdated,
      'auditorsUpdated', this.onAuditorsUpdated,
      'tradersUpdated', this.onTradersUpdated
    );
  },

  getState() {
    return {
      organizations: this.organizations,
      auditors: this.auditors,
      traders: this.traders,
    };
  },

  onOrganizationsUpdated(payload) {
    this.organizations = payload;
    this.emit('change');
    this.emit('organizationsUpdated');
  },

  onAuditorsUpdated(payload) {
    this.auditors = payload;
    this.emit('change');
    this.emit('auditorsUpdated');
  },

  onTradersUpdated(payload) {
    this.traders = payload;
    this.emit('change');
  },

  getOrganization(uuid) {
    return _.find(this.organizations, { uuid });
  },

  getAuditors() {
    return this.auditors;
  },

  getTraders() {
    return _.cloneDeep(this.traders);
  },
});

module.exports = OrganizationsStore;
