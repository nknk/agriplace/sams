const ProductsStore = Fluxxor.createStore({

  initialize() {
    this.productLinks = [];
    this.isProductLinksLoaded = false;

    this.bindActions(
      'productLinksLoaded', this.onProductLinksLoaded,
      'addProductLinksStarted', this.onAddProductsStarted,
      'addProductLinksFinished', this.onAddProductsFinished,
      'removeProductLinksStarted', this.onRemoveProductsStarted,
      'removeProductLinksFinished', this.onRemoveProductsFinished,
      'addProductDependentQuestionnairesFinished', this.onAddProductDependentQuestionnairesFinished,
      'removeProductDependentQuestionnairesFinished', this.onRemoveProductDependentQuestionnairesFinished
    );
  },

  createProductLinks(productLinks, isInProgress) {
    return productLinks.map(pl => ({ uuid: pl, isInProgress }));
  },

  getState() {
    return {
      productLinks: this.productLinks,
      productDependentQuestionnaires: this.productDependentQuestionnaires,
      isProductLinksLoaded: this.isProductLinksLoaded,
      isProductAdded: this.isProductAdded,
    };
  },

  onProductLinksLoaded(productLinks) {
    this.productLinks = this.createProductLinks(productLinks, false);
    this.isProductLinksLoaded = true;

    this.emit('change');
  },

  onAddProductsStarted(productLinks) {
    this.productLinks = this.productLinks.concat(this.createProductLinks(productLinks, true));

    this.emit('change');
  },

  onAddProductsFinished(productLinks) {
    productLinks.forEach(plUnblocked => {
      const plToModify = this.productLinks.find(pl => pl.uuid === plUnblocked) || {};
      plToModify.isInProgress = false;
    });

    this.emit('change');
  },

  onRemoveProductsStarted(productLinks) {
    productLinks.forEach(plForRemoval => {
      const plToModify = this.productLinks.find(pl => pl.uuid === plForRemoval) || {};
      plToModify.isInProgress = true;
    });

    this.emit('change');
  },

  onRemoveProductsFinished(productLinks) {
    productLinks.forEach(pl => _.remove(this.productLinks, oldPl => oldPl.uuid === pl));
    this.emit('change');
  },

  onAddProductDependentQuestionnairesFinished(questionnaires) {
    this.productDependentQuestionnaires = questionnaires;
    this.isProductAdded = true;
    this.emit('change');
  },

  onRemoveProductDependentQuestionnairesFinished(questionnaires) {
    this.productDependentQuestionnaires = questionnaires;
    this.isProductAdded = false;
    this.emit('change');
  },
});

module.exports = ProductsStore;
