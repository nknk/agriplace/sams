const moment = require('moment');
import pageData from '../components/Utils/pageData.js';

import _ from 'lodash';

const DashboardStore = Fluxxor.createStore({

  initialize() {
    this.assessmentTypes = {};
    this.isAssessmentTypesFetched = false;
    this.isAssessmentTypesLoading = false;

    this.assessmentStates = {};
    this.isAssessmentStatesFetched = false;
    this.isAssessmentStatesLoading = false;

    this.internalInspectors = {};
    this.isInternalInspectorsFetched = false;
    this.isInternalInspectorsLoading = false;

    this.filters = {
      year: +moment().format('YYYY'),
      startDate: null,
      endDate: null,
      assessmentTypes: [],
      assessmentStates: [],
      internalInspectors: [],
      search: null,
      pageNumber: 1,
    };

    this.filteredAssessments = {};
    this.filteredAssessmentsPageCount = {};

    this.isUpdatingFilteredAssessments = 0;

    this.organizationDetails = {};
    this.isFetchingOrganizationDetails = {};

    // Actions we are listening for
    this.bindActions(
      'auditorChanged', this.onAuditorChanged,
      'assessmentTypesForFiltersFetched', this.onAssessmentTypesForFiltersFetched,
      'assessmentTypesForFiltersLoading', this.onAssessmentTypesForFiltersLoading,
      'assessmentStatesForFiltersFetched', this.onAssessmentStatesForFiltersFetched,
      'assessmentStatesForFiltersLoading', this.onAssessmentStatesForFiltersLoading,
      'internalInspectorsForFiltersFetched', this.onInternalInspectorsForFiltersFetched,
      'internalInspectorsForFiltersLoading', this.onInternalInspectorsForFiltersLoading,
      'filteredAssessmentsFetched', this.onFilteredAssessmentsFetched,
      'updateFilters', this.onUpdateFilters,
      'updatingFilteredAssessments', this.onUpdatingFilteredAssessments,
      'plannedAuditDateUpdated', this.onPlannedAuditDateUpdated,
      'isFetchingOrganizationDetails', this.onIsFetchingOrganizationDetails,
      'updateOrganizationDetails', this.onUpdateOrganizationDetails
    );
  },

  getAssessmentTypes(year) {
    return this.assessmentTypes[year];
  },

  getAssessmentStates(year) {
    return this.assessmentStates[year];
  },

  getInternalInspectors(year) {
    return this.internalInspectors[year];
  },

  //
  // Get store state
  //
  getState() {
    const key = this.getFilteredKey(this.filters);
    const year = this.filters.year;

    return {
      assessmentTypes: this.getAssessmentTypes(year),
      isAssessmentTypesFetched: this.isAssessmentTypesFetched,
      isAssessmentTypesLoading: this.isAssessmentTypesLoading,
      assessmentStates: this.getAssessmentStates(year),
      isAssessmentStatesFetched: this.isAssessmentStatesFetched,
      isAssessmentStatesLoading: this.isAssessmentStatesLoading,
      internalInspectors: this.getInternalInspectors(year),
      isInternalInspectorsFetched: this.isInternalInspectorsFetched,
      isInternalInspectorsLoading: this.isInternalInspectorsLoading,
      filters: this.filters,
      filteredAssessments: this.filteredAssessments[key],
      filteredAssessmentsPageCount: this.filteredAssessmentsPageCount[key],
      isUpdatingFilteredAssessments: this.isUpdatingFilteredAssessments,
      organizationDetails: this.organizationDetails,
      isFetchingOrganizationDetails: this.isFetchingOrganizationDetails,
    };
  },

  getFilteredKey(filters = null) {
    if (!filters) {
      filters = this.filters;
    }

    return [
      filters.year,
      filters.startDate,
      filters.endDate,
      filters.assessmentTypes,
      filters.assessmentStates,
      filters.internalInspectors,
      filters.search,
      filters.pageNumber,
    ];
  },

  getAllFilteredAssessments() {
    return this.filteredAssessments;
  },

  getAuditorLabel(inspectorId) {
    const inspectors = this.getInternalInspectors(this.filters.year) || [];
    const inspector = (inspectors && inspectors.length && inspectorId) ?
      inspectors.find(ins => ins.id === inspectorId) : null;
    return inspector ? inspector.label : null;
  },

  assignLabels(assessments) {
    return assessments.map(assessment => {
      const auditMonthCode = assessment.preferred_month;
      // const auditorCode = assessment.auditor || 'open';
      // assessment.auditor = auditorCode;
      assessment.assessment.typeLabel = assessment.assessment.label;
      assessment.assessment.statusLabel = assessment.assessment.status;
      assessment.preferred_monthLabel = auditMonthCode ?
        tools.moment(auditMonthCode, 'MM').locale(pageData.languageCode).format('MMMM')
        : '';
      assessment.auditorLabel = this.getAuditorLabel(assessment.auditor);
      return assessment;
    });
  },

  onAuditorChanged({ workflowUuid, auditor }) {
    const key = this.getFilteredKey(this.filters);
    const assessments = this.filteredAssessments[key] || [];
    const assessment = assessments.find(x => x.workflow_uuid === workflowUuid);
    if (assessment) {
      assessment.auditor = auditor;
    }
    this.emit('change');
  },

  getPermissions(workflowUuid) {
    const key = this.getFilteredKey(this.filters);
    const assessments = this.filteredAssessments[key] || [];
    const assessment = assessments.find(x => x.workflow_uuid === workflowUuid) || {};
    return assessment.permissions || [];
  },

  onAssessmentTypesForFiltersLoading() {
    this.isAssessmentTypesLoading = true;
    this.emit('change');
  },

  onAssessmentTypesForFiltersFetched({ response, year }) {
    response.forEach(type => {
      type.label = type.name;
      type.count = type.work_flow_count;
      type.code = type.uuid;
    });
    this.assessmentTypes[year] = response;
    this.isAssessmentTypesFetched = true;
    this.isAssessmentTypesLoading = false;
    this.emit('change');
  },

  onAssessmentStatesForFiltersLoading() {
    this.isAssessmentStatesLoading = true;
    this.emit('change');
  },

  onAssessmentStatesForFiltersFetched({ response, year }) {
    response.forEach(state => {
      state.label = state.value;
      state.count = state.work_flow_count;
    });
    this.assessmentStates[year] = response;
    this.isAssessmentStatesFetched = true;
    this.isAssessmentStatesLoading = false;
    this.emit('change');
  },

  onInternalInspectorsForFiltersLoading() {
    this.isInternalInspectorsLoading = true;
    this.emit('change');
  },

  onInternalInspectorsForFiltersFetched({ response, year }) {
    response.forEach(inspector => {
      inspector.code = inspector.id;
      inspector.label = `${inspector.first_name} ${inspector.last_name}`;
      inspector.count = inspector.work_flow_count;
    });
    this.internalInspectors[year] = response;
    this.isInternalInspectorsFetched = true;
    this.isInternalInspectorsLoading = false;

    const key = this.getFilteredKey(this.filters);
    const assessments = this.filteredAssessments[key] || [];
    if (assessments.length) {
      this.filteredAssessments[key] = this.assignLabels(assessments);
    }
    this.emit('change');
  },

  onUpdateFilters({ filters }) {
    this.filters = filters;
    this.emit('change');
  },

  onFilteredAssessmentsFetched({ response, filters }) {
    const key = this.getFilteredKey(filters);
    this.filteredAssessments[key] = this.assignLabels(response.results);
    this.filteredAssessmentsPageCount[key] = Math.ceil(response.count / 10);
    this.isUpdatingFilteredAssessments = this.isUpdatingFilteredAssessments - 1;
    this.onUpdateFilters({ filters });
  },

  onUpdatingFilteredAssessments() {
    this.isUpdatingFilteredAssessments = this.isUpdatingFilteredAssessments + 1;
    this.emit('change');
  },

  onPlannedAuditDateUpdated({ workflowUuid, date }) {
    // update all assessments fetched with the updated date
    _.each(this.filteredAssessments, assessments => {
      const assessment = assessments.find(a => a.workflow_uuid === workflowUuid);
      if (assessment) {
        assessment.audit_date_planned = date;
      }
    });
    this.emit('change');
  },

  onIsFetchingOrganizationDetails({ status, selectedOrganizationUuid }) {
    this.isFetchingOrganizationDetails[selectedOrganizationUuid] = status;
  },

  onUpdateOrganizationDetails({ details, selectedOrganizationUuid }) {
    this.organizationDetails[selectedOrganizationUuid] = details;
    this.isFetchingOrganizationDetails[selectedOrganizationUuid] = false;
    this.emit('change');
  },

});

module.exports = DashboardStore;
