
const OfflineNotificationStore = Fluxxor.createStore({

  initialize() {
    this.status = 'online';
    this.checkInternetStatusInProgress = false;
    this.bindActions(
      'updateInternetStatus', this.onUpdateInternetStatus,
      'checkInternetStatusInProgress', this.onCheckInternetStatusInProgress
    );
  },

  getState() {
    return {
      status: this.status,
      checkInternetStatusInProgress: this.checkInternetStatusInProgress,
    };
  },

  onUpdateInternetStatus(status) {
    this.status = status;
    this.checkInternetStatusInProgress = false;
    this.emit('change');
  },

  onCheckInternetStatusInProgress() {
    this.checkInternetStatusInProgress = true;
    this.emit('change');
  },
});


module.exports = OfflineNotificationStore;
