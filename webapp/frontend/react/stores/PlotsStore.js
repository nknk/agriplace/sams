

var PlotsStore = Fluxxor.createStore({

  initialize() {
    this.kvk = '';
    this.plots = undefined;
    this.rvoData = undefined;
    this.rvoErrors = undefined;
    this.isRvoLoading = false;

    // Actions we are listening for
    this.bindActions(
      'updateKvk', this.onUpdateKvk,
      'loadPlots', this.onLoadPlots,
      'loadRvoSuccess', this.onLoadRvoSuccess,
      'loadRvoFail', this.onLoadRvoFail,
      'loadRvoInProgress', this.onRvoInProgress
    );
  },

  //
  // Get store state
  //
  getState() {
    return {
      plots: this.plots,
      isRvoLoading: this.isRvoLoading,
      rvoData: this.rvoData,
      kvk: this.kvk,
      rvoErrors: this.rvoErrors,
    };
  },

  onUpdateKvk(kvk) {
    this.kvk = kvk;

    this.emit('change');
  },

  onLoadPlots(payload) {
    this.plots = payload;

    this.emit('change');
  },

  // RVO handlers
  onRvoInProgress(payload) {
    this.isRvoLoading = payload.isLoading;

    if (payload.isLoading) {
      this.rvoData = undefined;
      this.rvoErrors = undefined;
    }

    this.emit('change');
  },

  onLoadRvoSuccess(payload) {
    this.rvoData = payload.success;
    this.rvoErrors = undefined;

    this.emit('change');
  },

  onLoadRvoFail(payload) {
    this.rvoErrors = payload.errors.map((error) => error.msg);

    this.emit('change');
  },
});

module.exports = PlotsStore;
