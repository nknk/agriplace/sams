
const DocTypeReuseStore = Fluxxor.createStore({

  initialize() {
    // Actions we are ening for
    this.bindActions(
      'loadHistoricalAttachmentsStarted', this.onLoadHistoricalAttachmentsStarted,
      'loadHistoricalAttachments', this.loadHistoricalAttachments,

      'updateHistoricalAttachmentsStarted', this.updateHistoricalAttachmentsStarted,
      'updateHistoricalAttachmentsComplete', this.updateHistoricalAttachmentsComplete
    );

    this.historicalAttachments = {};
    this.fetchingInProgress = [];
    this.postingInProgress = [];
  },

  //
  // Get store state
  //
  getState() {
    return _.cloneDeep({
      historicalAttachments: this.historicalAttachments,
      fetchingInProgress: this.fetchingInProgress,
      postingInProgress: this.postingInProgress,
    });
  },


  // Actions

  // Loads attachments of some type from server

  updateHistoricalAttachmentsComplete(documentTypeUuid) {
    this.postingInProgress = this.fetchingInProgress.filter(u => u !== documentTypeUuid);

    this.emit('change');
  },

  loadHistoricalAttachments(data) {
    const documentTypeUuid = data[0].document_type_attachment;
    this.historicalAttachments[documentTypeUuid] = data;
    this.fetchingInProgress = this.fetchingInProgress.filter(u => u !== documentTypeUuid);

    this.emit('change');
  },


  onLoadHistoricalAttachmentsStarted(documentTypeUuid) {
    this.fetchingInProgress.push(documentTypeUuid);

    this.emit('change');
  },

  updateHistoricalAttachmentsStarted(documentTypeUuid) {
    this.postingInProgress.push(documentTypeUuid);

    this.emit('change');
  },
});

module.exports = DocTypeReuseStore;
