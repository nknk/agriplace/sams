import cookies from 'js-cookie';

export const csrftoken = cookies.get('csrftoken');

export function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
