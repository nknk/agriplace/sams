import { csrftoken, csrfSafeMethod } from './csrfToken';

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

class PlotsDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.apiUrl = apiUrl;
  }

  get(organizationUuid) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}/organization/${organizationUuid}/plots/`,
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
      }).then(
        (data) => {
          log.info('Get plotsDAL succeeded');
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Get plotsDAL failed.');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  sendKvk(organizationUuid, kvk) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}/organization/${organizationUuid}/plots/fetch/rvo/`,
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify({ organization_KVK: kvk }),
      }).then(
        (response) => {
          resolve(response);
          log.info('Get plotsDAL succeeded');
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Get plotsDAL failed.');
          reject(xhr.responseJSON);
        });
    });

    return actionPromise;
  }
}

export default PlotsDAL;
