const request = require('axios');

import { csrftoken } from './csrfToken';

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

class CertificatesDAL {

  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error('Not allowed! You\'ve tried to create an ' +
        'instance of CertificatesDAL without a apiUrl argument.');
    }

    this.apiUrl = apiUrl;
  }

  getList(organizationUuid) {
    return request.get(
      `${this.apiUrl}organizations/${organizationUuid}/certifications/`, config
    ).then(defaultHandle);
  }

  getCertificate(organizationUuid, certificateUuid) {
    return request.get(
      `${this.apiUrl}organizations/${organizationUuid}/certifications/${certificateUuid}/`, config
    ).then(defaultHandle);
  }

  delete(organizationUuid, certificateUuid) {
    return request.delete(
      `${this.apiUrl}organizations/${organizationUuid}/certifications/${certificateUuid}/`, config
    ).then(defaultHandle).catch(defaultHandle);
  }

  getCertificateTypes(organizationUuid) {
    return request.get(
      `${this.apiUrl}organizations/certificate-types/`
    ).then(defaultHandle);
  }

  create({ certificate, organizationUuid }) {
    return request.post(
      `${this.apiUrl}organizations/${organizationUuid}/certifications/`, certificate, config
    ).then(defaultHandle);
  }

  update({ certificate, organizationUuid }) {
    return request.put(
      `${this.apiUrl}organizations/${organizationUuid}/certifications/${certificate.uuid}/`, certificate, config
    ).then(defaultHandle);
  }
}

export default CertificatesDAL;
