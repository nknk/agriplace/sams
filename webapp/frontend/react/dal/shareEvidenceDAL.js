const request = require('axios');

import { csrftoken } from './csrfToken';

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if (response.status < 400) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

class ShareEvidenceDAL {

  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error('Not allowed! You\'ve tried to create an ' +
        'instance of ShareEvidenceDAL without a apiUrl argument.');
    }

    this.apiUrl = apiUrl;
  }

  getOrganizations(organizationUuid, attachmentUuids) {
    const url = `${this.apiUrl}organizations/${organizationUuid}/shared_attachments/share/?`;

    const attachmentUuidsCsv = attachmentUuids.join(',');
    const filter = `attachment_uuids=${attachmentUuidsCsv}`;
    const urlWithFilters = `${url}${filter}`;

    return request.get(urlWithFilters, config)
      .then(defaultHandle);
  }

  shareEvidence(organizationUuid, attachmentUuids, organizationMembershipUuids) {
    const url = `${this.apiUrl}organizations/${organizationUuid}/shared_attachments/share/`;

    const post = {
      attachment_uuids: attachmentUuids,
      membership_uuids: organizationMembershipUuids,
    };

    return request.post(url, post, config)
      .then(defaultHandle)
      .catch(defaultHandle);
  }
}

export default ShareEvidenceDAL;

