import { csrftoken } from './csrfToken';

const request = require('axios');
const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

class RbacDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of CertificatesDAL without a apiUrl argument.'
      );
    }

    this.apiUrl = apiUrl;
  }

  get() {
    return request.get(`${this.apiUrl}rbac/cp/`).then(defaultHandle);
  }

  save(payload) {
    return request.post(`${this.apiUrl}rbac/cp/`, payload, config).then(defaultHandle);
  }
}

export default RbacDAL;
