const request = require('axios');

const defaultHandle = (response) => {
  // if status is undefined or 0 then status is offline else its online
  const status = response.status ? 'online' : 'offline';
  return Promise.resolve(status);
};

class HeartbeatDAL {

  get() {
    // Using an existent or non-existent url to get any response
    // Offline status is identified when response status code is '0'
    return request.get('offline-notification')
        .then(defaultHandle)
        .catch(defaultHandle);
  }
}

export default HeartbeatDAL;
