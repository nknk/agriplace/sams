import { csrftoken, csrfSafeMethod } from './csrfToken';

const request = require('axios');
const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

class organizationsDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.organizationUuid = window.jsContext.ORGANIZATION_UUID;
    this.membershipUuid = window.jsContext.MEMBERSHIP_UUID;
    this.apiUrl = apiUrl;
  }

  loadOrganizations() {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        type: 'GET',
        url: `${this.apiUrl}organizations/`,
        contentType: 'application/json',
      }).then(
        (data) => {
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('organizationsDAL::loadOrganizations: ', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  loadAuditors() {
    return request.get(
      `${this.apiUrl}memberships/${this.membershipUuid}/auditors/`
    ).then(defaultHandle);
  }

  loadAuditorsByAssessement(assessmentUuid) {
    return request.get(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/auditors/`
    ).then(defaultHandle);
  }

  loadTraders() {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        type: 'GET',
        url: `${this.apiUrl}organizations/traders`,
        contentType: 'application/json',
      }).then(
        (data) => {
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('organizationsDAL::loadTraders: ', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }
}

export default organizationsDAL;
