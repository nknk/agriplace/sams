import { csrftoken, csrfSafeMethod } from './csrfToken';

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

class AttachmentLinksDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.organizationId = window.jsContext.ORGANIZATION_UUID;
    this.membershipUuid = window.jsContext.MEMBERSHIP_UUID;
    this.apiUrl = apiUrl;
  }

  create(attachmentLink, assessmentUuid) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/attachments/`,
        data: JSON.stringify(attachmentLink),
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
      }).then(
        (data) => {
          log.info('Create attachmentLink succeeded');
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Create attachmentLink failed.');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  update(attachmentLink, assessmentUuid, attachmentLinkUuid) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/attachments/${attachmentLinkUuid}/`,
        data: JSON.stringify(attachmentLink),
        contentType: 'application/json',
        type: 'PUT',
        dataType: 'json',
      }).then(
        (data) => {
          log.info('Update attachmentLink succeeded');
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Update attachmentLink failed.');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  delete(assessmentUuid, attachmentLinkUuid) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/attachments/${attachmentLinkUuid}`,
        contentType: 'application/json',
        type: 'DELETE',
        dataType: 'json',
      }).then(
        (data) => {
          log.info('Delete attachmentLink succeeded');
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Delete attachmentLink failed.');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }
}

export default AttachmentLinksDAL;
