import { csrftoken, csrfSafeMethod } from './csrfToken';

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

class FormDAL {

  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of DAL without a apiUrl argument.'
      );
    }

    this.apiUrl = apiUrl;
  }

  saveSchema(uuid, schema) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}forms/schema/${uuid}/`,
        type: 'PUT',
        dataType: 'json',
        data: JSON.stringify(schema),
        contentType: 'application/json',
      }).then(
        (data) => {
          log.info('formDAL: Form PUT succeeded.', data);
          resolve();
        },

        (xhr, textStatus, errorThrown) => {
          log.error('formDAL: Form PUT failed...', textStatus, errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  getSchema(uuid) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}forms/schema/${uuid}/`,
        type: 'GET',
        dataType: 'json',
      }).then(
        (data) => {
          log.info('formDAL: Form get succeeded.', data);
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('formDAL: Form get failed...', textStatus, errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  getFormValue(formUuid) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}forms/form/${formUuid}/`,
        type: 'GET',
        dataType: 'json',
      }).then((data) => {
        log.info('formDAL: Form get succeeded.', data);
        resolve(data);
      },

      (xhr, textStatus, errorThrown) => {
        log.error('formDAL: Form get failed...', textStatus, errorThrown);
        reject({ textStatus, errorThrown });
      });
    });
  }

  submit(value, formUuid) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}forms/form/${formUuid}/`,
        type: 'PUT',
        data: JSON.stringify(value),
        dataType: 'json',
      }).then(
        (data) => {
          if (!_.isEmpty(data.errors)) {
            reject(data.errors);
          } else {
            resolve(data);
          }

          log.info('formDAL: Form submit succeeded.', data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('formDAL: Form submit failed...', textStatus, errorThrown);
          reject([errorThrown]);
        });
    });
  }
}

export default FormDAL;
