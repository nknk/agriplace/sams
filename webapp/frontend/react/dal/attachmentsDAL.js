import request from 'axios';
import { csrftoken, csrfSafeMethod } from './csrfToken';

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if (response.status < 400) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

class AttachmentsDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.organizationUuid = window.jsContext.ORGANIZATION_UUID;
    this.membershipUuid = window.jsContext.MEMBERSHIP_UUID;
    this.apiUrl = apiUrl;
  }

  loadAttachmentsOfType(attachmentType) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}organizations/${this.organizationUuid}/attachments/types/${attachmentType}`,
        dataType: 'json',
      }).then(
        (data) => {
          log.info('load attachments of type ', attachmentType, ' succeeded');
          resolve(data);
        },
        (xhr, textStatus, errorThrown) => {
          log.error('Load attachments or type ', attachmentType, ' failed');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  saveAttachment(attachment) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        url: `${this.apiUrl}organizations/${this.organizationUuid}/attachments/`,
        data: JSON.stringify(attachment),
        contentType: 'application/json',
      }).then(
        (data) => {
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  updateAttachment(attachment) {
    const attachmentUuid = attachment.uuid;
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        type: 'PUT',
        url: `${this.apiUrl}organizations/${this.organizationUuid}/attachments/${attachmentUuid}/`,
        data: JSON.stringify(attachment),
        contentType: 'application/json',
      }).then(
        (data) => {
          log.info('Attachment updated');
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Could not update attachment');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }

  deleteAttachment(attachment) {
    const attachmentUuid = attachment.uuid;
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        type: 'DELETE',
        url: `${this.apiUrl}organizations/${this.organizationUuid}/attachments/${attachmentUuid}/`,
        data: JSON.stringify(attachment),
        contentType: 'application/json',
      }).then(
      (data) => {
        log.info('Attachment deleted');
        resolve(data);
      },

      (xhr, textStatus, errorThrown) => {
        log.error('Could not delete attachment');
        reject({ textStatus, errorThrown });
      });
    });

    return actionPromise;
  }

  getPaginatedAttachments({ pageNumber }) {
    return request.get(`${this.apiUrl}organizations/${this.organizationUuid}/attachments/?page=${pageNumber}`, config)
      .then(defaultHandle);
  }

  getEvidenceTypesList() {
    return request.get(`${this.apiUrl}organizations/${this.organizationUuid}/attachments/document_types/`, config)
      .then(defaultHandle);
  }

  getAttachmentTypesList() {
    return request.get(`${this.apiUrl}organizations/${this.organizationUuid}/attachments/attachment_types/`, config)
      .then(defaultHandle);
  }

  getUsedInList() {
    return request
      .get(
        `${this.apiUrl}memberships/${this.membershipUuid}/assessment-types/?kind=assessment`,
        config
      )
      .then(defaultHandle);
  }

  getUsableForList() {
    return request
      .get(
        `${this.apiUrl}memberships/${this.membershipUuid}/assessment-types/?kind=assessment&exclude_non_public=1`,
        config
      )
      .then(defaultHandle);
  }

  applyFilters({
    pageNumber,
    startDate,
    endDate,
    title,
    usedIn,
    usableFor,
    attachmentType,
    evidenceType,
    isOptionalEvidenceIncluded,
  }) {
    const url = `${this.apiUrl}organizations/${this.organizationUuid}/attachments/?`;
    const filters = [];
    if (pageNumber) { filters.push(`page=${pageNumber}`); }
    if (startDate) { filters.push(`modified_range_start=${startDate}`); }
    if (endDate) { filters.push(`modified_range_end=${endDate}`); }
    if (title) { filters.push(`title=${title}`); }
    if (usedIn) { filters.push(`used_in_standard=${usedIn}`); }
    if (usableFor) { filters.push(`usable_for_standard=${usableFor}`); }
    if (attachmentType) { filters.push(`attachment_type=${attachmentType}`); }
    if (evidenceType) { filters.push(`evidence_type=${evidenceType}`); }
    if (isOptionalEvidenceIncluded) { filters.push(`include_optional_evidence=${isOptionalEvidenceIncluded}`); }

    return request.get(`${url}${filters.join('&')}`, config)
      .then(defaultHandle)
      .catch(defaultHandle);
  }
}

export default AttachmentsDAL;
