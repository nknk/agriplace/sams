import { csrftoken } from './csrfToken';

const request = require('axios');
const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if (response.status < 400) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response);
  }
  return promise;
};

class DashboardDAL {
  constructor({ apiUrl, organizationUuid, membershipUuid }) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ReviewsDAL without a apiUrl argument.'
      );
    }

    this.organizationUuid = organizationUuid;
    this.membershipUuid = membershipUuid;
    this.apiUrl = apiUrl;
  }

  get() {
    return request.get(`${this.apiUrl}memberships/${this.membershipUuid}/dashboard/`).then(defaultHandle);
  }

  changeDate({ workflowUuid, date }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/audit-date/`,
      { audit_date_planned: date },
      config
    ).then(defaultHandle);
  }

  changeAuditStartDate({ workflowUuid, date }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/audit-start-date/`,
      { audit_date_actual_start: date },
      config
    ).then(defaultHandle);
  }

  changeAuditEndDate({ workflowUuid, date }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/audit-end-date/`,
      { audit_date_actual_end: date },
      config
    ).then(defaultHandle);
  }

  changeAuditor({ workflowUuid, auditor }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/auditor/`, { auditor }, config
    ).then(defaultHandle);
  }

  getAssessmentTypes(year) {
    if (!year) {
      year = 'All';
    }
    const url = `${this.apiUrl}memberships/${this.membershipUuid}/assessment-types/workflow-counts?year=${year}`;
    // const url = `${this.apiUrl}memberships/${this.membershipUuid}/assessment-types/workflow-counts`;
    return request.get(url, config).then(defaultHandle);
  }

  getAssessmentStates(year) {
    if (!year) {
      year = 'All';
    }
    const url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/statuses/?year=${year}`;
    // const url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/statuses/`;
    return request.get(url, config).then(defaultHandle);
  }

  getInternalInspectors(year) {
    if (!year) {
      year = 'All';
    }
    const url = `${this.apiUrl}memberships/${this.membershipUuid}/auditors/?year=${year}`;
    // const url = `${this.apiUrl}memberships/${this.membershipUuid}/auditors/`;
    return request.get(url, config).then(defaultHandle);
  }

  getFilteredAssessments(filters) {
    const filtersArray = [];
    if (filters.pageNumber) filtersArray.push(`page=${filters.pageNumber}`);
    if (filters.year) {
      filtersArray.push(`year=${filters.year}`);
    } else {
      filtersArray.push('year=All');
    }
    if (filters.startDate) filtersArray.push(`year_start=${filters.startDate}`);
    if (filters.endDate) filtersArray.push(`year_end=${filters.endDate}`);
    if (filters.assessmentTypes.length) filtersArray.push(`assessment_types=${filters.assessmentTypes.join(',')}`);
    if (filters.assessmentStates.length) filtersArray.push(`assessment_states=${filters.assessmentStates.join(',')}`);
    if (filters.internalInspectors.length) filtersArray.push(`inspectors=${filters.internalInspectors.join(',')}`);
    if (filters.search) filtersArray.push(`search_str=${filters.search}`);

    const url = `${this.apiUrl}memberships/${this.membershipUuid}/dashboard/?${filtersArray.join('&')}`;
    const encodedUrl = encodeURI(url);
    return request.get(encodedUrl, config).then(defaultHandle);
  }

  getOrganizationDetails(selectedOrganizationUuid) {
    const url = `${this.apiUrl}organizations/${selectedOrganizationUuid}/detail-for-auditor`;
    return request.get(url, config).then(defaultHandle).catch(defaultHandle);
  }
}

export default DashboardDAL;
