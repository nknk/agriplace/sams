import { csrftoken } from './csrfToken';

const request = require('axios');
const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if ((response.status < 400) && (response.status >= 200)) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

class JudgementsDAL {
  constructor({ apiUrl, organizationUuid, membershipUuid }) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of JudgementsDAL without a apiUrl argument.'
      );
    }

    this.organizationId = organizationUuid;
    this.membershipUuid = membershipUuid;
    this.apiUrl = apiUrl;
  }

  get({ assessmentUuid }) {
    return request.get(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/judgements/`, config
    ).then(defaultHandle);
  }

  delete({ assessmentUuid, judgementUuid }) {
    return request.delete(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/judgements/${judgementUuid}/`,
      config
    ).then(defaultHandle);
  }

  create({ assessmentUuid, judgements }) {
    return request.post(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/judgements/`,
      judgements,
      config
    ).then(defaultHandle);
  }

  update({ assessmentUuid, judgementUuid, judgement }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/judgements/${judgementUuid}/`,
      judgement,
      config
    ).then(defaultHandle);
  }

  confirmUnansweredJudgements({ assessmentUuid, questionnaireUuid }) {
    let url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}`;
    url = `${url}/questionnaires/${questionnaireUuid}/confirm-all-judgements/`;
    return request.post(url, null, config).then(defaultHandle).catch(defaultHandle);
  }
}

export default JudgementsDAL;
