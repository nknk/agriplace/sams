import { csrftoken } from './csrfToken';
const request = require('axios');

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

class ReviewsDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ReviewsDAL without a apiUrl argument.'
      );
    }

    this.organizationUuid = window.jsContext.ORGANIZATION_UUID;
    this.membershipUuid = window.jsContext.MEMBERSHIP_UUID;
    this.apiUrl = apiUrl;
  }

  get(assessmentUuid) {
    return request.get(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/auditor_overview/`
    ).then(defaultHandle);
  }

  changeDecision({ workflowUuid, decision }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/judgement/`, { judgement: decision }, config
    ).then(defaultHandle);
  }

  changeReaudit({ workflowUuid, reaudit }) {
    return request.put(`${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/reaudit/`, { reaudit }, config)
    .then(defaultHandle);
  }

  changeInternalDescription({ workflowUuid, text }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/internal-description/`, { text }, config
    ).then(defaultHandle);
  }

  changeOverviewDescription({ workflowUuid, text }) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowUuid}/overview-description/`, { text }, config
    ).then(defaultHandle);
  }
}

export default ReviewsDAL;
