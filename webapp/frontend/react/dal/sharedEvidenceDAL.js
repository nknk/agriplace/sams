import { csrftoken } from './csrfToken';
const request = require('axios');

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;

  if (response.status < 400) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }

  return promise;
};

class SharedEvidenceDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of SharedEvidenceDAL without a apiUrl argument.'
      );
    }

    this.apiUrl = apiUrl;
  }

  getSharingOrganizations(organizationUuid) {
    return request
      .get(`${this.apiUrl}organizations/${organizationUuid}/shared_attachments/`, config)
        .then(defaultHandle);
  }

  getListOfAttachments(organizationUuid, selectedOrganizationUuid) {
    let url = `${this.apiUrl}organizations/${organizationUuid}/`;
    url = `${url}shared_attachments/${selectedOrganizationUuid}/attachments/`;
    return request.get(url, config).then(defaultHandle);
  }

  copySharedAttachmentsToArchive(organizationUuid, selectedOrganizationUuid, attachmentUuids) {
    const post = {
      share_attachment_list_uuids: attachmentUuids,
      copy_to_archive: true,
    };
    let url = `${this.apiUrl}organizations/${organizationUuid}/shared_attachments/`;
    url = `${url}${selectedOrganizationUuid}/copy_to_archive/`;
    return request.post(url, post, config)
      .then(defaultHandle)
      .catch(defaultHandle);
  }

  getOpenAssessmentTypes(membershipUuid) {
    const url = `${this.apiUrl}memberships/${membershipUuid}/assessment-types/open/`;
    return request.get(url, config).then(defaultHandle);
  }

  copySharedAttachmentsToAssessments(organizationUuid, selectedOrganizationUuid, post) {
    let url = `${this.apiUrl}organizations/${organizationUuid}`;
    url = `${url}/shared_attachments/${selectedOrganizationUuid}/copy_to_assessments/`;
    return request.post(url, post, config)
      .then(defaultHandle)
      .catch(defaultHandle);
  }
}

export default SharedEvidenceDAL;
