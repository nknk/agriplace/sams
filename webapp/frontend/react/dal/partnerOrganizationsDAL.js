const request = require('axios');

import { csrftoken } from './csrfToken';

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if (response.status < 400) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

class PartnerOrganizationsDAL {

  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error('Not allowed! You\'ve tried to create an ' +
        'instance of PartnerOrganizationsDAL without a apiUrl argument.');
    }

    this.apiUrl = apiUrl;
  }

  getPartnerOrganizations(organizationUuid, pageNumber, pageSize, organizationSearchText) {
    const url = `${this.apiUrl}organizations/${organizationUuid}/partners/?`;

    const filters = [];
    if (pageNumber) { filters.push(`page=${pageNumber}`); }
    if (pageSize) { filters.push(`page_size=${pageSize}`); }
    if (organizationSearchText) { filters.push(`organization_name=${organizationSearchText}`); }

    const urlWithFilters = `${url}${filters.join('&')}`;

    return request.get(urlWithFilters, config)
      .then(defaultHandle);
  }
}

export default PartnerOrganizationsDAL;

