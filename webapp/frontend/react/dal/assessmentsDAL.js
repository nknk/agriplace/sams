import storejs from 'store';
import * as request from 'axios';
import { csrftoken, csrfSafeMethod } from './csrfToken';

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if ((response.status < 400) && (response.status >= 200)) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

//
// Assessments data layer
//
class AssessmentsDAL {

  // Params:
  //  API_URL: URL of API root, without trailing slash
  constructor(apiUrl, organizationUuid, membershipUuid) {
    this.apiUrl = apiUrl;
    this.organizationUuid = organizationUuid;
    this.membershipUuid = membershipUuid;
  }

  //
  // Creates an assessment
  //
  // Returns: assessment (full object)
  //
  createAssessment(organizationUuid, assessmentData) {
    return new Promise((resolve, reject) => {
      const url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/`;
      $.post(url, assessmentData).then(

        // success
        (resultData) => {
          resolve(resultData);
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/createAssessment', errorThrown);
          reject(errorThrown);
        }
      );
    });
  }

  //
  // Get an assessment by id
  //
  // Returns: promise
  //
  loadAssessment(assessmentId) {
    return new Promise((resolve, reject) => {
      const result = {};

      $.getJSON(
        `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/`
      ).then(

        // success
        (data) => {
          // data contains {assessment, answers, attachments}
          log.info('AssessmentsDAL/loadAssessment/assessmentFull get success', data);
          result.assessment = data.assessment;
          result.answers = data.answers;
          result.attachments = data.attachments;
          resolve(data);
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/loadAssessment', errorThrown);
          reject(errorThrown);
        });
    });
  }

  loadAssessmentState(assessmentId, questionnaireId) {
    log.info('AssessmentDAL/getIsAssessmentDone', assessmentId, questionnaireId);

    let url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/`;
    url = `${url}${assessmentId}/questionnaires/${questionnaireId}/state/`;

    return new Promise((resolve, reject) => {
      $.ajax({
        type: 'GET',
        url,
        contentType: 'application/json',
      }).then(

        // success
        (data) => {
          resolve({
            isComplete: data.is_complete,
            isSigned: data.is_signed,
          });
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/LoadAssessmentState', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  saveAssessmentState(assessmentId, questionnaireId, isComplete, isSigned) {
    log.info('AssessmentDAL/getIsAssessmentDone', assessmentId, questionnaireId);
    const data = {
      is_complete: isComplete,
      is_signed: isSigned,
    };

    let url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/`;
    url = `${url}${assessmentId}/questionnaires/${questionnaireId}/state/`;

    return new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        data: JSON.stringify(data),
        url,
        contentType: 'application/json',
      }).then(

        // success
        (response) => {
          resolve();
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/saveAssessmentState', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  //
  // Get an assessment type by Id
  //
  // Returns: promise
  //
  loadAssessmentType(assessmentTypeId, assessmentUuid) {
    let url = `${this.apiUrl}memberships/${this.membershipUuid}/assessment-types/`;
    url = `${url}${assessmentTypeId}/assessment/${assessmentUuid}/`;

    return new Promise((resolve, reject) => {
      $.getJSON(url).then(
        (data) => {
          log.info('AssessmentsDAL/loadAssessmentType get success', data);
          resolve(data);
        },
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/loadAssessmentType', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  loadPreviousAssessments(assessmentTypeId) {
    return new Promise((resolve, reject) => {
      $.getJSON(
        `${this.apiUrl}memberships/${this.membershipUuid}/assessments/types/${assessmentTypeId}/`
      ).then(
        (data) => {
          log.info('AssessmentsDAL/loadPreviousAssessments get success', data);
          resolve(data);
        },
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentDAL/loadPreviousAssessment', errorThrown);
          reject({ textStatus, errorThrown });
        }
      );
    });
  }

  //
  // Get a questionnaire type by Id
  //
  // Returns: promise
  //
  loadQuestionnaire(questionnaireId, assessmentId) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}memberships/${this.membershipUuid}/assessments/questionnaires/${questionnaireId}`,
        dataType: 'json',
      }).then(

        // success
        (data) => {
          log.info('loadQuestionnaireType get succeeded', questionnaireId);
          log.info(data);
          resolve(data);
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/loadQuestionnaire', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  //
  // this is a generic patch method for patch-saving assessment details
  //
  genericPatch(assessmentId, data) {
    return new Promise((resolve, reject) => {
      $.ajax({
        type: 'PATCH',
        data: JSON.stringify(data),
        url: `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/`,
        contentType: 'application/json',
      }).then(

        // success
        (response) => {
          resolve(response);
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL::genericPatch', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  createRegistrationInfo({ payload, workflowContextUUID }) {
    return new Promise((resolve, reject) => {
      $.ajax({
        type: 'POST',
        data: JSON.stringify(payload),
        url: `${this.apiUrl}memberships/${this.membershipUuid}/workflows/${workflowContextUUID}/agreements/`,
        contentType: 'application/json',
      }).then(

        // success
        (response) => {
          resolve();
        },

        // fail
        (xhr, textStatus, errorThrown) => {
          log.error('AssessmentsDAL/saveAssessmentState', errorThrown);
          reject({ textStatus, errorThrown });
        });
    });
  }

  setEvidenceReuseAnswer({ assessmentId, val }) {
    storejs.set('evidenceReuseAnswer', { [assessmentId]: val });
  }

  getEvidenceReuseAnswer(assessmentId) {
    return storejs.get('evidenceReuseAnswer', {})[assessmentId];
  }

  loadAssessmentReport(assessmentUuid) {
    const url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentUuid}/report/`;
    return request.get(url, config).then(defaultHandle);
  }

  assessmentReuse(sourceAssessmentId, targetAssessmentId) {
    let url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/`;
    url = `${url}${sourceAssessmentId}/target/${targetAssessmentId}/`;

    return request.post(url, null, config).then(defaultHandle);
  }

  assessmentReuseStatus(assessmentUuid) {
    let url = `${this.apiUrl}memberships/${this.membershipUuid}/assessments/`;
    url = `${url}${assessmentUuid}/assessment-reuse-status/`;

    return request.get(url, config).then(defaultHandle);
  }
}

export default AssessmentsDAL;
