import { csrftoken } from './csrfToken';
const request = require('axios');

const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  let promise;
  if (response.status < 400) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

class HistoricalAttachmentDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.organizationId = window.jsContext.ORGANIZATION_UUID;
    this.apiUrl = apiUrl;
  }

  get(assessmentId, documentId) {
    return request
      .get(
        `${this.apiUrl}organizations/${this.organizationId}/assessments/${assessmentId}/document-types/${documentId}/`
      ).then(defaultHandle);
  }

  create(assessmentId, documentId, documentLinks) {
    const attachments = { document_links: documentLinks };
    return request
      .post(
        `${this.apiUrl}organizations/${this.organizationId}/assessments/${assessmentId}/document-types/${documentId}/`,
        attachments,
        config)
      .then(defaultHandle);
  }
}

export default HistoricalAttachmentDAL;
