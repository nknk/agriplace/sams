import { csrftoken } from './csrfToken';

const request = require('axios');
const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

class ProductsDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.organizationUuid = window.jsContext.ORGANIZATION_UUID;
    this.apiUrl = apiUrl;
  }

  getProductTypes() {
    return request.get(`${this.apiUrl}organizations/product-types/`)
    .then(defaultHandle);
  }

  getProducts({ organizationUuid = this.organizationUuid }) {
    return request.get(
      `${this.apiUrl}organizations/${organizationUuid}/products/`
    ).then(defaultHandle);
  }

  deleteProduct({ uuid }) {
    return request.delete(
      `${this.apiUrl}organizations/${this.organizationUuid}/products/${uuid}/`, config
    ).then(defaultHandle);
  }

  createProduct({ products }) {
    return request.post(
      `${this.apiUrl}organizations/${this.organizationUuid}/products/`, products, config
    ).then(defaultHandle);
  }

  updateProduct({ productUuid, product }) {
    return request.put(
      `${this.apiUrl}organizations/${this.organizationUuid}/products/${productUuid}/`, product, config
    ).then(defaultHandle);
  }
}

export default ProductsDAL;
