import { csrftoken } from './csrfToken';

const request = require('axios');
const config = {
  headers: { 'X-CSRFToken': csrftoken },
};

const defaultHandle = (response) => {
  if (response.status < 400) {
    return Promise.resolve(response.data);
  }

  return Promise.reject(response.data);
};

class ProductDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.organizationId = window.jsContext.ORGANIZATION_UUID;
    this.membershipUuid = window.jsContext.MEMBERSHIP_UUID;
    this.apiUrl = apiUrl;
  }

  get(assessmentId) {
    return request.get(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/products`
    ).then(defaultHandle);
  }

  create(assessmentId, productIds) {
    return request.post(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/products/`, productIds, config
    ).then(defaultHandle);
  }

  delete(assessmentId, productId) {
    return request.delete(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/products/${productId}/`, config
    ).then(defaultHandle);
  }

  getProductDependentQuestionnaires(assessmentId, productIds) {
    return request.post(
        `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/products/questionnaires/`, productIds, config
    ).then(defaultHandle);
  }
}

export default ProductDAL;
