import { csrftoken } from './csrfToken';
import * as request from 'axios';

const config = {
  headers: {
    'X-CSRFToken': csrftoken,
  },
};

const defaultHandle = (response) => {
  let promise;
  if ((response.status < 400) && (response.status >= 200)) {
    promise = Promise.resolve(response.data);
  } else {
    promise = Promise.reject(response.data);
  }
  return promise;
};

class AnswersDAL {
  constructor({ apiUrl, organizationUuid, membershipUuid }) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of AnswersDAL without a apiUrl argument.'
      );
    }

    this.organizationUuid = organizationUuid;
    this.membershipUuid = membershipUuid;
    this.apiUrl = apiUrl;
  }

  create(answer, assessmentId) {
    return request.post(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/answer/`, answer, config
    ).then(defaultHandle).catch(defaultHandle);
  }

  createAnswers(answers, assessmentId) {
    return request.post(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/answers/`, answers, config
    ).then(defaultHandle).catch(defaultHandle);
  }

  update(answer, assessmentId, answerId) {
    return request.put(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/answers/${answerId}/`,
      answer, config
    ).then(defaultHandle).catch(defaultHandle);
  }

  delete(assessmentId, answerId) {
    return request.delete(
      `${this.apiUrl}memberships/${this.membershipUuid}/assessments/${assessmentId}/answers/${answerId}/`, config
    ).then(defaultHandle).catch(defaultHandle);
  }
}

export default AnswersDAL;
