import { csrftoken, csrfSafeMethod } from './csrfToken';

$.ajaxSetup({
  beforeSend(xhr, settings) {
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader('X-CSRFToken', csrftoken);
    }
  },
});

class HelpLinksDAL {
  constructor(apiUrl) {
    if (!apiUrl) {
      throw new Error(
        'Not allowed! You\'ve tried to create an instance of ProductDAL without a apiUrl argument.'
      );
    }

    this.apiUrl = apiUrl;
  }

  get(assessmentUuid) {
    const actionPromise = new Promise((resolve, reject) => {
      $.ajax({
        url: `${this.apiUrl}app/help-text-blocks/?assessment-uuid=${assessmentUuid}`,
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
      }).then(
        (data) => {
          log.info('Get helpLinks succeeded');
          resolve(data);
        },

        (xhr, textStatus, errorThrown) => {
          log.error('Get helpLinks failed.');
          reject({ textStatus, errorThrown });
        });
    });

    return actionPromise;
  }
}

export default HelpLinksDAL;
