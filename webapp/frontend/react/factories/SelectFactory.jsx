import 'react-select/dist/default.css';
import './SelectFactory.less';

import Select from 'react-select';
import t from 'tcomb-form';

class SelectFactory extends t.form.Select {

  getTemplate() {
    return (locals) => { // <- locals contains the "recipe" to build the UI
      // handle error status
      let className = 'form-group';
      if (locals.hasError) {
        className += ' has-error';
      }

      // translate the option model from tcomb to react-select
      const options = locals.options.map(({ value, text }) => ({ value, label: text }));

      return (
        <div className={className}>
          {
            locals.label ? (
              <label className="control-label">{locals.label}</label>
            ) : null
          }
          <Select
            className="form-control fix-bootstrap-validation select-factory"
            placeholder={locals.attrs.placeholder ? locals.attrs.placeholder : _t('Select')}
            name={locals.attrs.name}
            value={locals.value}
            options={options}
            onChange={locals.onChange}
          />
        </div>
      );
    };
  }

}

export default SelectFactory;
