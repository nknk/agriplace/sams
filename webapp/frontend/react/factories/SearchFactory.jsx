import 'react-select/dist/default.css';
import './SelectFactory.less';
import t from 'tcomb-form';
const Bootstrap = require('react-bootstrap');

class SearchFactory extends t.form.Textbox {

  getTemplate() {
    return (locals) => {
      // handle error status
      let className = 'form-group search-box-with-icon';
      if (locals.hasError) {
        className += ' has-error';
      }

      return (
        <div className={className}>
          <Bootstrap.Input
            type="text"
            placeholder={locals.attrs.placeholder ? locals.attrs.placeholder : ''}
            onChange={(e) => locals.onChange(e.target.value)}
          />
          <span className="icon fa fa-search fa-lg" onClick={locals.attrs.onSearchClick} />
        </div>
      );
    };
  }

}

export default SearchFactory;
