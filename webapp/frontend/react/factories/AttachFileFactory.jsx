import './SelectFactory.less';
import t from 'tcomb-form';
import './AttachFileFactory.less';
const Bootstrap = require('react-bootstrap');
const { Button, Input } = Bootstrap;
import _ from 'lodash';

class AttachFileFactory extends t.form.Component {
  initialize() {
    this._onFileSelect = this._onFileSelect.bind(this);
  }

  removeExtension(filename) {
    let result = filename;
    const lastDotPosition = filename.lastIndexOf('.');
    if (lastDotPosition !== -1) {
      result = filename.substr(0, lastDotPosition);
    }
    return result;
  }

  getFileExtension(filename) {
    let result = filename;
    const lastDotPosition = filename.lastIndexOf('.');
    if (lastDotPosition !== -1) {
      result = filename.substr(lastDotPosition + 1);
    }
    return result;
  }

  _handleFile({ event, cb, allowedExtensions, sizeLimit }) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      const fileExtension = this.getFileExtension(file.name);
      const isExtensionInvalid = !_.includes(allowedExtensions, fileExtension);

      const value = {
        fileName: file.name,
        fileSize: file.size,
        dataUrl: e.target.result,
        title: this.removeExtension(file.name),
        sizeLimitExceeded: sizeLimit && (file.size > sizeLimit),
        isExtensionInvalid,
      };
      cb(value);
    };

    reader.readAsDataURL(file);
  }

  _onFileSelect() {
    this.fileInput.refs.input.getDOMNode().click();
  }

  getTemplate() {
    this.initialize();
    return (locals) => {
      // handle error status
      let className = 'form-group attach-file-factory';
      if (locals.hasError) {
        className += ' has-error';
      }

      const fileName = locals.value && locals.value.fileName;
      const fileUrl = locals.value && locals.value.fileUrl;
      const allowedExtensions = (locals.config && locals.config.allowedExtensions) || [];
      const allowedMimeTypes = (locals.config && locals.config.allowedMimeTypes) || [];
      const sizeLimit = locals.config && locals.config.sizeLimit;
      const onChange = locals.onChange;

      const isSizeLimitExceeded = locals.value && locals.value.sizeLimitExceeded;
      const isExtensionInvalid = locals.value && locals.value.isExtensionInvalid;

      const fileNameBox = fileName && fileUrl ? (
        <a target="_blank" href={fileUrl}>
          {fileName}
        </a>
      ) : (
        <div>
          {fileName}
        </div>
      );

      const acceptInputExtensions = allowedExtensions.length ? (
        allowedExtensions.map(ext => (
          `.${ext}`
        )).join(', ')
      ) : null;

      const acceptInputMimeTypes = allowedMimeTypes.length ? (
        allowedMimeTypes.map(mime => (
          `.${mime}`
        )).join(', ')
      ) : null;

      const acceptInput = acceptInputExtensions && acceptInputMimeTypes ? (
        [acceptInputMimeTypes, acceptInputExtensions].join(', ')
      ) : '*';

      return (
        <div className={className}>
          {
            fileName ? (
              fileNameBox
            ) : (
              <div>
                <Input
                  className="hidden"
                  type="file"
                  accept={`"${acceptInput}"`}
                  ref={(_input) => { this.fileInput = _input; }}
                  onChange={(event) => this._handleFile({
                    event,
                    cb: onChange,
                    allowedExtensions,
                    sizeLimit,
                  })}
                />
                <Button onClick={this._onFileSelect}>
                  {_t('Choose file')}
                </Button>
                &emsp;
                <i>{_t('No file chosen')}</i>
              </div>
            )
          }
          {
            locals.hasError && locals.error ? (
              <div className="error">
                {isExtensionInvalid && locals.error.invalidExtensionMessage}
                {isExtensionInvalid && isSizeLimitExceeded && <br />}
                {isSizeLimitExceeded && locals.error.limitExceedMessage}
              </div>
            ) : null
          }
        </div>
      );
    };
  }

}

export default AttachFileFactory;
