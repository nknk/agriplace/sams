

//
// GeneralEventsMixin is a helper for setting custom listeners.
//
// Multiple event types can be set at once for a specific eventManager.
//
// Main advantage of using this mixin is that you don't have to worry about putting the listeners in the
// right place and cleaning up the listeners during the unmount stage.
//
// Manually removing one of the set listeners is not implemented.
//
// Arguments:
//
//  <object>        eventManager  : any registered eventManager
//  <string/array>  events        : one or more event names put in a space seperated string or array of strings
//  <function>      callback      : the callback to be fired on occurence of any of the given events
//


var GeneralEventsMixin = {

  _generalEvents: [],


  addGeneralEvent(eventManager, events, callback) {
    // accept arrays, strings..
    events = tools.intoArray(events);

    this._generalEvents.push({
      eventManager,
      events,
      callback,
    });

    for (var index in events) {
      if ({}.hasOwnProperty.call(events, index)) {
        eventManager.on(events[index], callback);
      }
    }
  },


  componentWillUnmount() {
    // remove callback on each event type
    for (var index in this._generalEvents) {
      if ({}.hasOwnProperty.call(this._generalEvents, index)) {
        for (var event in this._generalEvents[index].events) {
          if ({}.hasOwnProperty.call(this._generalEvents[index].events, event)) {
            this._generalEvents[index].eventManager.removeListener(event, this._generalEvents.callback);
          }
        }
      }
    }
  },

};

module.exports = GeneralEventsMixin;
