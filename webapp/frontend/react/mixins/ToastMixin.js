

//
//  ToastMixin depends:
//    - toastr, a jQuery plugin, see API at: http://codeseven.github.io/toastr/demo.html
//
//  Usage:
//
//    - Add ToastMixin (is global) to your component
//
//    - You can override default ToastMixin 'toastr' settings by calling ToastMixin with
//       a toastr settings object in your component like so:
//        mixins: [
//          ToastMixin({
//            closeButton: true,
//            position: 'toast-bottom-left'
//          })
//      ]
//
//    - emit (custom)events from the store like: this.emit('update')
//
//    - start listening to these events in your component with:
//
//        this.getFlux().store('SomeStore').on( 'save', ()=> {
//          this.toast( 'Your file has been saved.');
//        });
//
//  To directly show a toast message without StoreWatchMixin, simply use:
//    this.toast( 'your message' );
//
//  The message argument for this.toast or the message key in addToast can be given a toastr settings
//  object as well so you can override any settings to create a specific toast.
//
//  The title and message can be passed as a function returning a string to show 'dynamic' messages,
//  they will be resolved in the mixin.

var toastr = require('toastr');


var ToastMixin = (defaultsOverride) =>
  ({
    toast: (message, title, _options) => {
      // these default settings can be overridden
      var options = {
        type: 'info',
        title: '',
        message: '',
        closeButton: false,
        debug: false,
        newestOnTop: true,
        progressBar: false,
        positionClass: 'toast-top-right',
        preventDuplicates: true,
        onclick: null,
        showDuration: '300',
        hideDuration: '1000',
        timeOut: '5000',
        extendedTimeOut: '1000',
        showEasing: 'swing',
        hideEasing: 'linear',
        showMethod: 'fadeIn',
        hideMethod: 'fadeOut',
      };

      // you can override the default options for a component
      if (defaultsOverride) {
        _.extend(options, defaultsOverride);
      }

      // accept options object, passing all options needed
      if (tools.isObject(message)) {
        _.extend(options, message);
      } else {
      // accept simple toasts with a message and title as strings
        _.extend(options, {
          type: options.type || 'info',
          title: title || options.title || '',
          message: message || '',
        });
      }

      toastr.options = options;
      toastr[options.type](options.message, options.title);
      return options;
    },
  });

ToastMixin.componentWillMount = () => {
  throw new Error('You are trying to use ToastMixin as an Object, but it is a Function ' +
                    'that takes an object with custom settings for toastr. Add braces ' +
                    'and pass a settings object if you need to.');
};


module.exports = ToastMixin;
