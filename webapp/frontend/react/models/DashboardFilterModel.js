export class DashboardFilter {
  constructor({ code, path, predicate, value }) {
    this.code = code;
    this.path = path;
    this.predicate = predicate.bind(this);
    this.value = value;
  }

  getFilter(assessments) {
    const value = this.value.map(item => _.assign(item, { count: 0 }));
    const { path } = this;
    const filter = assessments.reduce((result, assessment) => {
      const item = result.find(currentItem => currentItem.code === _.get(assessment, path));
      item.count++;
      return value;
    }, value);

    return filter;
  }

  getLabel(itemCode) {
    return this.value.find(item => item.code === itemCode).label;
  }
}
