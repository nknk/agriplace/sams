const models = require('../models/models.js');
import assessmentStatistics from '../components/AssessmentResults/assessmentStatistics.js';


//
// Assessment model
//
// Holds all assessment data and logic.
//
class AssessmentModel {

  constructor(payload) {
    payload = payload || {};
    // do not set directly, always go through setAssessmentType()
    this.assessmentType = payload.assessmentType;
    this.uuid = payload.uuid;
    this.permissions = payload.permissions;
    this.assessment = payload.assessment || {};
    this.answers = payload.answers || {};
    this.questionnaireState = payload.questionnaireState || {};
    this.overview = payload.overview || {};
    this.products = payload.products || {};
    this.attachmentLinks = payload.attachmentLinks || {};
    this.judgementMode = payload.judgementMode || 'not_available';
    this.isEvidenceReuseAnswered = payload.isEvidenceReuseAnswered || false;
    this.documentTypeReuseAttachments = this.formatReuseAttachmentsIntoDict(payload.document_type_reuse_attachments);
    this.isReportVisible = payload.isReportVisible;
    this.assessmentReuseStatus = payload.assessmentReuseStatus;
    // We are not saving questionnaire element states to the database anymore
    // but we need to keep them in the assessment model because some elements
    // depend on other elements' state (visibility).
    this.elementStates = {};
  }

  getState() {
    return {
      answers: this.answers,
      questionnaireState: this.questionnaireState,
      attachmentLinks: this.attachmentLinks,
      elementStates: this.elementStates,
    };
  }

  formatReuseAttachmentsIntoDict(documentTypeReuseAttachments) {
    documentTypeReuseAttachments = documentTypeReuseAttachments || [];
    return documentTypeReuseAttachments.reduce((prev, cur) => {
      prev[cur.document_type_uuid] = cur.attachment_count;
      return prev;
    }, {});
  }

  setAssessmentType(assessmentType) {
    this.assessmentType = assessmentType;

    // Once we have an assessment type, calculate triggers.
    // We could have triggered answers and visibility that weren't saved.
    _.each(assessmentType.questionnaires, (questionnaire) => {
      this.calculateElementStates(questionnaire.uuid);
    });
  }

  //
  // Gets or generates a state for the given element.
  // If no state exists in our collection, return a state based on this element's defaults.
  // If the parent element is hidden, always return isVisible=false
  //
  getElementState(elementUuid) {
    const element = this.assessmentType.getElement(elementUuid);
    let state = this.elementStates[elementUuid];

    if (!state) {
      if (!element) {
        return null;
        // throw new Error(`Cannot get state for unknown element ${elementUuid}`);
      }
      state = new models.QuestionnaireElementState({
        elementUuid: element.uuid,
        isVisible: element.is_initially_visible,
      });
    }

    // Check parent visibility
    // If the parent is not visible, override our state to also be invisible.
    if (element.parent_element) {
      const parentState = this.getElementState(element.parent_element);
      if (!parentState.isVisible) {
        state.isVisible = false;
      }
    }

    return state;
  }

  //
  // Checks whether element is visible also looking at parent elements recursively.
  //
  // Check order:
  //  parent hidden = false
  //  own QuestionState is hidden = false
  //  own is_initially_visible is false? = false
  //  else = true
  //  unknown elementUuid = true (and warn)
  //
  isElementVisible(elementUuid) {
    const elementState = this.getElementState(elementUuid);
    const element = this.assessmentType.getElement(elementUuid);

    if (!element) {
      // warn
      log(`QuestionnaireEditor/isElementVisible: Could not find element ${elementUuid}.`);
      return true;
    }
    let show = element.is_initially_visible;

    if (elementState) {
      show = elementState.isVisible;
    }

    if (element.parent_element) {
      const isParentVisible = this.isElementVisible(element.parent_element);
      show = show && isParentVisible;
    }

    return show;
  }

  isQuestionnaireDone(questionnaireUuid) {
    const questionnaireState = this.questionnaireState[questionnaireUuid];
    const requiresSignature = this.assessmentType.questionnaires[questionnaireUuid].requires_signature;

    if (requiresSignature) {
      return questionnaireState.isSigned;
    }

    const visibleQuestions = _.values(this.assessmentType.questionnaires[questionnaireUuid].questions)
      .filter((question) => this.isElementVisible(question.uuid)).map((question) => question.uuid);
    const triggerFilteredQuestions = visibleQuestions.filter(questionUuid => !this.isTriggerQuestion(questionUuid));
    const incompleteQuestions = triggerFilteredQuestions.filter((questionUuid) => !this.isQuestionDone(questionUuid));
    const isDone = incompleteQuestions.length === 0;

    return isDone;
  }

  isQuestionaireCompliant(questionnaireUuid) {
    let isCompliant = true;
    if (~['globalgap_ifa', 'globalgap_ifa_v5'].indexOf(this.assessmentType.meta.code)) {
      const questionnaire = this.assessmentType.questionnaires[questionnaireUuid];
      const questions = _.pick(
        questionnaire.questions,
        question => !this.isTriggerQuestion(question.uuid) && this.getElementState(question.uuid).isVisible
      );
      const calcs = assessmentStatistics.getLevelCalculations(questionnaire, 'no', questions, this.answers);
      // If level code contains 'major'. Has to be refactored with setting from admin panel.
      isCompliant = calcs.find(x => !!~x.code.indexOf('major')).count === 0;
    }
    return isCompliant;
  }

  isAssessmentComplete() {
    const questionnaireUuids = this.getRequiredQuestionnaires();
    const isAllQuestionnairesComplete = questionnaireUuids.reduce((result, current) =>
      result && this.isQuestionnaireDone(current)
    , true);

    return isAllQuestionnairesComplete;
  }

  isAssessmentCompliant() {
    const questionnaireUuids = this.getRequiredQuestionnaires();
    const isAllQuestionnairesCompliant = questionnaireUuids.reduce((result, current) =>
      result && this.isQuestionaireCompliant(current)
    , true);

    return isAllQuestionnairesCompliant;
  }

  getRequiredQuestionnaires() {
    return Object.keys(this.assessmentType.questionnaires).filter(
      x => ~this.assessment.completeness_required_for.indexOf(x)
    );
  }

  getCompleteQuestionsCountByQuestionnaire(uuid) {
    const questionnaire = this.assessmentType.questionnaires[uuid];
    const questions = _.pick(
      questionnaire.questions,
      question => !this.isTriggerQuestion(question.uuid) && this.getElementState(question.uuid).isVisible
    );

    const answeredQuestions = _.values(questions).filter((question) => this.isQuestionDone(question.uuid));

    return answeredQuestions.length;
  }

  getIsJustificationRequired(questionUuid) {
    const question = this.assessmentType.questions[questionUuid];
    const answer = this.getAnswers()[questionUuid] || {};
    // Is a justification required?
    // Look at PossibleAnswer.justification_required!
    let isJustificationRequired = false;
    const possibleAnswers = this.assessmentType.possibleAnswerSets[question.possible_answer_set].possible_answers;
    _.each(possibleAnswers, (possibleAnswer) => {  // eslint-disable-line consistent-return
      if (possibleAnswer.value === answer.value) {
        // we found the currently selected possible answer
        isJustificationRequired = possibleAnswer.requires_justification;
        return false; // break
      }
    });

    return isJustificationRequired;
  }

  //
  // Determines whether a question is done or not
  //
  isQuestionDone(questionUuid) {
    const answer = this.getAnswers()[questionUuid];
    const value = answer ? answer.value : null;
    if (!value) {
      return false;
    }

    const isJustificationRequired = this.getIsJustificationRequired(questionUuid);

    if (isJustificationRequired && value) {
      return !!(value && answer.justification);
    }

    return !!value;
  }

  getAnswers() {
    return _.indexBy(this.answers, 'question');
  }

  //
  // Changes answer and makes related changes, triggers, etc.
  //
  updateAnswer(questionUuid, answer) {
    this.answers[questionUuid].value = answer.value;
    this.answers[questionUuid].justification = answer.justification;
  }

  createAnswer(questionUuid, answer) {
    this.answers[questionUuid] = answer;
    this.answers[questionUuid].question = questionUuid;
  }

  removeAnswer(questionUuid) {
    delete this.answers[questionUuid];
  }

  //
  // Gets the triggered state and answer for this element
  //
  getTriggeredElementData(elementUuid) {
    // fire all the triggers that change this question
    let result = false;

    // These are all incoming triggers.
    const triggers = this.assessmentType.triggersByTargetElement[elementUuid];
    // Fire the ones with satisfied conditions.
    _.forEach(triggers, (trigger) => {
      // Careful: _.every() returns TRUE for empty collections
      const allConditionsHappy = _.every(trigger.conditions, (condition) => {
        let isConditionHappy = false;

        // Ignore this trigger if the source element is not visible
        // Only visible elements can trigger others.
        const sourceElementState = this.getElementState(condition.source_question);
        if (!!sourceElementState && !sourceElementState.isVisible) {
          return false;
        }

        switch (condition.source_property_name) {
          // The only possible source property is now 'value'
          // but we can add more later (e.g. trigger on justification)
        case 'value': // eslint-disable-line no-case-declarations
          const sourceQuestionAnswer = this.getAnswers()[condition.source_question];
          if (sourceQuestionAnswer && sourceQuestionAnswer.value === condition.source_value) {
            isConditionHappy = true;
          }
          break;
        default:
            // log(`WARNING: Unrecognized trigger condition source property name ${condition.source_property_name}
            // in condition ${condition.uuid}, trigger ${trigger.uuid}`);
        }
        return isConditionHappy;
      });
      if (trigger.conditions.length && allConditionsHappy) {
        result = {
          value: undefined,
          justification: undefined,
          isVisible: undefined,
        };
        // Fire trigger
        if (trigger.target_visibility !== null) {
          result.isVisible = trigger.target_visibility;
        }
        if (trigger.target_value !== null) {
          result.value = trigger.target_value;
        }
        if (trigger.target_justification) {
          result.justification = trigger.target_justification;
        }
      }
    });

    return result;
  }

  calculateElementStates(questionnaireUuid) {
    // All or nothing!
    const questionnaire = this.assessmentType.questionnaires[questionnaireUuid];
    const answers = this.getAnswers();
    // The questionnaires we are limiting ourselves to.
    let questionnaires;
    if (questionnaire) {
      questionnaires = [questionnaire];
    } else {
      questionnaires = this.assessmentType.questionnaires;
    }
    // TODO Do we need to sort elements or are they already sorted by API?
    // TODO REWRITE! questionnaire shadowing!
    _.forEach(questionnaires, (questionnaire) => { // eslint-disable-line no-shadow
      _.forEach(questionnaire.elements, (element) => {
        const triggeredData = this.getTriggeredElementData(element.uuid);
        const elementState = this.getElementState(element.uuid);

        const defaultVisibility = element.is_initially_visible;
        const visibility = triggeredData ? triggeredData.isVisible : defaultVisibility;
        let newState = new models.QuestionnaireElementState({
          elementUuid: element.uuid,
          isVisible: visibility,
        });

        // Apply answer (value and/or justification)

        // My current answer or an empty one
        const answer = answers[element.uuid] || new models.Answer();

        // if triggered data with value
        //    then set value
        // else if no triggered data with value
        //    if this element has an automatic value
        //        then delete its value
        if (triggeredData && triggeredData.value) {
          const isChanged = (triggeredData.value !== answer.value) ||
            // Justification might be undefined
            ((triggeredData.justification || '') !== (answer.justification || ''));
          // We have a forced automatic value from a trigger
          // set the value+justification
          newState.isAutomaticAnswer = true;
          answer.question = element.uuid;
          answer.value = triggeredData.value;
          answer.justification = triggeredData.justification;
          if (!answer.uuid) {
            this.createAnswer(element.uuid, answer);
          } else if (isChanged) {
            this.updateAnswer(element.uuid, answer);
          }
        } else {
          // This element doesn't have a forced answer from a trigger...
          if (elementState.isAutomaticAnswer) {
            // ...but its current value WAS forced by a trigger
            // ...so we can clear it
            this.removeAnswer(element.uuid);
            // and we can clear the state too
            newState = undefined;
          }
        }

        this.elementStates[element.uuid] = newState;
      });
    });
  }

  getTriggerredAnswers() {
    const toCreate = [];
    const toUpdate = [];
    const toDelete = [];
    const all = [];

    const answers = this.getAnswers();
    const questionnaires = this.assessmentType.questionnaires;

    _.forEach(questionnaires, (questionnaire) => { // eslint-disable-line no-shadow
      _.forEach(questionnaire.elements, (element) => {
        const triggeredData = this.getTriggeredElementData(element.uuid);
        const elementState = this.getElementState(element.uuid);

        const defaultVisibility = element.is_initially_visible;
        const visibility = triggeredData ? triggeredData.isVisible : defaultVisibility;
        let newState = new models.QuestionnaireElementState({
          elementUuid: element.uuid,
          isVisible: visibility,
        });

        const answer = answers[element.uuid] || new models.Answer();

        if (triggeredData && triggeredData.value) {
          const isChanged = (triggeredData.value !== answer.value) ||
            ((triggeredData.justification || '') !== (answer.justification || ''));

          newState.isAutomaticAnswer = true;
          answer.question = element.uuid;
          answer.value = triggeredData.value;
          answer.justification = triggeredData.justification;

          if (!answer.uuid) {
            toCreate.push(answer);
            all.push(answer);
          } else if (isChanged) {
            toUpdate.push(answer);
            all.push(answer);
          }
        } else {
          if (elementState.isAutomaticAnswer) {
            toDelete.push(answer);
            all.push(answer);
            newState = undefined;
          }
        }
        this.elementStates[element.uuid] = newState;
      });
    });

    return { toCreate, toUpdate, toDelete, all };
  }

  getAnswersToUpdate(documentTypeUuid) {
    const toCreate = [];
    const toUpdate = [];
    const toDelete = [];
    const all = [];
    const documentType = this.assessmentType.documentTypes[documentTypeUuid];

    if (!!documentType && !documentType.is_optional) {
      const compeletedAttachmentLinks = _.filter(
        this.attachmentLinks[documentTypeUuid],
        (attachmentLink) => attachmentLink.is_done
      );
      const isDocumentTypeDone = !!compeletedAttachmentLinks.length;

      const questionsOfThisDocType = _.filter(this.assessmentType.questions, (question) => (
        ~question.document_types_attachments.indexOf(documentTypeUuid)
      ));

      questionsOfThisDocType.forEach((question) => {
        const existingAnswer = _.clone(this.answers[question.uuid]);
        if (existingAnswer && !isDocumentTypeDone) {
          if (existingAnswer.value === question.evidence_default_answer
            && (existingAnswer.justification === question.evidence_default_justification)
            || (existingAnswer.justification === _t('See attachment'))
          ) {
            toDelete.push(existingAnswer);
            all.push(existingAnswer);
          }
        } else if (existingAnswer && isDocumentTypeDone && existingAnswer.uuid) {
          existingAnswer.value = question.evidence_default_answer;
          if (!existingAnswer.justification) {
            existingAnswer.justification = question.evidence_default_justification || _t('See attachment');
          }
          toUpdate.push(existingAnswer);
          all.push(existingAnswer);
        } else if (existingAnswer && isDocumentTypeDone && !existingAnswer.uuid) {
          existingAnswer.value = question.evidence_default_answer;
          if (!existingAnswer.justification) {
            existingAnswer.justification = question.evidence_default_justification || _t('See attachment');
          }
          toCreate.push(existingAnswer);
          all.push(existingAnswer);
        } else if (!existingAnswer && isDocumentTypeDone) {
          const newAnswer = {
            question: question.uuid,
            value: question.evidence_default_answer,
            justification: question.evidence_default_justification || _t('See attachment'),
          };
          toCreate.push(newAnswer);
          all.push(newAnswer);
        }
      });
    }

    return { toCreate, toUpdate, toDelete, all };
  }

  isNew() {
    return _.isEmpty(this.answers) && _.isEmpty(this.attachmentLinks);
  }

  isTriggerQuestion(questionUuid) {
    return this.assessmentType.triggersBySourceQuestion[questionUuid];
  }

}

_.forEach(assessmentStatistics, (method, key) => { AssessmentModel.prototype[key] = method; });

module.exports = AssessmentModel;
