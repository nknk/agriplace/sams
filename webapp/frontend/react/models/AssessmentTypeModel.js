//
// Assessment type model
//
// Holds all assessment content and logic.
//
class AssessmentTypeModel {

  constructor() {
    this.documentTypes = {};
    this.elementsByUuid = {};
    this.headings = {};
    this.levels = {};
    this.possibleAssessmentSets = {};
    this.questionnaires = {};
    this.questions = {};
    this.triggers = {};
    this.sections = [];
  }

  //
  // Get element by uuid if it exists in any of this assessmentType's questionnaires.
  //
  getElement(elementUuid) {
    let result = false;
    _.forEach(this.questionnaires, (questionnaire) => { // eslint-disable-line consistent-return
      const element = questionnaire.elementsByUuid[elementUuid];
      if (element) {
        result = element;

        // break out of foreach
        return false;
      }
    });
    return result;
  }

  getQuestionLevel(questionLevelUuid) {
    let result = false;
    _.forEach(this.questionnaires, (questionnaire) => { // eslint-disable-line consistent-return
      const questionLevel = questionnaire.question_levels[questionLevelUuid];
      if (questionLevel) {
        result = questionLevel;

        // break out of foreach
        return false;
      }
    });
    return result;
  }

  getQuestionnaire(questionUuid) {
    return _.find(this.questionnaires, (questionnaire) => !!questionnaire.elementsByUuid[questionUuid]);
  }

  load(assessmentTypeData) {
    this.questionnaires = assessmentTypeData.questionnaires;
    this.documentTypes = assessmentTypeData.document_types;
    this.sections = assessmentTypeData.sections;
    this.possibleAnswerSets = assessmentTypeData.possible_answer_sets;
    this.triggers = assessmentTypeData.triggers;
    this.meta = {
      kind: assessmentTypeData.kind,
      code: assessmentTypeData.code,
    };
    this.headings = {};
    this.questions = {};
    this.elementsByUuid = {};

    // Build objects indexing all questions and all elements in this assessment type by uuid
    // so they can be accessed without knowing
    _.each(this.questionnaires, (questionnaire) => {
      questionnaire.elementsByUuid = {};
      questionnaire.headings = {};
      questionnaire.questions = {};

      // Question levels
      _.each(questionnaire.question_levels, (level) => {
        this.levels[level.uuid] = level;
      });

      // Elements
      _.each(questionnaire.elements, (element) => {
        this.elementsByUuid[element.uuid] = element;
        questionnaire.elementsByUuid[element.uuid] = element;

        if (element.element_type === 'heading') {
          this.headings[element.uuid] = element;
          questionnaire.headings[element.uuid] = element;
        } else if (element.element_type === 'question') {
          this.questions[element.uuid] = element;
          questionnaire.questions[element.uuid] = element;
        }
      });  // element
    });  // questionnaire

    // Build object indexing triggers by source question uuid so we can find them directly
    this.triggersBySourceQuestion = {};
    this.triggersByTargetElement = {};

    _.forEach(this.triggers, (trigger) => {
      // For triggers by target question:
      // Make sure we have a trigger array for this (target) question.
      this.triggersByTargetElement[trigger.target_element] =
        this.triggersByTargetElement[trigger.target_element] || [];

      // and append the current trigger to it
      this.triggersByTargetElement[trigger.target_element].push(trigger);

      // For triggers by source question:
      _.forEach(trigger.conditions, (condition) => {
        if (condition.source_question) {
          // Make sure we have a trigger array for this (source) question
          this.triggersBySourceQuestion[condition.source_question] =
            this.triggersBySourceQuestion[condition.source_question] || [];

          // and append the current trigger to it if it's not already in there
          this.triggersBySourceQuestion[condition.source_question].push(trigger);
        }
      });  // condition
    });  // trigger
  }
}

export default AssessmentTypeModel;
