//
// models.js
//
// Various small models.
//

class Answer {
  constructor(properties) {
    this.value = '';
    this.justification = '';
    _.assign(this, properties);
  }
}

class DocumentType {
  constructor(properties) {
    this.name = '';
    this.description = '';
    this.helpDocument = '';
    _.assign(this, properties);
  }
}


//
// Attachments
//

class AssessmentAttachmentLink {
  constructor(properties) {
    this.id = undefined;
    this.author = undefined;
    this.timestamp = undefined;
    this.assessment = undefined;
    this.document_type_attachment = undefined;
    this.attachment = undefined;
    this.is_done = true;
    _.assign(this, properties);
  }
}

class AttachmentModel {
  constructor(properties) {
    this.uuid = undefined;
    this.title = '';
    this.attachment_type = '';
    this.modified_time = '';
    this.created_time = '';
    this.organization = '';
    _.assign(this, properties);
  }
}

class FileAttachmentModel extends AttachmentModel {
  constructor(properties) {
    super();
    this.original_file_name = '';
    this.file = '';
    this.expiration_date = '';
    this.modified_time = '';
    this.used_in = '';
    this.organization = '';
    _.assign(this, properties);
  }
}

class TextReferenceAttachmentModel extends AttachmentModel {
  constructor(properties) {
    super();
    _.assign(this, properties);
  }
}

class GenericFormAttachmentModel extends AttachmentModel {
  constructor(properties) {
    super();
    _.assign(this, properties);
  }
}

class NotApplicableAttachmentModel extends AttachmentModel {
  constructor(properties) {
    super();
    _.assign(this, properties);
  }
}

class AgriformAttachmentModel extends AttachmentModel {
  constructor(properties) {
    super();
    this.agriformUuid = '';
    _.assign(this, properties);
  }
}


//
// Questionnaire element state frontend model
//
class QuestionnaireElementState {
  constructor(properties) {
    this.elementUuid = undefined;
    this.isVisible = false;
    this.isAutomaticAnswer = false;
    _.assign(this, properties);
  }
}


module.exports = {
  Answer,
  AttachmentModel,
  FileAttachmentModel,
  TextReferenceAttachmentModel,
  NotApplicableAttachmentModel,
  AgriformAttachmentModel,
  AssessmentAttachmentLink,
  DocumentType,
  QuestionnaireElementState,
  GenericFormAttachmentModel,
};
