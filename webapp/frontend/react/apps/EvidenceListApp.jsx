const AssessmentDocumentType = require('../components/AssessmentDocumentType/AssessmentDocumentType.jsx');
const BusyFeedback = require('../components/BusyFeedback');
import textBlocks from '../components/Utils/textBlocks.js';
const pageData = require('../components/Utils/pageData.js');

const EvidenceHistoryList = require('../components/EvidenceHistory/EvidenceHistoryList.jsx');
import AnswerSaveError from '../components/QuestionnaireEditor/AnswerSaveError.jsx';

//
// Application component
//
const EvidenceListApp = React.createClass({


  propTypes: {
    params: React.PropTypes.object.isRequired,
    assessmentUuid: React.PropTypes.string.isRequired,
    readOnly: React.PropTypes.bool,
    history: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'OrganizationsStore', 'OfflineNotificationStore'),
    ToastMixin(),
  ],


  componentDidMount() {
    const flux = this.getFlux();
    const assessmentsStore = flux.store('AssessmentsStore');
    assessmentsStore.on('save', this.onAssessmentSaveSuccess);
    assessmentsStore.on('save-error', this.onAssessmentSaveFail);
  },


  componentWillUnmount() {
    const flux = this.getFlux();
    flux.store('AssessmentsStore').removeListener('save', this.onAssessmentSaveSuccess);
    flux.store('AssessmentsStore').removeListener('save-error', this.onAssessmentSaveFail);
  },


  getStateFromFlux() {
    const flux = this.getFlux();
    const assessmentsStore = flux.store('AssessmentsStore');
    const organizationsStore = flux.store('OrganizationsStore');
    const assessmentsStoreState = assessmentsStore.getState();
    const assessment = assessmentsStoreState.assessments[this.props.params.assessmentUuid];

    const assessmentTypeUuid = assessment.assessment.assessment_type;

    let documentTypes = _.cloneDeep(assessmentsStoreState.documentTypesDictionary[assessmentTypeUuid]);
    documentTypes = _.filter(documentTypes, dt => !dt.is_optional); // Show only required docTypes

    const evidenceAssociatedQuestions = assessmentsStoreState.evidenceAssociatedQuestionsDictionary[assessmentTypeUuid];

    const currentOrganization = organizationsStore.getOrganization(pageData.context.uuid);
    const answersWithErrors = assessmentsStoreState.answersWithErrors[assessment.uuid] || {};
    const offlineNotificationStoreState = flux.store('OfflineNotificationStore');
    const isApplicationOnline = offlineNotificationStoreState.status === 'online';

    return {
      assessmentsStoreState,
      assessment,
      currentOrganization,
      documentTypes,
      evidenceAssociatedQuestions,
      answersWithErrors,
      isApplicationOnline,
    };
  },

  onAssessmentSaveSuccess() {
    this.toast({
      title: `${_t('Assessment saved')}.`,
      type: 'success',
    });
  },


  onAssessmentSaveFail() {
    this.toast({
      title: `${_t('Assessment saving failed')}.`,
      type: 'error',
    });
  },

  isAnswered(attachmentLinks) {
    const isAnswered = _.some(attachmentLinks, 'is_done');

    return isAnswered;
  },


  makeDocumentTypesList(documentTypes) {
    const docTypes = _.map(documentTypes, (documentType) => {
      const attachmentLinks = this.state.assessment.attachmentLinks[documentType.uuid];
      return (
        <AssessmentDocumentType
          currentOrganization={this.state.currentOrganization}
          assessment={this.state.assessment}
          isAnswered={this.isAnswered(attachmentLinks)}
          documentType={documentType}
          readOnly={this.props.readOnly}
          evidenceAssociatedQuestions={this.state.evidenceAssociatedQuestions}
          params={this.props.params}
        />
      );
    });

    return (
      <div>
        {docTypes}
      </div>
    );
  },

  render() {
    const { assessment, currentOrganization } = this.state;
    if (!assessment || !assessment.assessmentType || !currentOrganization) {
      return <BusyFeedback />;
    }

    const { documentTypes, answersWithErrors, isApplicationOnline } = this.state;
    const answersHaveError = _.includes(answersWithErrors, true);
    const { params, history, readOnly } = this.props;

    return (
      <div className="evidence-list-app">

        <EvidenceHistoryList
          assessmentUuid={params.assessmentUuid}
          readOnly={readOnly}
          params={params}
          history={history}
        />

        <div className="row">
          <div className="col-xs-12">
            <p className="evidence-intro">
              {textBlocks.assessment_evidence_list_intro}
            </p>
          </div>
        </div>

        <div className="evidence-table">
          {this.makeDocumentTypesList(documentTypes)}
        </div>
        <AnswerSaveError answersHaveError={answersHaveError} isApplicationOnline={isApplicationOnline} />
      </div>

    );
  },

});

module.exports = EvidenceListApp;
