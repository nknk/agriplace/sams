const Navigation = require('../components/Navigation/Navigation.jsx');
const BusyFeedback = require('../components/BusyFeedback');
import pageData from '../components/Utils/pageData.js';
const { uuid: organizationUuid } = pageData.context;
import { Header } from '../components/Header/Header.jsx';
import { OfflineNotification } from '../components/OfflineNotification/OfflineNotification.jsx';

export const AssessmentApp = React.createClass({

  propTypes: {
    params: React.PropTypes.object,
    history: React.PropTypes.object,
    location: React.PropTypes.object,
    children: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'JudgementsStore'),
  ],

  componentDidMount() {
    const flux = this.getFlux();
    const { params } = this.props;
    const { assessmentUuid } = params;

    flux.actions.assessments.loadAssessmentFull(assessmentUuid);
    flux.actions.organizations.loadOrganizations();
  },

  componentWillUpdate(nextProps, nextState) {
    const { params, location, history } = nextProps;
    const { assessment } = nextState;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;

    const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/assessment-reuse-status`;

    if ((assessment.assessmentReuseStatus === 'pending') && (url !== location.pathname)) {
      history.pushState(null, url);

      // short fix for modal open css still present
      // problem: otherwise inadequate gap shown on right
      // and also scroll doesnt works
      document.body.classList.remove('modal-open'); // work around a bug in ngx-bootstrap
      document.body.style.padding = 0;
    }
  },

  getStateFromFlux() {
    const { params } = this.props;
    const flux = this.getFlux();
    const assessementsStoreState = flux.store('AssessmentsStore').getState();
    const organizationsStore = flux.store('OrganizationsStore');
    const judgementsStoreState = flux.store('JudgementsStore').getState();
    const assessmentUuid = params.assessmentUuid;

    return {
      assessment: assessementsStoreState.assessments[assessmentUuid],
      currentOrganization: organizationsStore.getOrganization(organizationUuid),
      isConfirmAllModalOpen: judgementsStoreState.isConfirmAllModalOpen,
      isConfirmAllInProgress: judgementsStoreState.isConfirmAllInProgress,
      judgementActions: flux.actions.judgements,
      permissionsAssessments: flux.stores.AssessmentsStore.getPermissions(assessmentUuid),
    };
  },

  render() {
    const { isConfirmAllModalOpen, isConfirmAllInProgress, currentOrganization,
      assessment, judgementActions, permissionsAssessments } = this.state;

    if (!(assessment && assessment.assessmentType) || !currentOrganization) {
      return <BusyFeedback />;
    }

    const isLocal = window && window.location && (
      window.location.hostname === 'localhost'
      || window.location.hostname === '0.0.0.0'
      || window.location.hostname === '127.0.0.1'
    );

    const { params, location } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;
    const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/assessment-reuse-status`;

    // hide navigation bar (breadcrumbs) and header buttons
    const showNavigation = (url !== location.pathname);

    return (
      <div>
        {!isLocal && (
          <OfflineNotification />
        )}
        <Header
          {...this.props}
          state={assessment.assessment.state}
          name={assessment.assessment.name}
          orgType={this.state.currentOrganization.type}
          showButtons={showNavigation}
        />
        {showNavigation ? (
          <Navigation
            {...this.props}
            navigationSections={assessment.assessmentType.sections}
            isConfirmAllModalOpen={isConfirmAllModalOpen}
            isConfirmAllInProgress={isConfirmAllInProgress}
            judgementActions={judgementActions}
            permissionsAssessments={permissionsAssessments}
            assessmentName={assessment.assessment.name}
          />
        ) : (
          this.props.children
        )}
      </div>
    );
  },
});
