

var AssessmentResults = require('../components/AssessmentResults/AssessmentResults.jsx');
var BusyFeedback = require('../components/BusyFeedback');
var HelpIcon = require('../components/HelpIcon/HelpIcon.jsx');
var actions = require('../actions');
var stores = require('../stores');
var fluxInstance = new Fluxxor.Flux(stores, actions);

var helpLinkTitle = 'results';
var pageData = require('../components/Utils/pageData.js');

//
// Application component
//
var ResultsApp = React.createClass({


  propTypes: {
    assessmentUuid: React.PropTypes.string.isRequired,
    readOnly: React.PropTypes.bool,
  },


  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'HelpLinksStore'),
  ],


  componentDidMount() {
    var flux = this.getFlux();

    flux.actions.assessments.loadAssessmentFull(this.props.params.assessmentUuid);
    flux.actions.helpLinks.loadHelpLinks(this.props.params.assessmentUuid);
    flux.actions.organizations.loadOrganizations();
  },

  getStateFromFlux() {
    var flux = this.getFlux();
    var assessmentsStore = flux.store('AssessmentsStore');
    var helpLinksStore = flux.store('HelpLinksStore');
    var assessmentsStoreState = assessmentsStore.getState();
    var organizationsStore = flux.store('OrganizationsStore');
    var helpLinksStoreState = helpLinksStore.getState();
    var assessment = assessmentsStoreState.assessments[this.props.params.assessmentUuid];
    var helpLinks = helpLinksStoreState.helpLinks;
    var currentOrganization = organizationsStore.getOrganization(pageData.context.uuid);

    return {
      assessmentsStoreState,
      assessment,
      helpLinks,
      currentOrganization,
    };
  },

  //
  // Render
  //

  render() {
    if (_.isEmpty(this.state.currentOrganization) || !this.state.assessment || !this.state.assessment.assessmentType) {
      return <BusyFeedback />;
    }

    var helpObj = _.find(this.state.helpLinks, (helpLink) => helpLink.title === helpLinkTitle) || {};
    var helpUrl = helpObj.url;

    return (
      <div className="results-app">
        <div className="lead">
            <span>{_t('Below you will find an overview of your answers')}.</span>
            <HelpIcon helpUrl={helpUrl} />
        </div>
        <AssessmentResults
          assessment={this.state.assessment}
          params={this.props.params}
    />
      </div>

    );
  },

});

module.exports = ResultsApp;
