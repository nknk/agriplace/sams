import React from 'react';
var FormBuilderOutput = require('../components/FormBuilder/FormBuilderOutput.jsx');
var formBuilderUtils = require('../components/FormBuilder/formBuilderUtils.js');
var BusyFeedback = require('../components/BusyFeedback');

var classnames = require('classnames');

var actions = require('../actions');
var stores = require('../stores');
var fluxInstance = new Fluxxor.Flux(stores, actions);

var FormReaderApp = React.createClass({ // eslint-disable-line no-unused-vars

  mixins: [
    FluxMixin,
    StoreWatchMixin('FormStore'),
  ],

  getStateFromFlux() {
    var flux = this.getFlux();
    var state = flux.store('FormStore').getState();
    var formData = state.schemas[this.props.schemaUuid] || {};

    return {
      schema: formData.schema,
      value: state.forms[this.props.formUuid],
      formSaveInProgress: state.formSaveInProgress,
      errors: state.errors,
    };
  },

  componentDidMount() {
    this.getFlux().actions.form.getSchema(this.props.schemaUuid);
    this.getFlux().actions.form.getFormValue(this.props.formUuid);
  },

  handleSubmit() {
    var value = this.refs.form.getValue();
    if (!value) {
      console.error('Validation failed!');
    } else {
      value = formBuilderUtils.format(value);
      this.getFlux().actions.form.submit(value, this.props.formUuid);
    }
  },

  render() {

    if (!this.state.schema) {
      return <BusyFeedback />;
    }

    var saveButtonClassName = classnames({
      'icon fa fa-save': !this.state.formSaveInProgress,
      'icon fa fa-spinner fa-spin': this.state.formSaveInProgress,
    });

    return (<div className="FormReaderApp">
      {
        _.map(this.state.errors, (msg) => {
          return <div className="alert alert-danger">{msg}</div>;
        })
      }

      <FormBuilderOutput
        ref="form"
        schema={this.state.schema}
        value={this.state.value}
      />
      <Bootstrap.Button
        bsStyle="success"
        onClick={this.handleSubmit}
    >
        <i className={saveButtonClassName} />
        { '  ' + _t('Submit') }
      </Bootstrap.Button>
    </div>);
  },

});

window.FormReaderApp = function (elementId, schemaUuid, formUuid) {
  elementId = elementId || 'app';
  React.render(
    <FormReaderApp
      flux={fluxInstance}
      schemaUuid={schemaUuid}
      formUuid={formUuid}
    />,
    document.getElementById(elementId));
};
