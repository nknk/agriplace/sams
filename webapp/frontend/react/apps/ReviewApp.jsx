import React, { PropTypes } from 'react';
const BusyFeedback = require('../components/BusyFeedback');
import { AssessmentReview } from '../components/AssessmentReview/AssessmentReview.jsx';

export const ReviewApp = React.createClass({

  propTypes: {
    params: PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('ReviewStore', 'AssessmentsStore'),
  ],

  componentDidMount() {
    this.getFlux().actions.review.loadReview(this.props.params.assessmentUuid);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const assessementsStoreState = flux.store('AssessmentsStore').getState();
    const reviewStoreState = flux.store('ReviewStore').getState();

    return {
      assessments: assessementsStoreState.assessments,
      review: reviewStoreState.review,
    };
  },

  onInternalDescriptionChange(text) {
    this.getFlux().actions.review.changeInternalDescription({ workflowUuid: this.state.review.workflow_uuid, text });
  },

  onOverviewDescriptionChange(text) {
    this.getFlux().actions.review.changeOverviewDescription({ workflowUuid: this.state.review.workflow_uuid, text });
  },

  onPlannedAuditDateChange(date) {
    this.getFlux().actions.review.changeDate({ workflowUuid: this.state.review.workflow_uuid, date });
  },

  onAuditStartDateChange(date) {
    this.getFlux().actions.review.changeAuditStartDate({ workflowUuid: this.state.review.workflow_uuid, date });
  },

  onAuditEndDateChange(date) {
    this.getFlux().actions.review.changeAuditEndDate({ workflowUuid: this.state.review.workflow_uuid, date });
  },

  onDecisionChange(decision) {
    this.getFlux().actions.review.changeDecision({ workflowUuid: this.state.review.workflow_uuid, decision });
  },

  onJudgementChange({ assessmentUuid, judgement }) {
    this.getFlux().actions.review.onJudgementChange({ assessmentUuid, judgement });
  },

  onReauditChange(reaudit) {
    this.getFlux().actions.review.changeReaudit({ workflowUuid: this.state.review.workflow_uuid, reaudit });
  },

  render() {
    const assessment = this.state.assessments[this.props.params.assessmentUuid] || {};
    if (!assessment.assessmentType || _.isEmpty(this.state.review)) {
      return <BusyFeedback />;
    }

    return (
      <div className="ReviewApp">
        <AssessmentReview
          assessment={assessment}
          questions={assessment.assessmentType.questions}
          review={this.state.review}
          onPlannedAuditDateChange={this.onPlannedAuditDateChange}
          onAuditStartDateChange={this.onAuditStartDateChange}
          onAuditEndDateChange={this.onAuditEndDateChange}
          onDecisionChange={this.onDecisionChange}
          onJudgementChange={this.onJudgementChange}
          onReauditChange={this.onReauditChange}
          onInternalDescriptionChange={this.onInternalDescriptionChange}
          onOverviewDescriptionChange={this.onOverviewDescriptionChange}
        />
      </div>
    );
  },

});

module.exports = ReviewApp;
