import React from 'react';
const AttachmentsStore = require('../stores/AttachmentsStore.js');
const ShareEvidenceStore = require('../stores/ShareEvidenceStore.js');
const SharedEvidenceStore = require('../stores/SharedEvidenceStore.js');
import { ArchiveTab } from '../components/ArchiveList/ArchiveTab.jsx';
import { SharedEvidenceTab } from '../components/SharedEvidence/SharedEvidenceTab.jsx';
import { Tabs } from '../components/Utils/Tabs.jsx';

const stores = {
  AttachmentsStore: new AttachmentsStore(),
  ShareEvidenceStore: new ShareEvidenceStore(),
  SharedEvidenceStore: new SharedEvidenceStore(),
};

const actions = require('../actions');

const fluxInstance = new Fluxxor.Flux(stores, actions);


const ArchiveApp = React.createClass({
  mixins: [
    FluxMixin,
    StoreWatchMixin('SharedEvidenceStore'),
  ],

  componentWillMount() {
    const flux = this.getFlux();
    flux.actions.sharedEvidence.getSharingOrganizations();
  },

  getStateFromFlux() {
    const sharedEvidenceStoreState = this.getFlux().store('SharedEvidenceStore').getState();

    return {
      sharingOrganizations: sharedEvidenceStoreState.sharingOrganizations,
    };
  },

  render() {
    const archiveTab = {
      name: _t('Archive'),
      tab: <ArchiveTab />,
    };

    const sharedEvidenceTab = {
      name: _t('Shared evidence'),
      tab: <SharedEvidenceTab />,
    };

    const tabs = [];
    tabs.push(archiveTab);

    const { sharingOrganizations } = this.state;
    if (sharingOrganizations && sharingOrganizations.length) {
      tabs.push(sharedEvidenceTab);
    }

    return (
      <div className="archive-app">
        <Tabs tabs={tabs} />
      </div>
    );
  },

});

window.ArchiveApp = (elementId) => {
  React.render(
    <ArchiveApp flux={fluxInstance} />, document.getElementById(elementId)
  );
};
