const { CreateCertificate } = require('../components/CreateCertificate/CreateCertificate.jsx');
import pageData from '../components/Utils/pageData';
import BusyFeedback from '../components/BusyFeedback/BusyFeedback.jsx';
const { uuid: organizationUuid } = pageData.context;

export const CertificateEditApp = React.createClass({

  propTypes: {
    params: React.PropTypes.object.isRequired,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('CertificatesStore', 'ProductsStore'),
  ],

  componentDidMount() {
    const flux = this.getFlux();
    flux.actions.certificates.getCertificate(organizationUuid, this.props.params.certificateUuid);
    flux.actions.certificates.getCertificateTypes(organizationUuid);
    flux.actions.products.getProductTypes();
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const certificatesStoreState = flux.stores.CertificatesStore.getState();
    const productsStoreState = flux.stores.ProductsStore.getState();

    return {
      certificateTypes: certificatesStoreState.certificateTypes[organizationUuid] || [],
      productTypes: this.sort(productsStoreState.productTypes || []),
      certificate: certificatesStoreState.selectedCertificate,
      createOrUpdateCertificateInProgress: certificatesStoreState.createOrUpdateCertificateInProgress,
    };
  },

  sort(productTypes) {
    return productTypes.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  },

  render() {
    const { certificateTypes, productTypes, certificate,
      createOrUpdateCertificateInProgress } = this.state;
    if (!certificate || _.isEmpty(certificateTypes) || _.isEmpty(productTypes)) {
      return <BusyFeedback />;
    }

    return (
      <CreateCertificate
        certificateTypes={certificateTypes}
        productTypes={productTypes}
        certificate={certificate}
        createOrUpdateCertificateInProgress={createOrUpdateCertificateInProgress}
        {...this.props}
      />

    );
  },
});

