import AuditorDashboard from '../components/AuditorDashboard/AuditorDashboard.jsx';

const roles = {
  AUDITOR: 'AUDITOR',
  COORDINATOR: 'COORDINATOR',
};

export const DashboardApp = React.createClass({

  propTypes: {
    role: React.PropTypes.string,
    assessmentUuid: React.PropTypes.string,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('DashboardStore'),
  ],

  componentDidMount() {
    const flux = this.getFlux();
    const year = this.state.filters.year;

    flux.actions.dashboard.loadFilteredAssessments();
    flux.actions.dashboard.getAssessmentTypesForFilters(year);
    flux.actions.dashboard.getAssessmentStatesForFilters(year);
    if (this.props.role === roles.COORDINATOR) {
      flux.actions.dashboard.getInternalInspectorsForFilters(year);
    }
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const dashboardStoreState = flux.store('DashboardStore').getState();

    return {
      internalInspectors: dashboardStoreState.internalInspectors,
      filteredAssessments: dashboardStoreState.filteredAssessments,
      pageNumber: dashboardStoreState.filters.pageNumber,
      pageCount: dashboardStoreState.filteredAssessmentsPageCount,
      isUpdatingFilteredAssessments: dashboardStoreState.isUpdatingFilteredAssessments,
      filters: dashboardStoreState.filters,
      organizationDetails: dashboardStoreState.organizationDetails,
    };
  },

  render() {
    return (
      <div className="DashboardApp">
        <AuditorDashboard
          role={this.props.role}
          auditors={this.state.internalInspectors}
          assessments={this.state.filteredAssessments}
          pageCount={this.state.pageCount}
          pageNumber={this.state.pageNumber}
          isLoading={this.state.isUpdatingFilteredAssessments}
          organizationDetails={this.state.organizationDetails}
        />
      </div>
    );
  },

});

