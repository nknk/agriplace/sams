const BusyFeedback = require('../components/BusyFeedback/BusyFeedback.jsx');

import { ShareAssessment } from '../components/ShareAssessment/ShareAssessment.jsx';

import React from 'react';

const GeneralEventsMixin = require('../mixins/GeneralEventsMixin.js');
const ToastMixin = require('../mixins/ToastMixin.js');
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

export const ShareAssessmentApp = React.createClass({
  propTypes: {
    params: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('OrganizationsStore', 'AssessmentsStore'),
    GeneralEventsMixin,
    ToastMixin(),
  ],


  componentWillMount() {
    const organizationsActions = this.getFlux().actions.organizations;
    organizationsActions.loadAuditorsByAssessement(this.props.params.assessmentUuid);
  },

  componentDidMount() {
    const assessmentsStore = this.getFlux().store('AssessmentsStore');
    this.addGeneralEvent(assessmentsStore, 'shares-saving-error', this.toastSharesSavingError);
    this.addGeneralEvent(assessmentsStore, 'state-saving-error', this.toastStateSavingError);
  },

  getStateFromFlux() {
    const { assessmentUuid } = this.props.params;
    const organizationsStore = this.getFlux().store('OrganizationsStore');
    const assessmentsStore = this.getFlux().store('AssessmentsStore');
    const assessmentShares = assessmentsStore.getAssessmentShares(assessmentUuid);

    //
    // we need full organization details for the selection in the listView
    // inefficient, need to refactor this piece later if needed
    //

    const auditors = organizationsStore.getAuditors();

    const sharedAuditorsUuids = assessmentShares.map((share) => share.partner);
    const selectedAuditors = auditors.filter((auditor) => sharedAuditorsUuids.indexOf(auditor.uuid) !== -1);
    const assessment = assessmentsStore.getState().assessments[assessmentUuid];
    const assessmentType = assessmentsStore.getState().assessmentTypes[assessment.assessment.assessment_type];

    return {
      auditors,
      selectedAuditors,
      assessment,
      assessmentType,
    };
  },

  onAssessmentClose(assessmentUuid, shares) {
    const assessmentsActions = this.getFlux().actions.assessments;

    assessmentsActions.saveAssessmentSharesAndClose(assessmentUuid, shares);
  },

  toastStateSavingError() {
    this.toast({
      type: 'error',
      message: `${_t('An error has occured, could not save this assessment closed')} !`,
    });
  },

  toastSharesSavingError() {
    this.toast({
      type: 'error',
      message: `${_t('Could not save your assessment shared')} !`,
    });
  },

  render() {
    const { selectedAuditors, assessment, assessmentType, auditors } = this.state;

    if (!(selectedAuditors
       && assessment
       && assessment.assessmentType
       && assessmentType
       && !_.isEmpty(assessment.questionnaireState))) {
      return <BusyFeedback />;
    }

    return (
      <ShareAssessment
        auditors={auditors}
        selectedAuditors={selectedAuditors}
        sharingMessage={assessmentType.sharing_conditions || ''}
        params={this.props.params}
        onAssessmentClose={this.onAssessmentClose}
        isReportVisible={assessment.isReportVisible}
      />
    );
  },
});

module.exports = ShareAssessmentApp;
