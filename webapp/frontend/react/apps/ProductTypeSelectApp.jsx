const ProductTypeSelect = require( '../components/ProductTypeSelect/ProductTypeSelect.jsx' );
const classnames = require('classnames');

let productTypeSelectOptions = {
  sortKeys: ['name'],
  // textarea comments are disabled in ProductTypeSelect as requested by Maarten
  textareaPlaceholder: _t("you can enter a comment specific to the selected crop here:")
};

let listViewOptions = {
  filterKeys: ['name'],
  columns: [{name: _t('Crops')}],
  sortKeys: ['title'],
  emptyListText: _t('No crop selected'),
  showCheckBox: true,
};

let queryInputOptions = {
  placeholder: _t('Search crop'),
  showSearchIcon: true,
};

let ProductTypeSelectApp = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('ProductsStore', 'OrganizationsStore'),
    GeneralEventsMixin,
    ToastMixin(),
  ],

  getInitialState() {
    return {
      selectedName: '',
      showModal: false,
      productsLoaded: false,
    };
  },

  getStateFromFlux() {
    var productsStore = this.getFlux().store('ProductsStore');
    var organizationsStore = this.getFlux().store('OrganizationsStore');
    return {
      years: productsStore.getState().years,
      traders: organizationsStore.getTraders(),
      productTypes: productsStore.productTypes,
    };
  },

  componentWillMount() {
    productTypeSelectOptions.onModalResult = this.onModalResult;
    const productsStore = this.getFlux().store( 'ProductsStore' );

    this.addGeneralEvent( productsStore, 'productTypesUpdated', () => {
      this.setState({ productsLoaded: true });
    });
  },

  componentDidMount() {
    // load entire store to be sure user products is loaded as well
    this.getFlux().actions.products.loadProductTypes();
    this.getFlux().actions.organizations.loadTraders();
  },

  onModalResult( product ) {
    this.setState({
      showModal: false
    });
    if ( product ) {
      // only apply to selectedName if there is product, and no need to rerender for that
      if (this.props.onSuccess) {
        this.props.onSuccess(product);
      } else {
        this.state.selectedName= product.name;
        this.getFlux().actions.products.addProduct( product );
      }
    }
  },

  onButtonClick() {
    this.setState({showModal: true});
  },

  renderLaunchModalButton() {
    var classNames = classnames({
      'icon fa fa-plus': this.state.productsLoaded,
      'icon fa fa-spinner fa-spin': !this.state.productsLoaded,
    });

    return (
      <Bootstrap.Button
        disabled={this.props.readOnly}
        bsStyle= "primary"
        id= "add-crop-button"
        onClick= { this.onButtonClick } >
        <i className={classNames} />
                { '  ' + _t('Add Crop') }
      </Bootstrap.Button>
    );
  },


  renderModal() {
    if ( !this.state.productsLoaded ) {
      return null;
    }
    productTypeSelectOptions.productTypes = this.state.productTypes;

    return ( this.state.showModal )
      ? <ProductTypeSelect
          traders = { this.state.traders }
          years = { this.state.years }
          options = { productTypeSelectOptions }
          listViewOptions = { listViewOptions }
          queryInputOptions = { queryInputOptions } />
      : null;
  },


  render() {
    return (
      <div>
        { this.renderLaunchModalButton() }
        { this.renderModal() }
      </div>
    );
  },
});

module.exports = ProductTypeSelectApp;
