import React from 'react';

var actions = require('../actions');
var stores = require('../stores');
var fluxInstance = new Fluxxor.Flux(stores, actions);
var CropsPlot = require('../components/CropsPlot/CropsPlot.jsx');
var RvoImport = require('../components/RvoImport/RvoImport.jsx');
var BusyFeedback = require('../components/BusyFeedback/BusyFeedback.jsx');

var pageData = require('../components/Utils/pageData.js');

var CropsPlotApp = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('PlotsStore', 'ProductsStore'),
  ],

  getStateFromFlux() {
    var flux = this.getFlux();
    var plotsStore = flux.store('PlotsStore');
    var organizationsStore = flux.store('OrganizationsStore');
    var currentOrganization = organizationsStore.getOrganization(pageData.context.uuid);
    var plotsData = plotsStore.getState().plots;
    var rvoErrors = plotsStore.getState().rvoErrors;
    var kvkManual = plotsStore.getState().kvk;
    var kvkProfile = currentOrganization && currentOrganization.identification_number;

    return {
      plotsData,
      rvoErrors,
      kvk: kvkManual || kvkProfile,
      currentOrganization,
    };
  },

  componentWillMount() {
    var flux = this.getFlux();
    var organizationUuid = pageData.context.uuid;

    flux.actions.plots.loadPlots(organizationUuid);
    flux.actions.organizations.loadOrganizations();
  },

  onFetch(kvk) {
    kvk = kvk || this.state.kvk;
    var flux = this.getFlux();
    var organizationUuid = pageData.context.uuid;

    flux.actions.plots.loadRvo(organizationUuid, kvk);
  },

  render() {
    var plotsData = this.state.plotsData;
    var defaultColor = '#6cb33f';

    if (this.state.plotsData === undefined || !this.state.currentOrganization) {
      return <BusyFeedback />;
    } else if (_.isEmpty(this.state.plotsData)) {
      return <RvoImport onFetch={this.onFetch} kvk={this.state.kvk} />;
    }

    _.forEach(plotsData.rvo_data, (crops, year) => {
      plotsData.rvo_data[year].forEach((crop, i, arr) => {
        arr[i].color = arr[i].color || crop.product_type.color || defaultColor;
        arr[i].name = arr[i].name || crop.name || crop.product_type.name;
        arr[i].field_id = arr[i].field_id || crop.field_id || Math.random();
      });
    });

    return (
      <div className="container-fluid">
        <div className="row">
          <CropsPlot onFetch={this.onFetch} plotsData={plotsData} />
        </div>
      </div>
    );
  },

});

app.cropsPlot = (id) => {
  var element = document.getElementById(id || 'app');
  React.render(<CropsPlotApp flux= { fluxInstance } />, element);
};
