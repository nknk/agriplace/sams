import React from 'react';
const actions = require('../actions');
const stores = require('../stores');
const fluxInstance = new Fluxxor.Flux(stores, actions);

const GgapRegistrationForm = require('../components/GgapRegistrationForm/GgapRegistrationForm.jsx');

const GgapRegistrationFormApp = React.createClass({

  mixins: [
    FluxMixin,
  ],

  getInitialState() {
    return {
      isWarningModal: false,
    };
  },

  onSubmit(value) {
    if (this.refs.form.isGgap(value)) {
      this.setState({ isWarningModal: true });
    } else {
      this.createAgreement({ callback: () => window.open(this.props.onFailUrl, '_self') });
    }
  },

  handleSuccessClick() {
    this.createAgreement({ callback: () => window.open(this.props.onSuccessUrl, '_self') });
  },

  handleFailClick() {
    this.createAgreement({ callback: () => window.open(this.props.onFailUrl, '_self') });
  },

  createAgreement({ callback }) {
    this.getFlux().actions.assessments.sendRegistrationInfo({
      value: this.refs.form.getValue(),
      workflowContextUUID: this.props.workflowContextUUID,
      callback,
    });
  },

  render() {
    return (
      <div>
        <GgapRegistrationForm ref="form" onSubmit={this.onSubmit} />
        <Bootstrap.Modal show={this.state.isWarningModal} backdrop="static">
          <Bootstrap.Modal.Header>
            <h2>Aangemeld</h2>
          </Bootstrap.Modal.Header>
          <Bootstrap.Modal.Body>
            U bent nu aangemeld voor GLOBALG.A.P. certificering. Wilt u direct starten met uw interne audit?
          </Bootstrap.Modal.Body>
          <Bootstrap.Modal.Footer>
            <div className="btn-group pull-right">
              <Bootstrap.Button
                onClick={this.handleFailClick}
    >
                Later
              </Bootstrap.Button>
              <Bootstrap.Button
                onClick={this.handleSuccessClick}
                bsStyle="success"
    >
                Ok
              </Bootstrap.Button>
            </div>
          </Bootstrap.Modal.Footer>
        </Bootstrap.Modal>
      </div>
    );
  },

});


// Use this global function to connect app to HTML
window.ggapRegistrationFormApp = function ({ elementId, onSuccessUrl, onFailUrl, workflowContextUUID }) {
  elementId = elementId || 'app';
  React.render(
    <GgapRegistrationFormApp
      flux={fluxInstance}
      onSuccessUrl={onSuccessUrl}
      onFailUrl={onFailUrl}
      workflowContextUUID={workflowContextUUID}
    />,
    document.getElementById(elementId)
  );
};
