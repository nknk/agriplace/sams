const CertificatesList = require('../components/Certificates/CertificatesList.jsx');
const pageData = require('../components/Utils/pageData.js');
const BusyFeedback = require('../components/BusyFeedback');

export const CertificatesListApp = React.createClass({

  propTypes: {
    params: React.PropTypes.object.isRequired,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('CertificatesStore', 'ProductsStore'),
  ],

  componentWillMount() {
    const flux = this.getFlux();
    flux.actions.certificates.getCertificatesList(pageData.context.uuid);
    flux.actions.products.getProductTypes();
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const certificatesStoreState = flux.store('CertificatesStore').getState();
    const productsStoreState = flux.store('ProductsStore').getState();

    return {
      certificateList: certificatesStoreState.certificateList[pageData.context.uuid] || [],
      isFetched: certificatesStoreState.isFetched[pageData.context.uuid],
      productTypes: productsStoreState.productTypes || [],
    };
  },

  resetSelectedCertificate() {
    const flux = this.getFlux();
    flux.actions.certificates.resetSelectedCertificate();
  },

  render() {
    return this.state.isFetched && !_.isEmpty(this.state.productTypes) ? (
      <CertificatesList
        params={this.props.params}
        certificateList={this.state.certificateList}
        productTypes={this.state.productTypes}
        onCreateOrOpenCertificate={this.resetSelectedCertificate}
      />
      ) : (
      <BusyFeedback />
    );
  },
});

