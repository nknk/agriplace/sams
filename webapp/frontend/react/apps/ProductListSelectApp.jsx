const ProductListSelect = require('../components/ProductListSelect/ProductListSelect.jsx');
const ProductTypeSelectApp = require('./ProductTypeSelectApp.jsx');
const ProductDependentQuestionnaireModal = require(
  '../components/ProductDependentQuestionnaire/ProductDependentQuestionnaireModal.jsx'
);

const pageData = require('../components/Utils/pageData.js');
import BusyFeedback from '../components/BusyFeedback';
//
//																	Props
//

const productListOptions = {};

const listViewOptions = {
  // QueryInput disabled
  // filterKeys: ['name'],
  columns: [
    { name: _t('Your crops') },
    { area: _t('ha') },
    { location: _t('Location storage/processing (other than own farm)​') },
  ],
  sortKeys: ['name'],
  // emptyListText : _t('Crop not found'),
  tableClasses: 'table-hover table-striped table-bordered',
  showCheckBox: true,
  showHeader: true,
};

//
//																		ProductListSelectApp
//

const ProductListSelectApp = React.createClass({

  propTypes: {
    params: React.PropTypes.string,
    readOnly: React.PropTypes.bool,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('ProductsStore', 'ProductLinksStore'),
  ],

  getInitialState() {
    return {
      selection: [],
      showProductDependentQuestionnaireModal: false,
    };
  },

  componentDidMount() {
    productListOptions.onSelectionChange = this.onSelectionChange;
    listViewOptions.readOnly = this.props.readOnly;
    const flux = this.getFlux();
    flux.actions.products.loadProducts(pageData.context.uuid);
    flux.actions.productLinks.loadProductLinks(this.props.params.assessmentUuid);
  },

  getStateFromFlux() {
    const productsStore = this.getFlux().store('ProductsStore');
    const productLinksStore = this.getFlux().store('ProductLinksStore');
    return {
      products: productsStore.getProductsByYear(),
      isProductsLoaded: productsStore.getState().isProductsLoaded,
      years: productsStore.getState().years,
      selectedYear: productsStore.getState().selectedYear,
      productLinks: productLinksStore.getState().productLinks,
      isProductLinksLoaded: productLinksStore.getState().isProductLinksLoaded,
      productDependentQuestionnaires: productLinksStore.getState().productDependentQuestionnaires,
      isProductAdded: productLinksStore.getState().isProductAdded,
    };
  },

  onSelectionChange(selection) {
    this.setState({
      showProductDependentQuestionnaireModal: true,
    });
    const productUuids = selection.map(product => product.uuid);
    this.getFlux().actions.productLinks.change(this.props.params.assessmentUuid, productUuids);
  },

  handleYearSelect(e) {
    this.getFlux().actions.products.changeYear({ year: +e.target.value });
  },

  handleSuccessfulAdd(product) {
    this.setState({
      showProductDependentQuestionnaireModal: true,
    });
    this.getFlux().actions.products.addProductAndSelect(product, this.props.params.assessmentUuid);
  },
  handleProductDependentQuestionnaireSuccess() {
    this.setState({
      showProductDependentQuestionnaireModal: false,
    });

    window.location.reload(true);
  },

  getSelected() {
    // We are doing this ugly thing since uuid of just added product isn't available right after adding
    return this.state.products.filter(p => ~this.state.productLinks.map(l => l.uuid).indexOf(p.uuid));
  },

  isReady() {
    return this.state.isProductLinksLoaded && this.state.isProductsLoaded;
  },

  getItems() {
    const inProgess = this.state.productLinks.filter(p => p.isInProgress).map(p => p.uuid);
    return this.state.products.map(p => _.extend(p, { disabled: ~inProgess.indexOf(p.uuid) }));
  },

  renderProductDependentQuestionnaireModal() {
    if (_.isEmpty(this.state.productDependentQuestionnaires)) {
      return null;
    }
    return (
      <ProductDependentQuestionnaireModal
        productDependentQuestionnaires={this.state.productDependentQuestionnaires}
        isProductAdded={this.state.isProductAdded}
        showModal={this.state.showProductDependentQuestionnaireModal}
        onSuccess={this.handleProductDependentQuestionnaireSuccess}
      />
    );
  },


  render() {
    if (!this.isReady()) {
      return <BusyFeedback />;
    }

    listViewOptions.items = this.getItems();
    const selected = this.getSelected();
    return (
      <div className="ProductListSelectApp">
        <h3>{_t('Please select the crops you want to certify')}</h3>
        <div className="row year" style={{ padding: 10 }}>
          <div className="col-xs-12">
            <span>{_t('Year')} </span>
            <select
              className="form-control"
              style={{ width: 'auto', display: 'inline-block' }}
              value={this.state.selectedYear}
              onChange={this.handleYearSelect}
            >
              {_.map(this.state.years, (year) => <option key={year} value={year}>{year}</option>)}
            </select>
          </div>
        </div>
        <ProductListSelect
          ref="ProductListSelect"
          selected={selected}
          productListOptions={productListOptions}
          listViewOptions={listViewOptions}
        />
        <div style={{ padding: '10px 0' }}>
          <ProductTypeSelectApp
            onSuccess={this.handleSuccessfulAdd}
            readOnly={this.props.readOnly}
          />
        </div>
          {this.renderProductDependentQuestionnaireModal()}
      </div>
    );
  },

});

module.exports = ProductListSelectApp;
