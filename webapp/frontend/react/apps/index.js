//
// Top-level apps
//

const ArchiveApp = require('./ArchiveApp.jsx');
const FormBuilderApp = require('./FormBuilderApp.jsx');
const FormReaderApp = require('./FormReaderApp.jsx');
const EvidenceListApp = require('./EvidenceListApp.jsx');
const ProductTypeSelectApp = require('./ProductTypeSelectApp.jsx');
const QuestionnaireEditorApp = require('./QuestionnaireEditorApp.jsx');
const QuestionnaireListApp = require('./QuestionnaireListApp.jsx');
const ProductListSelectApp = require('./ProductListSelectApp.jsx');
const ProductListApp = require('./ProductListApp.jsx');
const CropsPlotApp = require('./CropsPlotApp.jsx');
const App = require('./App.jsx');
const ResultsApp = require('./ResultsApp.jsx');
const ReviewApp = require('./ReviewApp.jsx');
const RBACApp = require('./RBACApp.jsx');
const DashboardApp = require('./DashboardApp.jsx');
const GgapRegistrationFormApp = require('./GgapRegistrationFormApp.jsx');
const ShareAssessmentApp = require('./ShareAssessmentApp.jsx');
const CertificatesListApp = require('./CertificatesListApp.jsx');
const CertificationCreateApp = require('./CertificationCreateApp.jsx');
const CertificationDetailApp = require('./CertificationDetailApp.jsx');
const CertificateEditApp = require('./CertificateEditApp.jsx');
const AssessmentReportApp = require('./AssessmentReportApp.jsx');

module.exports = {
  archiveApp: ArchiveApp,
  formBuilderApp: FormBuilderApp,
  formReaderApp: FormReaderApp,
  evidenceListApp: EvidenceListApp,
  productTypeSelectApp: ProductTypeSelectApp,
  questionnaireEditorApp: QuestionnaireEditorApp,
  questionnaireListApp: QuestionnaireListApp,
  productListSelectApp: ProductListSelectApp,
  productListApp: ProductListApp,
  cropsPlotApp: CropsPlotApp,
  App,
  resultsApp: ResultsApp,
  reviewApp: ReviewApp,
  rbacApp: RBACApp,
  dashboardApp: DashboardApp,
  ggapRegistrationFormApp: GgapRegistrationFormApp,
  shareAssessment: ShareAssessmentApp,
  certificatesListApp: CertificatesListApp,
  certificationCreateApp: CertificationCreateApp,
  certificationDetailApp: CertificationDetailApp,
  certificateEditApp: CertificateEditApp,
  assessmentReportApp: AssessmentReportApp,
};
