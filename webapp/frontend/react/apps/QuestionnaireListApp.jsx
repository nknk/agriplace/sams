const BusyFeedback = require('../components/BusyFeedback');
const QuestionnaireList = require('../components/QuestionnaireList/QuestionnaireList.jsx');

const QuestionnaireListApp = React.createClass({

  propTypes: {
    history: React.PropTypes.object,
    params: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore'),
  ],

  getStateFromFlux() {
    const flux = this.getFlux();
    const assessementsStore = flux.store('AssessmentsStore');
    const state = assessementsStore.getState();
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = this.props.params;

    const assessment = state.assessments[assessmentUuid] || {};

    if (assessment.assessmentType) {
      const questionnaires = Object.keys(assessment.assessmentType.questionnaires);
      if (questionnaires.length === 1) {
        this.props.history.pushState(null, `/${membershipUuid}/${assessmentAccessMode}/assessments\
/${assessmentUuid}/questionnaires/${questionnaires[0]}/edit`
        );
      }
    }

    return {
      assessment,
    };
  },

  render() {
    const { assessment } = this.state;

    if (!assessment.assessmentType) {
      return <BusyFeedback />;
    }

    const { params } = this.props;

    return (
      <div className="QuestionnaireListApp">
        <div className="lead">
          {`${_t('Here you can answer the (remaining) questions')}.`}
          {`${_t('For most questions you can retrieve additional information by clicking "Read More"')}.`}
        </div>
        <QuestionnaireList assessment={assessment} params={params} />
      </div>
    );
  },

});

module.exports = QuestionnaireListApp;
