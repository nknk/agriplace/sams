import React from 'react';
const ReactRouter = require('react-router');

const actions = require('../actions');
const stores = require('../stores');

const ResultsApp = require('./ResultsApp.jsx');
const QuestionnaireEditorApp = require('./QuestionnaireEditorApp.jsx');
const QuestionnaireListApp = require('./QuestionnaireListApp.jsx');
const ProductListSelectApp = require('./ProductListSelectApp.jsx');
const EvidenceListApp = require('./EvidenceListApp.jsx');
const ShareAssessmentApp = require('./ShareAssessmentApp.jsx');
const OverviewApp = require('./OverviewApp.jsx');
const ReviewApp = require('./ReviewApp.jsx');
import { AssessmentApp } from './AssessmentApp.jsx';
import { AssessmentReuseStatusApp } from './AssessmentReuseStatusApp.jsx';
import { CertificatesListApp } from './CertificatesListApp.jsx';
import { CertificationCreateApp } from './CertificationCreateApp.jsx';
import { CertificateEditApp } from './CertificateEditApp.jsx';
import { DashboardApp } from './DashboardApp.jsx';
import { AssessmentReportApp } from './AssessmentReportApp.jsx';

const Route = ReactRouter.Route;
const Router = ReactRouter.Router;
const createBrowserHistory = require('history/lib/createBrowserHistory');
const history = createBrowserHistory();

const fluxInstance = new Fluxxor.Flux(stores, actions);

const routes = (
  <Route>
    <Route component={DashboardApp} path="/memberships/:membershipUuid/home" />
    <Route path=":membershipUuid/:assessmentAccessMode/">
      <Route component={AssessmentReportApp} path="assessments/:assessmentUuid/report" />
      <Route component={AssessmentApp} path="assessments/:assessmentUuid/">
        <Route component={OverviewApp} path="detail" />
        <Route component={ResultsApp} path="results" />
        <Route component={ProductListSelectApp} path="products" />
        <Route component={QuestionnaireEditorApp} path="questionnaires/:questionnaireUuid/edit" />
        <Route component={QuestionnaireListApp} path="questionnaires" />
        <Route component={EvidenceListApp} path="documents" />
        <Route component={ShareAssessmentApp} path="sharing" />
        <Route component={ReviewApp} path="review" />
        <Route component={AssessmentReuseStatusApp} path="assessment-reuse-status" />
      </Route>
      <Route component={CertificatesListApp} path="certifications" />
      <Route path="certifications/">
        <Route component={CertificationCreateApp} path="product/new" />
        <Route component={CertificateEditApp} path=":certificateUuid/edit" />
      </Route>
    </Route>
  </Route>
);


window.App = function (args) {
  const createElement = function (Component, props) {
    return (
      <Component
        {...props}
        flux={fluxInstance}
        readOnly={args.readOnly}
        role={args.role}
        assessmentUuid={args.assessmentUuid}
      />
    );
  };

  const mountPoint = args.mountPoint || 'app';

  React.render(
    <Router
      createElement={createElement}
      history={history}
      routes={routes}
    />,
    document.getElementById(mountPoint)
  );
};
