const BusyFeedback = require('../components/BusyFeedback');
const QuestionnaireEditor = require('../components/QuestionnaireEditor');
import pageData from '../components/Utils/pageData';
const { uuid: organizationUuid } = pageData.context;
import { Link } from 'react-router';
const EvidenceHistoryList = require('../components/EvidenceHistory/EvidenceHistoryList.jsx');
import AnswerSaveError from '../components/QuestionnaireEditor/AnswerSaveError.jsx';

const QuestionnaireEditorApp = React.createClass({
  propTypes: {
    assessmentUuid: React.PropTypes.string,
    readOnly: React.PropTypes.bool,
    questionnaireUuid: React.PropTypes.string,
    params: React.PropTypes.object,
    location: React.PropTypes.object,
    history: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'OrganizationsStore', 'JudgementsStore', 'OfflineNotificationStore'),
    ToastMixin(),
  ],

  componentDidMount() {
    const { params } = this.props;
    const flux = this.getFlux();
    const questionnaireUuid = this.props.questionnaireUuid || params.questionnaireUuid;
    const assessmentUuid = params.assessmentUuid;

    // gets state of questionnaire as well
    flux.actions.assessments.loadAssessmentState(assessmentUuid, questionnaireUuid);
    flux.actions.judgements.load({ assessmentUuid });

    flux.store('AssessmentsStore').on('save', this.assessmentsSavedSuccessToast);
    flux.store('AssessmentsStore').on('answer-saving-success', this.onAnswerSaveSuccess);
    flux.store('AssessmentsStore').on('save-error', this.assessmentsSaveErrorToast);
  },

  componentWillUnmount() {
    const flux = this.getFlux();
    flux.store('AssessmentsStore').removeListener('save', this.assessmentsSavedSuccessToast);
    flux.store('AssessmentsStore').removeListener('save-error', this.assessmentsSaveErrorToast);
    flux.store('AssessmentsStore').removeListener('answer-saving-success', this.onAnswerSaveSuccess);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const assessmentsStore = flux.store('AssessmentsStore');
    const assessmentsStoreState = assessmentsStore.getState();
    const offlineNotificationStoreState = flux.store('OfflineNotificationStore');
    const organizationsStore = flux.store('OrganizationsStore');
    const judgementsStoreState = flux.store('JudgementsStore').getState();

    const { params } = this.props;
    const questionnaireUuid = this.props.questionnaireUuid || params.questionnaireUuid;
    const assessmentUuid = params.assessmentUuid;
    const assessment = assessmentsStoreState.assessments[assessmentUuid];
    const assessmentTypeUuid = assessment && assessment.assessment && assessment.assessment.assessment_type;
    const evidenceAssociatedQuestions = (assessmentTypeUuid
      && assessmentsStoreState.evidenceAssociatedQuestionsDictionary[assessmentTypeUuid]) || {};

    return {
      documentTypesDictionary: assessmentsStoreState.documentTypesDictionary,
      assessment,
      questionnaire: assessmentsStore.getQuestionnaire(assessmentUuid, questionnaireUuid),
      isJudgementLoaded: judgementsStoreState.isLoaded,
      currentOrganization: organizationsStore.getOrganization(organizationUuid),
      answersInProgress: assessmentsStoreState.answersInProgress[assessmentUuid],
      answersWithErrors: assessmentsStoreState.answersWithErrors[assessmentUuid] || {},
      isApplicationOnline: offlineNotificationStoreState.status === 'online',
      assessmentActions: flux.actions.assessments,
      judgements: judgementsStoreState.judgements,
      evidenceAssociatedQuestions,
      attachmentLinksUpdateCounter: assessmentsStoreState.attachmentLinksUpdateCounter,
    };
  },

  onAnswerSaveSuccess() {
    this.toast({
      title: `${_t('Answers saved')}.`,
      type: 'success',
    });
  },

  assessmentsSaveErrorToast() {
    this.toast({
      title: `${_t('Assessment saving failed')}.`,
      type: 'error',
    });
  },

  assessmentsSavedSuccessToast() {
    this.toast({
      title: `${_t('Assessment saved')}.`,
      type: 'success',
    });
  },

  render() {
    const { questionnaire, isJudgementLoaded } = this.state;
    if (!questionnaire || !isJudgementLoaded) {
      return <BusyFeedback />;
    }

    const { params, readOnly, location, history } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;
    const { assessment, documentTypesDictionary, answersWithErrors, currentOrganization,
      answersInProgress, isApplicationOnline, assessmentActions, judgements,
      evidenceAssociatedQuestions, attachmentLinksUpdateCounter } = this.state;
    const questionnaires = Object.keys(assessment.assessmentType.questionnaires);
    const assessmentTypeUuid = assessment.assessment.assessment_type;
    const documentTypes = documentTypesDictionary[assessmentTypeUuid];
    const answersHaveError = _.includes(answersWithErrors, true);

    return (
      <div className="questionnaire-editor-app">
        <EvidenceHistoryList
          assessmentUuid={assessmentUuid}
          readOnly={readOnly}
          params={params}
          history={history}
        />
        <br />
        <div className="row">
          <div className="col-xs-12">
            <QuestionnaireEditor
              attachmentLinksUpdateCounter={attachmentLinksUpdateCounter}
              assessment={assessment}
              documentTypes={documentTypes}
              questionnaire={questionnaire}
              readOnly={readOnly}
              showOutline
              currentOrganization={currentOrganization}
              location={location}
              params={params}
              history={history}
              assessmentActions={assessmentActions}
              answersInProgress={answersInProgress}
              judgements={judgements}
              evidenceAssociatedQuestions={evidenceAssociatedQuestions}
            />
          </div>
        </div>
        <div>
          {questionnaires.length > 1 && (
            <div className="row" style={{ textAlign: 'center' }}>
              <Link to={`/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/questionnaires`}>
               {_t('Go to all questionnaires')}
              </Link>
            </div>
          )}
        </div>
        <AnswerSaveError answersHaveError={answersHaveError} isApplicationOnline={isApplicationOnline} />
      </div>
    );
  },

});

module.exports = QuestionnaireEditorApp;
