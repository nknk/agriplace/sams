import AssessmentOverview from '../components/AssessmentOverview/AssessmentOverview.jsx';
const BusyFeedback = require('../components/BusyFeedback');

export const OverviewApp = React.createClass({

  propTypes: {
    params: React.PropTypes.object,
    navigationSections: React.PropTypes.array,
    role: React.PropTypes.string,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'ProductLinksStore'),
  ],

  componentDidMount() {
    this.getFlux().actions.productLinks.loadProductLinks(this.props.params.assessmentUuid);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const assessementsStoreState = flux.store('AssessmentsStore').getState();
    const productLinksStore = this.getFlux().store('ProductLinksStore');

    return {
      assessments: assessementsStoreState.assessments,
      assessmentTypes: assessementsStoreState.assessmentTypes,
      productLinks: productLinksStore.getState().productLinks,
    };
  },

  render() {
    const assessment = this.state.assessments[this.props.params.assessmentUuid] || {};
    if (!assessment.assessmentType) {
      return <BusyFeedback />;
    }
    const assessmentType = this.state.assessmentTypes[assessment.assessment.assessment_type] || {};
    const isProgressVisible = ~assessment.assessmentType.sections.indexOf('questionnaires');
    return (
      <div className="OverviewApp">
        <AssessmentOverview
          products={assessment.products}
          productLinks={this.state.productLinks}
          assessment={assessment}
          assessmentType={assessmentType}
          params={this.props.params}
          isProgressVisible={isProgressVisible}
          role={this.props.role}
        />
      </div>
    );
  },

});

module.exports = OverviewApp;
