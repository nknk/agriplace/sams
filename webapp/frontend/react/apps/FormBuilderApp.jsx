import React from 'react';
var FormBuilderInputJson = require('../components/FormBuilder/FormBuilderInputJson.jsx');
var FormBuilderInputHtml = require('../components/FormBuilder/FormBuilderInputHtml.jsx');
var FormBuilderOutput = require('../components/FormBuilder/FormBuilderOutput.jsx');
var BusyFeedback = require('../components/BusyFeedback');

var classnames = require('classnames');

var actions = require('../actions');
var stores = require('../stores');
var fluxInstance = new Fluxxor.Flux(stores, actions);

var FormBuilderApp = React.createClass({ // eslint-disable-line no-unused-vars

  mixins: [
    FluxMixin,
    StoreWatchMixin('FormStore'),
  ],

  PropTypes: {
    schemaUuid: React.PropTypes.string,
  },

  getInitialState() {
    return {
      schema: undefined,
      layout: undefined,
      options: undefined,
    };
  },

  shouldComponentUpdate(nextProps, nextState) {
    return !(_.isEqual(nextProps, this.props) && _.isEqual(nextState, this.state));
  },

  getStateFromFlux() {
    var flux = this.getFlux();
    var state = flux.store('FormStore').getState();
    var formData = state.schemas[this.props.schemaUuid] || {};

    return {
      schema: formData.schema,
      layout: formData.layout,
      options: formData.options,
      saveInProgress: state.schemaSaveInProgress,
      errors: state.errors,
    };
  },

  componentDidMount() {
    this.getFlux().actions.form.getSchema(this.props.schemaUuid);
  },

  onSchemaInputChange(schema) {
    this.setState({ schema });
  },

  onLayoutInputChange(layout) {
    this.setState({ layout });
  },

  onOptionsInputChange(options) {
    this.setState({ options });
  },

  handleValidationSuccess(type) {
    var { errors } = _.cloneDeep(this.state);

    if (errors !== undefined) {
      delete errors[type];
      this.setState({ errors: errors });
    }
  },

  handleValidationFail({ type, msg }) {
    var { errors } = _.cloneDeep(this.state);
    errors[type] = msg;
    this.setState({ errors: errors });
  },

  onSaveClick() {
    this.getFlux().actions.form.saveSchema(this.props.schemaUuid, {
      schema: this.state.schema,
      layout: this.state.layout,
      options: this.state.options,
    });
  },

  //
  // Render
  //

  render() {
    if (tools.isIE()) {
      return <div className="alert alert-danger">{_t('Internet Explorer is not supported')}</div>;
    }

    var saveButtonClassName = classnames({
      'icon fa fa-save': !this.state.schemaSaveInProgress,
      'icon fa fa-spinner fa-spin': this.state.schemaSaveInProgress,
    });

    return this.state.schema !== undefined ? (
      <div className="FormBuilderApp">

        <div className="toolbar wide">
            <a href="/forms/" className="btn btn-default"><i className="fa fa-long-arrow-left"></i></a>

            <Bootstrap.Button
              bsStyle="success"
              id="save-form-schema-button"
              className="pull-right"
              onClick={this.onSaveClick}
    >
              <i className={saveButtonClassName} />
              { '  ' + _t('Save') }
            </Bootstrap.Button>

            <div className="clearfix"></div>
        </div>

        <div className="row">
          <div className="col-xs-4">
            <Bootstrap.Tabs defaultActiveKey={1} animation={false}>
              <Bootstrap.Tab eventKey={1} title="Schema">
                <FormBuilderInputJson
                  onInputChange={this.onSchemaInputChange}
                  onValidationSuccess={this.handleValidationSuccess}
                  onValidationFail={this.handleValidationFail}
                  initSchema={this.state.schema}
                />
              </Bootstrap.Tab>
              <Bootstrap.Tab eventKey={2} title="Markup">
                <FormBuilderInputHtml
                  onInputChange={this.onLayoutInputChange}
                  onValidationSuccess={this.handleValidationSuccess}
                  onValidationFail={this.handleValidationFail}
                  initHtml={this.state.layout}
                />
              </Bootstrap.Tab>
              <Bootstrap.Tab eventKey={3} title="Options">
                <FormBuilderInputJson
                  onInputChange={this.onOptionsInputChange}
                  onValidationSuccess={this.handleValidationSuccess}
                  onValidationFail={this.handleValidationFail}
                  initSchema={this.state.options}
                />
              </Bootstrap.Tab>
            </Bootstrap.Tabs>
          </div>
          <div className="col-xs-8">
            {
              _.map(this.state.errors, (msg) => {
                return <div className="alert alert-danger">{msg}</div>;
              })
            }
            <FormBuilderOutput
              layout={this.state.layout}
              schema={this.state.schema}
              options={this.state.options}
              onValidationSuccess={this.handleValidationSuccess}
              onValidationFail={this.handleValidationFail}
            />
          </div>
        </div>
      </div>
    ) : (<BusyFeedback />);
  },
});

window.FormBuilderApp = function (elementId, schemaUuid) {
  elementId = elementId || 'app';
  React.render(
    <FormBuilderApp flux={fluxInstance} schemaUuid={schemaUuid} />,
    document.getElementById(elementId)
  );
};
