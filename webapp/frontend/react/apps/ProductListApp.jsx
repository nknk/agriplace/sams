import React from 'react';
const actions = require('../actions');
const stores = require('../stores');
const fluxInstance = new Fluxxor.Flux(stores, actions);
const ProductList = require('../components/ProductList/ProductList.jsx');
const ProductUpdate = require('../components/ProductUpdate/ProductUpdate.jsx');
const ProductTypeSelectApp = require('./ProductTypeSelectApp.jsx');

const ProductListApp = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('ProductsStore'),
  ],

  getInitialState() {
    return {
      selectedProductUuid: undefined,
    };
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const productsStore = flux.store('ProductsStore');
    const organizationsStore = this.getFlux().store('OrganizationsStore');

    return {
      products: productsStore.getProductsByYear(),
      traders: organizationsStore.getTraders(),
      years: productsStore.getState().years,
      selectedYear: productsStore.getState().selectedYear,
    };
  },

  componentDidMount() {
    this.getFlux().actions.products.loadProducts();
    this.getFlux().actions.organizations.loadTraders();
  },

  handleYearSelect(e) {
    this.getFlux().actions.products.changeYear({ year: +e.target.value });
  },

  handleProductSelect({ uuid }) {
    this.setState({ selectedProductUuid: uuid });
  },

  onDeleteProduct(uuid) {
    this.getFlux().actions.products.deleteProduct({ uuid });
    this.setState({ selectedProductUuid: undefined });
  },
  onCancel() {
    this.setState({ selectedProductUuid: undefined });
  },
  onSubmit({ product }) {
    this.getFlux().actions.products.updateProduct({ product, productUuid: product.uuid });
    this.setState({ selectedProductUuid: undefined });
  },

  render() {
    const product = this.state.products.find(p => p.uuid === this.state.selectedProductUuid);
    return (
      <div className="ProductListApp">
        {
          this.state.selectedProductUuid
          ? (
            <ProductUpdate
              product={ product }
              traders={ this.state.traders }
              years={ this.state.years }
              onDeleteProduct={this.onDeleteProduct}
              onCancel={this.onCancel}
              onSubmit={this.onSubmit}
            />
          )
          : (
            <div>
              <div className="row year" style={{ padding: 10 }}>
                <div className="col-xs-12">
                  <span>{_t('Year')} </span>
                  <select className="form-control" style={{ width: 'auto', display: 'inline-block' }} value={this.state.selectedYear} onChange={this.handleYearSelect}>
                    { _.map(this.state.years, (year) => <option key={year} value={year}>{year}</option>) }
                  </select>
                </div>
              </div>
              <ProductList
                onProductSelect={this.handleProductSelect}
                products={this.state.products}
              />
              <ProductTypeSelectApp />
            </div>
          )
        }
      </div>
    );
  },

});

app.ProductListApp = (id) => {
  var element = document.getElementById(id || 'app');
  React.render(<ProductListApp flux= { fluxInstance } />, element);
};
