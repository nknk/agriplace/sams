import React from 'react';
import BusyFeedback from '../components/BusyFeedback';
import { AssessmentReuseStatus } from '../components/AssessmentReuseStatus/AssessmentReuseStatus.jsx';

export const AssessmentReuseStatusApp = React.createClass({

  propTypes: {
    params: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore'),
  ],

  componentWillUpdate(nextProps, nextState) {
    const { params, location, history } = nextProps;
    const { assessment } = nextState;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;

    const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/detail`;

    if ((assessment.assessmentReuseStatus === 'done') && (url !== location.pathname)) {
      setTimeout(() => { history.pushState(null, url); }, 3000);
    }
  },

  getStateFromFlux() {
    const { params } = this.props;
    const assessmentUuid = params.assessmentUuid;
    const assessementsStoreState = this.getFlux().store('AssessmentsStore').getState();

    return {
      assessment: assessementsStoreState.assessments[assessmentUuid],
      assessmentReuseStatusInProgress: assessementsStoreState.assessmentReuseStatusInProgress,
    };
  },

  render() {
    const { assessment, assessmentReuseStatusInProgress } = this.state;
    const { params } = this.props;

    if (!assessment) {
      return <BusyFeedback />;
    }

    const loadAssessmentReuseStatus = this.getFlux().actions.assessments.loadAssessmentReuseStatus;

    return (
      <AssessmentReuseStatus
        status={assessment.assessmentReuseStatus}
        params={params}
        fetchStatus={loadAssessmentReuseStatus}
        fetchStatusInProgress={assessmentReuseStatusInProgress}
      />
    );
  },
});
