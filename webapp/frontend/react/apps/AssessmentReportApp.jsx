import React from 'react';
import { OfflineNotification } from '../components/OfflineNotification/OfflineNotification.jsx';
import { AssessmentReportHeader } from '../components/Header/AssessmentReportHeader.jsx';
const BusyFeedback = require('../components/BusyFeedback');
import { GeneralInfoTable } from '../components/AssessmentReport/GeneralInfoTable.jsx';
import { EvaluationResultsTable } from '../components/AssessmentReport/EvaluationResultsTable.jsx';
import { ShortcomingsTable } from '../components/AssessmentReport/ShortcomingsTable.jsx';
import '../components/AssessmentReport/AssessmentReport.less';

export const AssessmentReportApp = React.createClass({

  propTypes: {
    params: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore'),
  ],

  componentDidMount() {
    const flux = this.getFlux();
    const { assessmentUuid } = this.props.params;
    flux.actions.assessments.loadAssessmentReport(assessmentUuid);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const { assessmentUuid } = this.props.params;
    const assessementsStoreState = flux.store('AssessmentsStore').getState();

    return {
      report: assessementsStoreState.assessmentReports[assessmentUuid],
    };
  },

  render() {
    const { report } = this.state;

    if (!report) {
      return <BusyFeedback />;
    }

    const isLocal = window && window.location && (
      window.location.hostname === 'localhost'
      || window.location.hostname === '0.0.0.0'
      || window.location.hostname === '127.0.0.1'
    );

    return (
      <div className="assessment-report-app">
        {!isLocal && (
          <OfflineNotification />
        )}
        <AssessmentReportHeader title={report.name} />
        <GeneralInfoTable info={report.general_info} />
        <EvaluationResultsTable results={report.evaluation_results} />
        <ShortcomingsTable shortcomings={report.shortcoming_summary} />
      </div>
    );
  },
});
