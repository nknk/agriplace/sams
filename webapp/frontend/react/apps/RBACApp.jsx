import React from 'react';
const BusyFeedback = require('../components/BusyFeedback');
const RoleChooser = require('../components/RBAC/RoleChooser/RoleChooser.jsx');
const DataObjectsGrid = require('../components/RBAC/DataObjectsGrid/DataObjectsGrid.jsx');
const actions = require('../actions');
const stores = require('../stores');
const fluxInstance = new Fluxxor.Flux(stores, actions);

//
// Application component
//
const RBACApp = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('RbacStore'),
  ],


  componentDidMount() {
    const flux = this.getFlux();
    flux.actions.rbacRoles.loadData();
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const rbacStore = flux.store('RbacStore');
    const rbacRoles = rbacStore.getRoles();
    const rbacDataObjects = rbacStore.getDataObjects();
    const rbacPrivileges = rbacStore.getPrivileges();

    return {
      rbacRoles,
      rbacDataObjects,
      rbacPrivileges,
    };
  },

  //
  // Render
  //

  render() {
    if (_.isEmpty(this.state.rbacRoles)) {
      return <BusyFeedback />;
    }

    return (
      <div className="rbac-app">
        <div className="row">
          <div className="col-md-3">
            <RoleChooser roleList={this.state.rbacRoles} />
          </div>
          <div className="col-md-9">
            <DataObjectsGrid
              dataobjects={this.state.rbacDataObjects}
              privileges={this.state.rbacPrivileges}
            />
          </div>
        </div>
      </div>
    );
  },

});


// Use this global function to connect app to HTML
window.rbacApp = function (elementId) {
  elementId = elementId || 'app';
  React.render(
    <RBACApp
      flux={fluxInstance}
    />,
    document.getElementById(elementId)
  );
};
