import { Link } from 'react-router';

const AssessmentResults = React.createClass({
  propTypes: {
    assessment: React.PropTypes.object,
    params: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
  ],

  //
  // Render
  //

  render() {
    const assessment = this.props.assessment;
    const questionnaires = _.sortBy(assessment.assessmentType.questionnaires, 'order_index');
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = this.props.params;
    return (
      <div className="AssessmentResults">
        {questionnaires.map((questionnaire) => {
          const levels = assessment.getLevels(questionnaire);
          const questionnaireEditUrl = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/questionnaires/${questionnaire.uuid}/edit`;
          return (
            <div>
            <h2>
              <Link to={questionnaireEditUrl}>
                  {questionnaire.name}
              </Link>
              <small>
                  &nbsp;
                  {_t('questionnaire')}
              </small>
            </h2>
              <table className="table datatable contrast-datatable">
                  <thead>
                      <tr>
                          <th className="col-md-3">&nbsp;</th>
                          {
                            levels.map((level) => {
                              return <th className="col-md-2">{level.title}</th>;
                            })
                          }
                      </tr>
                  </thead>
                  <tbody>
                    {assessment.answerFilters.map((answerFilter) => {
                      const questions = _.filter(questionnaire.questions, question => question.is_statistics_valuable && assessment.getElementState(question.uuid).isVisible);
                      const calc = assessment.getLevelCalculations(questionnaire, answerFilter.code, questions, assessment.answers);

                      const questionnaireFilterUrl = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/questionnaires/${questionnaire.uuid}/edit?filter=${answerFilter.code}`;
                      return (
                        <tr>
                          <td>
                              <Link to={questionnaireFilterUrl}>
                                  {answerFilter.title}
                              </Link>
                          </td>
                            {
                             calc.map((filter) => {
                               return <td className="col-md-2">{filter.count}</td>;
                             })
                            }
                        </tr>
                      );
                    })
                  }
                  </tbody>
              </table>
              </div>
          );
        })}
      </div>
    );
  },

});

module.exports = AssessmentResults;
