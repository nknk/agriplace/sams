const assessmentStatistics = {
  answerFilters: [
    { code: 'noFilter', title: _t('Total number of questions') },
    { code: 'yes', title: _t('Questions answered with "Yes"') },
    { code: 'no', title: _t('Questions answered with "No"') },
    { code: 'not_applicable', title: _t('Questions answered with "N/A"') },
    { code: 'incomplete', title: _t('Unanswered questions') },
    { code: 'percent', title: _t('Percentage of questions answered with "Yes"') },
  ],

  getLevelCalculations(questionnaire, filter, questions, answers) {
    if (filter === 'percent') {
      return this.getPercent(questionnaire, questions, answers);
    }

    let filteredQuestions;
    let filteredAnswers;

    // select only relevant questions
    // if filter === 'noFilter' || 'incomplete' we dont need info about answers
    if (filter === 'noFilter') {
      filteredQuestions = questions;
    } else if (filter === 'incomplete') {
      filteredQuestions = questions.filter((question) => !this.isQuestionDone(question.uuid));
    } else {
      const questionUuids = _.map(questions, (question) => question.uuid);
      const answersByQuestionnaire = _.filter(answers, (answer) => ~questionUuids.indexOf(answer.question));
      filteredAnswers = _.filter(answersByQuestionnaire, (answer) => answer.value === filter);
      filteredQuestions = filteredAnswers.map(answer => questions[answer.question]);
    }

    const levels = this.getLevels(questionnaire);
    const levelsWithCount = this.count(levels, filteredQuestions);

    return levelsWithCount; // [{title: 'major': count:12}, {title: 'minor': count:16}];
  },

  getLevel(question, levels) {
    return levels.find(level => question.level_object === level.uuid) || levels[0];
  },

  count(levels, answeredQuestions) {
    levels = levels.map(level => _.assign(level, { count: 0 }));

    const calculations = _.reduce(answeredQuestions, (result, question) => {
      const level = _.find(result, (item) => {
        const answerCode = this.getLevel(question, levels).code;
        return item.code === answerCode;
      });

      if (level) {
        level.count++;
      }
      return result;
    }, levels);

    return calculations;
  },

  getLevels(questionnaire) {
    let levels = _.cloneDeep(questionnaire.question_levels);
    // Remove filter levels with empty titles
    levels = _.filter(levels, level => !!level.title);

    levels = _.map(levels, (questionLevel) => ({
      title: questionLevel.title,
      code: questionLevel.code,
      uuid: questionLevel.uuid,
      orderIndex: questionLevel.order_index,
    }));

    levels = _.sortBy(levels, 'orderIndex');

    return levels;
  },

  getPercent(questionnaire, questions, answers) {
    const yes = this.getLevelCalculations(questionnaire, 'yes', questions, answers);
    const all = this.getLevelCalculations(questionnaire, 'noFilter', questions, answers);
    const calculations = all.map((levelAll) => {
      const levelYes = _.find(yes, (item) => item.title === levelAll.title);
      return {
        title: levelAll.title,
        count: Math.round((levelYes.count / levelAll.count) * 1000) / 10,
      };
    });

    return calculations;
  },
};

export default assessmentStatistics;
