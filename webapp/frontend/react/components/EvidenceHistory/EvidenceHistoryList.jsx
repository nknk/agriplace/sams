import './EvidenceHistoryList.less';
import AnswerSaveError from '../QuestionnaireEditor/AnswerSaveError.jsx';
const _ = require('lodash');

module.exports = React.createClass({

  propTypes: {
    assessmentUuid: React.PropTypes.string.isRequired,
    readOnly: React.PropTypes.bool,
    params: React.PropTypes.object.isRequired,
    history: React.PropTypes.object.isRequired,
  },

  mixins: [
    Bootstrap.OverlayMixin,
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'ProductsStore'),
  ],

  getInitialState() {
    return {
      showImportModal: true,
      selectedAssessment: null,
    };
  },

  componentDidMount() {
    const { assessment } = this.state;
    const flux = this.getFlux();
    if (assessment.isNew() && !assessment.isEvidenceReuseAnswered && !this.props.readOnly) {
      flux.actions.assessments.loadPreviousAssessments(
        this.props.assessmentUuid,
        assessment.assessment.assessment_type
      );
      flux.actions.products.loadProducts();
    }
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const assessmentsStoreState = flux.store('AssessmentsStore').getState();
    const productsStoreState = flux.store('ProductsStore').getState();

    const previousNonEmptyAssessments = _.filter(
      assessmentsStoreState.previousAssessments, x => (
        x.assessment.attachment_link_count || x.assessment.justification_count
      )
    );

    return {
      previousAssessments: previousNonEmptyAssessments,
      assessment: assessmentsStoreState.assessments[this.props.assessmentUuid],
      products: productsStoreState.products,
      assessmentReuseFailed: assessmentsStoreState.assessmentReuseFailed,
      assessmentReuseInProgress: assessmentsStoreState.assessmentReuseInProgress,
      assessmentReuseSuccess: assessmentsStoreState.assessmentReuseSuccess,
    };
  },

  onReuseAccept() {
    const flux = this.getFlux();

    const targetAssessmentUuid = this.state.assessment.assessment.uuid;
    const sourceAssessment = this.state.selectedAssessment;
    const sourceAssessmentUuid = sourceAssessment && sourceAssessment.assessment.uuid;

    this.setState({ showImportModal: false });

    if (sourceAssessmentUuid) {
      flux.actions.assessments.assessmentReuse(sourceAssessmentUuid, targetAssessmentUuid);
    }

    flux.actions.assessments.answerEvidenceReuse(targetAssessmentUuid);
  },

  onSuccessOk() {
    const flux = this.getFlux();
    flux.actions.assessments.closeAssessmentReuseSuccessModal();
  },

  onRadioChange(assessment) {
    this.setState({
      selectedAssessment: assessment,
    });
  },

  getAssessmentProducts(assessment) {
    return assessment.assessment.products.map(prodUuid =>
      this.state.products.find(prod => prod.uuid === prodUuid).name
    ).join(', ');
  },

  renderPreviousAssessments() {
    const assessments = _.map(this.state.previousAssessments, (assessment) => (
      <Bootstrap.Input
        type="radio"
        key={assessment.uuid}
        label={`${assessment.assessment.name} ${this.getAssessmentProducts(assessment)}`}
        checked={this.state.selectedAssessment && this.state.selectedAssessment.uuid === assessment.uuid}
        onClick={this.onRadioChange.bind(null, assessment)}
      />
    ));

    assessments.unshift(
      <Bootstrap.Input
        checked={!this.state.selectedAssessment}
        type="radio"
        label={_t('No, thanks')}
        onClick={this.onRadioChange.bind(null, null)}
      />
    );

    return assessments;
  },

  renderModalButtons(onOk) {
    return (
      <div className="btn-toolbar" role="toolbar">
        <div className="btn-group pull-right">
          <Bootstrap.Button
            type="button"
            id="assessment-select-add-button"
            bsStyle="success"
            onClick={onOk}
          >
            {_t('OK')}
          </Bootstrap.Button>
        </div>
      </div>
    );
  },

  renderProgressModal() {
    return (
      <div className="row">
        <div className="col-xs-1">
          <span className="icon fa fa-spinner fa-spin fa-2x" />
        </div>
        <div className="col-xs-11 pull-left">
          {_t('The copying of the evidence and text justifications may take a few minutes')}
          {'. '}
          {_t('Please do not close this window')}
          {'.'}
        </div>
      </div>
    );
  },

  render() {
    const { previousAssessments, products, assessmentReuseInProgress,
      assessmentReuseFailed, showImportModal, assessment } = this.state;

    if (_.isEmpty(previousAssessments) || _.isEmpty(products)) {
      return null;
    }

    if (assessmentReuseFailed && !assessmentReuseInProgress) {
      return <AnswerSaveError answersHaveError isApplicationOnline />;
    }

    return (
      <div>
        <Bootstrap.Modal
          show={showImportModal && !assessment.isEvidenceReuseAnswered}
          onHide={null}
        >
          <Bootstrap.Modal.Header>
            <h2>{_t('Evidence reuse')}</h2>
          </Bootstrap.Modal.Header>
          <Bootstrap.Modal.Body>
            <div className="row">
              <div className="col-xs-12">
                {
                  `${
                    _t('Would you like to reuse evidence and text justifications from previous self-assessments')
                  }? ${
                    _t('Please make your choice below')
                  }:`
                }
              </div>
              <div className="col-xs-12">
                <div className="input-group">
                  <div className="radio">
                    {this.renderPreviousAssessments()}
                  </div>
                </div>
              </div>
            </div>
          </Bootstrap.Modal.Body>
          <Bootstrap.Modal.Footer>
            {this.renderModalButtons(this.onReuseAccept)}
          </Bootstrap.Modal.Footer>
        </Bootstrap.Modal>
        <Bootstrap.Modal show={assessmentReuseInProgress} backdrop={'static'} onRequestHide={null}>
          <Bootstrap.Modal.Body>
            {this.renderProgressModal()}
          </Bootstrap.Modal.Body>
        </Bootstrap.Modal>
        <Bootstrap.Modal show={false} backdrop={'static'} onRequestHide={null}>
          <Bootstrap.Modal.Header>
            <h2>{_t('Evidence reuse')}</h2>
          </Bootstrap.Modal.Header>
          <Bootstrap.Modal.Body>
            <div className="row">
              <div className="col-xs-12">
                {
                  `${
                    _t('The evidence and text justifications of the selected ' +
                      'self-assessment have been copied into this self-assessment')
                  }. ${
                    _t('Please check each individual evidence and text justification')
                  }. ${
                    _t('Update when necessary')
                  }. ${
                    _t('Agriforms require a new digital signature')
                  }.`
                }
              </div>
            </div>
          </Bootstrap.Modal.Body>
          <Bootstrap.Modal.Footer>
            {this.renderModalButtons(this.onSuccessOk)}
          </Bootstrap.Modal.Footer>
        </Bootstrap.Modal>
      </div>
    );
  },
});
