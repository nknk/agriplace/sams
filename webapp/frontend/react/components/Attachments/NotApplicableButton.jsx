import React, { PropTypes } from 'react';
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);

const AttachmentsUtils = require('../Utils/attachments.js');
const NOT_APPLICABLE_KEY = 'Not applicable';

const NotApplicableButton = React.createClass({

  mixins: [FluxMixin],

  propTypes: {
    isDisabled: React.PropTypes.bool,
    notApplicableAttachmentLink: React.PropTypes.array,
    realAttachmentLinks: React.PropTypes.array,
    assessmentUuid: React.PropTypes.string.isRequired,
    documentTypeUuid: React.PropTypes.string.isRequired,
  },

  handleChange(notApplicableAttachmentLink, event) {
    if (event.target.checked) {
      this.onChecked();
    } else {
      this.onUnchecked(notApplicableAttachmentLink);
    }
  },

  onUnchecked(notApplicableAttachmentLink) {
    var flux = this.getFlux();
    flux.actions.attachmentLinks.deleteAttachmentLink(notApplicableAttachmentLink.assessment, notApplicableAttachmentLink.id);
  },

  onChecked() {
    var tObj = {
      title: NOT_APPLICABLE_KEY,
      attachment_type: 'NotApplicableAttachment',
    };
    var attachment = AttachmentsUtils.objectToAttachment(tObj);

    var assessmentUuid = this.props.assessmentUuid;
    var documentTypeUuid = this.props.documentTypeUuid;

    var isDone = false;
    var attachmentLink = AttachmentsUtils.attachmentToAttachmentLink(
      attachment,
      assessmentUuid,
      documentTypeUuid,
      isDone
    );

    var flux = this.getFlux();
    flux.actions.attachmentLinks.createAttachmentLink(
      [attachmentLink],
      assessmentUuid
    );
  },


  render() {
    var isChecked = !!this.props.notApplicableAttachmentLink;
    var isDisabled = (this.props.realAttachmentLinks.length !== 0) || this.props.isDisabled;

    return (
      <Bootstrap.Input
        type="checkbox"
        label={_t('N/A')}
        checked={isChecked}
        disabled={isDisabled}
        onChange={this.handleChange.bind(this, this.props.notApplicableAttachmentLink)}
      />
    );
  },

});

module.exports = NotApplicableButton;
