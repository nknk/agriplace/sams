import React from 'react';

//
// Attachments List component
//
const AttachmentsList = React.createClass({

  propTypes: {
    attachmentLinks: React.PropTypes.arrayOf(React.PropTypes.object),
    readOnly: React.PropTypes.bool,
    onOpenAttachmentLink: React.PropTypes.func.isRequired,
  },

  render() {
    const { attachmentLinks } = this.props;

    if (attachmentLinks.length === 0) {
      return <small>{_t('No files or references added')}</small>;
    }

    const sortedAttchmentLinks = _.sortBy(attachmentLinks, (attachmentLink) => (
      attachmentLink.attachment.modified_time
    )).reverse();

    function isAgrifromAttachment(attachmentLink) {
      return attachmentLink.attachment.attachment_type == 'AgriformAttachment';
    }

    let agriformAttachmentsCount = attachmentLinks.filter(isAgrifromAttachment).length;

    const listItems = sortedAttchmentLinks.map((attachmentLink, itemIndex) => {
      let iconClass;
      let doneAgriformPart;
      let agriform_title = attachmentLink.attachment.title;
      switch (attachmentLink.attachment.attachment_type) {

      case 'AgriformAttachment':
        iconClass = 'fa-clipboard';
        doneAgriformPart = attachmentLink.is_done && <span><span className={"icon fa fa-check"}></span>&nbsp;</span>;
        agriform_title = agriform_title + '_' + agriformAttachmentsCount;
        agriformAttachmentsCount -= 1;
        break;

      case 'FormAttachment':
        iconClass = 'fa-clipboard';
        doneAgriformPart = attachmentLink.is_done && <span><span className={"icon fa fa-check"}></span>&nbsp;</span>;
        break;

      case 'FileAttachment':
        iconClass = 'fa-file';
        break;

      case 'TextReferenceAttachment':
        iconClass = 'fa-align-left';
        break;

      default:
        iconClass = 'fa-paperclip';
      }
      const attachmentButton = (
        <Bootstrap.Button
          bsStyle="link"
          bsSize="xsmall"
          style={{ 'text-align':'left' }}
          onClick={this.props.onOpenAttachmentLink.bind(null, attachmentLink)}
        >

          <span className={`icon fa ${iconClass}`}></span>
          &nbsp;
          {doneAgriformPart}
          {agriform_title}

        </Bootstrap.Button>
        );

      return (
        <li key={itemIndex}>
          {attachmentButton}
        </li>
      );
    });


    return (
      <ul className="attachments-list">
        {listItems}
      </ul>
    );
  },

});

module.exports = AttachmentsList;
