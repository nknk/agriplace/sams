import React from 'react';

const classNames = require('classnames');

//
// Attach Button component
//
const AttachButton = React.createClass({
  propTypes: {
    disabled: React.PropTypes.bool,
    onSelect: React.PropTypes.func,
  },

  //
  // Render
  //
  render() {

    const optionKeys = this.props.optionKeys || ['AGRIFORM', 'ARCHIVE_SELECT', 'FILE_UPLOAD', 'TEXT_REFERENCE', 'GENERIC_FORM'];

    // If the agriform option is not available, don't show it.
    var agriformMenuItem = null;
    if (optionKeys.indexOf('AGRIFORM') > -1) {
      agriformMenuItem = (
        <Bootstrap.MenuItem eventKey="AGRIFORM" btnStyle="primary">
          <span className="icon fa fa-plus"></span>
        &nbsp;
        { _t('Agriform') }
        </Bootstrap.MenuItem>
      );
    }

    var genericFormMenuItem = null;
    if (optionKeys.indexOf('GENERIC_FORM') > -1) {
      genericFormMenuItem = (
        <Bootstrap.MenuItem eventKey="GENERIC_FORM" btnStyle="primary">
          <span className="icon fa fa-plus"></span>
        &nbsp;
        { _t('Agriform') }
        </Bootstrap.MenuItem>
      );
    }

    var textReferenceMenuItem = (
      <Bootstrap.MenuItem eventKey="TEXT_REFERENCE">
        <span className="icon fa fa-plus"></span>
        &nbsp;
        {_t('Text reference')}
      </Bootstrap.MenuItem>
    );

    var archiveSelectMenuItem = (
      <Bootstrap.MenuItem eventKey="ARCHIVE_SELECT">
        <span className="icon fa fa-plus"></span>
      &nbsp;
            {_t('File from Archive')}
      </Bootstrap.MenuItem>
    );

    var fileUploadMenuItem = (
      <Bootstrap.MenuItem eventKey="FILE_UPLOAD">
        <span className="icon fa fa-plus"></span>
      &nbsp;
            {_t('File from your computer')}
      </Bootstrap.MenuItem>
    );

    return (
      <div>
        <Bootstrap.DropdownButton
          bsStyle="default"
          disabled={this.props.disabled}
          title={_t('Attach')}
          onSelect={this.props.onSelect}
    >

            {agriformMenuItem}

            {genericFormMenuItem}

            {textReferenceMenuItem}

            {archiveSelectMenuItem}

            {fileUploadMenuItem}

        </Bootstrap.DropdownButton>
      </div>
    );
  },
});

module.exports = AttachButton;
