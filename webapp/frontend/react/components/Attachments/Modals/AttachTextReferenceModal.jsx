import React from 'react';
const Bootstrap = require('react-bootstrap');
//
// Attach text reference modal
// Wraps text reference editor in an attachment modal
//
var AttachTextReferenceModal = React.createClass({
  mixins: [Bootstrap.OverlayMixin],

  propTypes: {
    isVisible: React.PropTypes.bool.isRequired,
    onSuccess: React.PropTypes.func.isRequired,
    onCancel: React.PropTypes.func.isRequired,
  },

  getInitialState() {
    return {
      text: '',

      // validation
      isValid: true,
      errors: {},
    };
  },


  //
  // Validates state
  //
  validate() {
    var isValid = this.state.isValid;
    var errors = {};

    // text

    if (!this.state.text) {
      isValid = false;
      errors.text = _t('You must fill text reference description.');
    }

    this.setState({
      isValid,
      errors,
    });

    return _.keys(errors).length == 0;
  },

  onSuccess() {
    if (this.validate()) {
      this.props.onSuccess(this.state.text);
    }
  },

  onTextChange(value) {
    this.setState({
      text: value,
    });
  },

  _onChange(event) {
    this.setState({
      text: event.target.value,
    });
  },

  render() {

    return (
      <Bootstrap.Modal show={this.props.isVisible} backdrop="static" title={ _t('Text reference') } onHide={this.props.onCancel}>
        <Bootstrap.Modal.Body>
          <textarea rows="10"
            onBlur={this._onChange} onChange={this._onChange}
          />
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>

          <Bootstrap.Button onClick={this.props.onCancel}>
              {_t('Cancel')}
            </Bootstrap.Button>

            <Bootstrap.Button bsStyle="primary" onClick = {this.onSuccess} >
              {_t('OK')}
            </Bootstrap.Button >
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  },


});

module.exports = AttachTextReferenceModal;

