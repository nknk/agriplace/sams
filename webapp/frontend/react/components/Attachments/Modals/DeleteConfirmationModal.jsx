import React, { PropTypes } from 'react';
import { Button, Modal } from 'react-bootstrap';
const { Header, Body, Footer } = Modal;
import './EditAttachmentModal.less';

export default class DeleteConfirmationModal extends React.Component {

  getCodes(questions) {
    return _.map(questions, question => {
      const questionnaireCodeExtract = tools.removePrefixFromString(question.questionnaire_code, '-');
      const questionCodeExtract = tools.removePrefixFromString(question.code, '-');
      return `${questionnaireCodeExtract}-${questionCodeExtract}`;
    }).join(', ');
  }

  render() {
    const { isVisible, onCancel, onDelete, questionCodes } = this.props;
    return (
      <Modal className="delete-modal" show={isVisible} backdrop={'static'} onRequestHide={onCancel} onHide={onCancel}>
        <Header onHide={onCancel} closeButton>
          <h2>
            <icon className="icon fa fa-exclamation-triangle delete-modal-warning-symbol" />
            <strong>{_t('Caution')}</strong>
          </h2>
        </Header>
        <Body>
          {_t('Please note that this evidence document is linked to the following questions')}
          {': '}
          {this.getCodes(questionCodes)}
          {'. '}
          {_t('It will be deleted from all these questions and could delete answers')}
          {'. '}
          {_t('Are you sure you want to delete this evidence document')}
          {'?'}
        </Body>
        <Footer>
          <div className="button-toolbar">
            <Button
              bsStyle="default"
              onClick={onCancel}
            >
              {_t('Cancel')}
            </Button>
            <Button
              bsStyle="success"
              onClick={onDelete}
            >
              {_t('OK')}
            </Button>
          </div>
        </Footer>
      </Modal>
    );
  }
}

DeleteConfirmationModal.propTypes = {
  questionCodes: PropTypes.array,
  isVisible: PropTypes.bool,
  onCancel: PropTypes.func,
  onDelete: PropTypes.func,
};
