import React from 'react';
const DatePicker = require('../../../DatePicker/DatePicker.jsx');
const Bootstrap = require('react-bootstrap');

var classNames = require('classnames');
var FileChooser = require('../../../FileChooser');

const ToastMixin = require('../../../../mixins/ToastMixin.js');


//
// Wraps file chooser in an attachment modal
//
// Props:
//  isVisible: the popup is displayed if this is truthy
//
//  onSuccess(attachments): called on OK with list of selected attachments
//  onCancel(): called when user closes dialog
//
//  datePicker props
var FileUploadModal = React.createClass({
  mixins: [
    Bootstrap.OverlayMixin,
    ToastMixin(),
  ],

  propTypes: {
    isVisible: React.PropTypes.bool.isRequired,

    onSuccess: React.PropTypes.func.isRequired,
    onCancel: React.PropTypes.func.isRequired,
  },

  defaults: {
        // selected files
    files: [],

    expirationDate: null,

        // file chooser state
    isFileChooserVisible: false,

        // Expires never or on date
    validityType: 'never',

        // validation
    isValid: true,
    errors: {},
  },

  getInitialState() {
    return this.defaults;
  },

  componentWillReceiveProps() {
    this.setState(this.defaults);
  },

    //
    // Validates state
    //

  validate() {
    var isValid = this.state.isValid;
    var errors = {};

      // Files

    if (this.state.files.length == 0) {
      isValid = false;
      errors.files = _t('The selected file exceeds the limit of 10 MB') + '. ' + _t('Please select another file or compress the current file');
    }

      // Validity type

    if (! this.state.validityType) {
      isValid = false;
      errors.validityType = _t('The expiry date is missing');
    }

    if (this.state.validityType === 'date') {
      if (! this.state.expirationDate) {
        isValid = false;
        errors.validityType = _t('The expiry date is missing');
      } else if (! tools.validateDate(this.state.expirationDate)) {
        isValid = false;
        errors.validityType = _t('The expiry date needs to be in the following format') + ': ' + pageData.dateFormat.toLowerCase();
      } else if (! tools.isFutureDate(this.state.expirationDate)) {
        isValid = false;
        errors.validityType = _t('An expiry date needs to be a future date');
      }
    }

    this.setState({
      isValid,
      errors,
    });

    return _.keys(errors).length == 0;
  },


    //
    // Handlers
    //
  onFileChooserChange(files) {
    this.state.errors.files = '';
    this.setState({
      files,
    });
  },

  onSuccess() {
    if (this.validate()) {
      var date = (this.state.validityType == 'date') ? this.state.expirationDate : null;
      this.props.onSuccess(this.state.files, this.state.validityType, date);
      var fileText = (this.state.files.length > 1)
          ? _t('files are')
          : _t('file is');
      this.toast(_t('Your') + ' ' + fileText + ' ' + _t('being added to your archive'));
    }
  },

  onNeverExpiry() {
    this.state.errors.validityType = '';
    this.setState({
      validityType  : 'never',
    });
  },

  onEnterExpiry(event) {
    this.setState({
      validityType  : 'date',
    });
  },

  onDateSelect(date) {
    this.setState({
      expirationDate: date,
      validityType: 'date',
    });
  },

    //
    // Render
    //

  render() {

    var title = _t('Upload file');

    return (

        <Bootstrap.Modal id="modal22" show={this.props.isVisible} title={title} onHide={this.props.onCancel} backdrop="static">
          <Bootstrap.Modal.Body>

            <form className="form-horizontal">

              <div
                className={classNames(
                  'form-group',
                  { 'has-error': this.state.errors.files }
                )}
              >
                <label className="control-label col-sm-2">
                  {_t('File')}
                </label>

                <div className="col-sm-10">

                  <div className="help-block">
                    {this.state.errors.files}
                  </div>

                 <FileChooser
                   isVisible={false}
                   onChange={this.onFileChooserChange}
                 />

                </div>
              </div>


              <div
                className={classNames(
                  'form-group',
                  { 'has-error': this.state.errors.validityType }
                )}
              >

                <label className="control-label col-sm-2">
                  {_t('Expiry date')}
                </label>

                <div className="col-sm-10">

                  <div className="help-block">
                    {this.state.errors.validityType}
                  </div>

                  <p>
                    {_t('Please indicate when the validity of this document expires')}:
                  </p>

                  <div className="form-group">
                    <label className="control-label">
                      <input type="radio"
                        name="validity_type"
                        checked={this.state.validityType === 'never'}
                        onChange={ this.onNeverExpiry }
                      />
                      { ' ' + _t('Never')}
                    </label>
                  </div>

                  <div className="form-group">

                    <label>
                      <input type="radio"
                        style={{ marginRight: '5px' }}
                        checked={this.state.validityType === 'date'}
                        onChange={ this.onEnterExpiry }
                      />
                      <span style={{ marginRight: '5px' }}>
                        {_t('Valid until')}:
                      </span>
                    </label>
                    &nbsp;

                    <DatePicker
                      options={{
                        format: pageData.dateFormat,
                        startDate: '0',
                        language: pageData.languageCode,
                      }}
                      onDateSelect= { this.onDateSelect }
    />

                  </div>

                </div>

              </div>

            </form>

          </Bootstrap.Modal.Body>

          <Bootstrap.Modal.Footer>


            <Bootstrap.Button onClick={this.props.onCancel}>
              {_t('Cancel')}
            </Bootstrap.Button>

            <Bootstrap.Button bsStyle="primary" onClick = {this.onSuccess} >
              {_t('Ok')}
            </Bootstrap.Button >

          </Bootstrap.Modal.Footer>

        </Bootstrap.Modal>
      );
  },


});

module.exports = FileUploadModal;
