import React, { PropTypes } from 'react';
import { Button, Modal, Alert, OverlayMixin } from 'react-bootstrap';
const { Header, Body, Footer } = Modal;
import classNames from 'classnames';
import './EditAttachmentModal.less';
import { EditGenericFormAttachment } from '../AttachmentEditors/EditGenericFormAttachment.jsx';
const EditAgriformAttachment = require('../AttachmentEditors/EditAgriformAttachment.jsx');
const EditFileAttachment = require('../AttachmentEditors/EditFileAttachment.jsx');
const EditTextReferenceAttachment = require('../AttachmentEditors/EditTextReferenceAttachment.jsx');
import DeleteConfirmationModal from './DeleteConfirmationModal.jsx';

//
// Generic edit attachment modal. Calls specific editor depending on attachment type.
//
const EditAttachmentModal = React.createClass({
  propTypes: {
    attachment: PropTypes.object,
    isDescriptionReadOnly: PropTypes.bool,
    isDeleteReadOnly: PropTypes.bool,
    isVisible: PropTypes.bool.isRequired,
    onSuccess: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    isDeleteConfirmationRequired: PropTypes.bool,
    docTypeAssociatedQuestions: PropTypes.array,
  },

  mixins: [
    OverlayMixin,
  ],

  getInitialState() {
    const { attachment } = this.props;
    return {
      attachment,
      modifiedAttachment: _.cloneDeep(attachment),

      // validation
      isValid: true,
      errors: {},
      agriform_reference: attachment.agriform_reference,
      showDeleteConfirmationModal: false,
    };
  },

  //
  // Handlers
  //

  onAttachmentChange(modifiedAttachment) {
    const { attachment } = this.props;
    if (modifiedAttachment.title !== attachment.title) {
      this.setState({
        attachment,
        modifiedAttachment,
      });
    }
  },

  onGenericFormChange(modifiedAttachment) {
    const { attachment } = this.props;
    this.setState({ modifiedAttachment, attachment });
  },

  onSuccess() {
    const { modifiedAttachment } = this.state;
    const isAgriform = modifiedAttachment.attachment_type === 'AgriformAttachment';
    if (this.validate()) {
      if (isAgriform) {
        modifiedAttachment.agriform_reference = this.refs.EditAgriformAttachmentRef.state.agriform_reference;
      }
      this.props.onSuccess(modifiedAttachment);
    }
  },

  onCancel() {
    const { attachment, onCancel } = this.props;
    onCancel(attachment);
    this.setState({ showDeleteConfirmationModal: false });
  },

  onDelete() {
    const { attachment, onDelete } = this.props;
    onDelete(attachment);
    this.setState({ showDeleteConfirmationModal: false });
  },

  showConfirmationDeleteModal() {
    this.setState({ showDeleteConfirmationModal: true });
  },

  // Validates state
  //
  validate() {
    let isValid = this.state.isValid;
    const errors = {};

    // text

    if (!this.state.modifiedAttachment.title) {
      isValid = false;
      errors.text = _t('You must fill in a text reference.');
    }

    this.setState({
      isValid,
      errors,
    });

    return _.keys(errors).length === 0;
  },

  // Warns, if there any linked closed assessents
  getWarningNode(linkedAssessments) {
    const closedAssessments = linkedAssessments.filter((linkedAssessment) => linkedAssessment.is_closed);

    return closedAssessments.length !== 0 ? (
      <Alert bsStyle="warning">
        <span>{_t('You cannot modify this attachment because the following assessments are closed ')}:&nbsp;</span>
        {
          closedAssessments.map((closedAssessment) => (
            <a target="_blank" href={closedAssessment.url}>{closedAssessment.name} </a>
          ))
        }
      </Alert>
    ) : null;
  },

  render() {
    const { isDescriptionReadOnly, isDeleteConfirmationRequired, docTypeAssociatedQuestions } = this.props;
    const { modifiedAttachment, showDeleteConfirmationModal } = this.state;

    let attachmentEditorPart = 'No editor attached';
    let title = _t('Attachment');
    const isAgriform = ~['AgriformAttachment', 'FormAttachment'].indexOf(modifiedAttachment.attachment_type);

    switch (modifiedAttachment.attachment_type) {

    case 'AgriformAttachment':
      title = _t('Agriform');
      attachmentEditorPart = (
        <div>
          <EditAgriformAttachment
            attachment={modifiedAttachment}
            readOnly={isDescriptionReadOnly}
            onChange={this.onAttachmentChange}
            ref="EditAgriformAttachmentRef"
          />
        </div>
      );
      break;

    case 'FileAttachment':
      title = _t('File');
      attachmentEditorPart = (
        <EditFileAttachment
          attachment={modifiedAttachment}
          readOnly={isDescriptionReadOnly}
          onChange={this.onAttachmentChange}
        />
      );
      break;

    case 'TextReferenceAttachment':
      title = _t('Text reference');
      attachmentEditorPart = (
        <EditTextReferenceAttachment
          attachment={modifiedAttachment}
          readOnly={isDescriptionReadOnly}
          onChange={this.onAttachmentChange}
        />
      );
      break;

    case 'FormAttachment':
      title = _t('Agriform');
      attachmentEditorPart = (
        <EditGenericFormAttachment
          attachment={modifiedAttachment}
          readOnly={isDescriptionReadOnly}
          onChange={this.onGenericFormChange}
        />
      );
      break;
      // no default
    }

    const cancelBtnPart = !isAgriform ? (
      <Button onClick={this.props.onCancel}>
        {_t('Cancel')}
      </Button>
    ) : null;

    const btnSize = classNames({ large: isAgriform });
    const acceptBtnPart = (
      <Button
        bsStyle="primary"
        onClick={this.onSuccess}
        bsSize={btnSize}
      >
        {_t('OK')}
      </Button>
    );

    const deleteBtnPart = (
      <Button
        className="pull-left"
        disabled={this.props.isDeleteReadOnly}
        onClick={isDeleteConfirmationRequired ? this.showConfirmationDeleteModal : this.onDelete}
      >
        {_t('Delete')}
      </Button>
    );

    const showAttachmentModal = !(isDeleteConfirmationRequired && showDeleteConfirmationModal);

    return (
      <div className="EditAttachmentModal">
        <Modal
          show={showAttachmentModal}
          bsSize="large"
          backdrop="static"
          onHide={this.onCancel}
          dialogClassName="EditAttachmentModal"
        >
          <Header>
            <div className="pull-right">
              <Button
                type="button"
                className="close pull-right"
                onClick={this.props.onCancel}
              >
                <span aria-hidden="true">&times;</span>
              </Button>
            </div>
            <div className="modal-title">{title}</div>
          </Header>
          <Body>
              {this.getWarningNode(this.props.attachment.used_in_assessments)}
              {attachmentEditorPart}
          </Body>
          <Footer>
            {deleteBtnPart}
            {cancelBtnPart}
            {acceptBtnPart}
          </Footer>
        </Modal>
        {isDeleteConfirmationRequired && (
          <DeleteConfirmationModal
            questionCodes={docTypeAssociatedQuestions}
            isVisible={showDeleteConfirmationModal}
            onCancel={this.props.onCancel}
            onDelete={this.props.onDelete}
          />
        )}
      </div>
    );
  },
});

module.exports = EditAttachmentModal;
