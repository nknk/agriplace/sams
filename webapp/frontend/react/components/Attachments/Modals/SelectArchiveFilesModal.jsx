import React from 'react';
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

const _ = require('lodash');

require('./SelectArchiveFilesModal.less');

const Alert = require('../../Utils/Alert.jsx');
const FilterableListView = require('../../FilterableListView/FilterableListView.jsx');
const Modal = require('../../Utils/Modal.jsx');
const ModalButtons = require('../../Utils/ModalButtons.jsx');

// TODO: add 'Used in Assessment' column later, as requested by Maarten.
let listViewProps = require('../../Utils/archiveListViewProps.js');

_.extend(listViewProps, {
  showCheckBox: true,
});

let queryInputProps = {
  placeholder: _t('Search'),
  showSearchIcon: true,
  showEraserIcon: true,
};

//
// Select archive files modal
//
const SelectArchiveFilesModal = React.createClass({

  propTypes: {
    visible: React.PropTypes.bool.isRequired,

    onSuccess: React.PropTypes.func.isRequired,
    onCancel: React.PropTypes.func.isRequired,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AttachmentsStore'),
  ],


  getInitialState() {
    return {
      selected: [],
      alert: false,
    };
  },

  componentDidMount() {
    listViewProps.onItemClicked = this.onItemClicked;
    this.getFlux().actions.attachments.loadAttachmentsOfType('FileAttachment');
  },

  getStateFromFlux() {
    const attachmentStoreState = this.getFlux().store('AttachmentsStore').getState();

    const attachments = attachmentStoreState.attachments;

    attachments.forEach(attachment => {
      attachment.used_in = attachment.used_in_assessments.map(usedInAssessment => (
        usedInAssessment.name
      )).join(', ');
    });

    return {
      attachments,
    };
  },


  //
  // Handlers
  //

  onItemClicked(item) {
    const selected = (~listViewProps.selected.indexOf(item))
      ? _.without(listViewProps.selected, item)
      : listViewProps.selected.concat(item);

    this.setState({
      selected,
      alert: false,
    });
  },


  onSuccess(event) {
    // prevent adding without selection by showing a notification
    if (_.isEmpty(this.state.selected)) {
      this.setState({ alert: true });
    } else {
      this.props.onSuccess(this.state.selected);
    }
  },


  onCancel() {
    this.props.onCancel();
  },


  //
  // Render
  //
  render() {
    if (!this.props.visible) {
      return null;
    }

    listViewProps.items = this.state.attachments;
    listViewProps.selected = this.state.selected;

    //
    // Header
    //

    const header = (
      <div className="modal-title">{_t('Archive')}</div>
    );

    //
    // Body
    //

    let noSelectionAlertPart;
    if (this.state.alert) {
      noSelectionAlertPart = (
        <Alert alertText={_t('Please select one or more files to attach first')} />
      );
    }

    const body = (
      <div className="select-archive-files-modal-body">
        <FilterableListView
          listViewOptions={listViewProps}
          queryInputOptions={queryInputProps}
        />
        {noSelectionAlertPart}
      </div>
    );

    //
    // Footer
    //

    const footer = (
      <ModalButtons
        onSuccess={this.onSuccess}
        onCancel={this.onCancel}
      />
    );

    return (
      <Modal
        header={header}
        body={body}
        footer={footer}
        backdrop="static"
        bsSize="large"
        onCancel={this.onCancel}
      />
    );
  },
});

module.exports = SelectArchiveFilesModal;

//  usage:
//
//  <SelectArchiveFilesModal
//    visible   = { true/false }
//    onSuccess = { aSuccessHandler }
//    onCancel  = { aCancelHandler }
//  />

