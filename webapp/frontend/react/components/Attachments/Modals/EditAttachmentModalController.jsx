import React, { Component, PropTypes } from 'react';
const EditAttachmentModal = require('./EditAttachmentModal.jsx');

export class EditAttachmentModalController extends Component {
  constructor() {
    super();
    this.state = {
      isVisible: false,
    };
    this.onCancel = this.onCancel.bind(this);
    this.openModal = this.openModal.bind(this);
  }

  onCancel() {
    this.setState({ isVisible: false });
  }

  openModal() {
    this.setState({ isVisible: true });
  }

  render() {
    if (!this.state.isVisible) {
      return null;
    }
    const { attachment, isDescriptionReadOnly, isDeleteReadOnly, onAttachmentModalSuccess,
      onAttachmentDelete, isDeleteConfirmationRequired, docTypeAssociatedQuestions } = this.props;

    return (
      <EditAttachmentModal
        isDescriptionReadOnly={isDescriptionReadOnly}
        isDeleteReadOnly={isDeleteReadOnly}
        onCancel={this.onCancel}
        onSuccess={onAttachmentModalSuccess}
        onDelete={onAttachmentDelete}
        attachment={attachment}
        isDeleteConfirmationRequired={isDeleteConfirmationRequired}
        docTypeAssociatedQuestions={docTypeAssociatedQuestions}
      />
    );
  }
}
EditAttachmentModalController.propTypes = {
  isDescriptionReadOnly: PropTypes.bool,
  isDeleteReadOnly: PropTypes.bool,
  attachment: PropTypes.object,
  onAttachmentModalSuccess: PropTypes.func,
  onAttachmentDelete: PropTypes.func,
  isDeleteConfirmationRequired: PropTypes.bool,
  docTypeAssociatedQuestions: PropTypes.array,
};
