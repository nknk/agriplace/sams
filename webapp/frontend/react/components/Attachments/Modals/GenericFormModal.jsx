import React from 'react';
import GenericForms from '../../Agriforms/';

export class GenericFormModal extends React.Component {
  onSuccess() {
    this.props.onSuccess({
      value: this.refs.form.getValue(),
      isSigned: this.refs.sign.getChecked(),
    });
  }

  render() {
    const GenericForm = GenericForms[this.props.genericFormSlug];

    return (
      <Bootstrap.Modal show bsSize={'lg'} backdrop="static" onHide={this.props.onCancel} dialogClassName="GenericFormModal">
        <Bootstrap.Modal.Header>
          <div className="pull-right">
            <Bootstrap.Button
              type = "button"
              className = "close pull-right"
              onClick = { this.props.onCancel }
    >
              <span aria-hidden="true">
              &times;
              </span>
            </Bootstrap.Button>
          </div>
          <div className="modal-title">{ _t('Agriform') }</div>
        </Bootstrap.Modal.Header>
        <Bootstrap.Modal.Body>
          <GenericForm ref="form" />
          <br />
          <Bootstrap.Input
            ref="sign"
            type="checkbox"
            disabled={this.props.readOnly}
            label={_t('Hereby I declare to have filled in the above form according to today’s reality') + '.'}
          />
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick = {this.onSuccess.bind(this)} >
            {_t('OK')}
          </Bootstrap.Button>
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}
