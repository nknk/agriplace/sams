import React from 'react';
const BusyFeedback = require('../../BusyFeedback');
const QuestionnaireEditor = require('../../QuestionnaireEditor/QuestionnaireEditor.jsx');
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;
import AnswerSaveError from '../../QuestionnaireEditor/AnswerSaveError.jsx';

//
// Editor for Agriform attachments
//
const EditAgriformAttachment = React.createClass({
  propTypes: {
    attachment: React.PropTypes.object.isRequired,
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'OfflineNotificationStore'),
  ],

  componentWillMount() {
    this.state.agriform_reference = this.props.attachment.agriform_reference;
  },

  componentDidMount() {
    // Reload agriform attachment structure+data
    const { attachment } = this.props;
    const flux = this.getFlux();
    const assessmentsStore = flux.stores.AssessmentsStore.getState();
    if (!assessmentsStore.assessments[attachment.agriformUuid]) {
      flux.actions.assessments.loadAssessmentFull(attachment.agriformUuid);
    }
  },

  getStateFromFlux() {
    const { attachment } = this.props;
    const flux = this.getFlux();
    const assessmentsStoreState = flux.stores.AssessmentsStore.getState();
    const assessment = assessmentsStoreState.assessments[attachment.agriformUuid];
    const answersInProgress = assessmentsStoreState.answersInProgress[attachment.agriformUuid];
    const answersWithErrors = assessmentsStoreState.answersWithErrors[attachment.agriformUuid] || {};
    const offlineNotificationStoreState = flux.store('OfflineNotificationStore');
    const isApplicationOnline = offlineNotificationStoreState.status === 'online';

    return {
      assessment,
      answersInProgress,
      answersWithErrors,
      isApplicationOnline,
    };
  },

  handleCheckboxClick(assessmentUuid, questionnaireUuid, e) {
    const isSigned = e.target.checked;
    const flux = this.getFlux();
    flux.actions.agriform.setSignature(assessmentUuid, questionnaireUuid, isSigned);
  },

  handleReferencePart(e) {
    this.state.agriform_reference = e.target.value;
  },

  render() {
    const { assessment, answersInProgress, answersWithErrors, isApplicationOnline } = this.state;
    const { attachment } = this.props;

    const questionnaireUuid = _.first(_.keys(assessment && assessment.assessmentType
      && assessment.assessmentType.questionnaires));
    if (!assessment || !assessment.assessmentType || !assessment.questionnaireState[questionnaireUuid]) {
      return (
        <BusyFeedback />
      );
    }

    const answersHaveError = _.includes(answersWithErrors, true);
    if (answersHaveError) {
      return <AnswerSaveError answersHaveError={answersHaveError} isApplicationOnline={isApplicationOnline} />;
    }

    const flux = this.getFlux();
    const assessmentActions = flux.actions.assessments;
    const questionnaire = _.first(_.values(assessment.assessmentType.questionnaires));
    const isSignatureRequired = questionnaire.requires_signature;
    const isReferenceRequired = questionnaire.requires_reference;

    const warningPart = isSignatureRequired ? (
      <Bootstrap.Input
        type="checkbox"
        onChange={this.handleCheckboxClick.bind(this, attachment.agriformUuid, questionnaireUuid)}
        disabled={this.props.readOnly}
        label={`${_t('Hereby I declare to have filled in the above form according to today’s reality')}.`}
        checked={assessment.questionnaireState[questionnaireUuid].isSigned}
      />
    ) : null;

    const referencePart = isReferenceRequired ? (
      <Bootstrap.Input
        ref="agriform_reference"
        type="text"
        onChange={this.handleReferencePart}
        defaultValue={this.state.agriform_reference}
      />
    ) : null;

    const smallFont = {
      'font-weight': 'normal',
    };

    const referencePartLabel = isReferenceRequired ? (
      <label htmlFor="agriform_reference">
        {`${_t('Agriform reference ')}`}
        <span style={smallFont}>
          {`${_t('(this will be part of the agriform name so that you can easily identify it)')}`}
        </span>
      </label>
    ) : null;

    return (
      <div className="edit-agriform-attachment">
        <h2>{questionnaire.name}</h2>
        <QuestionnaireEditor
          showOutline={false}
          assessment={assessment}
          documentTypes={{}}
          isWithFilter={false}
          questionnaire={questionnaire}
          readOnly={this.props.readOnly}
          showElementCodes={false}
          assessmentActions={assessmentActions}
          answersInProgress={answersInProgress}
        />
        {warningPart}
        <br />
        {referencePartLabel}
        {referencePart}
      </div>
    );
  },

});

module.exports = EditAgriformAttachment;
