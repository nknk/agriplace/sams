import React from 'react';
import GenericForms from '../../Agriforms/';

export class EditGenericFormAttachment extends React.Component {
  handleChange() {
    if (this.refs.form) {
      let { attachment } = this.props;
      const value = this.refs.form.getValue();
      attachment.generic_form_data.json_data = value;

      this.props.onChange(attachment);
    }
  }

  handleSignatureClick() {
    let { attachment } = this.props;
    attachment.is_done = !attachment.is_done;

    this.props.onChange(attachment);
  }

  render() {
    const GenericForm = GenericForms[this.props.attachment.generic_form_data.slug];

    return (
      <div>
        <GenericForm
          onChange={this.handleChange.bind(this)}
          readOnly={this.props.readOnly}
          ref="form"
          value={this.props.attachment.generic_form_data.json_data}
        />
        <Bootstrap.Input
          type="checkbox"
          onChange={this.handleSignatureClick.bind(this)}
          disabled={this.props.readOnly}
          label={_t('Hereby I declare to have filled in the above form according to today’s reality') + '.'}
          checked = {this.props.attachment.is_done}
        />
      </div>
    );
  }
}
