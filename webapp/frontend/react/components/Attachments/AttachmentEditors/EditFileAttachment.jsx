import React from 'react';

require('../attachments.less');

const FileAttachmentPreview = require('../FileAttachmentPreview.jsx');

const EditFileAttachment = React.createClass({

  propTypes: {
    attachment: React.PropTypes.object,
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func,
  },

  getInitialState() {
    // We only keep the text in state so we can allow the user
    // to update it locally and only send up an event on blur.
    let text;
    if (this.props.attachment) {
      text = this.props.attachment.title || '';
    } else {
      log.info('no attachment');
      text = '';
    }

    return {
      text,
    };
  },

  //
  // Handlers
  //

  onBlur(event) {
    const attachment = this.props.attachment;
    attachment.title = this.state.text;
    this.props.onChange(attachment);
  },

  onChange(event) {
    this.setState({
      text: event.target.value,
    });
  },

  //
  // Render
  //
  render() {
    const attachmentExtension = this.props.attachment.original_file_name.split('.').pop(-1);
    const attachmentDownloadName = `${this.props.attachment.title}.${attachmentExtension}`;
    const attachmentUrl = `/in/downloads/${this.props.attachment.uuid}/${encodeURIComponent(attachmentDownloadName)}`;

    return (

      <div className="row">

        <div className="col-md-9">
          <FileAttachmentPreview
            attachmentUrl={attachmentUrl}
            attachmentExtension={attachmentExtension}
            attachmentDownloadName={attachmentDownloadName}
          />
        </div>

        <div className="col-md-3">
          <div className="page-section">
            <section>
              <form name="editForm" role="form">
                <div className="form-group">
                  <label>
                    {_t('Set new title')}
                  </label>
                  <textarea
                    className="form-control"
                    disabled={this.props.readOnly}
                    rows="5"
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                    value={this.state.text}
                  />
                </div>
              </form>
            </section>
          </div>
          <div className="list-group">
            <a href={attachmentUrl} target="_self" className="list-group-item">
              <span className="fa fa-download"></span>
              &nbsp;
              <span>{_t('Download')}</span>
            </a>
          </div>
          {attachmentExtension === 'pdf' ?
            <div className="list-group">
              <a
                href={`${this.props.attachment.file}`}
                target="_blank"
                className="list-group-item"
              >
                <span className="fa fa-eye"></span>
                &nbsp;
                <span>{_t('View')}</span>
              </a>
            </div>
            : null
          }
        </div>
      </div>
    );
  },

});

module.exports = EditFileAttachment;
