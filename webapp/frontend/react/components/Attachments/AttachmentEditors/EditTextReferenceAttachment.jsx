import React from 'react';

require('../attachments.less');

//
// Text reference attachment editor
//
// Payload is of form {text}
//
const TextReferenceAttachment = React.createClass({

  propTypes: {
    attachment: React.PropTypes.object,
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func,
  },

  getInitialState() {
    // log('text reference attachment', this.props);
    // We only keep the text in state so we can allow the user
    // to update it locally and only send up an event on blur.
    let text;
    if (this.props.attachment) {
      text = this.props.attachment.description || '';
    } else {
      log.info('no attachment');
      text = '';
    }
    return {
      text,
    };
  },

  // A parent re-render will replace the current attachment state, losing changes.
  componentWillReceiveProps(newProps) {
    // only nuke the state if we just received a new attachment to edit
    if (newProps.attachment !== this.props.attachment) {
      let text;
      if (newProps.attachment) {
        text = newProps.attachment.description || '';
      } else {
        text = '';
      }
      this.setState({
        text,
      });
      log.info('CHANGING TEXT REFERENCE STATE', text);
    }
  },

  //
  // Handlers
  //

  onBlur(event) {
    // Return an attachment object based on the one passed in (if present).
    const attachment = this.props.attachment;
    const title = (this.state.text.length > 20) ? `${this.state.text.slice(0, 20)}...` : this.state.text;
    attachment.description = this.state.text;
    attachment.title = title;

    this.props.onChange(attachment);
  },

  onChange(event) {
    if (this.state.text !== event.target.value) {
      this.setState({
        text: event.target.value,
      });
    }
  },

  //
  // Render
  //
  render() {
    return (

      <textarea
        rows="10"
        disabled={this.props.readOnly}
        onChange={this.onChange}
        onBlur={this.onBlur}
        value={this.state.text}
      />

    );
  },

});

module.exports = TextReferenceAttachment;

