import React from 'react';
const AttachButton = require('../Attachments/AttachButton.jsx');
const FileUploadModal = require('./Modals/Attach/FileUploadModal.jsx');
const AttachmentsUtils = require('../Utils/attachments.js');
import { GenericFormModal } from './Modals/GenericFormModal.jsx';
const SelectArchiveFilesModal = require('./Modals/SelectArchiveFilesModal.jsx');
const AttachTextReferenceModal = require('./Modals/AttachTextReferenceModal.jsx');

//
// Attach Button component
//
const AttachmentCreator = React.createClass({
  propTypes: {
    readOnly: React.PropTypes.bool,
    optionKeys: React.PropTypes.array,
    genericForm: React.PropTypes.object,
    onCreateAttachments: React.PropTypes.func.isRequired,
    onCreateAgriform: React.PropTypes.func.isRequired,
  },

  getInitialState() {
    return {
      isArchiveChooserVisible: false,
      isTextReferenceAttachVisible: false,
      isFileUploadVisible: false,
    };
  },

  //
  // Handlers
  //

  onAttachButton(e, eventKey) {
    switch (eventKey) {

    case 'AGRIFORM':
      this.requestAgriform();
      break;

    case 'GENERIC_FORM':
      this.requestGenericForm();
      break;

    case 'ARCHIVE_SELECT':
      this.requestArchive();
      break;

    case 'FILE_UPLOAD':
      this.requestUploadFile();
      break;

    case 'TEXT_REFERENCE':
      this.requestTextReference();
      break;

    // no default
    }
  },


  onArchiveChooserSuccess(selectedAttachments) {
    this.setState({
      isArchiveChooserVisible: false,
    });
    this.props.onCreateAttachments(selectedAttachments);
  },

  onArchiveChooserCancel() {
    this.setState({
      isArchiveChooserVisible: false,
    });
  },

  onFileUploadSuccess(files, validityType, date) {
    const attachments = [];
    files.forEach(file => {
      log.info('[onFileUploadSuccess] ', file);
      const attachmentData = {
        attachment_type: 'FileAttachment',
        original_file_name: file.name,
        file: file.data_url,
        expiration_date: tools.dateToIso8601(date),
      };
      const attachment = AttachmentsUtils.objectToAttachment(attachmentData);
      attachments.push(attachment);
    });
    this.setState({
      isFileUploadVisible: false,
    });
    if (attachments.length) {
      this.props.onCreateAttachments(attachments);
    }
  },

  onFileUploadCancel() {
    this.setState({
      isFileUploadVisible: false,
    });
  },

  onTextReferenceAttachSuccess(text) {
    this.setState({
      isTextReferenceAttachVisible: false,
    });

    const title = (text.length > 20) ? `${text.slice(0, 20)}...` : text;
    const tObj = {
      description: text,
      title,
      attachment_type: 'TextReferenceAttachment',
    };
    const attachment = AttachmentsUtils.objectToAttachment(tObj);
    this.props.onCreateAttachments([attachment]);
  },

  onTextReferenceAttachCancel() {
    this.setState({
      isTextReferenceAttachVisible: false,
    });
  },

  onGenericFormAttachSuccess({ value, isSigned }) {
    this.setState({
      isGenericFormVisible: false,
    });
    const attachmentData = {
      attachment_type: 'FormAttachment',
      title: `${_t('New Agriform')} / ${new Date().toISOString().slice(0, 10)}`,
      is_done: isSigned,
      generic_form_data: {
        json_data: value,
        generic_form: this.props.genericForm.uuid,
      },
    };

    const attachment = AttachmentsUtils.objectToAttachment(attachmentData);

    this.props.onCreateAttachments([attachment]);
  },

  onGenericFormAttachCancel() {
    this.setState({
      isGenericFormVisible: false,
    });
  },

  //
  // Private
  //

  requestAgriform() {
    const attachmentData = {
      attachment_type: 'AgriformAttachment',

      // TODO better agriform attachment title
      title: `${_t('New Agriform')} / ${new Date().toISOString().slice(0, 10)}`,
    };
    const attachment = AttachmentsUtils.objectToAttachment(attachmentData);

    this.props.onCreateAgriform(attachment);
  },

  requestArchive() {
    this.setState({
      isArchiveChooserVisible: true,
    });
  },

  requestTextReference() {
    this.setState({
      isTextReferenceAttachVisible: true,
    });
  },

  requestGenericForm() {
    this.setState({
      isGenericFormVisible: true,
    });
  },

  requestUploadFile() {
    this.setState({
      isFileUploadVisible: true,
    });
  },

  render() {
    // Attach from archive
    let attachFromArchivePart = null;
    if (this.state.isArchiveChooserVisible) {
      attachFromArchivePart = (
        <SelectArchiveFilesModal
          visible
          onSuccess={this.onArchiveChooserSuccess}
          onCancel={this.onArchiveChooserCancel}
        />
      );
    }

    // Upload file
    let uploadFilePart = null;
    if (this.state.isFileUploadVisible) {
      uploadFilePart = (
        <FileUploadModal
          isVisible
          onSuccess={this.onFileUploadSuccess}
          onCancel={this.onFileUploadCancel}
        />
      );
    }

    // Attach text reference
    let attachTextReferencePart = null;
    if (this.state.isTextReferenceAttachVisible) {
      uploadFilePart = (
        <AttachTextReferenceModal
          isVisible
          onSuccess={this.onTextReferenceAttachSuccess}
          onCancel={this.onTextReferenceAttachCancel}
        />
      );
    }

    // Generic form
    let genericFormPart = null;
    if (this.state.isGenericFormVisible) {
      uploadFilePart = (
        <GenericFormModal
          isVisible
          genericFormSlug={this.props.genericForm && this.props.genericForm.slug}
          onSuccess={this.onGenericFormAttachSuccess}
          onCancel={this.onGenericFormAttachCancel}
        />
      );
    }

    let attachButtonPart = null;
    if (!this.props.readOnly) {
      attachButtonPart = (
        <AttachButton
          btnCls="default"
          optionKeys={this.props.optionKeys}
          title={_t('Attach')}
          onSelect={this.onAttachButton}
        />);
    }

    return (
      <div>

        {attachFromArchivePart}

        {uploadFilePart}

        {attachTextReferencePart}

        {attachButtonPart}

        {genericFormPart}

      </div>
    );
  },

});


module.exports = AttachmentCreator;
