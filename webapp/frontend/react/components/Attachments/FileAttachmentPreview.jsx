import React from 'react';

const pageData = require('../Utils/pageData.js');

const FileAttachmentPreview = React.createClass({

  propTypes: {
    attachmentUrl: React.PropTypes.string.isRequired,
    attachmentExtension: React.PropTypes.string.isRequired,
    attachmentDownloadName: React.PropTypes.string,
  },

  getFiletype(extension) {
    let matchedFiletype = '';

    const filetypes = {
      image: /(gif|jpg|jpeg|png)/i,
      excel: /(xls|xlt|xlm|xlsx|xlsm|xltx|xltm)/i,
      word: /(doc|dot|docx|docm|dotx)/i,
      archive: /(zip|rar|7zip)/i,
      pdf: /pdf/i,
      powerpoint: /(ppt|pot|pps|pptx)/i,
    };
    _.forEach(filetypes, (filetypeRegex, filetype) => { // eslint-disable-line consistent-return
      if (extension.match(filetypeRegex)) {
        matchedFiletype = filetype;
        return false;
      }
    });

    return matchedFiletype || 'other';
  },

  //
  // Render
  //
  render() {
    const attachmentExtension = this.props.attachmentExtension;
    const attachmentUrl = this.props.attachmentUrl;

    const filetype = this.getFiletype(attachmentExtension);

    const imgPart = <img alt="attachment" src={attachmentUrl} className="img-thumbnail img-responsive" />;

    const otherPart = (
      <div>
      {
        // There is a possibility to open pdfs in browser
        filetype !== 'pdf'
        ? <Bootstrap.Alert bsStyle="info">
          <span>{_t('No preview available')}. {_t('Download to view the document')}.</span>
        </Bootstrap.Alert>
        : null
      }
        <div style={{ textAlign: 'center' }}>
          <a href={attachmentUrl} target="_self" >
            <img
              src={`${pageData.context.staticUrl}frontend/img/icons/filetypes/${filetype}.svg`}
              style={{ width: '30%' }}
              alt="file"
            />
          </a>
        </div>
      </div>
    );

    return (
      <div className="col-md-12">
        {filetype === 'image' ? imgPart : otherPart}
      </div>
    );
  },
});

module.exports = FileAttachmentPreview;
