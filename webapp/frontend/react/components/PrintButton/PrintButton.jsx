import React, { Component } from 'react';
require('./PrintButton.less');

export class PrintButton extends Component {
  render() {
    return (
      <a href="#">
        <i
          className="PrintButton icon fa fa-print"
          onClick={window.print}
        />
      </a>
    );
  }
}
