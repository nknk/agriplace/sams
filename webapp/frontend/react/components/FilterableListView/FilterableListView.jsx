import React from 'react';

// FilterableListView
//
//  Just a simple wrapper to make available QueryInput for ListView's list items
//

const ListView = require('../ListView/ListView.jsx');
const QueryInput = require('../QueryInput/QueryInput.jsx');

const FilterableListView = React.createClass({
  propTypes: {
    listViewOptions: React.PropTypes.object.isRequired,
    queryInputOptions: React.PropTypes.object.isRequired,
  },

  getInitialState() {
    return {
      query: this.props.queryInputOptions.query,
    };
  },

  componentWillMount() {
    // if filterKeys is not given, use all keys from items objects
    this.keys = this.props.listViewOptions.filterKeys || tools.allObjectKeys(this.props.listViewOptions.items);
    this.props.queryInputOptions.onInputChange = this.onInputChange;
  },

  onInputChange(query) {
    this.setState({ query });
  },

  render() {
    const listViewOther = _.clone(this.props.listViewOptions);
    const items = tools.objectFilter(this.props.listViewOptions.items, this.state.query, this.keys);
    const listViewOptions = _.extend(listViewOther, { items });

    return (
      <div className="filterable-list-view">
        <QueryInput queryInputOptions={this.props.queryInputOptions} />

        <ListView listViewOptions={listViewOptions} />
      </div>
    );
  },

});

module.exports = FilterableListView;
