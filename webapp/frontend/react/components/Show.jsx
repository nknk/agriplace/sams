import React from 'react';
//
// Shows content when condition is met.
//
var Show = React.createClass({

  // propTypes: {
  //  when: React.PropTypes.object
  // },

  render() {
    if (this.props.when) {
      return this.props.children;
    }
    return null;
  },
});

module.exports = Show;
