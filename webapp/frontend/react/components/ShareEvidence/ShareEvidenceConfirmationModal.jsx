import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Button, Modal } = Bootstrap;
require('./ShareEvidence.less');

export class ShareEvidenceConfirmationModal extends React.Component {

  render() {
    const {
      selectedOrganizations,
      shareInProgress,
      isSuccessShareEvidence,
      isFailureShareEvidence,
    } = this.props;

    const names = selectedOrganizations.map(org => (
      org.organization_name
    )).join(', ');

    let display = null;
    if (shareInProgress) {
      display = (
        <i className="icon fa fa-spinner fa-spin fa-2x"></i>
      );
    } else if (isSuccessShareEvidence) {
      display = (
        <span>
          <i
            style={{ color: 'green' }}
            className="icon fa fa-check-circle fa-lg"
          />
          {' '}
          {_t('Sharing successful')}
        </span>
      );
    } else if (isFailureShareEvidence) {
      display = (
        <span>
          <i
            style={{ color: 'red' }}
            className="icon fa fa-exclamation-triangle"
          />
          {' '}
          {_t('Sharing unsuccessful. Please try again or contact us:')}
          {' '}
          <a href={"mailto:support@agriplace.com"}>{'support@agriplace.com'}</a>
        </span>
      );
    }

    return (
      <Modal className="share-evidence-modal" show={this.props.isVisible} backdrop={'static'} onRequestHide={null}>
        <Modal.Header>
          <h2>
            <strong>{_t('Share evidence')}</strong>
          </h2>
        </Modal.Header>
        <Modal.Body>
          {_t('You are about to provide \'Can View\' permission to')}
          {' '}
          {
            isFailureShareEvidence ? (
              <strong>{names}</strong>
            ) : names
          }
          {' '}
          {_t('organizations')}
          {'. '}
          {_t('These organizations will be able to view, import and download the shared files')}
          {'.'}
        </Modal.Body>
        <Modal.Footer className="share-evidence-footer">
          <div className="button-toolbar">
            <Button
              bsStyle="success"
              disabled={shareInProgress || isSuccessShareEvidence}
              onClick={this.props.onConfirmation}
            >
              {_t('Save')}
            </Button>
            <Button
              bsStyle="default"
              disabled={shareInProgress || isSuccessShareEvidence}
              onClick={this.props.onDismissShareEvidence}
            >
              {_t('Cancel')}
            </Button>
            <div className="next-to-buttons">
              {display}
            </div>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

ShareEvidenceConfirmationModal.propTypes = {
  isVisible: React.PropTypes.boolean,
  selectedOrganizations: React.PropTypes.array,
  onDismissShareEvidence: React.PropTypes.func.isRequired,
  onConfirmation: React.PropTypes.func.isRequired,
  shareInProgress: React.PropTypes.bool,
  isSuccessShareEvidence: React.PropTypes.bool,
  isFailureShareEvidence: React.PropTypes.bool,
};
