import React from 'react';
const ListView = require('../ListView/ListView.jsx');
const listViewProps = require('./WhoHasAccessListViewProps.js');

export class WhoHasAccessListView extends React.Component {

  render() {
    const { alreadySharedOrganizations } = this.props;

    let items = alreadySharedOrganizations;
    items = items.map(item => (
      _.extend(item, {
        organizationCity: item.organization_city ?
          `${item.organization_name} (${item.organization_city})` :
          `${item.organization_name}`,
      })
    ));

    const listViewOptions = _.extend(listViewProps, {
      items,
    });

    return (
      <div className="who-has-access-listview" >
        <ListView listViewOptions={listViewOptions} />
      </div>
    );
  }
}

WhoHasAccessListView.propTypes = {
  alreadySharedOrganizations: React.PropTypes.array.isRequired,
};

