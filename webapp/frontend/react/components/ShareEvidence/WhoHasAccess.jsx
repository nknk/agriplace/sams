import React from 'react';
require('./ShareEvidence.less');
import { WhoHasAccessListView } from './WhoHasAccessListView.jsx';
const BusyFeedback = require('../BusyFeedback');

export class WhoHasAccess extends React.Component {

  render() {
    const { alreadySharedOrganizations,
      isLoadingAlreadySharedOrganizations } = this.props;

    let body;
    if (isLoadingAlreadySharedOrganizations) {
      body = <BusyFeedback />;
    } else if (!alreadySharedOrganizations.length) {
      body = null;
    } else {
      body = (
        <div className="who-has-access" >
          <div className="header" >
            <strong>
              {_t('Who has access')}
            </strong>
          </div>
          <WhoHasAccessListView
            alreadySharedOrganizations={alreadySharedOrganizations}
          />
        </div>
      );
    }

    return body;
  }
}

WhoHasAccess.propTypes = {
  alreadySharedOrganizations: React.PropTypes.array.isRequired,
  isLoadingAlreadySharedOrganizations: React.PropTypes.bool,
};
