import React from 'react';
import { ShareEvidenceModal } from './ShareEvidenceModal.jsx';
import { ShareEvidenceConfirmationModal } from './ShareEvidenceConfirmationModal.jsx';

const ShareEvidenceModalController = React.createClass({

  propTypes: {
    partnerOrganizations: React.PropTypes.array,
    isLoadingPartnerOrganizations: React.PropTypes.bool,
    evidenceSharingInProgress: React.PropTypes.bool,
    selectedOrganizations: React.PropTypes.array,
    organizationSearchText: React.PropTypes.string,
    isShareEvidenceModalVisible: React.PropTypes.bool,
    isConfirmationModalVisible: React.PropTypes.bool,
    hasMorePartnerOrganizations: React.PropTypes.bool,
    loadingMorePartnerOrganizations: React.PropTypes.bool,
    pageNumber: React.PropTypes.number,
    alreadySharedOrganizations: React.PropTypes.array,
    isLoadingAlreadySharedOrganizations: React.PropTypes.bool,
    isSuccessShareEvidence: React.PropTypes.bool,
    isFailureShareEvidence: React.PropTypes.bool,
  },

  mixins: [
    FluxMixin,
  ],

  onSelectedOrganizationsChange(selectedOrganizations) {
    const flux = this.getFlux();
    flux.actions.shareEvidence.updateSelectedOrganizations(selectedOrganizations);
  },

  onSearchOrganizationChanged(organizationSearchText) {
    const flux = this.getFlux();
    flux.actions.shareEvidence.getPartnerOrganizationsSearch(organizationSearchText);
  },

  onSave() {
    const flux = this.getFlux();
    flux.actions.shareEvidence.saveEvidence();
  },

  onDismissShareEvidence() {
    const flux = this.getFlux();
    flux.actions.shareEvidence.stopSharing();
  },

  onConfirmation() {
    const flux = this.getFlux();
    flux.actions.shareEvidence.shareConfirmation();
  },

  loadMorePartnerOrganizations() {
    const flux = this.getFlux();
    const { pageNumber } = this.props;
    flux.actions.shareEvidence.loadMorePartnerOrganizations(pageNumber + 1);
  },

  render() {
    const { selectedOrganizations, evidenceSharingInProgress,
      isShareEvidenceModalVisible, isConfirmationModalVisible,
      isLoadingPartnerOrganizations, hasMorePartnerOrganizations,
      loadingMorePartnerOrganizations, alreadySharedOrganizations,
      partnerOrganizations, organizationSearchText, isFailureShareEvidence,
      isLoadingAlreadySharedOrganizations, isSuccessShareEvidence } = this.props;

    const isChanged = !!selectedOrganizations.length;
    return (
      <div>
        <ShareEvidenceModal
          isVisible={isShareEvidenceModalVisible}
          organizations={partnerOrganizations}
          alreadySharedOrganizations={alreadySharedOrganizations}
          selectedOrganizations={selectedOrganizations}
          onSelectedOrganizationsChange={this.onSelectedOrganizationsChange}
          onSearchOrganizationChanged={this.onSearchOrganizationChanged}
          isChanged={isChanged}
          onDismissShareEvidence={this.onDismissShareEvidence}
          onSave={this.onSave}
          isLoadingPartnerOrganizations={isLoadingPartnerOrganizations}
          hasMorePartnerOrganizations={hasMorePartnerOrganizations}
          loadMorePartnerOrganizations={this.loadMorePartnerOrganizations}
          loadingMorePartnerOrganizations={loadingMorePartnerOrganizations}
          organizationSearchText={organizationSearchText}
          isLoadingAlreadySharedOrganizations={isLoadingAlreadySharedOrganizations}
        />
        <ShareEvidenceConfirmationModal
          isVisible={isConfirmationModalVisible}
          selectedOrganizations={selectedOrganizations}
          onDismissShareEvidence={this.onDismissShareEvidence}
          onConfirmation={this.onConfirmation}
          shareInProgress={evidenceSharingInProgress}
          isSuccessShareEvidence={isSuccessShareEvidence}
          isFailureShareEvidence={isFailureShareEvidence}
        />
      </div>
    );
  },
});

module.exports = ShareEvidenceModalController;
