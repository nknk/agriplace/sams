import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Button, Modal, ButtonToolbar } = Bootstrap;
import { InviteOrganizations } from './InviteOrganizations.jsx';
import { WhoHasAccess } from './WhoHasAccess.jsx';
require('./ShareEvidence.less');

export class ShareEvidenceModal extends React.Component {

  render() {
    const { isVisible, organizations, isChanged, alreadySharedOrganizations,
      selectedOrganizations, hasMorePartnerOrganizations, organizationSearchText,
      loadingMorePartnerOrganizations, isLoadingPartnerOrganizations,
      isLoadingAlreadySharedOrganizations } = this.props;


    const body = (
      <div>
        <WhoHasAccess
          alreadySharedOrganizations={alreadySharedOrganizations}
          isLoadingAlreadySharedOrganizations={isLoadingAlreadySharedOrganizations}
        />
        <InviteOrganizations
          organizations={organizations}
          selectedOrganizations={selectedOrganizations}
          onSelectedOrganizationsChange={this.props.onSelectedOrganizationsChange}
          onSearchOrganizationChanged={this.props.onSearchOrganizationChanged}
          hasMorePartnerOrganizations={hasMorePartnerOrganizations}
          loadMorePartnerOrganizations={this.props.loadMorePartnerOrganizations}
          loadingMorePartnerOrganizations={loadingMorePartnerOrganizations}
          organizationSearchText={organizationSearchText}
          isLoadingPartnerOrganizations={isLoadingPartnerOrganizations}
        />
      </div>
    );
    return (
      <Modal className="share-evidence-modal" show={isVisible} backdrop={'static'} onRequestHide={null}>
        <Modal.Header>
          <h2>
            <strong>{_t('Share evidence')}</strong>
          </h2>
        </Modal.Header>
        <Modal.Body>
          {body}
        </Modal.Body>
        <Modal.Footer className="share-evidence-footer">
          <div className="share-evidence-footer pull-left">
            {
              isChanged ? (
                <div className="save-changes-bar">
                  {_t('You have made changes that you need to save')}
                </div>
              ) : null
            }
            <div>
              <ButtonToolbar className="pull-left">
                <Button
                  bsStyle="success"
                  disabled={!isChanged}
                  onClick={this.props.onSave}
                >
                  {_t('Save')}
                </Button>
                <Button
                  bsStyle="default"
                  onClick={this.props.onDismissShareEvidence}
                >{_t('Cancel')}</Button>
              </ButtonToolbar>
            </div>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

ShareEvidenceModal.propTypes = {
  isVisible: React.PropTypes.boolean,
  organizations: React.PropTypes.array,
  alreadySharedOrganizations: React.PropTypes.array,
  onSelectedOrganizationsChange: React.PropTypes.func,
  onSearchOrganizationChanged: React.PropTypes.func,
  isChanged: React.PropTypes.bool,
  onDismissShareEvidence: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired,
  selectedOrganizations: React.PropTypes.array,
  hasMorePartnerOrganizations: React.PropTypes.bool,
  loadMorePartnerOrganizations: React.PropTypes.func,
  loadingMorePartnerOrganizations: React.PropTypes.bool,
  isLoadingPartnerOrganizations: React.PropTypes.bool,
  organizationSearchText: React.PropTypes.string,
  isLoadingAlreadySharedOrganizations: React.PropTypes.bool,
};
