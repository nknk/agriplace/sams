import React from 'react';
const Bootstrap = require('react-bootstrap');

export class ShareEvidenceButton extends React.Component {

  render() {
    const isEnabled = this.props.count;
    const isVisible = this.props.isSharingAllowed;
    return (
      isVisible ? (
        <Bootstrap.Button
          bsStyle="default"
          onClick={this.props.onClick}
          disabled={!isEnabled}
        >
          <span className="icon fa fa-share-alt" />
          {' '}
          {_t('Share evidence')}
          {' '}
          {this.props.count ? ` (${this.props.count})` : null}
        </Bootstrap.Button>
      ) : (
        null
      )
    );
  }

}

ShareEvidenceButton.propTypes = {
  onClick: React.PropTypes.function,
  count: React.PropTypes.number,
  isSharingAllowed: React.PropTypes.bool.isRequired,
};
