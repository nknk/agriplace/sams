import pageData from '../Utils/pageData.js';

module.exports = {
  items: [],
  columns: [
    { organizationCity: _t('Organization (City)') },
    { organization_type: _t('Type') },
  ],
  sortKeys: [],
  filterKeys: [],
  showHeader: true,
  isReverseOrder: false,
  headerAlign: 'left',
  dateFormat: pageData.dateFormat,
  dateLocale: pageData.languageCode,
  showCheckBox: true,
  tableClasses: 'invite-organizations-table',
  tdOffset: 0,
};
