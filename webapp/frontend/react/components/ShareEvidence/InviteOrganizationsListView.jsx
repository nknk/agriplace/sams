import React from 'react';
const ListView = require('../ListView/ListView.jsx');
const listViewProps = require('./InviteOrganizationsListViewProps.js');

export class InviteOrganizationsListView extends React.Component {

  render() {
    const { onSelectAllToggle, onSelectOrganization,
      organizations, selectedOrganizations, hasMorePartnerOrganizations,
      loadingMorePartnerOrganizations } = this.props;

    let items = organizations;
    items = items.map(item => (
      _.extend(item, {
        organizationCity: item.organization_city ?
          `${item.organization_name} (${item.organization_city})` :
          `${item.organization_name}`,
      })
    ));

    const listViewOptions = _.extend(listViewProps, {
      items,
      hasMore: hasMorePartnerOrganizations,
      onSelectAllToogle: onSelectAllToggle,
      onItemClicked: onSelectOrganization,
      selected: selectedOrganizations,
      loadMore: this.props.loadMorePartnerOrganizations,
      isLoadingMore: loadingMorePartnerOrganizations,
    });
    // console.log(listViewOptions);

    return (
      <div className="invite-organizations-listview" >
        <ListView listViewOptions={listViewOptions} />
      </div>
    );
  }
}

InviteOrganizationsListView.propTypes = {
  organizations: React.PropTypes.array,
  selectedOrganizations: React.PropTypes.array,
  onSelectAllToggle: React.PropTypes.func,
  onSelectOrganization: React.PropTypes.func,
  hasMorePartnerOrganizations: React.PropTypes.bool,
  loadMorePartnerOrganizations: React.PropTypes.func,
  loadingMorePartnerOrganizations: React.PropTypes.bool,
};
