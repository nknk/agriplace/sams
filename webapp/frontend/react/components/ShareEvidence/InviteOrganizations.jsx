import React from 'react';
const Bootstrap = require('react-bootstrap');
require('./ShareEvidence.less');
import searchFactory from '../../factories/SearchFactory.jsx';
const tForm = require('tcomb-form');
const Form = tForm.form.Form;
const t = require('tcomb');
import { InviteOrganizationsListView } from './InviteOrganizationsListView.jsx';
const BusyFeedback = require('../BusyFeedback');

export class InviteOrganizations extends React.Component {

  constructor(props) {
    super(props);
    this.onSelectAllToggle = this.onSelectAllToggle.bind(this);
    this.onSelectOrganization = this.onSelectOrganization.bind(this);
  }

  onSearchOrganization(name) {
    this.props.onSearchOrganizationChanged(name.organizationName);
  }

  onSelectAllToggle(isChecked) {
    let selectedOrganizations = [];
    if (isChecked) {
      selectedOrganizations = this.props.organizations;
    }
    this.props.onSelectedOrganizationsChange(selectedOrganizations);
  }

  onSelectOrganization(item) {
    let selectedOrganizations = this.props.selectedOrganizations;
    if (selectedOrganizations.find(org => org.membership_uuid === item.membership_uuid)) {
      // remove if present
      selectedOrganizations = selectedOrganizations.filter(org => (
        org.membership_uuid !== item.membership_uuid
      ));
    } else {
      // insert if not present
      selectedOrganizations.push(item);
    }
    this.props.onSelectedOrganizationsChange(selectedOrganizations);
  }

  getTooltip() {
    return (
      <Bootstrap.Tooltip id="tooltip-invite-organizations">
        <strong>
          {_t('You can only share information with organizations that belong to the same farm group')}
          {'.'}
        </strong>
      </Bootstrap.Tooltip>
    );
  }

  render() {
    const options = {
      fields: {
        organizationName: {
          factory: searchFactory,
          attrs: {
            placeholder: _t('Type here'),
          },
        },
      },
      auto: 'none',
    };

    const searchStruct = t.struct({
      organizationName: t.maybe(t.String),
    });

    const { organizations, selectedOrganizations, hasMorePartnerOrganizations,
      loadingMorePartnerOrganizations, organizationSearchText,
      isLoadingPartnerOrganizations } = this.props;

    const formValue = {
      organizationName: organizationSearchText,
    };

    const body = isLoadingPartnerOrganizations ? (
      <BusyFeedback />
    ) : (
      <InviteOrganizationsListView
        onSelectOrganization={this.onSelectOrganization}
        onSelectAllToggle={this.onSelectAllToggle}
        organizations={organizations}
        selectedOrganizations={selectedOrganizations}
        hasMorePartnerOrganizations={hasMorePartnerOrganizations}
        loadMorePartnerOrganizations={this.props.loadMorePartnerOrganizations}
        loadingMorePartnerOrganizations={loadingMorePartnerOrganizations}
      />
    );

    return (
      <div className="invite-organizations" >
        <div className="header" >
          <span>
            <strong>
              {_t('Invite organization(s)')}
            </strong>
            {' '}
            <Bootstrap.OverlayTrigger placement="top" overlay={this.getTooltip()}>
              <i className="icon fa fa-info-circle fa-lg tooltip-color" />
            </Bootstrap.OverlayTrigger>
          </span>
        </div>
        <div className="sub-header" >
          <strong>
            {_t('Organization or group name')}
          </strong>
        </div>
        <div className="search-bar">
          <Form
            ref="form"
            options={options}
            value={formValue}
            type={searchStruct}
            onChange={_.debounce((value) => this.onSearchOrganization(value), 300)}
          />
        </div>
        {body}
      </div>
    );
  }
}

InviteOrganizations.propTypes = {
  organizations: React.PropTypes.array,
  onSelectedOrganizationsChange: React.PropTypes.func,
  onSearchOrganizationChanged: React.PropTypes.func,
  selectedOrganizations: React.PropTypes.array,
  hasMorePartnerOrganizations: React.PropTypes.bool,
  loadMorePartnerOrganizations: React.PropTypes.func,
  loadingMorePartnerOrganizations: React.PropTypes.bool,
  organizationSearchText: React.PropTypes.string,
  isLoadingPartnerOrganizations: React.PropTypes.bool,
};
