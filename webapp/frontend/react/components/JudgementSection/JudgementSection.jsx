import React from 'react';
import autosize from 'autosize';
import classNames from 'classnames';
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

require('./JudgementSection.less');

export const JudgementSection = React.createClass({
  propTypes: {
    readOnly: React.PropTypes.bool,
    questionUuid: React.PropTypes.string,
    assessmentUuid: React.PropTypes.string,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('JudgementsStore'),
  ],

  componentDidMount() {
    const node = $(this.getDOMNode()).children('textarea');
    autosize(node);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const judgementsStore = flux.store('JudgementsStore');
    const judgementsStoreState = judgementsStore.getState();
    const { questionUuid } = this.props;

    return {
      isInProgress: judgementsStoreState.questionsInProgress.indexOf(questionUuid) !== -1,
      judgement: judgementsStoreState.judgements.find(judgement => judgement.question === questionUuid) || {
        value: '',
        justification: '',
      },
    };
  },

  onJudgementChanged(judgement) {
    const flux = this.getFlux();
    const { assessmentUuid } = this.props;

    if (this.isEmpty(judgement)) {
      flux.actions.judgements.delete({ assessmentUuid, judgement });
    } else {
      if (!judgement.uuid) {
        flux.actions.judgements.create({ assessmentUuid, judgement });
      } else {
        flux.actions.judgements.update({ judgementUuid: judgement.uuid, judgement, assessmentUuid });
      }
    }
  },

  handleJudgementsClick(value) {
    const { judgement } = _.clone(this.state);
    value = judgement.value !== value ? value : '';
    this.changeJudgement({ value });
  },

  handleJustificationChange(e) {
    this.changeJudgement({ justification: e.target.value });
  },

  changeJudgement({ value = this.state.judgement.value, justification = this.state.judgement.justification }) {
    if (this.state.judgement.value !== value || this.state.judgement.justification !== justification) {
      const { judgement } = _.cloneDeep(this.state);
      _.assign(judgement, { value, justification, question: this.props.questionUuid });

      this.onJudgementChanged(judgement);
    }
  },

  isEmpty(judgement) {
    return judgement.value === '' && judgement.justification === '';
  },

  render() {
    const possibleJudgements = [{ text: _t('Yes'), value: 'yes' }, { text: _t('No'), value: 'no' }];
    const { judgement } = this.state;

    const controlsClassnames = classNames('controls', {
      answered: judgement.value,
    });

    const justificationClassnames = classNames({
      error: judgement.value === 'no' && judgement.justification === '',
    });

    return (
      <div className="JudgementSection">
        <div className="heading">
          {_t('Audit judgement')}
        </div>
        <div className={controlsClassnames}>
          <Bootstrap.ButtonToolbar className="form-single-choice-buttons">
            {
              possibleJudgements.map((possibleJudgement) => (
                <Bootstrap.Button
                  key={possibleJudgement.value}
                  className="btn-option"
                  disabled={this.props.readOnly || this.state.isInProgress}
                  value={possibleJudgement.value}
                  onClick={this.handleJudgementsClick.bind(null, possibleJudgement.value)}
                  active={judgement.value === possibleJudgement.value}
                >
                  {possibleJudgement.text}
                </Bootstrap.Button>
              ))
            }
          </Bootstrap.ButtonToolbar>
          <Bootstrap.Input
            groupClassName="judgement-justification"
            ref="justification"
            rows="1"
            disabled={this.props.readOnly || this.state.isInProgress}
            onBlur={this.handleJustificationChange}
            defaultValue={judgement.justification}
            bsStyle={justificationClassnames}
            placeholder={_t('Findings')}
            type="textarea"
          />
        </div>
      </div>
    );
  },
});
