import React from 'react';
const ListView = require('../ListView/ListView.jsx');
import textBlocks from '../Utils/textBlocks.js';

import { AssessmentCompletnessModal } from '../AssessmentCompletnessModal/AssessmentCompletnessModal.jsx';
import { ShareWithoutAuditorsModal } from '../ShareAssessmentModals/ShareWithoutAuditorsModal.jsx';
import { ShareTermsWarningModal } from '../ShareAssessmentModals/ShareTermsWarningModal.jsx';
import { ShareSuccessModal } from '../ShareAssessmentModals/ShareSuccessModal.jsx';

const AGRIPLACE_HOME_URL = '/';

import pageData from '../../components/Utils/pageData';
const { layout } = pageData.context;

const listViewOptions = {
  columns: [{
    name: layout === 'agriplace' ? _t('Certification body') : _t('Auditor'),
  }],
  sortKeys: ['name'],
  emptyListText: _t('No auditor found'),
  showCheckBox: true,
  showHeader: true,
};

export const ShareAssessment = React.createClass({
  propTypes: {
    params: React.PropTypes.object,
    onAssessmentClose: React.PropTypes.func,
    auditors: React.PropTypes.array,
    selectedAuditors: React.PropTypes.array,
    sharingMessage: React.PropTypes.string,
    isReportVisible: React.PropTypes.bool,
  },

  getInitialState() {
    return {
      progress: 'initial',
    };
  },

  componentWillMount() {
    this.setState({ selectedAuditors: this.props.selectedAuditors });
  },

  componentDidMount() {
    listViewOptions.onItemClicked = this.onSelectionChange;
  },

  onSelectionChange(auditor) {
    let selectedAuditors = this.state.selectedAuditors;
    selectedAuditors = tools.toggleCollectionItem(selectedAuditors, auditor);

    this.setState({ selectedAuditors });
  },

  onShareClicked() {
    this.goToNextState('initial');
  },

  closeAssessment(button) {
    const shares = _.map(this.state.selectedAuditors, (auditor) => ({ partner: auditor.uuid }));

    this.props.onAssessmentClose(this.props.params.assessmentUuid, shares);
    this.goToNextState('termsWarning');
  },


  resetProgress() {
    this.changeProgress({ progress: 'initial' });
  },

  isAssessmentDone() {
    return this.refs.assessmentCompletness.isDone();
  },

  goToNextState(prevState) {
    const isAssessmentDone = this.isAssessmentDone();

    const stateMashine = {
      // State options: ['initial' 'sharedWithoutAuditors' 'termsWarning', 'closeSucceed']
      // Prev state : next state
      initial: (() => {
        if (isAssessmentDone) {
          if (this.state.selectedAuditors.length === 0) {
            return 'sharedWithoutAuditors';
          }
          return 'termsWarning';
        }

        return 'completnessCheck';
      })(),
      completnessCheck: 'initial',
      sharedWithoutAuditors: 'termsWarning',
      termsWarning: 'closeSucceed',
      closeSucceed: 'initial',
    };

    const nextState = stateMashine[prevState];

    this.changeProgress(nextState);
  },

  changeProgress(state) {
    this.setState({ progress: state });
  },

  titleCase(str) {
    return str.toLowerCase().split(' ').map((word) => (
      word.replace(word[0], word[0].toUpperCase())
    )).join(' ');
  },

  render() {
    const { params, isReportVisible } = this.props;
    const { selectedAuditors } = this.state;

    listViewOptions.selected = selectedAuditors;
    listViewOptions.items = this.props.auditors;

    const buttonText = (selectedAuditors.length)
      ? `${_t('Share')} & ${_t('close assessment')}`
      : _t('Close assessment');

    let sharingText;
    if (isReportVisible) {
      const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;
      const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/report`;
      const linkToReport = (t) => (`<a target="_blank" href="${url}">${t}</a>`);
      sharingText = _t('You can find a report of your (self) assessment {link}');
      const linkText = sharingText.startsWith('{link}') ? this.titleCase(_t('here')) : _t('here');
      sharingText = sharingText.replace('{link}', linkToReport(linkText));
      const line2 = _t('When you are ready, you can share your assessment with your certification body below');
      sharingText = `${sharingText}. ${line2}.`;
      sharingText = `${sharingText} ${_t('Your certification body will get read-only access')}`;
      sharingText = `${sharingText} ${_t('to your assessment (questionnaires and evidence)')}.`;
    } else {
      sharingText = textBlocks.assessment_sharing_intro;
    }

    return (
      <div id="share-assessment-app">
        <AssessmentCompletnessModal
          ref="assessmentCompletness"
          onHide={this.changeProgress.bind(null, 'initial')}
          isClicked={this.state.progress === 'completnessCheck'}
          params={params}
        />
        <ShareWithoutAuditorsModal
          isVisible={this.state.progress === 'sharedWithoutAuditors'}
          onSuccess={this.goToNextState.bind(null, 'sharedWithoutAuditors')}
          onFail={this.resetProgress}
        />
        <ShareTermsWarningModal
          isVisible={this.state.progress === 'termsWarning'}
          onSuccess={this.closeAssessment}
          onFail={this.resetProgress}
          bodyText={this.props.sharingMessage}
        />
        <ShareSuccessModal
          isVisible={this.state.progress === 'closeSucceed'}
          isSharedWithAuditors={selectedAuditors.length > 0}
          onSuccess={() => { window.location = AGRIPLACE_HOME_URL; }}
          params={params}
        />

        <p className="lead" dangerouslySetInnerHTML={{ __html: sharingText }} />

        <ListView listViewOptions={listViewOptions} />

        <br />

        <Bootstrap.Button
          className="pull-right"
          bsStyle="primary"
          onClick={this.onShareClicked}
        >
          <i className="fa fa-check" />
          <span style={{ marginLeft: '10px' }}>{_t(buttonText)}</span>
        </Bootstrap.Button>
        <br /><br />
      </div>
  );
  },
});
