import React, { Component } from 'react';

export class ShareWithModal extends Component {
  constructor() {
    super();
    this.state = {
      isExtramodals: false,
    };
  }

  componentDidMount() {
    // Doing this ugly thing since we need only one modal.
    /* eslint-disable react/no-did-mount-set-state */
    this.setState({ isExtramodals: $('html').find('.modal-dialog:not(.ShareWithModal)').length > 0 });
  }

  render() {
    if (this.state.isExtramodals) {
      return null;
    }
    const { isVisible, heading, body, onSuccessUrl, onHide } = this.props;

    return (
      <Bootstrap.Modal show={isVisible} backdrop="static" onHide={onHide} dialogClassName="ShareWithModal">
        <Bootstrap.Modal.Header>
          <button type="button" className="close" aria-hidden="true" onClick={onHide}>&times;</button>
          <h3>{heading}</h3>
        </Bootstrap.Modal.Header>
        <Bootstrap.Modal.Body>
          <p>{body}</p>
        </Bootstrap.Modal.Body>
        <Bootstrap.Modal.Footer>
          <a className="btn btn-primary" href={onSuccessUrl}>
            <span className="fa fa-share-alt"></span>&nbsp;
            {_t('Yes')}
          </a>
          <a onClick={onHide} className="btn btn-default" >{_t('Go back')}</a>
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

ShareWithModal.propTypes = {
  isVisible: React.PropTypes.bool,
  heading: React.PropTypes.string,
  body: React.PropTypes.string,
  onHide: React.PropTypes.func,
  onSuccessUrl: React.PropTypes.string,
};
