import React from 'react';

const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

const AjaxInterceptor = require('ajax-interceptor');

import { Modal, Button } from 'react-bootstrap';
const { Body, Footer, Header: ModalHeader } = Modal;

import axios from 'axios';
import './OfflineNotification.less';

// How Offline Notification works ?
// We use 2 different methods to detect internet status, whereas
// both use the web request response in order to identify if user is online or offline.
// Firstly, there is a periodic dummy request thrown to server every 40 seconds.
// Secondly, there is an interceptor which checks for the response of every web request.
// In any case, if a response status 0 is detected or a null response is obtained,
// this triggers an offline condition and shows a notification.
// On receiving a different response, an online condition is triggered.

// No default mixins support for es6 in react, so using es5
// ref : https://facebook.github.io/react/blog/2015/01/27/react-v0.13.0-beta-1.html#mixins
export const OfflineNotification = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('OfflineNotificationStore'),
  ],

  componentDidMount() {
    // initialize axios interceptor
    this.axiosHook = axios.interceptors.response.use(
      this.axiosResponseHookCallBack,
      this.axiosErrorHookCallBack
    );

    // initialize ajax callback
    AjaxInterceptor.addResponseCallback(this.ajaxHookCallBack);
    AjaxInterceptor.wire();

    window.addEventListener('offline', this.onOffline);

    setTimeout(this.testInternetConnection, 0);
  },

  componentWillUnmount() {
    AjaxInterceptor.unwire();
    axios.interceptors.response.eject(this.axiosHook);
    window.removeEventListener('offline', this.onOffline);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    return flux.store('OfflineNotificationStore').getState();
  },

  onOffline() {
    const response = {
      status: 0,
    };
    this.checkInternetStatus(response);
  },

  checkInternetStatus(response) {
    // Update internet status only if application is online
    // whereas for offline, it will only update when OK button is pressed
    const isOnline = this.state.status === 'online';
    if (isOnline) {
      const flux = this.getFlux();
      const status = response.status ? 'online' : 'offline';
      flux.actions.offlineNotification.updateInternetStatus(status);
    }
  },

  ajaxHookCallBack(xhr) {
    this.checkInternetStatus(xhr);
  },

  axiosErrorHookCallBack(err) {
    this.checkInternetStatus(err);
    return Promise.reject(err);
  },

  axiosResponseHookCallBack(response) {
    this.checkInternetStatus(response);
    return response;
  },

  testInternetConnection() {
    const flux = this.getFlux();
    flux.actions.offlineNotification.checkInternetStatus();
  },

  render() {
    const { checkInternetStatusInProgress, status } = this.state;
    const isOpen = status === 'offline';
    return (
      <Modal show={isOpen} backdrop={'static'} onHide={() => {}} onRequestHide={null}>
        <ModalHeader>
          <h2>
            <icon className="icon fa fa-exclamation-triangle offline-warning-symbol" />
            <strong>{_t('Restore Internet connection')}</strong>
          </h2>
        </ModalHeader>
        <Body>
          {_t('You are currently offline')}
          {'. '}
          {_t('You need to reestablish the connection before you can proceed')}
          {'. '}
          {_t('After reconnecting press \'OK\'')}
          {'.'}
        </Body>
        <Footer>
          <Button
            bsStyle={'success'}
            disabled={checkInternetStatusInProgress}
            onClick={this.testInternetConnection}
          >
            {
              checkInternetStatusInProgress ? (
                <icon className="icon fa fa-spinner fa-spin fa-lg" />
              ) : _t('OK')
            }
          </Button>
        </Footer>
      </Modal>
    );
  },
});

