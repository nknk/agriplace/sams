import React from 'react';
const ToastMixin = require('../../mixins/ToastMixin.js');

var FileChooser = React.createClass({

  propTypes: {
    isVisible: React.PropTypes.bool.isRequired,
    onChange: React.PropTypes.func.isRequired,
  },

  mixins: [
    ToastMixin(),
  ],

  _readMultiFiles(files) {
    var result = [];
    var reader = new FileReader();
    var that = this;

    function readFile(index) {
      if (index >= files.length) return;
      var file = files[index];
      if (file.size > 10000000) {
        that.toast({
          title: _t('The selected file is too big') + '. ' + file.name + '(' + file.size + ')',
          type: 'error',
        });
      } else {
        reader.onload = (e) => {
          var fileObj = {
            'name': file.name,
            'size': file.size,
            'data_url': e.target.result,
          };
          result.push(fileObj);
          readFile(index + 1);
        };

        reader.readAsDataURL(file);

      }
      reader.onerror = (e) => {
        that.toast({
          title: _t('File can\'t be read, error code:') + ' ' + e.target.error.code,
          type: 'error',
        });
      };

    }
    readFile(0);
    return result;
  },

  _handleFiles(event) {
    this.props.onChange(this._readMultiFiles(event.target.files));
  },

  componentDidMount() {
    if (this.props.isVisible) {
      this.refs.files.getDOMNode().click();
    }
  },

  //
  // Render
  //
  render() {

    var inputClassName = '';
    if (this.props.hidden) {
      inputClassName = 'hidden';
    }

    if (!(window.File && window.FileReader && window.FileList && window.Blob)) {
      log.info('The File APIs are not fully supported in this browser.');
    }

    return (
      <div className="{inputClassName}">
        <input
          type="file"
          ref="files"
          onChange={this._handleFiles}
          multiple
        />
      </div>
    );
  },

});

module.exports = FileChooser;

