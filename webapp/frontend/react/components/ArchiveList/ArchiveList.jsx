const DataTable = require('../DataTable/DataTable.jsx');
const UploadFileButtonWithModal = require('../FileUploader/UploadFileButtonWithModal.jsx');
const pageData = require('../Utils/pageData.js');
const BusyFeedback = require('../BusyFeedback');
import { ShareEvidenceButton } from '../ShareEvidence/ShareEvidenceButton.jsx';
require('./pageSplit.less');

function renderMultiline(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return cellData.map(item => (<p>{item}</p>));
}

function getTitleIcon(iconType) {
  let iconClass;
  switch (iconType) {
  case 'AgriformAttachment':
    iconClass = 'fa-clipboard';
    break;

  case 'FormAttachment':
    iconClass = 'fa-clipboard';
    break;

  case 'FileAttachment':
    iconClass = 'fa-file';
    break;

  case 'TextReferenceAttachment':
    iconClass = 'fa-align-left';
    break;

  default:
    iconClass = 'fa-paperclip';
  }

  return `icon fa ${iconClass}`;
}

function renderTitleWithIcon(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return (
    <div
      onClick={(e) => (
        cellData.titleClicked ? (
          cellData.titleClicked(cellData.uuid, e)
        ) : null
      )}
      className={cellData.titleClicked ? 'hover-archive-table' : null}
    >
      <i className={getTitleIcon(cellData.attachmentType)}></i>
      {' '}
      {cellData.name}
    </div>
  );
}

function renderCheckBox(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return (
    <div>
      <Bootstrap.Input
        style={{
          margin: '0 auto',
          height: '16px',
          width: '16px',
          cursor: 'pointer',
        }}
        type="checkbox"
        checked={cellData.isChecked}
        onChange={(e) => cellData.handleCheckBoxClicked(cellData.uuid, e)}
      />
    </div>
  );
}

const columnSettings = [
  {
    dataKey: 'check_box',
    label: '',
    width: 50,
    cellRenderer: renderCheckBox,
    cellDataGetter: (key, item) => item[key],
    isCheckBoxColumn: true,
  },
  {
    dataKey: 'title',
    label: _t('Title'),
    width: 200,
    cellRenderer: renderTitleWithIcon,
    cellDataGetter: (key, item) => item[key],
  }, {
    dataKey: 'used_in_document_types',
    label: _t('Evidence type'),
    width: 240,
    flexGrow: 2,
    height: 350,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => `${i.code} ${i.name}`),
  }, {
    dataKey: 'used_in_assessments',
    label: _t('Used in'),
    width: 200,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => i.name),
  }, {
    dataKey: 'usable_for_assessments',
    label: _t('Usable for'),
    width: 200,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => i.name),
  },
  // {
  //   dataKey: 'expiration_date',
  //   label: _t('Expiry date'),
  //   width: 170,
  //   cellDataGetter: (key, item) => item[key],
  // },
  {
    dataKey: 'modified_time',
    label: _t('Date modified'),
    width: 200,
  },
];

const columnSettingsWithoutSelect = [
  {
    dataKey: 'title',
    label: _t('Title'),
    width: 200,
    cellRenderer: renderTitleWithIcon,
    cellDataGetter: (key, item) => item[key],
  }, {
    dataKey: 'used_in_document_types',
    label: _t('Evidence type'),
    width: 240,
    flexGrow: 2,
    height: 350,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => `${i.code} ${i.name}`),
  }, {
    dataKey: 'used_in_assessments',
    label: _t('Used in'),
    width: 200,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => i.name),
  }, {
    dataKey: 'usable_for_assessments',
    label: _t('Usable for'),
    width: 200,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => i.name),
  },
  {
    dataKey: 'modified_time',
    label: _t('Date modified'),
    width: 200,
  },
];

const ArchiveList = React.createClass({

  propTypes: {
    onUploadModalResult: React.PropTypes.func.isRequired,
    sortTable: React.PropTypes.func,
    dataTable: React.PropTypes.object.isRequired,
    queryInput: React.PropTypes.object,
    archive: React.PropTypes.array.isRequired,
    dontShowPreview: React.PropTypes.func.isRequired,
    selectedAttachments: React.PropTypes.array,
    onShareEvidenceClicked: React.PropTypes.func,
    isSharingAllowed: React.PropTypes.bool.isRequired,
  },

  mixins: [
    FluxMixin,
  ],

  getInitialState() {
    const { archive, selectedAttachments } = this.props;
    return {
      archive,
      query: '',
      selectedAttachments: selectedAttachments || [],
    };
  },

  componentWillMount() {
    this.columnSettings = this.props.isSharingAllowed ? columnSettings : columnSettingsWithoutSelect;
    this.columnSettings.rowGetter = this.rowGetter;
  },

  componentWillReceiveProps(props) {
    const { archive, selectedAttachments, dataTable } = props;
    if (dataTable) {
      this.props.dataTable = dataTable;
    }
    this.setState({
      archive,
      selectedAttachments,
    });
  },


  onQueryInputChange(query) {
    const archive = tools.objectFilter(this.props.archive, query, this.props.dataTable.filterKeys);
    this.setState({
      archive,
      query,
    });
  },

  onTitleClick(uuid, e) {
    const previewAttachment = this.state.archive.find(attachment => uuid === attachment.uuid);
    tools.forceFunction(this.props.dataTable.onRowClick)(previewAttachment);
  },

  onHeaderCheckBoxClick(column, e) {
    const selectedAttachments = e.target.checked ?
      _.union(this.state.selectedAttachments, this.state.archive.filter(attachment => (
        !this.state.selectedAttachments.find(a => a.uuid === attachment.uuid)
      ))) : this.state.selectedAttachments.filter(attachment => (
        !this.state.archive.find(a => a.uuid === attachment.uuid)
      ));

    const flux = this.getFlux();
    flux.actions.shareEvidence.updateSelectedAttachments(selectedAttachments);
  },

  onShareEvidence() {
    if (this.state.selectedAttachments.length) {
      this.props.onShareEvidenceClicked();
    }
  },

  handleCheckBoxClicked(attachmentUuid, e) {
    let selectedAttachments = this.state.selectedAttachments;

    if (e.target.checked) {
      const selectedAttachment = this.state.archive.find(attachment => attachmentUuid === attachment.uuid);
      selectedAttachments.push(selectedAttachment);
    } else {
      selectedAttachments = this.state.selectedAttachments.filter(attachment => attachment.uuid !== attachmentUuid);
    }

    const flux = this.getFlux();
    flux.actions.shareEvidence.updateSelectedAttachments(selectedAttachments);
  },

  sortTable(column, sortOrder) {
    tools.forceFunction(this.props.sortTable)(column, sortOrder);
  },

  rowGetter(index) {
    const item = this.state.archive[index];

    const title = {
      name: item.title,
      attachmentType: item.attachment_type,
      uuid: item.uuid,
      titleClicked: this.props.dontShowPreview(item) ? null : this.onTitleClick,
    };

    const checkBox = {
      uuid: item.uuid,
      handleCheckBoxClicked: this.handleCheckBoxClicked,
      isChecked: !!this.state.selectedAttachments.find(attachment => attachment.uuid === item.uuid),
    };

    const row = {
      check_box: checkBox,
      title,
      used_in_document_types: item.used_in_document_types,
      used_in_assessments: item.used_in_assessments,
      usable_for_assessments: item.usable_for_assessments,
      // expiration_date: tools.dateFilter(item.expiration_date, pageData.dateFormat, pageData.languageCode),
      modified_time: tools.dateFilter(item.modified_time, pageData.dateFormat, pageData.languageCode),
    };

    return row;
  },

  rowHeightGetter(index) {
    const item = this.state.archive[index];
    if (!item) return null;
    const maxItems = Math.max.apply(this, [
      item.used_in_document_types.length || 1,
      item.used_in_assessments.length || 1,
      item.usable_for_assessments.length || 1,
    ]);
    return maxItems * 40;
  },

  rowClassNameGetter(index) {
    return 'no-hover';
  },

  evaluateSelectAllStatus() {
    let selectAll = false;
    const { archive, selectedAttachments } = this.state;

    if (archive && selectedAttachments) {
      const currentSelectedAttachments = archive.filter(a => (
        selectedAttachments.find(att => att.uuid === a.uuid)
      ));

      selectAll = (currentSelectedAttachments.length === archive.length);
    }
    return selectAll;
  },

  render() {
    const isSelectAllChecked = this.evaluateSelectAllStatus();
    this.props.queryInput.onInputChange = this.onQueryInputChange;

    let tableHeight = 50;
    if (this.state.archive) {
      this.state.archive.forEach((entry, index) => (
        tableHeight += this.rowHeightGetter(index)
      ));
    }

    const tableWidth = 1180;

    return (
      <div className="archive-list">
        <div className="toolbar" style={{ width: tableWidth }} >
          <div style={{ display: 'inline-block' }}>
            <UploadFileButtonWithModal
              buttonText={_t('Add Evidence')}
              buttonIcon={'plus'}
              onSuccess={this.props.onUploadModalResult}
            />
          </div>
          <div style={{ display: 'inline-block', marginLeft: '5px' }}>
            <ShareEvidenceButton
              onClick={this.onShareEvidence}
              count={this.state.selectedAttachments.length}
              isSharingAllowed={this.props.isSharingAllowed}
            />
          </div>
        </div>
        {
          this.state.archive ? (
            <div className="archive-list-table">
              <DataTable
                sortTable={this.sortTable}
                rowsCount={this.state.archive.length}
                columnSettings={this.columnSettings}
                dataTable={this.props.dataTable}
                rowHeightGetter={this.rowHeightGetter}
                width={tableWidth}
                height={tableHeight}
                rowClassNameGetter={this.rowClassNameGetter}
                onHeaderCheckBoxClick={this.onHeaderCheckBoxClick}
                headerCheckBoxChecked={isSelectAllChecked}
                overflowX={'hidden'}
                overflowY={'hidden'}
              />
            </div>
          ) : (
            <BusyFeedback />
          )
        }
      </div>
    );
  },

});

module.exports = ArchiveList;
