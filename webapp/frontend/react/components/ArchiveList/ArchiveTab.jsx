const ArchiveList = require('./ArchiveList.jsx');
const AttachmentsUtils = require('../Utils/attachments.js');
import { EditAttachmentModalController } from '../Attachments/Modals/EditAttachmentModalController.jsx';
import { Pagination } from '../Utils/pagination.jsx';
import { ArchiveFilters } from '../ArchiveFilters/ArchiveFilters.jsx';
const ShareEvidenceModalController = require('../ShareEvidence/ShareEvidenceModalController.jsx');
require('./pageSplit.less');
const pageData = require('../Utils/pageData.js');

const queryInputProps = {
  placeholder: _t('search for title'),
  filterKeys: ['title'],
};

export const ArchiveTab = React.createClass({
  mixins: [
    FluxMixin,
    GeneralEventsMixin,
    StoreWatchMixin('AttachmentsStore', 'ShareEvidenceStore'),
  ],

  getInitialState() {
    return {
      openedAttachment: false,
    };
  },

  componentWillMount() {
    const pageNumber = this.getFlux().stores.AttachmentsStore.pageNumber;
    this.getFlux().actions.attachments.applyPageNumber({ pageNumber });
  },

  componentDidMount() {
    // show the list inverse-sorted on modified_time so the latest
    // added files will show as first in the list
    const attachmentsStore = this.getFlux().store('AttachmentsStore');
    this.addGeneralEvent(attachmentsStore, 'attachments-modified', () => {
      this.getFlux().actions.attachments.reLoadFilteredAttachments();
    });
  },

  getStateFromFlux() {
    const attachmentsStoreState = this.getFlux().store('AttachmentsStore').getState();
    const shareEvidenceStoreState = this.getFlux().store('ShareEvidenceStore').getState();

    return {
      paginatedAttachments: attachmentsStoreState.filteredAttachments,
      pageCount: attachmentsStoreState.pageCount,
      pageNumber: attachmentsStoreState.pageNumber,
      isLoading: attachmentsStoreState.isLoading,
      sortOrder: attachmentsStoreState.sortOrder,
      selectedColumn: attachmentsStoreState.selectedColumn,
      shareEvidenceStoreState,
    };
  },

  onUploadModalResult(files, validityType, date) {
    log.info('on upload modal result', files);

    if (!files) {
      return;
    }

    const attachments = [];

    files.forEach(file => {
      const attachmentData = {
        attachment_type: 'FileAttachment',
        original_file_name: file.name,
        file: file.data_url,
        expiration_date: tools.dateToIso8601(date),
        organization: window.jsContext.CURRENT_ORG_UUID,
      };
      const attachment = AttachmentsUtils.objectToAttachment(attachmentData);
      attachments.push(attachment);
    });

    const flux = this.getFlux();
    attachments.forEach((attachment) => {
      flux.actions.attachments.saveAttachment(attachment);
    });
  },

  onRowClick(attachment) {
    if (!this.dontShowPreview(attachment)) {
      this.setState({ openedAttachment: attachment });
      this.refs.EditAttachmentModalController.openModal();
    }
  },

  onAttachmentModalSuccess(modifiedAttachment) {
    const flux = this.getFlux();
    flux.actions.attachments.updateAttachment(modifiedAttachment);
    this.refs.EditAttachmentModalController.onCancel();
  },

  onAttachmentDelete(attachment) {
    const flux = this.getFlux();
    flux.actions.attachments.deleteAttachment(attachment);
    this.refs.EditAttachmentModalController.onCancel();
  },

  onShareEvidenceClicked() {
    const flux = this.getFlux();
    const selectedAttachments = this.state.shareEvidenceStoreState.selectedAttachments;
    flux.actions.shareEvidence.getPartnerOrganizationsPage(1);
    flux.actions.shareEvidence.getAlreadySharedOrganizations(selectedAttachments);
    flux.actions.shareEvidence.startSharing();
  },

  sortTable(column, sortOrder) {
    this.getFlux().actions.attachments.sortFilteredAttachments(column, sortOrder);
  },

  handlePageChange(pageNumber) {
    this.setState({ paginatedAttachments: null });
    this.getFlux().actions.attachments.applyPageNumber({ pageNumber });
  },

  dontShowPreview(attachment) {
    const forbiddenTypes = ['TextReferenceAttachment', 'AgriformAttachment'];
    return _.includes(forbiddenTypes, attachment.attachment_type);
  },

  render() {
    const { paginatedAttachments, openedAttachment, pageCount, pageNumber,
      isLoading, sortOrder, selectedColumn, shareEvidenceStoreState } = this.state;

    const { selectedAttachments } = shareEvidenceStoreState;

    const { isSharingAllowed } = pageData.context;

    const dataTableProps = {
      onRowClick: this.onRowClick,
      selected: selectedColumn,
      sortOrder,
    };

    return (
      <div className="archive-tab page-split">
        <div className="filters-section left">
          <ArchiveFilters />
        </div>
        <div className="table-section right">
          <EditAttachmentModalController
            ref="EditAttachmentModalController"
            isDescriptionReadOnly={openedAttachment && openedAttachment.is_frozen}
            isDeleteReadOnly={openedAttachment && openedAttachment.is_frozen}
            onAttachmentModalSuccess={this.onAttachmentModalSuccess}
            onAttachmentDelete={this.onAttachmentDelete}
            attachment={openedAttachment}
          />
          <ShareEvidenceModalController
            {...shareEvidenceStoreState}
          />
          <ArchiveList
            archive={isLoading ? null : paginatedAttachments}
            onUploadModalResult={this.onUploadModalResult}
            sortTable={this.sortTable}
            dataTable={dataTableProps}
            queryInput={queryInputProps}
            dontShowPreview={this.dontShowPreview}
            onShareEvidenceClicked={this.onShareEvidenceClicked}
            selectedAttachments={selectedAttachments}
            isSharingAllowed={isSharingAllowed}
          />
          <Pagination
            numberOfPages={pageCount}
            handlePageChange={this.handlePageChange}
            activePage={pageNumber}
          />
        </div>
      </div>
    );
  },

});

