

var pageData = require('../Utils/pageData.js');
var ProductModuleDoc = require('../ProductModuleDoc/ProductModuleDoc.jsx');
var Link = require('react-router').Link;
var History = require('react-router').History;

require('./QuestionnaireList.less');

var QuestionnaireList = React.createClass({

  propTypes: {
    assessment: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
    History,
  ],

  //
  // Handlers
  //

  getCompleteQuestionsCountByQuestionnaire(assessment, uuid) {
    const questionnaire = assessment.assessmentType.questionnaires[uuid];
    return assessment.getCompleteQuestionsCountByQuestionnaire(uuid);
  },

  //
  // Render
  //

  render() {
    var assessment = this.props.assessment;
    var questionnaires = _.sortBy(assessment.assessmentType.questionnaires, 'order_index');
    var membershipUuid = pageData.context.membershipUuid;
    return (
      <div className="QuestionnaireList">
        <Bootstrap.Table striped >
          <thead>
            <th>{_t('Questionnaire')}</th>
            <th></th>
          </thead>
          <tbody>
            {questionnaires.map((questionnaire) => {
              var link = `/${membershipUuid}/${this.props.params.assessmentAccessMode}/assessments/${assessment.uuid}/questionnaires/${questionnaire.uuid}/edit`;
              const questions = _.pick(questionnaire.questions, question => question.is_statistics_valuable && assessment.getElementState(question.uuid).isVisible);
              var completeQuestionsCount = _.size(_.pick(questions, question => assessment.isQuestionDone(question.uuid)));
              var totalQuestionsCount = _.size(questions);
              return (
                <tr key={questionnaire.uuid}>
                  <td>
                    <Link to={link}>
                      <span className="fa-list fa icon"></span>
                      <span className="questionnaire-name">{questionnaire.name}</span>
                    </Link>
                    <br />
                    <ProductModuleDoc docUrl={questionnaire.help_document} />
                  </td>
                  <td className="question-count">{`${completeQuestionsCount}/${totalQuestionsCount}`}</td>
                </tr>
              );
            })}
          </tbody>
        </Bootstrap.Table>
      </div>
    );
  },

});

module.exports = QuestionnaireList;
