import React, { Component } from 'react';
import { Link } from 'react-router';

export class StatusBox extends Component {
  render() {
    const { status, params } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;
    const assessmentUrl = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/detail`;

    let statusText = null;
    if (status === 'pending') {
      statusText = (
        <div style={{ display: 'inline-block' }}>
          {_t('In progress')}
        </div>
      );
    } else if (status === 'done') {
      statusText = (
        <div style={{ display: 'inline-block' }}>
          {_t('Action completed')}
          {'. '}
          <Link to={assessmentUrl}>
            {_t('Go to your assessment')}
          </Link>
        </div>
      );
    } else if (status === 'error') {
      statusText = (
        <div style={{ display: 'inline-block' }}>
          {_t('An error occurred')}
          {'. '}
          {_t('Please contact')}
          {': '}
          <a href={"mailto:support@agriplace.com"}>{'support@agriplace.com'}</a>
        </div>
      );
    }

    return (
      <div>
        <strong>{`${_t('Status')}: `}</strong>
        {statusText}
      </div>
    );
  }
}

StatusBox.propTypes = {
  status: React.PropTypes.string,
  params: React.PropTypes.object,
};
