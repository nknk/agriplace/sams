import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import classNames from 'classnames';
import './AssessmentReuseStatus.less';

const REUSE_STATUS_CHECK_INTERVAL = 5; // SECONDS
const REUSE_STATUS_CHECK_TIMEOUT = 60; // SECONDS

export class ReusePeriodicUpdate extends Component {

  constructor() {
    super();

    this.onRefresh = this.onRefresh.bind(this);

    this.state = {
      intervalTimer: null,
      timeoutTimer: null,
    };
  }

  componentWillMount() {
    const { status, fetchStatus, params } = this.props;
    const { assessmentUuid } = params;

    const getStatusUpdate = () => {
      fetchStatus(assessmentUuid);
    };

    const onTimeout = () => {
      this.clearTimers();
    };

    if (status === 'pending') {
      this.setState({
        intervalTimer: setInterval(getStatusUpdate, REUSE_STATUS_CHECK_INTERVAL * 1000),
        timeoutTimer: setInterval(onTimeout, REUSE_STATUS_CHECK_TIMEOUT * 1000),
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { status } = nextProps;

    if (status !== 'pending') {
      // if status not pending, stop timer
      this.clearTimers();
    }
  }

  componentWillUnmount() {
    this.clearTimers();
  }

  onRefresh(e) {
    e.preventDefault();
    const { fetchStatus, params } = this.props;
    const { assessmentUuid } = params;
    fetchStatus(assessmentUuid);
  }

  clearTimers() {
    const { intervalTimer, timeoutTimer } = this.state;
    if (intervalTimer) { clearInterval(intervalTimer); }
    if (timeoutTimer) { clearInterval(timeoutTimer); }

    this.setState({
      intervalTimer: null,
      timeoutTimer: null,
    });
  }

  render() {
    const { status, fetchStatusInProgress } = this.props;
    const { timeoutTimer } = this.state;

    const disabled = !!timeoutTimer || fetchStatusInProgress;
    const showSpinner = (status === 'pending');

    const spinnerIconClassnames = classNames({
      icon: true,
      fa: true,
      'fa-refresh': true,
      'fa-lg': true,
      'fa-spin': fetchStatusInProgress,
    });

    return (
      <div className="refresh-button">
        {showSpinner && (
          <Button
            bsStyle="default"
            disabled={disabled}
            onClick={this.onRefresh}
          >
            <i
              className={spinnerIconClassnames}
              aria-hidden="true"
              onClick={!disabled && this.onRefresh}
            />
            {' '}
            {_t('Refresh')}
          </Button>
        )}
      </div>
    );
  }
}

ReusePeriodicUpdate.propTypes = {
  status: React.PropTypes.string,
  params: React.PropTypes.object,
  fetchStatus: React.PropTypes.func,
  fetchStatusInProgress: React.PropTypes.bool,
};
