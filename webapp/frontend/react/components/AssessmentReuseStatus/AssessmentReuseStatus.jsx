import React, { Component } from 'react';
import './AssessmentReuseStatus.less';
import { StatusBox } from './StatusBox.jsx';
import { ReusePeriodicUpdate } from './ReusePeriodicUpdate.jsx';

export class AssessmentReuseStatus extends Component {
  render() {
    let forExample = _t('for example');
    forExample = (forExample === 'for example') ? 'e.g.' : forExample;

    return (
      <div className="assessment-reuse-status">
        <div className="heading">
          {`${_t('Action')}: ${_t('Assessment reuse')}`}
        </div>
        <div>
          {`${_t('We are currently copying data from a previous assessment')}. `}
          {`${_t('You can continue your work')} (${forExample} ${_t('on other assessments')}) `}
          {`${_t('or stay on this page until the action has been completed')}. `}
          {`${_t('If this action takes longer than 1-2 minutes, please refresh your page')}.`}
        </div>
        <br />
        <StatusBox {...this.props} />
        <ReusePeriodicUpdate {...this.props} />
      </div>
    );
  }
}

AssessmentReuseStatus.propTypes = {
  status: React.PropTypes.string,
  params: React.PropTypes.object,
  fetchStatus: React.PropTypes.func,
  fetchStatusInProgress: React.PropTypes.bool,
};
