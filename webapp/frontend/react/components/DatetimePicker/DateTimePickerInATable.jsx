import React, { Component, PropTypes } from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import { DatetimePicker } from './DatetimePicker.jsx';
import './DatetimePicker.less';
import './PopoverDateTimePicker.less';

export default class DateTimePickerInATable extends Component {

  constructor() {
    super();
    this.onChange = this.onChange.bind(this);
    this.onExit = this.onExit.bind(this);
  }

  onChange(dateObject) {
    this.dateTime = dateObject;
  }

  onExit(workflowUuid) {
    if (this.dateTime) {
      this.props.onChange(workflowUuid, this.dateTime);
    }
  }

  render() {
    const { date, assessmentInfo, disabled, readOnly, isDateValid } = this.props;

    const picker = (
      <DatetimePicker
        key={assessmentInfo.workflow_uuid}
        value={date}
        onChange={(dateObject) => this.onChange(dateObject)}
        dateFormat="DD-MM-YYYY"
        timeFormat="HH:mm"
        isValid={isDateValid}
        showPickerOnly
        disabled={disabled}
        readOnly={readOnly}
      />
    );
    const pickerPopOver = (
      <Popover
        id={`popver-date-time-${assessmentInfo.workflow_uuid}`}
        className="remove-decorations-popover"
      >
        {picker}
      </Popover>
    );

    return disabled || readOnly ? (
      <div className="cell-padding disabled popover-input-field">
        {isDateValid ? date : null}
      </div>
    ) : (
      <OverlayTrigger
        rootClose
        trigger="click"
        placement="bottom"
        overlay={pickerPopOver}
        onExit={() => this.onExit(assessmentInfo.workflow_uuid)}
      >
        <div className="cell-padding popover-input-field">
          {isDateValid ? date : null}
        </div>
      </OverlayTrigger>
    );
  }
}

DateTimePickerInATable.propTypes = {
  assessmentInfo: PropTypes.object,
  date: PropTypes.string,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  onChange: PropTypes.func,
  isDateValid: PropTypes.bool,
};
