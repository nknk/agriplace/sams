import React, { Component, PropTypes } from 'react';
import Datetime from 'react-datetime';
import cx from 'classnames';

import 'react-datetime/css/react-datetime.css';
import './PopoverDateTimePicker.less';

import moment from 'moment';

export default class DatetimePicker extends Component {

  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.verifyStartEnd = this.verifyStartEnd.bind(this);
    this.parseSelectedDate = this.parseSelectedDate.bind(this);

    const { value, dateFormat, timeFormat } = props;
    const dateTimeObject = value && moment(value, `${dateFormat} ${timeFormat}`);
    this.parseSelectedDate({ props, dateTime: dateTimeObject, init: true });
  }

  componentWillReceiveProps(nextProps) {
    const { dateTime } = this.state;
    this.parseSelectedDate({ props: nextProps, dateTime, init: true });
  }

  onBlur() {
    const { dateTime } = this.state;
    if (dateTime) {
      this.props.onChange(dateTime);
    }
  }

  parseSelectedDate({ props, dateTime, init, cb }) {
    const { isOptional, startDate, endDate } = props;
    const isDateTime = moment.isMoment(dateTime) && dateTime.isValid();
    const isStartDateTime = moment.isMoment(startDate);
    const isEndDateTime = moment.isMoment(endDate);

    let isDateTimeValid = isDateTime;
    if (isOptional) {
      isDateTimeValid = dateTime === '' || isDateTimeValid;
    }

    if (isStartDateTime && isDateTime) {
      const isStartValid = dateTime.isAfter(startDate);
      isDateTimeValid = isDateTimeValid && isStartValid;
    }

    if (isEndDateTime && isDateTime) {
      const isEndValid = dateTime.isBefore(endDate);
      isDateTimeValid = isDateTimeValid && isEndValid;
    }

    const updatedDateTime = isDateTimeValid ? dateTime : undefined;

    if (!init) {
      this.setState({
        isDateTimeValid: !!isDateTimeValid,
        dateTime: updatedDateTime,
      });
    } else {
      this.state = {
        isDateTimeValid: !!isDateTimeValid,
        dateTime: updatedDateTime,
      };
    }

    if (cb) {
      cb(updatedDateTime);
    }
  }

  handleOnChange(dateTime) {
    const { showPickerOnly } = this.props;

    const cb = (_dateTime) => {
      if (!_dateTime) {
        this.props.onChange(_dateTime);
      } else if (showPickerOnly && _dateTime) {
        this.props.onChange(_dateTime);
      }
    };

    this.parseSelectedDate({ props: this.props, dateTime, cb });
  }

  verifyStartEnd(selectedDateTime) {
    const isSelectedDateTime = moment.isMoment(selectedDateTime) && selectedDateTime.isValid();
    if (!isSelectedDateTime) {
      return true;
    }

    const { startDate, endDate } = this.props;
    const isStartDateTime = moment.isMoment(startDate) && startDate.isValid();
    const isEndDateTime = moment.isMoment(endDate) && endDate.isValid();

    if (!isStartDateTime && !isEndDateTime) {
      return true;
    }

    const isStartValid = !isStartDateTime || (
        isStartDateTime && selectedDateTime.isAfter(startDate.clone().subtract(1, 'day'))
    );

    const isEndValid = !isEndDateTime || (
      isEndDateTime && !selectedDateTime.isAfter(endDate)
    );

    return isStartValid && isEndValid;
  }

  render() {
    const { dateFormat, timeFormat, showPickerOnly, value, disabled, readOnly, error } = this.props;
    const { isDateTimeValid } = this.state;

    const isDisabled = disabled || readOnly;

    const timePickerClassNames = cx({
      'has-error': !isDisabled && (error || !isDateTimeValid),
    });

    return (
      <div className={timePickerClassNames}>
        <Datetime
          dateFormat={dateFormat}
          timeFormat={timeFormat}
          defaultValue={isDateTimeValid ? value : undefined}
          open={showPickerOnly}
          input={!showPickerOnly}
          onChange={this.handleOnChange}
          onBlur={this.onBlur}
          isValidDate={this.verifyStartEnd}
          inputProps={{
            disabled: isDisabled,
            className: 'datetimepicker form-control',
          }}
        />
      </div>
    );
  }
}

DatetimePicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  isValid: PropTypes.bool,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  placeholder: PropTypes.string,
  dateFormat: PropTypes.string,
  timeFormat: PropTypes.string,
  showPickerOnly: PropTypes.bool,
  startDate: PropTypes.object,
  endDate: PropTypes.object,
  isOptional: PropTypes.bool,
};

export { DatetimePicker };
