

var BusyFeedback = React.createClass({

  getInitialState() {
    return {
      seconds: 1,
    };
  },

  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({
        seconds: ++this.state.seconds,
      });
    }, 1000);
  },

  componentWillUnmount() {
    clearInterval(this.timer);
  },


  render() {
    return (
      <div className="busy-feedback" style={{ 'color': 'white', 'fontWeight': 'bold' }}>
        {this.state.seconds}
      </div>
    );
  },

});

module.exports = BusyFeedback;
