import React from 'react';

const BusyFeedback = React.createClass({

  render() {
    return (
      <div className="busy-feedback">
        <span className="icon fa fa-spinner fa-spin fa-3x"></span>
      </div>
    );
  },

});

module.exports = BusyFeedback;
