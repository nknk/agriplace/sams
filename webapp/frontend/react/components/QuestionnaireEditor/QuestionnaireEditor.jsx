import React from 'react';
import { AffixWrapper } from '../Affix/AffixWrapper.jsx';
import './questionnaireEditor.less';
const QuestionnaireElement = require('./QuestionnaireElement.jsx'); // eslint-disable-line no-unused-vars
import QuestionnaireOutline from './QuestionnaireOutline.jsx'; // eslint-disable-line no-unused-vars
import { NO_FILTER } from '../QuestionnaireFilters/FilterOptions';
import autosize from 'autosize';
//
// Questionnaire editor component
//
const QuestionnaireEditor = React.createClass({
  propTypes: {
    assessment: React.PropTypes.object.isRequired,
    documentTypes: React.PropTypes.object.isRequired,  // collection of applicable document types, by uuid
    questionnaire: React.PropTypes.object.isRequired,  // questionnaire definition
    currentOrganization: React.PropTypes.object,
    readOnly: React.PropTypes.bool,
    showElementCodes: React.PropTypes.bool,
    showOutline: React.PropTypes.bool,  // (outline) show questionnaire outline?
    history: React.PropTypes.object,
    location: React.PropTypes.object,
    params: React.PropTypes.object,
    judgements: React.PropTypes.array,
    assessmentActions: React.PropTypes.object,
    answersInProgress: React.PropTypes.object,
    evidenceAssociatedQuestions: React.PropTypes.object,
    attachmentLinksUpdateCounter: React.PropTypes.number,
  },

  componentDidMount() {
    autosize(document.querySelectorAll('textarea'));
    if (window.location.hash) {
      // strip hash '#'
      const location = window.location.hash.slice(1);
      if (location) {
        window.open(`#${location}`, '_self');
      }
    }
  },

  getQuestionnaireElements() {
    const { answersInProgress, questionnaire, assessment, location, documentTypes,
      readOnly, showElementCodes, currentOrganization, assessmentActions, judgements,
      evidenceAssociatedQuestions, attachmentLinksUpdateCounter } = this.props;

    const assessmentAnswers = assessment.getAnswers();

    return _.map(questionnaire.elements, (element, index) => {
      const answer = _.clone(assessmentAnswers[element.uuid]);
      const value = answer && answer.value && answer.value.toLowerCase();
      const judgement = _.find(judgements, j => (j.question === element.uuid)) || { value: '' };
      const answersFilter = (location && location.query && location.query.answers) || NO_FILTER;
      const judgementsFilter = (location && location.query && location.query.judgements) || NO_FILTER;
      const answerInProgress = answersInProgress && answersInProgress[element.uuid];

      // notFiltered is true for any line that evaluates to true
      const notFilteredByAnswers = () => (
        !answersFilter ||
        (answersFilter === 'noFilter') ||
        (element.element_type !== 'question') ||
        ((answersFilter === 'incomplete') && (!assessment.isQuestionDone(element.uuid))) ||
        (answersFilter === value)
      );

      const notFilteredByJudgements = () => (
        !judgementsFilter ||
        (judgementsFilter === 'noFilter') ||
        (element.element_type !== 'question') ||
        (judgementsFilter === judgement.value) ||
        ((judgementsFilter === 'open') && !judgement.value)
      );

      return (
        <div key={`element-${element.uuid}-${index}`}>
          {assessment.isElementVisible(element.uuid) && notFilteredByAnswers() && notFilteredByJudgements() && (
            <QuestionnaireElement
              attachmentLinksUpdateCounter={attachmentLinksUpdateCounter}
              answer={answer}
              assessment={assessment}
              documentTypes={documentTypes}
              element={element}
              questionnaire={questionnaire}
              readOnly={readOnly}
              showCode={showElementCodes}
              currentOrganization={currentOrganization}
              assessmentActions={assessmentActions}
              answerInProgress={answerInProgress}
              evidenceAssociatedQuestions={evidenceAssociatedQuestions}
            />
          )}
        </div>
      );
    });
  },

  render() {
    if (!this.props.questionnaire) {
      return (
        <div>
          <div>{_t('No questionnaire.')}</div>
        </div>
      );
    }

    //
    // Outline part
    //
    const outlinePart = (this.props.showOutline !== false) ? (
      <AffixWrapper uniqueClass="ToolbarUnique hidden-print" triggerOffset={120} isEnabled>
        <div className="ToolbarUnique">
          <QuestionnaireOutline questionnaire={this.props.questionnaire} />
        </div>
      </AffixWrapper>
    ) : null;

    const outlinePartPrint = (this.props.showOutline !== false) ? (
      <div className="ToolbarUnique visible-print">
        <QuestionnaireOutline questionnaire={this.props.questionnaire} />
      </div>
    ) : null;

    //
    // Questionnaire elements part
    //
    const questionnaireElements = (
      <div className="questionnaire-elements">
        {this.getQuestionnaireElements()}
      </div>
    );

    // We have a questionnaire.
    if (outlinePart) {
      // we have an outline, so show 3+9 column layout
      return (
        <div className="questionnaire-editor">
          <div className="row">
            <div className="col-xs-3">
              {outlinePart}
              {outlinePartPrint}
            </div>
            <div className="col-xs-9">
              {questionnaireElements}
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="questionnaire-editor">
        <div className="row">
          <div className="col-xs-12">
            {questionnaireElements}
          </div>
        </div>
      </div>
    );
  },

});

module.exports = QuestionnaireEditor;

