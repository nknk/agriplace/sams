import React from 'react';

const QuestionEditor = require('./QuestionEditor.jsx');
const QuestionnaireHeading = require('./QuestionnaireHeading.jsx');
import { JudgementSection } from '../JudgementSection/JudgementSection.jsx';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
//
// QuestionnaireElement
//
const QuestionnaireElement = React.createClass({
  propTypes: {
    answer: React.PropTypes.object,
    assessment: React.PropTypes.object.isRequired,
    documentTypes: React.PropTypes.object.isRequired,
    element: React.PropTypes.object.isRequired,
    readOnly: React.PropTypes.bool,
    questionnaire: React.PropTypes.object.isRequired,
    showCode: React.PropTypes.bool,
    currentOrganization: React.PropTypes.object,
    assessmentActions: React.PropTypes.object,
    answerInProgress: React.PropTypes.bool,
    evidenceAssociatedQuestions: React.PropTypes.object,
    attachmentLinksUpdateCounter: React.PropTypes.number,
  },

  componentDidMount() {
    const { element } = this.props;
    const hash = window.location.hash && window.location.hash.slice(1);

    let offset;
    const intoView = () => {
      offset = document.getElementById(hash).getBoundingClientRect().top + window.pageYOffset;
      window.scroll(0, offset - 100);
      $('body').scrollspy('refresh');
    };
    const intoViewAgain = () => {
      const _offset = document.getElementById(hash).getBoundingClientRect().top + window.pageYOffset;
      if ((_offset !== offset) && (offset > 500)) {
        window.scroll(0, offset - 100);
      }
      $('body').scrollspy('refresh');
    };

    if (hash && (element.uuid === hash)) {
      setTimeout(intoView, 1000);
      // retry if questionnaire is not rendered completely
      setTimeout(intoViewAgain, 1500);
      setTimeout(intoViewAgain, 2000);
    }
  },

  shouldComponentUpdate(nextProps, nextState) {
    return !(_.isEqual(nextProps, this.props) && _.isEqual(nextState, this.state));
  },

  render() {
    const { assessment } = this.props;
    // Element is a Heading
    if (this.props.element.element_type === 'heading') {
      return (
        <div className="questionnaire-heading">
          <QuestionnaireHeading heading={this.props.element} />
        </div>
      );
    // Element is a Question
    } else if (this.props.element.element_type === 'question') {
      return (
        <div className="QuestionnaireElement" id={this.props.element.uuid}>
          <QuestionEditor
            {...this.props}
            question={this.props.element}
          />
          <PermissionController assessmentUuid={assessment.uuid} component="JudgementSection">
            <JudgementSection
              questionUuid={this.props.element.uuid}
              assessmentUuid={assessment.uuid}
            />
          </PermissionController>
        </div>
      );
    // Element is a Paragraph
    } else if (this.props.element.element_type === 'paragraph') {
      return (
        <div dangerouslySetInnerHTML={{ __html: this.props.element.text }}>
        </div>
      );
    // Element is an Image
    } else if (this.props.element.element_type === 'image') {
      return (
        <div>
          {_t('Image.')}
          {this.props.element.image}
        </div>
      );
    }
    return null;
  },
});

module.exports = QuestionnaireElement;
