import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Button, Modal } = Bootstrap;

export default class AnswerSaveError extends React.Component {

  refreshWindow() {
    window.location.reload(true);
  }

  render() {
    const { answersHaveError, isApplicationOnline } = this.props;

    return (
      <Modal
        className="answer-save-error"
        show={answersHaveError && isApplicationOnline}
        onRequestHide={null}
        backdrop={'static'}
      >
        <Modal.Header>
          <h2>
            <strong>{_t('Error')}</strong>
          </h2>
        </Modal.Header>
        <Modal.Body>
          {_t('An unexpected error occurred')}
          {'. '}
          {_t('Please refresh the page')}
          {'. '}
          {_t('If the error remains')}
          {', '}
          <a href={"mailto:support@agriplace.com"}>{_t('contact AgriPlace support')}</a>
          {'.'}
        </Modal.Body>
        <Modal.Footer>
          <div className="button-toolbar">
            <Button
              role="button"
              bsStyle="success"
              onClick={this.refreshWindow}
            >
              {_t('OK')}
            </Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

AnswerSaveError.propTypes = {
  answersHaveError: React.PropTypes.bool.isRequired,
  isApplicationOnline: React.PropTypes.bool.isRequired,
};
