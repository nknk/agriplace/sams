import React from 'react';

//
// Questionnaire Heading component
//

const QuestionnaireHeading = React.createClass({
  propTypes: {
    heading: React.PropTypes.object,
  },

  render() {
    if (this.props.heading.level === 1) {
      return (
        <h1
          id={this.props.heading.uuid}
          dangerouslySetInnerHTML={{ __html: this.props.heading.text }}
        >
        </h1>
      );
    } else if (this.props.heading.level === 2) {
      return (
        <h2
          id={this.props.heading.uuid}
          dangerouslySetInnerHTML={{ __html: this.props.heading.text }}
        >
        </h2>
      );
    }

    return (
      <h3
        id={this.props.heading.uuid}
        dangerouslySetInnerHTML={{ __html: this.props.heading.text }}
      >
      </h3>
    );
  },
});

module.exports = QuestionnaireHeading;

