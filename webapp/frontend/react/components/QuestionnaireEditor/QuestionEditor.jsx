import React from 'react';
import pageData from '../Utils/pageData';
const { layout } = pageData.context;
const AnswerEditor = require('./AnswerEditor.jsx');
const AssessmentDocumentType = require('../AssessmentDocumentType/AssessmentDocumentType.jsx');
import { QuestionnaireInstructions } from './QuestionnaireInstructions.jsx';

//
// Question Edit
//
const QuestionEditor = React.createClass({
  propTypes: {
    answer: React.PropTypes.object,  // not required!
    assessment: React.PropTypes.object.isRequired,
    documentTypes: React.PropTypes.object.isRequired,
    question: React.PropTypes.object.isRequired,
    questionnaire: React.PropTypes.object.isRequired,
    readOnly: React.PropTypes.bool,
    showCode: React.PropTypes.bool,
    currentOrganization: React.PropTypes.object,
    assessmentActions: React.PropTypes.object,
    answerInProgress: React.PropTypes.bool,
    evidenceAssociatedQuestions: React.PropTypes.object,
  },

  getInitialState() {
    return {
      isExtraTextVisible: false,
    };
  },

  toggleExtraText() {
    this.setState({
      isExtraTextVisible: !this.state.isExtraTextVisible,
    });
  },

  render() {
    const { answer, question, assessment, showCode, readOnly, documentTypes,
      currentOrganization, assessmentActions, answerInProgress, evidenceAssociatedQuestions } = this.props;
    let className = 'question-editor ';
    const justification = answer ? answer.justification : '';
    const value = answer ? answer.value : '';
    const answerUuid = answer && answer.uuid;
    const isDone = assessment.isQuestionDone(question.uuid);

    if (isDone) {
      className += 'answered';
    }

    const level = assessment.assessmentType.getQuestionLevel(question.level_object);

    const shortQuestionCode = tools.removePrefixFromString(question.code, '-');
    const possibleAnswerSets = assessment.assessmentType.possibleAnswerSets || [];
    const possibleAnswerSet = possibleAnswerSets[question.possible_answer_set];

    const isAgriformAttachable = !!_.filter(question.document_types_attachments || [], documentTypeUuid => {
      const documentType = documentTypes.find(docType => docType.uuid === documentTypeUuid);
      return !!documentType.assessment_type && !(documentType.generic_form);
    }).length;

    // Hide question code if showCode prop = false
    // Used in AgriForm modals for not showing any codes at all.
    const questionInfoPart = showCode !== false ? (
      <div className="question-info">
        {shortQuestionCode}
      </div>
    ) : null;

    const isJustificationRequired = assessment.getIsJustificationRequired(question.uuid);

    return (
      <div className={className}>
        {/* level */}
        <div className="question-label-block">
          {isAgriformAttachable && (layout === 'hzpc') && (
            <div className="agriform-label">
              {_t('Agriform')}
            </div>
          )}
          {level && (
            <div style={{ backgroundColor: level.color }} className="question-level-label">
              {level.title}
            </div>
          )}
        </div>
        <div className="question-editor-content">
          {questionInfoPart}
          <div className="question-text" dangerouslySetInnerHTML={{ __html: question.text }}></div>
          <QuestionnaireInstructions
            criteria={question.criteria}
            guidance={question.guidance}
          />
          <div>
            <AnswerEditor
              justification={justification}
              question={question}
              isJustificationRequired={isJustificationRequired}
              possibleAnswerSet={possibleAnswerSet}
              readOnly={readOnly}
              value={value}
              answerUuid={answerUuid}
              assessmentActions={assessmentActions}
              answerInProgress={answerInProgress}
              assessmentUuid={assessment.uuid}
            />
            <div className="evidence">
              {
                question.document_types_attachments.map((documentTypeUuid) => {
                  // This part will not appear when traversing down from Evidence list App
                  const documentType = documentTypes.find(docType => docType.uuid === documentTypeUuid);
                  if (documentType) {
                    return (
                      <AssessmentDocumentType
                        key={documentTypeUuid}
                        isAnswered={isDone}
                        assessment={assessment}
                        readOnly={readOnly}
                        layout="question"
                        currentOrganization={currentOrganization}
                        documentType={documentType}
                        evidenceAssociatedQuestions={evidenceAssociatedQuestions}
                      />
                    );
                  }
                  log.info(`No document type <${documentTypeUuid}> was found for question ${question.uuid}`);
                  return null;
                })
              }
            </div>
          </div>
        </div>
      </div>
    );
  },

});

module.exports = QuestionEditor;

