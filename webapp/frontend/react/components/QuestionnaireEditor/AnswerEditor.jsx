import React from 'react';
import classnames from 'classnames';
const FormEditors = require('../FormEditors');
const Show = require('../Show.jsx');

const AnswerEditor = React.createClass({

  propTypes: {
    assessmentUuid: React.PropTypes.string,
    justification: React.PropTypes.string,
    possibleAnswerSet: React.PropTypes.object,
    question: React.PropTypes.object,
    readOnly: React.PropTypes.bool,
    isJustificationRequired: React.PropTypes.bool,
    value: React.PropTypes.string,
    answerUuid: React.PropTypes.string,
    assessmentActions: React.PropTypes.object,
    answerInProgress: React.PropTypes.bool,
  },

  getInitialState() {
    return {
      justification: this.props.justification || '',
      isJustificationEdited: false,
    };
  },

  componentWillReceiveProps(updatedProps) {
    const { answerInProgress } = updatedProps;
    if (!answerInProgress) {
      this.setState({
        justification: updatedProps.justification,
      });
    }
  },

  onJustificationChange(event) {
    this.setState({
      justification: event.target.value,
      isJustificationEdited: true,
    });
  },

  onJustificationBlur() {
    const { justification } = this.state;
    if (justification !== this.props.justification) {
      this._sendUpdatedData({ justification });
    }
  },

  _onValueChange(value) {
    this._sendUpdatedData({ value });
  },

  // all methods converge to this one before calling an action
  _sendUpdatedData(params) {
    if (this.answerTrigger) {
      clearTimeout(this.answerTrigger);
    }

    params.justification = (typeof params.justification !== 'undefined') ?
      params.justification : this.state.justification;

    params.value = (typeof params.value !== 'undefined') ?
      params.value : this.props.value;

    const inputPresent = !!params.value || !!params.justification;
    const { answerUuid, assessmentActions, assessmentUuid, question } = this.props;
    const questionUuid = question.uuid;

    let updateAnswer;
    if (!answerUuid && inputPresent) {
      updateAnswer = () => { assessmentActions.createAnswer({ assessmentUuid, questionUuid, answer: params }); };
    } else if (answerUuid && !inputPresent) {
      updateAnswer = () => { assessmentActions.deleteAnswer({ assessmentUuid, questionUuid, answerUuid }); };
    } else if (answerUuid && inputPresent) {
      updateAnswer = () => {
        assessmentActions.updateAnswer({ assessmentUuid, questionUuid, answerUuid, answer: params });
      };
    }

    if (updateAnswer) {
      this.answerTrigger = setTimeout(updateAnswer, 100);
    }
  },

  render() {
    const { readOnly, answerInProgress, question } = this.props;
    const disabled = readOnly || answerInProgress;
    const editorClasses = classnames(
      'answer-editor',
      { shrinked: this.props.question.answer_type === 'single_choice_buttons' }
    );

    const justificationClassess = classnames(
      'justification-section',
      'hideFromPrint',
      { warning: this.props.isJustificationRequired && !this.state.justification }
    );
    const justificationPlaceholder = question.justification_placeholder || _t('Please enter your explanation here');

    const value = this.props.value || '';

    const possibleAnswers = this.props.possibleAnswerSet ?
      this.props.possibleAnswerSet.possible_answers :
      [];

    // Choose a FormEditor to use depending on the question's answer_type
    const editorClassMapping = {
      single_choice_buttons: FormEditors.FormSingleChoiceButtonsEditor,
      checkboxes: FormEditors.FormCheckboxesEditor,
      memo: FormEditors.FormMemoEditor,
      radio: FormEditors.FormRadioButtonsEditor,
      text: FormEditors.FormTextEditor,
      default: FormEditors.FormSingleChoiceButtonsEditor,
    };

    const Editor = editorClassMapping[this.props.question.answer_type] || editorClassMapping.default;

    return (
      <div className={editorClasses}>
        <div className="editor">
          <Editor
            name={`answer-${this.props.question.uuid}`}
            possibleAnswers={possibleAnswers}
            readOnly={disabled}
            value={value}
            onChange={this._onValueChange}
          />
        </div>
        <Show when={this.props.question.is_justification_visible}>
          <div className={justificationClassess}>
            <textarea
              className="form-control"
              placeholder={justificationPlaceholder}
              value={this.state.justification}
              onChange={this.onJustificationChange}
              onBlur={this.onJustificationBlur}
              disabled={disabled}
              rows="1"
            />
          </div>
        </Show>
      </div>
    );
  },

});

module.exports = AnswerEditor;
