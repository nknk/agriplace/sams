import React from 'react';
import './QuestionnaireOutline.less';

export default class QuestionnaireOutline extends React.Component {

  constructor(props) {
    super(props);
    const { questionnaire } = props;

    const outlineHeadings = [];
    _.each(questionnaire.elements, (element) => {
      if (element.element_type === 'heading' && element.level === 1) {
        outlineHeadings.push(element);
      }
    });

    this.state = {
      outlineHeadings,
      questionnaireName: questionnaire.name,
    };
  }

  componentDidMount() {
    $('body').scrollspy({ target: '.questionnaire-outline', offset: 150 + 30 });
    $('.questionnaire-outline a').on('click', event => {
      event.preventDefault();
      const hash = event.currentTarget.hash && event.currentTarget.hash.slice(1);
      if (hash) {
        const offset = document.getElementById(hash).getBoundingClientRect().top + window.pageYOffset;
        location.hash = hash;
        window.scroll(0, offset - 100);
        const scrollToPosition = () => {
          const _offset = document.getElementById(hash).getBoundingClientRect().top + window.pageYOffset;
          if ((_offset !== offset) && (offset > 500)) {
            window.scroll(0, _offset - 100);
          }
        };
        const refreshScrollSpy = () => {
          $('body').scrollspy('refresh');
        };
        setTimeout(scrollToPosition, 500); // correction for affix
        setTimeout(refreshScrollSpy, 1000);
      }
    });
  }

  scrollToTop() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera
    document.documentElement.scrollTop = 0; // For IE and Firefox
  }

  render() {
    const { outlineHeadings, questionnaireName } = this.state;
    return (
      <div className="QuestionnaireOutline questionnaire-outline">
        <ul className="questionnaire-outline-nav nav nav-pills nav-stacked">
          <li>
            <a href="#" className="link" onClick={this.scrollToTop}>
              <span>{_t('Top')}</span>&nbsp;
              <span className="icon fa fa-caret-up" />
            </a>
          </li>
          <li>
            <h2 className="questionnaire-name">
              {questionnaireName}
            </h2>
          </li>
            {outlineHeadings.map((heading, i) => (
              <li key={heading.uuid}>
                <a className="link" href={`#${heading.uuid}`}>
                  <span dangerouslySetInnerHTML={{ __html: heading.text }}>
                  </span>
                </a>
              </li>
            ))}
        </ul>
      </div>
    );
  }
}

QuestionnaireOutline.propTypes = {
  questionnaire: React.PropTypes.object,
};
