import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Button } = Bootstrap;

export class QuestionnaireInstructions extends React.Component {

  constructor() {
    super();
    this.state = {
      isCriteriaVisible: false,
      isGuidanceVisible: false,
    };
    this.onCriteriaClicked = this.onCriteriaClicked.bind(this);
    this.onGuidanceClicked = this.onGuidanceClicked.bind(this);
  }

  onCriteriaClicked() {
    this.setState({ isCriteriaVisible: !this.state.isCriteriaVisible });
  }

  onGuidanceClicked() {
    this.setState({ isGuidanceVisible: !this.state.isGuidanceVisible });
  }

  render() {
    const criteriaButton = this.props.criteria ? (
      <Button bsStyle="link" onClick={this.onCriteriaClicked}>
        {_t('Criteria')}
        {' '}
        {
          this.state.isCriteriaVisible ? (
            <span className="icon fa fa-caret-up" />
          ) : (
            <span className="icon fa fa-caret-down" />
          )
        }
      </Button>
    ) : null;

    const separator = this.props.criteria && this.props.guidance ? (
      <div className="separator">|</div>
    ) : null;

    const criteriaText = `<strong>${_t('Criteria')}:</strong> ${this.props.criteria}`;

    const guidanceText = `<strong>${_t('Guidance')}:</strong> ${this.props.guidance}`;

    const guidanceButton = this.props.guidance ? (
      <Button bsStyle="link" onClick={this.onGuidanceClicked}>
        {_t('Guidance')}
        {' '}
        {
          this.state.isGuidanceVisible ? (
            <span className="icon fa fa-caret-up" />
          ) : (
            <span className="icon fa fa-caret-down" />
          )
        }
      </Button>
    ) : null;

    return this.props.criteria || this.props.guidance ? (
      <div className="instructions">
        <div className="buttons">
          {criteriaButton}
          {separator}
          {guidanceButton}
        </div>
        {
          this.props.criteria && this.state.isCriteriaVisible ? (
            <div className="text" dangerouslySetInnerHTML={{ __html: criteriaText }} />
          ) : null
        }
        {
          this.props.guidance && this.state.isGuidanceVisible ? (
            <div className="text" dangerouslySetInnerHTML={{ __html: guidanceText }} />
          ) : null
        }
      </div>
    ) : null;
  }

}

QuestionnaireInstructions.propTypes = {
  criteria: React.PropTypes.string,
  guidance: React.PropTypes.string,
};
