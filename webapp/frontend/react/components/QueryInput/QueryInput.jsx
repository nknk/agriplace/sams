import React from 'react';

var QueryInput = React.createClass({

  propTypes: {
    queryInputOptions: React.PropTypes.shape({
      query: React.PropTypes.string,
      label: React.PropTypes.string,
      placeholder: React.PropTypes.string,
      showSearchIcon: React.PropTypes.bool,
      showEraserIcon: React.PropTypes.bool,
      onInputChange: React.PropTypes.func,
    }).isRequired,
  },

  getInitialState() {
    return {
      value: this.props.queryInputOptions.query,
    };
  },

  renderSearchIcon() {
    if (this.props.queryInputOptions.showSearchIcon != false)
      return (
        <span className="fa fa-search"></span>
      );
  },

  renderEraseIcon() {
    if (this.props.queryInputOptions.showEraserIcon != false) {
      return (
        <Bootstrap.Button onClick={this.setValue}>
          <span className="fa fa-times"></span>
        </Bootstrap.Button>
      );
    }
  },

  renderLabel() {
    if (this.props.queryInputOptions.label)
      return (
        <div className="input-group-addon">
          <label>{ this.props.queryInputOptions.label }</label>
        </div>
      );
  },

  setValue(value) {
    // value can be either string or event
    if (tools.isObject(value)) {
      value.persist();
      value = value.target.value;
    }
    tools.forceFunction(this.props.queryInputOptions.onInputChange)(value);
    this.setState({ value: value });
  },

  render() {

    return (
      <div className="query-input" style={this.props.style}>
        <div className="form-group">
          <div className="input-group">

            { this.renderLabel() }

            <Bootstrap.Input
              ref = "queryInput"
              type = "text"
              value = { this.state.value }
              onChange = { this.setValue }
              placeholder = { this.props.queryInputOptions.placeholder || _t('Query') }
              addonBefore = { this.renderSearchIcon() }
              buttonAfter = { this.renderEraseIcon() }
            />

          </div>
        </div>
      </div>
    );
  },

});

module.exports = QueryInput;
