import React, { PropTypes } from 'react';
import permissionComponents from '../PermissionController/PermissionComponents.json';

export const ReviewValidationModal = React.createClass({
  propTypes: {
    assessmentUuid: PropTypes.string,
    isClicked: PropTypes.bool,
    onHide: PropTypes.func,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'ReviewStore'),
  ],

  getStateFromFlux() {
    const flux = this.getFlux();
    return {
      review: flux.store('ReviewStore').getState().review,
      errors: flux.store('ReviewStore').getState().errors,
      permissions: flux.store('AssessmentsStore').getPermissions(this.props.assessmentUuid),
    };
  },

  isValid() {
    return !this.state.errors.has_errors;
  },

  isRequired() {
    return this.state.permissions
      .find(p => p.name === permissionComponents.AuditDate)
      .actions.indexOf('UPDATE') > -1;
  },

  hasToShow() {
    return !this.isValid() && this.isRequired();
  },

  renderMessage() {
    return `${_t('\'Inspection start date/time\' and \'Inspection end date/time\' are required fields')}. ${
      _t('Please make sure that all required fields are filled out correctly')
    } (dd-mm-yyyy hh:mm) ${_t('before sharing')}.`;
  },

  render() {
    return (
      <Bootstrap.Modal show={this.props.isClicked && this.hasToShow()} backdrop="static" onHide={this.props.onHide}>
        <Bootstrap.Modal.Header>
            {_t('Share with internal auditor')}
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
            {this.renderMessage()}
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick={this.props.onHide} >
            {_t('OK')}
          </Bootstrap.Button >
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  },
});
