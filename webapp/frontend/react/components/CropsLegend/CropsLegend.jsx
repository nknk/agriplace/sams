var CropsLegend = React.createClass({

  propTypes: {
    crops: React.PropTypes.array.isRequired,
    onCropSelect: React.PropTypes.func,
  },

  handleCropClick(fieldId) {
    this.props.onCropSelect(fieldId);
  },

  render() {
    return (
      <Bootstrap.Table striped bordered condensed hover style={{ width: '100%', margin: '20px auto' }}>
        <thead>
          <tr>
            <th style={{ width: '8%' }}>{_t('Legend')}</th>
            <th style={{ width: '72%' }}>{_t('Crop')}</th>
            <th style={{ width: '20%', textAlign: 'right' }}>{_t('ha')}</th>
          </tr>
        </thead>
        <tbody>
          {this.props.crops.map((crop) => {
            return (
              <tr key={crop.field_id} style={{ cursor: 'pointer' }} onClick={this.handleCropClick.bind(null, crop.field_id)}>
                <td><div style={{ margin: '0 auto', width: 15, height: 15, backgroundColor: crop.color, borderRadius: '50%' }}></div></td>
                <td>{crop.name}</td>
                <td style={{ textAlign: 'right' }}>{Math.round(+crop.area * 10) / 10}</td>
              </tr>
            );
          })}
        </tbody>
      </Bootstrap.Table>
    );
  },
});

module.exports = CropsLegend;
