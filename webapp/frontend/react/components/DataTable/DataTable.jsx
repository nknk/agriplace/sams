

//
// DataTable.jsx uses FaceBook's FixedDataTable library for React style lists
//
// see for API: http://facebook.github.io/fixed-data-table/api-table.html
//

// override vendor css with some custom styles
require('fixed-data-table/dist/fixed-data-table.css');
require('./fixed-data-table.less');

const FixedDataTable = require('fixed-data-table');
const Table = FixedDataTable.Table;
const Column = FixedDataTable.Column;
const ColumnSettings = require('../DataTable/ColumnSettings.js');

// CONSTANTS:

const TABLE_WIDTH = 824;
const TABLE_HEIGHT = 800;
const TABLE_HEADER_HEIGHT = 45;
const TABLE_ROW_HEIGHT = 40;


export const DataTable = React.createClass({
  propTypes: {
    onRowClick: React.PropTypes.func,
    sortTable: React.PropTypes.func,
    dataTable: React.PropTypes.object.isRequired,
    columnSettings: React.PropTypes.array.isRequired,
    rowsCount: React.PropTypes.number.isRequired,
    onHeaderCheckBoxClick: React.PropTypes.func,
    headerCheckBoxChecked: React.PropTypes.bool,
  },

  statics: {
    TABLE_WIDTH,
    TABLE_HEIGHT,
    TABLE_HEADER_HEIGHT,
    TABLE_ROW_HEIGHT,
  },

  componentWillMount() {
    if (!this.props.columnSettings) {
      throw new Error('DataTable: cannot create table! missing columnSettings props..');
    }

    this.columnSettings = new ColumnSettings(this.props.columnSettings);
    this.updateColumn(this.props);
  },

  componentWillReceiveProps(props) {
    this.props = props;
    this.updateColumn(props);
  },


  //
  // Handlers
  //

  onRowClick(event, rowIndex) {
    tools.forceFunction(this.props.onRowClick)(rowIndex);
  },

  updateColumn(props) {
    this.columnSettings.setSelected(props.dataTable.selected);
    this.columnSettings.setSortOrder(props.dataTable.sortOrder);
  },


  sortToggle(column, event) {
    event.stopPropagation();
    tools.forceFunction(this.props.sortTable)(column, 'toggle');
  },


  sortNormal(column, event) {
    event.stopPropagation();
    tools.forceFunction(this.props.sortTable)(column, 'normal');
  },

  //
  // render all header columns
  //
  headerRenderer(column, label) {
    let icon = null;
    let style = {};
    const sortOrder = this.columnSettings.sortOrder;
    const selectedColumn = this.columnSettings.selected;

    // insert sort-icon and change the text color in the clicked/selected header cell
    if (column === selectedColumn) {
      icon = (sortOrder === 'normal')
        ? <i className="fa fa-arrow-down fa-1x"></i>
        : <i className="fa fa-arrow-up fa-1x"></i>;
      style = { color: 'rgb(108, 179, 63)' };
    } else {
      style = null;
    }

    return (
      <div>
        <div
          className="data-table-header-cell"
          style={style}
          onClick={this.sortNormal.bind(null, column)}
        >
          <div className="float-left">
            {label}
          </div>
          <div
            onClick={this.sortToggle.bind(null, column)}
            className="arrow-overlap-scroller float-right"
          >
            {icon}
          </div>
        </div>
      </div>
    );
  },

  checkBoxHeaderRenderer(column) {
    return (
      <div>
        <Bootstrap.Input
          style={{
            margin: '0 auto',
            height: '16px',
            width: '16px',
            cursor: 'pointer',
          }}
          type="checkbox"
          checked={this.props.headerCheckBoxChecked}
          onChange={(e) => this.props.onHeaderCheckBoxClick(column, e)}
        />
      </div>
    );
  },


  generateColumns() {
    const settings = this.columnSettings.settings;
    const result = [];

    _.forEach(settings, (column, index) => {
      result.push(
        <Column
          className="data-table-column"
          key={index}
          headerRenderer={
            column.isCheckBoxColumn ? (
              this.checkBoxHeaderRenderer.bind(null, column.dataKey)
            ) : (
              this.headerRenderer.bind(null, column.dataKey, column.label)
            )
          }
          flexGrow={1}
          {...column}
        />
      );
    });
    return result;
  },


  render() {
    return (
      <div className="data-table">

        <Table
          width={TABLE_WIDTH}
          height={TABLE_HEIGHT}
          headerHeight={TABLE_HEADER_HEIGHT}
          rowHeight={TABLE_ROW_HEIGHT}
          rowGetter={this.props.columnSettings.rowGetter}
          // can override default settings
          {...this.props}
        >

          {this.generateColumns()}

        </Table>

      </div>
    );
  },


});


module.exports = DataTable;
