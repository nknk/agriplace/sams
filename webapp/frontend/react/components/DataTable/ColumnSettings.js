const SORT_ORDER_OPTIONS = ['normal', 'inverse', 'toggle'];

export class ColumnSettings {
  constructor(settings) {
    if (settings && settings.length && settings[0].dataKey) {
      this.selected = settings[0].dataKey;
    } else {
      throw new Error('ColumnSettings.js: cannot instanciate with the given settings object, missing dataKey');
    }
    this.sortOrder = SORT_ORDER_OPTIONS[0];
    this.settings = settings;
  }

  setSelected(dataKey) {
    const index = _.findIndex(this.settings, { dataKey });
    if (index > -1) {
      this.selected = dataKey;
    } else {
      this.selected = this.settings[0].dataKey;
      log.info('ColumnSettings.js: cannot set selected to: ', dataKey);
    }
  }


  setSortOrder(sortOrder) {
    if (SORT_ORDER_OPTIONS.indexOf(sortOrder) > -1) {
      this.sortOrder = sortOrder;
    } else {
      this.sortOrder = SORT_ORDER_OPTIONS[0];
      log.info('ColumnSettings.js: cannot set sortOrder to: ', sortOrder);
    }
  }
}

module.exports = ColumnSettings;
