import React from 'react';
import cx from 'classnames';
const moment = require('moment');
const pageData = require('../Utils/pageData.js');
require('./DatePicker.less');

const DatePicker = React.createClass({

  propTypes: {
    options: React.PropTypes.object,
    onDateSelect: React.PropTypes.func,
    initValue: React.PropTypes.string,
    error: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    placeholder: React.PropTypes.string,
  },

  componentDidMount() {
    this.options = _.assign(this.options, this.props.options);
    const options = _.cloneDeep(this.options);
    options.format = options.format.toLowerCase();

    $(this.getDOMNode()).datepicker(options).on('changeDate', this.handleChange);

    const initValueMoment = moment(this.props.initValue, options.format.toUpperCase());

    if (initValueMoment.isValid()) {
      const initValue = initValueMoment.toDate();
      $(this.getDOMNode()).datepicker('setDate', initValue);
      $(this.getDOMNode()).datepicker('update');
    }
  },

  componentWillUnmount() {
    $(this.getDOMNode()).datepicker('destroy');
  },

  // defaults
  options: {
    autoclose: true,
    language: pageData.languageCode || 'en-US',
    format: 'DD-MM-YYYY',
  },

  handleInputChange(event) {
    if (!this.props.disabled) {
      this.props.onDateSelect(event.target.value);
    }
  },

  handleChange(event) {
    if (!this.props.disabled) {
      const formattedDateMoment = moment(event.date);
      const initDateMoment = moment(this.props.initValue, this.options.format.toUpperCase());


      if (!moment(formattedDateMoment).isSame(initDateMoment) && event.date) {
        this.props.onDateSelect(formattedDateMoment.format(this.options.format));
      } else if (!event.date) {
        this.props.onDateSelect(null);
      }
    }
  },

  render() {
    const classnames = cx('form-control DatePicker datepicker', {
      'has-error': this.props.error,
    });
    return (
      <input
        disabled={this.props.disabled}
        className={classnames}
        onChange={this.handleInputChange}
        type="text"
        placeholder={this.props.placeholder}
      />
    );
  },

});

module.exports = DatePicker;
