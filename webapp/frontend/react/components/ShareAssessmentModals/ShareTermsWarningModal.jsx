import React, { Component } from 'react';

export class ShareTermsWarningModal extends Component {
  render() {
    const { isVisible, onSuccess, onFail } = this.props;
    
    const fallDownBodyText = `${_t('The period that may elapse between sharing the self-assessment and the actual audit is limited, usually to a couple of weeks')}.
${_t('If you share the self-assessment too early, you run the risk that up-to-date information is missing')}. \
${_t('If you\'re unaware of the exact period, please contact your auditor')}.`;
    
    const bodyText = (typeof this.props.bodyText === 'undefined' || !this.props.bodyText)
      ? fallDownBodyText : this.props.bodyText;
    
    /* eslint-disable no-multi-str */
    return (
      <Bootstrap.Modal show={isVisible} backdrop="static" onHide={onFail}>
        <Bootstrap.Modal.Header>
          {_t('Sharing')}
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
          {bodyText}
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick={onSuccess}>
            {_t('Close assessment')}
          </Bootstrap.Button>
          <Bootstrap.Button onClick={onFail}>
            {_t('Back')}
          </Bootstrap.Button>
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

ShareTermsWarningModal.propTypes = {
  isVisible: React.PropTypes.bool,
  onSuccess: React.PropTypes.func,
  onFail: React.PropTypes.func,
  bodyText: React.PropTypes.string,
};
