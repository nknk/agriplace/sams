const Alert = require('../Utils/Alert.jsx');
import React, { Component } from 'react';

export class ShareWithoutAuditorsModal extends Component {
  render() {
    const { isVisible, onSuccess, onFail } = this.props;

    return (
      <Bootstrap.Modal show={isVisible} backdrop="static" onHide={onFail}>
        <Bootstrap.Modal.Header>
          {_t('Sharing')}
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
          <Alert
            alertText={_t('Are you sure you want to close the assessment without sharing?')}
          />
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick={onSuccess}>
            {_t('Close assessment')}
          </Bootstrap.Button>
          <Bootstrap.Button onClick={onFail}>
            {_t('Back')}
          </Bootstrap.Button>
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

ShareWithoutAuditorsModal.propTypes = {
  isVisible: React.PropTypes.bool,
  onSuccess: React.PropTypes.func,
  onFail: React.PropTypes.func,
};
