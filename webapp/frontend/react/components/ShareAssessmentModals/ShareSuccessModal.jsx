import React, { Component } from 'react';

export class ShareSuccessModal extends Component {
  render() {
    const { isVisible, onSuccess, isSharedWithAuditors, params } = this.props;
    return (
      <Bootstrap.Modal show={isVisible} backdrop="static">
        <Bootstrap.Modal.Header>
          {_t('Sharing')}
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
          {_t('This assessment has been finished now')}
          <br />

          {
            isSharedWithAuditors &&
              <p>{_t('You have shared your assessment with an audit organization, your auditor will contact you')}</p>
          }

          {`${_t('You can find your closed assessments under')}`}&nbsp;
          <a href={`/${params.membershipUuid}/${params.assessmentAccessMode}/assessments/list/closed`}>
            {_t('closed assessments')}
          </a>
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick={onSuccess}>
            {_t('Back to Agriplace home')}
          </Bootstrap.Button>
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

ShareSuccessModal.propTypes = {
  isVisible: React.PropTypes.bool,
  isSharedWithAuditors: React.PropTypes.bool,
  onSuccess: React.PropTypes.func,
  params: React.PropTypes.object,
};
