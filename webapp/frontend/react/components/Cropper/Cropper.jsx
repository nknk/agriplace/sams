

var React = require('react');

require('./Cropper.less');

var App = React.createClass({

  propTypes: {
    width: React.PropTypes.number,
    height: React.PropTypes.number,
    img: React.PropTypes.string.isRequired,
    zoom: React.PropTypes.number,
    x: React.PropTypes.number,
    y: React.PropTypes.number,
    onChange: React.PropTypes.func,
  },

  getInitialState() {
    return {
      pos: {
        x: this.props.x,
        y: this.props.y,
      },
      rel: {
        x: 0,
        y: 0,
      },
      originalSize: {
        width: null,
        height: null,
      },
      zoom: this.props.zoom,
      isDragging: false,
    };
  },

  componentWillMount() {
    var self = this;

    var img = new Image();
    img.src = this.props.img;

    img.onload = function () {
      self.setState({
        originalSize: {
          width: this.width,
          height: this.height,
        },
      });
    };
  },

  componentWillReceiveProps(nextProps) {
    if (nextProps.zoom) { this.setState({ zoom: nextProps.zoom }); }
    if (nextProps.x && nextProps.y) {
      this.setState({
        pos: {
          x:nextProps.x,
          y:nextProps.y,
        },
      });
    }
  },

  handleMove(e) {
    e.preventDefault();
    this.setState({
      pos: {
        y: e.clientY - this.state.rel.y,
        x: e.clientX - this.state.rel.x,
      },
    });
  },


  handleMouseUp() {
    this.setState({ isDragging: false });

    if (this.props.onChange) {
      this.props.onChange(this.state.pos.x, this.state.pos.y, this.state.zoom);
    }
  },

  handleMouseDown(e) {
    this.setState({
      isDragging: true,
      rel: {
        y: e.clientY - this.state.pos.y,
        x: e.clientX - this.state.pos.x,
      },
    });
  },

  handleZoomChange(e) {
    this.setState({ zoom: +e.target.value });
  },

  render() {
    var originalSize = this.state.originalSize;
    if (originalSize.height === null) return null;

    var cropBoxStyle = {
      height: this.props.height || 300,
      width: this.props.width || 400,
    };

    var imgStyle = {
      top: this.state.pos.y,
      left: this.state.pos.x,
      width: originalSize.width * this.state.zoom,
      height: originalSize.height * this.state.zoom,
    };

    return (
            <div className="Cropper">
                <div className="crop-box" style={cropBoxStyle}>
                    <img className="img"
                      src={this.props.img}
                      style={imgStyle}
                      onMouseDown={this.handleMouseDown}
                      onMouseUp={this.handleMouseUp}
                      onMouseMove={this.state.isDragging ? this.handleMove : null}
    >
                    </img>
                </div>
                <input type="range"
                  min="0.1"
                  max="2"
                  value={this.state.zoom}
                  step=".01"
                  onChange={this.handleZoomChange}
    />
                <div>{this.state.zoom}</div>
            </div>
        );
  },
});

module.exports = App;
