import React, { Component } from 'react';

export class UsedInList extends Component {
  render() {
    const { product } = this.props;
    return (
      <div>
        <span>{_t('You can not delete this crop') + '. ' + _t('You are using this crop in')}:&nbsp;</span>
        { !!product.used_in_assessments.length && (
          <div>
            {_t('Assessments')}: { product.used_in_assessments.map(a => a.name).join(', ')}
          </div>
        )}
        { !!product.used_in_certificates.length && (
          <div>
            {_t('Certificates')}: { product.used_in_certificates.map(c => c.certificate_number).join(', ')}
          </div>
        )}
      </div>
    );
  }
}

UsedInList.propTypes = {
  product: React.PropTypes.array,
};
