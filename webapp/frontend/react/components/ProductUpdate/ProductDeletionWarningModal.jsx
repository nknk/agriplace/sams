import { UsedInList } from './UsedInList.jsx';
import React, { Component } from 'react';

export default class ProductDeletionWarningModal extends Component {
  constructor() {
    super();
    this.state = { deleteClicked: false };
    this.deleteClicked = this.deleteClicked.bind(this);
    this.onOk = this.onOk.bind(this);
  }

  onOk() {
    this.setState({ deleteClicked: false });
  }

  deleteClicked() {
    this.setState({ deleteClicked: true });
  }

  isDeletionPossible(product = this.props.product) {
    return _.isEmpty(product.used_in_assessments) && _.isEmpty(product.used_in_certificates);
  }

  render() {
    const { product } = this.props;
    return (
      <Bootstrap.Modal
        bsSize="middle"
        show={this.state.deleteClicked && !this.isDeletionPossible(product)}
        backdrop="static"
      >

        <Bootstrap.Modal.Header>
          { _t('Warning') }
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
          <UsedInList product={product} />
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick={this.onOk}>
            {_t('OK')}
          </Bootstrap.Button>
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  }
}

ProductDeletionWarningModal.propTypes = {
  product: React.PropTypes.array,
};
