const ProductTypeSelectForm = require('../ProductTypeSelect/ProductTypeSelectForm.jsx');

import ProductDeletionWarningModal from './ProductDeletionWarningModal.jsx';

const ProductUpdate = React.createClass({

  propTypes: {
    onDeleteProduct: React.PropTypes.func,
    onCancel: React.PropTypes.func,
    onSubmit: React.PropTypes.func,
    product: React.PropTypes.object,
    traders: React.PropTypes.array,
    years: React.PropTypes.array,
  },

  getInitialState() {
    return {
      isClicked: false,
    };
  },


  handleDeleteClick() {
    this.refs.warning.deleteClicked();
    if (this.refs.warning.isDeletionPossible()) {
      this.props.onDeleteProduct(this.props.product.uuid);
    }
  },

  handleBackClick() {
    this.props.onCancel();
  },
  handleUpdateClick() {
    let product = this.refs.form.getValue();
    if (product) {
      product.uuid = this.props.product.uuid;
      product.name = this.props.product.name;
      product.product_type = this.props.product.product_type;

      this.props.onSubmit({ product });
    }
  },

  render() {
    const { product } = this.props;

    return (
      <div className="ProductUpdate">
        <ProductDeletionWarningModal ref="warning" product={product} isClicked={this.state.isClicked} />
        <h1 style={{ display: 'inline-block' }} >{product.name} <small>{_t('crop')}</small></h1>
        <i
          className="icon fa fa-trash-o fa-2x"
          style={{ marginLeft: 10, cursor: 'pointer' }}
          onClick={this.handleDeleteClick}
        />
        <ProductTypeSelectForm
          ref="form"
          value={ product }
          traders={ this.props.traders }
          years={ this.props.years }
        />
        <div className="btn-group pull-right">

          <Bootstrap.Button
            type= "button"
            bsStyle= "default"
            data-dismiss="modal"
            onClick= { this.handleBackClick }
          >
                    { _t('Back') }
          </Bootstrap.Button>

          <Bootstrap.Button
            type="button"
            bsStyle= "success"
            onClick= { this.handleUpdateClick }
          >
                    { _t('Update') }
          </Bootstrap.Button>

        </div>
      </div>
    );
  },

});

module.exports = ProductUpdate;
