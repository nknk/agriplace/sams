import React, { Component, PropTypes } from 'react';
import './Affix.less';

export class AffixWrapper extends Component {

  constructor() {
    super();
    this.state = {
      top: 0,
    };
    this.getTop = this.getTop.bind(this);
    this.getBottom = this.getBottom.bind(this);
  }

  componentDidMount() {
    const { triggerOffset } = this.props;

    this.scrollToTop();
    const node = React.findDOMNode(this.affixedDiv);
    const clientRects = node.getClientRects();
    this.state.top = (clientRects && clientRects[0] && clientRects[0].top) ? (clientRects[0].top - triggerOffset) : 0;

    const options = {
      top: this.getTop,
      bottom: this.getBottom,
    };

    $(node).affix({
      offset: options,
    });
  }

  scrollToTop() {
    document.body.scrollTop = 0; // For Chrome, Safari and Opera
    document.documentElement.scrollTop = 0; // For IE and Firefox
  }

  getTop() {
    return this.props.isEnabled ? this.state.top : (Math.max($(window).height(), document.body.scrollHeight) + 50);
  }

  getBottom() {
    return this.props.isEnabled ? 230 : 0;
  }

  render() {
    const { uniqueClass } = this.props;
    return (
      <div ref={(input) => { this.affixedDiv = input; }} className={uniqueClass}>
        {this.props.children}
      </div>
    );
  }
}

AffixWrapper.propTypes = {
  children: PropTypes.object,
  uniqueClass: PropTypes.string,
  triggerOffset: PropTypes.number,
  isEnabled: PropTypes.bool,
};
