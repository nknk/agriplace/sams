import React, { Component, PropTypes } from 'react';
import { ShareWithButton } from './ShareWithButton.jsx';
import { HzpcHeaderModals } from './HzpcHeaderModals.jsx';
import { PermissionController } from '../PermissionController/PermissionController.jsx';

export class HzpcHeaderButtons extends Component {
  constructor() {
    super();
    this.state = {
      clickedBtn: null,
    };
    this.onButtonClick = this.onButtonClick.bind(this);
    this.onHide = this.onHide.bind(this);
  }

  onButtonClick(rbacCodename) {
    this.setState({ clickedBtn: rbacCodename });
  }

  onHide() {
    this.setState({ clickedBtn: null });
  }

  render() {
    const { params } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;
    const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}`;
    const btns = [
      {
        title: _t('Share with internal inspector'),
        rbacCodename: 'ShareWithAuditor',
        warningText: `${_t('Are you sure you want to share this assessment with the internal inspector')}?`,
        successUrl: `${url}/share-with-auditor`,
      },
      {
        title: _t('Share with internal auditor'),
        rbacCodename: 'ShareWithCoordinator',
        warningText: `${_t('Are you sure you want to share this assessment with the internal auditor')}?`,
        successUrl: `${url}/share-with-coordinator`,
      },
      {
        title: _t('Document review done'),
        rbacCodename: 'DocumentReview',
        warningText: `${_t('Are you sure that you are done with the document review')}?`,
        successUrl: `${url}/doc-review-done`,
      },
      {
        title: _t('Confirm judgement'),
        rbacCodename: 'ConfirmJudgement',
        warningText: `${_t('Are you sure that you want to confirm this judgement')}?`,
        successUrl: `${url}/confirm-judgement`,
      },
    ];

    return (
      <div>
        {
          btns.map((btn, index) => (
            <PermissionController
              key={`hzpc-header-button-${index}`}
              assessmentUuid={this.props.params.assessmentUuid}
              component={btn.rbacCodename}
            >
              <ShareWithButton
                onClick={this.onButtonClick.bind(this, btn.rbacCodename)}
                title={btn.title}
              />
            </PermissionController>
          ))
        }
        &nbsp;&nbsp;
        <a href="/">
          <button type="button" className="btn btn-default">
            <span className="fa fa-floppy-o" />&nbsp;
            {_t('Proceed later')}
          </button>
        </a>
        {
          this.state.clickedBtn &&
            <HzpcHeaderModals
              clickedBtn={btns.find(b => b.rbacCodename === this.state.clickedBtn)}
              onHide={this.onHide}
              assessmentUuid={this.props.params.assessmentUuid}
              params={this.props.params}
            />
        }
      </div>
    );
  }
}

HzpcHeaderButtons.propTypes = {
  params: PropTypes.object,
};
