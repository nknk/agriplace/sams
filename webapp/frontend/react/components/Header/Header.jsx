import { Title } from './Title.jsx';
import { AgriplaceHeaderButtons } from './AgriplaceHeaderButtons.jsx';
import { HzpcHeaderButtons } from './HzpcHeaderButtons.jsx';
import pageData from '../Utils/pageData.js';
import './Header.less';
export const Header = React.createClass({

  propTypes: {
    params: React.PropTypes.object.isRequired,
    name: React.PropTypes.string.isRequired,
    state: React.PropTypes.string.isRequired,
    orgType: React.PropTypes.string,
    showButtons: React.PropTypes.bool,
  },

  render() {
    const { name, params, state, orgType, showButtons } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = params;
    const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/detail`;
    return (
      <div className="page-header hidden-print">
        <table>
          <tr>
            <td className="title">
              <Title
                url={url}
                secondary={_t('assessment')}
                title={name}
                icon={'page-icon hidden-phone icon-assessment fa icon fa-bar-chart-o'}
              />
            </td>
            {showButtons &&
              <td className="buttons">
                {pageData.context.layout === 'agriplace' ? (
                  <AgriplaceHeaderButtons
                    state={state}
                    type={orgType}
                  />
                ) : (
                  <HzpcHeaderButtons params={params} />
                )}
              </td>
            }
          </tr>
        </table>
      </div>
    );
  },
});
