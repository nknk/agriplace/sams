import React, { Component, PropTypes } from 'react';
require('./ShareWithButton.less');

export class ShareWithButton extends Component {
  render() {
    return (
      <button type="button" className="btn btn-default ShareWithButton" onClick={this.props.onClick}>
        <span className="fa fa-share-alt"></span>&nbsp;
          {this.props.title}
      </button>
    );
  }
}

ShareWithButton.propTypes = {
  title: PropTypes.string,
  onClick: PropTypes.func,
};
