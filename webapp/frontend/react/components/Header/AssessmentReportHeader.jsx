import React, { Component, PropTypes } from 'react';
import './Header.less';

export class AssessmentReportHeader extends Component {

  render() {
    return (
      <h1 className="page-header header">
        {_t('Assessment report')}
        {': '}
        <span className="assessment-name">
          {this.props.title}
        </span>
      </h1>
    );
  }
}

AssessmentReportHeader.propTypes = {
  title: PropTypes.string.isRequired,
};
