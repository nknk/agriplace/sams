import React, { Component } from 'react';

export class Title extends Component {
  render() {
    const { url, title, secondary, icon } = this.props;
    const titleNode = (
      <div>
        <span className={icon}></span>
        &nbsp;{title}
        &nbsp;<small>{secondary}</small>
      </div>
    );

    return (
      <h1 className="page-title">
        {url
          ? <a href={url}>{titleNode}</a>
          : titleNode
        }
      </h1>
    );
  }
}

Title.propTypes = {
  url: React.PropTypes.string,
  title: React.PropTypes.string,
  secondary: React.PropTypes.string,
  icon: React.PropTypes.string,
};
