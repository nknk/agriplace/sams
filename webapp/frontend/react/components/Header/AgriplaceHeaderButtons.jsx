import React, { Component } from 'react';

export class AgriplaceHeaderButtons extends Component {
  render() {
    return (
      <div className="btn-group pull-right">
        {
          this.props.type !== 'auditor' && this.props.state !== 'closed'
          ? <a href="/">
            <button type="button" className="btn btn-default">
              <span className="fa fa-floppy-o" />&nbsp;
              {_t('Proceed later')}
            </button>
          </a>
          : null
        }
      </div>
    );
  }
}

AgriplaceHeaderButtons.propTypes = {
  type: React.PropTypes.string.isRequired,
  state: React.PropTypes.string.isRequired,
};
