import React, { Component } from 'react';
import { ShareWithModal } from '../ShareWithModal/ShareWithModal.jsx';
import { AssessmentCompletnessModal } from '../AssessmentCompletnessModal/AssessmentCompletnessModal.jsx';
import { ReviewValidationModal } from '../ReviewValidationModal/ReviewValidationModal.jsx';

export class HzpcHeaderModals extends Component {
  render() {
    const { clickedBtn, assessmentUuid, params, onHide } = this.props;
    return (
      <div>
        <AssessmentCompletnessModal
          assessmentUuid={assessmentUuid}
          isClicked={clickedBtn.rbacCodename === 'ShareWithAuditor'}
          params={params}
          onHide={onHide}
        />
        <ReviewValidationModal
          onHide={onHide}
          isClicked={clickedBtn.rbacCodename === 'ShareWithCoordinator'}
          assessmentUuid={assessmentUuid}
        />
        <ShareWithModal
          isVisible
          heading={clickedBtn.title}
          body={clickedBtn.warningText}
          onSuccessUrl={clickedBtn.successUrl}
          onHide={onHide}
        />
      </div>
    );
  }
}

HzpcHeaderModals.propTypes = {
  clickedBtn: React.PropTypes.object,
  assessmentUuid: React.PropTypes.string,
  onHide: React.PropTypes.func,
  params: React.PropTypes.object,
};
