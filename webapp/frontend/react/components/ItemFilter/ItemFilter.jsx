import React from 'react';

require('./ItemFilter.less');

export const ItemFilter = React.createClass({

  propTypes: {
    items: React.PropTypes.shape({
      label: React.PropTypes.string,
      count: React.PropTypes.number,
      code: React.PropTypes.string,
    }),
    type: React.PropTypes.string,
    onChange: React.PropTypes.func,
  },

  getInitialState() {
    return {
      selected: [],
      deselected: [],
    };
  },

  componentWillMount() {
    this.setState({
      deselected: this.props.items,
    });
  },

  deselect(item) {
    let { selected, deselected } = this.state;
    const newSelected = selected.filter(currentItem => currentItem.label !== item.label);
    deselected.push(item);
    deselected = _.sortBy(deselected, (o) => _.findIndex(this.props.items, { code: o.code }));

    this.setState({
      selected: newSelected,
      deselected,
    });
    this.props.onChange({ type: this.props.type, value: newSelected.map(selectedItem => selectedItem.code) });
  },

  select(item) {
    const { selected, deselected } = this.state;
    const newDeselected = deselected.filter(currentItem => currentItem.code !== item.code);
    selected.push(item);

    this.setState({
      selected,
      deselected: newDeselected,
    });
    this.props.onChange({ type: this.props.type, value: selected.map(selectedItem => selectedItem.code) });
  },

  getSelected(selected) {
    return (
      <ul>
        {
          selected.map(item => {
            // Suppose counter was updated;
            return (
              <li key={item.code} onClick={this.deselect.bind(this, item)}>
                <div className="circle selected"></div>{item.label}
              </li>
            );
          })
        }
      </ul>
    );
  },

  getDeselected(deselected) {
    const isSmthSelectedPrefix = this.state.selected.length > 0 ? '+' : '';
    return (
      <ul>
        {
          deselected.map(item => {
            const count = this.props.items.find(currentItem => currentItem.code === item.code).count;
            const countLabel = count > 0 ? `(${isSmthSelectedPrefix}${count})` : '';
            return (
              <li key={item.code} onClick={this.select.bind(this, item)}>
                <div className="circle deselected"></div>
                <span>{item.label} </span>
                <span className="counter">{countLabel}</span>
              </li>
            );
          })
        }
      </ul>
    );
  },

  render() {
    const { selected, deselected } = this.state;
    return (
      <div className="ItemFilter">
        {this.getSelected(selected)}
        {this.getDeselected(deselected)}
      </div>
    );
  },
});
