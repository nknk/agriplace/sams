import React from 'react';
const Bootstrap = require('react-bootstrap');

export class CertificateDeleteButton extends React.Component {

  render() {
    return (
      <Bootstrap.Button
        bsStyle="danger"
        onClick={this.props.onClick}
      >
      {_t('Delete Certificate')}
      </Bootstrap.Button>
    );
  }

}

CertificateDeleteButton.propTypes = {
  onClick: React.PropTypes.function,
};

