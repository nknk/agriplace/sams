import React from 'react';
require('./Certifications.less');
const Bootstrap = require('react-bootstrap');
const pageData = require('../Utils/pageData.js');
const classnames = require('classnames');
import { Link } from 'react-router';

const CertificatesList = React.createClass({

  propTypes: {
    certificateList: React.PropTypes.array.isRequired,
    params: React.PropTypes.object.isRequired,
    productTypes: React.PropTypes.array.isRequired,
    onCreateOrOpenCertificate: React.PropTypes.func.isRequired,
  },

  getCropsLabel(certificate) {
    const label = certificate.crops && certificate.crops.length ? (
      certificate.crops.map((crop) => (
          this.props.productTypes.find(pt => pt.uuid === crop.product_type).name
        ) + (
        crop.total_surface ? (
          ` (${crop.total_surface} ha)`
        ) : ''
      )).join(', ')
    ) : '--';

    return (
      <span title={label} >
        {label}
      </span>
    );
  },

  render() {
    const certificateNameClass = classnames({
      'certificate-name-label': true,
      'clickable-hover': pageData.context.layout !== 'hzpc',
      'clickable-hover-hzpc': pageData.context.layout === 'hzpc',
    });

    const tableParams = pageData.context.layout !== 'hzpc' ? {
      striped: true,
      hover: true,
    } : {
      striped: true,
      hover: true,
      bordered: true,
    };

    const { membershipUuid, assessmentAccessMode } = this.props.params;

    return (
      <div className="certification-box">
        <div>
          <p>{_t('Here you can add your already obtained certificates')}.</p>
          <p>{
            _t('If you want to perform an assessment for a new certification')
          }, <a href="assessments/list">{
            _t('go to Certification')
          }</a>.</p>
        </div>
        <div className="toolbar certification-bar" role="toolbar">
          <div className="pull-left">
            <Link
              to={`/${membershipUuid}/${assessmentAccessMode}/certifications/product/new`}
              onClick={this.props.onCreateOrOpenCertificate}
            >
              <Bootstrap.Button type="button" bsStyle="default">
                <i className="icon fa fa-plus space" aria-hidden="true" />
                {_t('Add certificate')}
              </Bootstrap.Button>
            </Link>
          </div>
        </div>
        <div>
          <Bootstrap.Table {...tableParams} className="table-styling">
            <thead>
              <tr>
                <th><strong>{_t('Certificate')}</strong></th>
                <th><strong>{_t('Crops')}</strong></th>
                <th><strong>{_t('Issue Date')}</strong></th>
                <th><strong>{_t('Expiry Date')}</strong></th>
              </tr>
            </thead>
            <tbody>
              {
                this.props.certificateList.map(certificate => (
                  <tr>
                    <td className="first-col">
                      <Link
                        to={`/${membershipUuid}/${assessmentAccessMode}/certifications/${certificate.uuid}/edit`}
                        onClick={this.props.onCreateOrOpenCertificate}
                      >
                        <div className={certificateNameClass}>
                          {
                            pageData.context.layout !== 'hzpc' ? (
                              <i className="icon fa fa-certificate space" aria-hidden="true" />
                            ) : null
                          }
                          {certificate.certificate_type.name}
                        </div>
                      </Link>
                    </td>
                    <td className="crops-col">
                      <div className="truncate-ellipsis">
                        {
                          this.getCropsLabel(certificate)
                        }
                      </div>
                    </td>
                    <td className="date-col">{
                      certificate.issue_date ? (
                        tools.dateFilter(certificate.issue_date, pageData.dateFormat, pageData.languageCode)
                      ) : '--'
                    }
                    </td>
                    <td className="date-col">{
                      certificate.expiry_date ? (
                        tools.dateFilter(certificate.expiry_date, pageData.dateFormat, pageData.languageCode)
                      ) : '--'
                    }
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </Bootstrap.Table>
        </div>
      </div>
    );
  },
});

module.exports = CertificatesList;
