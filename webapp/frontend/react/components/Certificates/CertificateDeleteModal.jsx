import React from 'react';
const Modal = require('../Utils/Modal.jsx');

export class CertificateDeleteModal extends React.Component {
  constructor() {
    super();
    const isVisible = false;
    this.state = {
      isVisible,
    };
    this.onCancel = this.onCancel.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
  }

  onSuccess() {
    this.props.onDelete(this.props.certificateUuid);
  }

  onCancel() {
    this.props.onCancel();
  }

  changeVisbility(isVisible) {
    this.setState({ isVisible });
  }

  render() {
    if (!this.state.isVisible) {
      return null;
    }

    const header = (
      <div className="modal-title">
        <span>
          {_t('Delete')}
          {' '}
          {this.props.certificateName}
        </span>
      </div>
    );

    const footer = (
      <div className="btn-toolbar" role="toolbar">
        <div className="pull-right">

          <Bootstrap.Button
            bsStyle="danger"
            onClick={this.onSuccess}
          >
            {_t('Yes')}
          </Bootstrap.Button>

          <Bootstrap.Button
            bsStyle="default"
            onClick={this.onCancel}
          >
            {_t('Cancel')}
          </Bootstrap.Button>

        </div>
      </div>
    );

    const body = (
      <div>
        <span>
          {_t('Are you sure you want to delete this certificate')}
          {' '}
          <strong>{this.props.certificateName}</strong>
          {' ?'}
        </span>
      </div>
    );

    return (
      <Modal
        header={header}
        footer={footer}
        body={body}
        backdrop="static"
        onCancel={this.onCancel}
      />
    );
  }

}

CertificateDeleteModal.propTypes = {
  certificateName: React.PropTypes.string.isRequired,
  certificateUuid: React.PropTypes.string.isRequired,
  onDelete: React.PropTypes.func.isRequired,
  onCancel: React.PropTypes.func.isRequired,
};

