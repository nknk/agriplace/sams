import { Component, PropTypes } from 'react';
import './ConfirmAll.less';
import { Button } from 'react-bootstrap';

export default class ConfirmAllButton extends Component {

  render() {
    const { isDisabled } = this.props;

    return (
      <Button
        role="button"
        bsStyle="default"
        disabled={isDisabled}
        onClick={() => this.props.onClick()}
      >
        {_t('Confirm all')}
      </Button>
    );
  }
}

ConfirmAllButton.propTypes = {
  onClick: PropTypes.func,
  isDisabled: PropTypes.bool,
};
