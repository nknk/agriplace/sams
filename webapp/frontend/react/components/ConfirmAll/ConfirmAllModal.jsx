import { Component, PropTypes } from 'react';
import './ConfirmAll.less';
import { Button, ButtonToolbar, Modal } from 'react-bootstrap';

export default class ConfirmAllModal extends Component {

  render() {
    const { onCancel, onConfirm, isOpen } = this.props;
    return (
      <Modal backdrop={'static'} show={isOpen} onRequestHide={() => onCancel()}>
        <Modal.Header className="modal-header-top">
          <h3 className="confirm-all-header-title">
            {_t('Confirm all audit judgements')}
          </h3>
          <div className="confirm-all-close-button">
            <button type="button" className="close" onClick={() => onCancel()}>
              <i className="fa fa-times" />
            </button>
          </div>
        </Modal.Header>
        <Modal.Body>
          {_t('Are you sure you want to provide all unanswered audit judgements with a ‘yes’?')}
        </Modal.Body>
        <Modal.Footer>
          <ButtonToolbar>
            <Button bsStyle="default" onClick={() => onCancel()}>
              {_t('Cancel')}
            </Button>
            <Button bsStyle="success" onClick={() => onConfirm()}>
              {_t('OK')}
            </Button>
          </ButtonToolbar>
        </Modal.Footer>
      </Modal>
    );
  }
}

ConfirmAllModal.propTypes = {
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func,
  isOpen: PropTypes.bool,
};
