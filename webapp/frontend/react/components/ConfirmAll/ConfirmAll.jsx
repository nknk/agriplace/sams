import { Component, PropTypes } from 'react';
import ConfirmAllButton from './ConfirmAllButton.jsx';
import ConfirmAllModal from './ConfirmAllModal.jsx';
import ProgressModal from '../Utils/ProgressModal.jsx';
import { ButtonGroup } from 'react-bootstrap';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import pageData from '../Utils/pageData.js';
const layout = pageData.context.layout;
import './ConfirmAll.less';

export default class ConfirmAll extends Component {

  constructor() {
    super();
    this.onOpenConfirmationModal = this.onOpenConfirmationModal.bind(this);
    this.onCloseConfirmationModal = this.onCloseConfirmationModal.bind(this);
    this.onConfirmAllUnansweredJudgements = this.onConfirmAllUnansweredJudgements.bind(this);
  }

  onOpenConfirmationModal() {
    const { judgementActions } = this.props;
    judgementActions.openConfirmAllModal();
  }

  onCloseConfirmationModal() {
    const { judgementActions } = this.props;
    judgementActions.closeConfirmAllModal();
  }

  onConfirmAllUnansweredJudgements() {
    const { judgementActions, params } = this.props;
    const { assessmentUuid, questionnaireUuid } = params;
    judgementActions.confirmUnansweredJudgements({ assessmentUuid, questionnaireUuid });
  }

  render() {
    const { disabled, readOnly, params, isConfirmAllModalOpen, isConfirmAllInProgress } = this.props;
    const isDisabled = disabled || readOnly;
    const isVisible = params.questionnaireUuid && (layout !== 'agriplace');

    return (
      isVisible ? (
        <PermissionController
          assessmentUuid={params.assessmentUuid}
          component={'ConfirmAll'}
        >
          <ButtonGroup id="confirm-all-widget">
            <ConfirmAllButton onClick={this.onOpenConfirmationModal} isDisabled={isDisabled} />
            <ConfirmAllModal
              isOpen={isConfirmAllModalOpen}
              onCancel={this.onCloseConfirmationModal}
              onConfirm={this.onConfirmAllUnansweredJudgements}
            />
            <ProgressModal isOpen={isConfirmAllInProgress} />
          </ButtonGroup>
        </PermissionController>
      ) : null
    );
  }
}

ConfirmAll.propTypes = {
  params: PropTypes.object,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  isConfirmAllModalOpen: PropTypes.bool,
  isConfirmAllInProgress: PropTypes.bool,
  judgementActions: PropTypes.object,
};
