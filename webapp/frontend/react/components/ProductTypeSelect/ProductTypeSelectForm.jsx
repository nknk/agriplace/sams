import React, { Component, PropTypes } from 'react';
const t = require('tcomb');
const tForm = require('tcomb-form');
const moment = require('moment');
const Form = tForm.form.Form;
const utils = require('../FormBuilder/formBuilderUtils.js');
const isPartnersAdd = require('../Utils/pageData.js').context.isPartnersAdd;
import SelectFactory from '../../factories/SelectFactory.jsx';

const i18n = {
  add: _t('Add'),
  down: _t('Down'),
  optional: _t('Optional'),
  required: _t('Required'),
  remove: _t('Remove'),
  up: _t('Up'),
};

require('./ProductTypeSelectForm.less');

const layout = (locals) => {
  var inputs = locals.inputs;
  return (
    <div>
      <div className="row">
        <div className="col-xs-3">{inputs.harvest_year}</div>
      </div>
      <div className="row">
        <div className="col-xs-3">{inputs.area}</div>
      </div>
      <div className="row">
        <div className="col-xs-12">{inputs.location}</div>
      </div>
      {
        isPartnersAdd
        ? (
          <div className="row traders">
            <div className="col-xs-12">
              <label>{_t('Buyers (optional)')}</label>
              {inputs.partners}
            </div>
          </div>
        )
        : null
      }
      <div className="row">
        <div className="col-xs-12"> {inputs.comment} </div>
      </div>
    </div>
  );
};

class ProductTypeSelectForm extends Component {
  componentWillMount() {
    const { value = {} } = this.props;
    value.harvest_year = value.harvest_year || +moment().format('YYYY');
    this.setState({ value });
  }

  getPartners(traders = []) {

    let tradersObj = {};

    traders.forEach(tr => {
      tradersObj[tr.uuid] = tr.name;
    });

    return t.struct({
      organization_uuid: t.enums(tradersObj),
      contract_number: t.maybe(t.String),
    });
  }

  getYears(years = []) {
    let yearsObj = {};

    years.forEach(year => {
      yearsObj[year] = year;
    });

    return yearsObj;
  }

  getProduct({ Traders, years }) {
    return t.struct({
      harvest_year: t.enums(years),
      area: t.Number,
      partners: t.maybe(t.list(Traders)),
      comment: t.maybe(t.String),
      location: t.maybe(t.String),
    }, 'Product');
  }

  getPartnersLayout() {
    return (locals) => {
      var inputs = locals.inputs;

      return (
        <div>
          <div className="row">
            <div className="col-xs-6">{inputs.organization_uuid}</div>
            <div className="col-xs-6">{inputs.contract_number}</div>
          </div>
        </div>
      );
    };
  }

  getValue() {
    const value = this.refs.form.getValue();
    if (value) {
      return _.cloneDeep(utils.format(value));
    }
  }

  handleChange(value) {
    this.setState({ value });
  }

  render() {
    let Traders = this.getPartners(this.props.traders);
    let years = this.getYears(this.props.years);
    return (
      <div className="ProductTypeSelectForm">
        <Form
          ref="form"
          type={this.getProduct({ Traders, years })}
          value={ this.state.value }
          onChange= {this.handleChange.bind(this)}
          options={{
            i18n,
            fields: {
              harvest_year: {
                nullOption: false,
                label: _t('Harvest year'),
              },
              area: {
                label: `${_t('Total surface')}*`,
                attrs: {
                  placeholder: 'Ha',
                },
                error: (value, path, context) => {
                  if (value === null) {
                    return _t('This field is required');
                  } else if (!t.Number.is(value)) {
                    return _t('Only numbers and points (for decimals) are allowed here');
                  }
                },
              },
              location: {
                attrs: {
                  placeholder: _t('Address'),
                },
                label: _t('Location(s) storage/processing if other than own farm (optional)'),
              },
              comment: {
                label: _t('Remarks regarding crop (optional)'),
                type: 'textarea',
              },
              partners: {
                disableOrder: true,
                legend: false,
                item: {
                  template: this.getPartnersLayout(),
                  fields: {
                    organization_uuid: {
                      factory: SelectFactory,
                      label: _t('Buyer'),
                    },
                    contract_number: {
                      label: _t('Relation number'),
                    },
                  },
                },
              },
            },
            template: layout,
          }}
        />
      </div>
    );
  }
}

ProductTypeSelectForm.propTypes = {
  traders: React.PropTypes.array,
  years: React.PropTypes.array,
};

module.exports = ProductTypeSelectForm;
