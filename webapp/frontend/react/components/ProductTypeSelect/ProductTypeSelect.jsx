

require('./ProductTypeSelect.less');

var ListView = require('../ListView/ListView.jsx');
var QueryInput = require('../QueryInput/QueryInput.jsx');
var ProductTypeSelectForm = require('./ProductTypeSelectForm.jsx');
const utils = require('../FormBuilder/formBuilderUtils.js');
import Fuse from 'fuse.js';
module.exports = React.createClass({

  PropTypes: {

    options: React.PropTypes.shape({
      productTypes: React.PropTypes.array.isRequired,
      textareaPlaceholder: React.PropTypes.string,
      sortKeys: React.PropTypes.array,
    }).isRequired,

    listView: React.PropTypes.object,
    queryInput: React.PropTypes.object,
  },


  mixins: [Bootstrap.OverlayMixin],


  getInitialState() {
    this.props.listViewOptions.onItemClicked = this.onItemClicked;
    this.props.queryInputOptions.onInputChange = this.onQueryInputChange;

    return {
      products: this.props.options.productTypes,
      selected: {},
      query: '',
      showModal: true,
      noSelectionNotification: false,
    };
  },

  componentWillMount() {
    this.debounced = _.debounce(this.debounced, 300);
  },

  filterProductList(categoryUuid) {
    if (categoryUuid === 'allProductTypes') {
      return this.props.options.productTypes;
    }

    var products = [];
    _.forEach(this.props.options.productTypes, (productType) => {

      _.forEach(productType.product_type_groups, (groupUuid) => {
        if (categoryUuid === groupUuid)
          return products.push(productType);
      });

    });
    return products;
  },


  productTypesSelect(event) {
    var categoryUuid = event.target.value;
    this.setState({
      products: this.filterProductList(categoryUuid),
      query: '',
    });
  },

  onQueryInputChange(value) {
    this.debounced(value);
  },

  debounced(value) {
    this.setState({ query: value });
  },

  formatBeforeSend(productType) {
    let product = this.refs.extraInfoForm.getValue();
    if (product) {
      product.name = productType.name;
      product.product_type = productType.uuid;

      return product;
    }
  },

  onModalResult(event) {
    var selected;

    // prevent adding without selection by showing a notification
    if (event && event.target.id === 'product-select-add-button') {
      if (_.isEmpty(this.state.selected)) {
        this.setState({ noSelectionNotification: true });
        return null;
      } else if (!this.refs.extraInfoForm.getValue()) {
        return null;
      }
      selected = this.formatBeforeSend(this.state.selected);
    }
    tools.forceFunction(this.props.options.onModalResult)(selected);
    this.setState({ showModal: false });
  },


  onItemClicked(item) {
    this.setState({
      selected: item,
      noSelectionNotification: false,
    });
  },


//
//                                      RENDER METHODS
//

  renderCloseIcon() {
    return (<Bootstrap.Button
      type = "button"
      className = "close pull-right"
      onClick = { this.onModalResult }
    >
      <span aria-hidden="true">&times;</span>
    </Bootstrap.Button>);
  },

  renderQueryInput() {
    return (<QueryInput
      queryInputOptions={{
        showEraserIcon: false,
        onInputChange: this.onQueryInputChange,
        placeholder: this.props.queryInputOptions.placeholder,
      }}
    />);
  },


  renderListFilters() {
    return (<form role="form" className="form-horizontal" id="product-type-select-filters">
      { this.renderQueryInput() }
    </form>);
  },


  renderListView() {
    var keys = this.props.listViewOptions.filterKeys || tools.allObjectKeys(this.props.listViewOptions.items);
    // extract the full list of items from the original props.listView and use for the filter
    var listViewOther = _.clone(this.props.listViewOptions);
    // var items = tools.objectFilter(this.state.products, this.state.query, keys);
    const fuse = new Fuse(this.state.products, { keys });
    let items = this.state.query !== ''
      ? fuse.search(this.state.query)
      : tools.sortby(this.state.products, this.props.options.sortKeys);

    var listViewProps = _.extend(listViewOther, {
      items,
      selected: [this.state.selected] || [],
    });

    return <ListView listViewOptions={ listViewProps } />;
  },


  renderModalButtons() {
    return (<div className="btn-toolbar" role="toolbar">
      <div className="btn-group pull-right">

        <Bootstrap.Button
          type= "button"
          id="product-select-back-button"
          bsStyle= "default"
          data-dismiss="modal"
          onClick= { this.onModalResult }
        >
                  { _t('Back') }
        </Bootstrap.Button>

        <Bootstrap.Button
          type="button"
          id="product-select-add-button"
          bsStyle= "success"
          onClick= { this.onModalResult }
        >
                  { _t('Add') }
        </Bootstrap.Button>

      </div>
    </div>);
  },

  // maybe make this a generic notification component
  renderNoSelectionAlert() {
    return (<div>
      <br />
      <Bootstrap.Alert bsStyle="danger" style={{ marginBottom: '-40px' }}>
        <span className="fa fa-exclamation-triangle pull-left fa-2x"> </span>
        <strong className="selected-product-type-text pull-left">
          <h4>{ _t('Please select a crop to add first') }</h4>
        </strong>
        <div style={{ height: '20px' }} />
      </Bootstrap.Alert>
    </div>);
  },


  renderSelectedProductType() {
    if (this.state.noSelectionNotification)
      return this.renderNoSelectionAlert();

    return (<h3 id="selected-product-type" className="pull-left">
      <span id="selected-product-type-text">{ _t('Your selection') + ':' }</span>
      <span id="selected-product-type-product"> { this.state.selected.name }</span>
    </h3>);
  },


  render() {

    return (
      <Bootstrap.Modal
        backdrop="static"
        show={this.state.showModal}
        dialogClassName="custom-modal ProductTypeSelect"
        onHide={ this.onModalResult }
      >

      <Bootstrap.Modal.Body>
        <div className="row">
          <div className="col-xs-6">
            <h3 className="section-header">{`1. ${_t('Select one crop')}`}</h3>
            { this.renderListFilters() }
            <br />
            { this.renderListView() }
            <br />
            { this.renderSelectedProductType() }
          </div>
          <div className="col-xs-6">
            <h3 className="section-header">{`2. ${_t('Features')} & ${_t('buyers')}`}</h3>
            <ProductTypeSelectForm
              ref="extraInfoForm"
              traders={ this.props.traders }
              years={ this.props.years }
            />
          </div>
        </div>
      </Bootstrap.Modal.Body>


      <Bootstrap.Modal.Footer>
                    { this.renderModalButtons() }
      </Bootstrap.Modal.Footer>

    </Bootstrap.Modal>
    );
  },
});
