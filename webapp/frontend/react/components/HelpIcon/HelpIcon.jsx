import React from 'react';
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;
import helpIconsDict from './helpIconsDict.json';
require('./HelpIcon.less');

const HelpIcon = React.createClass({
  propTypes: {
    assessmentUuid: React.PropTypes.string,
    helpLink: React.PropTypes.string,
  },
  mixins: [
    FluxMixin,
    StoreWatchMixin('HelpLinksStore'),
  ],

  componentDidMount() {
    const flux = this.getFlux();
    flux.actions.helpLinks.loadHelpLinks(this.props.assessmentUuid);
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const helpLinksStore = flux.store('HelpLinksStore');
    const helpLinksStoreState = helpLinksStore.getState();
    const helpLinks = helpLinksStoreState.helpLinks;
    return {
      helpLinks,
    };
  },

  render() {
    const helpLinkTitle = helpIconsDict[this.props.helpLink];
    const help = _.find(this.state.helpLinks, (helpLink) => helpLink.title === helpLinkTitle);
    return help && !_.isEmpty(this.state.helpLinks)
    ? (
      <div
        className="HelpIcon"
        style={{ display: 'inline-block', paddingLeft: '0.2em' }}
      >
        <a href={help.url} target="_blank">
          <span className="fa fa-info-circle"></span>
        </a>
      </div>
    )
    : null;
  },

});

module.exports = HelpIcon;
