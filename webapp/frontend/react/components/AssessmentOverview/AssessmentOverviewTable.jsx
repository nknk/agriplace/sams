import React, { Component, PropTypes } from 'react';
import { ReadMore } from '../ReadMore/ReadMore.jsx';
import pageData from '../Utils/pageData.js';
const HelpIcon = require('../HelpIcon/HelpIcon.jsx');

class AssessmentOverviewTable extends Component {

  render() {
    const { organizationName, assessmentStateDescription, auditor, createdTime, assessmentType,
    assessmentTypeDescription, assessmentState, products, createdBy,
    prefferedMonth, auditDate, auditTime, documentReviewDate, dateShared } = this.props.options;

    const { layout } = pageData.context;
    const isExtraFields = layout === 'hzpc' || layout === 'farmGroup';
    return (
      <div className="AssessmentOverview">
        <table className="table table-striped">
          <tr>
            <td>{_t('Organization')}:</td>
            <td>{organizationName}</td>
          </tr>
          <tr>
            <td>{_t('Assessment type')}:</td>
            <td>{assessmentType}<ReadMore text={assessmentTypeDescription} /></td>
          </tr>
          <tr>
            <td> {_t('Status')}: </td>
            <td>
              <strong>{assessmentState}</strong>
              <span className="HelpIconInline">
              {isExtraFields &&
                <HelpIcon
                  helpLink="assessment-state"
                  assessmentUuid={this.props.assessmentUuid}
                />
              }
              </span>
              <br />
                {assessmentStateDescription}
            </td>
          </tr>
          <tr>
            <td> {_t('Object')}: </td>
            <td> {products.map(x => <div><i className="fa fa-leaf"></i>{` ${x.name} (${x.area} ha)`}</div>)} </td>
          </tr>
          {
            this.props.isProgressVisible
            ? (
              <tr>
                <td> {_t('Progress')}: </td>
                <td> {this.props.assessmentOverviewStats} </td>
              </tr>
            )
            : null
          }
          <tr className="date-created">
            <td> {_t('Date created')}: </td>
            <td id="created-date-container">
                {createdTime}
                {
                  createdBy &&
                    <span> {_t('by')} <i className="fa fa-user" /> {createdBy} </span>
                }
            </td>
          </tr>
          <tr className="date-shared">
            <td> {_t('Date shared')}: </td>
            <td id="shared-date-container"> {dateShared} </td>
          </tr>
          {
            isExtraFields && (
              <tr>
                <td> {_t('Preference period')}: </td>
                <td> {prefferedMonth} </td>
              </tr>
            )
          }
          {
            isExtraFields && (
              <tr>
                <td> {_t('Document review date')}: </td>
                <td> {documentReviewDate} </td>
              </tr>
            )
          }
          {
            isExtraFields && (
              <tr>
                <td>{_t('Planned inspection date/time')}: </td>
                <td>{`${auditDate} ${auditTime}`}</td>
              </tr>
            )
          }
          {
            isExtraFields && (
              <tr>
                <td> {_t('Auditor')}: </td>
                <td> {auditor} </td>
              </tr>
            )
          }
        </table>
      </div>
    );
  }
}

AssessmentOverviewTable.propTypes = {
  options: PropTypes.shape({
    organizationName: PropTypes.string,
    createdTime: PropTypes.string,
    assessmentType: PropTypes.string,
    assessmentTypeDescription: PropTypes.string,
    assessmentState: PropTypes.string,
    products: PropTypes.array,
    createdBy: PropTypes.string,
    prefferedMonth: PropTypes.string,
    auditDate: PropTypes.string,
    auditTime: PropTypes.string,
    dateShared: PropTypes.string,
    documentReviewDate: PropTypes.string,
    assessmentStateDescription: PropTypes.string,
    auditor: PropTypes.string,
  }),
  isProgressVisible: PropTypes.bool,
  assessmentOverviewStats: PropTypes.string,
  assessmentUuid: PropTypes.string,
};

export default AssessmentOverviewTable;
