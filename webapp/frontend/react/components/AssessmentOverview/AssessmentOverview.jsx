import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import AssessmentOverviewTable from './AssessmentOverviewTable.jsx';
import AssessmentActionsToolbar from './AssessmentActionsToolbar.jsx';
import AssessmentOverviewStats from './AssessmentOverviewStats.jsx';
import pageData from '../Utils/pageData.js';

class AssessmentOverview extends Component {

  formatDate(date) {
    return moment(date).format('DD-MM-YYYY');
  }

  formatTime(time) {
    return moment(time).format('HH:mm');
  }

  render() {
    const { assessment, assessmentType, role } = this.props;
    const { overview } = assessment;
    const { membershipUuid, assessmentAccessMode, assessmentUuid } = this.props.params;

    const assessmentUrl = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}`;
    const stateDescription = {
      open: `${_t('This assessment is open')}. ${_t('You can edit its questionnaires')}.`,
      closed: `${_t('This assessment is closed')}.\
 ${_t('You can no longer edit the questionnaires because they have already been processed')}.`,
    };

    const stateTrans = {
      initial: `${_t('Initial')}`,
      open: `${_t('Open')}`,
      closed: `${_t('Closed')}`,
    };

    const options = {
      organizationName: overview.created_by_organization_name,
      dateShared: overview.date_shared ? this.formatDate(overview.date_shared) : '-',
      createdTime: this.formatDate(overview.created_time),
      assessmentType: assessmentType.name,
      assessmentTypeDescription: assessmentType.description,
      assessmentState: assessment.assessment.state in stateTrans
        ? _.capitalize(stateTrans[assessment.assessment.state]) : assessment.assessment.state,
      assessmentStateDescription: stateDescription[assessment.assessment.state],
      products: this.props.products,
      createdBy: overview.created_by,
      prefferedMonth: overview.preferred_month
        ? _.capitalize(tools.moment(overview.preferred_month, 'MM').locale(pageData.languageCode).format('MMMM'))
        : '-',
      sharedTime: overview.shared_time ? this.formatTime(overview.shared_time) : '-',
      auditDate: overview.audit_date ? this.formatDate(overview.audit_date) : '-',
      auditTime: overview.audit_date ? this.formatTime(overview.audit_date) : '',
      documentReviewDate: overview.document_review_date ? this.formatDate(overview.document_review_date) : '-',
      auditor: overview.auditor ? overview.auditor : '-',
    };

    const assessmentOverviewStats = <AssessmentOverviewStats assessment={assessment} params={this.props.params} />;
    const reportUrl = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/report`;

    return (
      <div>
        <AssessmentActionsToolbar
          assessmentState={assessment.assessment.state}
          reopenUrl={`${assessmentUrl}/reopen`}
          deleteUrl={`${assessmentUrl}/delete`}
          reportUrl={reportUrl}
          isReportVisible={assessment.isReportVisible}
          role={role}
          layout={pageData.context.layout}
        />
        <AssessmentOverviewTable
          options={options}
          assessmentOverviewStats={assessmentOverviewStats}
          isProgressVisible={this.props.isProgressVisible}
          assessmentUuid={assessment.uuid}
        />
      </div>
    );
  }
}

AssessmentOverview.propTypes = {
  params: PropTypes.object,
  isProgressVisible: PropTypes.bool,
  products: PropTypes.array,
  assessment: PropTypes.object,
  assessmentType: PropTypes.object,
  role: PropTypes.string,
};

export default AssessmentOverview;
