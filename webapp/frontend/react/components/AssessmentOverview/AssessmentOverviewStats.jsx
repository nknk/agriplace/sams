import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import frontendUrls from '../Utils/frontendUrls';
import pageData from '../Utils/pageData.js';

const membershipUuid = pageData.context.membershipUuid;

class AssessmentOverviewStats extends Component {

  getQuestionDoneCount(questions, assessment) {
    return _.values(questions).reduce((result, current) => {
      if (assessment.isQuestionDone(current.uuid)) {
        result++;
      }
      return result;
    }, 0);
  }

  getHeader(assessment) {
    const nodes = assessment.getLevels(_.values(assessment.assessmentType.questionnaires)[0]).map(
      level => <th>{`${level.title}`}<br />{_t('deviations')}</th>
    );

    return (
      <thead>
        <th>{_t('Questionnaire')}</th>
        <th>{_t('Progress')}</th>
        {nodes}
      </thead>
    );
  }

  getBody(assessment) {
    const assessmentAccessMode = this.props.params.assessmentAccessMode;
    const questionnaires = _.sortBy(_.values(assessment.assessmentType.questionnaires), 'order_index');
    const nodes = _.map(questionnaires, questionnaire => {
      const questions = _.pick(
        questionnaire.questions, question => question.is_statistics_valuable && assessment.getElementState(question.uuid).isVisible
      );
      const calcs = assessment.getLevelCalculations(questionnaire, 'no', questions, assessment.answers);
      const questionnaireLink = frontendUrls.get(
        'questionnaire', [membershipUuid, assessmentAccessMode, assessment.uuid, questionnaire.uuid]
      );
      return (
        <tr>
          <td>
            <Link
              to={questionnaireLink}>
              {questionnaire.name}
            </Link></td>
          <td>
            {`${this.getQuestionDoneCount(questions, assessment)}/${_.size(questions)}`}
          </td>
          {calcs.map(cal => <td>{cal.count}</td>)}
        </tr>
      );
    });

    return nodes;
  }

  render() {
    const { assessment } = this.props;

    return (
      <div>
        <table className="table datatable contrast-datatable">
          {this.getHeader(assessment)}
          {this.getBody(assessment)}
        </table>
      </div>
    );
  }
}

AssessmentOverviewStats.propTypes = {
  assessment: PropTypes.object,
  params: PropTypes.object,
};

export default AssessmentOverviewStats;
