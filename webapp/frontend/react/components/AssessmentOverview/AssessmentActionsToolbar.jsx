import React, { PropTypes, Component } from 'react';

class AssessmentActionsToolbar extends Component {

  render() {
    const { assessmentState, reopenUrl, deleteUrl, reportUrl, isReportVisible, layout, role } = this.props;

    if (layout !== 'agriplace') {
      return null;
    }

    return (
      <div className="toolbar wide">
        {(role !== 'external_auditor') && (assessmentState === 'closed') && (
          <div className="btn-group">
            <a className="btn btn-default" href={reopenUrl} rel="tooltip">
              <span className="{{ ICONS.edit }}" />&nbsp;
              {_t('Reopen assessment')}
            </a>
          </div>
        )}
        {(role !== 'external_auditor') && (
          <div className="btn-group">
            <a href="#actions" className="btn btn-default dropdown-toggle" data-toggle="dropdown">
              <i className="fa fa-cog" />&nbsp;
              {_t('Actions')}&nbsp;
              <span className="caret" />
            </a>
            <ul className="dropdown-menu">
              <li>
                <a href={deleteUrl}>
                  <i className="fa fa-trash-o" />&nbsp;
                  {_t('Delete assessment')}
                </a>
              </li>
            </ul>
          </div>
        )}
        {isReportVisible && reportUrl && (
          <div className="btn-group">
            <a href={reportUrl} target="_blank" className="btn btn-default report-button">
              <i className="fa fa-arrow-right" />&nbsp;
              {_t('Assessment report')}
            </a>
          </div>
        )}
      </div>
    );
  }
}

AssessmentActionsToolbar.propTypes = {
  assessmentState: PropTypes.string,
  reopenUrl: PropTypes.string,
  deleteUrl: PropTypes.string,
  reportUrl: PropTypes.string,
  isReportVisible: PropTypes.bool,
  role: PropTypes.string,
  layout: PropTypes.string,
};

export default AssessmentActionsToolbar;
