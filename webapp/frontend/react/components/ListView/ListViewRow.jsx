import React from 'react';

var getItem = function (item, column, format, locale) {
  for (var col in column)
    return tools.dateFilter(item[col], format, locale) || '';
};

var ListViewRow = React.createClass({

  propTypes: {
    listView: React.PropTypes.object.isRequired,
    item: React.PropTypes.object,
    columns: React.PropTypes.arrayOf(React.PropTypes.object)
  },


  onItemClicked: function (item) {
    if (!this.props.listView.readOnly) {
      tools.forceFunction(this.props.listView.onItemClicked)(item);
    }
  },


  getTds: function (item) {

    var tds = [];

    if (this.props.listView.showCheckBox)
      tds.push(
        <td key={ 'check-1' } className="list-view-checkbox">
          <input
            disabled={item.disabled || this.props.listView.readOnly}
            type = "checkbox"
            // need onChange otherwise React will give a warning/error in console..
            onChange = { tools.nop }
            checked = { this.isSelected }
          />
        </td>
      );

    _.forEach(this.props.columns, (column, key)=> {
      tds.push(
        <td key={ key }>
          <div style={{
            marginLeft: (this.props.listView.tdOffset === undefined) ? 8 : this.props.listView.tdOffset,
            color: item.disabled ? 'gray' : 'inherit'
          }}>
            { getItem(item, column, this.props.listView.dateFormat, this.props.listView.dateLocale) }
          </div>
        </td>
      );
    });

    return tds;
  },


  render: function () {
    var item = this.props.item || {};
    this.isSelected = ( this.props.listView.selected )
      ? _.findWhere( this.props.listView.selected, this.props.item )
      : false;

    return (
      <tr
        title = { this.props.listView.tooltip }
        className = { (this.isSelected) ? 'selected' : '' }
        onClick = { this.onItemClicked.bind(null, item) }
      >
        { this.getTds(item) }
      </tr>
    )
  }

});

module.exports = ListViewRow;
