import React from 'react';

require('./ListView.less');

const ListViewHeader = require('./ListViewHeader.jsx');
const ListViewRow = require('./ListViewRow.jsx');

// this runs only when ListView is called without a "listView.columns" array, all columns available in items
// will be shown in that case
const generateAllColumns = (items) => {
  const columns = [];
  if (items && items.length) {
    _.forEach(items[0], (value, key) => {
      const obj = {};
      obj[key] = key;
      columns.push(obj);
    });
  }
  return columns;
};


const ListView = React.createClass({

  propTypes: {
    listViewOptions: React.PropTypes.shape({
      items: React.PropTypes.arrayOf(React.PropTypes.object),
      columns: React.PropTypes.arrayOf(React.PropTypes.object),
      selected: React.PropTypes.arrayOf(React.PropTypes.object),
      sortKeys: React.PropTypes.arrayOf(React.PropTypes.string),
      isReverseOrder: React.PropTypes.bool,
      showHeader: React.PropTypes.bool,
      tableClasses: React.PropTypes.string,
      showCheckBox: React.PropTypes.bool,
      headerAlign: React.PropTypes.string,
      tooltip: React.PropTypes.string,
      onItemClicked: React.PropTypes.func,
      readOnly: React.PropTypes.bool,
      onSelectAllToogle: React.PropTypes.func,
      // for date formatting, see moment.js
      dateFormat: React.PropTypes.string,
      dateLocale: React.PropTypes.string,
      tdOffset: React.PropTypes.string,
      hasMore: React.PropTypes.bool,
      isLoadingMore: React.PropTypes.bool,
      loadMore: React.PropTypes.func,
    }).isRequired,
  },

  getInitialState() {
    this.columns = this.props.listViewOptions.columns || generateAllColumns(this.props.listViewOptions.items);
    return {
      sortCount: 0,
      sortColumn: '',
      isReverseOrder: !this.props.listViewOptions.isReverseOrder,
      sortKeys: this.props.listViewOptions.sortKeys || [],
    };
  },

  componentDidUpdate() {
    const { isLoadingMore } = this.props.listViewOptions;
    if (isLoadingMore) {
      this.spinner.getDOMNode().scrollIntoView();
    }
  },

  onHeaderClick(column) {
    let sortCount = this.state.sortCount;
    let sortColumn = this.state.sortColumn;
    let isReverseOrder = this.state.isReverseOrder;
    let sortKeys = this.state.sortKeys;

    // Logic to sort asc on first click
    // desc on second click
    // no sort on third click
    if (column === sortColumn) {
      sortCount = sortCount + 1;
      isReverseOrder = !isReverseOrder;
    } else {
      sortColumn = column;
      sortCount = 1;
      isReverseOrder = false;
    }

    if (sortCount > 2) {
      sortColumn = '';
    } else {
      sortColumn = column;
    }

    sortKeys = [sortColumn];
    // --------------------------

    this.setState({
      sortCount,
      sortColumn,
      isReverseOrder,
      sortKeys,
    });
  },

  handleScroll(value) {
    const { hasMore, isLoadingMore, loadMore } = this.props.listViewOptions;
    const { scrollTop, scrollHeight } = this.scroller.getDOMNode();
    const scrollOffset = this.scrollerParent.getDOMNode().scrollHeight;

    if (hasMore && !isLoadingMore && ((scrollTop + scrollOffset) >= scrollHeight)) {
      loadMore();
    }
  },

  //
  // Render
  //
  renderBody() {
    const listViewOptions = this.props.listViewOptions;
    listViewOptions.sortKeys = this.state.sortKeys;
    listViewOptions.isReverseOrder = this.state.isReverseOrder;

    let rows = [];

    if (listViewOptions.items) {
      const keysLowercase = [];
      const isReverseOrder = [];
      listViewOptions.sortKeys.forEach(key => {
        // pushes true or false in array, used by sortByOrder function
        isReverseOrder.push(listViewOptions.isReverseOrder);

        // generate a new key for the lowercase object
        const keyLowerCase = `${key}LowerCase`;

        // fill keysLowerCase Array
        keysLowercase.push(keyLowerCase);

        // add lowercase object to item
        listViewOptions.items.forEach(item => {
          item[keyLowerCase] = (typeof item[key] === 'string') ? item[key].toLowerCase() : item[key];
        });
      });

      // sort on the basis of new lowercase keys and values
      const list = _.sortByOrder(listViewOptions.items, keysLowercase, isReverseOrder);

      _.forEach(list, (item, key) => {
        rows.push(
          <ListViewRow
            key={key}
            listView={listViewOptions}
            columns={this.columns}
            item={item}
          />
        );
      });
    }

    // show emptyListText if list is empty, also skip one column if we have a checkBox
    if (rows.length < 1 && listViewOptions.emptyListText) {
      const checkBoxPart = (listViewOptions.showCheckBox) ? <td></td> : null;
      rows = (
        <tr key="0">
          {checkBoxPart}
          <td>
            {listViewOptions.emptyListText}
          </td>
        </tr>
      );
    }

    return (
      <tbody>{rows}</tbody>
    );
  },

  render() {
    let listViewOptions = this.props.listViewOptions;
    const { isLoadingMore, tableClasses = '' } = listViewOptions;

    listViewOptions.sortKeys = this.state.sortKeys;
    listViewOptions.isReverseOrder = this.state.isReverseOrder;

    return (
      <div ref={(div) => { this.scrollerParent = div; }} className="list-view">
        <div
          ref={(div) => { this.scroller = div; }}
          onScroll={this.handleScroll}
          className={`select-box ${tableClasses}`}
        >
          <table className={'table table-condensed'}>
          {
            listViewOptions.showHeader
            ? <ListViewHeader
              listViewOptions={listViewOptions}
              columns={this.columns}
              onHeaderClick={this.onHeaderClick}
            />
            : null
          }
            {this.renderBody()}
          </table>
          {
            isLoadingMore ? (
              <div ref={(div) => { this.spinner = div; }} className="busy-feedback-small">
                <span className="icon fa fa-spinner fa-spin fa-2x"></span>
              </div>
            ) : null
          }
        </div>
        <div className="clearfix"></div>
      </div>
    );
  },
});

module.exports = ListView;

