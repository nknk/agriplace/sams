import React from 'react';

const ListViewHeader = React.createClass({

  propTypes: {
    listViewOptions: React.PropTypes.object.isRequired,
    columns: React.PropTypes.arrayOf(React.PropTypes.object),
    onHeaderClick: React.PropTypes.func,
  },

  onSelectAllToogle(e) {
    const isAll = e.target.checked;
    this.props.listViewOptions.onSelectAllToogle(isAll);
  },

  onHeaderClick(column, e) {
    const onHeaderClick = this.props.onHeaderClick;
    onHeaderClick(column);
  },

  getColumns() {
    const html = [];

    if (this.props.listViewOptions.showCheckBox) {
      html.push(
        <th key="0" className="list-view-checkbox">
        {
          this.props.listViewOptions.onSelectAllToogle ?
            <input
              type="checkbox"
              onChange={this.onSelectAllToogle}
              disabled={this.props.listViewOptions.readOnly}
            /> : null
        }
        </th>
      );
    }

    const sortKeys = this.props.listViewOptions.sortKeys;
    const isReverseOrder = !!this.props.listViewOptions.isReverseOrder;
    const onHeaderClick = this.props.onHeaderClick;

    _.forEach(this.props.columns, (column, key) => {
      const columnKey = Object.keys(column)[0];
      const contains = !!sortKeys.find(sortKey => (sortKey === columnKey));

      const arrow = contains ? (
        <div style={{ display: 'inline-block', marginLeft: 5 }}>
          {
            isReverseOrder ? (
              <span className="icon fa fa-sort-up"></span>
            ) : (
              <span className="icon fa fa-sort-down"></span>
            )
          }
        </div>
      ) : null;

      html.push(
        <th
          style={{
            textAlign: this.props.listViewOptions.headerAlign || 'left',
            paddingLeft: this.props.listViewOptions.onSelectAllToogle ? 5 : 13,
            paddingRight: 5,
            cursor: onHeaderClick ? 'pointer' : 'arrow',
          }}
          key={key + 1}
          className="cell-sm"
          onClick={this.onHeaderClick.bind(this, columnKey)}
        >
          <div>
            {column}
            {arrow}
          </div>
        </th>
      );
    });

    return (
      <tr>{html}</tr>
    );
  },

  render() {
    return (
      <thead>
        {this.getColumns()}
      </thead>
    );
  },
});

module.exports = ListViewHeader;
