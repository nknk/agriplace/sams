

var t = require('tcomb-form');
var Form = t.form.Form;
var transform = require('tcomb-json-schema');
var formBuilderUtils = require('./formBuilderUtils.js');

transform.registerType('date', t.Date);

var FormBuilderOutput = React.createClass({
  propTypes: {
    schema: React.PropTypes.object,
    onFail: React.PropTypes.func,
  },

  shouldComponentUpdate(nextProps, nextState) {
    return !(_.isEqual(nextProps, this.props) && _.isEqual(nextState, this.state));
  },

  getInitialState() {
    return {
      tcomb: transform(this.props.schema),
      value: this.props.value,
    };
  },

  getValue() {
    return this.refs.form.getValue();
  },

  handleChange(value) {
    this.setState({ value });
  },

  componentWillReceiveProps(nextProps) {
    var err;
    var tcomb;

    try {
      tcomb = transform(nextProps.schema);
    } catch (e) {
      err = e.message;
    }
    if (!err) {
      this.setState({ tcomb });
      if (this.props.onValidationSuccess) {
        this.props.onValidationSuccess('tcomb');
      }
    } else {
      if (this.props.onValidationFail) {
        this.props.onValidationFail({ type: 'tcomb', msg: err });
      }
    }
    this.setState({ value: nextProps.value });
  },

  layoutFunc(layout) {
    return layout
      ? (locals) => {
        var dom = formBuilderUtils.buildHtmlToDom(layout, locals);
        return dom;
      }
    : undefined;
  },

  render() {
    var template = this.layoutFunc(this.props.layout);

    var options = formBuilderUtils.run(this.props.options) || {};

    options.template = template;
    return (
      <div className="FormBuilderOutput">
        <Form ref="form"
          type={this.state.tcomb}
          onChange={this.handleChange}
          value={this.state.value}
          options={options}
        />
      </div>
    );
  },

});

module.exports = FormBuilderOutput;
