

var CodeMirror = require('codemirror');
window.JSHINT = require('jshint').JSHINT;
require('codemirror/lib/codemirror.css');
require('codemirror/addon/lint/lint.css');
require('codemirror/addon/display/autorefresh.js');
require('codemirror/mode/javascript/javascript.js');
require('codemirror/addon/lint/javascript-lint.js');
require('codemirror/addon/lint/lint.js');

require('./FormBuilderInputJson.less');

var FormBuilderInputJson = React.createClass({

  onChange(instance) {
    var text = instance.doc.getValue();
    var err;
    var schema;
    try {
      schema = JSON.parse(text);
    } catch (e) {
      err = e.message;
    }
    if (!err) {
      this.props.onInputChange(schema);
      this.props.onValidationSuccess('json');
    } else {
      this.props.onValidationFail({ type: 'json', msg: err });
    }
  },

  componentDidMount() {
    var myCodeMirror = CodeMirror(this.getDOMNode(), {
      value: JSON.stringify(this.props.initSchema, undefined, 4),
      autoRefresh: true,
      mode: 'javascript',
      indentUnit: 1,
      lineNumbers: true,
      lint: true,
      gutters: ['CodeMirror-lint-markers'],
    }
    );
    myCodeMirror.on('change', this.onChange);
  },

  render() {
    return (
      <div className="FormBuilderInputJson code">
      </div>
    );
  },

});

module.exports = FormBuilderInputJson;
