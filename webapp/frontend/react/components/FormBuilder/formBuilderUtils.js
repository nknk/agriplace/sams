const traverse = require('traverse');
const HTMLtoJSX = require('htmltojsx');

const converter = new HTMLtoJSX({
  createClass: false,
});

const jsPrefix = 'js:';

module.exports = {

  buildHtmlToDom(html, locals) {
    if (tools.isIE()) {
      return new Error('Internet Explorer is not supported');
    }
    const babel = require('babel-core'); // eslint-disable-line global-require
    const jsxPlugin = require('babel-plugin-transform-react-jsx'); // eslint-disable-line global-require

    const jsx = converter.convert(html);
    const rDom = babel.transform(jsx, { plugins: [jsxPlugin] });
    // No other way
    /* eslint-disable no-eval */
    return eval(rDom.code);
  },

  isJsExp(exp) {
    const regex = new RegExp(jsPrefix);
    return regex.test(exp);
  },

  run(options) {
    const self = this;

    // Traverse has another api, so we can call map
    /* eslint-disable array-callback-return */
    const jsifyed = traverse(options).map((x) => {
      if (self.isJsExp(x)) {
        // No other way
        /* eslint-disable no-eval */
        this.update(eval(x.replace(jsPrefix, '')));
      }
    });
    return jsifyed;
  },

  format(value) {
    // Traverse has another api, so we can call map
    /* eslint-disable array-callback-return */
    const formattedValue = traverse(value).map(function (x) {
      if (x === null) {
        this.delete();
      } else {
        const newX = +x;
        if (typeof x === 'string' && !isNaN(newX)) {
          this.update(newX);
        }
      }
    });

    return formattedValue;
  },

  keepStringsformat(value) {
    // Traverse has another api, so we can call map
    /* eslint-disable array-callback-return */
    const formattedValue = traverse(value).map(function (x) {
      if (x === null) {
        this.delete();
      }
    });

    return formattedValue;
  },
};
