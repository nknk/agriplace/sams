

var CodeMirror = require('codemirror');
require('codemirror/addon/display/autorefresh.js');
require('codemirror/lib/codemirror.css');
require('codemirror/mode/htmlmixed/htmlmixed.js');

require('./FormBuilderInputJson.less');

var FormBuilderInputHtml = React.createClass({

  onChange(instance) {
    var text = instance.doc.getValue();
    this.props.onInputChange(text);
  },

  componentDidMount() {
    var myCodeMirror = CodeMirror(this.getDOMNode(), {
      value: this.props.initHtml,
      autoRefresh: true,
      mode: 'htmlmixed',
      indentUnit: 1,
      lineNumbers: true,
    }
    );
    this.myCodeMirror = myCodeMirror;
    myCodeMirror.on('change', this.onChange);
  },

  render() {
    if (this.myCodeMirror) {
      this.myCodeMirror.refresh();
    }
    return (
      <div className="FormBuilderInputHtml code">
      </div>
    );
  },

});

module.exports = FormBuilderInputHtml;
