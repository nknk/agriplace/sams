

var BusyFeedback = require('../BusyFeedback/BusyFeedback.jsx');

var pageData = require('../Utils/pageData.js');

var RvoImport = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('PlotsStore'),
  ],

  propTypes: {
    onFetch: React.PropTypes.func.isRequired,
    kvk: React.PropTypes.string,
  },

  getInitialState() {
    return {
      kvk: this.props.kvk || '',
    };
  },

  componentWillReceiveProps(nextProps) {
    if (!this.state.kvk) {
      this.setState({ kvk: nextProps.kvk });
    }
  },

  getStateFromFlux() {
    var flux = this.getFlux();
    var plotsStore = flux.store('PlotsStore');
    var plotsStoreState = plotsStore.getState();

    return {
      rvoData: plotsStoreState.rvoData,
      rvoErrors: plotsStoreState.rvoErrors,
      isRvoLoading: plotsStoreState.isRvoLoading,
    };
  },

  onFetchButtonClick() {
    this.props.onFetch(this.state.kvk);
  },

  handleKvkImportChange(e) {
    this.setState({ kvk: e.target.value });
  },

  render() {
    var fetchButton = (
      <Bootstrap.Button
        bsStyle= "success"
        disabled={this.state.isRvoLoading}
        onClick= { this.onFetchButtonClick }
      >
       {
        this.state.isRvoLoading
        ? <i className="icon fa fa-spinner fa-spin" style={{ marginRight: 5 }} />
        : null
       }
       Ophalen
     </Bootstrap.Button>
    );

    var kvkInput = (
      <Bootstrap.Input
        value={this.state.kvk}
        onChange={this.handleKvkImportChange}
        style={{ top: 0 }}
        bsStyle={this.state.kvk.length !== 8 ? 'warning' : 'success'}
        type="text"
        placeholder="12345678"
        hasFeedback
      />
    );
    var helpDocUrl = '/media/help_documents/rvo_help.pdf';

    return (
      <div className="RvoImport container-fluid">
        <div className="row">
          {
           this.state.rvoData
           ? (
             <div className="alert alert-success col-md-12">
               <span>{this.state.rvoData.plots_fetched} gewassen geïmporteerd</span>
             </div>
           )
           : null
          }
          {
            this.state.rvoErrors
            ? this.state.rvoErrors.map((error) => <div className="alert alert-danger col-md-12">{error}</div>)
            : null
          }
        </div>
        <div className="row" style={{ lineHeight: '2em' }}>
          <div>We hebben geen RVO perceelgegevens van u. Om deze beschikbaar te maken moet u de volgende dingen doen:​</div>
          <ol>
              <li>Autoriseer AgriPlace op <a href="https://mijn.rvo.nl" target="_blank">mijn.rvo.nl​</a>. Klik <a href={helpDocUrl} target="_blank">hier</a> voor een handleiding.​</li>
              <li><div style={{ display: 'inline-block' }}>Voeg uw KVK-nummer toe:</div><div style={{ display: 'inline-block', marginLeft: 10 }}>{kvkInput}</div></li>
          </ol>

          {fetchButton}
        </div>
      </div>
    );
  },

});

module.exports = RvoImport;
