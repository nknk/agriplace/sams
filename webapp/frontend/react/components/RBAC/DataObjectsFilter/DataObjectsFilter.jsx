

var pageData = require('../../Utils/pageData.js');


var DataObjectsFilter = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('RbacStore'),
  ],

  getStateFromFlux() {
    var flux = this.getFlux();
    var rbacStore = flux.store('RbacStore');
    var rbacFilter = rbacStore.selectedFilters;
    var privileges = rbacStore.privileges;
    var states = rbacStore.states;
    return {
      'filter': rbacFilter,
      'privileges': privileges,
      'states': states,
    };
  },

  handleDataObjectNameChange(e) {
    var flux = this.getFlux();
    flux.actions.rbacRoles.updateFilter(
      {
        'dataobjectName': {
          'name': e.target.value,
        },
      }
    );
  },

  handleDataObjectStateChange(e) {
    var flux = this.getFlux();
    flux.actions.rbacRoles.updateFilter(
      {
        'dataobjectState': {
          'state.name': e.target.value,
        },
      }
    );
  },

  handlePrivilegeSelectionChange(e) {
    var flux = this.getFlux();
    flux.actions.rbacRoles.updateFilter(
      {
        'privilege': {
          'privilegeId': e.target.value,
        },
      }
    );
  },

  handlePrivilegeAssignedChange(e) {
    var flux = this.getFlux();
    flux.actions.rbacRoles.updateFilter(
      {
        'privilegeAssigned': e.target.checked,
      }
    );
  },

  //
  // Render
  //

  render() {
    return (
      <table className="table table-bordered">
        <tr>

          <td className="col-xs-4">

            <div className="input-group">
              <input type="text"
                value={this.state.name}
                onChange={this.handleDataObjectNameChange}
                className="form-control"
                placeholder="Data object..."
              />
              <span className="input-group-btn">
                <button className="btn btn-default" type="button">
                  <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
                </button>
              </span>
            </div>

          </td>

          <td className="col-xs-3">

            <select className="form-control" onChange={this.handleDataObjectStateChange}>
              <option value="">All</option>
              {
                this.state.states.map((state) => {
                  return (
                    <option key={state.id} value={state.name}>{state.name}</option>
                  );
                })
              }
            </select>

          </td>

          <td className="col-xs-5">

            <form className="form-inline">

              <div className="form-group">
                <select className="form-control" onChange={this.handlePrivilegeSelectionChange}>
                  <option value="0">All</option>
                  {
                    this.state.privileges.map((privilege) => {
                      return (
                        <option key={privilege.id} value={privilege.id}>{privilege.name}</option>
                      );
                    })
                  }
                </select>
              </div>

              <div className="checkbox">
                <label className="checkbox-inline">
                  <input type="checkbox"
                    value={this.state.filter.privilegeAssigned}
                    checked={this.state.filter.privilegeAssigned}
                    onChange={this.handlePrivilegeAssignedChange}
                  /> Assigned only
                </label>
              </div>

            </form>

          </td>

        </tr>
      </table>

    );
  },

});

module.exports = DataObjectsFilter;
