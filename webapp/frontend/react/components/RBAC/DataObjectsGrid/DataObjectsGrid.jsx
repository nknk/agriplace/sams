
const DataObjectsTable = require('../DataObjectsTable/DataObjectsTable.jsx');
const DataObjectsFilter = require('../DataObjectsFilter/DataObjectsFilter.jsx');

const DataObjectsGrid = React.createClass({

  propTypes: {
    dataobjects: React.PropTypes.arrayOf(React.PropTypes.object),
    privileges: React.PropTypes.arrayOf(React.PropTypes.object),
  },

  //
  // Render
  //

  render() {
    return (
      <div>
        <DataObjectsFilter />
        <DataObjectsTable
          dataobjects={this.props.dataobjects}
          privileges={this.props.privileges}
        />
      </div>
    );
  },

});

module.exports = DataObjectsGrid;
