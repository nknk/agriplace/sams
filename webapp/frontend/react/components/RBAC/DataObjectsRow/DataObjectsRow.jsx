const DataObjectsRow = React.createClass({

  propTypes: {
    dataobject: React.PropTypes.object,
    handleChange: React.PropTypes.func,
    roleGrants: React.PropTypes.array,
    privileges: React.PropTypes.arrayOf(React.PropTypes.object),
  },

  shouldComponentUpdate(nextProps, nextState) {
    return !(_.isEqual(this.props, nextProps) && _.isEqual(this.state, nextState));
  },

  handleChange(e) {
    this.props.handleChange(this.props.dataobject, e.target.value, e.target.checked);
  },

  isChecked(dataobject, privilege, grants) {
    return _.some(grants, { to_role: privilege.id });
  },

  //
  // Render
  //

  render() {
    const checkboxList = this.props.privileges.map((privilege) => (
      <label key={privilege.id} className="checkbox-inline">
        <input
          type="checkbox"
          value={privilege.id}
          checked={this.isChecked(this.props.dataobject, privilege, this.props.roleGrants)}
          onChange={this.handleChange}
        />
        {privilege.name}
      </label>
    ));

    return (
      <tr key={this.props.dataobject.id}>
        <td>{this.props.dataobject.name}</td>
        <td>{this.props.dataobject.state.name}</td>
        <td>{checkboxList}</td>
      </tr>
    );
  },

});

module.exports = DataObjectsRow;
