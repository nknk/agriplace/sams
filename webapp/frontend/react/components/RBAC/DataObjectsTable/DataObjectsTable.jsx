const DataObjectsRow = require('../DataObjectsRow/DataObjectsRow.jsx');


const DataObjectsTable = React.createClass({

  propTypes: {
    dataobjects: React.PropTypes.arrayOf(React.PropTypes.object),
    privileges: React.PropTypes.arrayOf(React.PropTypes.object),
  },


  mixins: [
    FluxMixin,
    StoreWatchMixin('RbacStore'),
  ],

  shouldComponentUpdate(nextProps, nextState) {
    return !(_.isEqual(this.props, nextProps) && _.isEqual(this.state, nextState));
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const rbacStore = flux.store('RbacStore');
    const selectedRole = rbacStore.getCurrentRole();
    const roleGrants = _.filter(rbacStore.getGrants(), { from_role: selectedRole.id });
    return {
      selectedRole,
      roleGrants,
    };
  },

  handleChange(dataobject, value, checked) {
    const flux = this.getFlux();
    flux.actions.rbacRoles.updatePermissions(
      {
        assignment: {
          state: dataobject.state.name,
          data_object: dataobject.name,
        },
        privilegeId: value,
        privilegeState: checked,
        selectedRole: this.state.selectedRole,
      }
    );
  },


  //
  // Render
  //

  render() {
    const dataobjectList = this.props.dataobjects.map((dataobject) => (
      <DataObjectsRow
        key={dataobject.id}
        dataobject={dataobject}
        roleGrants={this.state.roleGrants.filter(
          (grant) => (
            grant.assignment.state === dataobject.state.name &&
            grant.assignment.data_object === dataobject.name
          )
        )}
        privileges={this.props.privileges}
        handleChange={this.handleChange}
      />
      )
    );

    return (
      <table className="table table-hover table-bordered">
        <thead>
          <tr>
            <th className="col-xs-4">Data object</th>
            <th className="col-xs-3">State</th>
            <th className="col-xs-5">Permissions</th>
          </tr>
        </thead>
        <tbody>
          {dataobjectList}
        </tbody>
      </table>
    );
  },

});

module.exports = DataObjectsTable;
