

var pageData = require('../../Utils/pageData.js');


var RoleFinder = React.createClass({

  mixins: [
    FluxMixin,
    StoreWatchMixin('RbacStore'),
  ],

  getStateFromFlux() {
    var flux = this.getFlux();
    var rbacStore = flux.store('RbacStore');
    var filter = rbacStore.selectedFilters;
    return {
      'filter': filter,
    };
  },

  handleRoleNameChange(e) {
    var flux = this.getFlux();
    flux.actions.rbacRoles.updateFilter(
      {
        'role': {
          'name': e.target.value,
        },
      }
    );
  },

  //
  // Render
  //

  render() {
    return (
      <div className="input-group">
        <input type="text" value={this.state.name} onChange={this.handleRoleNameChange} className="form-control" placeholder="Role name..." />
        <span className="input-group-btn">
          <button className="btn btn-default" type="button">
            <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
          </button>
        </span>
      </div>
    );
  },

});

module.exports = RoleFinder;
