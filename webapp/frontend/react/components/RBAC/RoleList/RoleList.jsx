

var pageData = require('../../Utils/pageData.js');

var RoleList = React.createClass({

  propTypes: {
    roleList: React.PropTypes.arrayOf(React.PropTypes.object),
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('RbacStore'),
  ],

  getStateFromFlux() {
    var flux = this.getFlux();
    var rbacStore = flux.store('RbacStore');
    var selectedRoleId = rbacStore.selectedRoleId;
    return {
      selectedRoleId,
    };
  },

  onRoleSelect(e) {
    var flux = this.getFlux();
    flux.actions.rbacRoles.setCurrentRole(+e.target.value);
  },

  //
  // Render
  //

  render() {
    return (
      <div>
        <select size="20" className="form-control" value={this.state.selectedRoleId} onChange={this.onRoleSelect}>
          {
            this.props.roleList.map((role) => {
              return (
                <option value={role.id} key={role.id}>{role.name}</option>
              );
            })
          }
        </select>
      </div>
    );
  },

});

module.exports = RoleList;
