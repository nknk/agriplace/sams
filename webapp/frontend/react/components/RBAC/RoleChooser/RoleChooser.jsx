

var pageData = require('../../Utils/pageData.js');
var RoleFinder = require('../RoleFinder/RoleFinder.jsx');
var RoleList = require('../RoleList/RoleList.jsx');

var RoleChooser = React.createClass({

  propTypes: {
    roleList: React.PropTypes.arrayOf(React.PropTypes.object),
  },

  //
  // Render
  //

  render() {
    return (
      <div className="panel panel-default">
        <div className="panel-heading">Available roles</div>
        <div>
          <RoleFinder />
          <RoleList roleList={this.props.roleList} />
        </div>
      </div>
    );
  },

});

module.exports = RoleChooser;
