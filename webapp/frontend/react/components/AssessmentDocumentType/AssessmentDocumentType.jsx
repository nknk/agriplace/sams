import React, { PropTypes } from 'react';
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
import '../Attachments/attachments.less';

const AttachmentCreator = require('../Attachments/AttachmentCreator.jsx');
const AttachmentsList = require('../Attachments/AttachmentsList.jsx');
const AttachmentsUtils = require('../Utils/attachments.js');
const NotApplicableButton = require('../Attachments/NotApplicableButton.jsx');
const classnames = require('classnames');
const ToastMixin = require('../../mixins/ToastMixin.js');

import { AssessmentDocumentTypeOptional } from './AssessmentDocumentTypeOptional.jsx';
import { DocTypeReuse } from '../DocTypeReuse/DocTypeReuse.jsx';
import { DocTypeReuseLink } from '../DocTypeReuse/DocTypeReuseLink.jsx';
import { EditAttachmentModalController } from '../Attachments/Modals/EditAttachmentModalController.jsx';
import { AssociatedQuestions } from './AssociatedQuestions.jsx';

//
// Detailed document type row in an assessment
//
const AssessmentDocumentType = React.createClass({
  propTypes: {
    assessment: PropTypes.object,
    currentOrganization: PropTypes.object,
    documentType: PropTypes.object.isRequired,
    isAnswered: PropTypes.bool,
    readOnly: PropTypes.bool,
    layout: PropTypes.string,
    evidenceAssociatedQuestions: PropTypes.object,
    params: PropTypes.object.isRequired,
  },

  mixins: [
    FluxMixin,
    ToastMixin(),
  ],

  getInitialState() {
    return {
      editModalParams: {
        isVisible: false,
      },
      fileChooserParams: {
        isVisible: false,
        files: [],
      },
      openedAttachmentLink: undefined,
      isDescriptionExpanded: false,
      isHistoricalAttachmentVisible: false,
    };
  },

  //
  // Event handler for attach modal success.
  //
  onCreateAttachments(attachments) {
    const { assessment, documentType } = this.props;
    const assessmentUuid = assessment && assessment.assessment.uuid;
    const documentTypeUuid = documentType.uuid;

    const attachmentLinks = [];
    let isDone = true;
    _.forEach(attachments, (attachment) => {
      if (attachment.attachment_type === 'FormAttachment') {
        isDone = attachment.is_done;
        const currentAttachmentLinks = assessment.attachmentLinks[documentTypeUuid];

        let agriformQuantity = 0;
        if (currentAttachmentLinks) {
          agriformQuantity = this.getAgriformQuantity(currentAttachmentLinks);
        }

        attachment.title = this.getAgriformName({ documentTypeName: documentType.name, agriformQuantity });
      }
      const attachmentLink = AttachmentsUtils.attachmentToAttachmentLink(
        attachment,
        assessmentUuid,
        documentTypeUuid,
        isDone
      );
      attachmentLinks.push(attachmentLink);
    });

    const flux = this.getFlux();
    flux.actions.attachmentLinks.createAttachmentLink(
      attachmentLinks,
      assessmentUuid
    );
  },


  onCreateAgriform(attachment) {
    const { assessment, documentType } = this.props;
    const assessmentUuid = assessment ? assessment.uuid : undefined;
    const documentTypeUuid = documentType.uuid;

    if (!documentType.assessment_type) {
      log.info('[onCreateAgriform]: no agriforms attached to this documentType: ', documentType.uuid);
    } else {
      // adding correct documentType
      attachment.agriformAssessmentTypeUuid = documentType.assessment_type;

      // fixing name
      // TODO - think about correct place for this
      const currentAttachmentLinks = assessment.attachmentLinks[documentTypeUuid];

      let agriformQuantity = 0;
      if (currentAttachmentLinks) {
        agriformQuantity = this.getAgriformQuantity(currentAttachmentLinks);
      }

      attachment.title = this.getAgriformName({ documentTypeName: documentType.name, agriformQuantity });

      const isDone = false;
      const attachmentLink = AttachmentsUtils.attachmentToAttachmentLink(
        attachment,
        assessmentUuid,
        documentTypeUuid,
        isDone
      );
      const flux = this.getFlux();
      const self = this;

      flux.actions.agriform.createAgriformAttachment(
        attachmentLink,
        assessment.assessment.organization,
        self.onOpenAttachmentLink
      );
    }
  },

  //
  // Handler for existing attachment opened (clicked)
  //
  onOpenAttachmentLink(attachmentLink) {
    // We are doing this ugly thing, because we can't do in other way. We should somehow render FormAttachment... :(
    const { documentType, currentOrganization } = this.props;
    if (attachmentLink.attachment.generic_form_data) {
      attachmentLink.attachment.generic_form_data.slug = documentType.generic_form.slug;
      attachmentLink.attachment.is_done = attachmentLink.is_done;
    }
    const attachment = attachmentLink.attachment;
    // Ensure is it a file && it's extension is pdf
    const isPdf = (attachment.attachment_type === 'FileAttachment') &&
                  (attachment.original_file_name.split('.').pop(-1) === 'pdf');
    const isAuditor = currentOrganization.type === 'auditor';
    if (isPdf && isAuditor) {
      window.open(`${attachment.file}`, '_blank');
    } else {
      this.setState({ openedAttachmentLink: attachmentLink });
      this.refs.EditAttachmentModalController.openModal();
    }
  },


  onAttachmentDelete() {
    const flux = this.getFlux();
    const { openedAttachmentLink } = this.state;
    flux.actions.attachmentLinks.deleteAttachmentLink(openedAttachmentLink.assessment, openedAttachmentLink.id);
    this.toast({
      title: _t('Attachment deleted.'),
      type: 'success',
    });

    this.refs.EditAttachmentModalController.onCancel();
  },


  onAttachmentModalSuccess(modifiedAttachment) {
    if (!this.props.readOnly) {
      const flux = this.getFlux();
      const updatedAttachmentLink = this.state.openedAttachmentLink;
      updatedAttachmentLink.attachment = modifiedAttachment;

      if (modifiedAttachment.attachment_type === 'FormAttachment') {
        updatedAttachmentLink.is_done = modifiedAttachment.is_done;
      }

      const agriformUuid = modifiedAttachment.agriformUuid;
      if (agriformUuid) {
        const assessments = flux.stores.AssessmentsStore.getState().assessments;
        const agriform = assessments[agriformUuid];
        const questionnaireUuid = _.first(_.keys(agriform.assessmentType.questionnaires));
        updatedAttachmentLink.is_done = agriform.questionnaireState[questionnaireUuid].isDone;
        flux.actions.assessments.saveAssessmentState(agriformUuid);
      }

      const utl = updatedAttachmentLink;
      flux.actions.attachmentLinks.updateAttachmentLink(utl, utl.assessment, utl.id);
      this.toast({
        title: `${_t('Attachment updated')}.`,
        type: 'success',
      });
    }
    this.refs.EditAttachmentModalController.onCancel();
  },

  onHistoricalAttachmentReuse() {
    this.setState({ isHistoricalAttachmentVisible: false });
  },

  onToggleHistoricalAttachmentBox() {
    this.setState({ isHistoricalAttachmentVisible: !this.state.isHistoricalAttachmentVisible });
  },


  isNotApplicableAvailable(assessmentTypesWhereOptional, assessmentTypeUuid) {
    return assessmentTypesWhereOptional.indexOf(assessmentTypeUuid) !== -1;
  },

  handleExpandDescriptionClick(e) {
    e.preventDefault();
    this.setState({ isDescriptionExpanded: !this.state.isDescriptionExpanded });
  },

  getAgriformQuantity(attachmentLinks) {
    return attachmentLinks.reduce((privValue, attachmentLink) => {
      const isAgriform = ~['AgriformAttachment', 'FormAttachment'].indexOf(attachmentLink.attachment.attachment_type);
      return isAgriform ? privValue + 1 : privValue;
    }, 0);
  },

  getAgriformName({ documentTypeName, agriformQuantity = 0 }) {
    const today = tools.moment().format('DD/MM/YYYY');
    return `${documentTypeName}_${today}_${agriformQuantity + 1}`;
  },

  getEvidenceQuestionLevel(documentType) {
    const questionLevel = documentType.question_level;
    const color = (questionLevel && questionLevel.color) || '#F5F5F5';
    const style = {
      background: color,
    };
    return (
      <div className="row">
        <div className="col-md-8">
          {
            questionLevel && questionLevel.title ? (
              <div className="pull-right question-level-component" style={style}>
                {questionLevel.title}
              </div>
            ) : null
          }
        </div>
      </div>
    );
  },

  getEvidenceAssociatedQuestions(docTypeAssociatedQuestions) {
    return (docTypeAssociatedQuestions && docTypeAssociatedQuestions.length ? (
      <div className="inline-item space-on-right">
        <AssociatedQuestions questions={docTypeAssociatedQuestions} params={this.props.params} />
      </div>
    ) : null);
  },

  //
  // Render
  //
  render() {
    const { documentType, assessment, layout = 'evidence',
      isAnswered, readOnly, evidenceAssociatedQuestions } = this.props;
    const { isDescriptionExpanded, openedAttachmentLink, isHistoricalAttachmentVisible } = this.state;
    const docTypeAssociatedQuestions = documentType && documentType.uuid
      && evidenceAssociatedQuestions && evidenceAssociatedQuestions[documentType.uuid];

    const attachmentLinks = assessment.attachmentLinks[documentType.uuid] || [];

    const notApplicableAttachmentLink = _.find(attachmentLinks,
      (attachmentLink) => attachmentLink.attachment.attachment_type === 'NotApplicableAttachment');
    const realAttachmentLinks = _.filter(attachmentLinks,
      (attachmentLink) => attachmentLink.attachment.attachment_type !== 'NotApplicableAttachment');

    const assessmentTypesWhereOptional = documentType.assessment_types_where_optional;
    const assessmentTypeUuid = assessment.assessment.assessment_type;

    const helpDocumentPart = !!documentType.template &&
      <a href={documentType.template} target="_blank">{_t('Help document')}</a>;

    const attachButtonOptionKeys = ['ARCHIVE_SELECT', 'FILE_UPLOAD', 'TEXT_REFERENCE'];
    if (documentType.assessment_type) {
      if (documentType.generic_form) {
        attachButtonOptionKeys.push('GENERIC_FORM');
      } else {
        attachButtonOptionKeys.push('AGRIFORM');
      }
    }

    const className = classnames({
      answered: isAnswered,
    });

    // Historical Attachment Stuff
    const reuseCount = assessment.documentTypeReuseAttachments[documentType.uuid];
    const docTypeReuseNode = isHistoricalAttachmentVisible ? (
      <DocTypeReuse
        ref="DocTypeReuse"
        layout={layout}
        onAttach={this.onHistoricalAttachmentReuse}
        assessmentUuid={assessment.uuid}
        documentTypeUuid={documentType.uuid}
        onOpenAttachmentLink={this.onOpenAttachmentLink}
      />
    ) : null;

    const docTypeReuseLinkNode = (
      <DocTypeReuseLink
        isHistoricalAttachmentVisible={isHistoricalAttachmentVisible}
        reuseCount={reuseCount}
        onClick={this.onToggleHistoricalAttachmentBox}
        documentTypeUuid={documentType.uuid}
      />
    );

    const attachment = openedAttachmentLink && openedAttachmentLink.attachment || {};
    const attachmentAssessment = openedAttachmentLink && openedAttachmentLink.assessment;
    const EditAttachmentModalControllerNode = (
      <EditAttachmentModalController
        ref="EditAttachmentModalController"
        isDescriptionReadOnly={readOnly || attachment.is_frozen || attachmentAssessment !== assessment.uuid}
        isDeleteReadOnly={readOnly || attachmentAssessment !== assessment.uuid}
        onAttachmentModalSuccess={this.onAttachmentModalSuccess}
        onAttachmentDelete={this.onAttachmentDelete}
        attachment={attachment}
        docTypeAssociatedQuestions={docTypeAssociatedQuestions}
        isDeleteConfirmationRequired
      />
    );


    const isAttachmentCreatorReadOnly = readOnly || notApplicableAttachmentLink;

    const isDocTypeLevelPresent = documentType && documentType.question_level && documentType.question_level.title;
    const paddingClassForEvidenceBox = classnames({
      'padded-content-except-top': isDocTypeLevelPresent,
      'padded-content': !isDocTypeLevelPresent,
    });

    return layout === 'evidence' ? (// eslint-disable-line no-nested-ternary
      <div className={className}>
        {EditAttachmentModalControllerNode}
        <div className="document-type row-list-item">
          {this.getEvidenceQuestionLevel(documentType)}
          <div className={paddingClassForEvidenceBox}>
            <div className="row">
              <div className="col-md-8">
                <div>
                  {this.getEvidenceAssociatedQuestions(docTypeAssociatedQuestions)}
                  <div className="inline-item space-on-right">
                    <strong>{documentType.name}</strong>
                  </div>
                  <div className="inline-item">
                    <small>({documentType.code})</small>
                  </div>
                </div>
                <p dangerouslySetInnerHTML={{ __html: documentType.description }} />
                <div>
                  {helpDocumentPart}
                </div>
              </div>
              <div className="col-md-4">
                <div className="row">
                  {/* Attach button */}
                  <div className="col-xs-2 col-md-4 pull-right">
                    <div className="pull-right">
                      {this.isNotApplicableAvailable(assessmentTypesWhereOptional, assessmentTypeUuid) ? (
                        <NotApplicableButton
                          isDisabled={readOnly}
                          notApplicableAttachmentLink={notApplicableAttachmentLink}
                          realAttachmentLinks={realAttachmentLinks}
                          assessmentUuid={assessment.uuid}
                          documentTypeUuid={documentType.uuid}
                        />) : null
                      }
                      <AttachmentCreator
                        readOnly={isAttachmentCreatorReadOnly}
                        optionKeys={attachButtonOptionKeys}
                        genericForm={documentType.generic_form}
                        onCreateAttachments={this.onCreateAttachments}
                        onCreateAgriform={this.onCreateAgriform}
                      />
                    </div>
                  </div>
                {/* Attachment list */}
                  <div className="row">
                    <div className="col-md-12" id="attachment-list">
                      <div>
                        <strong>
                          {_t('Attachments')}:
                        </strong>
                      </div>
                      <AttachmentsList
                        attachmentLinks={realAttachmentLinks}
                        onOpenAttachmentLink={this.onOpenAttachmentLink}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {docTypeReuseLinkNode}
            {docTypeReuseNode}
          </div>
        </div>
      </div>
    ) : documentType.is_optional ? (
      <AssessmentDocumentTypeOptional
        documentType={documentType}
        readOnly={isAttachmentCreatorReadOnly}
        attachButtonOptionKeys={attachButtonOptionKeys}
        realAttachmentLinks={realAttachmentLinks}
        onCreateAttachments={this.onCreateAttachments}
        onCreateAgriform={this.onCreateAgriform}
        onOpenAttachmentLink={this.onOpenAttachmentLink}
        docTypeReuseLinkNode={docTypeReuseLinkNode}
        EditAttachmentModalControllerNode={EditAttachmentModalControllerNode}
        docTypeReuseNode={docTypeReuseNode}
      />
    ) : (
      <div className={className}>
        {EditAttachmentModalControllerNode}
        <div className="document-type row-list-item">
          <div className="row">
            <div className="col-md-8">
              <div className="padded-content">
                <div className="row">
                  <div className="col-md-12">
                    <div>
                      <strong>{_t('Evidence')}: {documentType.name}</strong>
                      &nbsp;
                      <small>({documentType.code})</small>
                      &nbsp;
                      <Bootstrap.Button
                        bsStyle="link"
                        className="expandDescriptionBtn"
                        onClick={this.handleExpandDescriptionClick}
                      >
                        <span className="icon fa fa-info-circle" />
                        &nbsp;
                        {isDescriptionExpanded ? `${_t('Read less')}...` : `${_t('Read more')}...`}
                      </Bootstrap.Button>
                      {isDescriptionExpanded && (
                        <div
                          className="description"
                          dangerouslySetInnerHTML={{ __html: documentType.description }}
                        />
                      )}
                    </div>
                    <div>{helpDocumentPart}</div>
                  </div>
                </div>
                {/* Attachment list */}
                <div className="row">
                  <div className="col-md-12" id="attachment-list">
                    <AttachmentsList
                      attachmentLinks={realAttachmentLinks}
                      onOpenAttachmentLink={this.onOpenAttachmentLink}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="padded-content">
                <div className="row">
                  {/* Attach button */}
                  <div className="col-xs-2 col-md-4 pull-right">
                    <div className="pull-right">
                      {this.isNotApplicableAvailable(assessmentTypesWhereOptional, assessmentTypeUuid) ? (
                        <NotApplicableButton
                          isDisabled={readOnly}
                          notApplicableAttachmentLink={notApplicableAttachmentLink}
                          realAttachmentLinks={realAttachmentLinks}
                          assessmentUuid={assessment.uuid}
                          documentTypeUuid={documentType.uuid}
                        />) : null
                      }
                      <AttachmentCreator
                        readOnly={isAttachmentCreatorReadOnly}
                        optionKeys={attachButtonOptionKeys}
                        genericForm={documentType.generic_form}
                        onCreateAttachments={this.onCreateAttachments}
                        onCreateAgriform={this.onCreateAgriform}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {docTypeReuseLinkNode}
        {docTypeReuseNode}
      </div>
    );
  },
});

module.exports = AssessmentDocumentType;

