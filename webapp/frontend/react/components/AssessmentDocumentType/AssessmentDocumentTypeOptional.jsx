import React, { Component } from 'react';
const AttachmentCreator = require('../Attachments/AttachmentCreator.jsx');
const AttachmentsList = require('../Attachments/AttachmentsList.jsx');
require('./AssessmentDocumentTypeOptional.less');

export class AssessmentDocumentTypeOptional extends Component {
  constructor() {
    super();
    this.state = {
      isExpanded: false,
    };
    this.handleExpandDescriptionClick = this.handleExpandDescriptionClick.bind(this);
  }

  handleExpandDescriptionClick() {
    this.setState({ isExpanded: !this.state.isExpanded });
  }

  render() {
    const { documentType, readOnly, attachButtonOptionKeys, docTypeReuseLinkNode, docTypeReuseNode,
            onCreateAttachments, onCreateAgriform, onOpenAttachmentLink, realAttachmentLinks,
            EditAttachmentModalControllerNode } = this.props;

    const { isExpanded } = this.state;

    const attachmentsTitle = `${_t('Attachments')} (${realAttachmentLinks.length})`;

    return (
      <div className="AssessmentDocumentTypeOptional document-type row-list-item">
        {EditAttachmentModalControllerNode}
        <div className="row">
          <div className="col-xs-8 title">
            <strong title="These attachments are optional">{documentType.name}</strong>
          </div>
          <div className="col-xs-4">
            <Bootstrap.Button
              bsStyle="link"
              className="expandDescriptionBtn pull-right"
              onClick={this.handleExpandDescriptionClick}
            >
              {isExpanded ? `${attachmentsTitle} ▲` : `${attachmentsTitle} ▼`}
            </Bootstrap.Button>
          </div>
        </div>
        {
          isExpanded &&
            <div className="clearfix">
              <div className="pull-right">
                <AttachmentCreator
                  readOnly={readOnly}
                  optionKeys={attachButtonOptionKeys}
                  genericForm={documentType.generic_form}
                  onCreateAttachments={onCreateAttachments}
                  onCreateAgriform={onCreateAgriform}
                />
              </div>
              <AttachmentsList
                attachmentLinks={realAttachmentLinks}
                onOpenAttachmentLink={onOpenAttachmentLink}
              />
              {docTypeReuseLinkNode}
              {docTypeReuseNode}
            </div>
        }
      </div>
    );
  }
}

AssessmentDocumentTypeOptional.propTypes = {
  documentType: React.PropTypes.object,
  readOnly: React.PropTypes.bool,
  attachButtonOptionKeys: React.PropTypes.bool,
  realAttachmentLinks: React.PropTypes.array,
  onCreateAttachments: React.PropTypes.func,
  onCreateAgriform: React.PropTypes.func,
  onOpenAttachmentLink: React.PropTypes.func,
  docTypeReuseLinkNode: React.PropTypes.func,
  EditAttachmentModalControllerNode: React.PropTypes.func,
  docTypeReuseNode: React.PropTypes.func,
};
