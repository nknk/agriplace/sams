import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Overlay } = Bootstrap;
import { Link } from 'react-router';
require('./AssociatedQuestions.less');

export class AssociatedQuestions extends React.Component {

  constructor() {
    super();
    this.closeDropDown = this.closeDropDown.bind(this);
    this.toggleDropDown = this.toggleDropDown.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  toggleDropDown() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  closeDropDown() {
    this.setState({
      isOpen: false,
    });
  }

  getQuestionTitle(question) {
    const questionnaireCodeExtract = tools.removePrefixFromString(question.questionnaire_code, '-');
    const questionCodeExtract = tools.removePrefixFromString(question.code, '-');
    return `${questionnaireCodeExtract}-${questionCodeExtract}`;
  }

  getQuestionLink(question, assessmentAccessMode, membershipUuid, assessmentUuid) {
    const url = `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}`;
    return `${url}/questionnaires/${question.questionnaire}/edit#${question.uuid}`;
  }

  render() {
    const { questions, params } = this.props;
    const { assessmentAccessMode, membershipUuid, assessmentUuid } = params;

    return (
      <div className="associated-questions space-on-right" >
        {
          questions && (questions.length > 1) ? (
            <div className="questions-popup">
              <div className="overlay-container" ref={o => { this.overlayContainer = o; }} >
                <Overlay
                  show={this.state.isOpen}
                  onHide={this.closeDropDown}
                  placement="top"
                  container={this.overlayContainer}
                  rootClose
                >
                  <div className="overlay-popup">
                    {
                      questions.map(question => (
                        <div className="link-div">
                          <Link
                            to={this.getQuestionLink(question, assessmentAccessMode, membershipUuid, assessmentUuid)}
                          >
                            {this.getQuestionTitle(question)}
                          </Link>
                        </div>
                      ))
                    }
                  </div>
                </Overlay>
              </div>
              <div className="popup-overlay-button" onClick={this.toggleDropDown}>
                <Link
                  className="inline-item"
                  onClick={(e) => { e.preventDefault(); }}
                  to={''}
                >
                  {this.getQuestionTitle(questions[0])}
                </Link>
                <div className="inline-item drop-down-icon" onClick={this.toggleDropDown}>
                  <i className="fa fa-caret-down" aria-hidden="true" />
                </div>
              </div>
            </div>
          ) : (
            <Link to={this.getQuestionLink(questions[0], assessmentAccessMode, membershipUuid, assessmentUuid)}>
              {this.getQuestionTitle(questions[0])}
            </Link>
          )
        }
      </div>
    );
  }
}

AssociatedQuestions.propTypes = {
  questions: React.PropTypes.array.isRequired,
  params: React.PropTypes.object.isRequired,
};
