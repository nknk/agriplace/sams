import React, { Component } from 'react';
import { ButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap';
import { answerFilterOptions, NO_FILTER } from './FilterOptions';
import * as _ from 'lodash';

export default class AnswersFilter extends Component {

  constructor() {
    super();
    this.onAnswerFilterSelect = this.onAnswerFilterSelect.bind(this);
  }

  onAnswerFilterSelect(e, filterKey) {
    const { params, history, query } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid, questionnaireUuid } = params;
    query.answers = filterKey;
    history.replace({
      query,
      pathname: `/${membershipUuid}/${assessmentAccessMode}/assessments\
/${assessmentUuid}/questionnaires/${questionnaireUuid}/edit`,
    });
  }

  render() {
    const { query } = this.props;
    const filter = query.answers || NO_FILTER;

    return (
      <ButtonGroup>
        <DropdownButton
          bsStyle="default"
          title={`${_t('Answer')}: ${answerFilterOptions[filter]}`}
          onSelect={this.onAnswerFilterSelect}
        >
          {_.map(answerFilterOptions, (itemText, key) => (
            <MenuItem key={key} eventKey={key}>
              {`${_t('Answer')}: ${itemText}`}
            </MenuItem>
          ))}
        </DropdownButton>
      </ButtonGroup>
    );
  }
}

AnswersFilter.propTypes = {
  query: React.PropTypes.string,
  params: React.PropTypes.object,
  history: React.PropTypes.object,
};
