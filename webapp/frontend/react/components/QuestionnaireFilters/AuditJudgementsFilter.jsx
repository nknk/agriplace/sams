import React, { Component } from 'react';
import { ButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import { auditJudgementFilterOptions, NO_FILTER } from './FilterOptions';
import * as _ from 'lodash';

export default class AuditJudgementsFilter extends Component {

  constructor() {
    super();
    this.onJudgementsFilterSelect = this.onJudgementsFilterSelect.bind(this);
  }

  onJudgementsFilterSelect(e, filterKey) {
    const { params, history, query } = this.props;
    const { membershipUuid, assessmentAccessMode, assessmentUuid, questionnaireUuid } = params;
    query.judgements = filterKey;
    history.replace({
      query,
      pathname: `/${membershipUuid}/${assessmentAccessMode}/assessments\
/${assessmentUuid}/questionnaires/${questionnaireUuid}/edit`,
    });
  }

  render() {
    const { query, params } = this.props;
    const filter = query.judgements || NO_FILTER;

    return (
      <PermissionController assessmentUuid={params.assessmentUuid} component="JudgementSection">
        <ButtonGroup>
          <DropdownButton
            bsStyle="default"
            title={`${_t('Judgement')}: ${auditJudgementFilterOptions[filter]}`}
            onSelect={this.onJudgementsFilterSelect}
          >
            {_.map(auditJudgementFilterOptions, (itemText, key) => (
              <MenuItem key={key} eventKey={key}>
                {`${_t('Judgement')}: ${itemText}`}
              </MenuItem>
            ))}
          </DropdownButton>
        </ButtonGroup>
      </PermissionController>
    );
  }
}

AuditJudgementsFilter.propTypes = {
  query: React.PropTypes.string,
  params: React.PropTypes.object,
  history: React.PropTypes.object,
};
