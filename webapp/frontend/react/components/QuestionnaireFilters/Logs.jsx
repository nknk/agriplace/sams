import React, { Component } from 'react';
import pageData from '../Utils/pageData.js';
const { membershipUuid } = pageData.context;
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import { ButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap';

export default class Logs extends Component {

  render() {
    const { assessmentUuid } = this.props;
    return (
      <PermissionController assessmentUuid={assessmentUuid} component="MutationLogs">
        <ButtonGroup>
          <DropdownButton
            bsStyle="default"
            title={_t('Logs')}
          >
            <PermissionController assessmentUuid={assessmentUuid} component="AssessmentLogs">
              <MenuItem
                target="_blank"
                href={`/${membershipUuid}/our/assessments/${assessmentUuid}/logs/assessment`}
              >
                {_t('Assessment log')}
              </MenuItem>
            </PermissionController>

            <PermissionController assessmentUuid={assessmentUuid} component="JudgementLogs">
              <MenuItem
                target="_blank"
                href={`/${membershipUuid}/our/assessments/${assessmentUuid}/logs/judgement`}
              >
                {_t('Judgement log')}
              </MenuItem>
            </PermissionController>

          </DropdownButton>
        </ButtonGroup>
      </PermissionController>
    );
  }
}

Logs.propTypes = {
  assessmentUuid: React.PropTypes.string,
};
