import React, { Component } from 'react';
import './QuestionnaireFilters.less';
import Logs from './Logs.jsx';
import AnswersFilter from './AnswersFilter.jsx';
import AuditJudgementsFilter from './AuditJudgementsFilter.jsx';
import ConfirmAll from '../ConfirmAll/ConfirmAll.jsx';

export default class QuestionnaireFilters extends Component {

  render() {
    const { query, params, history, isConfirmAllInProgress, isConfirmAllModalOpen, judgementActions } = this.props;
    return (
      <div className="questionnaire-filters">
        <div className="col-xs-7">
          <div className="toolbar wide muted row">
            <strong className="filters-text">
              {_t('Filters')}
              {':'}
            </strong>
            <AnswersFilter
              query={query}
              params={params}
              history={history}
            />
            <AuditJudgementsFilter
              query={query}
              params={params}
              history={history}
            />
          </div>
        </div>
        <div className="col-xs-5">
          <div className="pull-right">
            <div className="toolbar wide muted row">
              <ConfirmAll
                params={params}
                isConfirmAllModalOpen={isConfirmAllModalOpen}
                isConfirmAllInProgress={isConfirmAllInProgress}
                judgementActions={judgementActions}
              />
              <Logs assessmentUuid={params.assessmentUuid} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

QuestionnaireFilters.propTypes = {
  query: React.PropTypes.string,
  params: React.PropTypes.object,
  history: React.PropTypes.object,
  isConfirmAllModalOpen: React.PropTypes.bool,
  isConfirmAllInProgress: React.PropTypes.bool,
  judgementActions: React.PropTypes.object,
};
