export const NO_FILTER = 'noFilter';

export const answerFilterOptions = {
  noFilter: _t('all'),
  incomplete: _t('incomplete'),
  yes: _t('yes'),
  no: _t('no'),
  not_applicable: _t('not applicable'),
};

export const auditJudgementFilterOptions = {
  noFilter: _t('all'),
  yes: _t('yes'),
  no: _t('no'),
  open: _t('open'),
};
