import React, { Component } from 'react';
const t = require('tcomb');
const tForm = require('tcomb-form');
const Form = tForm.form.Form;
const utils = require('../FormBuilder/formBuilderUtils.js');
const moment = require('moment');


const layout = (locals) => {
  var inputs = locals.inputs;
  const currentYear = new Date().getFullYear();
  return (
    <div>
      <div className="row">
        <h2 style={{ textAlign: 'center' }}>
          Aanmelding GLOBALG.A.P. { currentYear }
        </h2>
      </div>
      <div className="row">
        <h4 style={{ textAlign: 'center' }}>
          Wij verzoeken u het onderstaande formulier voor 1 mei in te zenden
        </h4>
      </div>
      <br />
      <div className="row" style={ { 'display': 'flex', 'align-items': 'flex-end' } }>
        <div className="col-xs-6">
          <div className="row">
            {inputs.jaNeeHzpc}
          </div>
        </div>
        <div className="col-xs-3" style={ { marginBottom: -7 } }>
          <div className="row">
            {inputs.ggn}
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
          <strong>2. Voorkeursperiode (voor 1 oktober)​</strong>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-3">
          {inputs.start}
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
          {inputs.confirm}
        </div>
      </div>
    </div>
  );
};

const months = _.range(1, 13);

class GgapRegistrationForm extends Component {
  constructor(props) {
    super();
    this.state = {
      type: this.getType({ isGgnRequired: true }),
      isConfirmationAlert: false,
      value: {},
    };
  }

  getType({ isGgap }) {
    return t.struct({
      jaNeeHzpc: t.enums.of(['jaa', 'nee']),
      ggn: isGgap ? t.maybe(t.String) : t.String,
      start: isGgap ? t.enums.of(months) : t.maybe(t.enums.of(months)),
      confirm: isGgap ? t.Bool : t.maybe(t.Bool),
    }, 'Registration');
  }

  getValue() {
    return utils.format(this.refs.form.getValue());
  }

  handleChange(value) {
    const isGgap = this.isGgap(value);
    const type = this.getType({ isGgap });
    this.setState({ type, value });
  }

  handleSubmitClick() {
    const value = this.getValue();
    if (this.isGgap(value)) {
      if (!value.confirm) {
        this.setState({ isConfirmationAlert: true });
        return;
      }
    }
    this.setState({ isConfirmationAlert: false });
    this.props.onSubmit(value);
  }

  isGgap(value) {
    if (value) {
      return value.jaNeeHzpc === 'jaa';
    }
  }

  render() {
    const startOptiions = months.map(m => {
      return { value: m, text: moment(m, 'M').locale('nl').format('MMMM') };
    });
    return (
      <div className="GgapRegistrationForm">
        <Form
          ref="form"
          onChange={this.handleChange.bind(this)}
          type={this.state.type}
          value={this.state.value}
          options={{
            'fields': {
              'jaNeeHzpc': {
                'options': [
                  { 'value': 'jaa', 'text': 'Ja, ik neem deel aan het HZPC Zorgsysteem ' },
                  { 'value': 'nee', 'text': 'Nee, ik regel mijn certificering zelf, mijn GGN-nummer is:' },
                ],
                'factory': t.form.Radio,
                'label': '1. Wilt u deelnemen aan het HZPC Zorgsysteem​',
              },
              'start': {
                'label': ' ',
                'options': startOptiions,
                nullOption: { value: '', text: 'Selecteer maand' },
              },
              'ggn': {
                'label': ' ',
              },
              confirm: {
                hasError: this.state.isConfirmationAlert,
                label: <div>
                  Verklaart:
                  <ul style={{ paddingLeft: 15 }}>
                    <li>kennis genomen te hebben van het algemeen reglement van GlobalG.A.P.  en zich te houden aan de vereisten, vermeld in dit reglement o.a. het naleven van de GlobalG.A.P.-normen;</li>
                    <li>de controle op de naleving van de GlobalG.A.P. –normen door de onafhankelijke controleorganisatie ECAS B.V. toe te laten.</li>
                    <li>kennis genomen te hebben voor aardappelen en eventueel granen, uien en wortelen, deel te nemen aan het monitoringssysteem van HZPC en de volledige medewerking hieraan te verlenen.</li>
                  </ul>
                </div>,
              },
            },
            template: layout,
          }}
        />
        <div>
          <i>
          <strong>Disclaimer:</strong>
          <p>
            De door u verstrekte gegevens via HZPC bedrijfscertificering blijven ten alle tijden uw eigendom. Buiten de autorisatie middels het systeem, worden uw gegevens nooit zonder uw schriftelijke toestemming door HZPC / GreenlinQdata of AgriPlace aan derden verstrekt.
          </p>
          <p>
            De gegevens worden bij AgriPlace op goed beveiligde servers opgeslagen.
          </p>
          </i>
        </div>
        <br></br>
        <div className="text-center">
          {
            this.state.isConfirmationAlert
            ? <div className="alert alert-danger">Gelieve akkoord te gaan met de hierboven genoemde verklaring</div>
            : null
          }
          <Bootstrap.Button
            bsStyle="success"
            onClick={this.handleSubmitClick.bind(this)}
          >
            Verzenden
          </Bootstrap.Button>
        </div>
      </div>
    );
  }
}

module.exports = GgapRegistrationForm;
