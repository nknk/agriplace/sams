import React from 'react';
import permissionComponents from './PermissionComponents';
const Fluxxor = require('fluxxor')
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

export const PermissionController = React.createClass({
  propTypes: {
    assessmentUuid: React.PropTypes.string,
    workflowUuid: React.PropTypes.string,
    component: React.PropTypes.string.isRequired,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore', 'DashboardStore'),
  ],

  getStateFromFlux() {
    return {
      permissions: this.getPermissions(),
    };
  },

  // componentWillMount() {
  //  // This component accept only one children element
  //  if (React.isValidElement(this.props.children)) {
  //    console.log('This is not valid React element');
  //  }
  // },

  getPermissions() {
    const flux = this.getFlux();
    const permissionsAssessments = flux.store('AssessmentsStore').getPermissions(this.props.assessmentUuid);
    const permissionsDashboard = flux.store('DashboardStore').getPermissions(this.props.workflowUuid);
    return _.isEmpty(permissionsAssessments)
      ? permissionsDashboard
      : permissionsAssessments;
  },

  render() {
    const emptyNode = <span></span>;
    if (_.isEmpty(this.state.permissions)) {
      return emptyNode;
    }
    const name = permissionComponents[this.props.component];
    const permission = this.state.permissions.find(x => x.name === name) || {};
    const actions = permission.actions || [];

    let node = emptyNode;

    if (actions.length) {
      if (~actions.indexOf('READ')) {
        node = React.cloneElement(this.props.children, { readOnly: true, disabled: true });
      }
      if (~actions.indexOf('UPDATE') || ~actions.indexOf('CREATE') || ~actions.indexOf('DELETE')) {
        node = this.props.children;
      }
    }
    return node;
  },
});
