
require('./FormEditors.less');

module.exports = {
  FormCheckboxesEditor: require('./FormCheckboxesEditor.jsx'),
  FormMemoEditor: require('./FormMemoEditor.jsx'),
  FormRadioEditor: require('./FormRadioEditor.jsx'),
  FormRadioButtonsEditor: require('./FormRadioButtonsEditor.jsx'),
  FormTextEditor: require('./FormTextEditor.jsx'),
  FormSingleChoiceButtonsEditor: require('./FormSingleChoiceButtonsEditor.jsx'),
};
