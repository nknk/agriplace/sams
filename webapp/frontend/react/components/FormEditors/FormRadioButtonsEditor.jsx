import React from 'react';
var FormRadioButtonsEditor = React.createClass({

  propTypes: {
    name: React.PropTypes.string,
    possibleAnswers: React.PropTypes.array,
    readOnly: React.PropTypes.bool,
    value: React.PropTypes.string,
    onChange: React.PropTypes.func,
  },


  //
  // Handlers
  //

  onRadioChange(possibleAnswer) {
    // make it impossible to deselect the button
    if (possibleAnswer.value !== this.props.value)
      this.props.onChange(possibleAnswer.value);
  },


  getPossibleAnswers() {
    return _.map(this.props.possibleAnswers, (possibleAnswer) => {
      return (
        <div
          onClick = { this.onRadioChange.bind(null, possibleAnswer) }
        >
          <Bootstrap.Input
            type = "radio"
            key = { possibleAnswer.uuid }
            disabled = { this.props.readOnly }
            checked = { possibleAnswer.value == this.props.value }
            onChange = { tools.nop }
            name = { this.props.name }
          >
            { possibleAnswer.text }
          </Bootstrap.Input>
        </div>
      );
    });
  },


  render() {

    // TODO Content translation

    return (
      <div className="form-editor form-radio-buttons-editor">
        <Bootstrap.ButtonToolbar className="form-radio-buttons">
          { this.getPossibleAnswers() }
        </Bootstrap.ButtonToolbar>
      </div>
    );
  },

});

module.exports = FormRadioButtonsEditor;

