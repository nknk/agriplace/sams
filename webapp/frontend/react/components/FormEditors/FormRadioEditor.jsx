import React from 'react';


//
// Form Radio Editor
//
var FormRadioEditor = React.createClass({

  propTypes: {
    name: React.PropTypes.string,
    possibleAnswers: React.PropTypes.array,
    value: React.PropTypes.string,
    onChange: React.PropTypes.func,
  },

  //
  //
  //

  // //
  // // Determines whether the given possibleAnswer should be checked
  // //
  // _isChecked(possibleAnswer) {
  //  return this.props.value == possibleAnswer.value;
  // },

  //
  // Handlers
  //

  _onRadioChange(possibleAnswer) {
    this.props.onChange(possibleAnswer.value);
  },

  //
  //
  //
  render() {

    var possibleAnswers = this.props.possibleAnswers || [];

    return (

      <div className="form-editor form-radio-editor">
        <div className="form-radio-editor">
          {
            possibleAnswers.map((possibleAnswer) => {

              return (
                <div key={possibleAnswer.uuid}>
                  <div className="radio">
                    <label>

                      <input type="radio"
                        checked={this.props.value == possibleAnswer.value}
                        disabled={this.props.readOnly}
                        name={this.props.name}
                        value={possibleAnswer.value}
                        onChange={this._onRadioChange.bind(null, possibleAnswer)}
                      />

                      { possibleAnswer.text }

                    </label>
                  </div>
                </div>
              );

            })
          }
        </div>

      </div>

    );
  },

});

module.exports = FormRadioEditor;

