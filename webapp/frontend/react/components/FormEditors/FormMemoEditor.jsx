

var React = require('react');

//
// Form Memo Editor
//
var FormMemoEditor = React.createClass({

  propTypes: {
    name: React.PropTypes.string,
    value: React.PropTypes.string,
    onChange: React.PropTypes.func,
  },

  getInitialState() {
    return {
      value: this.props.value || '',
    };
  },


  _onChange(event) {
    this.setState({
      value: event.target.value,
    });
  },

  _onBlur(event) {
    if (this.state.value != this.props.value) {
      this.props.onChange(this.state.value);
    }
  },


  //
  // Render
  //
  render() {
    return (
      <div className="form-editor form-text-editor">
        <textarea
          className="form-control"
          disabled={this.props.readOnly}
          name={this.props.name}
          value={this.state.value}
          onChange={this._onChange}
          onBlur={this._onBlur}
        />
      </div>
    );
  },

});

module.exports = FormMemoEditor;
