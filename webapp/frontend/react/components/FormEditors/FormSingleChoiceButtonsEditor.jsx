import React from 'react';


//
// Form Radio Editor
//
var FormSingleChoiceButtonsEditor = React.createClass({

  propTypes: {
    name: React.PropTypes.string,
    possibleAnswers: React.PropTypes.array,
    readOnly: React.PropTypes.bool,
    value: React.PropTypes.string,
    onChange: React.PropTypes.func,
  },

  //
  // Handlers
  //

  _onRadioChange(possibleAnswer) {
    var newValue = possibleAnswer.value !== this.props.value ? possibleAnswer.value : '';
    this.props.onChange(newValue);
  },

  //
  //
  //
  render() {

    var possibleAnswers = this.props.possibleAnswers || [];

    return (

      <div className="form-editor form-single-choice-buttons-editor">
        <Bootstrap.ButtonToolbar className="form-single-choice-buttons">
          {
            possibleAnswers.map((possibleAnswer) => {

              return (
                  <Bootstrap.Button
                    key={possibleAnswer.uuid}
                    className="btn-option"
                    disabled={this.props.readOnly}
                    value={possibleAnswer.value}
                    name={this.props.name}
                    onClick={this._onRadioChange.bind(null, possibleAnswer)}
                    active={this.props.value == possibleAnswer.value}
                  >
                    { possibleAnswer.text }
                  </Bootstrap.Button>
              );

            })
          }
        </Bootstrap.ButtonToolbar>

      </div>

    );
  },

});

module.exports = FormSingleChoiceButtonsEditor;

