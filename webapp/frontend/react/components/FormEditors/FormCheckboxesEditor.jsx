import React from 'react';


//
// Form Checkbox Editor
//
var FormCheckboxesEditor = React.createClass({

  propTypes: {
    name: React.PropTypes.string,
    possibleAnswers: React.PropTypes.array,
    value: React.PropTypes.string,
    onChange: React.PropTypes.func,
  },

  //
  // Handlers
  //

  _onCheckboxChange(possibleAnswer, event) {
    var selectedValues = this.props.value.split('^');
    var changed = {
      value: possibleAnswer.value,
      isChecked: event.target.checked,
    };
    if (changed.isChecked) {
      // checked
      selectedValues.push(changed.value);
    } else {
      // unchecked
      _.remove(selectedValues, (value) => value == changed.value);
    }

    // update value and send it upstream
    selectedValues.sort();
    var newValue = selectedValues.join('^');
    this.props.onChange(newValue);

  },

  //
  // Determines whether the given possibleAnswer should be checked
  //
  _isChecked(possibleAnswer) {
    var selectedValues = this.props.value.split('^');
    return !!(_.find(selectedValues, (selectedValue) => selectedValue === possibleAnswer.value));
  },

  render() {

    var possibleAnswers = this.props.possibleAnswers || [];

    return (

      <div className="form-editor form-text-editor">
        <div className="form-checkbox-editor">
          {
            possibleAnswers.map((possibleAnswer) => {

              var checked = this._isChecked(possibleAnswer);

              return (
                <div key={possibleAnswer.value}>
                  <div className="checkbox">
                    <label>

                      <input type="checkbox"
                        disabled={this.props.readOnly}
                        name={this.props.name}
                        value={possibleAnswer.value}
                        onChange={this._onCheckboxChange.bind(this, possibleAnswer)}
                        checked={this._isChecked(possibleAnswer)}
                      />

                      { possibleAnswer.text }

                    </label>
                  </div>
                </div>
              );

            })
          }
        </div>

      </div>

    );
  },

});

module.exports = FormCheckboxesEditor;

