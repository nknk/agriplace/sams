import React, { PropTypes } from 'react';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import './AssessmentReview.less';

export default class InternalRemarksPrint extends React.Component {

  render() {
    const { remarks, assessmentUuid } = this.props;
    return (
      <PermissionController assessmentUuid={assessmentUuid} component="InternalRemarks">
        <div>
          <div className="internal-remarks-header">{_t('Internal remarks')}</div>
          <div className="remarks">
            {remarks}
          </div>
        </div>
      </PermissionController>
    );
  }
}

InternalRemarksPrint.propTypes = {
  remarks: PropTypes.string,
  assessmentUuid: PropTypes.string,
};
