import React, { PropTypes } from 'react';
import './AssessmentReview.less';
import OverviewPrintTable from './OverviewPrintTable.jsx';
import FindingsPrintTable from './FindingsPrintTable.jsx';
import OtherRemarksPrint from './OtherRemarksPrint.jsx';
import InternalRemarksPrint from './InternalRemarksPrint.jsx';

export default class AssessmentReviewPrint extends React.Component {

  render() {
    const { assessment, review, questions, otherRemarks, internalRemarks } = this.props;
    const assessmentUuid = assessment && assessment.uuid;
    const judgements = (review && review.judgements) || [];
    return (
      <div className="assessment-review-app visible-print">
        <OverviewPrintTable review={review} assessmentUuid={assessmentUuid} />
        <FindingsPrintTable judgements={judgements} assessment={assessment} questions={questions} />
        <OtherRemarksPrint remarks={otherRemarks} assessmentUuid={assessmentUuid} />
        <InternalRemarksPrint remarks={internalRemarks} assessmentUuid={assessmentUuid} />
      </div>
    );
  }
}

AssessmentReviewPrint.propTypes = {
  review: PropTypes.object,
  assessment: PropTypes.object,
  questions: PropTypes.object,
  otherRemarks: PropTypes.string,
  internalRemarks: PropTypes.string,
};
