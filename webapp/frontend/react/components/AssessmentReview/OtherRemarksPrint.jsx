import React, { PropTypes } from 'react';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import './AssessmentReview.less';

export default class OtherRemarksPrint extends React.Component {

  render() {
    const { remarks, assessmentUuid } = this.props;
    return (
      <PermissionController assessmentUuid={assessmentUuid} component="OtherRemarks">
        <div>
          <div className="other-remarks-header">{_t('Other remarks')}</div>
          <div className="remarks">
            {remarks}
          </div>
        </div>
      </PermissionController>
    );
  }
}

OtherRemarksPrint.propTypes = {
  remarks: PropTypes.string,
  assessmentUuid: PropTypes.string,
};
