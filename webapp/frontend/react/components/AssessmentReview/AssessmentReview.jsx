import React, { PropTypes } from 'react';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import { DatetimePicker } from '../DatetimePicker/DatetimePicker.jsx';
const moment = require('moment');
const membershipUuid = require('../Utils/pageData.js').context.membershipUuid;
import AssessmentReviewPrint from './AssessmentReviewPrint.jsx';

require('./AssessmentReview.less');

const DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss';

export class AssessmentReview extends React.Component {

  constructor(props) {
    super(props);
    const { review } = props;
    this.state = {
      internalDescription: review.internal_description,
      overviewDescription: review.overview_description,
      plannedAuditDate: review.audit_date_planned && moment(review.audit_date_planned, DATE_FORMAT),
      auditStartDate: review.audit_date_actual_start && moment(review.audit_date_actual_start, DATE_FORMAT),
      auditEndDate: review.audit_date_actual_end && moment(review.audit_date_actual_end, DATE_FORMAT),
    };
    this.handleInternalDescriptionChange = this.handleInternalDescriptionChange.bind(this);
    this.handleOverviewDescriptionChange = this.handleOverviewDescriptionChange.bind(this);
    this.handleInternalDescriptionClick = this.handleInternalDescriptionClick.bind(this);
    this.handleOverviewDescriptionClick = this.handleOverviewDescriptionClick.bind(this);

    this.handlePlannedAuditDateTimeChange = this.handlePlannedAuditDateTimeChange.bind(this);
    this.handleAuditStartDateTimeChange = this.handleAuditStartDateTimeChange.bind(this);
    this.handleAuditEndDateTimeChange = this.handleAuditEndDateTimeChange.bind(this);
  }

  sortJudgements({ judgements, questions, assessment }) {
    return judgements.sort((a, b) => {
      const questionnaireA = assessment.assessmentType.getQuestionnaire(a.question);
      const questionnaireB = assessment.assessmentType.getQuestionnaire(b.question);

      if (questionnaireA && questionnaireB) {
        const questionnaireIndexA = questionnaireA.order_index;
        const questionnaireIndexB = questionnaireB.order_index;
        const questionIndexA = questions[a.question].order_index;
        const questionIndexB = questions[b.question].order_index;

        return questionnaireIndexA - questionnaireIndexB || questionIndexA - questionIndexB;
      }

      return 0;
    });
  }

  getTable({ judgements, questions, assessment }) {
    judgements = this.sortJudgements({ judgements, questions, assessment });
    return (
      <table className="table review">
        <thead>
          <tr className="row">
            <th>{_t('Questionnaire')}</th>
            <th>{_t('Findings')}</th>
            <PermissionController assessmentUuid={assessment.uuid} component="EvaluationActions">
              <th>{_t('Solved?')}​</th>
            </PermissionController>
            <PermissionController assessmentUuid={assessment.uuid} component="EvaluationActions">
              <th>{_t('Remarks')}</th>
            </PermissionController>

          </tr>
        </thead>
        <tbody>
          {
            judgements.map(judgement => {
              const question = questions[judgement.question];
              if (!question) {
                return null;
              }

              const questionnaire = assessment.assessmentType.getQuestionnaire(question.uuid);
              if (!questionnaire) {
                return null;
              }

              const shortQuestionCode = tools.removePrefixFromString(question.code, '-');
              const assessmentUuid = assessment.assessment.uuid;

              let questionLink = `/${membershipUuid}/our/assessments/${assessmentUuid}/`;
              questionLink = `${questionLink}questionnaires/${questionnaire.uuid}/edit#${question.uuid}`;

              return (
                <tr className="row">
                  <td className="col-xs-2">
                    <a href={questionLink}>
                      {`${questionnaire.code}${shortQuestionCode}`}&nbsp;
                      {`(${assessment.assessmentType.getQuestionLevel(question.level_object).title})`}
                    </a>
                  </td>
                  <td className="col-xs-4 justification">{judgement.justification}</td>
                  <PermissionController assessmentUuid={assessmentUuid} component="EvaluationActions">
                    <td className="col-xs-4">
                      <PermissionController assessmentUuid={assessmentUuid} component="EvaluationActions">
                        <Bootstrap.Input
                          type="checkbox"
                          defaultChecked={judgement.evaluation_is_solved}
                          onChange={this.handleIsSolvedClick.bind(this, judgement)}
                        />
                      </PermissionController>
                    </td>
                  </PermissionController>
                  <PermissionController assessmentUuid={assessmentUuid} component="EvaluationActions">
                    <td className="col-xs-2">
                      <PermissionController assessmentUuid={assessmentUuid} component="EvaluationActions">
                        <textarea
                          defaultValue={judgement.evaluation_remarks}
                          onChange={this.handleRemarksChanged.bind(this, judgement)}
                        />
                      </PermissionController>
                    </td>
                  </PermissionController>
                </tr>
              );
            })
          }
        </tbody>
      </table>
    );
  }

  handleIsSolvedClick(judgement, e) {
    judgement.evaluation_is_solved = e.target.checked;
    this.props.onJudgementChange({ assessmentUuid: this.props.assessment.uuid, judgement });
  }

  handleRemarksChanged(judgement, e) {
    judgement.evaluation_remarks = e.target.value;
    this.props.onJudgementChange({ assessmentUuid: this.props.assessment.uuid, judgement });
  }

  handleInternalDescriptionChange({ target: { value } }) {
    this.setState({ internalDescription: value });
  }

  handleInternalDescriptionClick() {
    this.props.onInternalDescriptionChange(this.state.internalDescription);
  }

  handleOverviewDescriptionChange({ target: { value } }) {
    this.setState({ overviewDescription: value });
  }

  handleOverviewDescriptionClick() {
    this.props.onOverviewDescriptionChange(this.state.overviewDescription);
  }

  handlePlannedAuditDateTimeChange(plannedAuditDate) {
    this.setState({ plannedAuditDate });
    this.props.onPlannedAuditDateChange(moment.isMoment(plannedAuditDate)
      && plannedAuditDate.format(DATE_FORMAT));
  }

  handleAuditStartDateTimeChange(auditStartDate) {
    this.setState({ auditStartDate });
    this.props.onAuditStartDateChange(moment.isMoment(auditStartDate)
      && auditStartDate.format(DATE_FORMAT));
  }

  handleAuditEndDateTimeChange(auditEndDate) {
    this.setState({ auditEndDate });
    this.props.onAuditEndDateChange(moment.isMoment(auditEndDate)
      && auditEndDate.format(DATE_FORMAT));
  }

  handleDecisionChange(event) {
    this.props.onDecisionChange(event.target.value);
  }

  handleReauditChange(event) {
    this.props.onReauditChange(event.target.checked);
  }

  render() {
    const { assessment, review } = this.props;
    const { questions } = assessment.assessmentType;
    const assessmentState = assessment.assessment.state_code;
    const assessmentUuid = assessment.uuid;
    return (
      <div className="AssessmentReview">
        <table className="table table-striped hidden-print">
          <tbody>
            <tr>
              <td>{_t('Grower')}:</td>
              <td>{review.grower_organization_name}</td>
            </tr>
            <tr>
              <td>{_t('Internal inspector')}:</td>
              <td>​{review.auditor_user.name}</td>
            </tr>
            <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
              <tr>
                <td>{_t('Planned inspection date/time')}:</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
                    <DatetimePicker
                      onChange={this.handlePlannedAuditDateTimeChange}
                      value={review.audit_date_planned && moment(review.audit_date_planned).format('DD-MM-YYYY HH:mm')}
                      dateFormat="DD-MM-YYYY" timeFormat="HH:mm"
                      isValid={review.audit_date_planned && moment(review.audit_date_planned).isValid()}
                      isOptional
                      disabled
                    />
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
            <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
              <tr>
                <td>{_t('Inspection start date/time')}:</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
                    <DatetimePicker
                      onChange={this.handleAuditStartDateTimeChange}
                      value={
                        review.audit_date_actual_start
                         && moment(review.audit_date_actual_start).format('DD-MM-YYYY HH:mm')
                      }
                      dateFormat="DD-MM-YYYY" timeFormat="HH:mm"
                      isValid={review.audit_date_actual_start && moment(review.audit_date_actual_start).isValid()}
                      endDate={this.state.auditEndDate}
                      disabled={assessmentState !== 'document_review_done'}
                    />
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
            <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
              <tr>
                <td>{_t('Inspection end date/time')}:</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
                    <DatetimePicker
                      onChange={this.handleAuditEndDateTimeChange}
                      value={
                        review.audit_date_actual_end
                          && moment(review.audit_date_actual_end).format('DD-MM-YYYY HH:mm')
                      }
                      dateFormat="DD-MM-YYYY" timeFormat="HH:mm"
                      isValid={review.audit_date_actual_end && moment(review.audit_date_actual_end).isValid()}
                      startDate={this.state.auditStartDate}
                      disabled={assessmentState !== 'document_review_done'}
                    />
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
            <PermissionController assessmentUuid={assessmentUuid} component="AuditorDecision">
              <tr>
                <td>{_t('Judgement')}:</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="AuditorDecision">
                    <Bootstrap.Input
                      type="select"
                      groupClassName="decision"
                      defaultValue={review.audit_results.decision}
                      onChange={this.handleDecisionChange.bind(this)}
                    >
                      {
                        review && review.possible_decision_list.map(item => (
                          <option value={item[0]}>{item[1]}</option>
                        ))
                      }
                    </Bootstrap.Input>
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
            <PermissionController assessmentUuid={assessmentUuid} component="PhysicalReaudit">
              <tr>
                <td>{_t('Physical reaudit required')}:</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="PhysicalReaudit">
                    <Bootstrap.Input
                      type="checkbox"
                      groupClassName="checkbox"
                      defaultChecked={review.audit_results.reaudit}
                      onChange={this.handleReauditChange.bind(this)}
                    />
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
            <tr>
              <td>{_t('Findings')}:</td>
              <td>{this.getTable({ judgements: review.judgements, questions, assessment })}</td>
            </tr>
            <PermissionController assessmentUuid={assessmentUuid} component="OtherRemarks">
              <tr>
                <td>{_t('Other remarks')}:​</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="OtherRemarks">
                    <textarea
                      defaultValue={this.state.overviewDescription}
                      onChange={this.handleOverviewDescriptionChange}
                    />
                  </PermissionController>
                  <PermissionController assessmentUuid={assessmentUuid} component="OtherRemarksSave">
                    <Bootstrap.Button
                      type="button"
                      bsStyle="default"
                      data-dismiss="modal"
                      onClick={this.handleOverviewDescriptionClick}
                    >
                      <i className="fa fa-floppy-o" /> {_t('Save remarks')}
                    </Bootstrap.Button>
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
            <PermissionController assessmentUuid={assessmentUuid} component="InternalRemarks">
              <tr>
                <td>{_t('Internal remarks')}:​</td>
                <td>
                  <PermissionController assessmentUuid={assessmentUuid} component="InternalRemarks">
                    <textarea
                      defaultValue={this.state.internalDescription}
                      onChange={this.handleInternalDescriptionChange}
                    />
                  </PermissionController>
                  <PermissionController assessmentUuid={assessmentUuid} component="InternalRemarks">
                    <Bootstrap.Button
                      type="button"
                      bsStyle="default"
                      data-dismiss="modal"
                      onClick={this.handleInternalDescriptionClick}
                    >
                      <i className="fa fa-floppy-o" /> {_t('Save remarks')}
                    </Bootstrap.Button>
                  </PermissionController>
                </td>
              </tr>
            </PermissionController>
          </tbody>
        </table>
        <AssessmentReviewPrint
          assessment={assessment}
          review={review}
          questions={questions}
          otherRemarks={this.state.overviewDescription}
          internalRemarks={this.state.internalDescription}
        />
      </div>
    );
  }
}

AssessmentReview.propTypes = {
  review: PropTypes.object,
  assessment: PropTypes.object,
  onJudgementChange: PropTypes.func,
  onInternalDescriptionChange: PropTypes.func,
  onOverviewDescriptionChange: PropTypes.func,
  onPlannedAuditDateChange: PropTypes.func,
  onAuditStartDateChange: PropTypes.func,
  onAuditEndDateChange: PropTypes.func,
  onDecisionChange: PropTypes.func,
  onReauditChange: PropTypes.func,
};
