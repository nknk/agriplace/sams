import React, { PropTypes } from 'react';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import moment from 'moment';
import './AssessmentReview.less';
import { Table, Input } from 'react-bootstrap';

export default class OverviewPrintTable extends React.Component {

  render() {
    const { assessmentUuid, review } = this.props;
    const decisionKey = (review && review.audit_results.decision);
    const possibleDecisions = (review && review.possible_decision_list) || [];
    const decisionArray = _.find(possibleDecisions, d => ((d && d[0]) === decisionKey));
    const decision = decisionArray && decisionArray[1];

    return (
      <Table condensed striped className="table-outline borderless overview-table">
        <tbody>
          <tr>
            <td>{_t('Grower')}:</td>
            <td>{review && review.grower_organization_name}</td>
          </tr>
          <tr>
            <td>{_t('Internal inspector')}:</td>
            <td>​{review && review.auditor_user.name}</td>
          </tr>
          <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
            <tr>
              <td>{_t('Planned inspection date/time')}:</td>
              <td>
                {review && review.audit_date_planned
                  && moment(review.audit_date_planned).format('DD-MM-YYYY HH:mm')}
              </td>
            </tr>
          </PermissionController>
          <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
            <tr>
              <td>{_t('Inspection start date/time')}:</td>
              <td>
                {review && review.audit_date_actual_start
                  && moment(review.audit_date_actual_start).format('DD-MM-YYYY HH:mm')}
              </td>
            </tr>
          </PermissionController>
          <PermissionController assessmentUuid={assessmentUuid} component="AuditDate">
            <tr>
              <td>{_t('Inspection end date/time')}:</td>
              <td>
                {review && review.audit_date_actual_end
                  && moment(review.audit_date_actual_end).format('DD-MM-YYYY HH:mm')}
              </td>
            </tr>
          </PermissionController>
          <PermissionController assessmentUuid={assessmentUuid} component="AuditorDecision">
            <tr>
              <td>{_t('Judgement')}:</td>
              <td>
                {decision}
              </td>
            </tr>
          </PermissionController>
          <PermissionController assessmentUuid={assessmentUuid} component="PhysicalReaudit">
            <tr>
              <td>{_t('Physical reaudit required')}:</td>
              <td>
                <Input
                  type="checkbox"
                  checked={review && review.audit_results.reaudit}
                />
              </td>
            </tr>
          </PermissionController>
        </tbody>
      </Table>
    );
  }
}

OverviewPrintTable.propTypes = {
  review: PropTypes.object,
  assessmentUuid: PropTypes.string,
};
