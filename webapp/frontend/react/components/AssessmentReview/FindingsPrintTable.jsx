import React, { PropTypes } from 'react';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import pageData from '../Utils/pageData';
const { membershipUuid } = pageData.context;
import './AssessmentReview.less';
import { Table, Input } from 'react-bootstrap';

export default class FindingsPrintTable extends React.Component {

  sortJudgements({ judgements, questions, assessment }) {
    return judgements.sort((a, b) => {
      const questionnaireA = assessment.assessmentType.getQuestionnaire(a.question);
      const questionnaireB = assessment.assessmentType.getQuestionnaire(b.question);

      if (questionnaireA && questionnaireB) {
        const questionnaireIndexA = questionnaireA.order_index;
        const questionnaireIndexB = questionnaireB.order_index;
        const questionIndexA = questions[a.question].order_index;
        const questionIndexB = questions[b.question].order_index;

        return questionnaireIndexA - questionnaireIndexB || questionIndexA - questionIndexB;
      }

      return 0;
    });
  }

  render() {
    const { judgements, assessment, questions } = this.props;
    const sortedJudgements = this.sortJudgements({ judgements, questions, assessment });
    const assessmentUuid = assessment.assessment.uuid;
    return (
      <div>
        <div className="findings-header">
          {_t('Findings')}
        </div>
        <Table condensed striped className="table-outline borderless findings-table">
          <thead>
            <tr>
              <th>{_t('Questionnaire')}</th>
              <th>{_t('Findings')}</th>
              <PermissionController assessmentUuid={assessment.uuid} component="EvaluationActions">
                <th>{_t('Solved?')}​</th>
              </PermissionController>
              <PermissionController assessmentUuid={assessment.uuid} component="EvaluationActions">
                <th>{_t('Remarks')}</th>
              </PermissionController>
            </tr>
          </thead>
          <tbody>
          {
            sortedJudgements.map((judgement, index) => {
              const question = questions[judgement.question];
              const questionnaire = question && question.uuid
                && assessment.assessmentType.getQuestionnaire(question.uuid);
              if (!question || !questionnaire) {
                return null;
              }

              const shortQuestionCode = tools.removePrefixFromString(question.code, '-');
              let questionLink = `/${membershipUuid}/our/assessments/${assessmentUuid}/`;
              questionLink = `${questionLink}questionnaires/${questionnaire.uuid}/edit#${question.uuid}`;

              return (
                <tr key={`judgement-finding-${index}`}>
                  <td>
                    <a href={questionLink}>
                      {`${questionnaire.code}${shortQuestionCode}`}&nbsp;
                      {`(${assessment.assessmentType.getQuestionLevel(question.level_object).title})`}
                    </a>
                  </td>
                  <td className="justification">{judgement.justification}</td>
                  <PermissionController assessmentUuid={assessmentUuid} component="EvaluationActions">
                    <td>
                      <div className="evaluation_is_solved">
                        <Input
                          type="checkbox"
                          checked={judgement.evaluation_is_solved}
                        />
                      </div>
                    </td>
                  </PermissionController>
                  <PermissionController assessmentUuid={assessmentUuid} component="EvaluationActions">
                    <td className="evaluation_remarks">
                      {judgement.evaluation_remarks}
                    </td>
                  </PermissionController>
                </tr>
              );
            })
          }
          </tbody>
        </Table>
      </div>
    );
  }
}

FindingsPrintTable.propTypes = {
  judgements: PropTypes.array,
  assessment: PropTypes.object,
  questions: PropTypes.object,
};
