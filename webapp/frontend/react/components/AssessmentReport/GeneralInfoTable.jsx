import React from 'react';
import { Table } from 'react-bootstrap';

export class GeneralInfoTable extends React.Component {

  render() {
    const { info } = this.props;

    return (
      <div className="general-information">
        <label className="table-heading">{_t('General information')}</label>
        <Table condensed striped className="table-outline borderless general-info-table">
          <tbody>
            <tr>
              <td><strong>{`${_t('Organization name')}:`}</strong></td>
              <td>{info.organization}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Farm group')}:`}</strong></td>
              <td>{info.farm_group}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Farm group membership number')}:`}</strong></td>
              <td>{info.membership_number}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('GGN')}:`}</strong></td>
              <td>{info.ggn_number}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Date created')}:`}</strong></td>
              <td>
                {
                  info.date_created && (
                    `${info.date_created} ${_t('by')} ${info.date_created_by}`
                  )
                }
              </td>
            </tr>
            <tr>
              <td><strong>{`${_t('Date shared')}:`}</strong></td>
              <td>
                {
                  info.date_shared && (
                    `${info.date_shared} ${_t('by')} ${info.date_shared_by}`
                  )
                }
              </td>
            </tr>
            <tr>
              <td><strong>{`${_t('Crops')}:`}</strong></td>
              <td>
                {
                  info.crops.map(crop => (
                    <div>{`${crop.name} (${crop.area} ha)`}</div>
                  ))
                }
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }

}

GeneralInfoTable.propTypes = {
  info: React.PropTypes.object,
};
