import React from 'react';
import { Table } from 'react-bootstrap';
import './AssessmentReport.less';

export class EvaluationResultsTable extends React.Component {

  getPercentageTemplate(percentage) {
    return (
      percentage.value !== '' && (percentage.is_positive ? (
        <div className="positive-compliance-percentage">
          {percentage.value}
          {'%'}
        </div>
      ) : (
        <div className="negative-compliance-percentage">
          {percentage.value}
          {'%'}
          <icon className="fa fa-exclamation-triangle fa-lg warning-icon" />
        </div>
      ))
    );
  }

  render() {
    const { results } = this.props;
    return (
      <div className="evaluation-results">
        <label className="table-heading">{_t('Results')}</label>
        <Table condensed striped className="table-outline borderless evaluation-results-table">
          <thead>
            <tr>
              <td />
              <td><strong>{`${_t('Major')}`}</strong></td>
              <td><strong>{`${_t('Minor')}`}</strong></td>
              <td><strong>{`${_t('Recommended')}`}</strong></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><strong>{`${_t('Total number of control points')}`}</strong></td>
              <td>{results.total_control_points.major}</td>
              <td>{results.total_control_points.minor}</td>
              <td>{results.total_control_points.recommended}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Evaluated control points')}`}</strong></td>
              <td>{results.evaluated_control_points.major}</td>
              <td>{results.evaluated_control_points.minor}</td>
              <td>{results.evaluated_control_points.recommended}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Not applicable')}`}</strong></td>
              <td>{results.not_applicable.major}</td>
              <td>{results.not_applicable.minor}</td>
              <td>{results.not_applicable.recommended}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Shortcomings allowed')}`}</strong></td>
              <td>{results.short_comings_allowed.major}</td>
              <td>{results.short_comings_allowed.minor}</td>
              <td>{results.short_comings_allowed.recommended}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Shortcomings')}`}</strong></td>
              <td>{results.short_comings.major}</td>
              <td>{results.short_comings.minor}</td>
              <td>{results.short_comings.recommended}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Compliance %')}`}</strong></td>
              <td>{this.getPercentageTemplate(results.compliance_percentage.major)}</td>
              <td>{this.getPercentageTemplate(results.compliance_percentage.minor)}</td>
              <td>{this.getPercentageTemplate(results.compliance_percentage.recommended)}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }

}

EvaluationResultsTable.propTypes = {
  results: React.PropTypes.object,
};
