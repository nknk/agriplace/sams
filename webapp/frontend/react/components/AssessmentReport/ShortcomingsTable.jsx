import React from 'react';
import { Table } from 'react-bootstrap';
import _ from 'lodash';

export class ShortcomingsTable extends React.Component {

  breakCode(code) {
    let result = code;
    if (code.indexOf('-q-') !== -1) {
      const codeArray = code.split('-');
      _.forEach(codeArray, (n, key) => {
        if (n === 'q' && codeArray[key + 1]) {
          result = `${codeArray[key - 1]} ${codeArray[key + 1]}`;
        }
      });
    }
    return result;
  }

  render() {
    const { shortcomings } = this.props;

    if (!(shortcomings && shortcomings.length)) {
      return null;
    }

    return (
      <div className="shortcomings">
        <label className="table-heading">{_t('Summary of shortcomings')}</label>
        <Table condensed striped className="table-outline borderless shortcomings-table">
          <tbody>
            <tr>
              <td><strong>{`${_t('Control point (level)')}`}</strong></td>
              <td><strong>{`${_t('Description of shortcoming')}`}</strong></td>
            </tr>
            {
              shortcomings.map(shortcoming => (
                <tr>
                  <td>
                    {
                      `${this.breakCode(shortcoming.code)} (${shortcoming.level})`
                    }
                  </td>
                  <td>{shortcoming.description}</td>
                </tr>
              ))
            }
          </tbody>
        </Table>
      </div>
    );
  }

}

ShortcomingsTable.propTypes = {
  shortcomings: React.PropTypes.object,
};
