require('./fileuploader.less');


var UploadFileButton = React.createClass({

  propTypes: {
    title: React.PropTypes.string,
    onFileAttached: React.PropTypes.func,
  },

  handleFile(event) {
    var that = this;
    var reader = new FileReader();
    var file = event.target.files[0];

          // todo: move onload to componentDidMount and remove the listener in componentWillUnmount
          // to prevent leaks and so..
    reader.onload = function (upload) {
      that.props.onFileAttached(upload.target.result);
    };
    reader.readAsDataURL(file);
  },

  render() {
    return (
              <div className="fileUpload btn btn-primary">
                  <i className="fa fa-cloud-upload" />
                  <span>{ '  ' + this.props.title }</span>
                  <input type="file" className="upload" onChange={this.handleFile} />
              </div>
          );
  },
});

module.exports = UploadFileButton;
