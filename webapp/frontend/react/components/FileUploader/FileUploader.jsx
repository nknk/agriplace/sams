

module.exports = React.createClass({

  getInitialState() {
    this.reader = new FileReader();
    return {
      data_uri: null,
    };
  },

  setDataUri(upload) {
    this.setState({ data_uri: upload.target.result });
  },

  componentDidMount() {
    this.reader.addEventListener('load', this.setDataUri);
  },

  componentWillUnmount() {
    this.reader.removeEventListener('load', this.setDataUri);
  },

  handleSubmit(event) {
    event.preventDefault();
  },

  handleFile(event) {
    var file = event.target.files[0];
    this.reader.readAsDataURL(file);
  },


  render() {
    return (
      <form
        onSubmit= { this.handleSubmit }
        encType = "multipart/form-data"
      >
          <input
            type = "file"
            onChange= { this.handleFile }
          />
      </form>
    );
  },
});

