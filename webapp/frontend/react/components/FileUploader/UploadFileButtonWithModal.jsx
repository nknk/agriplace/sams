

const FileUploadModal = require('../Attachments/Modals/Attach/FileUploadModal.jsx');

require('./fileuploader.less');


const UploadFileButtonWithModal = React.createClass({

  propTypes: {
    title: React.PropTypes.string,
    onFileAttached: React.PropTypes.func,
    onSuccess: React.PropTypes.func,
    buttonText: React.PropTypes.string,
    buttonIcon: React.PropTypes.string,
  },

  getInitialState() {
    return {
      isFileUploadModalVisible: false,
    };
  },

  //
  // Handlers
  //

  onFileModalSuccess(files, validityType, date) {
    this.props.onSuccess(files, validityType, date);
    this.hideFileModal();
  },

  onFileModalCancel() {
    this.hideFileModal();
  },

  hideFileModal() {
    this.setState({
      isFileUploadModalVisible: false,
    });
  },

  showFileModal() {
    this.setState({
      isFileUploadModalVisible: true,
    });
  },

  //
  //
  //

  render() {
    const buttonText = this.props.buttonText ? this.props.buttonText : _t('Upload file');
    const buttonIcon = this.props.buttonIcon ? this.props.buttonIcon : 'upload';
    const icon = `icon fa fa-${buttonIcon}`;
    return (
      <div>

        <Bootstrap.Button
          onClick={this.showFileModal}
        >
          <span className={icon}></span>&nbsp;
          {buttonText}
        </Bootstrap.Button>

        <FileUploadModal
          isVisible={this.state.isFileUploadModalVisible}
          onSuccess={this.onFileModalSuccess}
          onCancel={this.onFileModalCancel}
        />

      </div>
    );
  },

});

module.exports = UploadFileButtonWithModal;

