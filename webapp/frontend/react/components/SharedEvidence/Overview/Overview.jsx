import React from 'react';
const DataTable = require('../../DataTable/DataTable.jsx');
const BusyFeedback = require('../../BusyFeedback');
const pageData = require('../../Utils/pageData.js');

function renderOrganizationName(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return (
    <div
      onClick={(e) => cellData.clicked(cellData.uuid, e)}
      className={'hover-archive-table'}
    >
      {cellData.name}
    </div>
  );
}


function getRemarks(remark) {
  let remarsText = "";
  if (remark) {
    remarsText = _t("The following files could not be processed") + ": " + remark +" . " + _t("It is possible that source file is no longer available") + ". " + _t("If this problem persists then please contact ") + "support@agriplace.com."
  }
  return remarsText
}

function renderRemarks(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return getRemarks(cellData)
}

const columnSettings = [
  {
    dataKey: 'organization_name',
    label: _t('Organization'),
    width: 300,
    flexGrow: 2,
    cellRenderer: renderOrganizationName,
    cellDataGetter: (key, item) => item[key],
  }, {
    dataKey: 'sharing_date',
    label: _t('Sharing date'),
    width: 250,
    flexGrow: 1,
  }, {
    dataKey: 'state',
    label: _t('State'),
    width: 150,
    flexGrow: 1,
    cellDataGetter: (key, item) => item[key],
  },{
    dataKey: 'remarks',
    label: _t('Remarks'),
    width: 200,
    flexGrow: 1,
    cellRenderer: renderRemarks,
    cellDataGetter: (key, item) => item[key],
  },
];

export class Overview extends React.Component {

  constructor() {
    super();
    this.rowGetter = this.rowGetter.bind(this);
    this.sortTable = this.sortTable.bind(this);
    this.rowClassNameGetter = this.rowClassNameGetter.bind(this);
    this.organizationClicked = this.organizationClicked.bind(this);
    this.rowHeightGetter = this.rowHeightGetter.bind(this);
  }
  componentWillMount() {
    this.columnSettings = columnSettings;
    this.columnSettings.rowGetter = this.rowGetter;
  }

  sortTable(column, sortOrder) {
    this.props.flux.actions.sharedEvidence.sortSharingOrganizations(column);
  }

  organizationClicked(sharingOrganizationUuid) {
    this.props.flux.actions.sharedEvidence.sharingOrganizationSelected(sharingOrganizationUuid);
    this.props.flux.actions.sharedEvidence.getListOfAttachments(sharingOrganizationUuid);
    this.props.flux.actions.sharedEvidence.getOpenAssessmentTypes();
  }

  rowHeightGetter(index) {
    const item = this.props.organizations[index];
    const maxItems = Math.max.apply(this, [
      item.sharing_organization.name.length || 1,
      item.sharing_date.length || 1,
      getRemarks(item.remarks).length || 1,
      40
    ]);
    return maxItems;
  }

  rowGetter(index) {
    const item = this.props.organizations[index];

    const organization = {
      name: item.sharing_organization.name,
      uuid: item.uuid,
      clicked: this.organizationClicked,
    };

    const row = {
      organization_name: organization,
      sharing_date: tools.dateFilter(item.sharing_date, pageData.dateFormat, pageData.languageCode),
      state: item.state,
      remarks: item.remarks
    };

    return row;
  }

  rowClassNameGetter(index) {
    return 'no-hover';
  }

  render() {
    const dataTableProps = {
      selected: this.props.selectedColumn,
      sortOrder: this.props.sortOrder,
    };

    const tableWidth = 900;

    const tableContainer = {
      marginTop: 10,
    };

    return (
      <div>
        {_t('The table below provides an overview of organizations that shared evidence with your organization')}
        {'.'}
        {
          this.props.organizations ? (
            <div style={tableContainer}>
              <DataTable
                sortTable={this.sortTable}
                rowsCount={this.props.organizations.length}
                columnSettings={this.columnSettings}
                dataTable={dataTableProps}
                rowHeightGetter={this.rowHeightGetter}
                width={tableWidth}
                rowClassNameGetter={this.rowClassNameGetter}
              />
            </div>
          ) : (
            <BusyFeedback />
          )
        }
      </div>
    );
  }

}

Overview.propTypes = {
  organizations: React.PropTypes.array.isRequired,
  selectedColumn: React.PropTypes.string.isRequired,
  sortOrder: React.PropTypes.string.isRequired,
  flux: React.PropTypes.object,
};
