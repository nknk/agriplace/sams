import React from 'react';
import { Overview } from './Overview/Overview.jsx';
import { Copy } from './Copy/Copy.jsx';
require('./SharedEvidence.less');

export const SharedEvidenceTab = React.createClass({
  mixins: [
    FluxMixin,
    StoreWatchMixin('SharedEvidenceStore'),
  ],

  getStateFromFlux() {
    const sharedEvidenceStoreState = this.getFlux().store('SharedEvidenceStore').getState();

    const selectedOrganization = sharedEvidenceStoreState.selectedOrganization;
    const isOverviewVisible = !selectedOrganization;
    const selectedOrganizationUuid = selectedOrganization ? selectedOrganization.uuid : null;
    const sharedAttachments = sharedEvidenceStoreState.sharedAttachments[selectedOrganizationUuid];
    const selectedSharedAttachments =
      sharedEvidenceStoreState.selectedSharedAttachments[selectedOrganizationUuid] || [];

    return {
      sharingOrganizations: sharedEvidenceStoreState.sharingOrganizations,
      selectedColumn: sharedEvidenceStoreState.selectedColumnSharingOrganizations,
      sortOrder: sharedEvidenceStoreState.sortOrderSharingOrganizations,
      isOverviewVisible,
      selectedOrganization,
      sharedAttachments,
      sortOrderSharedAttachments: sharedEvidenceStoreState.sortOrderSharedAttachments,
      selectedColumnSharedAttachments: sharedEvidenceStoreState.selectedColumnSharedAttachments,
      selectedSharedAttachments,
      copyToArchive: sharedEvidenceStoreState.copyToArchive,
      isConfirmationModalVisible: sharedEvidenceStoreState.isConfirmationModalVisible,
      openAssessmentTypes: sharedEvidenceStoreState.openAssessmentTypes,
      selectedOpenAssessmentTypes: sharedEvidenceStoreState.selectedOpenAssessmentTypes,
    };
  },

  render() {
    const flux = this.getFlux();
    return (
      <div className="shared-evidence-tab">
        {
          this.state.isOverviewVisible ? (
            <Overview
              organizations={this.state.sharingOrganizations}
              selectedColumn={this.state.selectedColumn}
              sortOrder={this.state.sortOrder}
              flux={flux}
            />
          ) : (
            <Copy
              selectedOrganization={this.state.selectedOrganization}
              sharedAttachments={this.state.sharedAttachments}
              selectedSharedAttachments={this.state.selectedSharedAttachments}
              sortOrderSharedAttachments={this.state.sortOrderSharedAttachments}
              selectedColumnSharedAttachments={this.state.selectedColumnSharedAttachments}
              copyToArchive={this.state.copyToArchive}
              isConfirmationModalVisible={this.state.isConfirmationModalVisible}
              openAssessmentTypes={this.state.openAssessmentTypes}
              selectedOpenAssessmentTypes={this.state.selectedOpenAssessmentTypes}
              flux={flux}
            />
          )
        }
      </div>
    );
  },

});

