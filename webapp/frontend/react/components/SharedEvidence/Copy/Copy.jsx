import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Button, ButtonToolbar } = Bootstrap;
require('../SharedEvidence.less');

import { ConfirmationModal } from './ConfirmationModal.jsx';
import { AssessmentTypesSection } from './AssessmentTypesSection.jsx';
import { SharedEvidenceSection } from './SharedEvidenceSection.jsx';
import { ArchiveSection } from './ArchiveSection.jsx';

import { EditAttachmentModalController } from '../../Attachments/Modals/EditAttachmentModalController.jsx';

export class Copy extends React.Component {

  constructor() {
    super();
    this.openAttachment = this.openAttachment.bind(this);
    this.closeAttachment = this.closeAttachment.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onModalOk = this.onModalOk.bind(this);
    this.state = {
      openedAttachment: null,
    };
  }

  onConfirm() {
    this.props.flux.actions.sharedEvidence.openSharedEvidenceConfirmationModal();
    if (this.props.copyToArchive) {
      this.props.flux.actions.sharedEvidence.copySharedEvidenceToArchive();
    } else if (this.props.selectedOpenAssessmentTypes.length) {
      this.props.flux.actions.sharedEvidence.copySharedEvidenceToOpenAssessments({});
    }
  }

  onCancel() {
    this.props.flux.actions.sharedEvidence.copySharedEvidenceCancel();
  }

  onModalOk() {
    this.props.flux.actions.sharedEvidence.copySharedEvidenceCancel();
  }

  openAttachment(attachment) {
    this.setState({
      openedAttachment: attachment,
    });
    this.refs.EditAttachmentModalController.openModal();
  }

  closeAttachment() {
    this.refs.EditAttachmentModalController.onCancel();
  }

  render() {
    const { copyToArchive, selectedSharedAttachments, selectedOrganization,
      openAssessmentTypes, selectedOpenAssessmentTypes } = this.props;

    const canShare = selectedSharedAttachments.length && (
      copyToArchive || selectedOpenAssessmentTypes.length
    );

    const organizationName = selectedOrganization.sharing_organization.name;
    const readOnly = true;

    return (
      <div className="copy-shared-evidence">
        <ConfirmationModal
          isVisible={this.props.isConfirmationModalVisible}
          onOk={this.onModalOk}
        />
        <EditAttachmentModalController
          ref="EditAttachmentModalController"
          isDescriptionReadOnly={readOnly}
          isDeleteReadOnly={readOnly}
          onAttachmentModalSuccess={this.closeAttachment}
          attachment={this.state.openedAttachment}
        />
        <div id="sub-heading">
          {organizationName}
          {' '}
          {_t('shared the following evidence with you')}
          {'. '}
          {_t('Please determine what you want to do with this evidence')}
          {'.'}
        </div>
        {
          this.props.sharedAttachments ? (
            <SharedEvidenceSection
              sharedAttachments={this.props.sharedAttachments}
              selectedAttachments={selectedSharedAttachments || []}
              sortOrder={this.props.sortOrderSharedAttachments}
              selectedColumn={this.props.selectedColumnSharedAttachments}
              openAttachment={this.openAttachment}
              flux={this.props.flux}
            />
          ) : null
        }
        <ArchiveSection
          isSelected={this.props.copyToArchive}
          flux={this.props.flux}
        />
        <AssessmentTypesSection
          assessmentTypes={openAssessmentTypes}
          selectedAssessmentTypes={selectedOpenAssessmentTypes}
          flux={this.props.flux}
        />
        <div className="section-below">
          <ButtonToolbar className="pull-left">
            <Button bsStyle="success" disabled={!canShare} onClick={this.onConfirm}>
              {_t('Confirm')}
            </Button>
            <Button bsStyle="default" onClick={this.onCancel}>
              {_t('Cancel')}
            </Button>
          </ButtonToolbar>
        </div>
      </div>
    );
  }

}

Copy.propTypes = {
  selectedOrganization: React.PropTypes.object.isRequired,
  sharedAttachments: React.PropTypes.array.isRequired,
  selectedSharedAttachments: React.PropTypes.array,
  sortOrderSharedAttachments: React.PropTypes.string,
  selectedColumnSharedAttachments: React.PropTypes.string,
  copyToArchive: React.PropTypes.bool.isRequired,
  isConfirmationModalVisible: React.PropTypes.bool,
  openAssessmentTypes: React.PropTypes.array,
  selectedOpenAssessmentTypes: React.PropTypes.array,
  flux: React.PropTypes.object,
};
