import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Input } = Bootstrap;
require('../SharedEvidence.less');

export class AssessmentTypesSection extends React.Component {

  constructor() {
    super();
    this.isSelected = this.isSelected.bind(this);
    this.getCheckBox = this.getCheckBox.bind(this);
    this.getList = this.getList.bind(this);
  }

  isSelected(e, uuid) {
    this.props.flux.actions.sharedEvidence.openAssessmentTypeSelected({
      isSelected: e.target.checked,
      assessmentTypeUuid: uuid,
    });
  }

  getCheckBox(isChecked, label, uuid) {
    return (
      <div className="checkbox-parent">
        <Input
          className="select-checkbox"
          type="checkbox"
          checked={isChecked}
          onChange={(e) => this.isSelected(e, uuid)}
          label={label}
        />
      </div>
    );
  }

  getList() {
    const list = this.props.assessmentTypes.map(assessment =>
      this.getCheckBox(
        this.props.selectedAssessmentTypes.find(a => a.uuid === assessment.uuid),
        assessment.name,
        assessment.uuid
      )
    );
    return list;
  }

  render() {
    return this.props.assessmentTypes.length ? (
      <div className="section-below">
        <div className="sub-heading">
          <strong>
            {_t('Copy selected items to open assessments of')}
            {' :'}
          </strong>
        </div>
        <div className="space-after-sub-heading">
          {this.getList()}
        </div>
      </div>
    ) : null;
  }
}

AssessmentTypesSection.propTypes = {
  assessmentTypes: React.PropTypes.array,
  selectedAssessmentTypes: React.PropTypes.array,
  flux: React.PropTypes.object,
};
