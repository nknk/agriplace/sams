import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Input } = Bootstrap;
const DataTable = require('../../DataTable/DataTable.jsx');
const pageData = require('../../Utils/pageData.js');
require('../SharedEvidence.less');

function renderMultiline(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return cellData.map(item => (<p>{item}</p>));
}

function getTitleIcon(iconType) {
  let iconClass;
  switch (iconType) {
  case 'AgriformAttachment':
    iconClass = 'fa-clipboard';
    break;

  case 'FormAttachment':
    iconClass = 'fa-clipboard';
    break;

  case 'FileAttachment':
    iconClass = 'fa-file';
    break;

  case 'TextReferenceAttachment':
    iconClass = 'fa-align-left';
    break;

  default:
    iconClass = 'fa-paperclip';
  }
  return `icon fa ${iconClass}`;
}

function renderTitleWithIcon(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return (
    <div
      onClick={(e) => (
        cellData.titleClicked ? (
          cellData.titleClicked(cellData.uuid, e)
        ) : null
      )}
      className={cellData.titleClicked ? 'hover-archive-table' : null}
    >
      <i className={getTitleIcon(cellData.attachmentType)}></i>
      {' '}
      {cellData.name}
    </div>
  );
}

function renderCheckBox(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
  return (
    <div>
      <Input
        style={{
          margin: '0 auto',
          height: '16px',
          width: '16px',
          cursor: 'pointer',
        }}
        type="checkbox"
        checked={cellData.isChecked}
        onChange={(e) => cellData.handleCheckBoxClicked(cellData.uuid, e)}
      />
    </div>
  );
}

const columnSettings = [
  {
    dataKey: 'check_box',
    label: '',
    width: 50,
    cellRenderer: renderCheckBox,
    cellDataGetter: (key, item) => item[key],
    isCheckBoxColumn: true,
  },
  {
    dataKey: 'title',
    label: _t('Title'),
    width: 240,
    cellRenderer: renderTitleWithIcon,
    cellDataGetter: (key, item) => item[key],
  }, {
    dataKey: 'used_in_document_types',
    label: _t('Evidence type'),
    width: 240,
    flexGrow: 2,
    height: 350,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => `${i.code} ${i.name}`),
  }, {
    dataKey: 'usable_for_assessments',
    label: _t('Usable for'),
    width: 200,
    cellRenderer: renderMultiline,
    cellDataGetter: (key, item) => item[key].map(i => i.name),
  },
  {
    dataKey: 'modified_time',
    label: _t('Date modified'),
    width: 250,
  },
];

export class SharedEvidenceSection extends React.Component {

  constructor() {
    super();

    this.rowGetter = this.rowGetter.bind(this);
    this.rowHeightGetter = this.rowHeightGetter.bind(this);
    this.sortTable = this.sortTable.bind(this);
    this.rowClassNameGetter = this.rowClassNameGetter.bind(this);
    this.handleCheckBoxClicked = this.handleCheckBoxClicked.bind(this);
    this.onHeaderCheckBoxClick = this.onHeaderCheckBoxClick.bind(this);
    this.onTitleClick = this.onTitleClick.bind(this);

    this.columnSettings = columnSettings;
    this.columnSettings.rowGetter = this.rowGetter;
  }

  onTitleClick(uuid, e) {
    const { sharedAttachments } = this.props;
    const attachment = sharedAttachments.find(a => uuid === a.uuid);
    this.props.openAttachment(attachment.attachment);
  }

  onHeaderCheckBoxClick(column, e) {
    this.props.flux.actions.sharedEvidence.selectAllSharedAttachments(e.target.checked);
  }

  handleCheckBoxClicked(attachmentUuid, e) {
    this.props.flux.actions.sharedEvidence.selectSharedAttachment(e.target.checked, attachmentUuid);
  }

  dontShowPreview(attachment) {
    const allowedTypes = ['FileAttachment'];
    return !_.includes(allowedTypes, attachment.attachment.attachment_type);
  }

  rowGetter(index) {
    const { sharedAttachments, selectedAttachments } = this.props;
    const item = sharedAttachments[index];

    const title = {
      name: item.attachment.title,
      attachmentType: item.attachment.attachment_type,
      uuid: item.uuid,
      titleClicked: this.dontShowPreview(item) ? null : this.onTitleClick,
    };

    const checkBox = {
      uuid: item.uuid,
      handleCheckBoxClicked: this.handleCheckBoxClicked,
      isChecked: !!selectedAttachments.find(a => a.uuid === item.uuid),
    };

    const row = {
      check_box: checkBox,
      title,
      used_in_document_types: item.attachment.used_in_document_types,
      used_in_assessments: item.attachment.used_in_assessments,
      usable_for_assessments: item.attachment.usable_for_assessments,
      modified_time: tools.dateFilter(item.attachment.modified_time, pageData.dateFormat, pageData.languageCode),
    };

    return row;
  }

  rowHeightGetter(index) {
    const item = this.props.sharedAttachments[index];
    if (!item || !item.attachment) return null;
    const maxItems = Math.max.apply(this, [
      item.attachment.used_in_document_types.length || 1,
      item.attachment.used_in_assessments.length || 1,
      item.attachment.usable_for_assessments.length || 1,
    ]);
    return maxItems * 40;
  }

  rowClassNameGetter(index) {
    return 'no-hover';
  }

  sortTable(column, sortOrder) {
    this.props.flux.actions.sharedEvidence.sortSharedAttachments(column);
  }

  render() {
    const { sharedAttachments, selectedAttachments } = this.props;
    const headerCheckBoxChecked = (sharedAttachments.length === selectedAttachments.length);

    const dataTableProps = {
      selected: this.props.selectedColumn,
      sortOrder: this.props.sortOrder,
    };

    let tableHeight = 50;
    if (sharedAttachments) {
      sharedAttachments.forEach((entry, index) => (
        tableHeight += this.rowHeightGetter(index)
      ));
    }
    tableHeight = (tableHeight > 600) ? 600 : tableHeight;

    return (
      <div className="space-after-sub-heading">
        <DataTable
          sortTable={this.sortTable}
          rowsCount={sharedAttachments.length}
          columnSettings={this.columnSettings}
          dataTable={dataTableProps}
          rowHeightGetter={this.rowHeightGetter}
          width={1000}
          height={tableHeight}
          overflowY={'auto'}
          rowClassNameGetter={this.rowClassNameGetter}
          onHeaderCheckBoxClick={this.onHeaderCheckBoxClick}
          headerCheckBoxChecked={headerCheckBoxChecked}
        />
      </div>
    );
  }

}

SharedEvidenceSection.propTypes = {
  sharedAttachments: React.PropTypes.array.isRequired,
  selectedAttachments: React.PropTypes.array,
  sortOrder: React.PropTypes.string,
  selectedColumn: React.PropTypes.string,
  openAttachment: React.PropTypes.func.isRequired,
  flux: React.PropTypes.object,
};
