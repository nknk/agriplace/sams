import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Button, Modal } = Bootstrap;
require('../SharedEvidence.less');

export class ConfirmationModal extends React.Component {

  render() {
    return (
      <Modal className="shared-evidence-modal" show={this.props.isVisible} backdrop={'static'} onRequestHide={null}>
        <Modal.Header>
          <h2>
            <strong>{_t('Shared evidence')}</strong>
          </h2>
        </Modal.Header>
        <Modal.Body>
          {_t('It may take a couple of minutes before your request is processed')}
          {'.'}
        </Modal.Body>
        <Modal.Footer className="shared-evidence-footer">
          <div className="button-toolbar">
            <Button
              bsStyle="success"
              onClick={this.props.onOk}
            >
              {_t('OK')}
            </Button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }
}

ConfirmationModal.propTypes = {
  isVisible: React.PropTypes.bool,
  onOk: React.PropTypes.func,
};
