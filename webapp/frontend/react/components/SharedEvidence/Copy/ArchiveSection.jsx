import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Input } = Bootstrap;
require('../SharedEvidence.less');

export class ArchiveSection extends React.Component {

  constructor() {
    super();
    this.isSelected = this.isSelected.bind(this);
  }

  isSelected(e) {
    this.props.flux.actions.sharedEvidence.toggleCopyToArchive();
  }

  render() {
    return (
      <div className="section-below">
        <div className="sub-heading">
          <strong>{_t('Copy selected items to archive')}</strong>
        </div>
        <div className="space-after-sub-heading">
          <div className="inline">
            <Input
              className="select-checkbox"
              type="checkbox"
              checked={this.props.isSelected}
              onChange={(e) => this.isSelected(e)}
            />
          </div>
          <div className="select-checkbox-label inline" onClick={(e) => this.isSelected(e)}>
            <strong>
              {_t('Copy to archive')}
              {'.'}
            </strong>
            {' '}
            {_t('Choose this option if you want to later use them for open and new assessments')}
            {'. '}
            {_t('In every assessment you can per evidence type determine which evidence you want to reuse')}
            {'.'}
          </div>
        </div>
      </div>
    );
  }
}

ArchiveSection.propTypes = {
  isSelected: React.PropTypes.bool.isRequired,
  flux: React.PropTypes.object,
};
