import React, { PropTypes } from 'react';
const classnames = require('classnames');

const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

export const DocTypeReuseLink = React.createClass({
  propTypes: {
    reuseCount: PropTypes.number,
    onClick: PropTypes.func,
    documentTypeUuid: PropTypes.string,
    isHistoricalAttachmentVisible: PropTypes.bool,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('DocTypeReuseStore'),
  ],

  getStateFromFlux() {
    const flux = this.getFlux();
    const state = flux.store('DocTypeReuseStore').getState();

    return {
      isFetching: ~state.fetchingInProgress.indexOf(this.props.documentTypeUuid),
    };
  },

  onClick() {
    this.props.onClick(this.props.documentTypeUuid);
  },

  render() {
    const { reuseCount, documentTypeUuid } = this.props;
    if (!reuseCount) {
      return null;
    }

    const title = ` ${_t('Historical Attachments')} (${reuseCount}) `;
    const className = classnames('icon fa fa-recycle', {
      'fa-spin': this.state.isFetching,
    });

    return (
      <div
        onClick={this.onClick.bind(this, documentTypeUuid)}
        className="historical-attachment-link"
      >
        <span className={className} />
        {title}
        {
          this.props.isHistoricalAttachmentVisible ?
            <span className="icon fa fa-caret-up" /> :
            <span className="icon fa fa-caret-down" />
        }
      </div>
    );
  },
});
