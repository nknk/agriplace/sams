import React from 'react';
const Bootstrap = require('react-bootstrap');
require('./DocTypeReuse.less');

const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

const BusyFeedback = require('../BusyFeedback/BusyFeedback.jsx');
const moment = require('moment');
const pageData = require('../Utils/pageData.js');
export const DocTypeReuse = React.createClass({

  propTypes: {
    assessmentUuid: React.PropTypes.string.isRequired,
    onOpenAttachmentLink: React.PropTypes.func.isRequired,
    onAttach: React.PropTypes.func.isRequired,
    documentTypeUuid: React.PropTypes.string.isRequired,
    layout: React.PropTypes.string.isRequired,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('DocTypeReuseStore'),
  ],

  getInitialState() {
    return {
      selected: [],
    };
  },

  componentWillMount() {
    const { assessmentUuid, documentTypeUuid } = this.props;
    if (this.state.attachments.length === 0) {
      this.getFlux().actions.docTypeReuse.getHistoricalAttachmentsList(assessmentUuid, documentTypeUuid);
    }
  },

  getStateFromFlux() {
    const flux = this.getFlux();
    const state = flux.store('DocTypeReuseStore').getState();
    const attachments = state.historicalAttachments[this.props.documentTypeUuid] || [];
    return {
      attachments: this.format(this.sort(attachments)),
      isPosting: ~state.postingInProgress.indexOf(this.props.documentTypeUuid),
    };
  },

  onCheckBoxChanged(attachmentId, e) {
    if (e.target.checked) {
      this.state.selected.push(attachmentId);
    } else {
      this.state.selected = this.state.selected.filter(id => (
        id !== attachmentId
      ));
    }
  },

  onAttach() {
    if (this.state.selected.length) {
      const flux = this.getFlux();
      flux.actions.docTypeReuse
        .updateHistoricalAttachmentsList(
          this.props.assessmentUuid,
          this.props.documentTypeUuid,
          this.state.selected,
          this.props.onAttach
        );
    }
  },

  format(attachments) {
    attachments.forEach(a => {
      a.attachment.modified_time = moment(a.attachment.modified_time).format(pageData.dateFormat);
    });
    return attachments;
  },

  sort(attachments) {
    return attachments.sort((a, b) => (
      moment(b.attachment.modified_time).unix() - moment(a.attachment.modified_time).unix()
    ));
  },

  onOpenAttachmentLink(attachment) {
    this.props.onOpenAttachmentLink(attachment);
  },

  fillTable() {
    return this.state.attachments.map(attachment => {
      const attachmentClasses = {
        AgriformAttachment: 'fa-clipboard',
        FormAttachment: 'fa-clipboard',
        FileAttachment: 'fa-file',
        TextReferenceAttachment: 'fa-align-left',
      };
      const iconClass = attachmentClasses[attachment.attachment.attachment_type] || 'fa-paperclip';

      const attachmentUsedIn = [];
      attachment.attachment.used_in_assessments.forEach(usedIn => {
        attachmentUsedIn.push(
          <div title={usedIn.name} className="assessmentName">{usedIn.name}</div>
        );
      });

      return (
        <tr key={attachment.uuid}>
          <td>
            <Bootstrap.Input
              className="checkbox-historical"
              type="checkbox"
              onChange={this.onCheckBoxChanged.bind(this, attachment.id)}
            >
            </Bootstrap.Input>
          </td>
          <td>
            <div className="historical-title" onClick={this.onOpenAttachmentLink.bind(this, attachment)}>
              <span className={`icon fa , ${iconClass}`} />
              <span title={attachment.attachment.title}>&nbsp;{attachment.attachment.title}</span>
            </div>
          </td>
          <td>{attachmentUsedIn.length ? attachmentUsedIn : '-'}</td>
          <td className="modifiedTime">
            {
              attachment.attachment.modified_time
              ? attachment.attachment.modified_time
              : '-'
            }
          </td>
          <td className="expirationDate">
            {
              attachment.attachment.expiration_date
              ? attachment.attachment.expiration_date
              : '-'
            }
          </td>
        </tr>
      );
    });
  },

  render() {
    if (this.state.isPosting) {
      return <BusyFeedback />;
    } else if (this.state.attachments.length === 0) {
      return null;
    }

    const usedIn = `${_t('Used in')}:`;

    return (
      <div className="DocTypeReuse">
        <div className="historical-attachment-box" >
          <Bootstrap.Table hover className="differentTable">
            <thead>
              <tr>
                <th className="historical-checkbox-column-width"></th>
                <th>{_t('Title')}</th>
                <th>{usedIn}</th>
                <th>{_t('Date Modified')}</th>
                <th>{_t('Expiry Date')}</th>
              </tr>
            </thead>
            <tbody>
            {this.fillTable()}
            </tbody>
          </Bootstrap.Table>

          <div >
            <Bootstrap.Button className="button" onClick={this.onAttach}>{_t('Attach')}</Bootstrap.Button>
          </div>

        </div>
        {
          (this.props.layout === 'evidence') ?
            <div className="historical-attachment-box-bottom-space"></div> : null
        }
      </div>
    );
  },
});
