import React from 'react';
import 'readmore-js';

export const ReadMore = React.createClass({

  propTypes: {
    text: React.PropTypes.string,
    options: React.PropTypes.object,
  },

  componentDidMount() {
    const options = {
      maxHeight: 48,
      sectionCSS: 'display: block; max-width: 800px;',
      moreLink: `<a href="#">${_t('Read more')}...</a>`,
      lessLink: `<a href="#">${_t('Read less')}...</a>`,
    };

    _.assign(options, this.props.options);
    const node = React.findDOMNode(this.readMoreDiv);
    $(node).readmore(options);
  },

  componentDidUnMount() {
    const node = React.findDOMNode(this.readMoreDiv);
    $(node).readmore('destroy');
  },

  render() {
    const { text } = this.props;
    return (
      <div ref={o => { this.readMoreDiv = o; }} dangerouslySetInnerHTML={{ __html: text }}>
      </div>
    );
  },

});
