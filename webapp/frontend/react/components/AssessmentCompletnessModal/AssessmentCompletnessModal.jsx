import React from 'react';
import { Link } from 'react-router';
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;

export const AssessmentCompletnessModal = React.createClass({
  propTypes: {
    assessmentUuid: React.PropTypes.object,
    params: React.PropTypes.object,
    onHide: React.PropTypes.func,
    isClicked: React.PropTypes.bool,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('AssessmentsStore'),
  ],

  getStateFromFlux() {
    const flux = this.getFlux();
    const assessementsStoreState = flux.store('AssessmentsStore').getState();

    return {
      assessment: assessementsStoreState.assessments[this.props.params.assessmentUuid],
    };
  },

  isComplete() {
    return this.state.assessment.isAssessmentComplete();
  },

  isCompliant() {
    return this.state.assessment.isAssessmentCompliant();
  },

  isDone() {
    return this.isComplete() && this.isCompliant();
  },

  isRequiredForView() {
    return !this.isDone;
  },

  render() {
    if (!this.state.assessment) {
      return null;
    }
    const isVisible = this.props.isClicked && !this.isDone();

    const { membershipUuid, assessmentAccessMode, assessmentUuid } = this.props.params;
    return (
      <Bootstrap.Modal show={isVisible} backdrop="static" onHide={this.props.onHide}>
        <Bootstrap.Modal.Header>
          {_t('Warning')}
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
          {!this.isComplete()
            ? (
            <p>
              {
                `${_t('You have not completed all the questions yet')}.
                ${_t('Please fill in all the questions before closing and sharing the assessment')}`
              }
            </p>
            )
            : null
          }
          {!this.isCompliant()
            ? (
            <p>
              {
                `${_t('You have answered one or more "Major" questions with "No"')}.
                ${_t('Please take corrective action and adjust your answer')}`}
            </p>
            )
            : null
          }
          {_t('See')}&nbsp;
          <Link
            onClick={this.props.onHide}
            to={`/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/detail`}
          >
            {_t('overview')}
          </Link>
          &nbsp;{_t('for details')}
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          <Bootstrap.Button bsStyle="primary" onClick={this.props.onHide} >
            {_t('OK')}
          </Bootstrap.Button >
        </Bootstrap.Modal.Footer>
      </Bootstrap.Modal>
    );
  },

});
