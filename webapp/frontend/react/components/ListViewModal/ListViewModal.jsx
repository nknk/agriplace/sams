

// ListViewModal
//
//  FilterableListView wrapped in a Ok/Cancel Bootstrap modal
//

var ListView = require('../ListView/ListView.jsx');

// require( './ListViewModal.less' );

var FilterableListView = require('../FilterableListView/FilterableListView.jsx');


var ListViewModal = React.createClass({

  propTypes: {
    listView: React.PropTypes.object,
    queryInput: React.PropTypes.object,
    modal: React.PropTypes.shape({
      title: React.PropTypes.string,
      okText: React.PropTypes.string,
      cancelText: React.PropTypes.string,
      onResult: React.PropTypes.func.required,
    }),
  },

  mixins: [Bootstrap.OverlayMixin],


  onItemClicked(item) {
    var selected = (this.props.listView.selected.indexOf(item) > -1)
        ? _.without(this.props.listView.selected, item)
        : this.props.listView.selected.concat(item)
      ;
    this.setState({ selected: selected });
  },

  getInitialState() {
    this.props.listView.onItemClicked = this.onItemClicked;
    return {
      selected: this.props.listView.selected || [],
      query: this.props.queryInput.query,
    };
  },

  onQueryInputChange(event) {
    this.setState({ query: event.target.value });
  },


  onModalResult(event) {
    var selected;

    // prevent adding without selection by showing a notification
    if (event && event.target.id === 'listview-ok-button') {
      if (_.isEmpty(this.state.selected)) {
        this.setState({ noSelectionNotification: true });
        return null;
      }
      selected = this.state.selected;
    }
    // tools.forceFunction( this.props.modal.onResult )( selected );
    this.setState({ showModal: false });
  },


  renderCloseIcon() {
    return (
      <Bootstrap.Button
        type = "button"
        className = "close pull-right"
        onClick = { this.onModalResult }
      >
        <span aria-hidden="true">&times;</span>
      </Bootstrap.Button>
    );
  },


  renderTitle() {
    return (
      <h1>
        <span className={ this.props.modal.icon }></span>
        <span id="listview-modal-title">{ this.props.modal.title }</span>
      </h1>
    );
  },


  renderQueryInput() {
    return (
      <div id="listview-query-input" className="input-group">

        <div className="input-group-addon">
          <span className="fa fa-search"></span>
        </div>

        <input
          type = "text"
          className = "form-control"
          value = { this.state.query }
          placeholder = { this.props.queryInput.placeholder }
          onChange = { this.onQueryInputChange }
        />

      </div>
    );
  },


  renderListFilter() {
    return (
      <form role="form" className="form-horizontal">
        <fieldset>
          <div className="form-group">

            <div className="col-sm-6">
              { this.renderQueryInput() }
            </div>

          </div>
        </fieldset>
      </form>
    );
  },


  renderListView() {
    // extract the full list of items from the original props.listView and use for the filter
    // var { items, ...listViewOther }= this.props.listView;
    // items= tools.objectFilter( this.props.listView.items, this.state.query, this.keys );
    // // sort the filtered list
    // items= tools.sortby( items, this.props.listView.sortKeys );

    // var listViewProps= _.extend( listViewOther, {
    //   items      : items,
    //   selected  : [ this.state.selected ] || []
    // });

    this.props.listView.selected = this.state.selected;

    return (
      <FilterableListView
        listViewOptions={ this.props.listView }
        queryInputOptions={ this.props.queryInput }
      />
    );
  },


  renderModalButtons() {
    return (
      <div className="btn-toolbar" role="toolbar">
        <div className="btn-group pull-right">

          <Bootstrap.Button
            id="listview-cancel-button"
            data-dismiss="modal"
            onClick= { this.onModalResult }
          >

          { _t('Back') }

          </Bootstrap.Button>

          <Bootstrap.Button
            id="listview-ok-button"
            onClick= { this.onModalResult }
          >
          { _t('Attach') }
          </Bootstrap.Button>

        </div>
      </div>
    );
  },


  render() {

    return (
      <Bootstrap.Modal
        backdrop="static"
        onHide={ this.onModalResult }
      >

        <Bootstrap.Modal.Header>
          { this.renderCloseIcon() }
          { this.renderTitle() }
        </Bootstrap.Modal.Header>

        <Bootstrap.Modal.Body>
          { this.renderListView() }
        </Bootstrap.Modal.Body>

        <Bootstrap.Modal.Footer>
          { this.renderModalButtons() }
        </Bootstrap.Modal.Footer>

      </Bootstrap.Modal>
    );
  },

});


module.exports = ListViewModal;
