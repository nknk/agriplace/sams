const urls = {
  get(id, args) {
    const refs = {
      questionnaire(membershipUuid, assessmentAccessMode, assessmentUuid, questionnaireUuid) {
        return `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/questionnaires/${questionnaireUuid}/edit`;
      },
      reopen(membershipUuid, assessmentAccessMode, assessmentUuid) {
        return `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/reopen`;
      },
    };
    return refs[id].apply(null, args);
  },
};

export default urls;
