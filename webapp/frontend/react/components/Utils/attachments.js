var Models = require('../../models/models.js');

//
// Constans
//

// see attachments/api/internal/serializers.py -> ATTACHMENT_TYPES
var ATTACHMENT_TYPES = [
  'FileAttachment',
  'TextReferenceAttachment',
  'AgriformAttachment',
  'NotApplicableAttachment',
  'FormAttachment',
];

//
// Convertation between different attachment(document) representations
//

// random object to attachment instance.
// to achieve success attachment_type must be one of obj props
// available types: ATTACHMENT_TYPES
function objectToAttachment(obj) {
  if (obj && obj.attachment_type && _.include(ATTACHMENT_TYPES, obj.attachment_type)) {
    // log('objectToAttachment: ', 'we have ', obj.attachment_type);
    var attachment = {};

    switch (obj.attachment_type) {

    case 'FileAttachment':
      attachment = new Models.FileAttachmentModel(obj);
      if (attachment.expiration_date === 'never') {
        attachment.expiration_date = null;
      }
      if (((attachment.title === '') || (!attachment.title)) && (attachment.original_file_name)) {
        var filename = attachment.original_file_name;
        attachment.title = (filename.lastIndexOf('.') === -1)
          ? (filename)
          : (filename.substring(0, filename.lastIndexOf('.')));
      }
      break;

    case 'TextReferenceAttachment':
      attachment = new Models.TextReferenceAttachmentModel(obj);
      break;

    case 'NotApplicableAttachment':
      attachment = new Models.NotApplicableAttachmentModel(obj);
      break;

    case 'AgriformAttachment':
      attachment = new Models.AgriformAttachmentModel(obj);
      break;

    case 'FormAttachment':
      attachment = new Models.GenericFormAttachmentModel(obj);
      break;
      // no default
    }
    return (attachment);
  }

  return {};
}


function attachments2documentType(attachmentList, assessmentUuid, documentTypeUuid) {
  var documentTypeAttachments = {};
  documentTypeAttachments[documentTypeUuid] = [];

  attachmentList.forEach((item) => {
    item.assessmentUuid = assessmentUuid;
    item.documentTypeUuid = documentTypeUuid;
    documentTypeAttachments[documentTypeUuid].push(item);
  });

  return documentTypeAttachments;
}


// wraps attachment to AssessmentAttachmentLink instance
function attachmentToAttachmentLink(attachment, assessmentUuid, documentTypeUuid, isDone) {
  return new Models.AssessmentAttachmentLink({
    assessment: assessmentUuid,
    document_type_attachment: documentTypeUuid,
    attachment,
    is_done: isDone,
  });
}

//
// Convertation for data structures(assessment attachments and so on)
//

// convert data that comes from API to frontend models instances
// attachments - dict of AssessmentDocumentLinks grouped by attachmentTypeUuid
function initAssessmentAttachments(attachmentLinkObjectsDict) {
  var attachmentLinksDict = {};
  _.forOwn(attachmentLinkObjectsDict, (value, key) => {
    var attachmentLinks = [];
    _.forEachRight(value, (item) => {
      var attachmentLink = new Models.AssessmentAttachmentLink(item);
      attachmentLink.attachment = objectToAttachment(attachmentLink.attachment);
      attachmentLinks.push(attachmentLink);
    });
    attachmentLinksDict[key] = attachmentLinks;
  });
  return attachmentLinksDict;
}


module.exports = {
  attachments2documentType,
  attachmentToAttachmentLink,
  objectToAttachment,
  initAssessmentAttachments,
};
