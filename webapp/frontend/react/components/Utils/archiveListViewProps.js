import pageData from './pageData.js';

module.exports = {
  items: [],
  columns: [
    { title: _t('Title') },
    { modified_time: _t('Date modified') },
    { expiration_date: _t('Expiry date') },
    { used_in: _t('Used in') },
  ],
  tooltip: _t('Click to view'),
  sortKeys: [],
  filterKeys: ['title', 'modified_time'],
  showHeader: true,
  isReverseOrder: false,
  headerAlign: 'left',
  dateFormat: pageData.dateFormat,
  dateLocale: pageData.languageCode,
};
