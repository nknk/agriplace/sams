import React from 'react';
const Bootstrap = require('react-bootstrap');
const { Nav, NavItem } = Bootstrap;
require('./Tabs.less');

export class Tabs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activeKey: 0,
    };
    this.handleTabChange = this.handleTabChange.bind(this);
  }

  handleTabChange(eventKey) {
    this.setState({
      activeKey: eventKey,
    });
  }

  getNavItems() {
    const navItems = this.props.tabs.map((tab, index) => (
      <NavItem eventKey={index}>{tab.name}</NavItem>
    ));
    return navItems;
  }

  render() {
    return (
      <div className="tabs">
        <Nav bsStyle="tabs" activeKey={this.state.activeKey} onSelect={this.handleTabChange}>
          {this.getNavItems()}
        </Nav>
        <div className="tab">
          {this.props.tabs[this.state.activeKey].tab}
        </div>
      </div>
    );
  }

}

Tabs.propTypes = {
  tabs: React.PropTypes.array,
};
