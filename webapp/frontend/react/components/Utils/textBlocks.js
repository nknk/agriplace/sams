const textBlocks = {
  assessment_evidence_list_intro: `${_t('Here you can enter your evidence documents')}.
   ${_t('AgriPlace links each evidence document to the questionnaire of the corresponding certificate')}.
   ${_t('The questions related to the evidence document will be answered automatically')}.
   ${_t('After the evidence document has been registered, it will be stored in your archive from where it can be easily retrieved and reused')}.`,
  assessment_sharing_intro: `${_t('Please indicate below with which audit organization(s) you want to share your assessment')}. ${_t('Upon your authorization the auditor will have read-only access to your assessment (questionnaires and evidence)')}.`,
};

export default textBlocks;
