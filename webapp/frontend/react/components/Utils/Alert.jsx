import React from 'react';

var Alert = React.createClass({

  propTypes: {
    alertText  : React.PropTypes.string,
    alertIcon  : React.PropTypes.string,
  },


  render() {

    var alertIcon = this.props.alertIcon || 'fa fa-exclamation-triangle';

    // TODO: need to center the icon and text in the Alert label
    return (<div className="row">
            <br />
            <Bootstrap.Alert bsStyle="danger" style={{ marginBottom: '-40px' }} className="col-md-10 col-md-offset-1">
                <span className= { alertIcon + ' pull-left fa-2x' }></span>
                <div style={{ marginTop: '5px' }}>
                  <strong>
                    <h4 style={{ marginLeft: '10px' }}>{ this.props.alertText }</h4>
                  </strong>
                </div>
            </Bootstrap.Alert>
            <div style={{ height: '40px' }} />
            <div className="clearfix"></div>
           </div>);
  },

});

module.exports = Alert;
