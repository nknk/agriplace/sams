import React from 'react';


const ModalButtons = React.createClass({

  propTypes: {
    successText: React.PropTypes.string,
    successButtonStyle: React.PropTypes.string,
    onSuccess: React.PropTypes.func,
    cancelText: React.PropTypes.string,
    onCancel: React.PropTypes.func,
    dataDismiss: React.PropTypes.string,
  },


  render() {
    return (
      <div className="btn-toolbar" role="toolbar">
        <div className="pull-right">

          <Bootstrap.Button
            id="modal-cancel-button"
            bsStyle="default"
            data-dismiss={this.props.dataDismiss || 'modal'}
            onClick={this.props.onCancel}
          >
            {_t(this.props.cancelText || 'Cancel')}
          </Bootstrap.Button>


          <Bootstrap.Button
            bsStyle={this.props.successButtonStyle ? this.props.successButtonStyle : 'primary'}
            id="modal-ok-button"
            onClick={this.props.onSuccess}
          >
            {_t(this.props.successText || 'OK')}
          </Bootstrap.Button>

        </div>
      </div>
    );
  },

});


module.exports = ModalButtons;
