import React from 'react';
const Bootstrap = require('react-bootstrap');
require('./Modal.less');

const Modal = React.createClass({
  propTypes: {
    header: React.PropTypes.element,
    body: React.PropTypes.element,
    footer: React.PropTypes.element,
    bsSize: React.PropTypes.string,
    onOk: React.PropTypes.func,
    okButtonText: React.PropTypes.string,
    onCancel: React.PropTypes.func,
    showCancelButton: React.PropTypes.bool,
    cancelButtonText: React.PropTypes.string,
  },

  mixins: [Bootstrap.OverlayMixin],

  componentWillMount() {
    this.createButtons();
    this.createCloseIcon();
  },

  createButtons() {
    const cancelButton = this.props.showCancelButton
    ? (
      <Bootstrap.Button
        type="button"
        id="modal-cancel-button"
        bsStyle="default"
        onClick={this.props.onCancel || tools.nop}
      >
        {_t(this.props.cancelButtonText || 'Cancel')}
      </Bootstrap.Button>
      )
    : null;

    const okButton = (
      <Bootstrap.Button
        type="button"
        id="modal-ok-button"
        bsStyle="primary"
        onClick={this.props.onOk || tools.nop}
      >
      {_t(this.props.okButtonText || 'Ok')}
      </Bootstrap.Button>
    );

    this.buttons = [cancelButton, okButton];
  },

  createCloseIcon() {
    this.closeIcon = (
      <Bootstrap.Button
        type="button"
        className="close pull-right"
        onClick={this.props.onCancel || this.props.onOk || tools.nop}
      >
        <span aria-hidden="true">&times;</span>
      </Bootstrap.Button>
    );
  },

  renderHeader() {
    const header = this.props.header || <br />;
    return (
      <Bootstrap.Modal.Header>
        {this.closeIcon}
        {header}
      </Bootstrap.Modal.Header>
    );
  },

  renderBody() {
    return (
      <Bootstrap.Modal.Body>
        {this.props.body}
      </Bootstrap.Modal.Body>
    );
  },


  renderFooter() {
    return (
      <Bootstrap.Modal.Footer>
        <br />
        {this.props.footer || this.buttons}
      </Bootstrap.Modal.Footer>
    );
  },

  render() {
    return (
      <Bootstrap.Modal
        show={true}
        {...this.props}
        onHide={this.props.onCancel || this.props.onOk || tools.nop}
      >
        {this.renderHeader()}
        {this.renderBody()}
        {this.renderFooter()}
      </Bootstrap.Modal>
    );
  },

});

module.exports = Modal;
