import { Component, PropTypes } from 'react';
import { Modal } from 'react-bootstrap';
import BusyFeedback from '../BusyFeedback';

export default class ProgressModal extends Component {

  render() {
    const { isOpen } = this.props;
    return (
      <Modal show={isOpen} bsSize={'small'} backdrop={'static'} onRequestHide={null}>
        <Modal.Body>
          <BusyFeedback />
        </Modal.Body>
      </Modal>
    );
  }
}

ProgressModal.propTypes = {
  isOpen: PropTypes.bool,
};
