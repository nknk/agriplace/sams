import React, { PropTypes } from 'react';
const Bootstrap = require('react-bootstrap');

export class Pagination extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      activePage: this.props.activePage,
    };
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.state = {
      activePage: nextProps.activePage,
    };
  }

  handlePageChange(event, selectedEvent) {
    this.props.handlePageChange(selectedEvent.eventKey);
    this.setState({
      activePage: selectedEvent.eventKey,
    });
  }

  render() {
    const style = {
      textAlign: 'center',
      width: '100%',
    };

    if (!this.props.numberOfPages) {
      return null;
    }

    return (
      <div style={style}>
        <Bootstrap.Pagination
          prev
          next
          first
          last
          ellipsis={false}
          boundaryLinks
          items={this.props.numberOfPages}
          maxButtons={5}
          activePage={this.state.activePage}
          onSelect={this.handlePageChange}
        />
      </div>
    );
  }

}

Pagination.propTypes = {
  handlePageChange: PropTypes.func,
  numberOfPages: PropTypes.number,
  activePage: PropTypes.number,
};
