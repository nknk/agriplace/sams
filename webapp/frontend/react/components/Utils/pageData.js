const { jsContext = {} } = window;

const layoutOptions = {
  hzpc: 'hzpc',
  agriplace: 'agriplace',
  farm_group: 'farmGroup',
};

const pageData = {
  // default to US English
  languageCode: 'en-US',
  dateFormat: 'DD-MM-YYYY',
  dateFormatRegExp: new RegExp('^\\d{2}\\-\\d{2}\\-\\d{4}$'),

  context: {
    isPartnersAdd: jsContext.isPartnersAdd === 'True',
    isSharingAllowed: jsContext.EVIDENCE_SHARING === 'True',
    uuid: jsContext.ORGANIZATION_UUID,
    organizationSlug: jsContext.ORGANIZATION_SLUG,
    membershipUuid: jsContext.MEMBERSHIP_UUID,
    layout: layoutOptions[jsContext.LAYOUT] || layoutOptions.agriplace,
    apiUrl: jsContext.API_URL,
    staticUrl: jsContext.STATIC_URL,
    theme: jsContext.THEME,
  },
};

switch (jsContext.LANGUAGE_CODE) {
case 'nl':
  pageData.languageCode = 'nl';
  break;
case 'es':
  pageData.languageCode = 'es';
  break;
// no default
}

module.exports = pageData;
