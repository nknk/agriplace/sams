require('./ProductList.less');

const ProductList = React.createClass({

  propTypes: {
    onProductSelect: React.PropTypes.func,
    products: React.PropTypes.array,
  },

  mixins: [
    FluxMixin,
  ],


  handleProductClick(uuid) {
    this.props.onProductSelect({ uuid });
  },

  render() {
    return (
      <div className="ProductList">
        <table className="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th className="name">{_t('Crop')}</th>
              <th className="area">{_t('ha')}</th>
              <th className="location">{_t('Location(s) storage/processing if other than own farm (optional)')}</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.products.map(product => {
                return (
                  <tr onClick={this.handleProductClick.bind(this, product.uuid)}>
                    <td>{product.name}</td>
                    <td>{product.area}</td>
                    <td>{product.location}</td>
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
    );
  },

});

module.exports = ProductList;
