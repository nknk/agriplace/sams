import React from 'react';
import './OrganizationDetails.less';
import { Table } from 'react-bootstrap';

export class OrganizationDetailsTable extends React.Component {

  render() {
    const organizationDetails = this.props.organizationDetails || {};
    return (
      <div>
        <div className="header">
          {
            !this.props.organizationDetails ? (
              <div className="busy-feedback spinner">
                <icon className="fa fa-spinner fa-spin fa-lg" />
              </div>
            ) : null
          }
          <label className="name">{_t('Organization details')}</label>
          <div role="button" className="close-button">
            <i className="fa fa-times fa-lg" onClick={this.props.closePopup} />
          </div>
        </div>
        <Table condensed striped className="organization-table borderless">
          <tbody>
            <tr>
              <td><strong>{`${_t('Organization name')}:`}</strong></td>
              <td>{organizationDetails.name}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Address (line 1)')}:`}</strong></td>
              <td>{organizationDetails.mailing_address1}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Address (line 2)')}:`}</strong></td>
              <td>{organizationDetails.mailing_address2}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('City')}:`}</strong></td>
              <td>{organizationDetails.mailing_city}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Province')}:`}</strong></td>
              <td>{organizationDetails.mailing_province}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Postal code')}:`}</strong></td>
              <td>{organizationDetails.mailing_postal_code}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Country')}:`}</strong></td>
              <td>{organizationDetails.mailing_country}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('E-mail')}:`}</strong></td>
              <td>
                <a href={`mailto:${organizationDetails.email}`}>
                  {organizationDetails.email}
                </a>
              </td>
            </tr>
            <tr>
              <td><strong>{`${_t('Phone')}:`}</strong></td>
              <td>{organizationDetails.phone}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('Website')}:`}</strong></td>
              <td>
                <a href={`//${organizationDetails.url}`} target="_blank">
                  {organizationDetails.url}
                </a>
              </td>
            </tr>
            <tr>
              <td><strong>{`${_t('Chamber of Commerce #')}:`}</strong></td>
              <td>{organizationDetails.identification_number}</td>
            </tr>
            <tr>
              <td><strong>{`${_t('GGN')}:`}</strong></td>
              <td>{organizationDetails.ggn_number}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }

}

OrganizationDetailsTable.propTypes = {
  organizationDetails: React.PropTypes.object,
  closePopup: React.PropTypes.func,
};
