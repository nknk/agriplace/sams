import React from 'react';
import { OverlayTrigger, Popover } from 'react-bootstrap';
import './OrganizationDetails.less';
import { OrganizationDetailsTable } from './OrganizationDetailsTable.jsx';

export class OrganizationDetails extends React.Component {

  constructor() {
    super();
    this.state = {
      isOpen: true,
      show: true,
    };
    this.toggle = this.toggle.bind(this);
    this.popupTriggered = this.popupTriggered.bind(this);
  }

  toggle() {
    const node = React.findDOMNode(this.organizationOverlayTrigger);
    node.click();
  }

  popupTriggered() {
    const organizationUuid = this.props.assessmentInfo.organization_uuid;
    this.props.requestOrganizationDetails(organizationUuid);
  }

  render() {
    const customPopOverRight = (
      <Popover
        id={`popover-${this.props.assessmentInfo.workflow_uuid}`}
        className="organization-details-popup"
      >
        <OrganizationDetailsTable
          organizationDetails={this.props.organizationDetails}
          closePopup={this.toggle}
        />
      </Popover>
    );

    return (
      <OverlayTrigger
        rootClose
        trigger="click"
        placement="right"
        overlay={customPopOverRight}
        ref={o => { this.organizationOverlayTrigger = o; }}
        onClick={this.popupTriggered}
      >
        <div className="organization-details">
          <div className="organization-name">
            <div className="holder">
              {this.props.assessmentInfo.organization_name}
            </div>
          </div>
        </div>
      </OverlayTrigger>
    );
  }

}

OrganizationDetails.propTypes = {
  assessmentInfo: React.PropTypes.object,
  organizationDetails: React.PropTypes.object,
  requestOrganizationDetails: React.PropTypes.func,
};
