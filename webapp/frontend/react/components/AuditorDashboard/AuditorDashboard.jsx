import React from 'react';
import { AuditorDashboardFilters } from './AuditorDashboardFilters.jsx';
import AuditorDashboardTable from './AuditorDashboardTable.jsx';
import { AuditorDashboardTopbar } from './AuditorDashboardTopbar.jsx';
import { Pagination } from '../Utils/pagination.jsx';

const AuditorDashboard = React.createClass({
  propTypes: {
    role: React.PropTypes.string,
    assessments: React.PropTypes.array,
    auditors: React.PropTypes.array,
    pageNumber: React.PropTypes.number,
    pageCount: React.PropTypes.number,
    isLoading: React.PropTypes.number,
    organizationDetails: React.PropTypes.object,
  },

  mixins: [
    FluxMixin,
  ],

  getInitialState() {
    return {
      height: 700,
    };
  },

  handleDateChange({ workflowUuid, date }) {
    const flux = this.getFlux();
    flux.actions.dashboard.changeAuditDate({ workflowUuid, date });
  },

  handleAuditorChange({ workflowUuid, auditor }) {
    const flux = this.getFlux();
    flux.actions.dashboard.changeAuditor({ workflowUuid, auditor });
  },

  handlePageChange(pageNumber) {
    const flux = this.getFlux();
    flux.actions.dashboard.applyPageNumberFilter(pageNumber);
  },

  render() {
    const flux = this.getFlux();
    const requestOrganizationDetails = flux.actions.dashboard.getOrganizationDetails;
    return (
      <div>
        <div className="row">
          <div className="col-xs-12">

            <AuditorDashboardTopbar />
            <div className="col-xs-2">
              <AuditorDashboardFilters
                onChange={this.handleFilterChange}
                role={this.props.role}
              />
            </div>
            <div className="col-xs-10">
              <AuditorDashboardTable
                ref="AuditorDashboardTable"
                assessments={this.props.assessments}
                onDateChange={this.handleDateChange}
                onAuditorChange={this.handleAuditorChange}
                role={this.props.role}
                auditors={this.props.auditors}
                isLoading={this.props.isLoading}
                organizationDetails={this.props.organizationDetails}
                requestOrganizationDetails={requestOrganizationDetails}
              />
              <Pagination
                numberOfPages={this.props.pageCount}
                handlePageChange={this.handlePageChange}
                activePage={this.props.pageNumber}
              />
            </div>
          </div>
        </div>
      </div>
    );
  },
});

export default AuditorDashboard;
