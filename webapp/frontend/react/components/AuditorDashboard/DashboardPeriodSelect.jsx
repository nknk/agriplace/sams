const aptools = require('../../../lib/ap-tools.js');
const moment = require('moment');
import { DaterangePicker } from '../DaterangePicker/DaterangePicker.jsx';
import React from 'react';

require('./DashboardPeriodSelect.less');

export const DashboardPeriodSelect = React.createClass({

  propTypes: {
    onChange: React.PropTypes.func,
  },

  getInitialState() {
    const year = +moment().format('YYYY');
    const period = {
      start: null,
      end: null,
    };
    return {
      period,
      year,
      yearRange: this.getYearRange(year),
    };
  },

  handleYearSelect(e) {
    this.dateRangePicker.reset();

    const year = e.target.value === 'all' ? null : +e.target.value;
    const yearRange = this.getYearRange(year);

    const period = {
      start: null,
      end: null,
    };

    this.props.onChange({ type: 'year', value: year });

    this.setState({
      year,
      period,
      yearRange,
    });
  },

  getYearRange(year) {
    const years = aptools.getProdutYears(2015);
    const start = year ? moment(`01-01-${year}`, 'DD-MM-YYYY') : moment(`01-01-${years[0]}`, 'DD-MM-YYYY');
    const end = year ? moment(`31-12-${year}`, 'DD-MM-YYYY') : moment(`31-12-${years[years.length - 1]}`, 'DD-MM-YYYY');
    return {
      start,
      end,
    };
  },

  handleRangeSelect({ type, date }) {
    const { period } = this.state;
    _.assign(period, { [type]: date });

    if (period.start && period.end) {
      const start = moment(period.start, 'DD-MM-YYYY').format('YYYY-MM-DD');
      const end = moment(period.end, 'DD-MM-YYYY').format('YYYY-MM-DD');
      this.props.onChange({ type: 'audit_period', value: { start, end } });
    }

    this.setState({ period });
  },

  resetSelectedYear() {
    const year = null;
    this.setState({
      year,
      yearRange: this.getYearRange(year),
    });
  },

  render() {
    const years = aptools.getProdutYears(2015);

    const options = {
      startDate: moment(this.state.yearRange.start).format('DD-MM-YYYY'),
      endDate: moment(this.state.yearRange.end).format('DD-MM-YYYY'),
      format: 'DD-MM-YYYY',
    };

    return (
      <div className="DashboardPeriodSelect">
        <strong>{_t('Year')}</strong>
        <select value={this.state.year} className="form-control year" onChange={this.handleYearSelect}>
          <option value="all">{_t('All years')}</option>
          {years.map(year => <option key={year} value={year}>{year}</option>)}
        </select>

        <div className="wrap"><div className="or">of</div></div>
        <div><strong>{_t('Audit period')}</strong></div>
        <DaterangePicker
          ref={(input) => { this.dateRangePicker = input; }}
          options={options}
          onDateSelect={this.handleRangeSelect}
        />
      </div>
    );
  },
});
