import React from 'react';

require('./AuditorDashboardTopbar.less');

export const AuditorDashboardTopbar = React.createClass({
  mixins: [
    FluxMixin,
  ],

  onEnterKeyPressed(e) {
    if (e.charCode === 13) {
      this.inputCallback();
    }
  },

  onSearchButtonPressed(e) {
    e.preventDefault();
    e.stopPropagation();
    this.inputCallback();
  },

  inputCallback() {
    const search = this.refs.searchInput.getDOMNode().value;
    this.getFlux().actions.dashboard.applySearchFilter(search);
  },

  render() {
    return (
      <div className="AuditorDashboardTopbar row">
        <div className="col-xs-10 col-xs-offset-2">
          <div className="col-xs-4">
            <div className="input-group">
              <input
                type="search"
                ref="searchInput"
                className="form-control"
                placeholder={_t('Organization or relation#')}
                onKeyPress={this.onEnterKeyPressed}
              />
              <span className="input-group-btn">
                <button className="btn btn-default" type="submit" onClick={this.onSearchButtonPressed}>
                  <i className="fa fa-search"></i>
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  },
});
