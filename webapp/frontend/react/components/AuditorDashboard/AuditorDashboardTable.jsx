import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import { Table, Column } from 'fixed-data-table';
import DateTimePickerInATable from '../DatetimePicker/DateTimePickerInATable.jsx';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import { OrganizationDetails } from './OrganizationDetails/OrganizationDetails.jsx';
const BusyFeedback = require('../BusyFeedback');
require('./AuditorDashboardTable.less');

const SortTypes = {
  DESC: 'DESC',
  ASC: 'ASC',
};

class AuditorDashboardTable extends Component {

  constructor() {
    super();

    this.state = {
      sortBy: '',
      sortDir: SortTypes.ASC,
    };

    this.onHeadingClick = this.onHeadingClick.bind(this);
    this.onPlannedAuditDateChange = this.onPlannedAuditDateChange.bind(this);
    this.onAuditorChange = this.onAuditorChange.bind(this);
    this.renderCertificate = this.renderCertificate.bind(this);
    this.renderAuditorSelect = this.renderAuditorSelect.bind(this);
    this.renderAuditDate = this.renderAuditDate.bind(this);
    this.cellDataGetter = this.cellDataGetter.bind(this);
    this.sortByColumn = this.sortByColumn.bind(this);
    this._rowGetter = this._rowGetter.bind(this);
    this._renderHeader = this._renderHeader.bind(this);
    this.getTableParams = this.getTableParams.bind(this);
    this.renderOrganizationName = this.renderOrganizationName.bind(this);
  }

  componentWillMount() {
    this.sortByColumn('audit_date_planned', 'ASC', this.props.assessments);
  }

  componentWillReceiveProps(nextProps) {
    // fixed-data-table has very strict ShouldComponentUpdate :(, works for auditor dropdown
    const assessments = _.cloneDeep(nextProps.assessments);
    this.sortByColumn(this.state.sortBy, this.state.sortDir, assessments);
  }

  onHeadingClick(cellDataKey) {
    const sortDir = (this.state.sortDir === 'ASC') && (cellDataKey === this.state.sortBy) ? 'DESC' : 'ASC';
    this.sortByColumn(cellDataKey, sortDir, this.state.assessments);
  }

  onPlannedAuditDateChange(workflowUuid, dateObject) {
    const date = moment(dateObject).format('YYYY-MM-DDTHH:mm:ss.SSS');
    this.props.onDateChange({ workflowUuid, date });
  }

  onAuditorChange(workflowUuid, event) {
    this.props.onAuditorChange({ workflowUuid, auditor: event.target.value });
  }

  renderCertificate(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
    return (<div>
      {
        rowData.assessment.uuid
        ? <a href={rowData.assessment.url} target="_blank">{cellData}</a>
          : <span>{cellData}</span>
      }

    </div>);
  }

  renderAuditorSelect(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
    return (
      <PermissionController key={rowData.workflow_uuid} component="AssignAuditor" workflowUuid={rowData.workflow_uuid}>
        <select
          className="form-control"
          value={rowData.auditor}
          onChange={this.onAuditorChange.bind(this, rowData.workflow_uuid)}
        >
          {this.props.auditors.map(auditor => <option key={auditor.code} value={auditor.code}>{auditor.label}</option>)}
        </select>
      </PermissionController>
      );
  }

  renderAuditDate(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
    const isDateValid = cellData && moment(cellData).isValid();
    const date = isDateValid ? moment(cellData).format('DD-MM-YYYY HH:mm') : undefined;

    return (
      <PermissionController key={rowData.workflow_uuid} component="AuditDate" workflowUuid={rowData.workflow_uuid}>
        <DateTimePickerInATable
          assessmentInfo={rowData}
          date={date}
          onChange={this.onPlannedAuditDateChange}
          isDateValid={isDateValid}
        />
      </PermissionController>
    );
  }

  renderOrganizationName(cellData, cellDataKey, rowData, rowIndex, columnData, width) {
    const { organizationDetails, requestOrganizationDetails } = this.props;
    const details = organizationDetails && organizationDetails[rowData.organization_uuid];
    return (
      <OrganizationDetails
        assessmentInfo={rowData}
        organizationDetails={details}
        requestOrganizationDetails={requestOrganizationDetails}
      />
    );
  }

  cellDataGetter(cellDataKey, rowData) {
    return _.get(rowData, cellDataKey);
  }

  sortByColumn(sortBy, direction, assessments) {
    const sortedAscAssessments = _.sortBy(assessments, n => {
      if (sortBy === 'preferred_monthLabel') {
        return n.preferred_month || '';
      }

      return _.get(n, sortBy) || '';
    });
    const sortedAssessments = direction === 'DESC' ? sortedAscAssessments.reverse() : sortedAscAssessments;

    this.setState({
      assessments: sortedAssessments,
      sortBy,
      sortDir: direction,
    });
  }

  _rowGetter(rowIndex) {
    return this.state.assessments[rowIndex];
  }

  _renderHeader(label, cellDataKey) {
    return (
      <a onClick={this.onHeadingClick.bind(this, cellDataKey)}>{label}</a>
    );
  }

  getTableParams(isCoordinator, assessments) {
    const width = isCoordinator ? 1280 : 1070;
    const rowsCount = assessments.length;
    const rowHeight = 45;
    const rowGetter = this._rowGetter;
    const headerHeight = 50;
    // Scroll to the top after filtering
    const scrollToRow = 0;
    const height = headerHeight + (rowHeight * rowsCount) + 50;
    const maxHeight = height;
    return { width, rowsCount, rowHeight, height, headerHeight, rowGetter, scrollToRow, maxHeight };
  }

  render() {
    if (this.props.isLoading) {
      return <BusyFeedback />;
    }

    let sortDirArrow = '';

    if (this.state.sortDir !== null) {
      sortDirArrow = this.state.sortDir === SortTypes.DESC ? ' ↓' : ' ↑';
    }

    const isCoordinator = this.props.role === 'COORDINATOR';

    const tableParams = this.getTableParams(isCoordinator, this.state.assessments);

    return (
      <div className="AuditorDashboardTable">
        <Table {...tableParams}>
          <Column
            headerRenderer={this._renderHeader}
            label={_t('Organization') + (this.state.sortBy === 'organization_name' ? sortDirArrow : '')}
            width={210}
            dataKey="organization_name"
            cellRenderer={this.renderOrganizationName}
          />
          <Column
            headerRenderer={this._renderHeader}
            label={_t('Relation number') + (this.state.sortBy === 'reference_number' ? sortDirArrow : '')}
            width={140}
            dataKey="membership_number"
          />
          <Column
            headerRenderer={this._renderHeader}
            label={_t('Certificate') + (this.state.sortBy === 'assessment.typeLabel' ? sortDirArrow : '')}
            cellRenderer={this.renderCertificate}
            width={140}
            cellDataGetter={this.cellDataGetter}
            dataKey="assessment.typeLabel"
          />
          <Column
            headerRenderer={this._renderHeader}
            label={_t('State') + (this.state.sortBy === 'assessment.statusLabel' ? sortDirArrow : '')}
            width={200}
            cellDataGetter={this.cellDataGetter}
            dataKey="assessment.statusLabel"
          />
          <Column
            headerRenderer={this._renderHeader}
            label={_t('Planned inspection date/time') + (this.state.sortBy === 'audit_date_planned' ?
              sortDirArrow : '')}
            width={240}
            cellRenderer={this.renderAuditDate}
            dataKey="audit_date_planned"
          />
          {
            isCoordinator
            ? (
              <Column
                headerRenderer={this._renderHeader}
                label={_t('Internal inspector') + (this.state.sortBy === 'auditor' ? sortDirArrow : '')}
                width={210}
                cellRenderer={this.renderAuditorSelect}
                dataKey="auditor"
              />
            )
            : null
          }
          <Column
            headerClassName="table-header"
            headerRenderer={this._renderHeader}
            label={_t('Preference period') + (this.state.sortBy === 'preferred_month.label' ? sortDirArrow : '')}
            width={140}
            cellDataGetter={this.cellDataGetter}
            dataKey="preferred_monthLabel"
          />
        </Table>
      </div>
    );
  }
}

AuditorDashboardTable.propTypes = {
  assessments: PropTypes.array,
  auditors: PropTypes.array,
  role: PropTypes.string,
  onDateChange: PropTypes.func,
  onAuditorChange: PropTypes.func,
  sidebarHeight: PropTypes.number,
  isLoading: PropTypes.number,
  organizationDetails: PropTypes.object,
  requestOrganizationDetails: PropTypes.func,
};

export default AuditorDashboardTable;
