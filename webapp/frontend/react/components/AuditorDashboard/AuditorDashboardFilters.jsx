import React from 'react';
const Fluxxor = require('fluxxor');
const FluxMixin = Fluxxor.FluxMixin(React);
const StoreWatchMixin = Fluxxor.StoreWatchMixin;
import { ItemFilter } from '../ItemFilter/ItemFilterStateless.jsx';
import { DashboardPeriodSelect } from './DashboardPeriodSelect.jsx';
const BusyFeedback = require('../BusyFeedback');

export const AuditorDashboardFilters = React.createClass({
  propTypes: {
    role: React.PropTypes.string,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('DashboardStore'),
  ],

  getStateFromFlux() {
    const flux = this.getFlux();
    const dashboardState = flux.store('DashboardStore').getState();

    return {
      assessmentStates: dashboardState.assessmentStates,
      isAssessmentStatesFetched: dashboardState.isAssessmentStatesFetched,
      isAssessmentStatesLoading: dashboardState.isAssessmentStatesLoading,
      assessmentTypes: dashboardState.assessmentTypes,
      isAssessmentTypesFetched: dashboardState.isAssessmentTypesFetched,
      isAssessmentTypesLoading: dashboardState.isAssessmentTypesLoading,
      internalInspectors: dashboardState.internalInspectors,
      isInternalInspectorsFetched: dashboardState.isInternalInspectorsFetched,
      isInternalInspectorsLoading: dashboardState.isInternalInspectorsLoading,
      filters: dashboardState.filters,
    };
  },

  handleFilterChange({ type, value }) {
    const flux = this.getFlux();

    if (type === 'assessmentTypes') {
      flux.actions.dashboard.applyAssessmentTypesFilter(value);
    } else if (type === 'assessmentStates') {
      flux.actions.dashboard.applyAssessmentStatesFilter(value);
    } else if (type === 'year') {
      flux.actions.dashboard.applyYearFilter(value);
      // update filters data for selected year
      flux.actions.dashboard.getAssessmentTypesForFilters(value);
      flux.actions.dashboard.getAssessmentStatesForFilters(value);
      if (this.props.role === 'COORDINATOR') {
        flux.actions.dashboard.getInternalInspectorsForFilters(value);
      }
    } else if (type === 'audit_period') {
      const startDate = value.start;
      const endDate = value.end;
      flux.actions.dashboard.applyDateRangeFilter({ startDate, endDate });
    } else if (type === 'auditor') {
      flux.actions.dashboard.applyInternalInspectorFilter(value);
    }
  },

  render() {
    const { isAssessmentTypesFetched, isAssessmentStatesFetched, isInternalInspectorsFetched,
      assessmentTypes, assessmentStates, internalInspectors, filters,
      isAssessmentTypesLoading, isAssessmentStatesLoading, isInternalInspectorsLoading } = this.state;
    if (!isAssessmentTypesFetched
      || !isAssessmentStatesFetched
      || (this.props.role === 'COORDINATOR' && !isInternalInspectorsFetched)
    ) {
      return <BusyFeedback />;
    }

    return (
      <div>
        <strong>{_t('Filters')}</strong>
        <DashboardPeriodSelect onChange={this.handleFilterChange} />
        {
          assessmentTypes && assessmentTypes.length ? (
            <div>
              <strong>{_t('Certificate')}</strong>
              {' '}
              {
                isAssessmentTypesLoading ? (
                  <span className="icon fa fa-spinner fa-spin fa" />
                ) : null
              }
              <ItemFilter
                items={assessmentTypes}
                selectedCodes={filters.assessmentTypes}
                onChange={this.handleFilterChange}
                type="assessmentTypes"
              />
            </div>
          ) : null
        }
        {
          assessmentStates && assessmentStates.length ? (
            <div>
              <strong>{_t('Status')}</strong>
              {' '}
              {
                isAssessmentStatesLoading ? (
                  <span className="icon fa fa-spinner fa-spin fa" />
                ) : null
              }
              <ItemFilter
                items={assessmentStates}
                selectedCodes={filters.assessmentStates}
                onChange={this.handleFilterChange}
                type="assessmentStates"
              />
            </div>
          ) : null
        }
        {
          this.props.role === 'COORDINATOR' && internalInspectors && internalInspectors.length ? (
            <div>
              <strong>{_t('Internal inspector')}</strong>
              {' '}
              {
                isInternalInspectorsLoading ? (
                  <span className="icon fa fa-spinner fa-spin fa" />
                ) : null
              }
              <ItemFilter
                items={internalInspectors}
                selectedCodes={filters.internalInspectors}
                onChange={this.handleFilterChange}
                type="auditor"
              />
            </div>
          ) : null
        }
      </div>
    );
  },
});
