import React, { Component } from 'react';
const tForm = require('tcomb-form');
const Form = tForm.form.Form;
const utils = require('../FormBuilder/formBuilderUtils.js');
import moment from 'moment';
import { CertificateStruct } from './CertificateStruct.js';
import { productsLayout, layout } from './CreateCertificateLayout.js';
import datepicker from './Datepicker.jsx';
import selectFactory from '../../factories/SelectFactory.jsx';
import attachFileFactory from '../../factories/AttachFileFactory.jsx';
import _ from 'lodash';

const i18n = {
  add: _t('Add'),
  down: _t('Down'),
  optional: _t('Optional'),
  required: _t('Required'),
  remove: _t('Remove'),
  up: _t('Up'),
};

export class CreateCertificateForm extends Component {
  constructor(props) {
    super(props);
    const { certificate } = props;
    this.state = {
      value: this.setValue(certificate),
    };
  }

  componentWillMount() {
    const { certificateTypes, productTypes } = this.props;
    this.Certificate = CertificateStruct(certificateTypes, productTypes);
  }

  shouldComponentUpdate() {
    // turn off re-rendering
    // so as to keep the form in previous state when showing loader
    return false;
  }

  setValue(value) {
    if (!value) {
      return null;
    }
    const normalised = this.reverseFormat(utils.keepStringsformat(value));
    return _.cloneDeep(normalised);
  }

  reverseFormat(value) {
    return {
      certificateType: value.certificate_type.uuid,
      certificateNumber: value.certificate_number,
      issueDate: moment(value.issue_date, 'YYYY-MM-DD').format('DD-MM-YYYY'),
      expiryDate: moment(value.expiry_date, 'YYYY-MM-DD').format('DD-MM-YYYY'),
      auditor: value.auditor,
      products: value.crops.map(c => ({
        area: c.total_surface,
        productType: c.product_type,
      })),
      certificateFiles: value.certificate_files && value.certificate_files.map(attachmentLink => ({
        uuid: attachmentLink.uuid,
        title: attachmentLink.title,
        fileName: attachmentLink.original_file_name,
        fileUrl: attachmentLink.file,
      })),
      auditReportFiles: value.audit_report_files && value.audit_report_files.map(attachmentLink => ({
        uuid: attachmentLink.uuid,
        title: attachmentLink.title,
        fileName: attachmentLink.original_file_name,
        fileUrl: attachmentLink.file,
      })),
    };
  }

  getValue() {
    const value = this.refs.form.getValue();
    if (!value) {
      return null;
    }
    const normalised = this.format(utils.keepStringsformat(value));
    return _.cloneDeep(normalised);
  }

  format(value) {
    const { certificate } = this.props;

    const certificateFiles = value.certificateFiles && value.certificateFiles.map(file => (
      file.uuid ? (
        certificate.certificate_files.find(cF => cF.uuid === file.uuid)
      ) : ({
        title: file.title,
        file: file.dataUrl,
        original_file_name: file.fileName,
      })
    ));

    // // remove empty files
    _.remove(certificateFiles, (n) => !n);

    const auditReportFiles = value.auditReportFiles && value.auditReportFiles.map(file => (
      file.uuid ? (
        certificate.audit_report_files.find(aF => aF.uuid === file.uuid)
      ) : ({
        title: file.title,
        file: file.dataUrl,
        original_file_name: file.fileName,
      })
    ));
    _.remove(auditReportFiles, (n) => !n);

    const crops = value.products && value.products.map(ps => ({
      total_surface: ps.area,
      product_type: ps.productType,
    }));
    _.remove(crops, (n) => !n.product_type);

    return {
      certificate_type: value.certificateType,
      certificate_number: value.certificateNumber,
      issue_date: moment(value.issueDate, 'DD-MM-YYYY').format('YYYY-MM-DD'),
      expiry_date: moment(value.expiryDate, 'DD-MM-YYYY').format('YYYY-MM-DD'),
      auditor: value.auditor,
      crops: crops || [],
      certificate_files: certificateFiles || [],
      audit_report_files: auditReportFiles || [],
    };
  }

  clear() {
    this.setState({ value: null });
  }

  render() {
    let limitExceedMessage = `${_t('The selected file exceeds the limit of 10 MB')}.`;
    limitExceedMessage = `${limitExceedMessage} ${_t('Please select another file or compress the current file')}.`;

    let invalidExtensionMessage = `${_t('The file type of the selected file is not supported')}.`;
    invalidExtensionMessage = `${invalidExtensionMessage} ${_t('The supported file types are')}:`;
    invalidExtensionMessage = `${invalidExtensionMessage} ${_t('PNG, PDF, JPEG, DOC and DOCX')}.`;

    const allowedExtensions = ['png', 'jpeg', 'jpg', 'pdf', 'doc', 'docx'];
    const allowedMimeTypes = [
      'image/png',
      'image/jpeg',
      'application/pdf',
      'application/msword',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    ];

    const options = {
      i18n,
      fields: {
        issueDate: {
          factory: datepicker,
        },
        expiryDate: {
          factory: datepicker,
        },
        products: {
          disableOrder: true,
          attrs: {
            className: 'products',
          },
          item: {
            fields: {
              productType: {
                factory: selectFactory,
                label: _t('Crop'),
              },
              area: {
                label: _t('Total surface (Ha)'),
                error: _t('Only numbers and points (for decimals) are allowed here'),
              },
            },
            template: productsLayout,
          },
        },
        certificateFiles: {
          disableOrder: true,
          attrs: {
            className: 'attachment-files',
          },
          item: {
            factory: attachFileFactory,
            config: {
              sizeLimit: 10000000, // 10 MB
              allowedExtensions,
              allowedMimeTypes,
            },
            error: {
              limitExceedMessage,
              invalidExtensionMessage,
            },
          },
        },
        auditReportFiles: {
          disableOrder: true,
          attrs: {
            className: 'attachment-files',
          },
          item: {
            factory: attachFileFactory,
            config: {
              sizeLimit: 10000000, // 10 MB
              allowedExtensions,
              allowedMimeTypes,
            },
            error: {
              limitExceedMessage,
              invalidExtensionMessage,
            },
          },
        },
      },
      auto: 'none',
      template: layout,
    };

    return (
      <div>
        <Form
          ref="form"
          value={this.state.value}
          type={this.Certificate}
          options={options}
        />
      </div>
    );
  }
}

CreateCertificateForm.propTypes = {
  certificateTypes: React.PropTypes.array,
  productTypes: React.PropTypes.array,
  certificate: React.PropTypes.object,
};
