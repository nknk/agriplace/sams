const t = require('tcomb');
const moment = require('moment');
const isoDate = t.refinement(t.String, (date) => moment(date, 'DD-MM-YYYY').isValid());
const sizeLimit = t.refinement(t.Bool, (isExceeded) => !isExceeded);
const invalidExtension = t.refinement(t.Bool, (isInvalid) => !isInvalid);

const format = (collection) => (
  collection.reduce((res, cur) => {
    res[cur.uuid] = cur.name;
    return res;
  }, {})
);

const attachmentLink = t.struct({
  fileName: t.String,
  title: t.maybe(t.String),
  fileUrl: t.maybe(t.String),
  uuid: t.maybe(t.String),
  dataUrl: t.maybe(t.String),
  sizeLimitExceeded: t.maybe(sizeLimit),
  isExtensionInvalid: t.maybe(invalidExtension),
});

export const CertificateStruct = (certificateTypes, productTypes) => (
  t.struct({
    certificateType: t.enums(format(certificateTypes)),
    certificateNumber: t.maybe(t.String),
    issueDate: isoDate,
    expiryDate: isoDate,
    auditor: t.maybe(t.String),
    products: t.maybe(t.list(t.struct({
      productType: t.maybe(t.enums(format(productTypes))),
      area: t.maybe(t.Number),
    }))),
    certificateFiles: t.maybe(t.list(t.maybe(attachmentLink))),
    auditReportFiles: t.maybe(t.list(t.maybe(attachmentLink))),
  })
);
