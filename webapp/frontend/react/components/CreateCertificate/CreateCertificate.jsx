import React from 'react';
import { CreateCertificateForm } from './CreateCertificateForm.jsx';
import { Button } from 'react-bootstrap';
import pageData from '../Utils/pageData';
import { CertificateDeleteButton } from '../Certificates/CertificateDeleteButton.jsx';
import { CertificateDeleteModal } from '../Certificates/CertificateDeleteModal.jsx';
import ProgressModal from '../Utils/ProgressModal.jsx';
require('./CreateCertificate.less');

const { uuid: organizationUuid } = pageData.context;
export const CreateCertificate = React.createClass({
  propTypes: {
    params: React.PropTypes.object,
    history: React.PropTypes.object,
    certificateTypes: React.PropTypes.array,
    productTypes: React.PropTypes.array,
    certificate: React.PropTypes.object,
    createOrUpdateCertificateInProgress: React.PropTypes.bool.isRequired,
  },

  mixins: [
    FluxMixin,
  ],

  onCancelClick() {
    this.refs.CreateCertificateForm.clear();
    this.navigateToCerts();
  },

  onDeleteClick() {
    this.refs.DeleteCertificateModal.changeVisbility(true);
  },

  onDeleteConfirmation(certificateUuid) {
    const flux = this.getFlux();
    flux.actions.certificates.deleteCertificate({
      organizationUuid,
      certificateUuid,
      cb: this.navigateToCerts,
    });
  },

  onSaveClick() {
    const certificate = this.refs.CreateCertificateForm.getValue();
    if (certificate) {
      if (this.props.certificate) {
        certificate.uuid = this.props.certificate.uuid;
        this.getFlux().actions.certificates.update({
          organizationUuid,
          certificate,
          cb: this.navigateToCerts,
        });
      } else {
        this.getFlux().actions.certificates.create({
          organizationUuid,
          certificate,
          cb: this.navigateToCerts,
        });
      }
    }
  },

  notOnDeleteConfirmation() {
    this.refs.DeleteCertificateModal.changeVisbility(false);
  },

  navigateToCerts() {
    const { membershipUuid, assessmentAccessMode } = this.props.params;
    this.props.history.pushState(null, `/${membershipUuid}/${assessmentAccessMode}/certifications`);
  },

  render() {
    const { certificateTypes, productTypes, certificate, createOrUpdateCertificateInProgress } = this.props;

    return (
      <div className="CreateCertificate">
        <CreateCertificateForm
          certificateTypes={certificateTypes}
          productTypes={productTypes}
          ref="CreateCertificateForm"
          certificate={certificate}
        />
        <div className="buttons">
          <Button
            bsStyle="success"
            onClick={this.onSaveClick}
          >
            {_t('Save')}
          </Button>
          <Button
            bsStyle="default"
            onClick={this.onCancelClick}
          >
            {_t('Cancel')}
          </Button>
          {
            this.props.certificate ? (
              <CertificateDeleteButton
                onClick={this.onDeleteClick}
              />
            ) : null
          }
        </div>
        {
          this.props.certificate ? (
            <CertificateDeleteModal
              certificateName={this.props.certificate.certificate_type.name}
              certificateUuid={this.props.certificate.uuid}
              onDelete={this.onDeleteConfirmation}
              onCancel={this.notOnDeleteConfirmation}
              ref="DeleteCertificateModal"
            />
          ) : null
        }
        <ProgressModal isOpen={createOrUpdateCertificateInProgress} />
      </div>
    );
  },
});
