require('./CreateCertificateLayout.less');
export function layout(locals) {
  const inputs = locals.inputs;
  return (
    <div className="CreateCertificateLayout">
      <table>
        <tbody>
          <tr>
            <td className="title"><label>{_t('Certificate type')}</label></td>
            <td className="input">{inputs.certificateType}</td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Certificate number')}</label></td>
            <td className="input">{inputs.certificateNumber}</td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Issue date')}</label></td>
            <td className="input">{inputs.issueDate}</td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Expiry date')}</label></td>
            <td className="input">{inputs.expiryDate}</td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Auditor (Certification body)')}</label></td>
            <td className="input">{inputs.auditor}</td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Crops (optional)')}</label></td>
            <td className="products">{inputs.products}</td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Original certificate (optional)')}</label></td>
            <td className="products">
              <div className="divider"></div>
              {inputs.certificateFiles}
            </td>
          </tr>
          <tr>
            <td className="title"><label>{_t('Audit/inspection report (optional)')}</label></td>
            <td className="products">
              <div className="divider"></div>
              {inputs.auditReportFiles}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export function productsLayout(locals) {
  const inputs = locals.inputs;
  return (
    <div className="row">
      <div className="col-xs-7">{inputs.productType}</div>
      <div className="col-xs-5">{inputs.area}</div>
    </div>
  );
}
