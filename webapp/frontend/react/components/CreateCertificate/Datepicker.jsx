import React, { Component, PropTypes } from 'react';
import { validate } from 'tcomb-validation';
const DatePicker = require('../DatePicker/DatePicker.jsx');
require('./DatePicker.less');

export default class DatePickerFactory extends Component { // extend the base class
  constructor() {
    super();
    this.state = {
      value: '',
      isValid: true,
    };
    this.onChange = this.onChange.bind(this);
    this.validate = this.validate.bind(this);
    this.onClickDatePicker = this.onClickDatePicker.bind(this);
  }

  componentWillMount() {
    this.setState({
      value: this.props.value || '',
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      value: nextProps.value || '',
    });
  }

  onChange(value) {
    this.setState({ value });

    this.props.onChange(value, this.props.ctx.path);
  }

  onClickDatePicker(event) {
    event.preventDefault();
    this.refs.datePicker.getDOMNode().focus();
  }

  validate() {
    const validateIns = validate(this.state.value, this.props.type);
    this.setState({ isValid: validateIns.isValid() });

    return validateIns;
  }

  render() {
    const { value, isValid } = this.state;
    const { options } = this.props;
    const placeholder = options.attrs && options.attrs.placeholder ?
      this.props.options.attrs.placeholder : _t('Select Date');
    return (
      <DatePicker
        ref="datePicker"
        placeholder={placeholder}
        error={!isValid}
        initValue={value}
        onDateSelect={this.onChange}
      />
    );
  }
}

DatePickerFactory.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  ctx: PropTypes.obj,
  type: PropTypes.func,
  options: PropTypes.array,
};
