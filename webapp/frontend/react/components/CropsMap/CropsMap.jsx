

var Map = require('react-leaflet').Map;
var Marker = require('react-leaflet').Marker;
var Popup = require('react-leaflet').Popup;
var TileLayer = require('react-leaflet').TileLayer;
var Polygon = require('react-leaflet').Polygon;

require('leaflet/dist/leaflet.css');
require('leaflet.fullscreen/Control.FullScreen.css');
require('leaflet.fullscreen');

var CropsMap = React.createClass({

  propTypes: {
    crops: React.PropTypes.array.isRequired,
  },

  componentDidMount() {
    var bounds = this.props.crops.reduce((result, crop) => result.concat(crop.polygon), []);
    this.refs.map.getLeafletElement().fitBounds(bounds);
  },

  componentWillReceiveProps(nextProps) {
    var bounds = nextProps.crops.reduce((result, crop) => result.concat(crop.polygon), []);
    this.refs.map.getLeafletElement().fitBounds(bounds);
  },

  onCropSelect(fieldId) {
    var bounds = this.props.crops.find((crop) => crop.field_id === fieldId).polygon;
    this.refs.map.getLeafletElement().fitBounds(bounds);
  },

  makePolygons(crops) {
    return crops.map((crop) => {
      return (
        <Polygon
          key={Math.random()}
          positions={crop.polygon}
          fillOpacity={0.5}
          fillColor={crop.color}
          color={crop.color}
      >
          <Popup><span>{crop.name},&nbsp;{Math.round(+crop.area * 10) / 10}&nbsp;{_t('ha')}</span></Popup>
        </Polygon>
      );
    });
  },

  render() {
    return (
      <Map ref="map" fullscreenControl fullscreenControlOptions={{ position: 'topleft' }} >
        <TileLayer
          url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        { this.makePolygons(this.props.crops) }
      </Map>
    );
  },

});

module.exports = CropsMap;
