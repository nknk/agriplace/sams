import React from 'react';
import moment from 'moment';
import pageData from '../Utils/pageData.js';
require('bootstrap-datepicker');

export const DaterangePicker = React.createClass({

  propTypes: {
    options: React.PropTypes.object,
    onDateSelect: React.PropTypes.func,
    initValue: React.PropTypes.string,
  },

  componentDidMount() {
    const el = React.findDOMNode(this);
    this.options = _.assign(this.options, this.props.options);
    const options = _.cloneDeep(this.options);
    options.format = options.format.toLowerCase();

    $(el).datepicker(options);
    $(el).datepicker('update');
    $(el).find('input').first().datepicker().on('changeDate', this.handleChange.bind(this, 'start'));
    $(el).find('input').last().datepicker().on('changeDate', this.handleChange.bind(this, 'end'));

    if (this.props.initValue) {
      const initValue = moment(this.props.initValue, options.format.toUpperCase()).toDate();
      $(el).datepicker('setDate', initValue);
      $(el).datepicker('update');
    }
  },

  componentWillReceiveProps(nextProps) {
    this.options = _.assign(this.options, nextProps.options);
    const options = _.cloneDeep(this.options);
    options.format = options.format.toLowerCase();

    $('.input-daterange input').each((index, element) => {
      $(element).datepicker('setStartDate', options.startDate);
      $(element).datepicker('setEndDate', options.endDate);
    });
  },

  componentWillUnmount() {
    // $(this.getDOMNode()).datepicker('destroy');
    $('.input-daterange input').each((index, element) => {
      $(element).datepicker('destroy');
    });
  },

  // defaults
  options: {
    autoclose: true,
    language: pageData.languageCode || 'en-US',
    format: 'DD-MM-YYYY',
  },

  reset() {
    // $('.input-daterange input').each(() => { $(this).val('').datepicker('update'); });
    $('.input-daterange input').each((index, element) => {
      $(element).datepicker('clearDates');
      $(element).datepicker('update', '');
    });
  },

  handleChange(type, event) {
    // refresh both datepickers after update;
    // $('.input-daterange input').each(() => { $(this).datepicker('update'); });
    $('.input-daterange input').each((index, element) => {
      $(element).datepicker('update');
    });

    const formattedDate = moment(event.date).format(this.options.format);
    this.props.onDateSelect({ type, date: formattedDate });
  },

  render() {
    return (
      <div className="DaterangePicker input-group input-daterange">
        <input
          style={{ marginBottom: 10 }}
          type="text"
          placeholder={_t('Start date')}
          className="form-control datepicker"
        />
        <input type="text" placeholder={_t('End date')} className="form-control datepicker" />
      </div>
    );
  },

});
