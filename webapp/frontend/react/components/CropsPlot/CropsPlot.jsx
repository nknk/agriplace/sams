

var CropsMap = require('../CropsMap/CropsMap.jsx');
var CropsLegend = require('../CropsLegend/CropsLegend.jsx');
var moment = require('moment');

var CropsPlot = React.createClass({

  propTypes: {
    plotsData: React.PropTypes.object.isRequired,
    onFetch: React.PropTypes.func.isRequired,
  },

  mixins: [
    FluxMixin,
    StoreWatchMixin('PlotsStore'),
  ],

  getInitialState() {
    var years = Object.keys(this.props.plotsData.rvo_data).map((year) => parseInt(year, 10));
    return {
      year: _.max(years),
    };
  },

  getStateFromFlux() {
    var flux = this.getFlux();
    var plotsStore = flux.store('PlotsStore');
    var plotsStoreState = plotsStore.getState();

    return {
      isRvoLoading: plotsStoreState.isRvoLoading,
      rvoErrors: plotsStoreState.rvoErrors,
    };
  },

  handleYearSelect(e) {
    this.setState({ year: e.target.value });
  },

  onCropSelect(cropUuid) {
    this.refs.map.onCropSelect(cropUuid);
  },

  onFetchButtonClick() {
    this.props.onFetch();
  },

  render() {
    var plotsData = this.props.plotsData.rvo_data;
    var modifiedDate = this.props.plotsData.last_modified;
    var crops = plotsData[this.state.year];
    var now = moment();
    var fetchButton = (
      <Bootstrap.Button
        bsStyle="success"
        disabled={this.state.isRvoLoading}
        onClick= { this.onFetchButtonClick }
    >
       {
        this.state.isRvoLoading
        ? <i className="icon fa fa-spinner fa-spin" style={{ marginRight: 5 }} />
        : null
       }
       { _t('Update') }
     </Bootstrap.Button>
    );

    return (
      <div className="CropsPlot">
        <div className="row">
          {
            this.state.rvoErrors
            ? this.state.rvoErrors.map((error) => <div className="alert alert-danger col-md-12">{error}</div>)
            : null
          }
        </div>
        <div className="row" style={{ lineHeight: '30px', padding: '10px 0' }}>
          <div className="col-md-12">
            <div className="pull-left">{_t('Year')}&nbsp;</div>
            <div className="pull-left">
              <select className="form-control" value={this.state.year} onChange={this.handleYearSelect}>
                { _.map(plotsData, (item, year) => <option key={year} value={year}>{year}</option>) }
              </select>
            </div>
            <div className="pull-right">{fetchButton}</div>
            <div className="pull-right" style={{ marginRight: 10 }}>
              {`Laatste update: ${moment(modifiedDate).locale('nl').format('DD-MM-YYYY')}`}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <CropsMap ref="map" crops={crops} />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <CropsLegend crops={crops} onCropSelect={this.onCropSelect} />
          </div>
        </div>
      </div>
    );
  },
});

module.exports = CropsPlot;
