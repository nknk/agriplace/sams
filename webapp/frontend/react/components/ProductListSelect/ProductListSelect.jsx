const ListView = require('../ListView/ListView.jsx');

require('./ProductListSelect.less');

const queryInputOptions = {
  placeholder: _t('Search product'),
  showSearchIcon: true,
};


const ProductListSelect = React.createClass({

  propTypes: {
    productListOptions: React.PropTypes.object.isRequired,
    listViewOptions: React.PropTypes.object.isRequired,
  },

  getInitialState() {
    return {
      products: this.props.listViewOptions.items,
      query: '',
    };
  },

  componentWillMount() {
    // if filterKeys is not given, use all keys from items objects
    this.filterKeys = this.props.listViewOptions.filterKeys ||
                      tools.allObjectKeys(this.props.listViewOptions.items);
    queryInputOptions.onInputChange = this.onInputChange;
  },

  onInputChange(query) {
    this.setState({ query });
  },


  onItemClicked(item) {
    this.props.productListOptions.onSelectionChange(tools.toggleUnique(this.props.selected, item));
  },


  onSelectAllToogle(isAll) {
    if (isAll) {
      this.groupSelect(this.props.listViewOptions.items);
    } else {
      this.groupSelect([]);
    }
  },

  groupSelect(items) {
    this.props.productListOptions.onSelectionChange(items);
  },

  render() {
    const listViewOptions = _.clone(this.props.listViewOptions);
    this.props.listViewOptions.onItemClicked = this.onItemClicked;
    this.props.listViewOptions.onSelectAllToogle = this.onSelectAllToogle;
    this.props.listViewOptions.items = tools.sortby(this.props.listViewOptions.items, this.props.listViewOptions.sortKeys);
    // apply queryInput filter to listView's items
    listViewOptions.items = tools.objectFilter(this.props.listViewOptions.items, this.state.query, this.filterKeys);
    listViewOptions.selected = this.props.selected;

    return (
      <div className="ProductListSelect">
        <ListView listViewOptions={listViewOptions} />
      </div>
    );
  },


});

module.exports = ProductListSelect;
