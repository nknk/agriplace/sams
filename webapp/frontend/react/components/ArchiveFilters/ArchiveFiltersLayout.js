require('./ArchiveFiltersLayout.less');
export function layout(locals) {
  const inputs = locals.inputs;
  return (
    <div className="ArchiveFiltersLayout">
      <div className="toolbar wide topOffset">
      </div>
      <table>
        <tr>
          <td>
            <div className="date-row">
              <div><strong>{_t('Period (date modified)')}</strong></div>
              <div className="input">{inputs.startDate}</div>
              <div className="input">{inputs.endDate}</div>
            </div>
          </td>
        </tr>
        <tr className="middleOffset">
        </tr>
        <tr>
          <td className="input"><strong>{_t('Filters')}</strong></td>
        </tr>
        <tr>
          <td className="input">{inputs.title}</td>
        </tr>
        <tr>
          <td className="input">{inputs.usedIn}</td>
        </tr>
        <tr>
          <td className="input">{inputs.usableFor}</td>
        </tr>
        <tr>
          <td className="input">{inputs.attachmentType}</td>
        </tr>
        <tr>
          <td className="input">{inputs.evidenceType}</td>
        </tr>
        <tr>
          <td className="input">{inputs.isOptionalEvidenceIncluded}</td>
        </tr>
      </table>
    </div>
  );
}
