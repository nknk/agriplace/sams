import React, { Component } from 'react';
const tForm = require('tcomb-form');
const Form = tForm.form.Form;
import moment from 'moment';
import { ArchiveFiltersStruct } from './ArchiveFiltersStruct.js';
import { layout } from './ArchiveFiltersLayout.js';
import datepicker from '../CreateCertificate/Datepicker.jsx';
import selectFactory from '../../factories/SelectFactory.jsx';
import searchFactory from '../../factories/SearchFactory.jsx';

const i18n = {
  add: _t('Add'),
  down: _t('Down'),
  optional: _t('Optional'),
  required: _t('Required'),
  remove: _t('Remove'),
  up: _t('Up'),
};

export class ArchiveFiltersForm extends Component {
  constructor() {
    super();
    this.state = {
      value: null,
    };
  }

  componentWillMount() {
    const { usedInAssessments, usableForAssessments, attachmentTypes, evidenceTypes } = this.props;
    this.Filters = ArchiveFiltersStruct(usedInAssessments, usableForAssessments, attachmentTypes, evidenceTypes);
  }

  onSearchClick(event) {
  }

  onChange(value) {
    this.state.value = value;
    const filters = {
      startDate: moment(value.startDate, 'DD-MM-YYYY').isValid() ?
        moment(value.startDate, 'DD-MM-YYYY').format('YYYY-MM-DD') : null,
      endDate: moment(value.endDate, 'DD-MM-YYYY').isValid() ?
        moment(value.endDate, 'DD-MM-YYYY').format('YYYY-MM-DD') : null,
      title: value.title || null,
      usedIn: value.usedIn || null,
      usableFor: value.usableFor || null,
      attachmentType: value.attachmentType || null,
      evidenceType: value.evidenceType || null,
      isOptionalEvidenceIncluded: value.isOptionalEvidenceIncluded ? '1' : '0',
    };
    this.props.filtersChanged(filters);
  }

  clear() {
    this.setState({ value: null });
  }

  render() {
    const options = {
      i18n,
      fields: {
        startDate: {
          factory: datepicker,
          attrs: {
            placeholder: _t('Start date'),
          },
        },
        endDate: {
          factory: datepicker,
          attrs: {
            placeholder: _t('End date'),
          },
        },
        title: {
          factory: searchFactory,
          attrs: {
            placeholder: _t('Title'),
          },
        },
        usedIn: {
          factory: selectFactory,
          attrs: {
            placeholder: _t('Used in (standard)'),
          },
        },
        usableFor: {
          factory: selectFactory,
          attrs: {
            placeholder: _t('Usable for (standard)'),
          },
        },
        attachmentType: {
          factory: selectFactory,
          attrs: {
            placeholder: _t('Attachment type'),
          },
        },
        evidenceType: {
          factory: selectFactory,
          attrs: {
            placeholder: _t('Evidence type'),
          },
        },
        isOptionalEvidenceIncluded: {
          label: _t('Include optional attachments'),
        },
      },
      auto: 'none',
      template: layout,
    };
    return (
      <div>
        <Form
          ref="form"
          value={this.state.value}
          type={this.Filters}
          options={options}
          onChange={_.debounce((value) => this.onChange(value), 300)}
        />
      </div>
    );
  }
}

ArchiveFiltersForm.propTypes = {
  usedInAssessments: React.PropTypes.array.isRequired,
  usableForAssessments: React.PropTypes.array.isRequired,
  attachmentTypes: React.PropTypes.array.isRequired,
  evidenceTypes: React.PropTypes.array.isRequired,
  filtersChanged: React.PropTypes.func.isRequired,
};
