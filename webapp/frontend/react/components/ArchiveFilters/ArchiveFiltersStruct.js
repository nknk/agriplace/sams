const t = require('tcomb');
const moment = require('moment');
const isoDate = t.refinement(t.String, (date) => moment(date, 'DD-MM-YYYY').isValid());

const format = (collection) => (
  collection.reduce((res, cur) => {
    res[cur.uuid] = cur.name;
    return res;
  }, {})
);

const formatEvidenceType = (collection) => (
  collection.reduce((res, cur) => {
    res[cur.uuid] = `${cur.code} ${cur.name}`;
    return res;
  }, {})
);

export const ArchiveFiltersStruct = (usedInAssessments, usableForAssessments, attachmentTypes, evidenceTypes) => (
  t.struct({
    startDate: t.maybe(isoDate),
    endDate: t.maybe(isoDate),
    title: t.maybe(t.String),
    usedIn: t.maybe(t.enums(format(usedInAssessments))),
    usableFor: t.maybe(t.enums(format(usableForAssessments))),
    attachmentType: t.maybe(t.enums(format(attachmentTypes))),
    evidenceType: t.maybe(t.enums(formatEvidenceType(evidenceTypes))),
    isOptionalEvidenceIncluded: t.maybe(t.Boolean),
  })
);
