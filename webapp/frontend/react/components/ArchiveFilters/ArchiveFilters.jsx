const BusyFeedback = require('../BusyFeedback');
import { ArchiveFiltersForm } from './ArchiveFiltersForm.jsx';

export const ArchiveFilters = React.createClass({
  mixins: [
    FluxMixin,
    StoreWatchMixin('AttachmentsStore'),
  ],

  componentWillMount() {
    const flux = this.getFlux();
    flux.actions.attachments.getEvidenceTypesList();
    flux.actions.attachments.getAttachmentTypesList();
    flux.actions.attachments.getUsedInList();
    flux.actions.attachments.getUsableForList();
  },

  getStateFromFlux() {
    const attachmentsStoreState = this.getFlux().store('AttachmentsStore').getState();

    return {
      evidenceTypesList: attachmentsStoreState.evidenceTypesList,
      attachmentTypesList: attachmentsStoreState.attachmentTypesList,
      usedInList: attachmentsStoreState.usedInList,
      usableForList: attachmentsStoreState.usableForList,
    };
  },

  onFiltersChanged(filters) {
    const flux = this.getFlux();
    flux.actions.attachments.applyFilters(filters);
  },
  //
  // Render
  //

  render() {
    const { evidenceTypesList, attachmentTypesList, usedInList, usableForList } = this.state;

    let result;
    if (evidenceTypesList.length &&
      attachmentTypesList.length &&
      usedInList.length &&
      usableForList.length
    ) {
      result = (
        <ArchiveFiltersForm
          usedInAssessments={usedInList}
          usableForAssessments={usableForList}
          attachmentTypes={attachmentTypesList}
          evidenceTypes={evidenceTypesList}
          filtersChanged={this.onFiltersChanged}
        />
      );
    } else {
      result = <BusyFeedback />;
    }
    return result;
  },
});
