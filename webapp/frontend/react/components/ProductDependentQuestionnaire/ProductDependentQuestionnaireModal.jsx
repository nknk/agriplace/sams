module.exports = React.createClass({

  propTypes: {
    productDependentQuestionnaires: React.PropTypes.string.isRequired,
    isProductAdded: React.PropTypes.bool.isRequired,
    showModal: React.PropTypes.bool.isRequired,
    onSuccess: React.PropTypes.func.isRequired,
  },

  mixins: [
    Bootstrap.OverlayMixin,
    FluxMixin,
    StoreWatchMixin('ProductsStore'),
  ],

  componentDidMount() {},

  getStateFromFlux() {
    const flux = this.getFlux();
    const productsStoreState = flux.store('ProductsStore').getState();
    return {
      products: productsStoreState.products,
    };
  },

  onSuccess() {
    if (this.props.onSuccess) {
      this.props.onSuccess();
    }
  },

  renderDependentQuestionnaires() {
    return _.map(this.props.productDependentQuestionnaires, (questionnaire) => {
      return (
        <Bootstrap.ListGroupItem key={questionnaire.uuid}>
          {questionnaire.name}
        </Bootstrap.ListGroupItem>
      );
    });
  },

  render() {
    let message = '';
    if (this.props.isProductAdded) {
      message = _t('Based on your crop selection the following questionnaire will be added to your assessment');
    } else {
      message = _t('Based on your crop selection the following questionnaire will be removed from your assessment');
    }
    return (
      <div>
        <Bootstrap.Modal show={this.props.showModal} backdrop={'static'} onRequestHide={null}>
          <Bootstrap.Modal.Header>
            <h2>{_t('Product dependent questionnaire')}</h2>
          </Bootstrap.Modal.Header>
          <Bootstrap.Modal.Body>
            <div className="row">
              <div className="col-xs-12">
                {message}
              </div>
              <div className="col-xs-12">
                <div className="input-group">
                  <div className="radio">
                    <Bootstrap.ListGroup>
                      {this.renderDependentQuestionnaires()}
                    </Bootstrap.ListGroup>
                  </div>
                </div>
              </div>
            </div>
          </Bootstrap.Modal.Body>
          <Bootstrap.Modal.Footer>
            <div className="btn-toolbar" role="toolbar">
              <div className="btn-group pull-right">
                <Bootstrap.Button
                  type="button"
                  id="assessment-select-add-button"
                  bsStyle="success"
                  onClick={this.onSuccess}
                >
                  {_t('OK')}
                </Bootstrap.Button>
              </div>
            </div>
          </Bootstrap.Modal.Footer>
        </Bootstrap.Modal>
      </div>
    );
  },
});
