import React from 'react';
const DatePicker = require('../DatePicker/DatePicker.jsx');
require('./EV55.less');

export class EV55 extends React.Component {
  constructor() {
    super();
    this.state = {
      columns: [
        { uuid: 'eeaf5336e56541e59db19037f7b85c82', value: '' },
      ],
      rows: [
        {
          uuid: '515df827761d49298f7addfb4d9d881b',
          pesticide: '',
          unit: '',
          quantity: [''],
        },
      ],
    };
  }

  componentWillMount() {
    const { value } = this.props;
    if (value) {
      this.setState({
        columns: value.columns,
        rows: value.rows,
      });
    }
  }

  onChange() {
    if (this.props.onChange) {
      this.props.onChange();
    }
  }

  handleDateChange(columnUuid, date) {
    let { columns } = this.state;
    let column = _.find(columns, columnVal => columnVal.uuid === columnUuid);
    column.value = date;

    this.setState({ columns }, this.onChange());
  }

  handlePesticideChange(rowUuid, e) {
    let { rows } = this.state;
    let row = _.find(rows, rowVal => rowVal.uuid === rowUuid);
    const pesticide = e.target.value;
    row.pesticide = pesticide;

    this.setState({ rows }, this.onChange());
  }

  handleUnitChange(rowUuid, e) {
    let { rows } = this.state;
    let row = _.find(rows, rowVal => rowVal.uuid === rowUuid);
    const unit = e.target.value;
    row.unit = unit;

    this.setState({ rows }, this.onChange());
  }

  handleQuantityChange(rowUuid, index, e) {
    let { rows } = this.state;
    let row = _.find(rows, rowVal => rowVal.uuid === rowUuid);
    const quantity = e.target.value;
    row.quantity[index] = quantity;

    this.setState({ rows }, this.onChange());
  }

  makeColumns() {
    return (
      <tr>
        <th></th>
        <th></th>
        <th></th>
        {
          this.state.columns.map((column) => {
            return (
              <th key={column.uuid}>
                <DatePicker
                  disabled={this.props.readOnly}
                  initValue={column.value}
                  onDateSelect={this.handleDateChange.bind(this, column.uuid)}
                  placeholder={'dd/mm/yyyy'}
            />
                {
                  this.state.columns.length > 1
                  ? <i title={_t('Delete this column')} className="fa fa-minus fa-1x delete-column-btn" onClick={this.handleRemoveColumnClick.bind(this, column.uuid)}></i>
                  : null
                }
              </th>
            );
          })
        }
      </tr>
    );
  }

  makeHeaders() {
    return (
      <tr>
        <th></th>
        <th>{_t('Crop protection product')}</th>
        <th>{_t('Unit')}</th>
        {
          this.state.columns.map(() => <th>{_t('Quantity')}</th>)
        }
      </tr>
    );
  }

  makeHeadersDate() {
    return (
      <tr>
        <th></th>
        <th></th>
        <th></th>
        {
          this.state.columns.map(() => <th>{_t('Date')}</th>)
        }
      </tr>
    );
  }

  makeRows() {
    return this.state.rows.map((item) => {
      return (
        <tr key={item.uuid}>
          <td>
            {
              this.state.rows.length > 1
              ? <i title={_t('Delete this row')} className="fa fa-minus fa-1x delete-row-btn" onClick={this.handleRemoveRowClick.bind(this, item.uuid)}></i>
              : null
            }
          </td>
          <td><Bootstrap.Input
            disabled={this.props.readOnly}
            onChange={this.handlePesticideChange.bind(this, item.uuid)}
            type="text"
            placeholder={_t('Name')}
            value={item.pesticide}
      /></td>
          <td><Bootstrap.Input
            disabled={this.props.readOnly}
            onChange={this.handleUnitChange.bind(this, item.uuid)}
            type="text"
            placeholder={_t('e.g. kg')}
            value={item.unit}
      /></td>
          {
            item.quantity.map((insideItem, index) => {
              return (
                <td key={this.state.columns[index].uuid}>
                  <Bootstrap.Input
                    disabled={this.props.readOnly}
                    onChange={this.handleQuantityChange.bind(this, item.uuid, index)}
                    type="text"
                    placeholder={_t('Amount')}
                    value={insideItem}
              />
                </td>
              );
            })
          }
        </tr>
        );
    });
  }

  handleAddRowClick() {
    const { rows, columns } = this.state;
    rows.push({
      uuid: Math.random(),
      quantity: columns.map(() => ''),
      pesticide: '',
      unit: '',
    });

    this.setState({ rows }, this.onChange());
  }

  handleRemoveRowClick(uuid) {
    let { rows } = this.state;
    rows = rows.filter(row => row.uuid !== uuid);

    this.setState({ rows }, this.onChange());
  }

  handleAddColumnClick() {
    const { rows, columns } = this.state;
    columns.push({ value: '', uuid: Math.random() });
    rows.forEach(row => {
      row.quantity.push('');
    });

    this.setState({ rows, columns }, this.onChange());
  }

  handleRemoveColumnClick(uuid) {
    let { rows, columns } = this.state;
    const columnIndex = _.findIndex(columns, { uuid: uuid });
    columns = columns.filter(column => column.uuid !== uuid);

    rows.forEach(row => {
      row.quantity.splice(columnIndex, 1);
    });

    this.setState({ rows, columns }, this.onChange());
  }

  getValue() {
    return {
      columns: this.state.columns,
      rows: this.state.rows,
    };
  }

  render() {
    return (
      <div className="EV55">
        <h2>{_t('Stocklist Crop protection products')}</h2>
        <br />
        <div className="table-wrap">
          <table className="table table-hover table-striped table-bordered">
            <thead>
                {this.makeHeadersDate()}
                {this.makeColumns()}
                {this.makeHeaders()}
            </thead>
            <tbody>
                {this.makeRows()}
            </tbody>
          </table>
          <i title={_t('Add a column')} className="fa fa-plus fa-2x add-column-btn" onClick={this.handleAddColumnClick.bind(this)}></i>
        </div>
        <i title={_t('Add a row')} className="fa fa-plus fa-2x add-row-btn" onClick={this.handleAddRowClick.bind(this)}></i>
      </div>
    );
  }
}
