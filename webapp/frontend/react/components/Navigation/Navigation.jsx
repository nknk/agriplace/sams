import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { NavigationTabs } from './NavigationTabs.jsx';
import { AffixWrapper } from '../Affix/AffixWrapper.jsx';
import QuestionnaireFilters from '../../components/QuestionnaireFilters/QuestionnaireFilters.jsx';
import './NavigationTabs.less';
import pageData from '../Utils/pageData.js';
const layout = pageData.context.layout;
import AssessmentPrintHeader from './AssessmentPrintHeader.jsx';

const Navigation = React.createClass({
  propTypes: {
    params: PropTypes.object,
    location: PropTypes.object,
    history: PropTypes.object,
    routes: PropTypes.array,
    navigationSections: PropTypes.array,
    children: PropTypes.object,
    isConfirmAllModalOpen: React.PropTypes.bool,
    isConfirmAllInProgress: React.PropTypes.bool,
    judgementActions: React.PropTypes.object,
    permissionsAssessments: React.PropTypes.object,
    assessmentName: React.PropTypes.string,
  },

  getNavs(sections) {
    const { membershipUuid, assessmentUuid, assessmentAccessMode = 'our' } = this.props.params;
    const knownSections = [
      {
        code: 'overview',
        label: _t('Overview'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/detail`,
        alsoActiveFor: null,
      },
      {
        code: 'evidence',
        label: _t('Evidence'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/documents`,
        alsoActiveFor: null,
      },
      {
        code: 'questionnaires',
        label: _t('Questionnaires'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/questionnaires`,
        alsoActiveFor: new RegExp(`/.*${assessmentAccessMode}/assessments/.*/questionnaires/.*/edit`, 'i'),
      },
      {
        code: 'results',
        label: _t('Results'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/results`,
        alsoActiveFor: null,
      },
      {
        code: 'products',
        label: _t('Crops'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/products`,
        alsoActiveFor: null,
      },
      {
        code: 'sharing',
        label: _t('Finish'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/sharing`,
        alsoActiveFor: null,
      },
      {
        code: 'review',
        label: _t('Inspection overview'),
        url: `/${membershipUuid}/${assessmentAccessMode}/assessments/${assessmentUuid}/review`,
        alsoActiveFor: null,
      },
    ];

    return sections.map(section => knownSections.find(x => x.code === section));
  },

  getActiveSection(sections) {
    const { history } = this.props;
    return sections.find(x =>
      (history.isActive(x.url) || window.location.pathname.match(x.alsoActiveFor))
    );
  },

  getPrevSectionNode(prevSection) {
    return prevSection ? (
      <Link to={prevSection.url} className="btn btn-default">
        <i className="fa fa-arrow-left" />
        &nbsp;
        {prevSection.label}
      </Link>
    ) : null;
  },

  getNextSectionNode(nextSection) {
    return nextSection ? (
      <div>
        <span className="nav-description">{_t('Continue to')}</span>
        &nbsp;
        <Link
          to={nextSection.url}
          className="btn btn-primary"
        >
          {nextSection.label}&nbsp;
          <i className="fa fa-arrow-right" />
        </Link>
      </div>
    ) : null;
  },

  getNavsWithPermissions() {
    const navs = this.getNavs(this.props.navigationSections);
    const { permissionsAssessments } = this.props;

    return navs.filter(nav => {
      let include = true;
      if (nav.code === 'products') {
        if (layout === 'agriplace') {
          include = true;
        } else {
          const permission = permissionsAssessments.find(x => x.name === 'crops app') || {};
          const actions = permission.actions || [];
          include = actions.length;
        }
      } else if (nav.code === 'review') {
        const permission = permissionsAssessments.find(x => x.name === 'review tab') || {};
        const actions = permission.actions || [];
        include = actions.length;
      }
      return include;
    });
  },

  render() {
    const navs = this.getNavsWithPermissions();
    const activeSection = this.getActiveSection(navs);

    if (!activeSection) {
      return null;
    }

    const index = _.findIndex(navs, activeSection);
    const prevSection = (index > 0) && navs[index - 1];
    const nextSection = (index < navs.length) && navs[index + 1];

    const { params, location, history, routes, isConfirmAllModalOpen, isConfirmAllInProgress,
      judgementActions, children, assessmentName } = this.props;
    const isQuestionnairesEditRoute = (activeSection.code === 'questionnaires')
      && params && params.questionnaireUuid;
    const enableAffix = (activeSection.code === 'evidence') || isQuestionnairesEditRoute;

    return (
      <div>
        <AssessmentPrintHeader assessmentName={assessmentName} assessmentTab={activeSection.label} />
        <AffixWrapper uniqueClass="NavigationTabsUnique hidden-print" triggerOffset={10} isEnabled={enableAffix}>
          <div className="NavigationTabsUnique">
            <NavigationTabs
              routes={routes}
              sections={navs}
              params={params}
            />
            {isQuestionnairesEditRoute && (
              <QuestionnaireFilters
                query={location && location.query}
                params={params}
                history={history}
                isConfirmAllModalOpen={isConfirmAllModalOpen}
                isConfirmAllInProgress={isConfirmAllInProgress}
                judgementActions={judgementActions}
              />
            )}
          </div>
        </AffixWrapper>
        {children}
        <div className="toolbar wide muted row hidden-print">
          <div className="col-xs-2">
              {this.getPrevSectionNode(prevSection)}
          </div>
          <div className="col-xs-10">
            <div className="pull-right">
              {this.getNextSectionNode(nextSection)}
            </div>
          </div>
        </div>
      </div>
    );
  },
});

module.exports = Navigation;
