import React, { Component } from 'react';
import './NavigationTabs.less';

export default class AssessmentPrintHeader extends Component {

  render() {
    const { assessmentName, assessmentTab } = this.props;

    return (
      <div className="visible-print page-print-title">
        <strong className="tab-name">{assessmentTab}:</strong>
        {' '}
        {assessmentName}
      </div>
    );
  }
}

AssessmentPrintHeader.propTypes = {
  assessmentName: React.PropTypes.string,
  assessmentTab: React.PropTypes.string,
};
