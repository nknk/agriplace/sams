import React, { Component } from 'react';
import { PrintButton } from '../PrintButton/PrintButton.jsx';
import PrintButtonLocations from './PrintButtonLocations.json';

export class PrintButtonController extends Component {
  render() {
    const isVisible = !!PrintButtonLocations[this.props.location];
    return isVisible && <PrintButton />;
  }
}

PrintButtonController.propTypes = {
  location: React.PropTypes.string,
};
