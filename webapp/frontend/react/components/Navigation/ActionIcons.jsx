import React, { Component } from 'react';
const HelpIcon = require('../HelpIcon/HelpIcon.jsx');
import { PrintButtonController } from './PrintButtonController.jsx';

export class ActionIcons extends Component {
  render() {
    const renderedComponentName = this.props.routes[this.props.routes.length - 1].component.displayName;

    return (
      <div className="ActionIcons">
        <PrintButtonController location={renderedComponentName} />
        <HelpIcon
          assessmentUuid={this.props.assessmentUuid}
          helpLink={renderedComponentName}
        />
      </div>
    );
  }
}

ActionIcons.propTypes = {
  assessmentUuid: React.PropTypes.string,
  routes: React.PropTypes.array,
};
