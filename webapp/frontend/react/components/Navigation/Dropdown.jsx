import React, { Component } from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';

export class Dropdown extends Component {
  render() {
    return (
      <DropdownButton
        eventKey={3}
        title={<span><i className="fa fa-ellipsis-v"></i></span>}
      >
        <MenuItem eventKey="1"><i className="fa fa-envelope fa-fw"></i> User Profile</MenuItem>
        <MenuItem eventKey="2"><i className="fa fa-gear fa-fw"></i> Settings</MenuItem>
        <MenuItem divider />
        <MenuItem eventKey="3"><i className="fa fa-sign-out fa-fw"></i> Logout</MenuItem>
      </DropdownButton>
    );
  }
}
