import React from 'react';
const Router = require('react-router');

const Link = Router.Link;
const History = Router.History;

const Tab = React.createClass({
  propTypes: {
    alsoActiveFor: React.PropTypes.string,
    to: React.PropTypes.string,
  },

  mixins: [History],

  handleClick() {
    this.history.pushState(null, this.props.to);
  },

  render() {
    const { alsoActiveFor = false } = this.props;
    const isActive = this.history.isActive(this.props.to) || window.location.pathname.match(alsoActiveFor);
    const className = isActive ? 'active' : '';
    return <li onClick={this.handleClick} className={className}><Link {...this.props} /></li>;
  },
});

module.exports = Tab;
