import React, { Component } from 'react';
const Tab = require('./Tab.jsx');

export class DefaultTab extends Component {
  render() {
    const { url, label } = this.props;

    return (
      <Tab to={url}>
        {label}
        <span className="chevron"></span>
      </Tab>
    );
  }
}

DefaultTab.propTypes = {
  url: React.PropTypes.string,
  label: React.PropTypes.string,
};
