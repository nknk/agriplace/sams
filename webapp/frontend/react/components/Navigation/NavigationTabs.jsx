import React, { Component } from 'react';
import { PermissionController } from '../PermissionController/PermissionController.jsx';
import { ActionIcons } from './ActionIcons.jsx';
import pageData from '../Utils/pageData.js';
import { DefaultTab } from './DefaultTab.jsx';
const Tab = require('./Tab.jsx');
const layout = pageData.context.layout;
import './NavigationTabs.less';

export class NavigationTabs extends Component {

  render() {
    const { params } = this.props;
    const { assessmentUuid } = params;

    const knownSections = {
      overview: (section) => <DefaultTab url={section.url} label={section.label} />,
      evidence: (section) => <DefaultTab url={section.url} label={section.label} />,
      questionnaires: (section) => (
        <Tab
          to={section.url}
          alsoActiveFor={section.alsoActiveFor}
        >
          {section.label}
          <span className="chevron" />
        </Tab>
       ),
      results: (section) => <DefaultTab url={section.url} label={section.label} />,
      products: (section) => (
        layout === 'agriplace'
        ? <DefaultTab url={section.url} label={section.label} />
        : (
          <PermissionController
            assessmentUuid={assessmentUuid}
            component="CropsApp"
          >
            <DefaultTab url={section.url} label={section.label} />
          </PermissionController>
        )
      ),
      sharing: (section) => <DefaultTab url={section.url} label={section.label} />,
      review: (section) => (
        <PermissionController
          assessmentUuid={params.assessmentUuid}
          component="ReviewTab"
        >
          <DefaultTab url={section.url} label={section.label} />
        </PermissionController>

      ),
    };

    return (
      <div className="row nav-wizard-wrapper">
        <div className="col-sm-12 nav-wizard-slot">
          <div className="nav-wizard">
            <ul className="steps">
              {this.props.sections.map(section => knownSections[section.code](section))}
            </ul>
            <ActionIcons
              routes={this.props.routes}
              assessmentUuid={assessmentUuid}
            />
          </div>
        </div>
      </div>
    );
  }
}

NavigationTabs.propTypes = {
  sections: React.PropTypes.array,
  routes: React.PropTypes.array,
  params: React.PropTypes.object,
};
