

//
// Product module file
//

var ProductModuleDoc = React.createClass({

  propTypes: {
    docUrl: React.PropTypes.string,
  },

  render() {
    var message = _t('Not required for all crops');
    var linkText = _t('check relevance');

    return this.props.docUrl
    ? (
      <div>
        <span className="fa-warning fa icon"></span>
        <span> {message}, <a href={this.props.docUrl} target="_blank">{linkText}</a></span>
      </div>
    )
    : null;
  },

});

module.exports = ProductModuleDoc;
