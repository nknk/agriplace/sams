Web Fonts
==========

Source Sans Pro
----------------
Main AgriPlace font.

Font Awesome
--------------
FontAwesome font files added here (repeated from `/webapp/frontend/lib/bower_components/font-awesome/fonts` so that they
are found by relative paths in font-awesome styles concatenated in `/webapp/frontend/static/css/vendor.css`.