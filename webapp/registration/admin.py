from django.contrib import admin
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from import_export import fields
from import_export.admin import ExportActionModelAdmin
from import_export.resources import ModelResource

from core.admin import BaseAdmin
from organization.utils import get_org_channel, MASS_MAIL, WEB_REGISTRATION
from registration.filters import PlatformContextFilter, ChannelFilter, FarmgroupFilter
from registration.models import RegistrationProfile

FIELDS = ('user__username', 'organizations', 'created_time')


class RegistrationProfileResource(ModelResource):
    organizations = fields.Field()
    activation_link = fields.Field()

    class Meta:
        fields = FIELDS
        export_order = FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = RegistrationProfile

    def dehydrate_organizations(self, profile):
        return ','.join([organization.name for organization in profile.user.organizations.all()])

    def dehydrate_activation_link(self, profile):
        if profile.activation_key == "ALREADY_ACTIVATED":
            return profile.activation_key

        domain = Site.objects.get_current().domain
        org = profile.user.organizations.first()
        channel = get_org_channel(org)
        route_name = 'registration_activate'

        if channel is not None and channel == MASS_MAIL:
            route_name = 'accounts:account_activate'

        return 'http://%s%s' % (domain, reverse(
            route_name,
            kwargs=dict(
                activation_key=profile.activation_key,
            )
        ))


class RegistrationProfileAdmin(BaseAdmin, ExportActionModelAdmin):
    resource_class = RegistrationProfileResource
    list_display = ('user', 'activation_key', 'is_activated', 'created_time')
    list_filter = ('is_activated', PlatformContextFilter, ChannelFilter, FarmgroupFilter)
    search_fields = ('user__username',)


admin.site.register(RegistrationProfile, RegistrationProfileAdmin)
