# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registration', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='registrationprofile',
            options={'verbose_name': 'registration profile', 'verbose_name_plural': 'registration profiles', 'permissions': (('view_registrationprofile', 'Can view registration profile'),)},
        ),
    ]
