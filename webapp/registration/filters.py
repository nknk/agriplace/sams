from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _

from assessments.models import PLATFORM_CHOICES
from organization.models import Organization, OrganizationMembership
from organization.utils import ORGANIZATION_LABEL_TAXONOMY, CHANNEL
from tagging.models import TaggedItem


class PlatformContextFilter(SimpleListFilter):
    title = _('Platform Context')
    parameter_name = 'platform_context'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """

        return PLATFORM_CHOICES

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value():
            return queryset.filter(user__profile__platform_context=self.value())
        else:
            return queryset


class ChannelFilter(SimpleListFilter):
    title = _('Channel')
    parameter_name = CHANNEL

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return [(channel, channel) for channel in ORGANIZATION_LABEL_TAXONOMY[self.parameter_name]]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            organization_ids = TaggedItem.objects.filter(tag__name=self.value()).values_list('object_id', flat=True)
            return queryset.filter(user__organizations__in=organization_ids)
        else:
            return queryset


class FarmgroupFilter(SimpleListFilter):
    title = 'Farm group'
    parameter_name = 'farm_group'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """

        return OrganizationMembership.objects.select_related('primary_organization').values_list(
            'primary_organization__uuid', 'primary_organization__name'
        ).distinct()

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value():
            secondary_organizations = OrganizationMembership.objects.filter(
                primary_organization=self.value()).values_list('secondary_organization', flat=True).distinct()
            return queryset.filter(user__organizations__uuid__in=secondary_organizations).distinct()
        else:
            return queryset
