from __future__ import unicode_literals

from django.conf import settings
from django import forms
from django.core.validators import RegexValidator
from django.utils.translation  import ugettext_lazy as _, get_language
from django.contrib.auth.forms import UserCreationForm
from django.utils.safestring import mark_safe
from django.utils.translation import pgettext_lazy

from app_settings.constants import LEGAL_DOCUMENT_PP
from app_settings.constants import LEGAL_DOCUMENT_TC
from app_settings.utils import get_legal_document
from core import countries
from organization.models import Organization

from .users import UserModel
from .models import RegistrationProfile

User = UserModel()


class AgriplaceRegistrationForm(UserCreationForm):
    MIN_LENGTH = 8
    BLANK_CHOICE = (('', '--------'),)
    error_messages = {
        'duplicate_username': _("A user with that username already exists."),
        'password_mismatch': _("The two password fields didn't match."),
        'duplicate_company': _("A company with that name already exists."),
        'invalid_password_length': _("Password must contains at least 8 characters."),
        'invalid_password_characters': _("Password must contains at least one letter and number."),
        'resend_activation_link': _("This email address (user name) has been reserved for a currently "
                                    "inactivated account.<br/> We have sent a new activation link to the email address."),
        'invalid_phone_number': _("Phone number can only contain these special characters ()+-")
    }

    title = forms.ChoiceField(label=_(pgettext_lazy('reg_form', 'Title')), choices=(("Mr.", _("Mr.")), ("Mrs.", _("Mrs."))))
    first_name = forms.CharField(label=_('First name'))
    last_name = forms.CharField(label=_('Surname'))
    company_name = forms.CharField(label=_('Company name'))
    phone = forms.CharField(
            label=_('Phone'),
            validators=[
                RegexValidator(
                    regex='^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$',
                    message=error_messages['invalid_phone_number'],
                ),
            ])
    country = forms.ChoiceField(label=_('Country'), choices=BLANK_CHOICE + countries.COUNTRIES, initial='')
    language = forms.ChoiceField(label=_('Language'), choices=BLANK_CHOICE + settings.LANGUAGES, initial='')
    username = forms.EmailField(label=_('Email (= user name)'))
    agree_terms = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(AgriplaceRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['password2'].label = _("Confirm password")

        # legal documents
        legal_documents = get_legal_document(language=get_language())
        privacy_policy = _(u'AgriPlace Privacy Policy')
        if legal_documents[LEGAL_DOCUMENT_PP]:
            privacy_policy = u"<a target='_blank' href='{}'>{}</a>".format(
                legal_documents[LEGAL_DOCUMENT_PP].document.url,
                privacy_policy
            )

        terms_and_conditions = _(u'AgriPlace Terms and Conditions')
        if legal_documents[LEGAL_DOCUMENT_TC]:
            terms_and_conditions = u"<a target='_blank' href='{}'>{}</a>".format(
                legal_documents[LEGAL_DOCUMENT_TC].document.url,
                terms_and_conditions
            )

        agree_terms = mark_safe(
            u"{} {} {} {}".format(
                _('I agree with the'),
                terms_and_conditions,
                _('and the'),
                privacy_policy
                )
            )
        self.fields['agree_terms'].label = agree_terms

    class Meta:
        model = User
        fields = (
            "title",
            "first_name",
            "last_name",
            "company_name",
            "phone",
            "country",
            "language",
            "username",
            "password1",
            "password2",
            "agree_terms"
        )

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            user = User._default_manager.get(username=username)
            if not user.is_active:
                registration = RegistrationProfile.objects.get(user=user)
                if not registration.activation_key == 'ALREADY_ACTIVATED' \
                    or not registration.is_activated \
                    or registration.activation_key == '':
                    self._errors[forms.forms.NON_FIELD_ERRORS] = self.error_class(
                        [self.error_messages['resend_activation_link']]
                    )
                    registration.send_activation_email_coop()
        except User.DoesNotExist, RegistrationProfile.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def clean_company_name(self):
        company_name = self.cleaned_data["company_name"]
        try:
            Organization.objects.get(name__iexact=company_name)
        except Organization.DoesNotExist:
            return company_name
        raise forms.ValidationError(
            self.error_messages['duplicate_company'],
            code='duplicate_company'
        )

    def clean_agree_terms(self):
        agree_terms = self.cleaned_data.get('agree_terms')
        if not agree_terms:
            raise forms.ValidationError("", code='disagree_terms')
        return agree_terms

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')

        # At least MIN_LENGTH long
        if len(password1) < self.MIN_LENGTH:
            raise forms.ValidationError(
                self.error_messages['invalid_password_length'],
                code='invalid_password_length'
            )

        # At least one letter and one non-letter
        first_isalpha = password1[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password1):
            raise forms.ValidationError(
                self.error_messages['invalid_password_characters'],
                code='invalid_password_characters'
            )
        return password1
