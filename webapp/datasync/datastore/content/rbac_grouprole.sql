--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: rbac_grouprole; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE rbac_grouprole DISABLE TRIGGER ALL;

COPY rbac_grouprole (id, group_id, role_id) FROM stdin;
\.


ALTER TABLE rbac_grouprole ENABLE TRIGGER ALL;

--
-- Name: rbac_grouprole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('rbac_grouprole_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

