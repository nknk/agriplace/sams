--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: PossibleAnswer_triggered_questions; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "PossibleAnswer_triggered_questions" DISABLE TRIGGER ALL;

COPY "PossibleAnswer_triggered_questions" (id, possibleanswer_id, question_id) FROM stdin;
\.


ALTER TABLE "PossibleAnswer_triggered_questions" ENABLE TRIGGER ALL;

--
-- Name: PossibleAnswer_triggered_questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"PossibleAnswer_triggered_questions_id_seq"', 1, false);


--
-- PostgreSQL database dump complete
--

