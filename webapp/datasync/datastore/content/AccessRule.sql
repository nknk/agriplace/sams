--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: ACList_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ACList_id_seq"', 1, false);


--
-- Data for Name: AccessRule; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "AccessRule" DISABLE TRIGGER ALL;

COPY "AccessRule" (id, data_object_id, permission_id) FROM stdin;
\.


ALTER TABLE "AccessRule" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

