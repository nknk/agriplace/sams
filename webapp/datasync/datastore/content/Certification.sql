--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: Certification; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "Certification" DISABLE TRIGGER ALL;

COPY "Certification" (uuid, remarks, is_test, modified_time, created_time, certification_type_id, organization_id, organization_unit_id, product_id, certificate_number, issue_date, expiry_date, issuer) FROM stdin;
6PiLk8SNtADGGpBE5WJ6j9		f	2014-10-01 12:11:10.930678+04	2014-10-01 12:11:10.910474+04	jPF4sgqUtkqdkmHHsfFwl	5Y3WEl4tfDXcWjXmZK8YR9	\N	\N		\N	\N	
4DiGB0yQhPo88NqzPDBsUP		f	2014-10-14 08:14:11.626529+04	2014-10-14 08:14:11.517948+04	jPF4sgqUtkqdkmHHsfFwl	5vyQCBLxgqklfMGLlJ88Pl	\N	4XGb8zc2pJDsXm5ZB2Wtma		\N	\N	
34cPyiMoiFTOUnmTOzyYfQ		f	2014-10-22 16:36:41.189228+04	2014-10-22 16:36:41.147232+04	jPF4sgqUtkqdkmHHsfFwl	423ollmD6c9hnCXnktCyWO	\N	4gXKy6xwfIaxnpUuij8kQs		2014-09-01	2015-08-31	
5qACAiAcny4bffGu9PavjZ		f	2015-07-22 11:49:23.880284+03	2015-07-22 11:49:23.82463+03	BjhNBgkp2f7J6nkQZVYb9E	wcD5zskiRp8i78WzF29zu	\N	\N		\N	\N	
\.


ALTER TABLE "Certification" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

