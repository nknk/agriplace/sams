--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: CertificationType; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "CertificationType" DISABLE TRIGGER ALL;

COPY "CertificationType" (uuid, remarks, is_test, modified_time, created_time, name, verified, code) FROM stdin;
jPF4sgqUtkqdkmHHsfFwl		f	2015-07-01 15:19:29.764744+03	2014-10-01 12:11:10+04	GLOBALG.A.P.	t	
cxZvUz5zZVhAxZDZGdUtLZ		f	2015-07-01 16:00:32.856344+03	2015-07-01 15:55:50+03	ISO 22000	t	
DWasC6KUX8abLGqrdgdJkV		f	2015-07-01 16:00:46.455402+03	2015-07-01 15:42:48+03	LEAF Marque	t	
gAGTGx4M9bnaQbWGB3B9WR		f	2015-07-01 16:00:56.603418+03	2015-07-01 15:37:20+03	GLOBALG.A.P. GRASP	t	
GL7obqLbkm5WXTRZKjL4jA		f	2015-07-01 16:01:06.261371+03	2015-07-01 15:43:07+03	Field to Fork	t	
gtqKBDbWjsx5sH97nEsYPk		f	2015-07-01 16:01:14.523696+03	2015-07-01 15:37:02+03	AH Protocol	t	
HkjmFZeRzTFxwy3jD2SMNi		f	2015-07-01 16:01:25.543+03	2015-07-01 15:52:17+03	KRAV	t	
HwrVDJZMCfewFp7PMJdUyU		f	2015-07-01 16:01:34.387221+03	2015-07-01 15:54:26+03	AB Logo	t	
JzGGaKs4g5eLFGA4yN36dg		f	2015-07-01 16:01:44.317262+03	2015-07-01 15:37:36+03	Voedselveiligheid Akkerbouw	t	
kCQreoMcfmRxscqeZtYGu5		f	2015-07-01 16:01:54.801979+03	2015-07-01 15:29:39+03	Milieukeur Fruit	t	
kwHUUWWK8J9SNx33GtjxuU		f	2015-07-01 16:02:03.75351+03	2015-07-01 15:55:09+03	FSSC 22000	t	
LuPkHRySfoeqit8JnqLnHX		f	2015-07-01 16:02:10.105114+03	2015-07-01 15:38:25+03	Voedselveiligheid GZP	t	
mjWUjA6SF4Sy9SpuZf4GdE		f	2015-07-01 16:02:16.352742+03	2015-07-01 15:42:40+03	SEDEX	t	
LVx2M2heF9qtjh8dBGTvRo		f	2015-07-01 16:02:25.695017+03	2015-07-01 15:36:10+03	EU Biologisch  (Regelgeving (EEG) No. 834/2007 en 889/2008)	t	
Pn4pFYnUTRiW4wHULR9EUB		f	2015-07-01 16:02:32.829049+03	2015-07-01 15:37:51+03	Voedselveiligheid Aardappelen	t	
quqA3wrDXTDjM6eG85N7y6		f	2015-07-01 16:02:42.709642+03	2015-07-01 15:50:04+03	IFS	t	
RAaHJ3cbSXAS8i4cYVa7PJ		f	2015-07-01 16:02:48.60916+03	2015-07-01 15:52:27+03	BioSuisse	t	
sBfdWGswqiCA9U7KXsxVWH		f	2015-07-01 16:02:54.804625+03	2015-07-01 15:51:13+03	QS	t	
t3Dcd7v8TTEKf9SMvRujp		f	2015-07-01 16:03:01.621062+03	2015-07-01 15:38:49+03	Voedselveiligheid Suikerbieten	t	
vvwZWrjTknEKTcwGBLs8LJ		f	2015-07-01 16:03:08.763072+03	2015-07-01 15:54:11+03	Soil Association Organic Standards	t	
Y8nXTmjCM2iwjpPZajbCZZ		f	2015-07-01 16:03:15.327943+03	2015-07-01 15:47:20+03	ISO 14001 	t	
Z7PaLxT2cgkaeqGvXcP88		f	2015-07-01 16:03:23.847932+03	2015-07-01 15:29:09+03	Milieukeur Plantaardige producten uit de open teelt	t	
ZXEFGnE6XgARuKXfRNssvm		f	2015-07-01 16:03:29.88151+03	2015-07-01 15:55:33+03	HACCP Food Safety Management System certification version 5	t	
39fEB9yJooHzGT9qo4MvmC		f	2015-07-01 16:05:48.526234+03	2015-07-01 15:38:07+03	Voedselveiligheid Zetmeelaardappelen	t	
6wTwAmMEKxnfSSrMEn3TW4		f	2015-07-01 16:06:10.617516+03	2015-07-01 15:42:31+03	Tesco Nurture	t	
BjhNBgkp2f7J6nkQZVYb9E		f	2015-07-01 16:06:28.053035+03	2015-07-01 15:49:56+03	BRC	t	
\.


ALTER TABLE "CertificationType" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

