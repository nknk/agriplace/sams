--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: rbac_role; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE rbac_role DISABLE TRIGGER ALL;

COPY rbac_role (id, slug, name, description, parameters, privelege) FROM stdin;
\.


ALTER TABLE rbac_role ENABLE TRIGGER ALL;

--
-- Name: rbac_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('rbac_role_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

