--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: ProductTypePermission_questionnaire; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "ProductTypePermission_questionnaire" DISABLE TRIGGER ALL;

COPY "ProductTypePermission_questionnaire" (id, producttypepermission_id, questionnaire_id) FROM stdin;
\.


ALTER TABLE "ProductTypePermission_questionnaire" ENABLE TRIGGER ALL;

--
-- Name: ProductTypePermission_questionnaire_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ProductTypePermission_questionnaire_id_seq"', 1, false);


--
-- PostgreSQL database dump complete
--

