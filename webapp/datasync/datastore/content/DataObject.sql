--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: ACLDataObject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ACLDataObject_id_seq"', 1, false);


--
-- Data for Name: DataObject; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "DataObject" DISABLE TRIGGER ALL;

COPY "DataObject" (id, name, state_id) FROM stdin;
\.


ALTER TABLE "DataObject" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

