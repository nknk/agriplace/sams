--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: ContentBlock; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "ContentBlock" DISABLE TRIGGER ALL;

COPY "ContentBlock" (id, code, description, text) FROM stdin;
6	assessment_evidence_list_intro		<p>Here you can enter your evidence documents. AgriPlace links each evidence document to the questionnaire of the corresponding certificate. The questions related to the evidence document will be answered automatically. After the evidence document has been registered, it will be stored in your archive from where it can be easily retrieved and reused.</p>\r\n\n
3	assessment_results_intro		<p>Below you will find an overview of your answers.</p>\r\n\r\n
7	water_risk_assessment_intro		Voorafgaand aan het gebruik van water dient met dit Agriform beoordeeld te worden of dit risico’s voor de voedselveiligheid op kan leveren. Door het gebruik van dit Agriform kunt u bepalen of het water zonder risico gebruikt kan worden of dat er een aanvullende uitgebreide risicoanalyse nodig is.\r\nVoor elke unieke combinatie van type water en doelgebruik dient dit formulier apart te worden ingevuld en jaarlijks te worden geactualiseerd.
8	plot_risk_assesmment_info		Voorafgaand aan het gebruik van elk perceel dient met dit beslisschema beoordeeld te worden of dit risico’s voor de voedselveiligheid op kan leveren. Onder perceel wordt hier verstaan: Homogene teelteenheid die kan bestaan uit meerdere secties, aaneengesloten stukken land of deel van kas waarop/waarin de teler hetzelfde gewas tijdens een bepaald seizoen verbouwt. Door het gebruik van onderstaand schema kunt u bepalen of het perceel zonder risico gebruikt kan worden of dat er een aanvullende uitgebreide risicoanalyse nodig is.\r\nVoor elk perceel dient dit formulier apart te worden ingevuld en jaarlijks te worden geactualiseerd.\r\n\r\n\r\n
11	Fooddefence_risk_analysis_intro	\r\n\r\n	Het risico tot opzettelijk besmetten van product minimaliseren door het analyseren van de risico's en eventueel nemen van preventieve en/of curatieve maatregelen. Deze analyse dient jaarlijks te worden geactualiseerd.
10	Organic_fertilizer_risk_assessment_intro		Voorafgaand aan het gebruik van organische mest dient middels een risicoanalyse te worden vastgesteld of het gebruik van de betreffende mest een potentieel risico kan vormen voor de te telen producten. Door middel van het invullen van dit Agriform bepaalt u of de aan te wenden organische mest veilig te gebruiken is, of dat er een uitgebreidere risicoanalyse nodig is om dit vast te kunnen stellen.\r\nU dient per type organische mest een analyse uit te voeren.
9	MRL_risk_assessment_intro		Elk GLOBALG.A.P. gecertificeerd bedrijf dient een risicoanalyse te hebben waarin wordt aangegeven of er risico’s tot overschrijden van de MRL’s (Maximum Residu Level) zijn en welke maatregelen er zijn genomen om deze risico’s te beheersen.\r\nOnderstaande risicoanalyse kunt u hiervoor invullen per gewas(groep).
12	Health_and_hygiene_risk_assessment_intro		Door het invullen van dit Agriform toont u aan dat u bent nagegaan of er potentiële risico's zijn voor de voedselveiligheid van het product binnen uw bedrijf. Voor iedere beheersmaatregel dient te worden aangegeven of deze reeds is genomen, deze niet van toepassing is of mogelijk in de toekomst genomen zal worden. Nog te nemen maatregelen moeten worden opgenomen in een plan van aanpak. Deze risicoanalyse dient jaarlijks te worden geactualiseerd.
1	feedback_form_intro		<p>We value your feedback. Let us know how we can improve.</p>\r\n
4	certificate_list_intro		<p>Here you can add your already obtained certificates.</p>\r\n\r\n<p>If you want to perform an assessment for a new certification, <a href="assessments/list">go to Certification</a>.</p>\r\n
2	assessment_sharing_intro		<p>Please indicate below with which audit organization(s) you want to share your assessment. Upon your authorization the auditor will have read-only access to your assessment (questionnaires and evidence).&nbsp;</p>\r\n
\.


ALTER TABLE "ContentBlock" ENABLE TRIGGER ALL;

--
-- Name: ContentBlock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ContentBlock_id_seq"', 12, true);


--
-- PostgreSQL database dump complete
--

