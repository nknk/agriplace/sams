--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: ProductIdentifier; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "ProductIdentifier" DISABLE TRIGGER ALL;

COPY "ProductIdentifier" (id, product_id, identifier_type, identifier_type_name, value) FROM stdin;
\.


ALTER TABLE "ProductIdentifier" ENABLE TRIGGER ALL;

--
-- Name: ProductIdentifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ProductIdentifier_id_seq"', 1, false);


--
-- PostgreSQL database dump complete
--

