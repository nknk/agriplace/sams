--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: ACLPermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ACLPermission_id_seq"', 1, false);


--
-- Data for Name: Permission; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "Permission" DISABLE TRIGGER ALL;

COPY "Permission" (id, name) FROM stdin;
\.


ALTER TABLE "Permission" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

