--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: DocumentType_assessment_types; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "DocumentType_assessment_types" DISABLE TRIGGER ALL;

COPY "DocumentType_assessment_types" (id, documenttype_id, assessmenttype_id) FROM stdin;
1	7DVNGGXoxMypLbBFdWHs2i	16JV0yi89rGtmhI2gbRjvO
2	7DVNGGXoxMypLbBFdWHs2i	3xjZEjnSo4Ut1W7ImwRbxT
\.


ALTER TABLE "DocumentType_assessment_types" ENABLE TRIGGER ALL;

--
-- Name: DocumentType_assessment_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"DocumentType_assessment_types_id_seq"', 2, true);


--
-- PostgreSQL database dump complete
--

