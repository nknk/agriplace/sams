--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: Questionnaire; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "Questionnaire" DISABLE TRIGGER ALL;

COPY "Questionnaire" (uuid, remarks, is_test, modified_time, created_time, content_package_id, assessment_type_id, order_index, code, intro_text, available_languages, requires_signature, help_document, name) FROM stdin;
ZrhfkbF69ges7kEFCD5JmB		f	2015-07-30 15:20:57.043583+03	2015-07-30 15:20:57.043567+03	\N	\N	10			en	f		
pwCYSJFAySidtcmFAEaM4U		f	2015-12-09 11:55:10.855656+03	2015-12-08 18:04:52+03	\N	Wpn58b5iHPN7DynnhDLrKF	10	AFEV0		en,nl,es	t		Food Safety Policy declaration
CzzW2dMPSh5ipCKMpA2mn		f	2015-11-12 15:18:19.602451+03	2015-01-20 12:06:14+03	\N	AhnpHyBDEJFfhwCipW4NFF	10	AFEV13-QN1		en,nl	t		Integrated pest management
kJUyQs4pepzS3fZosbYUvG		f	2016-01-05 13:20:39.603198+03	2016-01-04 16:12:09+03	\N	powmcgqeA7678EjD3feGJ9	10	AFEV17-QN1		nl	t		Klachtenprocedure en registratie
1kVVtT60Zns6U6Bj58RuS6		f	2015-11-12 15:20:15.329875+03	2014-11-10 18:13:22+03	\N	YeGQDxFHDuyxMkDjWIUCT	10	AFEV24-QN1	Voorafgaand aan het gebruik van organische mest dient middels een risicoanalyse te worden vastgesteld of het gebruik van de betreffende mest een potentieel risico kan vormen voor de te telen producten. Door middel van het invullen van onderstaand schema kunt u bepalen of de aan te wenden organische mest veilig te gebruiken is, of dat er een uitgebreidere risicoanalyse nodig is om dit vast te kunnen stellen.\r\nU dient per type organische mest een analyse uit te voeren.	en,nl	t		Organic fertilizer risk analysis
7vlxaFaGRtWzMj2wyoE3k0		f	2015-11-12 15:20:35.105006+03	2014-11-12 12:55:59+03	\N	4XQ2m6hj4lRq2AeiiM7ugE	10	AFEV27-QN1		en,nl	t		Plot risk analysis
Twg57rf26t3EArmcD34BJT		f	2015-11-12 15:21:31.854692+03	2015-01-21 14:09:19+03	\N	6es6ZyyYX6CsuRwtpygxQG	10	AFEV31-QN1		nl,en	t		Recall procedure
cudFLWwqg4s4JitSE6toZU		f	2015-11-12 15:21:50.926258+03	2015-01-20 14:26:43+03	\N	ZJkDqJUznc75TUbHV5xsqR	10	AFEV4-QN1		en,nl	t		Description traceability
54UtxA8Noq0NAPRz991aau		f	2015-11-12 15:22:31.554054+03	2014-10-16 16:41:49+04	\N	1p7DygIoLDfMPeQ0kPK5lu	10	AFEV44-QN1	Doel: Het risico tot opzettelijk besmetten van product minimaliseren door het analyseren van de risico's en eventueel nemen van preventieve en/of curatieve maatregelen.	en,nl	t		Food defence risk analysis 
6HQn5azf7unzjaQoAhiKkS		f	2015-11-12 15:23:37.586547+03	2014-11-04 11:59:12+03	\N	8xyJAnz4YiCCqqGQKttR0	10	AFEV46-QN1	Doel van deze vragenlijst is het aantoonbaar maken dat u bent nagegaan of er potentiële risico's zijn voor de voedselveiligheid van het product binnen uw bedrijf. Voor iedere beheersmaatregel dient te worden aangegeven of deze reeds is genomen, deze niet van toepassing is of mogelijk in de toekomst genomen zal worden. Nog te nemen maatregelen moeten worden opgenomen in een plan van aanpak.	nl, en	t		Food safety operational risk analysis 
FxD72R9k8XiXFJz4Ymd8ET		f	2016-01-15 18:57:07.363938+03	2016-01-05 18:03:08+03	\N	VRhfVBqvFNQ9kMAYjznanf	10	AFEV47		nl	t		Arbeidsrisico-inventarisatie
1wZQRfK1eh4rPx1dIAYE6X		f	2015-11-12 15:24:21.543511+03	2014-11-11 16:21:11+03	\N	2zrEr2yuQowTipJznUUpu8	10	AFEV57-QN1	Voorafgaand aan het gebruik van water dient met dit Agriform beoordeeld te worden of dit risico’s voor de voedselveiligheid op kan leveren. Door dit Agriform in te vullen kunt u bepalen of het water zonder risico gebruikt kan worden of dat er een aanvullende uitgebreide risicoanalyse nodig is.\r\nVoor elke unieke combinatie van type water en doelgebruik dient dit formulier apart te worden ingevuld en jaarlijks te worden geactualiseerd.	en,nl	t		Water risk analysis
cqyCyLDJ2jFVM3rcdNLYp9		f	2015-11-12 15:25:10.94762+03	2015-01-20 13:43:45+03	\N	w56T8Ag3A22ew7YpznaraS	10	AFEV58-QN1		en,nl	t		Waste and environmental management plan
VtDcxbsgL2XfK8k6MaiuNE		f	2015-11-12 15:25:35.584311+03	2015-01-21 12:58:24+03	\N	2A9rrpsJhWeFuoYe2FSXnJ	10	AFEV59-QN1		en,nl	t		Nature conservation policy
FyuRL29PrXyAMWyhwGRGQg		f	2016-02-10 12:44:02.458059+03	2016-02-08 11:05:12+03	\N	seRSs5KQm94NMLNwZbFJNM	10	AFHPREV1	Elke pootgoedteler dient voor zijn bedrijf een getekende eigenverklaring te hebben. In deze evrklaring geeft de teler aan aan welke voorwaarden zijn bedrijf voldoet met betrekking tot de teelt, be/verwerking en opslag van pootaardappelen.	nl	t		Eigen verklaring pootgoedteler
4W6qmzcqnXL54pz4I7f2wY		f	2015-08-13 17:13:13.503095+03	2014-09-17 12:24:20+04	\N	2dljiL0mPCAuOoYembVzqE	10	AH		en,nl	f		Albert Heijn Protocol for residue control
EVTpGX2si4A8Y2doww94Nk		f	2015-08-17 15:21:11.220053+03	2014-11-25 16:58:15+03	\N	2Dvvg8ewAWV6WMY3BXPA2m	10	EU Bio		en, nl	f		EU Organic Questionnaire
78QSBUDQP8z2za1q1lTLnl		f	2015-08-17 14:58:04.532651+03	2014-09-17 02:53:42+04	\N	uaZHkcWBdNp0tW0S_IpzJ	1	AF		en,nl	f		All Farm Base Module
1kHdSacPEmO65q2c35mjHi		f	2016-03-30 15:55:13.689801+03	2014-11-05 15:38:28+03	\N	4joLpor3PA8PHf9D9wWz59	10	AFEV45-QN1		en,nl	t		MRL risk analysis
3qveuv8LTo2P5qtR5lEQAY		f	2015-08-17 15:00:25.343463+03	2014-09-17 02:52:03+04	\N	uaZHkcWBdNp0tW0S_IpzJ	2	CB		en,nl	f		Module Crop Base
2ZIZ5SZ4BrdHMxFlxLWn2c		f	2015-10-12 17:48:29.689372+03	2014-09-17 02:49:52+04	\N	uaZHkcWBdNp0tW0S_IpzJ	3	FV		en,nl	f	help_documents/Croplist_EN-NL_Fruit_and_Vegetables_GLOBALGAP_12-10-2015.pdf	Module Fruit & Vegetables 
67fH1afOnfOQ9qLtNu6Jyk		f	2015-10-12 17:49:51.947123+03	2014-09-17 02:51:06+04	\N	uaZHkcWBdNp0tW0S_IpzJ	4	CC		en,nl	f	help_documents/Croplist_EN-NL_Combinable_Crops_GLOBALGAP_12-10-2015.pdf	Module Combinable Crops
jQicQzU32tqrkntuAYm79Q		f	2015-12-04 16:39:40.404667+03	2015-12-03 18:05:25+03	\N	BVkBxcZVCw2NKQ9AYvsqBW	10	CC5		en	f		Module Combinable Crops
V7Q3NuWkHk2rhva3jSRVrV		f	2015-12-04 16:37:33.205553+03	2015-12-03 18:05:59+03	\N	BVkBxcZVCw2NKQ9AYvsqBW	10	FV5		en	f		Module Fruit & Vegetables 
19HzCC2BUdLaHxEDFlWybD		f	2016-02-01 19:00:58.242082+03	2014-09-17 13:05:47+04	\N	4cekaZp0ZBl6OQWPh5DzXX	10	GRASP		en,nl	f		GRASP Module V1.1
4DzhfLSUxoKwHoGGRYvvM6		f	2016-02-04 15:56:36.184426+03	2015-07-29 09:38:52+03	\N	UPekHswKQMTgQFFySpp7qb	10	HPR	Dit protocol geeft aan welke preventieve maatregelen de Nederlandse pootgoedketen hanteert om de risico’s van\r\nintroductie en verspreiding van ringrot in Nederlands pootgoed te minimaliseren. Het Hygiëneprotocol Ringrot is\r\nuitgewerkt op basis van de ‘Best Practices’ in de praktijk. Op enkele punten worden adviezen gegeven die verder\r\ngaan. Deze zijn bedoeld voor bedrijven die de risico’s op ringrot verder willen beperken of die te maken hebben\r\nmet een verhoogd risico op een ringrotbesmetting.\r\n	nl	f		Hygiene Protocol Ringrot 2.2
1RUFKbxHwcnOFGd7uo9OYK		f	2015-07-29 09:29:22.734803+03	2014-09-17 14:07:17+04	\N	1TkCcuXUfnGlbXRF9hzNka	10	FSA		en	f		Farmer Sustainability Assessment 
12LRKSb4C156cBbscS6P3D		f	2015-05-21 13:27:51.615121+03	2014-09-17 13:41:35+04	\N	16AwAbqGsVpQVatC761v2P	10	TN10		en,nl	f		Tesco Nurture 10
21a0SdgBdIn3qjiTBV6iuq		f	2015-07-29 09:29:40.975531+03	2014-09-17 10:59:24+04	\N	3xjZEjnSo4Ut1W7ImwRbxT	10	VVA		nl	f		VV Aardappelen
1achodm5BCc13eRxuqth2t		f	2015-07-29 09:29:57.972795+03	2014-09-17 10:59:26+04	\N	16JV0yi89rGtmhI2gbRjvO	10	VVAK		nl	f		VV Akkerbouw 
1tIy63HV6okadar4iuKTrB		f	2015-07-29 09:30:08.47336+03	2014-09-17 10:59:29+04	\N	2KPUKhZ4VkZw5AYYEgbFSu	10	VVGZP		nl	f		VV Granen, Zaden en Peulvruchten
wEtWp1DJ7jX0oOti9qQUE		f	2016-02-01 13:31:48.677719+03	2014-09-17 10:59:23+04	\N	UsqaMp2kgYYmN3E9p9A66	10	VVSU		nl	f		VV Suikerbieten
4bR8IQQj3lz3n2xD6vqEi4		f	2015-12-17 14:13:23.238058+03	2014-09-17 10:59:23+04	\N	gmQBZsZ64NhwNFReaa6rc	10	VVZA		nl	f		VV Zetmeelaardappelen
MzCRxa87kRrskaUVur7PJA		f	2016-03-23 13:55:11.785497+03	2016-03-11 17:30:56+03	\N	o295RPkZH5qxUbYs6mceWf	10	AFEV65-QN1		en	t		Plan duurzaam energie gebruik
dvsaj5Lgkmt25yKUZSiym		f	2016-03-09 14:32:04.973714+03	2016-03-09 13:51:59+03	\N	BVkBxcZVCw2NKQ9AYvsqBW	10	AOSHZPC	Extra eisen zelfbeoordelingslijst GlobalGAP IFA voor suikerbieten	nl	f		Add-on Suikerbieten HZPC
BbGuyGk6PNXEr8N3XwDorF		f	2016-03-23 13:56:06.809537+03	2016-03-09 16:57:58+03	\N	7u3fGkzavdv97hc8khCKJ8	10	AFEV24-1-QN1	Voorafgaand aan het gebruik van organische mest dient middels een risicoanalyse te worden vastgesteld of het gebruik van de betreffende mest een potentieel risico kan vormen voor de te telen producten. Door middel van het invullen van onderstaand schema kunt u bepalen of de aan te wenden organische mest veilig te gebruiken is, of dat er een uitgebreidere risicoanalyse nodig is om dit vast te kunnen stellen.\r\nU dient per type organische mest een analyse uit te voeren.	nl	t		Risico analyse organische meststoffen
VTJdffJhxVThAtCRiRYK27		f	2016-03-25 16:13:24.186077+03	2016-03-11 18:27:11+03	\N	Uv3qxX8YTPrRwhEAWGGaNm	10	AFEV31-1		en,nl	t		Recall (terugroep) en recallprocedure
vd77ENd94EMgtFGWswtn46		f	2016-03-23 15:12:32.274293+03	2016-01-19 17:12:11+03	\N	PTrVn8Wn4rw7zTEFnm2Kje	10	GRASP 1.3		en,nl	f		GRASP Module V1.3
UKUnwpzW5zPBMrbBLbf7Dm		f	2016-03-31 12:19:46.537455+03	2016-03-15 11:25:47+03	\N	yjj5YfhV6GBRjiZ5noxtEh	10	EV46-1		nl, en	t		Bedrijfshygiëne risicoanalyse
vs2QL7TerZNVpPBxtXcax		f	2016-03-23 13:52:08.145901+03	2016-03-15 16:31:48+03	\N	39LndKGuRtsk2BGdNvMdVF	10	AFEV57-1	Voorafgaand aan het gebruik van water dient met dit Agriform beoordeeld te worden of dit risico’s voor de voedselveiligheid op kan leveren. Door dit Agriform in te vullen kunt u bepalen of het water zonder risico gebruikt kan worden of dat er een aanvullende uitgebreide risicoanalyse nodig is.\r\nVoor elke unieke combinatie van type water en doelgebruik dient dit formulier apart te worden ingevuld en jaarlijks te worden geactualiseerd.	en,nl	t		Water risico analyse
xN3m4QW6x6dd4w72CLN6Wc		f	2016-03-31 12:23:57.990054+03	2015-12-03 18:03:39+03	\N	BVkBxcZVCw2NKQ9AYvsqBW	20	AF5		en	f		All Farm Base Module
6QEYDs6PsPZCXRWJVyW8F5		f	2016-03-23 15:31:04.54803+03	2016-03-23 15:17:18+03	\N	brERLPRN9bYA6NJvCKERae	10	AFEV198-1		nl	t		Verklaring geen werknemers
B4Kd3LuJwfMNP8CD2fn8d5		f	2016-03-24 16:37:05.219281+03	2015-12-03 18:04:15+03	\N	BVkBxcZVCw2NKQ9AYvsqBW	10	CB5		en	f		Module Crop Base
\.


ALTER TABLE "Questionnaire" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

