--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: QuestionLevel; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "QuestionLevel" DISABLE TRIGGER ALL;

COPY "QuestionLevel" (uuid, remarks, is_test, modified_time, created_time, order_index, color, code, assessment_type_id, requires_attachment, requires_justification, title) FROM stdin;
1PEBWKCRmXaaXN9Z02H9xG		f	2014-09-19 15:52:18.513497+04	2014-09-17 13:05:47.841606+04	0	#ffffff	?	4cekaZp0ZBl6OQWPh5DzXX	f	f	
2BTERy3qAtTC0p7MEarBoY		f	2014-09-19 15:52:10.210577+04	2014-09-17 14:07:17.264524+04	0	#ffffff	?	1TkCcuXUfnGlbXRF9hzNka	f	f	
53ge3Uws8AXNE8yrddGLl0		f	2014-09-19 15:52:23.367962+04	2014-09-17 13:41:36.393581+04	0	#ffffff	?	16AwAbqGsVpQVatC761v2P	f	f	
d9KpaQ2U6C29oREgDUthPV		f	2015-01-27 13:13:19.666269+03	2014-11-26 13:50:39+03	0	#FFFFFF	eu-bio-level	\N	f	f	
BARzEDcoCbEAXac9uCXdrh		f	2016-03-11 19:39:30.282898+03	2015-12-03 18:03:39+03	1	#FAD4BB	major	BVkBxcZVCw2NKQ9AYvsqBW	f	f	Major
QDragdiwYFL239MLf7rLCV		f	2016-03-11 19:39:40.225376+03	2015-12-03 18:03:43+03	2	#FFF3D6	minor	BVkBxcZVCw2NKQ9AYvsqBW	f	f	Minor
t3fSox7xRwhGoZPfwoUKCN		f	2016-03-11 19:39:48.452719+03	2015-12-03 18:03:54+03	3	#F5F5F5	recommended	BVkBxcZVCw2NKQ9AYvsqBW	f	f	Recommended
H5XJwwK8pYPyUWZQEASf5N		f	2016-03-14 11:53:30.972027+03	2015-12-03 18:04:01+03	4	#D9FF96	Suikerbieten	BVkBxcZVCw2NKQ9AYvsqBW	f	f	Suikerbieten
2rDaxliQwxEaXKMFe2IWEz		f	2014-09-17 03:20:47.220088+04	2014-09-17 02:49:52.183343+04	1	#fad4bb	major	uaZHkcWBdNp0tW0S_IpzJ	f	f	Major
3ab5ObQvweixAT2SxtBFEN		f	2014-09-17 13:43:39.180844+04	2014-09-17 13:41:35.814846+04	1	#fad4bb	critical	16AwAbqGsVpQVatC761v2P	f	f	Critical
3RoXXP1U7HqEVUcLnRf1ps		f	2014-09-17 11:34:05.902459+04	2014-09-17 10:59:23.823147+04	1	#ffffff	standard	gmQBZsZ64NhwNFReaa6rc	f	f	Standard
41GxCeIeCoM8eLVO74St24		f	2014-09-17 11:32:48.138998+04	2014-09-17 10:59:29.773757+04	1	#ffffff	should	2KPUKhZ4VkZw5AYYEgbFSu	f	f	Should
42bxQliUBGhAvzDf3wJpFA		f	2014-09-17 12:25:13.31779+04	2014-09-17 12:24:20.825118+04	1	#ffffff	major	2dljiL0mPCAuOoYembVzqE	f	f	Major
5ykVGw8gdo0QbhhzNVH17d		f	2014-09-17 11:33:41.911921+04	2014-09-17 10:59:23.216546+04	1	#ffffff	standard	UsqaMp2kgYYmN3E9p9A66	f	f	Standard
6ufWowFy94lT7zHbh9gabm		f	2014-09-17 11:32:00.366485+04	2014-09-17 10:59:24.389517+04	1	#ffffff	standard	3xjZEjnSo4Ut1W7ImwRbxT	f	f	Standard
fjDX5jfQoGH48sWGSW8i6V		f	2015-07-30 15:32:33.792548+03	2015-07-30 15:31:48+03	1	#FF120A	verplichting	UPekHswKQMTgQFFySpp7qb	f	f	Obligation
ILXIIgPHf8yqGPVbk2mM0		f	2014-09-17 14:09:10.026763+04	2014-09-17 14:07:17.56607+04	1	#ffffff	essential	1TkCcuXUfnGlbXRF9hzNka	f	f	Essential
leYKTriZItOBhSaxs2tAH		f	2014-09-17 11:32:17.93845+04	2014-09-17 10:59:26.52916+04	1	#ffffff	should	16JV0yi89rGtmhI2gbRjvO	f	f	Should
12oOOLmi8XRa5kwehi8xAc		f	2014-09-17 13:43:49.691416+04	2014-09-17 13:41:35.77972+04	2	#ffffff	obligatory	16AwAbqGsVpQVatC761v2P	f	f	Obligatory
3A5ypbJx4Luzuzrl0vQcKY		f	2014-10-22 19:12:48.069555+04	2014-09-17 02:49:52.06117+04	2	#fff3d6	minor	uaZHkcWBdNp0tW0S_IpzJ	f	f	Minor
3okQCUNrYmf5ILFKLXgcHy		f	2014-09-17 11:33:17.817373+04	2014-09-17 10:59:29.640809+04	2	#ffffff	standard	2KPUKhZ4VkZw5AYYEgbFSu	f	f	Standard
4xBf5BSk3scE1WoLyx4wAg		f	2014-09-17 11:32:40.348665+04	2014-09-17 10:59:26.155103+04	2	#ffffff	standard	16JV0yi89rGtmhI2gbRjvO	f	f	Standard
5QEgkot3K7vjCDogenN6Pz		f	2014-09-17 14:09:15.163167+04	2014-09-17 14:07:17.63176+04	2	#ffffff	advanced	1TkCcuXUfnGlbXRF9hzNka	f	f	Advanced
oDYiSSoi2KjAFKHcrpzE66		f	2015-07-30 15:33:42.783608+03	2015-07-30 15:33:42.78359+03	2	#F7FF14	sterke aanbeveling	UPekHswKQMTgQFFySpp7qb	f	f	Strong recommendation
3TlA5U8fbtaBL9AxoTaTZ3		f	2014-10-22 19:12:58.84872+04	2014-09-17 02:49:52.15453+04	3	#f5f5f5	recommended	uaZHkcWBdNp0tW0S_IpzJ	f	f	Recommended
5TGNtWOmZQB7q5bUN3kP4		f	2014-09-17 14:09:21.510905+04	2014-09-17 14:07:17.545755+04	3	#ffffff	basic	1TkCcuXUfnGlbXRF9hzNka	f	f	Basic
7hAt7rbuXl4QjhikOWfBM2		f	2014-09-17 13:43:56.00173+04	2014-09-17 13:41:36.843063+04	3	#ffffff	standard	16AwAbqGsVpQVatC761v2P	f	f	Standard
Rk3cAxaKgCXmqJqbKGn6dT		f	2015-07-30 15:34:35.960297+03	2015-07-30 15:34:35.96028+03	3	#54FF29	advies	UPekHswKQMTgQFFySpp7qb	f	f	Advice
uK2sjZKYwhAgVzzCM4L4t		f	2014-12-08 17:11:50.608012+03	2014-12-08 17:11:50.607987+03	3	#FFFFFF	trigger	16JV0yi89rGtmhI2gbRjvO	f	f	
3tvq9zQ52E3Rxuu008VAd8		f	2014-09-17 03:15:30.977452+04	2014-09-17 02:49:52.032061+04	4	#ffffff	trigger	uaZHkcWBdNp0tW0S_IpzJ	f	f	
hKkpyfYw9Gfp9urweUQrwW		f	2016-03-22 12:58:39.125204+03	2016-03-22 12:58:39.125196+03	0	#FFFFFF	GRASP 1.3 Geenwerknemers	PTrVn8Wn4rw7zTEFnm2Kje	f	f	Geen werknemers
XB5XmvgxPdUsDXT2jNdu6i		f	2016-03-22 13:01:02.240618+03	2016-01-19 17:12:15+03	1	#FFFFFF	Basic 	PTrVn8Wn4rw7zTEFnm2Kje	f	f	Basic
\.


ALTER TABLE "QuestionLevel" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

