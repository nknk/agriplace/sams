--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: News; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "News" DISABLE TRIGGER ALL;

COPY "News" (uuid, remarks, is_test, modified_time, created_time, order_index, title, text) FROM stdin;
6soLbYrNbMj4WDO5G2MjEg		f	2014-10-27 13:17:33.497682+03	2014-10-14 03:12:18.281661+04	0	Tesco Nurture... now available on AgriPlace	<p>Tesco Nurture is an exclusive independently accredited scheme to Tesco. It is dedicated to ensuring all fruit and vegetables are grown to environmental and responsible standards. Each grower is audited on an annual basis. Using AgriPlace you can now save time on your audit preparation, reusing any existing information you already submitted for other assessments.&nbsp;</p>\r\n\n
50raJVfTFZrsydEXFJ66JJ		f	2014-10-27 13:32:10.023961+03	2014-10-14 03:13:00.56425+04	0	AgriPlace now provides a camera upload app for easy evidence uploading 	<p>The majority of certification assessments requires you to demonstrate compliance through evidence documents (copies of invoices, first aid diploma&#39;s, etc.). The camera upload simplifies adding these documents to AgriPlace. It allows you to select one or more photos from you mobile phone and upload them directly to your AgriPlace archive.&nbsp;</p>\r\n\n
NX2kJ76s4yp6S3gFh2BXCK		f	2014-12-04 20:12:05.471953+03	2014-12-04 20:12:05.47194+03	0	5 December AgriPlace Kick-off event!	This is it. On December 5, Agri Place goes live! At a special event a selected group of users will be familiarized with the Agri Place platform. In the coming months they will prepare their GLOBALG.A.P. 2015 audit do with Agri Place.
\.


ALTER TABLE "News" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

