--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: HelpContentBlock_assessment_type; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "HelpContentBlock_assessment_type" DISABLE TRIGGER ALL;

COPY "HelpContentBlock_assessment_type" (id, helptextblock_id, assessmenttype_id) FROM stdin;
396	2	uaZHkcWBdNp0tW0S_IpzJ
397	12	4cekaZp0ZBl6OQWPh5DzXX
398	11	4cekaZp0ZBl6OQWPh5DzXX
399	8	16JV0yi89rGtmhI2gbRjvO
400	3	uaZHkcWBdNp0tW0S_IpzJ
406	6	16AwAbqGsVpQVatC761v2P
409	5	16AwAbqGsVpQVatC761v2P
351	4	2Dvvg8ewAWV6WMY3BXPA2m
352	4	3xjZEjnSo4Ut1W7ImwRbxT
353	4	16AwAbqGsVpQVatC761v2P
354	4	2Eja9QpUfbZUcnXSgsCKLH
355	4	2dljiL0mPCAuOoYembVzqE
356	4	UPekHswKQMTgQFFySpp7qb
357	4	1TkCcuXUfnGlbXRF9hzNka
358	4	uaZHkcWBdNp0tW0S_IpzJ
359	4	16JV0yi89rGtmhI2gbRjvO
360	4	2KPUKhZ4VkZw5AYYEgbFSu
361	4	gmQBZsZ64NhwNFReaa6rc
362	4	4cekaZp0ZBl6OQWPh5DzXX
363	4	UsqaMp2kgYYmN3E9p9A66
367	1	uaZHkcWBdNp0tW0S_IpzJ
368	13	16JV0yi89rGtmhI2gbRjvO
369	13	3xjZEjnSo4Ut1W7ImwRbxT
370	13	gmQBZsZ64NhwNFReaa6rc
371	13	UsqaMp2kgYYmN3E9p9A66
372	13	2KPUKhZ4VkZw5AYYEgbFSu
375	9	16JV0yi89rGtmhI2gbRjvO
376	9	3xjZEjnSo4Ut1W7ImwRbxT
377	9	gmQBZsZ64NhwNFReaa6rc
378	9	UsqaMp2kgYYmN3E9p9A66
379	9	2KPUKhZ4VkZw5AYYEgbFSu
381	7	16AwAbqGsVpQVatC761v2P
\.


ALTER TABLE "HelpContentBlock_assessment_type" ENABLE TRIGGER ALL;

--
-- Name: HelpContentBlock_assessment_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"HelpContentBlock_assessment_type_id_seq"', 411, true);


--
-- PostgreSQL database dump complete
--

