--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: ProductTypePermissionSet; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "ProductTypePermissionSet" DISABLE TRIGGER ALL;

COPY "ProductTypePermissionSet" (uuid, remarks, is_test, modified_time, created_time, title) FROM stdin;
\.


ALTER TABLE "ProductTypePermissionSet" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

