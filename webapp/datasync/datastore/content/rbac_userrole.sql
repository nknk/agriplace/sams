--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: rbac_userrole; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE rbac_userrole DISABLE TRIGGER ALL;

COPY rbac_userrole (id, role_id, user_id) FROM stdin;
\.


ALTER TABLE rbac_userrole ENABLE TRIGGER ALL;

--
-- Name: rbac_userrole_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('rbac_userrole_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

