--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: ACLDataObjectState_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ACLDataObjectState_id_seq"', 1, false);


--
-- Data for Name: DataObjectState; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "DataObjectState" DISABLE TRIGGER ALL;

COPY "DataObjectState" (id, name) FROM stdin;
\.


ALTER TABLE "DataObjectState" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

