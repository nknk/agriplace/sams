--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: rbac_grant; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE rbac_grant DISABLE TRIGGER ALL;

COPY rbac_grant (id, assignment, from_role_id, to_role_id) FROM stdin;
\.


ALTER TABLE rbac_grant ENABLE TRIGGER ALL;

--
-- Name: rbac_grant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('rbac_grant_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

