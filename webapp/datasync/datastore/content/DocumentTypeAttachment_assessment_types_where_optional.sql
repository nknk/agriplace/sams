--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: DocumentTypeAttachment_assessment_types_where_optional; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "DocumentTypeAttachment_assessment_types_where_optional" DISABLE TRIGGER ALL;

COPY "DocumentTypeAttachment_assessment_types_where_optional" (id, documenttype_id, assessmenttype_id) FROM stdin;
36	4MwyiRQZa1DRHyABjXdMcE	uaZHkcWBdNp0tW0S_IpzJ
37	6MKpccQe2KiECiC4zvvnci	uaZHkcWBdNp0tW0S_IpzJ
38	2EazvIsvQjYnYxiGe3igh5	uaZHkcWBdNp0tW0S_IpzJ
39	Z5pwo4wZllvk1dc1EllTr	uaZHkcWBdNp0tW0S_IpzJ
42	25tR64snlaZ4RNUBb0b7pA	uaZHkcWBdNp0tW0S_IpzJ
46	240t2TnONbIPalRzDibjeu	uaZHkcWBdNp0tW0S_IpzJ
59	1dB4JtqkmgNsu26ksuEyZy	uaZHkcWBdNp0tW0S_IpzJ
60	1dB4JtqkmgNsu26ksuEyZy	PTrVn8Wn4rw7zTEFnm2Kje
68	5dzWnTsZgcDLLiDUijTb2j	uaZHkcWBdNp0tW0S_IpzJ
69	YWf1MqiH9ZW9EDsPqQdnx	uaZHkcWBdNp0tW0S_IpzJ
71	XQ0ALpetkdLOqz4a8eORy	uaZHkcWBdNp0tW0S_IpzJ
73	G3S4XTYApCGjzUXT5uQxuJ	PTrVn8Wn4rw7zTEFnm2Kje
75	ddFxStunB2NcYMXETkWiaE	PTrVn8Wn4rw7zTEFnm2Kje
76	46ZOyPIfdrKCPaJPJwPD1Q	uaZHkcWBdNp0tW0S_IpzJ
\.


ALTER TABLE "DocumentTypeAttachment_assessment_types_where_optional" ENABLE TRIGGER ALL;

--
-- Name: DocumentTypeAttachment_assessment_types_where_optional_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"DocumentTypeAttachment_assessment_types_where_optional_id_seq"', 76, true);


--
-- PostgreSQL database dump complete
--

