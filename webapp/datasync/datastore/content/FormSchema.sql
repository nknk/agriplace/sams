--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: FormSchema; Type: TABLE DATA; Schema: public; Owner: -
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE "FormSchema" DISABLE TRIGGER ALL;

COPY "FormSchema" (uuid, remarks, is_test, modified_time, created_time, json) FROM stdin;
\.


ALTER TABLE "FormSchema" ENABLE TRIGGER ALL;

--
-- PostgreSQL database dump complete
--

