# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('datasync', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContentDataImportWizard',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(max_length=36, serialize=False, editable=False, primary_key=True, blank=True)),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('version', models.CharField(default=b'1.0.0', max_length=10)),
                ('assessment_type_data', models.FileField(max_length=255, null=True, upload_to=b'content_data', blank=True)),
            ],
            options={
                'ordering': ['version'],
                'abstract': False,
                'db_table': 'ContentDataImportWizard',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='assessmenttypefullresource',
            name='assessment_type',
        ),
        migrations.DeleteModel(
            name='AssessmentTypeFullResource',
        ),
    ]
