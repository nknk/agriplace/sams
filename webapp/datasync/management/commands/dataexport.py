import os, subprocess
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from ._migration_settings import *

# helper function to create directory for current database if it not exists
def check_create_folder(name):
    folder_path = os.path.join(DATASTORE_DIR, name)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)


class Command(BaseCommand):
    help = "Data export utility. Works with PostgresDB only, pg_dump and psql utilities must be available."
    can_import_settings = True

    option_list = BaseCommand.option_list + (
        make_option('-d', '--datasets',
            action='store',
            dest='datasets',
            #nargs='*',
            help='List of names(comma separated, no spaces) of datasets predefined in _migration_settings.py'),
        )

    def handle(self, *args, **options):
        if options['datasets']:
            for dataset in options['datasets'].split(','):
                if dataset in DATASETS:
                    for db_table in DATASETS[dataset]:
                        check_create_folder(dataset)
                        foldername = "%s/%s" % (DATASTORE_DIR, dataset)
                        filename = "%s/%s.sql" % (foldername, db_table)
                        command_to_run = 'pg_dump -h %s -p %s -U %s -O -Fp -a --disable-triggers -t "\\\"%s\\\"" %s' % (DB_HOST, DB_PORT, DB_USER, db_table, DB_NAME)
                        #print(command_to_run)
                        return_code = subprocess.call('export PGPASSWORD=%s\n %s > %s' % (DB_PASSWORD, command_to_run, filename), shell=True)
                else:
                    self.stdout.write('Unknown dataset name: "%s".' % (dataset,))
