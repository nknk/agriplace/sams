import os, subprocess
from os import walk
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from ._migration_settings import *


class Command(BaseCommand):
    help = "Data import utility. Works with PostgresDB only, pg_dump and psql utilities must be available. Execute every script in folder that passed with -f option."
    can_import_settings = True

    option_list = BaseCommand.option_list + (
        make_option('-f', '--folder',
            action='store',
            dest='foldername',
            help='Name of folder with sql scripts to run.'),
        )

    def handle(self, *args, **options):
        if options['foldername']:
            foldername = options['foldername']
            folder_path = os.path.join(DATASTORE_DIR, foldername)
            if os.path.exists(folder_path):
                f = []
                for (dirpath, dirnames, filenames) in walk(folder_path):
                    f.extend(filenames)
                    break
                for filename in f:
                    #filename = "%s/%s/%s.sql" % (DATASTORE_DIR, DB_NAME, db_table)
                    db_table = filename.split('.')[0]
                    clear_sql_script = 'ALTER TABLE "\\\"%s\\\"" DISABLE TRIGGER ALL; DELETE FROM "\\\"%s\\\""; ALTER TABLE "\\\"%s\\\"" ENABLE TRIGGER ALL;' % (db_table, db_table, db_table)
                    clear_data_command = 'psql -h %s -p %s -d %s -U %s -c "%s"' % (DB_HOST, DB_PORT, DB_NAME, DB_USER, clear_sql_script)
                    insert_data_command = 'cat %s/%s | psql -h %s -p %s -d %s -U %s ' % (folder_path, filename, DB_HOST, DB_PORT, DB_NAME, DB_USER)

                    return_code = subprocess.call('export PGPASSWORD=%s\n %s' % (DB_PASSWORD, clear_data_command), shell=True)
                    return_code = subprocess.call('export PGPASSWORD=%s\n %s' % (DB_PASSWORD, insert_data_command), shell=True)
            else:
                self.stdout.write('Folder "%s" not exists in DATASTORE_DIR. See _migration_settings.py' % (foldername,))


