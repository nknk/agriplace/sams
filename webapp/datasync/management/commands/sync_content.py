from django.core import management
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.management.base import BaseCommand
from django.db.models.loading import get_model


#
# helpers
#

ASSESSMENTS_MODELS = [
    'AssessmentType',
    'ContentPackage',
    'Questionnaire',
    'QuestionLevel',
    'QuestionnaireHeading',
    'QuestionnaireParagraph',
    'PossibleAnswerSet',
    'Question',
    'QuestionTrigger',
    'QuestionTriggerCondition',
    'PossibleAnswer'
]
ATTACHMENTS_MODELS = [
    'DocumentType'
]
FORM_BUILDER_MODELS = [
    'FormSchema',
    'GenericForm'
]
ORGANIZATIONS_MODELS = [
    'CertificationType',
    'ProductType',
    'StandardProductType'
]
RBAC_MODELS = [
    'AccessRule',
    'DataObject',
    'DataObjectState',
    'Permission',
    'RoleAccessRuleLink',
    'Grant',
    'GroupRole',
    'Role',
    'UserRole'
]
APP_SETTINGS_MODELS = [
    'TextBlock',
    'HelpTextBlock'
]
HELP_MODELS = [
    'Screencast',
    'News',
    'Faq'
]


DEPRECATED_FIELDS = ['template', 'document_types']


def print_report(app_name, model_list, simple=False):
    print("\n")
    print("REPORT FOR %s APPLICATION" % app_name)
    print("%s" % "-" * 40)

    for model_name in model_list:
        model_class = get_model(app_name, model_name)
        source_instances = model_class.objects.using('source').all()
        target_instances = model_class.objects.using('target').all()
        print("MODEL NAME: %s -> SOURCE db contains: %s instances, TARGET: %s" % (
            model_name,
            source_instances.count(),
            target_instances.count()
        ))
        if not simple:
            print("    SOURCE db instances: %s" % source_instances)
            print("    TARGET db instances: %s" % target_instances)

    print("%s" % "-" * 40)


def sync_model(app_name, model_name):
    model_class = get_model(app_name, model_name)
    model_pk_name = model_class._meta.pk.name

    for source_instance in model_class.objects.using('source').all():
        source_instance_pk = getattr(source_instance, model_pk_name)

        # like always special case ProductType model
        # for it pk == name, so...
        if model_name == 'ProductType':
            source_instance_name = getattr(source_instance, 'name')
            try:
                model_class.objects.using('target').get(name=source_instance_name)
            except ObjectDoesNotExist:
                source_instance.save(using='target', force_insert=True)
                continue
            except MultipleObjectsReturned:
                # wow - special special case
                print("ERROR please fix Product Type list on TARGET db. more than one %s exists." % source_instance_name)
                continue

        # now we can return to normal flow
        # making decision about what we will do insert or update
        try:
            model_class.objects.using('target').get(pk=source_instance_pk)
            # print("%s exist on TARGET" % source_instance_pk)
            source_instance.save(using='target', force_update=True)
        except ObjectDoesNotExist:
            # print("%s not exist on TARGET" % source_instance_pk)
            source_instance.save(using='target', force_insert=True)

        # lets now save many to many
        target_instance = model_class.objects.using('target').get(pk=source_instance_pk)
        source_instance_real =  model_class.objects.using('source').get(pk=source_instance_pk)
        for field in target_instance._meta.many_to_many:

            if field.name not in DEPRECATED_FIELDS:
                getattr(target_instance, field.name).db_manager('target').clear()
                field_value_list = getattr(source_instance_real, field.name).all()
                if field_value_list:
                    # print("Field %s -> %s, %s" % (field.name, field.rel.to, field_value_list))
                    item_pk_name = field.rel.to._meta.pk.name
                    for item in field_value_list:
                        try:
                            m2m_instance = field.rel.to.objects.using('target').get(pk=getattr(item, item_pk_name))
                        except ObjectDoesNotExist:
                            m2m_instance = None
                            print("ERROR can't find by pk %s in %s" % (getattr(item, item_pk_name), field.rel.to))

                        # second chance - try to find instance by code
                        if not m2m_instance:
                            try:
                                m2m_instance = field.rel.to.objects.using('target').get(code=getattr(item, 'code'))
                            except ObjectDoesNotExist:
                                print("ERROR can't find  by code %s in %s" % (getattr(item, 'code'), field.rel.to))

                        # final assignment
                        if m2m_instance:
                            print(m2m_instance)
                            getattr(target_instance, field.name).db_manager('target').add(m2m_instance)


#
# sync apps
#

def sync_app(app_name, model_list):
    print_report(app_name, model_list, simple=True)
    for model_name in model_list:
        sync_model(app_name, model_name)


class Command(BaseCommand):
    help = "Content data sync script."
    can_import_settings = True

    def handle(self, *args, **options):
        # Step 1.
        # Sync dbs schema
        # ------------------------------------
        management.call_command("migrate", database="source")
        management.call_command("migrate", database="target")

        # Step 2.
        # Load all necessary data from scripts
        # ------------------------------------
        # TODO update scripts to use them as management commands

        # Step 3.
        # Now lets start sync data
        # ------------------------------------
        sync_app('assessments', ASSESSMENTS_MODELS)
        sync_app('attachments', ATTACHMENTS_MODELS)
        sync_app('form_builder', FORM_BUILDER_MODELS)
        sync_app('organization', ORGANIZATIONS_MODELS)
        sync_app('rbac', RBAC_MODELS)
        sync_app('app_settings', APP_SETTINGS_MODELS)
        sync_app('help', HELP_MODELS)

