import os
from django.conf import settings

#paths
PROJECT_DIR = settings.PROJECT_DIR
DATASTORE_DIR = os.path.join(PROJECT_DIR, 'datasync/datastore')

# database settings
DB_NAME = settings.DATABASES['default']['NAME']
DB_USER = settings.DATABASES['default']['USER']
DB_PASSWORD = settings.DATABASES['default']['PASSWORD']
DB_HOST = settings.DATABASES['default']['HOST']
DB_PORT = settings.DATABASES['default']['PORT']

DATASETS = {
    'content': [
        'AccessRule',
        'AssessmentType',
        'Category',
        'Certification',
        'CertificationType',
        'ContentBlock',
        'ContentPackage',
        'DataObject',
        'DataObjectState',
        'DocumentType',
        'DocumentType_assessment_types',
        'DocumentTypeAttachment',
        'DocumentTypeAttachment_assessment_types_where_optional',
        'Faq',
        'FormSchema',
        'GenericForm',
        'HelpContentBlock',
        'HelpContentBlock_assessment_type',
        'News',
        'Permission',
        'PossibleAnswer',
        'PossibleAnswerSet',
        'PossibleAnswer_triggered_questions',
        'ProductIdentifier',
        'ProductPartner',
        'ProductType',
        'ProductTypeGroup',
        'ProductTypeGroup_product_types',
        'ProductTypePermission',
        'ProductTypePermissionSet',
        'ProductTypePermission_assessment_type',
        'ProductTypePermission_questionnaire',
        'Question',
        'Question_document_types',
        'Question_document_types_attachments',
        'QuestionLevel',
        'Questionnaire',
        'QuestionnaireElement',
        'QuestionnaireHeading',
        'QuestionnaireParagraph',
        'QuestionTrigger',
        'QuestionTriggerCondition',
        'RoleAccessRuleLink',
        'Screencast',
        'StandardProductType',
        'rbac_grant',
        'rbac_grouprole',
        'rbac_role',
        'rbac_userrole'
    ]
}