import os, subprocess
from optparse import make_option

from django.core.management.base import BaseCommand, CommandError

from ._migration_settings import *


class Command(BaseCommand):
    help = "Data import utility. Works with PostgresDB only, pg_dump and pg_restore utilities must be available."
    can_import_settings = True

    def handle(self, *args, **options):
        for db_table in DATASETS['content']:
            filename = "%s/%s/%s.sql" % (DATASTORE_DIR, 'app-agriplace-content', db_table)
            clear_sql_script = 'ALTER TABLE "\\\"%s\\\"" DISABLE TRIGGER ALL; DELETE FROM "\\\"%s\\\""; ALTER TABLE "\\\"%s\\\"" ENABLE TRIGGER ALL;' % (db_table, db_table, db_table)
            clear_data_command = 'psql -h %s -p %s -d %s -U %s -c "%s"' % (DB_HOST, DB_PORT, DB_NAME, DB_USER, clear_sql_script)
            insert_data_command = 'cat %s | psql -h %s -p %s -d %s -U %s ' % (filename, DB_HOST, DB_PORT, DB_NAME, DB_USER)

            return_code = subprocess.call('export PGPASSWORD=%s\n %s' % (DB_PASSWORD, clear_data_command), shell=True)
            return_code = subprocess.call('export PGPASSWORD=%s\n %s' % (DB_PASSWORD, insert_data_command), shell=True)
        print('all ok')

