from django.db import models

from core.models import UuidModel


class ContentDataImportWizard(UuidModel):
    version = models.CharField(max_length=10, default='1.0.0')
    assessment_type_data = models.FileField(max_length=255, upload_to="content_data", blank=True, null=True)


    class Meta(UuidModel.Meta):
        db_table = 'ContentDataImportWizard'
        ordering = ['version']

    def __unicode__(self):
        return u"{}".format(self.version)

