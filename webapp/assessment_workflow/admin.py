from django.contrib import admin
from django.contrib.auth import get_user_model

from assessment_workflow.filters import AssessmentTypeFilter, FarmgroupFilter
from assessment_workflow.forms import AssessmentWorkflowAdminForm

from core.admin import BaseTabularInline, BaseAdmin, BaseAbpAdminMixin
from assessment_workflow.models import AssessmentWorkflow, AssessmentWorkflowFile
from organization.models import OrganizationMembership


class AssessmentWorkflowFilesInline(BaseTabularInline):
    model = AssessmentWorkflowFile
    fields = ('file', 'attachment_type', 'workflow_status', 'created_time', 'modified_time')
    readonly_fields = ('file', 'attachment_type', 'workflow_status', 'created_time', 'modified_time')
    ordering = ('-created_time',)


class AssessmentWorkflowAdmin(BaseAbpAdminMixin, BaseAdmin):
    list_display = ['author_organization', 'assessment_type', 'harvest_year', 'assessment_status', 'judgement', 'created_time']
    list_filter = (AssessmentTypeFilter, 'harvest_year', FarmgroupFilter)

    search_fields = ['uuid', 'author_organization__name']

    form = AssessmentWorkflowAdminForm

    raw_id_fields = ('membership', 'configuration', 'label_configuration',)

    inlines = [
        AssessmentWorkflowFilesInline
    ]

    def get_queryset(self, request):
        assessment_workflows = AssessmentWorkflow.objects.all()
        if self.is_abp_manager(request):
            return assessment_workflows.filter(
                author_organization__in=OrganizationMembership.get_sisters_by_user(request.user)
            ).exclude(assessment_type__kind__in=['agriform', 'agriform2'])
        return assessment_workflows

    def get_fields(self, request, obj=None):
        if self.is_abp_manager(request):
            return ['assessment_type', 'harvest_year', 'assessment_status', 'auditor_organization',
                    'auditor_user', 'audit_preferred_month', 'audit_date_planned', 'audit_date_actual',
                    'document_review_date', 'action_required_date', 'certification_date', 'judgement']
        return list(self.get_form(request, obj, fields=None).base_fields)

    def get_readonly_fields(self, request, obj=None):
        if self.is_abp_manager(request):
            return ['assessment_type', 'harvest_year', 'assessment_status', 'auditor_organization',
                    'auditor_user', 'audit_preferred_month', 'audit_date_planned', 'audit_date_actual',
                    'document_review_date', 'action_required_date', 'certification_date', 'judgement']
        return ['modified_time', 'created_time']

    def get_actions(self, request):
        actions = super(AssessmentWorkflowAdmin, self).get_actions(request)
        if self.is_abp_manager(request):
            del actions['delete_selected']
        return actions

    def save_model(self, request, obj, form, change):
        obj.save()

admin.site.register(AssessmentWorkflow, AssessmentWorkflowAdmin)


class AssessmentWorkflowFileAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'file', 'attachment_type', 'notification_type', 'workflow_status', 'created_time')
    list_filter = ('assessment_workflow', 'attachment_type')
    ordering = ('-created_time',)

    def save_model(self, request, obj, form, change):
        obj.save()

admin.site.register(AssessmentWorkflowFile, AssessmentWorkflowFileAdmin)
