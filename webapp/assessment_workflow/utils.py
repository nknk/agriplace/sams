import json
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from django.utils.translation import ugettext_lazy as _

from assessment_workflow.models import AssessmentWorkflowFile, AssessmentWorkflowImportExport
from assessment_workflow.resources import AssessmentWorkflowResource, AssessmentWorkflowFileResource
from farm_group.models import AssessmentMutationLog
from farm_group.resources import AssessmentMutationLogResource


class WorkflowExport(object):
    def __init__(self, workflows, user_id):
        self.workflows = workflows
        self.user_id = user_id
        self.order_by_column = 'created_time'

    def export_workflows(self):
        data = []
        for workflow in self.workflows:
            standard_json = self.get_workflow_json(workflow)
            data.append(standard_json)

        file_name = 'workflow-{uuid}.json'.format(uuid=str(uuid4()))
        standard = AssessmentWorkflowImportExport()
        standard.file.save(file_name, ContentFile((json.dumps(data))))
        standard.status = _("Success")
        standard.user = get_user_model().objects.get(pk=self.user_id)
        standard.save()

    def get_workflow_json(self, workflow):
        files = AssessmentWorkflowFile.objects.filter(assessment_workflow=workflow).order_by(self.order_by_column)
        logs = AssessmentMutationLog.objects.filter(assessment_workflow=workflow).order_by(self.order_by_column)

        exported_workflow = AssessmentWorkflowResource().export([workflow])
        exported_files = AssessmentWorkflowFileResource().export(files)
        exported_logs = AssessmentMutationLogResource().export(logs)

        return {
            "workflow": json.loads(exported_workflow.json),
            "files": json.loads(exported_files.json),
            "logs": json.loads(exported_logs.json)
        }
