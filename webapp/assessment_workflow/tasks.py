from celery.task import task

from assessment_workflow.models import AssessmentWorkflow
from assessment_workflow.utils import WorkflowExport


@task()
def workflow_export_task(ids, user_id):
    workflows = AssessmentWorkflow.objects.filter(uuid__in=ids)
    standard_export = WorkflowExport(workflows=workflows, user_id=user_id)
    return standard_export.export_workflows()
