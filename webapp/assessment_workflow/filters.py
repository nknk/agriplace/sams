from django.contrib import admin
from django.contrib.auth import get_user_model

from assessment_workflow.models import AssessmentWorkflow
from assessments.models import AssessmentType
from organization.models import OrganizationMembership, Organization


class AssessmentTypeFilter(admin.SimpleListFilter):
    title = 'Assessment type'
    parameter_name = 'assessment_type'

    def lookups(self, request, model_admin):

        if get_user_model().objects.filter(pk=request.user.pk, groups__name='abp_manager').exists():
            return AssessmentWorkflow.objects.filter(
                assessment__assessment_type__kind='assessment',
                author_organization__in=OrganizationMembership.get_sisters_by_user(request.user)).values_list(
                'assessment__assessment_type__code', 'assessment__assessment_type__name').distinct()
        else:
            return AssessmentType.objects.filter(kind='assessment').values_list('code', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(assessment_type__code=self.value())
        else:
            return queryset


class FarmgroupFilter(admin.SimpleListFilter):
    title = 'Farm group'
    parameter_name = 'farm_group'

    def lookups(self, request, model_admin):

        if get_user_model().objects.filter(pk=request.user.pk, groups__name='abp_manager').exists():
            return Organization.objects.filter(
                uuid__in=OrganizationMembership.get_parents_by_user(request.user)
            ).values_list('uuid', 'name').distinct()
        else:
            return OrganizationMembership.objects.select_related('primary_organization').values_list(
                'primary_organization__uuid', 'primary_organization__name').distinct()

    def queryset(self, request, queryset):
        if self.value():
            secondary_organizations = OrganizationMembership.objects.filter(
                primary_organization=self.value()).values_list('secondary_organization', flat=True).distinct()
            return queryset.filter(author_organization__uuid__in=secondary_organizations)
        else:
            return queryset
