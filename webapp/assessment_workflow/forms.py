from django import forms

from assessment_workflow.models import AssessmentWorkflow
from assessments.models import AssessmentType, PLATFORM_CHOICES
from organization.models import Organization


class AssessmentWorkflowAdminForm(forms.ModelForm):

    class Meta:
        model = AssessmentWorkflow
        fields = '__all__'


class WorkflowAbpCreateForm(forms.ModelForm):
    farm_group = forms.ModelChoiceField(queryset=None, empty_label=None)
    selected_workflow = forms.ChoiceField(choices=PLATFORM_CHOICES, required=True)

    def __init__(self, primary_organization_ids, *args, **kwargs):
        super(WorkflowAbpCreateForm, self).__init__(*args, **kwargs)
        self.fields['farm_group'].queryset = Organization.objects.filter(pk__in=primary_organization_ids)
        self.fields['assessment_type'].queryset = AssessmentType.objects.filter(kind='assessment')

    class Meta:
        model = AssessmentWorkflow
        fields = ['farm_group', 'assessment_type', 'selected_workflow', 'harvest_year']