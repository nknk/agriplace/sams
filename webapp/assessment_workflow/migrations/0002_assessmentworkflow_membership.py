# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0016_auto_20170110_0752'),
        ('assessment_workflow', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmentworkflow',
            name='membership',
            field=models.ForeignKey(related_name='workflows', verbose_name='membership', to='organization.OrganizationMembership', null=True),
        ),
    ]
