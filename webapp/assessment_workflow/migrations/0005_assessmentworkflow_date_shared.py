# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_workflow', '0004_auto_20170113_0852'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmentworkflow',
            name='date_shared',
            field=models.DateField(null=True, verbose_name='Date shared', blank=True),
        ),
    ]
