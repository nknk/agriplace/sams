# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0013_merge'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('organization', '0014_auto_20170103_0703'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssessmentWorkflow',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('assessment_status', models.CharField(default=b'internal_audit_request', max_length=50, verbose_name='Assessment status', choices=[(b'internal_audit_request', 'Internal audit request'), (b'audit_hzpc', 'Internal audit pending'), (b'self_managed', 'Self managed'), (b'internal_audit_open', 'Internal audit open'), (b'internal_audit_done', 'Internal audit done'), (b'document_review_done', 'Document review done'), (b'audit_done', 'Audit done'), (b'action_required', 'Action required'), (b'action_done', 'Action done'), (b'reaudit_required', 'Reaudit required'), (b'certified', 'Certified'), (b'rejected', 'Rejected')])),
                ('harvest_year', models.IntegerField(default=2017, null=True, verbose_name='Harvest year', choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018)])),
                ('audit_preferred_month', models.IntegerField(null=True, verbose_name='Preferred audit month', blank=True)),
                ('audit_date_planned', models.DateField(null=True, verbose_name='Planned audit date', blank=True)),
                ('audit_date_actual', models.DateField(null=True, verbose_name='Actual audit date', blank=True)),
                ('shared_with_auditor', models.NullBooleanField(verbose_name='Shared with auditor?')),
                ('shared_with_coordinator', models.NullBooleanField(verbose_name='Shared with coordinator?')),
                ('workflow_status', models.CharField(default=b'initial', max_length=50, verbose_name='Workflow status', choices=[(b'initial', 'Initial'), (b'in_progress', 'In progress'), (b'final', 'Final'), (b'error', 'Error')])),
                ('judgement', models.CharField(default=b'', max_length=50, verbose_name='Judgement', blank=True, choices=[(b'', b''), (b'positive', 'positive'), (b'negative', 'negative'), (b'reevaluate', 're-evaluation')])),
                ('re_audit', models.BooleanField(default=False, verbose_name='Physical re-audit required')),
                ('action_required_notification_status', models.BooleanField(default=False, verbose_name='Action required notification status')),
                ('overview_description', models.TextField(default=b'', verbose_name='Overview description', blank=True)),
                ('internal_description', models.TextField(default=b'', verbose_name='Internal description', blank=True)),
                ('document_review_date', models.DateField(null=True, verbose_name='Document review date', blank=True)),
                ('action_required_date', models.DateField(null=True, verbose_name='Action required date', blank=True)),
                ('certification_date', models.DateField(help_text=b'The date on which user got status certified.', null=True, verbose_name='Certification date', blank=True)),
                ('selected_workflow', models.CharField(default=b'agriplace', max_length=50, verbose_name='Selected workflow', choices=[(b'agriplace', b'Agriplace'), (b'hzpc', b'Hzpc'), (b'farm_group', b'FarmGroup')])),
                ('assessment', models.ForeignKey(blank=True, to='assessments.Assessment', null=True)),
                ('assessment_type', models.ForeignKey(blank=True, to='assessments.AssessmentType', null=True)),
                ('auditor_organization', models.ForeignKey(blank=True, to='organization.Organization', null=True)),
                ('auditor_user', models.ForeignKey(related_name='assessment_workflow_auditor', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('author', models.ForeignKey(related_name='assessment_workflow_author', to=settings.AUTH_USER_MODEL)),
                ('author_organization', models.ForeignKey(related_name='assessment_workflow_organization', to='organization.Organization', null=True)),
            ],
            options={
                'db_table': 'AssessmentWorkflow',
                'permissions': (('view_assessment_workflow', 'Can view assessment workflow'),),
            },
        ),
        migrations.CreateModel(
            name='AssessmentWorkflowFile',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('file', models.FileField(max_length=255, upload_to=b'farm_group/reports/tmp')),
                ('attachment_type', models.CharField(max_length=50, choices=[(b'audit_summary', 'Audit summary'), (b'proof_of_participation', 'Proof of participation')])),
                ('workflow_status', models.CharField(default=b'', max_length=50, verbose_name='Workflow file status', blank=True, choices=[(b'active', 'Active'), (b'deprecated', 'Deprecated')])),
                ('notification_type', models.CharField(default=b'', max_length=50, verbose_name='Notification type', blank=True, choices=[(b'audit_result', 'Audit result'), (b'acton_reminder', 'Corrective acton reminder'), (b'obtaining_certificate', 'Obtaining certificate')])),
                ('assessment_workflow', models.ForeignKey(related_name='assessment_workflow_files', to='assessment_workflow.AssessmentWorkflow')),
            ],
            options={
                'db_table': 'AssessmentWorkflowFile',
                'permissions': (('view_assessment_workflow_file', 'Can view assessment workflow file'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='assessmentworkflow',
            unique_together=set([('author', 'assessment_type', 'harvest_year', 'selected_workflow')]),
        ),
    ]
