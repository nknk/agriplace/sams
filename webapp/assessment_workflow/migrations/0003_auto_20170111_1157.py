# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_workflow', '0002_assessmentworkflow_membership'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='assessmentworkflow',
            unique_together=set([]),
        ),
    ]
