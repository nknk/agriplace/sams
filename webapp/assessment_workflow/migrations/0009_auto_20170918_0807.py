# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0024_dynamicevidenceconfiguration_is_default'),
        ('assessment_workflow', '0008_auto_20170412_0923'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmentworkflow',
            name='configuration',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='configuration',blank=True, to='assessments.DynamicEvidenceConfiguration', null=True),
        ),
        migrations.AlterField(
            model_name='assessmentworkflow',
            name='assessment_type',
            field=models.ForeignKey(to='assessments.AssessmentType'),
        ),
    ]
