# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def remove_agriforms_workflows(apps, schema_editor):
    """Remove all the workflows of assessment type of kind non-assessment."""

    AssessmentWorkflow = apps.get_model("assessment_workflow", "AssessmentWorkflow")
    AssessmentWorkflow.objects.exclude(assessment_type__kind='assessment').delete()


def revert_agriforms_workflows(apps, schema_editor):
    """Backward migration is not needed."""
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_workflow', '0006_merge'),
    ]

    operations = [
        migrations.RunPython(remove_agriforms_workflows, revert_agriforms_workflows)
    ]
