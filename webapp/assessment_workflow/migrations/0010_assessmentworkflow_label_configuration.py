# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0026_auto_20171003_0946'),
        ('assessment_workflow', '0009_auto_20170918_0807'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmentworkflow',
            name='label_configuration',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='label configuration', blank=True, to='assessments.DynamicLabelConfiguration', null=True),
        ),
    ]
