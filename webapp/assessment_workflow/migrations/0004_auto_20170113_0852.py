# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('assessment_workflow', '0003_auto_20170111_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assessmentworkflow',
            name='author',
            field=models.ForeignKey(related_name='assessment_workflow_author', to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
