# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime
from django.db import migrations, models


def migrate_audit_date(apps, schema_editor):
    """Migrate the audit_date to audit_date_start and audit_date_end fields."""
    AssessmentWorkflow = apps.get_model('assessment_workflow', 'AssessmentWorkflow')
    workflows = AssessmentWorkflow.objects.all()

    for workflow in workflows:
        audit_date_planned = workflow.audit_date_planned
        if audit_date_planned:
            print("migrating audit dates for workflow: {}".format(workflow.pk))
            workflow.audit_date_actual_start = audit_date_planned.replace(hour=00, minute=00)
            workflow.audit_date_actual_end = audit_date_planned.replace(hour=23, minute=59)

            workflow.save()


def revert_audit_date(apps, schema_editor):
    """Backward migration is not needed."""
    pass


class Migration(migrations.Migration):
    dependencies = [
        ('assessment_workflow', '0007_auto_20170315_1227'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='assessmentworkflow',
            options={'permissions': (('view_assessmentworkflow', 'Can view assessment workflow'),)},
        ),
        migrations.AlterModelOptions(
            name='assessmentworkflowfile',
            options={'permissions': (('view_assessmentworkflowfile', 'Can view assessment workflow file'),)},
        ),
        migrations.AddField(
            model_name='assessmentworkflow',
            name='audit_date_actual_end',
            field=models.DateTimeField(null=True, verbose_name='Actual audit date end', blank=True),
        ),
        migrations.AddField(
            model_name='assessmentworkflow',
            name='audit_date_actual_start',
            field=models.DateTimeField(null=True, verbose_name='Actual audit date start', blank=True),
        ),
        migrations.AlterField(
            model_name='assessmentworkflow',
            name='audit_date_planned',
            field=models.DateTimeField(null=True, verbose_name='Planned audit date', blank=True),
        ),
        migrations.RunPython(migrate_audit_date, revert_audit_date)
    ]
