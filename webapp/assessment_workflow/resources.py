from import_export import resources
from import_export.widgets import ForeignKeyWidget
from django.conf import settings
from import_export import fields

from assessment_workflow.models import AssessmentWorkflow, AssessmentWorkflowFile
from organization.models import Organization

UUID_MODEL_FIELDS = (
    'uuid', 'remarks', 'is_test', 'modified_time', 'created_time'
)

ASSESSMENT_WORKFLOW_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'author', 'author_organization', 'membership', 'assessment_type', 'assessment',
    'assessment_status', 'harvest_year', 'auditor_organization', 'auditor_user', 'audit_preferred_month',
    'audit_date_planned', 'audit_date_actual', 'shared_with_auditor', 'shared_with_coordinator',
    'workflow_status', 'judgement', 're_audit', 'action_required_notification_status',
    'overview_description', 'internal_description', 'document_review_date', 'action_required_date',
    'certification_date', 'selected_workflow'
)

ASSESSMENT_WORKFLOW_FILE_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'file', 'attachment_type', 'assessment_workflow', 'workflow_status', 'notification_type'
)


# Import/Export resources for assessment workflow
class AssessmentWorkflowResource(resources.ModelResource):
    author = fields.Field(column_name='author', attribute='author',
                          widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    auditor_user = fields.Field(column_name='auditor_user', attribute='auditor_user',
                                widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    class Meta:
        fields = ASSESSMENT_WORKFLOW_RESOURCE_FIELDS
        export_order = ASSESSMENT_WORKFLOW_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentWorkflow


# Import/Export resources for assessment workflow files
class AssessmentWorkflowFileResource(resources.ModelResource):
    class Meta:
        fields = ASSESSMENT_WORKFLOW_FILE_RESOURCE_FIELDS
        export_order = ASSESSMENT_WORKFLOW_FILE_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentWorkflowFile
