from datetime import datetime

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.core.files import File
from django.core.urlresolvers import reverse
from django.db import models, IntegrityError
from django.template.defaultfilters import date as _date
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _
from translations.helpers import tolerant_ugettext
from django.core.exceptions import ValidationError

from assessment_workflow.managers import AssessmentWorkflowManager, INTERNAL_AUDIT_REQUEST
from assessments.models import Assessment, AssessmentType, DynamicEvidenceConfiguration, HARVEST_YEAR_CHOICES, \
    PLATFORM_CHOICES, DynamicLabelConfiguration, BackgroundEvidenceCopy
from core.models import UuidModel
from notifications.models import (
    ACTION_REMINDER, ATTACHMENT_TYPES, AUDIT_RESULT, OBTAINING_CERTIFICATE, PROOF_OF_PARTICIPATION
)
from organization.models import Organization, OrganizationMembership
from rbac.utils import membership_has_privilege
from sams.constants import ICONS

try:
    from django.apps import apps

    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model

TODAY = datetime.now()

ASSESSMENT_WORKFLOW_STATUSES = [
    ("initial", _("Initial")),
    ("in_progress", _("In progress")),
    ("final", _("Final")),
    ("error", _("Error"))
]

WORKFLOW_FILES_STATUSES = [
    ("active", _("Active")),
    ("deprecated", _("Deprecated"))
]

NOTIFICATION_WITH_FILE_TYPES = [
    (AUDIT_RESULT, _("Audit result")),
    (ACTION_REMINDER, _("Corrective acton reminder")),
    (OBTAINING_CERTIFICATE, _("Obtaining certificate"))
]

ASSESSMENT_WORKFLOW_JUDGEMENTS = [
    ("", ""),
    ("positive", _("positive")),
    ("negative", _("negative")),
    ("reevaluate", _("re-evaluation"))
]

ASSESSMENT_STATUSES = [
    (INTERNAL_AUDIT_REQUEST, _("Internal audit request")),
    ("audit_hzpc", _("Internal audit pending")),
    ("self_managed", _("Self managed")),
    ("internal_audit_open", _("Internal audit open")),
    ("internal_audit_done", _("Internal audit done")),
    ("document_review_done", _("Document review done")),
    ("audit_done", _("Audit done")),
    ("action_required", _("Action required")),
    ("action_done", _("Action done")),
    ("reaudit_required", _("Reaudit required")),
    ("certified", _("Certified")),
    ("rejected", _("Rejected"))
]

CLOSED_STATUSES = ['certified', 'rejected']


class AssessmentWorkflow(UuidModel):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="assessment_workflow_author", null=True)
    author_organization = models.ForeignKey(
        'organization.Organization', related_name="assessment_workflow_organization", null=True
    )
    membership = models.ForeignKey(OrganizationMembership, related_name="workflows", verbose_name=_("membership"), null=True)
    assessment_type = models.ForeignKey(AssessmentType)
    configuration = models.ForeignKey(DynamicEvidenceConfiguration, verbose_name=_("configuration"), blank=True, null=True, on_delete=models.SET_NULL)
    label_configuration = models.ForeignKey(DynamicLabelConfiguration, verbose_name=_("label configuration"), blank=True, null=True, on_delete=models.SET_NULL)
    assessment = models.ForeignKey(Assessment, blank=True, null=True)
    assessment_status = models.CharField(
        _("Assessment status"),
        max_length=50,
        choices=ASSESSMENT_STATUSES,
        default=INTERNAL_AUDIT_REQUEST
    )
    harvest_year = models.IntegerField(
        _('Harvest year'),
        choices=HARVEST_YEAR_CHOICES,
        default=TODAY.year,
        null=True
    )
    # auditor
    auditor_organization = models.ForeignKey(Organization, blank=True, null=True)
    auditor_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="assessment_workflow_auditor",
        blank=True,
        null=True
    )
    # important dates
    audit_preferred_month = models.IntegerField(
        _('Preferred audit month'),
        blank=True,
        null=True
    )
    audit_date_planned = models.DateTimeField(_("Planned audit date"), blank=True, null=True)
    audit_date_actual = models.DateField(_("Actual audit date"), blank=True, null=True)
    audit_date_actual_start = models.DateTimeField(_("Actual audit date start"), blank=True, null=True)
    audit_date_actual_end = models.DateTimeField(_("Actual audit date end"), blank=True, null=True)
    date_shared = models.DateField(_("Date shared"), blank=True, null=True)
    # helpers
    shared_with_auditor = models.NullBooleanField(_("Shared with auditor?"))
    shared_with_coordinator = models.NullBooleanField(_("Shared with coordinator?"))
    workflow_status = models.CharField(
        _("Workflow status"),
        max_length=50,
        choices=ASSESSMENT_WORKFLOW_STATUSES,
        default="initial"
    )
    judgement = models.CharField(
        _("Judgement"),
        max_length=50,
        choices=ASSESSMENT_WORKFLOW_JUDGEMENTS,
        default="",
        blank=True
    )
    re_audit = models.BooleanField(_("Physical re-audit required"), default=False)
    action_required_notification_status = models.BooleanField(_("Action required notification status"), default=False)
    overview_description = models.TextField(_("Overview description"), blank=True, default="")
    internal_description = models.TextField(_("Internal description"), blank=True, default="")
    document_review_date = models.DateField(_("Document review date"), blank=True, null=True)
    action_required_date = models.DateField(_("Action required date"), blank=True, null=True)

    certification_date = models.DateField(_("Certification date"), null=True, blank=True,
                                          help_text="The date on which user got status certified.")

    selected_workflow = models.CharField(
        _("Selected workflow"), choices=PLATFORM_CHOICES, max_length=50,
        default='agriplace'
    )
    objects = AssessmentWorkflowManager()

    def __init__(self, *args, **kwargs):

        super(AssessmentWorkflow, self).__init__(*args, **kwargs)
        self.__important_fields = ['assessment_status', 'judgement']

        for field in self.__important_fields:
            setattr(self, '__original_{}'.format(field), getattr(self, field))

    class Meta:
        db_table = 'AssessmentWorkflow'
        permissions = (
            ('view_assessmentworkflow', 'Can view assessment workflow'),
        )

    CONFIGURATION_ERROR = 'Configuration does not match with the assessment type.'

    def clean(self):
        if self.configuration and self.configuration.assessment_type != self.assessment_type:
            raise ValidationError({'configuration': _(self.CONFIGURATION_ERROR)})

    def get_configuration(self):
        if not self.configuration:
            try:
                dec = DynamicEvidenceConfiguration.objects.get(assessment_type=self.assessment_type, is_default=True)
            except ObjectDoesNotExist:
                dec = None

            return dec

        return self.configuration


    def set_default_configuration(self):

        try:
            configuration = DynamicEvidenceConfiguration.objects.get(assessment_type=self.assessment_type,
                                                                     is_default=True)
        except ObjectDoesNotExist:
            configuration = None

        self.configuration = configuration
        self.save()
        return self


    def get_text(self):
        return u"{} {}".format(
            tolerant_ugettext(self.assessment_type.name), str(self.harvest_year)
        )

    def get_url(self):
        return self.get_assessment_url()

    def get_icon_class(self):
        if BackgroundEvidenceCopy.objects.filter(target_assessment=self.assessment, status="pending").exists():
            return ICONS['assessment_reuse']

        return ICONS['assessment']

    def get_status(self):
        return self.get_assessment_status_display()

    def get_progress(self):
        if self.assessment:
            return self.assessment.get_progress()
        else:
            return '-'

    def get_email_attachments(self, attachment_types):
        results = self.assessment_workflow_files.filter(attachment_type__in=attachment_types).order_by('-created_time')
        files = {}
        for workflow_file in results:
            if workflow_file.attachment_type in attachment_types:
                files[workflow_file.attachment_type] = workflow_file.file
                attachment_types.remove(workflow_file.attachment_type)
                if not attachment_types:
                    break

        return files

    def get_audit_preferred_month_display(self):
        """
        Output the localized month name
        """
        if not self.audit_preferred_month:
            return None
        dummy_date = datetime(2015, self.audit_preferred_month, 1)
        month_name = _date(dummy_date, "F")
        return month_name.title()

    def get_audit_date(self):
        """
        Get the audit date either the actual, planned or -
        """
        if self.audit_date_actual:
            return self.audit_date_actual
        elif self.audit_date_planned:
            return u"{} {}".format(
                self.audit_date_planned,
                force_text(_('(planned)')))
        else:
            return '-'

    def get_assessment_url(self):
        """
        Retrieve the assessment url

        Could be the create url when no assessment present
        """
        if self.assessment_status == 'self_managed':
            return None
        elif not self.assessment:
            return reverse(
                'create-assessment-from-workflow', args=[self.membership.pk, self.pk]
            )
        else:
            return reverse(
                'my_assessment_detail',
                args=[self.membership.pk, self.assessment.pk]
            )

    def fill_global_gap_registration_form(self, audit_prefered_month):
        try:
            assessment_type = AssessmentType.objects.get(uuid=self.assessment_type)
            if assessment_type.code == "globalgap_ifa" or assessment_type.code == "globalgap_ifa_v5":
                if audit_prefered_month:
                    self.audit_prefered_month = audit_prefered_month
                    self.assessment_status = "audit_hzpc"
                    self.workflow_status = "in_progress"
                else:
                    self.assessment_status = "self_managed"
                    self.workflow_status = "final"
                self.save()
            return self
        except ObjectDoesNotExist:
            return None

    def set_planned_audit_date(self, planned_audit_date):
        self.audit_date_planned = planned_audit_date
        self.save()
        return self

    def set_audit_date_actual_start(self, audit_date_actual_start):
        self.audit_date_actual_start = audit_date_actual_start
        self.save()
        return self

    def set_audit_date_actual_end(self, audit_date_actual_end):
        self.audit_date_actual_end = audit_date_actual_end
        self.save()
        return self

    def set_judgement(self, judgement):
        self.judgement = judgement
        self.save()
        return self

    def set_reaudit(self, reaudit):
        self.reaudit = reaudit
        self.save()
        return self

    def set_internal_description(self, text):
        self.internal_description = text
        self.save()
        return self

    def set_overview_description(self, text):
        self.overview_description = text
        self.save()
        return self

    def set_auditor(self, auditor):
        self.auditor_organization = auditor
        self.save()
        return self

    def assign_assessment(self, assessment):
        if self.assessment_type == assessment.assessment_type:
            self.assessment = assessment
            self.assessment_status = "internal_audit_open"
        else:
            self.workflow_status = "error"
        self.save()
        return self

    def assign_auditor(self, auditor_user_id, auditor_organization_uuid):
        if not auditor_user_id and not auditor_organization_uuid:
            self.auditor_organization = None
            self.auditor_user = None
            self.save()
            return self
        try:
            auditor_user = get_model(settings.AUTH_USER_MODEL).objects.get(pk=auditor_user_id)
            auditor_organization = Organization.objects.get(uuid=auditor_organization_uuid)
            self.auditor_organization = auditor_organization
            self.auditor_user = auditor_user
            self.save()
            return self
        except ObjectDoesNotExist:
            return None

    def share_with_auditor(self):
        self.shared_with_auditor = True
        self.assessment_status = "internal_audit_done"
        self.save()
        return self

    def perform_document_review(self):
        self.assessment_status = "document_review_done"
        self.save()
        return self

    def share_with_coordinator(self):
        self.shared_with_coordinator = True
        if self.assessment_status == 'document_review_done':
            self.assessment_status = "audit_done"
        elif self.assessment_status == 'action_required':
            self.assessment_status = "action_done"
        elif self.assessment_status == 'reaudit_required':
            self.assessment_status = "audit_done"
        self.save()
        return self

    def confirm_judgement(self, user, membership):
        from farm_group.models import AssessmentMutationLog
        from farm_group.signals import save_mutation_log, END_JUDGEMENT_ADDED
        mutation = _("End judgement '{0}' added").format(dict(ASSESSMENT_WORKFLOW_JUDGEMENTS).get(self.judgement))
        judgement_text = _("End judgement")
        save_mutation_log(
            self,
            self.assessment,
            None,
            judgement_text,
            mutation,
            user,
            membership.role,
            AssessmentMutationLog.JUDGEMENT_MUTATION,
            priority=END_JUDGEMENT_ADDED
        )

        # audit_done 1st round
        if self.assessment_status == 'audit_done':
            if self.judgement == "negative":
                self.judgement = ""
                self.assessment_status = "action_required"
            elif self.judgement == "positive":
                self.assessment_status = "certified"
                self.assessment.state = 'closed'
                self.assessment.save()
                self.certification_date = datetime.now().date()
            elif self.judgement == "reevaluate":
                self.assessment_status = "reaudit_required"
        # action_done 2nd round
        elif self.assessment_status == 'action_done':
            if self.judgement == "negative":
                self.assessment_status = "rejected"
                self.assessment.state = 'closed'
                self.assessment.save()
            elif self.judgement == "positive":
                self.assessment_status = "certified"
                self.assessment.state = 'closed'
                self.assessment.save()
                self.certification_date = datetime.now().date()
        self.save()
        return self

    def has_privilege(self, membership, name, data_object):
        return membership_has_privilege(membership, name, data_object, self.assessment_status)

    def has_assessment_status_changed(self):
        field = 'assessment_status'
        orig = '__original_{}'.format(field)
        if getattr(self, orig) != getattr(self, field):
            return True

        return False

    def has_document_review_done(self):
        if self.has_assessment_status_changed() and self.assessment_status == 'document_review_done':
            return True

        return False

    def has_action_required_done(self):
        if self.has_assessment_status_changed() and self.assessment_status == 'action_required':
            return True

        return False

    def check_uniqueness(self):
        if not self.pk and self.selected_workflow != 'agriplace':
            workflow = AssessmentWorkflow.objects.filter(
                membership=self.membership,
                assessment_type=self.assessment_type,
                harvest_year=self.harvest_year
            )
            if workflow:
                raise IntegrityError(
                    "Workflow context already exists for (org=\"{}\", assessment type=\"{}\" "
                    "and harvest year=\"{}\" ) combination.".format(
                        self.author_organization,
                        self.assessment_type,
                        self.harvest_year
                    )
                )

    def save(self, *args, **kwargs):
        self.check_uniqueness()

        if self.configuration and self.configuration.assessment_type != self.assessment_type:
            raise ValidationError(self.CONFIGURATION_ERROR)

        if self.has_document_review_done():
            self.document_review_date = datetime.now()

        if self.has_action_required_done():
            self.action_required_date = datetime.now()

        return super(AssessmentWorkflow, self).save(*args, **kwargs)

    def deprecate_old_files(self, file_type, notification):
        if file_type == PROOF_OF_PARTICIPATION:
            AssessmentWorkflowFile.objects.filter(
                attachment_type=file_type,
                assessment_workflow=self,
                workflow_status=''
            ).update(
                notification_type=notification,
                workflow_status='deprecated',
                modified_time=datetime.now()
            )
        AssessmentWorkflowFile.objects.filter(
            attachment_type=file_type,
            assessment_workflow=self,
            notification_type=notification,
            workflow_status='active'
        ).update(
            workflow_status='deprecated',
            modified_time=datetime.now()
        )

    def save_workflow_file(self, notification, file_path, file_name, file_type):
        if notification:
            self.deprecate_old_files(file_type, notification)
            context_file = AssessmentWorkflowFile(
                attachment_type=file_type,
                assessment_workflow=self,
                notification_type=notification,
                workflow_status='active'
            )
            with open(file_path) as f:
                context_file.file.save(file_name, File(f))
                context_file.save()

    @classmethod
    def get_workflow_by_assessment(cls, assessment):
        try:
            return cls.objects.get(assessment=assessment)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            return None


class AssessmentWorkflowFile(UuidModel):
    file = models.FileField(max_length=255, upload_to='farm_group/reports/tmp')
    attachment_type = models.CharField(choices=ATTACHMENT_TYPES, max_length=50)
    assessment_workflow = models.ForeignKey(AssessmentWorkflow, related_name="assessment_workflow_files")
    workflow_status = models.CharField(
        _("Workflow file status"),
        max_length=50,
        choices=WORKFLOW_FILES_STATUSES,
        default="",
        blank=True
    )
    notification_type = models.CharField(
        _("Notification type"),
        max_length=50,
        choices=NOTIFICATION_WITH_FILE_TYPES,
        default="",
        blank=True
    )

    class Meta:
        db_table = 'AssessmentWorkflowFile'
        permissions = (
            ('view_assessmentworkflowfile', 'Can view assessment workflow file'),
        )


def get_workflow_file_name(instance, filename):
    return u'exported/workflow/{file}'.format(
        file=filename
    )


class AssessmentWorkflowImportExport(UuidModel):
    export_date = models.DateTimeField(auto_now_add=True)
    import_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=255, default="pending")
    file = models.FileField(max_length=255, verbose_name='json filename', upload_to=get_workflow_file_name)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    import_outcome = models.TextField(max_length=1000, null=True, blank=True)

    class Meta:
        db_table = 'AssessmentWorkflowExport'
