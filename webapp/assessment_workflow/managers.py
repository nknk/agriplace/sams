from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db import IntegrityError
from django.db.models import Manager
from django.db.models.aggregates import Count

from assessments.models import AssessmentType, Assessment, TODAY
from farm_group.utils import BLANK_JUDGEMENT_ERROR_MSG
from core.constants import INTERNAL_INSPECTOR
from organization.models import OrganizationMembership, AGRIPLACE

ASSESSMENT_ANSWERS = 'Assessment answers'
EVIDENCE = 'Evidence'
AUDIT_JUDGEMENTS = 'Audit judgements'
EVALUATION_ACTIONS = 'Evaluation actions'
AUDIT_DATE = 'Audit date'
ASSIGN_AUDITOR = 'Assign auditor'
SHARE_WITH_AUDITOR = 'Share with auditor'
SHARE_WITH_COORDINATOR = 'Share with coordinator'
DOCUMENT_REVIEW = 'Document review'
CONFIRM_JUDGEMENT = 'Confirm judgement'
OTHER_REMARKS = 'Other remarks'
INTERNAL_REMARKS = 'Internal remarks'
AUDITOR_DECISION = 'Auditor decision'
PHYSICAL_REAUDIT = 'Physical reaudit'
INTERNAL_AUDIT_REQUEST = "internal_audit_request"


class AssessmentWorkflowManager(Manager):
    def get_assessments(self, membership, is_auditor, is_coordinator):
        if is_auditor:
            return self.auditor_assessments(
                membership=membership
            )
        if is_coordinator:
            return self.coordinator_assessments(
                membership=membership
            )

    def auditor_assessments(self, membership):
        return self.filter(
            auditor_organization=membership.secondary_organization
        ).exclude(
            workflow_status='error'
        ).select_related('assessment').select_related('assessment__organization')

    def coordinator_assessments(self, membership):
        ids = OrganizationMembership.objects.filter(
            primary_organization=membership.primary_organization).distinct().values_list(
            'secondary_organization', flat=True
        )

        return self.exclude(
            assessment_status=""
        ).exclude(
            workflow_status='error'
        ).filter(
            author_organization__uuid__in=ids
        ).select_related('assessment').select_related('assessment__organization')

    def get_organization_auditors(self, membership):
        primary_organization = membership.primary_organization
        ids = OrganizationMembership.objects.filter(
            primary_organization=primary_organization,
            role__name=INTERNAL_INSPECTOR
        ).distinct().values_list(
            'secondary_organization', flat=True
        )
        return User.objects.filter(organizations__uuid__in=ids).distinct()

    def initialize_workflow(self, user, assessment_type, harvest_year, membership, selected_workflow):
        try:
            workflow_context = self.create(
                author=user,
                author_organization=membership.secondary_organization,
                membership=membership,
                assessment_type=assessment_type,
                harvest_year=harvest_year,
                selected_workflow=selected_workflow,
                workflow_status="initial"
            )
            return workflow_context
        except (ObjectDoesNotExist, IntegrityError):
            return None

    def share_with_auditor(self, membership, assessment_id):
        try:
            workflow_context = self.get(
                membership=membership,
                assessment_id=assessment_id,
            )
            if not workflow_context.has_privilege(membership, 'Update', SHARE_WITH_AUDITOR):
                raise PermissionDenied()
            workflow_context.share_with_auditor()
            return workflow_context
        except (ObjectDoesNotExist, IntegrityError):
            return None

    def share_with_coordinator(self, membership, assessment_id):
        try:
            workflow_context = self.get(
                assessment_id=assessment_id,
            )
            if not workflow_context.has_privilege(membership, 'Update', SHARE_WITH_COORDINATOR):
                raise PermissionDenied()
            workflow_context.share_with_coordinator()
            return workflow_context
        except (ObjectDoesNotExist, IntegrityError):
            return None

    def perform_document_review(self, membership, assessment_id):
        try:
            workflow_context = self.get(
                assessment_id=assessment_id,
            )
            if not workflow_context.has_privilege(membership, 'Update', DOCUMENT_REVIEW):
                raise PermissionDenied()
            workflow_context.perform_document_review()
            return workflow_context
        except (ObjectDoesNotExist, IntegrityError):
            return None

    def confirm_judgement(self, user, membership, assessment_id):
        try:
            workflow_context = self.get(
                assessment_id=assessment_id,
            )
            if not workflow_context.has_privilege(membership, 'Update', CONFIRM_JUDGEMENT):
                raise PermissionDenied()
            if workflow_context.judgement == '':
                return BLANK_JUDGEMENT_ERROR_MSG
            else:
                workflow_context.confirm_judgement(user, membership)
            return workflow_context
        except (ObjectDoesNotExist, IntegrityError):
            return None

    def initialize_assessment_and_workflow(self, user, membership, organization, assessment_type_code):
        assessment_type = AssessmentType.objects.get(code=assessment_type_code)
        assessment = Assessment(
            assessment_type=assessment_type,
            created_by=user,
            organization=organization
        )
        assessment.save()

        assessment_workflow = self.initialize_workflow(
            user, assessment_type, TODAY.year, membership, AGRIPLACE
        )

        assessment_workflow.assign_assessment(assessment)
        assessment_workflow.set_default_configuration()

        return assessment, assessment_workflow

