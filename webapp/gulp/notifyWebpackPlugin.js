'use strict';

var notifier = require('node-notifier');

module.exports = function() {
  this.plugin('done', function(stats) {
    var error = stats.compilation.errors[0];
    if (!error) return;

    notifier.notify({
      sound: true,
      title: 'Build Error',
      message: error.message
    });
  });
};
