"use strict"

//
// NEED TO FIX: crashes on 'gulp jsx2po' invocation, run for now with: 'node jsx2po'

//
// This script:
//
//    generates a new djangojs.pot template file in webapp/frontend/locale/ from all tags found in all
//    .js* files in webapp/frontend/react recursively
//
//    merges the new djangojs.pot file into each of the locale/language directories djangojs.po files
//

var
  gulp            = require( 'gulp' ),
  extract         = require( 'jsxgettext-recursive' ),
  babel           = require( 'gulp-babel' ),
  walker          = require( 'walker' ),
  bash            = require( 'child_process' ).exec,
  source          = '../../frontend/react',             // webapp/frontend/react
  tempDir         = './__temp',                         // webapp/gulp/tasks/__temp
  targetDir       = '../../frontend/locale',            // webapp/frontend/locale
  targetFilename  = 'djangojs.pot',
  target          = targetDir+ '/'+ targetFilename,
  tag             = '_t';                               // the translate function identifier that is used
                                                        // in the source files

// in ms
var ASYNC_DELAY= 3000;

gulp.task( 'jsx2js', function(){

  console.log( 'jsx2po: Starting..' );
  console.log( 'creating '+ tempDir+ ', converting '+ source+ '/** to '+ tempDir+ '/**/*.js' );
  return gulp.src( source+ '/**/*.js*' )
    .pipe( babel({ presets: ['es2015', 'react'], ignore: '/**/*.json' }) )
    .pipe( gulp.dest(tempDir) )
});



var extract2pot= function(){

  console.log( 'Creating a backup of '+ targetFilename );
  bash( 'cp -f '+ target+ ' '+ target+ '.bak' );

  console.log( 'extracting '+ tag+ ' tags and render them to '+ targetFilename );
  extract({
    'input-dir'   : './',
    'output-dir'  : targetDir,
    output        : targetFilename,
    keyword       : tag,
    parsers       : {
      '.js'   : 'javascript',
      '.html' : 'html'
    }
  });
  console.log( 'done extracting, '+ targetFilename+ ' has been updated.' );
};




var mergePot2Po= function(){

  walker( targetDir+'/' )
    .on( 'file', function(file, stat){
      if ( ~file.indexOf('LC_MESSAGES') && ~file.indexOf('djangojs.po') && ! ~file.indexOf('.bak') ){

        // create a backup of this specific django.po
        bash( 'cp -f '+ file+ ' '+ (file+ '.bak'), function(){
          console.log( 'backup file: ', file+ '.bak', ' has been updated.' );
        });

        // use GNU's msgmerge to merge new tags from djangojs.pot into this specific django.po
        // check the following link for msgmerge arguments: https://www.gnu.org/software/gettext/manual/html_node/msgmerge-Invocation.html
        var args    = ' --add-location --force-po -U -F -m --previous --backup=off ',
            command = 'msgmerge '+ args+ file+ ' '+ target;

        bash( command+ args, function(err, stdout, stderr){
          if ( err )
            console.log( 'msgmerge error: ', err );
          console.log( 'merged into: ', file );
        });

      }
    });
};



gulp.task( 'jsx2po', ['jsx2js'], function(){

  extract2pot();

  // wait for 3d party async file writes and processing being completed..
  // ASYNC_DELAY setting might need some adjustment depending on the system and configuration
  setTimeout( function(){
    bash( 'rm -R ' + tempDir, function(err, stdout, stderr){
      console.log( tempDir+ ' directory has been removed.' );
      console.log( 'merging the new .pot template into all exitsting language .po files...' );
      mergePot2Po();
    });
  }, ASYNC_DELAY );

});

// NEED TO FIX: crashes on 'gulp jsx2po' invocation
// gulp.task( 'default', ['jsx2po'] );

gulp.start( 'jsx2po' );
