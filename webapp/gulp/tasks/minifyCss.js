'use strict';

var gulp = require('gulp');
var config = require('../config').production;
var minifyCSS = require('gulp-minify-css');
var size = require('gulp-filesize');

gulp.task('minifyCss', ['less'], function() {
  return gulp.src(config.dest + 'css/**/*.css')
    .pipe(minifyCSS({keepBreaks: true}))
    .pipe(gulp.dest(config.dest + 'css'))
    .pipe(size());
});
