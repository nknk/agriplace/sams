const gulp = require('gulp');
const gzip = require('gulp-gzip');
const config = require('../config.js');
const clean = require('gulp-clean');
const gulpSequence = require('gulp-sequence').use(gulp);

gulp.task('clean-gz', function () {
  return gulp.src([
    config.dest + '*.gz',
    config.dest + '**/*.gz',
    '!' + config.dest + 'CACHE/**',
    '!' + config.dest + 'lib/**',
  ], { read: false }
  ).pipe(clean({ force: true }));
});

// js compression handled by django compressor
gulp.task('compress-js', function () {
  return gulp.src([
    config.dest + '**/*.js',
    '!' + config.dest + '**/*.gz',
    '!' + config.dest + 'CACHE/**',
    '!' + config.dest + 'lib/**',
    '!' + config.dest + 'bundle.js',
  ]).pipe(gzip())
    .pipe(gulp.dest(config.dest));
});

// css compression handled by django compressor
gulp.task('compress-css', function () {
  return gulp.src([
    config.dest + '**/*.css',
    '!' + config.dest + '**/*.gz',
    '' + config.dest + '**/*.css.map',
    '!' + config.dest + 'CACHE/**',
    '!' + config.dest + 'lib/**',
  ]).pipe(gzip())
    .pipe(gulp.dest(config.dest));
});

gulp.task('compress-fonts', function () {
  return gulp.src([
    config.dest + 'fonts/**',
    '!' + config.dest + '**/*.gz',
    '!' + config.dest + 'fonts/**/*.woff',
    '!' + config.dest + 'fonts/**/*.woff2',
    '!' + config.dest + 'fonts/**/*.eot',
    '!' + config.dest + 'fonts/**/*.rst',
  ]).pipe(gzip())
    .pipe(gulp.dest(config.dest + 'fonts/'));
});

gulp.task('compress-readmore', function () {
  return gulp.src([
    config.dest + 'readmore.min.js',
  ]).pipe(gzip())
    .pipe(gulp.dest(config.dest));
});

gulp.task('compress-leaflet', function () {
  return gulp.src([
    config.dest + 'leaflet/leaflet.js',
    config.dest + 'leaflet/leaflet.css',
  ]).pipe(gzip())
    .pipe(gulp.dest(config.dest + 'leaflet/'));
});


gulp.task('compress',
  gulpSequence('clean-gz', ['compress-fonts', 'compress-readmore', 'compress-leaflet'])
);
