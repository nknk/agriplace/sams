'use strict';

var browserSync = require('browser-sync');
var gulp = require('gulp');
var handleErrors = require('../util/handleErrors');
var less = require('gulp-less');
var print = require('gulp-print');
var sourcemaps = require('gulp-sourcemaps');

var config = require('../config.js').less.main;
var contextOptions = ['agriplace', 'hzpc', 'farm_group'];
//
// LESS to CSS task
//
// Compiles given less files
// with sourcemaps
// with autoprefixer: adds browser-specific prefixes as needed
//
gulp.task('less', function () {
  contextOptions.forEach( function(context) {
    gulp.src(config[context].src)
      .pipe(sourcemaps.init())
      .pipe(less({
          imagePath: '/images',  // Used by the image-url helper
          //plugins: [autoprefix],
          sourceComments: 'map'
      }))
      .on('error', handleErrors)
      .pipe(sourcemaps.write('./maps'))  // write sourcemaps in separate dir
      .pipe(gulp.dest(config[context].dest))
      .pipe(print())
      .pipe(browserSync.reload({stream: true}));
  })
});
