//
// Vendor task
//
// Concatenate and minify vendor JavaScript and CSS files
//

var concat = require('gulp-concat');
var gulp = require('gulp');
var print = require('gulp-print');

var config = require('../config.js');


//
// Vendor CSS task
//
// Concatenates files and minifies the result for each configuration object in the list
//
gulp.task('vendor-css', function () {
  config.vendorCSS.forEach(
    function vendorCSS(configObj) {
      gulp.src(configObj.src)
        .pipe(concat(configObj.file))
        .pipe(gulp.dest(configObj.dest))
        .pipe(print());
    }
  );
});


//
// Vendor JavaScript task
//
// Concatenates files and minifies the result for each configuration object in the list
//
gulp.task('vendor-js', function () {
  config.vendorJavaScript.forEach(
    function vendorJavaScript(configObj) {
      gulp.src(configObj.src)
        .pipe(concat(configObj.file))
        .pipe(gulp.dest(configObj.dest))
        .pipe(print());
    }
  );
});


//
// Vendor JavaScript+CSS task
//
gulp.task('vendor', ['vendor-css', 'vendor-js']);
