const gulp = require('gulp');
const gulpSequence = require('gulp-sequence').use(gulp);

gulp.task('production', gulpSequence(['vendor', 'minifyCss', 'images', 'buildMinimized'], 'compress'));
