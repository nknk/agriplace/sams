
// vim: ts=2 sw=2 et :

"use strict"

var gulp = require('gulp');
var async = require('async');

// TODO: it should generate a .json file with all languages available in webapp/locale, while
// still being compatible with i18next-client
//

// this script runs a shell-command to convert the actual main locale/nl/LC_MESSAGES/django.po
// to nl.json so all server-side translations can be used/required in the frontend
//
// to use this script, i18next-conv needs to be installed globally with npm (version 0.1.8)
//

gulp.task('po2json', function () {

  function runShellCommand(cmd, args, callback) {
    var spawn = require('child_process').spawn,
      child = spawn(cmd, args),
      response = "";

    child.stdout.on('data', function (buffer) {
      response += buffer.toString();
    });

    child.stdout.on('end', function () {
      callback(response);
    });
  }

  var languages = ['nl', 'es'];

  async.eachSeries(languages, function(language, cb) {
    var sourcePath = __dirname + '/../../frontend/locale/' + language +'/LC_MESSAGES/djangojs.po',
      targetPath = __dirname + '/../../frontend/locale/' + language + '.json',
      command = 'i18next-conv',
      args = ['-l', language, '-s', sourcePath, '-t', targetPath];

    runShellCommand(command, args, function (stdout) {
      console.log(stdout);
      cb();
    });
  },
  function() {
    console.log('done');
  });  

});
