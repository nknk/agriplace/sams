'use strict';

var webpack = require('webpack');
var WebpackDevServer = require("webpack-dev-server");

var gulp = require('gulp');
var gutil = require('gulp-util');

// Build & start devserver
gulp.task("devServer", function(callback) {
  var webpackConfig = require('../webpack.development.config.js');

  new WebpackDevServer(webpack(webpackConfig), {
    publicPath: '/static/frontend/javascript',
    hot: true,
    stats: {
      colors: true,
      chunks: false
    },
    proxy: {
      '*': 'http://0.0.0.0:8000'
    }
  }).listen(3000, "0.0.0.0", function(err) {

    if (err) {
      throw new gutil.PluginError("webpack-dev-server", err);
    }
    gutil.log("[webpack-dev-server]", " started at http://0.0.0.0:3000");
  });
});

// Build for production
gulp.task('buildMinimized', function(callback) {
  var webpackConfig = require('../webpack.production.config.js');

  webpack(webpackConfig, function() {
    callback();
  });
});
