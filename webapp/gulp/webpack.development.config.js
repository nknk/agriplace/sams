var notifyWebpackPlugin = require('./notifyWebpackPlugin.js');
var webpack = require('webpack');

module.exports = {
    entry: [
      'webpack-dev-server/client?http://0.0.0.0:3000', // WebpackDevServer host and port
      'webpack/hot/dev-server', // 'only' prevents reload on syntax errors
      './frontend/javascript/react.js' // Your appʼs entry point
    ],
    output: {
        path: __dirname + '/frontend/static/frontend/javascript',
        filename: 'agriplace-bundle-react.js',
        publicPath: '/static/frontend/javascript'
    },

    devtool: 'eval',
    debug: true,
    module: {
        loaders: [
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.less$/, loader: 'style-loader!css-loader!less-loader'},
            { test: /\.png$/, loader: 'url-loader' },
            { test: /\.js.?$/, loader: 'react-hot!babel?presets[]=es2015,presets[]=react', exclude: [/node_modules/, /bower_components/, /public/] },
            { test: /\.json$/, loader: 'json-loader'}
        ]
    },
    plugins: [
        notifyWebpackPlugin,
        new webpack.HotModuleReplacementPlugin()
    ],
    node: {
      fs: 'empty',
      child_process: 'empty',
      module: 'empty',
      net: 'empty',
      tls: 'empty'
    }
};
