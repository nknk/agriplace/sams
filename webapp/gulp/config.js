//
// config.js
//
// Main configuration file for gulp build
//
// This file decides _what_ to do, and the tasks/ decide _how_ to do it.
// Tasks should be as reusable in other projects as possible, keep project details and paths in this file.
//

// Paths relative to gulpfile.js (webapp root)
var dest           = './frontend/static/frontend/',
    src            = './',
    vendorLibPath  = 'frontend/static/frontend/lib/';

var config = {

  dest  : dest,
  src   : src,

  //
  // Browserify task config
  //
  browserify: {
    bundleConfigs: [
      {
        src: src + 'frontend/javascript/react.js',
        dest: dest + 'javascript',
        outputName: 'agriplace-bundle-react.js',
      }
    ]
  },


  //
  // Production task config
  //
  production: {
    cssSrc: dest + '*.css',
    jsSrc: dest + '*.js',
    dest: dest
  },


  //
  // Less task config
  //
  less: {
    main: {
      //
      //  frontend/less contains the main styles for this project
      //  The two root files are:
      //    app.less: includes project styles
      //    bootstrap.less: includes bootstrap and the custom project variables
      //
      //  Build the two .css files, then minify them.
      //  Copy the resulting files into frontend/build/css/
      //

      // A separate .css file will be generated for each less file in the list below.
      agriplace: {
        src: [
        	"frontend/less/**/app.less",  // main project styles
        	"frontend/less/**/bootstrap.less"  // bootstrap + custom variables
        ],
        dest: dest + 'css/'
      },
      hzpc: {
      //
      //  frontend/less/hzpc contains hzpc styles for HZPC project
        src: [
          "frontend/less/hzpc/**/app.less",  // main project styles
          "frontend/less/hzpc/**/bootstrap.less"  // bootstrap + custom variables
        ],
        dest: dest + 'css/hzpc/'
      },
      farm_group: {
        //
        //  frontend/less/hzpc contains hzpc styles for HZPC project
        src: [
        	"frontend/less/farm_group/**/app.less",  // main project styles
        	"frontend/less/farm_group/**/bootstrap.less"  // bootstrap + custom variables
        ],
        dest: dest + 'css/farm_group/'
      },
    },
  },

  images: {
   src: src + "/img",
   dest: dest + "/img"
  },

  //
  // Vendor JavaScript task config
  // Concatenates files and minifies the result for each configuration object in the list
  //
  vendorJavaScript: [
    {
      file: 'vendor.js',
      dest: dest + 'javascript/',
      src: [
        // all paths relative to vendorLibPath
        'bower_components/jquery/dist/jquery.js',
        'bower_components/jquery-cookie/jquery.cookie.js',
        'bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.nl.js',
        'bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/bootstrap/js/affix.js'
      ].map(
        function (file) {
          return vendorLibPath + file;
        }
      )
    }
  ],

  //
  // Vendor CSS task config
  // Concatenates files and minifies the result for each configuration object in the list
  //
  vendorCSS: [
    {
      file: 'vendor.css',
      dest: dest + 'css/',
      src: [
        // All paths relative to vendorLibPath
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/bootstrap-datepicker/css/datepicker.css',
        'toastr.min.css'
      ].map(
        // add lib path to all file paths
        function (file) {
          return vendorLibPath + file;
        }
      )
    }
  ]

};



module.exports = config;

