var webpack = require('webpack');

module.exports = {
    entry: [
      './frontend/javascript/react.js' // Your appʼs entry point
    ],
    output: {
        path: './frontend/static/frontend/javascript',
        filename: 'agriplace-bundle-react.js'
    },

    module: {
        loaders: [
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.less$/, loader: 'style-loader!css-loader!less-loader'},
            { test: /\.png$/, loader: 'url-loader' },
            { test: /\.js.?$/, loader: 'babel?presets[]=es2015,presets[]=react', exclude: [/node_modules/, /bower_components/, /public/] },
            { test: /\.json$/, loader: 'json-loader'}
        ]
    },
    node: {
      fs: 'empty',
      child_process: 'empty',
      module: 'empty',
      net: 'empty',
      tls: 'empty'
    }
};
