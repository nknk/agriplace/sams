import React from 'react';
import { shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
import { AssessmentOverviewTable } from '../frontend/react/components/AssessmentOverview/AssessmentOverviewTable.jsx';
const SharedAssessmentMock = require('./mocks/SharedAssessmentMock.json');
const moment = require('moment');

describe('Testing Assessment overview table', () => {
  let sharedAssessmentDetail;
  let sharedAssessment;
  beforeEach(() => {
    jasmineEnzyme();

    const { detail: overview, assessment } = SharedAssessmentMock;
    sharedAssessment = assessment;
    sharedAssessmentDetail = {
      organizationName: overview.created_by_organization_name,
      dateShared: overview.date_shared ? moment(overview.date_shared).format('DD-MM-YYYY') : '-',
      createdTime: moment(overview.created_time).format('DD-MM-YYYY'),
      products: [],
      createdBy: overview.created_by,
      prefferedMonth: overview.preferred_month
        ? _.capitalize(tools.moment(overview.preferred_month, 'MM').locale('en').format('MMMM'))
        : '-',
      sharedTime: overview.shared_time ? moment(overview.shared_time).format('DD-MM-YYYY') : '-',
      auditDate: overview.audit_date ? moment(overview.audit_date).format('DD-MM-YYYY') : '-',
      documentReviewDate: overview.document_review_date ?
        moment(overview.document_review_date).format('DD-MM-YYYY') : '-',
      auditor: overview.auditor ? overview.auditor : '-',
    };
  });

  it('AssessmentOverviewTable is present', () => {
    const wrapper = shallow(
      <AssessmentOverviewTable
        options={sharedAssessmentDetail}
        assessmentOverviewStats={null}
        isProgressVisible
        assessmentUuid={sharedAssessment.uuid}
      />
    );
    expect(wrapper.find('.AssessmentOverview')).toBePresent();
  });

  it('Assessment overview table contains shared date text', () => {
    const sharedDate = sharedAssessmentDetail.dateShared;
    const sharedDateHTML = `<td id="shared-date-container"> ${sharedDate} </td>`;
    const wrapper = shallow(
      <AssessmentOverviewTable
        options={sharedAssessmentDetail}
        assessmentOverviewStats={null}
        isProgressVisible
        assessmentUuid={sharedAssessment.uuid}
      />
    );
    expect(wrapper.find('.date-shared')).toBePresent();
    expect(wrapper.find('#shared-date-container')).toHaveHTML(sharedDateHTML);
  });
});
