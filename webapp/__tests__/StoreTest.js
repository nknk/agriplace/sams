describe('Testing MyStore', function() {
  beforeEach(function() {
    const FluxxorTestUtils = require ('fluxxor-test-utils').extendJasmineMatchers(jasmine);
    // now our jasmine matchers are available

    const Store = require('../frontend/react/stores/ReviewStore.js');
    this.fakeFlux = FluxxorTestUtils.fakeFlux({ ReviewStore: new Store()});
    // now we have a FakeFlux instance that has .stores.MyStore

    this.myStore = this.fakeFlux.store('ReviewStore');
    // easier access to my store instance

    this.myStoreSpy = this.fakeFlux.makeStoreEmitSpy('ReviewStore');
    // now all our this.emit() calls from within the store are captured
  });
  it('when dispatcher dispatches event of "loadReview" with payload: "positive",' +
   'it should set .getState() and emit "change" event', function() {
    this.fakeFlux.dispatcher.dispatch({ type: 'loadReview', payload: 'positive' });
    expect(this.myStore.getState().review).toBe('positive');
    expect(this.myStoreSpy.getLastCall()).toEqual(['change']);
  });
});