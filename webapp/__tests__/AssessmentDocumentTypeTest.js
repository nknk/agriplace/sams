window._t = () => {};
window.pageData = {};
window.jsContext = {
  API_URL: '/internal-api/',
};
window.$ = require('jquery');
window.Fluxxor = require('fluxxor');
import jasmineEnzyme from 'jasmine-enzyme';
import { shallow } from 'enzyme';
import React from 'react';
import _ from 'lodash';
const AssessmentDocumentType = require('../frontend/react/components/AssessmentDocumentType/AssessmentDocumentType.jsx');
const AssessmentModel = require('../frontend/react/models/AssessmentModel.js');
// const AssessmentTypeModel = require('../frontend/react/models/AssessmentTypeModel.js');
import AssessmentTypeModel from '../frontend/react/models/AssessmentTypeModel';
const AssessmentMock = require('./mocks/AssessmentMock.json');
const AssessmentTypeMock = require('./mocks/AssessmentTypeMock.json');
const FluxxorTestUtils = require('fluxxor-test-utils');


describe('Testing AssessmentDocumentType', () => {

  let assessment;
  let fakeFlux;
  beforeEach( () => {
    jasmineEnzyme();

    assessment = new AssessmentModel({
      assessment: AssessmentMock.assessment,
      uuid: AssessmentMock.assessment.uuid,
      answers: AssessmentMock.answers,
      permissions: AssessmentMock.permissions,
      overview: AssessmentMock.detail,
      products: AssessmentMock.products,
      judgementMode: AssessmentMock.judgement_mode,
      document_type_reuse_attachments: AssessmentMock.document_type_reuse_attachments,
      isEvidenceReuseAnswered: AssessmentMock.isEvidenceReuseAnswered,
    });

    const assessmentType = new AssessmentTypeModel();
    assessmentType.load(AssessmentTypeMock);
    assessment.setAssessmentType(assessmentType);
    const AssessmentsStore = require('../frontend/react/stores/AssessmentsStore.js');
    fakeFlux = FluxxorTestUtils.fakeFlux({ AssessmentsStore: new AssessmentsStore });
  });

  it('initially renders without description', () => {

    const wrapper = shallow(
      <AssessmentDocumentType
        assessment={assessment}
        currentOrganization={{}}
        documentType={_.values(assessment.assessmentType.documentTypes)[0]}
        layout=""
        flux={fakeFlux}
      />
    );

    expect(wrapper.find('.description')).not.toBePresent();
  });

  it('makes description visible after btn click', () => {
    const wrapper = shallow(
      <AssessmentDocumentType
        assessment={assessment}
        currentOrganization={{}}
        documentType={_.values(assessment.assessmentType.documentTypes)[0]}
        layout=""
        flux={fakeFlux}
      />
    );

    const fakeEvent = { preventDefault: () => {} };

    wrapper.find('.expandDescriptionBtn').simulate('click', fakeEvent);

    expect(wrapper.find('.description')).toBePresent();
  });
});
