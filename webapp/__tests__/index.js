// require all modules ending in "_test" from the
// current directory and all subdirectories
// see https://github.com/webpack/karma-webpack
const testsContext = require.context(".", true, /.Test*$/);
testsContext.keys().forEach(testsContext);
