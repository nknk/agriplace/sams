import React from 'react';
import { PrintButtonController } from '../frontend/react/components/Navigation/PrintButtonController.jsx';
import AssessmentPrintHeader from '../frontend/react/components/Navigation/AssessmentPrintHeader.jsx';
import AssessmentReviewPrint from '../frontend/react/components/AssessmentReview/AssessmentReviewPrint.jsx';
import { render, mount, shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';

describe('Assessment review print test', () => {
  let props;
  beforeEach(() => {
    jasmineEnzyme();
    props = {
      location: 'ReviewApp', // location for inspection overview tab
    };
  });

  it('Print button is visible for assessment review tab', () => {
    const wrapper = render(<PrintButtonController {...props} />);
    expect(wrapper.find('.PrintButton')).toBePresent();
  });

  it('Assessment header title should be present', () => {
    const wrapper = mount(<AssessmentPrintHeader />);
    expect(wrapper.find('.page-print-title')).toBePresent();
  });

  it('Assessment header title for print should have visible-print class', () => {
    const wrapper = mount(<AssessmentPrintHeader />);
    expect(wrapper.find('.page-print-title')).toHaveClassName('visible-print');
  });

  it('Assessment review page for print is present', () => {
    const wrapper = shallow(<AssessmentReviewPrint />);
    expect(wrapper).toBePresent();
  });
});
