import React from 'react';
import { AssociatedQuestions } from '../frontend/react/components/AssessmentDocumentType/AssociatedQuestions.jsx';
import { shallow, mount } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';

const fakeAssociatedQuestions = require('./mocks/AssociatedQuestionsMock.json');
const fakeUrlParams = require('./mocks/UrlParamsMock.json');
global.tools = require('../frontend/lib/ap-tools');

describe('Testing auditor dashboard filters', () => {
  beforeEach(() => {
    jasmineEnzyme();
  });

  it('Associated Questions component is rendered', () => {
    const wrapper = shallow(
      <AssociatedQuestions
        params={fakeUrlParams}
        questions={fakeAssociatedQuestions}
      />
    );
    expect(wrapper.find('.associated-questions')).toBePresent();
  });

  it('Associated Questions component will contain a popup for multiple questions', () => {
    const wrapper = shallow(
      <AssociatedQuestions
        params={fakeUrlParams}
        questions={fakeAssociatedQuestions}
      />
    );
    expect(wrapper.find('.questions-popup')).toBePresent();
    expect(wrapper.find('Link')).toBePresent();
  });

  it('Associated Questions component will not contain a popup for single questions', () => {
    const singleQuestion = [fakeAssociatedQuestions[0]];
    const wrapper = shallow(
      <AssociatedQuestions
        params={fakeUrlParams}
        questions={singleQuestion}
      />
    );
    expect(wrapper.find('.questions-popup')).not.toBePresent();
    expect(wrapper.find('Link')).toBePresent();
  });

  it('Associated Questions popup will open when the button is clicked', () => {
    const wrapper = mount(
      <AssociatedQuestions
        params={fakeUrlParams}
        questions={fakeAssociatedQuestions}
      />
    );
    expect(wrapper).toHaveState('isOpen', false);
    wrapper.find('.popup-overlay-button').simulate('click');
    expect(wrapper).toHaveState('isOpen', true);
  });
});
