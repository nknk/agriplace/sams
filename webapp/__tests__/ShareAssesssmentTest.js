import React from 'react';
import { ShareAssessment } from '../frontend/react/components/ShareAssessment/ShareAssessment.jsx';
import { shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';

describe('Testing ShareAssessment', () => {
  const props = {
    params: {
      organizationSlug: 'foo',
      assessmentUuid: 'foo',
      assessmentAccessMode: 'foo',
    },
    onAssessmentClose: () => {},
    auditors: [],
    selectedAuditors: [],
  };

  beforeEach(() => {
    jasmineEnzyme();
  });

  it('initially renders without any modals', () => {
    const wrapper = shallow(
      <ShareAssessment {...props} />
    );

    expect(wrapper.state().progress).toEqual('initial');
  });

  // WILL BE FIXED LATER

  // it('goes to sharedWithoutAuditors', () => {
  //   const wrapper = shallow(
  //     <ShareAssessment {...props} />
  //   );

  //   spyOn(ShareAssessment.prototype.__reactAutoBindMap, 'isAssessmentDone').and.returnValue(true);

  //   wrapper.find('.pull-right').simulate('click');
  //   expect(wrapper.state().progress).toEqual('sharedWithoutAuditors');
  // });
});
