import React from 'react';
import { AuditorDashboardFilters } from '../frontend/react/components/AuditorDashboard/AuditorDashboardFilters.jsx';
import { DashboardPeriodSelect } from '../frontend/react/components/AuditorDashboard/DashboardPeriodSelect.jsx';
import { shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';

const moment = require('moment');
const stores = require('../frontend/react/stores');
const FluxxorTestUtils = require('fluxxor-test-utils');
const AssessmentTypesFilterMock = require('./mocks/AssessmentTypesFilterMock.json');
const AssessmentStatesFilterMock = require('./mocks/AssessmentStatesFilterMock.json');

describe('Testing auditor dashboard filters', () => {
  let fakeFlux;
  beforeEach(() => {
    jasmineEnzyme();
    const DashboardStore = stores.DashboardStore;
    fakeFlux = FluxxorTestUtils.fakeFlux({ DashboardStore });
    fakeFlux.stores.DashboardStore.assessmentTypes = AssessmentTypesFilterMock;
    // fakeFlux.stores.DashboardStore.assessmentStates = AssessmentStatesFilterMock;
  });

  it('Dashboard year and period exists inside the component', () => {
    const wrapper = shallow(<DashboardPeriodSelect />);
    expect(wrapper.find('.DashboardPeriodSelect')).toBePresent();
  });

  it('Year selector exists inside the component', () => {
    const wrapper = shallow(<DashboardPeriodSelect />);
    expect(wrapper.find('.year')).toBePresent();
  });

  it('Audit period selector exists inside the component', () => {
    const wrapper = shallow(<DashboardPeriodSelect />);
    expect(wrapper.find('DaterangePicker')).toBePresent();
  });

  it('Year selector has present year selected by default', () => {
    const wrapper = shallow(<DashboardPeriodSelect />);
    const year = +moment().format('YYYY');
    expect(wrapper.find('.year').at(0)).toHaveValue(year);
  });

  it('Assessment types filter is present', () => {
    const wrapper = shallow(
      <AuditorDashboardFilters flux={fakeFlux} />
    );
    expect(wrapper.find('.ItemFilter').at(0)).toBePresent();
  });

  it('No assessment types filter is selected by default', () => {
    const wrapper = shallow(
      <AuditorDashboardFilters flux={fakeFlux} />
    );
    expect(wrapper.find('.ItemFilter').at(0).find('.selected')).not.toBePresent();
  });

  it('Assessment types filter is selected when clicked', () => {
    const wrapper = shallow(
      <AuditorDashboardFilters flux={fakeFlux} />
    );
    wrapper.find('.ItemFilter').at(0).find('.deselected').at(0).simulate('click');
    expect(wrapper.find('.ItemFilter').at(0).find('.selected').at(0)).toBePresent();
  });

  it('No assessment states filter is selected by default', () => {
    const wrapper = shallow(
      <AuditorDashboardFilters flux={fakeFlux} />
    );
    expect(wrapper.find('.ItemFilter').at(1).find('.selected')).not.toBePresent();
  });

  it('Assessment states filter is selected when clicked', () => {
    const wrapper = shallow(
      <AuditorDashboardFilters flux={fakeFlux} />
    );
    wrapper.find('.ItemFilter').at(1).find('.deselected').at(0).simulate('click');
    expect(wrapper.find('.ItemFilter').at(1).find('.selected').at(0)).toBePresent();
  });
});
