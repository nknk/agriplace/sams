import React from 'react';
import { CertificateDeleteButton } from '../frontend/react/components/Certificates/CertificateDeleteButton.jsx';
import { shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';

describe('Testing Certificates', () => {
  beforeEach(() => {
    jasmineEnzyme();
  });

  it('button exists inside the component', () => {
    const wrapper = shallow(<CertificateDeleteButton />);
    expect(wrapper.find('Button')).toBePresent();
  });

  it('button performs the click event passed in props', () => {
    const onClick = jasmine.createSpy('changed');
    const wrapper = shallow(<CertificateDeleteButton onClick={onClick} />);
    wrapper.find('Button').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
