import React from 'react';
import { shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
const { NavigationTabs } = require('../frontend/react/components/Navigation/NavigationTabs.jsx');

describe('Testing NavigationTabs', () => {
  beforeEach(() => {
    jasmineEnzyme();
  });
  it('Shows NavigationTabs', () => {
    const props = {
      params: {
        organizationSlug: 'foo',
        assessmentUuid: 'foo',
        assessmentAccessMode: 'foo',
      },
      sections: [
        { code: 'overview', label: 'overview', url: 'overview' },
        { code: 'evidence', label: 'evidence', url: 'evidence' },
        { code: 'questionnaires', label: 'questionnaires', url: 'questionnaires' },
        { code: 'results', label: 'results', url: 'results' },
        { code: 'products', label: 'products', url: 'products' },
      ],
    };
    const wrapper = shallow(
      <NavigationTabs {...props} />,
      { context: { history: { isActive() {} } } }
    );
    expect(wrapper.find('.steps').children().length).toEqual(props.sections.length);
  });
});
