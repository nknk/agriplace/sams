const actions = require('../frontend/react/actions');
const FluxxorTestUtils = require('fluxxor-test-utils');
import jasmineEnzyme from 'jasmine-enzyme';

describe('Testing JudgementsActions', () => {
  let fakeFlux;
  let myActionsSpy;
  beforeEach(() => {
    jasmineEnzyme();
    fakeFlux = FluxxorTestUtils.fakeFlux({}, actions.judgements );
    myActionsSpy = fakeFlux.makeActionsDispatchSpy();
  });

  it('when changeJudgement() called with "foo value", ' +
    'it should dispatch "judgementChanged" and { value: "foo value" }',
    () => {
      fakeFlux.actions.changeJudgement('foo value');
      expect(myActionsSpy.getLastCall()).toEqual(['judgementChanged', { judgement: "foo value" }]);
    }
  );
});
