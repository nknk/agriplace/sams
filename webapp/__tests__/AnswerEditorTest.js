import _ from 'lodash';
import React from 'react';
import { shallow } from 'enzyme';
import jasmineEnzyme from 'jasmine-enzyme';
const AnswerEditor = require('../frontend/react/components/QuestionnaireEditor/AnswerEditor.jsx');

describe('Testing AnswerEditor', () => {
  let props;
  beforeEach(() => {
    jasmineEnzyme();
    props = {
      assessmentUuid: '4816950b2f014066b9b816b3e1ffcf70',
      justification: 'This is just',
      possibleAnswerSet: {},
      question: {},
      readOnly: false,
      isJustificationRequired: false,
      value: 'Yes',
      onChange: () => {},
    };
  });

  it('Has no warning if justification not required', () => {
    const wrapper = shallow(
      <AnswerEditor {...props} />
    );
    expect(wrapper.find('.justification-section').hasClass('warning')).toEqual(false);
  });


  it('Has warning if justification is required & its empty', () => {
    const _props = _.assign(_.clone(props), {
      isJustificationRequired: true,
      justification: '',
    });
    const wrapper = shallow(
      <AnswerEditor {..._props} />
    );
    expect(wrapper.find('.justification-section').hasClass('warning')).toEqual(true);
  });

  it('Placeholder attribute is present inside justification text area', () => {
    const props_ = _.assign(_.clone(props), {
      isJustificationRequired: true,
      justification: '',
    });
    const wrapper = shallow(
      <AnswerEditor {...props_} />
    );
    expect(wrapper.find('textarea')).toHaveProp('placeholder');
  });

  it('Justification box placeholder text is the one supplied in question', () => {
    const fakeText = 'fake placeholder text';
    const props_ = _.assign(_.clone(props), {
      isJustificationRequired: true,
      justification: '',
      question: {
        justification_placeholder: fakeText,
      },
    });
    const wrapper = shallow(
      <AnswerEditor {...props_} />
    );
    expect(wrapper.find('textarea')).toHaveProp('placeholder', fakeText);
  });
});
