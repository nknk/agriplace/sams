global._ = require('lodash');

import AssessmentTypeModel from '../frontend/react/models/AssessmentTypeModel.js';
var AssessmentTypeMock = require('./mocks/AssessmentTypeMock.json');

describe('Testing AssessmentTypeModel', function () {
  it('getElement', function () {
    var assessmentTypeModel = new AssessmentTypeModel();
    assessmentTypeModel.load(AssessmentTypeMock);
    var el = assessmentTypeModel.getElement('7gPHnmuqHavmc8xHAYYoZ9');
    expect(el).toBeTruthy();
  });

  it('getQuestionLevel', function () {
    var assessmentTypeModel = new AssessmentTypeModel();
    assessmentTypeModel.load(AssessmentTypeMock);
    var el = assessmentTypeModel.getQuestionLevel('2rDaxliQwxEaXKMFe2IWEz');
    expect(el).toBeTruthy();
  });

  it('load headings', function () {
    var assessmentTypeModel = new AssessmentTypeModel();
    assessmentTypeModel.load(AssessmentTypeMock);
    expect(assessmentTypeModel.headings).toBeTruthy();
  });

  it('load questions', function () {
    var assessmentTypeModel = new AssessmentTypeModel();
    assessmentTypeModel.load(AssessmentTypeMock);
    expect(assessmentTypeModel.questions).toBeTruthy();
  });

  it('load levels', function () {
    var assessmentTypeModel = new AssessmentTypeModel();
    assessmentTypeModel.load(AssessmentTypeMock);
    expect(assessmentTypeModel.levels).toBeTruthy();
  });

  it('load triggersByTargetElement', function () {
    var assessmentTypeModel = new AssessmentTypeModel();
    assessmentTypeModel.load(AssessmentTypeMock);
    expect(assessmentTypeModel.triggersByTargetElement).toBeTruthy();
  });
});