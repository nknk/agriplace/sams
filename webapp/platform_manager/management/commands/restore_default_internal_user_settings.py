from django.core.management import BaseCommand

from platform_manager.helpers import remove_superuser_status, reset_admin_app_user_groups, reset_users, \
    reset_users_organizations, assign_default_permissions_to_groups, assign_default_groups_to_users


class Command(BaseCommand):
    help = 'Reset to default user/group permissions(admin app)'

    def handle(self, *args, **options):
        # first nobody can be superuser anymore
        remove_superuser_status()
        # second update/create current roles
        reset_admin_app_user_groups()
        # third agriplace users
        reset_users()
        # and they organizations
        reset_users_organizations()
        # now time to assign permissions to groups
        assign_default_permissions_to_groups()
        # and attach them(groups) to users
        assign_default_groups_to_users()

