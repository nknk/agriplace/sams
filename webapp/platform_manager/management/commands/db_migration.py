import sys

from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db.models.loading import get_model
from tabulate import tabulate

from organization.models import Organization

DATA_BLOCKS = [
    {
        'name': 'user_organization',
        'model_list': [
            "auth.User", "auth.Group", "sams.UserProfile", "organization.OrganizationTheme",
            "organization.OrganizationType", "organization.Organization",  "organization.OrganizationMembership",
            "urllogin.Key"
        ]
    },
    {
        'name': 'product_product_partner',
        'model_list': ["organization.Product", "organization.ProductPartner"]
    },
    {
        'name': 'assessment',
        'model_list': [
            "app_settings.TextBlock", "app_settings.HelpTextBlock", "assessments.Assessment", "assessments.Answer"
        ]
    },
    {
        'name': 'workflow',
        'model_list': [
            "assessment_workflow.AssessmentWorkflow", "assessment_workflow.AssessmentWorkflowFile"
        ]
    },
    {
        'name': 'judgement_agreement',
        'model_list': [
            "assessments.AssessmentShare", "assessments.AssessmentSharePermission", "assessments.Judgement",
            "assessments.QuestionnaireState", "farm_group.AssessmentAgreement", "farm_group.AssessmentMutationLog"
        ]
    },
    {
        'name': 'attachment',
        'model_list': ["attachments.Attachment"]
    },
    {
        'name': 'sub_attachment',
        'model_list': [
            "attachments.FileAttachment", "attachments.TextReferenceAttachment", "attachments.NotApplicableAttachment",
            "attachments.AgriformAttachment", "attachments.FormAttachment", "attachments.DocumentType",
            "attachments.HelpDocument", "attachments.SharedAttachment", "attachments.SharedAttachmentList",
            "assessments.AssessmentDocumentLink"
        ]
    }
]

MODELS_NATURAL_KEYS = {
    'assessment document link': ['assessment', 'document_type_attachment'],
    'user': ['username'],
    'group': ['name'],
    'organization theme': ['slug'],
    'organization type': ['slug'],
    'text block': ['code'],
    'help text block': ['title', 'internal_code']
}

BUYERS_ORGINIZATIONS_SLUGS = ['hzpc', 'agrifirm', 'suikerunie']


class Command(BaseCommand):
    help = '''Migrate models from an old database to the current one.
    Expects a secondary database configured in Django's settings.
    '''

    src_db = 'source'
    dest_db = 'target'

    models_to_migrate = []

    def add_arguments(self, parser):
        parser.add_argument('-s', '--summary', default=False,
                            action='store_true', help='Display summary information only')
        parser.add_argument('-r', '--rename', default=False,
                            action='store_true', help='Fix for buyers organizations')

    def handle(self, *args, **options):

        # prerequisite
        call_command("migrate", database=self.src_db)
        call_command("migrate", database=self.dest_db)

        self.prepare_models_to_migrate()
        self.summary()
        # if summary only was requested, exit
        if options['summary']:
            return
        else:
            self.update_duplicates()  # to solve problem with same organizations
            for block in DATA_BLOCKS:
                filename = u'{}.json'.format(block['name'])
                print(u"{} start".format(filename))
                # export
                sysout = sys.stdout
                sys.stdout = open(filename, 'w')
                call_command("dumpdata",  "--indent", "2", "--database", "source", "--natural-foreign",
                             "--natural-primary", *block['model_list'])
                sys.stdout = sysout
                # import
                call_command("loaddata", "--database", "target", filename)
                print(u"{} finish".format(filename))

            if options['rename']:
                self.fix_buyers()
        return

    def summary(self):
        """Output totals of models by db and the differences"""
        self.stdout.write(self.style.MIGRATE_HEADING('Model totals by database'))
        data = [[self.style.MIGRATE_LABEL(heading) for heading in ['model', self.dest_db, self.src_db, 'difference']]]
        for model in self.models_to_migrate:
            data.append(self.model_db_counts(model))
        self.stdout.write(tabulate(data, headers='firstrow'))

    def model_db_counts(self, model):
        """Model name, count in each db, and difference"""
        from_list = model.objects.using(self.src_db)
        to_list = model.objects.using(self.dest_db)

        model_properties = [f.name for f in model._meta.get_fields()]
        model_methods = dir(model)
        model_keys = MODELS_NATURAL_KEYS.get(model._meta.verbose_name, []) if "natural_key" in model_methods else [
            'uuid'] if 'uuid' in model_properties else ['id']

        form_list_pk = set(from_list.values_list(*model_keys))
        to_list_pk = set(to_list.values_list(*model_keys))
        diff = len(form_list_pk.difference(to_list_pk))

        return [
            model._meta.verbose_name,
            intcomma(to_list.count(), use_l10n=False),
            intcomma(from_list.count(), use_l10n=False),
            intcomma(diff, use_l10n=False)
        ]

    def prepare_models_to_migrate(self):
        for block in DATA_BLOCKS:
            for model_name in block['model_list']:
                self.models_to_migrate.append(get_model(*model_name.split(".")))

    def update_duplicates(self):
        """ Some Organization has duplicates on platforms. This function temporary
        resolving this situation, by adding postfix "-HZPC" to the name and "-hzpc"
        to slug fields.
        """
        self.stdout.write(self.style.MIGRATE_HEADING('Fixed organizations'))

        src_org_list = Organization.objects.using(self.src_db).all()

        data = [[self.style.MIGRATE_LABEL(heading) for heading in [
            'organization', 'slug',
            'uuid({})'.format(self.dest_db), 'uuid({})'.format(self.src_db),
            'assessments({})'.format(self.dest_db), 'assessments({})'.format(self.src_db),
            'products({})'.format(self.dest_db), 'products({})'.format(self.src_db)
        ]]]

        for src_org in src_org_list:
            try:
                dest_org = Organization.objects.using(self.dest_db).get(slug=src_org.slug)
                data.append([dest_org.name, dest_org.slug, dest_org.pk, src_org.pk,
                             dest_org.assessments.count(), src_org.assessments.count(),
                             dest_org.productpartner_set.count(), src_org.productpartner_set.count()
                             ])
                if not src_org.name[:-4] == '-HZPC':
                    src_org.name += '-HZPC'
                    src_org.slug += '-hzpc'
                    src_org.save(using=self.src_db)
            except ObjectDoesNotExist:
                continue

        self.stdout.write(tabulate(data, headers='firstrow'))

    def fix_buyers(self):
        """
        We have 3 organization which are plying the role of buyers and they are exists on both platforms.
        This function updating references after migration and renaming this organizations.
        """
        for item in BUYERS_ORGINIZATIONS_SLUGS:
            org_to_rename = Organization.objects.using(self.dest_db).get(slug=item)
            target_org = Organization.objects.using(self.dest_db).get(slug=item+'-hzpc')
            crop_list = org_to_rename.productpartner_set.all()

            # updating references
            for crop in crop_list:
                crop.organization_uuid = target_org
                crop.save(using=self.dest_db)

            # renaming unused organization
            org_to_rename.slug += '-old'
            org_to_rename.name += '-OLD'
            org_to_rename.groups = []
            org_to_rename.save()

            target_org.slug = org_to_rename.slug[:-4]
            target_org.name = org_to_rename.name[:-4]
            target_org.save()
