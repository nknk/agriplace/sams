AGRIPLACE_ORGANIZATION = {
    "name": "Agriplace",
    "slug": "agriplace",
    "identification_number": "12345678",
    "type": "coop",
    "mailing_address1": "Oostenburgermiddenstraat 206",
    "mailing_city": "Amsterdam",
    "mailing_province": "Noord-Holland",
    "mailing_postal_code": "1018 LL",
    "mailing_country": "NL",
    "email": "info@agriplace.com",
    "url": "http://www.agriplace.com",
    "phone": "+31 (0) 854 897 333"
}

ACCOUNT_MANAGER_GROUP_NAME = "account_manager"
CONTENT_MANAGER_GROUP_NAME = "content_creator"
CONTENT_SUPERVISOR_GROUP_NAME = "content_owner"
FARM_GROUP_MANAGER_GROUP_NAME = "farm_group_manager"
QA_ENGINEER_GROUP_NAME = "qa_engineer"
SUPPORT_ENGINEER_GROUP_NAME = "support_engineer"
