from platform_manager.constants import ACCOUNT_MANAGER_GROUP_NAME, CONTENT_MANAGER_GROUP_NAME, \
    CONTENT_SUPERVISOR_GROUP_NAME, FARM_GROUP_MANAGER_GROUP_NAME, QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME

ADMINAPP_USER_GROUPS = [
    ACCOUNT_MANAGER_GROUP_NAME,
    CONTENT_MANAGER_GROUP_NAME,
    CONTENT_SUPERVISOR_GROUP_NAME,
    FARM_GROUP_MANAGER_GROUP_NAME,
    QA_ENGINEER_GROUP_NAME,
    SUPPORT_ENGINEER_GROUP_NAME
]

ADMINAPP_SUPER_USERS = ['admin']

AGRIPLACE_USER_LIST = [
    {
        "username": "abhinit.ravi@agriplace.com",
        "first_name": "Abhinit",
        "last_name": "Ravi",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "afeef.janjua@arpatech.com",
        "first_name": "Afeef",
        "last_name": "Janjua",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "andrey.kotsubailo@agriplace.com",
        "first_name": "Andrei",
        "last_name": "Kotsubailo",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "diana.kos@agriplace.com",
        "first_name": "Diana",
        "last_name": "Kos",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "gertjan.lieffering@agriplace.com",
        "first_name": "Gert-Jan",
        "last_name": "Lieffering",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "jelle.deruiter@agriplace.com",
        "first_name": "Jelle",
        "last_name": "de Ruiter",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "laury.buijs@agriplace.com",
        "first_name": "Laury",
        "last_name": "Buijs",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "maarten.denuijl@agriplace.com",
        "first_name": "Maarten",
        "last_name": "den Uijl",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "marieke.drdw@agriplace.com",
        "first_name": " Marieke",
        "last_name": "de Ruyter de Wildt",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "martijn.vanes@agriplace.com",
        "first_name": "Martijn",
        "last_name": "van Es",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "nico.broersen@people4earth.org",
        "first_name": "Nico",
        "last_name": "Broersen",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "oles.maiboroda@agriplace.com",
        "first_name": "Oles",
        "last_name": "Maiboroda",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "waqas.khalid@arpatech.com",
        "first_name": "Waqas",
        "last_name": "Khalid",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "wiard.gorter@agriplace.com",
        "first_name": "Wiard",
        "last_name": "Gorter",
        "is_staff": True,
        "is_active": True
    },
    {
        "username": "aamir.khan@arpatech.com",
        "first_name": "Aamir",
        "last_name": "Khan",
        "is_staff": True,
        "is_active": True
    }
]


AGRIPLACE_USER_GROUPS = {
    "abhinit.ravi@agriplace.com": [CONTENT_SUPERVISOR_GROUP_NAME, QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
    "afeef.janjua@arpatech.com": [QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
    "andrey.kotsubailo@agriplace.com": [QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
    "diana.kos@agriplace.com": [CONTENT_MANAGER_GROUP_NAME],
    "gertjan.lieffering@agriplace.com": [CONTENT_MANAGER_GROUP_NAME],
    "jelle.deruiter@agriplace.com": [CONTENT_MANAGER_GROUP_NAME],
    "laury.buijs@agriplace.com": [CONTENT_MANAGER_GROUP_NAME],
    "maarten.denuijl@agriplace.com": [CONTENT_MANAGER_GROUP_NAME],
    "marieke.drdw@agriplace.com": [CONTENT_MANAGER_GROUP_NAME],
    "martijn.vanes@agriplace.com": [CONTENT_SUPERVISOR_GROUP_NAME],
    "nico.broersen@people4earth.org": [CONTENT_MANAGER_GROUP_NAME],
    "oles.maiboroda@agriplace.com": [QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
    "waqas.khalid@arpatech.com": [QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
    "wiard.gorter@agriplace.com": [CONTENT_SUPERVISOR_GROUP_NAME, QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
    "aamir.khan@arpatech.com": [QA_ENGINEER_GROUP_NAME, SUPPORT_ENGINEER_GROUP_NAME],
}


AGRIPLACE_GROUPS_PERMISSIONS = {
    ACCOUNT_MANAGER_GROUP_NAME: [
        'add_user', 'view_user', 'change_user',
        'view_token',
        'add_organizationpublicprofile', 'view_organizationpublicprofile', 'change_organizationpublicprofile',
        'view_profilephoto',
        'add_organizationreport', 'view_organizationreport', 'change_organizationreport',
        'add_organization', 'view_organization', 'change_organization',
        'view_registrationprofile',
        'view_userprofile', 'change_userprofile',
        'add_tag', 'view_tag', 'change_tag',
        'view_key'
    ],
    CONTENT_MANAGER_GROUP_NAME: [
        'add_textblock', 'view_textblock', 'change_textblock',
        'add_helptextblock', 'view_helptextblock', 'change_helptextblock',
        'add_assessmenttypesection', 'view_assessmenttypesection', 'change_assessmenttypesection',
        'add_assessmenttype', 'view_assessmenttype', 'change_assessmenttype',
        'add_possibleanswerset', 'view_possibleanswerset', 'change_possibleanswerset',
        'add_possibleanswer', 'view_possibleanswer', 'change_possibleanswer',
        'add_questionlevel', 'view_questionlevel', 'change_questionlevel',
        'add_questiontriggercondition', 'view_questiontriggercondition', 'change_questiontriggercondition',
        'add_questiontrigger', 'view_questiontrigger', 'change_questiontrigger',
        'add_questionnaireheading', 'view_questionnaireheading', 'change_questionnaireheading',
        'add_questionnaireparagraph', 'view_questionnaireparagraph', 'change_questionnaireparagraph',
        'add_questionnaire', 'view_questionnaire', 'change_questionnaire',
        'add_question', 'view_question', 'change_question',
        'add_documenttype', 'view_documenttype', 'change_documenttype',
        'add_helpdocument', 'view_helpdocument', 'change_helpdocument',
        'add_contentpush', 'view_contentpush',
        'add_certificationtype', 'view_certificationtype', 'change_certificationtype',
        'add_producttype', 'view_producttype', 'change_producttype',
        'add_standardproducttype', 'view_standardproducttype', 'change_standardproducttype',
        'add_languagetranslation', 'view_languagetranslation', 'change_languagetranslation'
    ],
    CONTENT_SUPERVISOR_GROUP_NAME: [
        'add_textblock', 'view_textblock', 'change_textblock', 'delete_textblock',
        'add_helptextblock', 'view_helptextblock', 'change_helptextblock', 'delete_helptextblock',
        'add_assessmenttypesection', 'view_assessmenttypesection', 'change_assessmenttypesection', 'delete_assessmenttypesection',
        'add_assessmenttype', 'view_assessmenttype', 'change_assessmenttype', 'delete_assessmenttype',
        'add_possibleanswerset', 'view_possibleanswerset', 'change_possibleanswerset', 'delete_possibleanswerset',
        'add_possibleanswer', 'view_possibleanswer', 'change_possibleanswer', 'delete_possibleanswer',
        'add_questionlevel', 'view_questionlevel', 'change_questionlevel', 'delete_questionlevel',
        'add_questiontriggercondition', 'view_questiontriggercondition', 'change_questiontriggercondition', 'delete_questiontriggercondition',
        'add_questiontrigger', 'view_questiontrigger', 'change_questiontrigger', 'delete_questiontrigger',
        'delete_questionnaireelement',
        'add_questionnaireheading', 'view_questionnaireheading', 'change_questionnaireheading', 'delete_questionnaireheading',
        'add_questionnaireparagraph', 'view_questionnaireparagraph', 'change_questionnaireparagraph', 'delete_questionnaireparagraph',
        'add_questionnaire', 'view_questionnaire', 'change_questionnaire', 'delete_questionnaire',
        'add_question', 'view_question', 'change_question', 'delete_question',
        'add_documenttype', 'view_documenttype', 'change_documenttype', 'delete_documenttype',
        'add_helpdocument', 'view_helpdocument', 'change_helpdocument', 'delete_helpdocument',
        'add_contentpush', 'view_contentpush', 'change_contentpush', 'delete_contentpush',
        'add_certificationtype', 'view_certificationtype', 'change_certificationtype', 'delete_certificationtype',
        'add_producttype', 'view_producttype', 'change_producttype', 'delete_producttype',
        'add_standardproducttype', 'view_standardproducttype', 'change_standardproducttype', 'delete_standardproducttype',
        'add_languagetranslation', 'view_languagetranslation', 'change_languagetranslation', 'delete_languagetranslation',
    ],
    FARM_GROUP_MANAGER_GROUP_NAME: [
        'add_assessment', 'view_assessment', 'change_assessment', 'delete_assessment',
        'add_judgement', 'view_judgement', 'change_judgement', 'delete_judgement',
        'add_agriformattachment', 'view_agriformattachment', 'change_agriformattachment', 'delete_agriformattachment',
        'add_fileattachment', 'view_fileattachment', 'change_fileattachment', 'delete_fileattachment',
        'add_formattachment', 'view_formattachment', 'change_formattachment', 'delete_formattachment',
        'add_notapplicableattachment', 'view_notapplicableattachment', 'change_notapplicableattachment', 'delete_notapplicableattachment',
        'add_textreferenceattachment', 'view_textreferenceattachment', 'change_textreferenceattachment', 'delete_textreferenceattachment',
        'add_user', 'view_user', 'change_user',
        'add_feedbackmessage', 'view_feedbackmessage', 'change_feedbackmessage',
        'add_genericformdata', 'view_genericformdata', 'change_genericformdata',
        'add_hzpcassessmentagreement', 'view_hzpcassessmentagreement', 'change_hzpcassessmentagreement',
        'add_hzpcuserimport', 'view_hzpcuserimport', 'change_hzpcuserimport',
        'view_notificationjournal',
        'add_notificationsettings', 'view_notificationsettings', 'change_notificationsettings',
        'add_hzpcworkflowcontextfiles', 'view_hzpcworkflowcontextfiles', 'change_hzpcworkflowcontextfiles',
        'add_hzpcworkflowcontext', 'view_hzpcworkflowcontext', 'change_hzpcworkflowcontext',
        'view_organizationreport',
        'add_organization', 'view_organization', 'change_organization',
        'add_product', 'view_product', 'change_product',
        'add_key', 'view_key',
        'delete_assessmentdocumentlink',
        'delete_questionnairestate',
        'delete_attachment',
        'delete_assessmentdocumentlink'
    ],
    QA_ENGINEER_GROUP_NAME: [
        'view_helptextblock',
        'view_textblock',
        'view_assessmentdocumentlink',
        'view_assessmentshare',
        'view_assessmenttypesection',
        'view_assessmenttype',
        'view_assessment',
        'view_judgement',
        'view_possibleanswerset',
        'view_possibleanswer',
        'view_questionlevel',
        'view_questiontriggercondition',
        'view_questiontrigger',
        'view_questionnaireheading',
        'view_questionnaireparagraph',
        'view_questionnairestate',
        'view_questionnaire',
        'view_question',
        'view_agriformattachment',
        'view_documenttype',
        'view_fileattachment',
        'view_formattachment',
        'view_helpdocument',
        'view_notapplicableattachment',
        'view_textreferenceattachment',
        'view_genericformdata',
        'view_hzpcassessmentagreement',
        'view_hzpcuserimport',
        'view_notificationjournal',
        'view_notificationsettings',
        'view_hzpcworkflowcontextfiles',
        'view_hzpcworkflowcontext',
        'view_organizationpublicprofile',
        'view_profilephoto',
        'view_certificationtype',
        'view_certification',
        'view_organizationreport',
        'view_organization',
        'view_plotrvo',
        'view_producttype',
        'view_product',
        'view_standardproducttype',
        'view_userprofile',
        'view_languagetranslation',
        'view_key'
    ],
    SUPPORT_ENGINEER_GROUP_NAME:  [
        'view_assessmentdocumentlink',
        'view_assessmentshare',
        'view_assessment',
        'view_judgement',
        'view_agriformattachment',
        'view_fileattachment',
        'view_formattachment',
        'view_notapplicableattachment',
        'view_textreferenceattachment',
        'view_user',
        'view_token',
        'view_feedbackmessage',
        'view_genericformdata',
        'view_notificationjournal',
        'view_organizationpublicprofile',
        'view_profilephoto',
        'view_certification',
        'view_organization',
        'view_product',
        'view_userprofile',
        'view_key',
        'view_registrationprofile'
    ]
}
