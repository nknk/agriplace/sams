import json
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from core.models import UuidModel


@python_2_unicode_compatible
class GroupsPermissionsBackup(UuidModel):
    data = models.TextField()

    class Meta:
        db_table = 'GroupsPermissionsBackup'
        permissions = (
            ('view_grouppermissionsbackup', 'Can view group permissions backup'),
        )

    def __str__(self):
        return "Backup from {}".format(self.created_time)

    @classmethod
    def backup(cls):
        state = dict()
        for group in Group.objects.all():
            permissions = [{'name': p.name, 'codename': p.codename, 'content_type_id': p.content_type.id,
                            'content_type_app': p.content_type.app_label, 'content_type_model': p.content_type.model}
                           for p in group.permissions.all()]
            state[group.name] = permissions
        cls.objects.create(data=json.dumps(state))

    def restore(self):
        state = json.loads(self.data)
        number_of_created = {
            'groups': 0,
            'permissions': 0,
            'links': 0
        }
        errors = list()

        for group_name, permissions in state.iteritems():
            # groups
            group, created = Group.objects.get_or_create(name=group_name)
            if created:
                number_of_created['groups'] += 1
            else:
                group.permissions.clear()
            # permissions
            for p in permissions:
                # check permission
                # but first content type - if it exists and have same id
                try:
                    content_type = ContentType.objects.get(
                        id=p['content_type_id'],
                        app_label=p['content_type_app'],
                        model=p['content_type_model']
                    )
                except ObjectDoesNotExist:
                    errors.append(
                        u"Permission {} can't be added to Group {}, because Content Type {} not exist or changed".format(
                            p.codename,
                            group_name,
                            "{} {}".format(p.content_type.app_label, p.content_type.model)
                        ))
                else:
                    # time for permissions
                    permission, created = Permission.objects.get_or_create(
                        name=p['name'],
                        codename=p['codename'],
                        content_type=content_type
                    )
                    if created:
                        number_of_created['permissions'] += 1

                    # adding permission to group
                    group.permissions.add(permission)
                    number_of_created['links'] += 1
        return {
            'groups': number_of_created['groups'],
            'permissions': number_of_created['permissions'],
            'links': number_of_created['links'],
            'errors': errors
        }
