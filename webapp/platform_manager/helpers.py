from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import slugify

from organization.models import Organization, OrganizationMembership, OrganizationType
from platform_manager.constants import AGRIPLACE_ORGANIZATION
from .defaults import ADMINAPP_USER_GROUPS, ADMINAPP_SUPER_USERS, AGRIPLACE_USER_LIST, AGRIPLACE_GROUPS_PERMISSIONS, \
    AGRIPLACE_USER_GROUPS


# used to remove superuser status for all users which are not in ADMINAPP_SUPER_USERS(see ./defaults.py for details)
# returns list of usernames, which was updated by it
def remove_superuser_status(exclude_list=ADMINAPP_SUPER_USERS):
    list_of_updated_users = []
    user_list = get_user_model().objects.exclude(username__in=exclude_list)
    for user in user_list:
        if user.is_superuser:
            user.is_superuser = False
            user.save()
            list_of_updated_users.append(user.username)
    return list_of_updated_users


# used to create default user groups(see ./defaults.py for details)
def reset_admin_app_user_groups():
    for group_name in ADMINAPP_USER_GROUPS:
        _, _ = Group.objects.get_or_create(
            name=group_name,
        )


# used to reset users to initial state described in user_list (see ./defaults.py for details)
def reset_users(user_list=AGRIPLACE_USER_LIST):
    for user_desc in user_list:
        # TODO: add user_desc validation
        user, created = get_user_model().objects.get_or_create(
            username=user_desc.get("username", "")
        )
        user.email = user_desc.get("username", "")
        user.first_name = user_desc.get("first_name", "")
        user.last_name = user_desc.get("last_name", "")
        user.groups = []
        user.is_staff = user_desc.get("is_staff", False)
        user.is_active = user_desc.get("is_active", True)
        user.is_superuser = False
        # if user already exist then don't touch password, if no set new random one
        if created:
            user.set_password(get_user_model().objects.make_random_password())
        user.save()


# used to get_or_create organization from organization_description
# returns Organization instance
def get_or_create_organization(organization_description):
    organization, _ = Organization.objects.get_or_create(
        name=organization_description["name"],
        slug=organization_description["slug"]
    )
    organization.identification_number = organization_description.get("identification_number", "")
    organization.type = organization_description.get("type", "")
    organization.mailing_address1 = organization_description.get("mailing_address1", "")
    organization.mailing_city = organization_description.get("mailing_city", "")
    organization.mailing_province = organization_description.get("mailing_province", "")
    organization.mailing_postal_code = organization_description.get("mailing_postal_code", "")
    organization.mailing_country = organization_description.get("mailing_country", "")
    organization.email = organization_description.get("email", "")
    organization.url = organization_description.get("url", "")
    organization.phone = organization_description.get("phone", "")
    organization.save()
    return organization


# used to create default organization for all users from user_list and to attach this organization to parent
# provided in primary_organization_agriplace parameter
# NB not really generic function - use only for internal users
def reset_users_organizations(user_list=AGRIPLACE_USER_LIST, primary_organization_agriplace=AGRIPLACE_ORGANIZATION):
    for user_desc in user_list:
        try:
            user = get_user_model().objects.get(username=user_desc['username'])
            # removing existing links(just links, not organizations)
            user.organizations = []
            user.save()
            # creating or updating default organization for user
            user_organization_name = u"{}'s Farm".format(user.get_full_name())
            user_organization_description = {
                "name": user_organization_name,
                "slug": slugify(user_organization_name),
                "type": "producer",
                "email": user.email
            }
            user_organization = get_or_create_organization(user_organization_description)
            # attaching to parent organization
            primary_organization = get_or_create_organization(primary_organization_agriplace)

            organization_type, _ = OrganizationType.objects.get_or_create(
                name='producer',
                slug=slugify('producer')
            )

            OrganizationMembership.objects.get_or_create(
                primary_organization=primary_organization,
                secondary_organization=user_organization,
                organization_type=organization_type
            )

            # adding organization to user
            user.organizations.add(user_organization)
            user.save()

        except ObjectDoesNotExist:
            print("User with username: {} does not exist".format(user_desc['username']))
            continue


def assign_default_permissions_to_groups(permissions=AGRIPLACE_GROUPS_PERMISSIONS):
    for group_name, permission_list in permissions.iteritems():
        group, created = Group.objects.get_or_create(name=group_name)
        if created:
            print("Group with name: {} created".format(group_name))
        for permission_codename in permission_list:
            try:
                permission = Permission.objects.get(codename=permission_codename)
                group.permissions.add(permission)
            except ObjectDoesNotExist:
                print("  Permission with codename: {} does not exist".format(permission_codename))
                continue
            except Permission.MultipleObjectsReturned:
                print("  Permission with codename: {} exist and it not alone. Item with greatest id taken.".format(permission_codename))
                permission = Permission.objects.filter(codename=permission_codename).order_by('id').last()
                group.permissions.add(permission)


def assign_default_groups_to_users(user_list=AGRIPLACE_USER_GROUPS):
    for user_name, group_name_list in user_list.iteritems():
        try:
            user = get_user_model().objects.get(username=user_name)
            for group_name in group_name_list:
                group, created = Group.objects.get_or_create(name=group_name)
                if created:
                    print("Group with name: {} created".format(group_name))
                user.groups.add(group)
            user.save()
        except ObjectDoesNotExist:
            print("User with username: {} does not exist".format(user_name))
            continue

