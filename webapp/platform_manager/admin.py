from django.conf.urls import url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from core.admin import BaseAdmin
from platform_manager.models import GroupsPermissionsBackup


@admin.register(GroupsPermissionsBackup)
class GroupsPermissionsBackupAdmin(BaseAdmin):
    model = GroupsPermissionsBackup
    date_hierarchy = 'created_time'
    actions = ['restore']

    def get_urls(self):
        urls = super(GroupsPermissionsBackupAdmin, self).get_urls()
        local_urls = [
            url(r'^add_backup/$', self.admin_site.admin_view(self.backup), name='groups_permissions_backup_create'),
        ]
        return local_urls + urls

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        self.change_list_template = 'platform_manager/groups_permissions_backup_change_list.html'
        return super(GroupsPermissionsBackupAdmin, self).changelist_view(request, extra_context=extra_context)

    def backup(self, request):
        GroupsPermissionsBackup.backup()
        self.message_user(request, 'Backup created')
        return HttpResponseRedirect(reverse('admin:platform_manager_groupspermissionsbackup_changelist'))

    def restore(self, request, queryset):
        if queryset.count() > 1:
            self.message_user(request, 'Please, select backup(only one)', level="error")
        else:
            results = queryset.first().restore()
            self.message_user(
                request,
                u"Groups added: {}; permisions added: {}; links added: {}; errors: {}".format(
                    results['groups'],
                    results['permissions'],
                    results['links'],
                    results['errors']
                )
            )
