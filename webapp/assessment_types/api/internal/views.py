from django.conf import settings
from django.db.models import Count
from django.utils import translation
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rest_framework.views import APIView
from assessment_workflow.models import AssessmentWorkflow
from assessments.api.internal.serializers import (
    AssessmentTypeFullSerializer, AssessmentTypeSerializer, AssessmentTypeWorkflowCountSerializer,
    AssessmentTypeFullSerializer_V2
)
from assessments.models import AssessmentType
from core.constants import INTERNAL_INSPECTOR, INTERNAL_AUDITOR, GROWER
from organization.models import OrganizationMembership
from sams.view_mixins import MembershipRequiredPermission


class BaseAssessmentTypeView(APIView):
    """
    Base APIView for AssessmentType views

    """
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Optionally restricts the returned Assessment Types to a given kind,
        by filtering against a `kind` query parameter in the URL. Choises
        for kind: assessment, agriform, agriform2.

        """
        queryset = AssessmentType.objects.all()
        assessment_type_kind = self.request.query_params.get('kind', None)
        exclude_non_public = self.request.query_params.get('exclude_non_public', 0)
        try:
            exclude_non_public = int(exclude_non_public)
        except ValueError:
            exclude_non_public = 0
        if assessment_type_kind is not None:
            queryset = queryset.filter(kind=assessment_type_kind)
        if exclude_non_public:
            queryset = queryset.exclude(is_public=False)
        return queryset


class AssessmentTypeListView(BaseAssessmentTypeView, ListAPIView):
    """
    List of all Assessment Types with connected
    instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections

    :param kind - optional, used to filter by assessment_type__kind

    """
    serializer_class = AssessmentTypeSerializer


class AssessmentTypeWorkflowCountView(ListAPIView):
    """
    List of all Assessment Types with workflow counts with connected
    instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections
    """
    
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)
    serializer_class = AssessmentTypeWorkflowCountSerializer

    def get_queryset(self):
        membership = self.request.membership
        organization_role = membership.role
        is_auditor = INTERNAL_INSPECTOR == organization_role.name
        is_coordinator = INTERNAL_AUDITOR == organization_role.name

        queryset = AssessmentType.objects.filter(kind='assessment').distinct()
        harvest_year = self.request.query_params.get('year')
        exclude_non_public = self.request.query_params.get('exclude_non_public', 0)
        try:
            exclude_non_public = int(exclude_non_public)
        except ValueError:
            exclude_non_public = 0
        if exclude_non_public:
            queryset = queryset.exclude(is_public=False)
        if is_auditor:
            queryset = self.auditor_workflows(queryset, membership, harvest_year)
        if is_coordinator:
            queryset = self.coordinator_workflows(queryset, membership, harvest_year)

        return queryset.annotate(
            wrokflow_count=Count('assessmentworkflow')
        )

    def auditor_workflows(self, queryset, membership, harvest_year):
        if harvest_year and harvest_year != "All":
            queryset = queryset.filter(
                assessmentworkflow__auditor_organization=membership.secondary_organization,
                assessmentworkflow__harvest_year=harvest_year,
            )
        else:
            queryset = queryset.filter(
                assessmentworkflow__auditor_organization=membership.secondary_organization
            )

        return queryset.exclude(
            assessmentworkflow__workflow_status='error'
        )

    def coordinator_workflows(self, queryset, membership, harvest_year):
        ids = OrganizationMembership.objects.filter(
            primary_organization=membership.primary_organization).distinct().values_list(
            'secondary_organization', flat=True
        )
        if harvest_year and harvest_year != "All":
            queryset = queryset.filter(
                assessmentworkflow__author_organization__uuid__in=ids,
                assessmentworkflow__harvest_year=harvest_year,
            )
        else:
            queryset = queryset.filter(
                assessmentworkflow__author_organization__uuid__in=ids,
            )

        return queryset.exclude(
            assessmentworkflow__assessment_status=""
        ).exclude(
            assessmentworkflow__workflow_status='error'
        )


class OpenAssessmentTypesListView(BaseAssessmentTypeView, ListAPIView):
    """
    List of all Assessment Types with connected
    instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections

    :param kind - optional, used to filter by assessment_type__kind

    """
    serializer_class = AssessmentTypeSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return AssessmentType.objects.open_assessment_types(self.request.membership)

# Todo: To be removed
class AssessmentTypeRetrieveView(BaseAssessmentTypeView, RetrieveAPIView):
    """
    Detail representation of Assessment Type with
    connected instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections

    """
    serializer_class = AssessmentTypeFullSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_serializer_context(self):
        assessment_pk = self.kwargs.get('assessment_pk', '')
        workflow = AssessmentWorkflow.objects.filter(assessment__pk=assessment_pk).first()

        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'workflow': workflow if workflow else None
        }

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        assessment_type_data = serializer.data

        return Response(assessment_type_data)


class AssessmentTypeRetrieveView_V2(BaseAssessmentTypeView, RetrieveAPIView):
    """
    Detail representation of Assessment Type with
    connected instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections

    Using Dynamic Evidence Configuration
    """
    serializer_class = AssessmentTypeFullSerializer_V2
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_serializer_context(self):
        assessment_pk = self.kwargs.get('assessment_pk', '')
        workflow = AssessmentWorkflow.objects.filter(assessment__pk=assessment_pk).first()

        dec = workflow.get_configuration() if workflow else None

        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'dec': dec,
            'workflow': workflow if workflow else None
        }

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        assessment_type_data = serializer.data

        return Response(assessment_type_data)




class AssessmentTypeRetrieveAPIView(BaseAssessmentTypeView, RetrieveAPIView):
    """
    Detail representation of Assessment Type with
    connected instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections

    """
    serializer_class = AssessmentTypeFullSerializer
    permission_classes = (IsAuthenticated, )

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'lang': self.kwargs.get('lang'),
            'role': GROWER
        }

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        lang = self.kwargs.get('lang')
        serializer = self.get_serializer(instance)
        if lang in dict(settings.LANGUAGES):
            with translation.override(lang):
                assessment_type_data = serializer.data
        else:
            assessment_type_data = serializer.data

        return Response(assessment_type_data)


class AssessmentTypesStandards(ListAPIView):
    """
    List of all Assessment Types with connected
    instances of AssessmentTypeDetail contains connected Questionnaires,
    PossibleAnswerSets, DocumentTypes, Triggers, AssessmentTypeSections

    :param kind - optional, used to filter by assessment_type__kind

    """
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        query_set = AssessmentType.objects.filter(kind='assessment')
        is_public = self.request.query_params.get('is_public')
        if is_public:
            try:
                is_public = int(is_public)
                query_set = query_set.filter(is_public=is_public)
            except ValueError:
                pass

        query_set = query_set.values(
            'uuid', 'edition', 'available_languages', 'is_public'
        ).distinct()
        return query_set

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        data = []
        for assessment_type in queryset:
            data.append(
                {
                    assessment_type['uuid']: {
                        'edition': assessment_type['edition'],
                        'language': assessment_type['available_languages'],
                        'is_public': assessment_type['is_public']
                    }
                }
            )
        return Response(data)
