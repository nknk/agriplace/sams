from django.conf.urls import patterns, url

from assessment_types.api.internal.views import (
    AssessmentTypeRetrieveView, AssessmentTypeListView, OpenAssessmentTypesListView,
    AssessmentTypeWorkflowCountView, AssessmentTypeRetrieveView_V2
)

urlpatterns = patterns(
    'assessment_types.api.internal.views',
    url(
        r'^open/$',
        OpenAssessmentTypesListView.as_view(),
        name='open-assessments-types'
    ),
    url(
        r'(?P<pk>.\w+)/assessment/(?P<assessment_pk>.+)/$',
        AssessmentTypeRetrieveView_V2.as_view(),
        name='assessment-type'
    ),
    url(
        r'^workflow-counts$',
        AssessmentTypeWorkflowCountView.as_view(),
        name='assessment-type-count'
    ),
    url(
        r'^$',
        AssessmentTypeListView.as_view(),
        name='assessment-types'
    ),
)