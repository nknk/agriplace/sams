import json

import faker
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from assessments.tests.factories import QuestionFactory, QuestionLevelFactory, QuestionnaireFactory, \
    QuestionLabelFactory, DynamicLabelConfigurationFactory, QuestionLabelConfigurationsFactory, PossibleAnswerSetFactory
from core.constants import INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.tests.factories import AssessmentWorkflowFactory
from farm_group.utils import GROWER
from api.internal.tests.mixins import AuthAPIMixin, APITestSetupMixin
from assessment_types.tests.factories import AssessmentTypeFactory
from organization.tests.factories import (
    OrganizationFactory, ProductFactory, OrganizationTypeFactory, OrganizationMembershipFactory
)

faker = faker.Factory.create()


class TestAssessmentTypes(APITestCase, APITestSetupMixin, AuthAPIMixin):

    GG_CODE = 'globalgap_ifa'
    TN_CODE = 'Tesco_nurture'

    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_farm_groups()
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)

        self.primary_organization = OrganizationFactory.create()
        self.organization = OrganizationFactory.create(users=(self.user,))
        organization_type = OrganizationTypeFactory.create()
        self.membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )

        self.GG_assessment_type = AssessmentTypeFactory.create(code=self.GG_CODE)
        self.TN_assessment_type = AssessmentTypeFactory.create(code=self.TN_CODE)
        self.TN_agrifram = AssessmentTypeFactory.create(code=self.TN_CODE, kind="agriform")
        self.product = ProductFactory.create(organization=self.organization)
        self.gg_assessment = self.create_assessment(
            self.organization, self.user, self.GG_assessment_type, self.product
        )

        self.gg_assessment_without_label = self.create_assessment(
            self.organization, self.user, self.GG_assessment_type, self.product
        )

        self.tn_assessment = self.create_assessment(
            self.organization, self.user, self.TN_assessment_type, self.product
        )

        question_levels = []
        question_levels.append(QuestionLevelFactory.create(assessment_type=self.GG_assessment_type, title="Major"))
        question_levels.append(QuestionLevelFactory.create(assessment_type=self.GG_assessment_type, title="Minor"))
        question_levels.append(QuestionLevelFactory.create(assessment_type=self.GG_assessment_type, title="Recommended"))

        possible_answer_set = PossibleAnswerSetFactory.create(assessment_type=self.GG_assessment_type)

        questionnaire1 = QuestionnaireFactory.create(
            code='AF', assessment_type=self.GG_assessment_type, order_index=1, name='All Farm Base Module'
        )

        questionnaire2 = QuestionnaireFactory.create(
            code='FV', assessment_type=self.GG_assessment_type, order_index=3, name='Module Fruit & Vegetables'
        )

        # Create Question Labels
        self._create_question_labels()

        self.all_questions = []
        for question_level in question_levels:
            if question_level.title == "Recommended":
                questionnaire = questionnaire2
                counter = 5
            else:
                questionnaire = questionnaire1
                counter = 10

            for i in range(counter):
                if i <= 7:
                    question = QuestionFactory.create(
                        level_object=question_level, questionnaire=questionnaire,
                        possible_answer_set=possible_answer_set
                    )
                else:
                    question = QuestionFactory.create(
                        level_object=question_level, questionnaire=questionnaire,
                        possible_answer_set=possible_answer_set, code="{}-{}".format(question_level.title, i)
                    )

                self.all_questions.append(question)


        # creating DLC
        self.DLC = DynamicLabelConfigurationFactory.create()
        rq_10 = self.all_questions[0:10]
        i = -1
        for x in rq_10:
            i += 1
            QuestionLabelConfigurationsFactory.create(
                configuration=self.DLC,
                question=x,
                label=self.question_labels[i]
            )
            if i > 5:
                QuestionLabelConfigurationsFactory.create(
                    configuration=self.DLC,
                    question=x,
                    label=self.question_labels[i+10]
                )

        self.workflow_context = AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.GG_assessment_type, assessment=self.gg_assessment,
            membership=self.membership, label_configuration=self.DLC
        )

        self.workflow_context_without_label = AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.GG_assessment_type, assessment=self.gg_assessment_without_label,
            membership=self.membership
        )


        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))


    def _create_question_labels(self):
        self.question_labels = []
        for i in range(30):
            self.question_labels.append(QuestionLabelFactory.create())



    def test_get_open_assessment_types(self):

        response = self.client.get(
            reverse('open-assessments-types', kwargs={
                'membership_pk': self.membership.pk
            }),
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 2)

        self.tn_assessment.state = 'closed'
        self.tn_assessment.save()

        response = self.client.get(
            reverse('open-assessments-types', kwargs={
                'membership_pk': self.membership.pk
            }),
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 1)

        self.assertEqual(response_content[0]["uuid"], self.GG_assessment_type.pk)


    def test_assessment_type_detail_full_v2(self):
        response = self.client.get(
            reverse('assessment-type', kwargs={
                'pk': self.TN_agrifram.pk,
                'membership_pk': self.membership.pk,
                'assessment_pk': self.gg_assessment.pk
            }),

            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        assessment_type = response.data

        self.assertNotEqual(assessment_type, {}, msg='Assessment type must not be empty')


    def test_assessment_type_question_label_configuration(self):
        response = self.client.get(
            reverse('assessment-type', kwargs={
                'pk': self.GG_assessment_type.pk,
                'membership_pk': self.membership.pk,
                'assessment_pk': self.gg_assessment.pk
            }),

            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        assessment_type = response.data
        self.assertNotEqual(assessment_type['question_label_configuration'], {}, msg='Assessment type must have question_label_configuration object')
        self.assertNotEqual(assessment_type['question_label_configuration'], None, msg='Assessment type must not be Null or None')


        # Checking without label Configuration

        response = self.client.get(
            reverse('assessment-type', kwargs={
                'pk': self.GG_assessment_type.pk,
                'membership_pk': self.membership.pk,
                'assessment_pk': self.gg_assessment_without_label.pk
            }),

            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        assessment_type = response.data
        self.assertEqual(assessment_type['question_label_configuration'], None,
                            msg='Assessment type without label configuration must not have question_label_configuration object without')


    def test_agrifarm_detail_full(self):
        response = self.client.get(
            reverse('assessment-type', kwargs={
                'pk': self.GG_assessment_type.pk,
                'membership_pk': self.membership.pk,
                'assessment_pk': self.gg_assessment.pk
            }),

            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        assessment_type = response.data

        self.assertNotEqual(assessment_type, {}, msg='Assessment type must not be empty')
        self.assertTrue('triggers' in assessment_type,
                        msg='The assessment must have an \'triggers\' field')
        self.assertEqual(assessment_type['triggers'], {}, msg='Triggers must be empty')

    def test_assessment_types_versions(self):

        response = self.client.get(
            reverse('assessments-types-versions'),
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 2)

        self.TN_assessment_type.is_public = False
        self.TN_assessment_type.save()

        response = self.client.get(
            reverse('assessments-types-versions'),
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 2)

    def test_assessment_types_retrieval(self):

        response = self.client.get(
            reverse('assessment-standard', kwargs={
                'pk': self.GG_assessment_type.pk,
                'lang': 'en'
            }),
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(self.GG_assessment_type.uuid, response_content['uuid'])
        self.assertIn('questionnaires', response_content)
        self.assertIn('document_types', response_content)
        self.assertIn('sections', response_content)
        self.assertIn('sharing_conditions', response_content)


class TestAssessmentTypeWorkflowCount(APITestCase, APITestSetupMixin, AuthAPIMixin):

    GG_CODE = 'globalgap_ifa'
    TN_CODE = 'Tesco_nurture'

    GROWER_USERNAME = faker.user_name()
    INSPECTOR_USERNAME = faker.user_name()
    AUDITOR_USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_farm_groups()
        self.grower = self.create_user(username=self.GROWER_USERNAME, password=self.PASSWORD)
        self.internal_inspector = self.create_user(username=self.INSPECTOR_USERNAME, password=self.PASSWORD)
        self.internal_auditor = self.create_user(username=self.AUDITOR_USERNAME, password=self.PASSWORD)

        self.primary_organization = OrganizationFactory.create()
        self.grower_organization = OrganizationFactory.create(
            users=(self.grower,)
        )
        self.internal_inspector_organization = OrganizationFactory.create(
            users=(self.internal_inspector,)
        )
        self.internal_auditor_organization = OrganizationFactory.create(
            users=(self.internal_auditor,)
        )
        organization_type = OrganizationTypeFactory.create()
        self.grower_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.grower_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        self.internal_inspector_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_inspector_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )
        self.internal_auditor_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_auditor_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_AUDITOR)
        )
        self.product = ProductFactory.create(organization=self.grower_organization)
        self.GG_assessment_type = AssessmentTypeFactory.create(code=self.GG_CODE)
        self.TN_assessment_type = AssessmentTypeFactory.create(code=self.TN_CODE)
        self.create_workflows()

    def create_workflows(self):
        for i in range(4):
            gg_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.GG_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.GG_assessment_type, assessment=gg_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership, assessment_status='internal_audit_request'
            )

            tn_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.TN_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.TN_assessment_type, assessment=tn_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership, assessment_status='internal_audit_request'
            )

    def test_internal_inspector_workflow_count(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))
        response = self.client.get(
            reverse('assessment-type-count', kwargs={
                'membership_pk': self.internal_inspector_membership.pk
            }),
            HTTP_AUTHORIZATION=token
        )
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 2)

        for result in response_content:
            self.assertEqual(result.get('work_flow_count'), 4)

    def test_internal_auditor_workflow_count(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.AUDITOR_USERNAME, password=self.PASSWORD))
        response = self.client.get(
            reverse('assessment-type-count', kwargs={
                'membership_pk': self.internal_auditor_membership.pk
            }),
            HTTP_AUTHORIZATION=token
        )
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 2)

        for result in response_content:
            self.assertEqual(result.get('work_flow_count'), 4)
