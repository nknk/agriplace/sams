import factory
from factory import DjangoModelFactory
from datetime import datetime
from assessments.models import AssessmentType


class AssessmentTypeFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "Test AssessmentType {0}".format(n))
    description = factory.sequence(lambda n: "Description of AssessmentType {0}".format(n))
    code = factory.sequence(lambda n: "asm-type-{0}".format(n))
    kind = "assessment"
    has_logo = True
    modified_time = datetime(2015, 1, 1)
    is_test = False
    created_time = datetime(2014, 1, 1)
    assessment_sections = "overview,evidence,questionnaires,results,sharing"
    edition = 1
    is_public = True
    remarks = ""
    has_product_selection = True

    class Meta:
        model = AssessmentType
