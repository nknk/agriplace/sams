# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tagging', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tag',
            options={'ordering': ('name',), 'verbose_name': 'tag', 'verbose_name_plural': 'tags', 'permissions': (('view_tag', 'Can view tag'),)},
        ),
        migrations.AlterModelOptions(
            name='taggeditem',
            options={'verbose_name': 'tagged item', 'verbose_name_plural': 'tagged items', 'permissions': (('view_taggeditem', 'Can view tagged item'),)},
        ),
    ]
