from django.conf.urls import patterns, url

from organization.views_certification import (
    OrgCertificateCreator,
    ProductCertificateCreator
)

urlpatterns = patterns(
    'organization.views',
)

urlpatterns += patterns(
    'organization.views_organization',
    # Organization
    url(r'^$', 'organization_home', name='my_org'),

    url(r'^overview/$', 'organization_overview', name='my_org_overview'),

    url(r'^access/$', 'organization_access', name='my_org_access'),

    url(r'^detail/$', 'organization_detail', name='my_org_detail'),

    url(r'^edit/full$', 'organization_update', name='my_org_form_full'),

    url(r'^delete/$', 'organization_delete', name='my_org_delete'),

    url(r'^factsheet/edit$', 'organization_factsheet_edit', name='my_org_fact_sheet_edit'),

    url(r'^factsheet', 'organization_factsheet_detail', name='my_org_fact_sheet_detail'),

    url(r'^database', 'my_database', name='my_database'),
)

urlpatterns += patterns(
    'organization.views_organization_unit',
    # Organization units
    url(r'^units/$', 'unit_list', name='my_unit_list'),

    url(r'^units/(?P<unit_pk>[\w-]+)/delete$', 'unit_delete', name='my_unit_delete'),

    url(r'^units/new$', 'unit_create', name='my_unit_create'),

    url(r'^units/(?P<unit_pk>[\w-]+)/overview$', 'unit_overview', name='my_unit_overview'),

    url(r'^units/(?P<unit_pk>[\w-]+)/detail$', 'unit_detail', name='my_unit_detail'),

    url(r'^units/(?P<unit_pk>[\w-]+)/factsheet/edit', 'unit_factsheet_edit', name='my_unit_fact_sheet_edit'),

    url(r'^units/(?P<unit_pk>[\w-]+)/factsheet$', 'unit_factsheet_detail', name='my_unit_fact_sheet_detail'),

    url(r'^units/(?P<unit_pk>[\w-]+)/edit', 'unit_update', name='my_unit_update'),

    url(r'^units/(?P<unit_pk>[\w-]+)$', 'unit_home', name='my_unit'),
)

urlpatterns += patterns(
    'organization.views_product',
    # Products
    url(r'^products/$', 'product_list', name='my_product_list'),

    url(r'^products/new$', 'product_create', name='my_product_create'),

    url(r'^products/(?P<product_pk>[\w-]+)/overview$', 'product_overview', name='my_product_overview'),

    url(r'^products/(?P<product_pk>[\w-]+)/detail$', 'product_detail', name='my_product_detail'),

    url(r'^products/(?P<product_pk>[\w-]+)/edit', 'product_update', name='my_product_update'),

    url(r'^products/(?P<product_pk>[\w-]+)/factsheet/edit$', 'product_factsheet_edit',
        name='my_product_fact_sheet_edit'),

    url(r'^products/(?P<product_pk>[\w-]+)/factsheet$', 'product_factsheet_detail',
        name='my_product_fact_sheet_detail'),

    url(r'^products/(?P<product_pk>[\w-]+)/delete$', 'product_delete', name='my_product_delete'),

    url(r'^products/(?P<product_pk>[\w-]+)$', 'product_home', name='my_product'),

    url(
        r'^products/(?P<product_pk>[\w-]+)/identifier/new$',
        'create_product_identifier',
        name='create_product_identifier'
    ),

    url(
        r'^products/(?P<product_pk>[\w-]+)/identifier/(?P<identifier_pk>[\w-]+)/edit$',
        'edit_product_identifier',
        name='edit_product_identifier'
    ),

    url(
        r'^products/(?P<product_pk>[\w-]+)/identifier/(?P<identifier_pk>[\w-]+)/delete$',
        'delete_product_identifier',
        name='delete_product_identifier'
    ),

)

urlpatterns += patterns(
    'organization.views_certification',
    # # Certifications

    # Create Organization Certification
    url(
        r'^certifications/org/new$',
        OrgCertificateCreator.as_view(),
        name='my_org_certification_create'
    ),

    # Create Product Certification
    url(
        r'^certifications/product/new$',
        ProductCertificateCreator.as_view(),
        name='my_product_certification_create'
    ),

    url(r'^certifications/(?P<certification_pk>[\w-]+)/edit', 'certification_edit', name='my_certification_edit'),
    # Edit

    url(r'^certifications/(?P<certification_pk>[\w-]+)/delete', 'certification_delete', name='my_certification_delete'),
    # Delete

    url(r'^certifications/(?P<certification_pk>[\w-]+)', 'certification_detail', name='my_certification_detail'),

    url(r'^certifications$', 'certification_list', name='my_certification_list'),
)

urlpatterns += patterns(
    'attachments.views',
    # Documents
    url(r'^documents/$', 'index', name='documents_index'),
)
