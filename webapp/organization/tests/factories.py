from django.contrib.auth.models import Group
import factory
from datetime import datetime
from django.contrib.auth import get_user_model
from factory import DjangoModelFactory, SubFactory
from factory.django import FileField

from organization.models import (
    Organization,
    Product,
    ProductType,
    ProductTypeGroup,
    ProductIdentifier,
    Certification,
    CertificationType,
    OrganizationType, OrganizationMembership, CertificationFile, OrganizationTheme)


class OrganizationThemeFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "Theme {0}".format(n))
    slug = factory.sequence(lambda n: "theme-{0}".format(n))
    description = factory.sequence(lambda n: "Theme {0} description".format(n))
    layout = factory.sequence(lambda n: "Layout {0}".format(n))

    class Meta:
        model = OrganizationTheme


class OrganizationFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "Organization {0}".format(n))
    slug = factory.sequence(lambda n: "org-{0}".format(n))
    description = factory.sequence(lambda n: "Organization {0} description".format(n))
    is_test = False
    logo = factory.sequence(lambda n: "logo-{0}".format(n))
    created_time = datetime.now()
    mailing_country = factory.sequence(lambda n: "NL {0}".format(n))
    mailing_address2 = factory.sequence(lambda n: "Address 2 {0}".format(n))
    mailing_address1 = factory.sequence(lambda n: "Address 1 {0}".format(n))
    mailing_province = factory.sequence(lambda n: "North Holland {0}".format(n))
    email = factory.sequence(lambda n: " {0}-org@organization.com".format(n))
    modified_time = datetime.now()
    phone = factory.sequence(lambda n: "+31 123 456789{0}".format(n))
    remarks = factory.sequence(lambda n: "Remarks {0}".format(n))
    url = factory.sequence(lambda n: "http://producer-{0}.com".format(n))
    mailing_postal_code = factory.sequence(lambda n: "5372{0}".format(n))
    mailing_city = factory.sequence(lambda n: "Amsterdam {0}".format(n))
    identification_number = factory.sequence(lambda n: "123456789{0}".format(n))
    theme = SubFactory(OrganizationThemeFactory)

    class Meta:
        model = Organization

    @factory.post_generation
    def users(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for user in extracted:
                self.users.add(user)


class UserFactory(DjangoModelFactory):
    username = factory.Sequence(u'user_{0}'.format)
    email = factory.Sequence(u'username+{0}@example.com'.format)
    password = factory.PostGenerationMethodCall('set_password', 'test')
    first_name = factory.Sequence(u'first_name_{0}'.format)
    last_name = factory.Sequence(u'last_name_{0}'.format)
    is_staff = False
    is_active = True
    is_superuser = False
    last_login = datetime(2016, 1, 1)
    date_joined = datetime(2015, 1, 1)

    class Meta(object):
        model = get_user_model()
        django_get_or_create = ('email', 'username')


class ProductTypeFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "ProductType {0}".format(n))

    class Meta:
        model = ProductType


class ProductFactory(DjangoModelFactory):
    organization = SubFactory(OrganizationFactory)
    product_type = SubFactory(ProductTypeFactory)

    class Meta:
        model = Product


class ProductTypeGroupFactory(DjangoModelFactory):
    class Meta:
        model = ProductTypeGroup


class ProductIdentifierFactory(DjangoModelFactory):
    class Meta:
        model = ProductIdentifier


class CertificationTypeFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "Certification type name {0}".format(n))
    code = factory.sequence(lambda n: "Certification type code {0}".format(n))

    class Meta:
        model = CertificationType


class CertificationFactory(DjangoModelFactory):
    organization = SubFactory(OrganizationFactory)
    product = SubFactory(ProductFactory)
    certification_type = SubFactory(CertificationTypeFactory)

    class Meta:
        model = Certification


class CertificationFileFactory(DjangoModelFactory):
    original_file_name = factory.sequence(lambda n: "original file name {0}".format(n))
    certification = SubFactory(CertificationFactory)
    file = FileField()

    class Meta:
        model = CertificationFile


class GroupFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "group_name_{}".format(n))

    class Meta:
        model = Group


class OrganizationTypeFactory(DjangoModelFactory):
    name = factory.sequence(lambda n: "OrganizationType {0}".format(n))
    slug = factory.sequence(lambda n: "org-{0}".format(n))

    class Meta:
        model = OrganizationType


class OrganizationMembershipFactory(DjangoModelFactory):
    primary_organization = SubFactory(OrganizationFactory)
    secondary_organization = SubFactory(OrganizationFactory)
    organization_type = SubFactory(OrganizationTypeFactory)
    membership_number = factory.sequence(lambda n: "123456789{0}".format(n))
    role = SubFactory(GroupFactory)

    class Meta:
        model = OrganizationMembership
