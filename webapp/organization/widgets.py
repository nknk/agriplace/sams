from django.forms import Widget, widgets
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _


class ProductTypeSelect(Widget):
    def render(self, name, value, attrs=None):
        """Renders this widget into an html string

            args:
            name  (str)  -- name of the field
            value (str)  -- a json string of a two-tuple list automatically passed in by django
            attrs (dict) -- automatically passed in by django (unused in this function)
        """
        # return render('organization/widgets/productTypeSelect.html')
        return mark_safe(u'<product-type-select name="product_type" initial-value="{value}"></product-type-select>'.format(
            value=value
        ))


class ColorPickerWidget(widgets.TextInput):

    class Media:
        css = {
            'all': ('/static/organization/components/farbtastic/farbtastic.css',)
            }
        js = ('/static/organization/components/farbtastic/colorpicker.js',
              '/static/organization/components/farbtastic/farbtastic.js',)

    def render(self, name, value, attrs=None):
        text_input_html = super(ColorPickerWidget, self).render(name, value, attrs)
        text_link_html = u'<a id="id_color_picker" href="#" onclick="return false;"> %s</a>' % _(u'Palette')
        return mark_safe('%s %s' % (text_input_html, text_link_html))