import logging

from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.forms.util import ErrorList
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from assessments.models import Questionnaire
from organization.models import OrganizationUnit
from sams.constants import ICONS
from sams.context_processors import questionnaire_form_context_processor
from sams.flipsection import FlipSectionManager
from ap_extras.item_list import ItemListModel
from sams.decorators import session_organization_required

from organization.forms import OrganizationUnitBasicsForm, OrganizationUnitUpdateForm
from organization.views import get_organization_context

logger = logging.getLogger(__name__)

"""
Organization unit views
"""


def get_unit_context(request, kwargs, page_name=""):
    """
    Common context for Organization Unit views
    """
    context, organization = get_organization_context(request, kwargs, page_name)
    assert 'unit_pk' in kwargs
    try:
        unit = organization.units.get(pk=kwargs['unit_pk'])
    except OrganizationUnit.DoesNotExist:
        raise Http404(_("Organization unit does not exist."))
    context.update({
        'unit': unit,
        'page_title': unit.name,
        'page_title_secondary': _("organization unit")
    })
    context['page_triggers'] += ['my_unit_list']
    context['breadcrumbs'] += [
        {
            'text': _("Organization units"),
            'icon': ICONS['organization_unit'],
            'url': reverse('my_unit_list', args=[organization.slug])
        },
        {
            'text': unit.name,
            'icon': ICONS['organization_unit']
        }
    ]

    context['nav3_links'] = []
    return context, organization, unit


#################################################################################
# Views
#################################################################################


@login_required
@session_organization_required
def unit_list(request, *args, **kwargs):
    """
    Organization unit list view
    """
    context, organization = get_organization_context(request, kwargs, "my_unit_list")
    # Units item list model
    units_model = ItemListModel()
    units_model.id = "units"
    units_model.headers = [_("Organization unit")]
    units_model.items_model = organization.units.all().order_by('name')
    units_model.item_url = lambda unit: reverse(
        'my_unit', kwargs={'organization_slug': organization.slug, 'unit_pk': unit.pk}
    )
    units_model.item_icon_class = ICONS.get("organization_unit")
    units_model.create_url = reverse("my_unit_create", kwargs={'organization_slug': organization.slug})
    units_model.create_text = _("Create new organization unit")
    units_model.no_items_text = _("No units available.")
    context["units_model"] = units_model
    # Generate the items here so we catch lambda errors that would otherwise be ignored in the template
    context['breadcrumbs'].append(
        {
            'text': _("Organization units"),
            'icon': ICONS['organization_unit']
        }
    )
    return render(request, "organization_unit/unit_list.html", context)


@login_required
def unit_home(request, *args, **kwargs):
    """
    Organization unit section entry point

    :param: organization_slug: organization slug
    :param: unit_pk: unit primary key
    """
    return redirect('my_unit_detail', *args, **kwargs)


@login_required
@session_organization_required
def unit_overview(request, *args, **kwargs):
    """
    Organization unit overview page
    """
    context, organization, unit = get_unit_context(request, kwargs, "my_unit_overview")

    unit_details = []
    context["unit_details"] = unit_details

    # Units item list model
    products_model = ItemListModel()
    products_model.id = "products"
    products_model.headers = [_("Product")]
    products_model.items_model = unit.products.all()
    products_model.item_url = lambda product: reverse(
        'my_product', kwargs={'organization_slug': organization.slug, 'product_pk': product.pk}
    )
    products_model.item_icon_class = ICONS.get("product")
    products_model.create_url = reverse("my_product_create", kwargs={'organization_slug': unit.organization.pk})
    products_model.create_text = _("Create new product")
    products_model.no_items_text = _("No products available.")
    context["products_model"] = products_model
    # Generate the items here so we catch lambda errors that would otherwise be ignored in the template
    return render(request, "organization_unit/unit_overview.html", context)


@login_required
@session_organization_required
def unit_detail(request, *args, **kwargs):
    """
    Organization detail (view organization profile) + edit
    """
    context, organization, unit = get_unit_context(request, kwargs, 'my_unit_detail')
    # Section models for flip sections
    section_models = context['section_models'] = {
        'basics': {
            'form_class': OrganizationUnitBasicsForm,
            'title': _("Organization unit details")
        },
    }
    flip_manager = context['flip_manager'] = FlipSectionManager(unit, section_models)

    if request.method == 'POST':
        (success, url_hash) = flip_manager.save(request.POST, request.FILES)
        if success:
            messages.success(request, _("{unit} saved.").format(unit=unit))
        else:
            messages.error(
                request,
                _("Could not save {unit}. Please fix the errors below.".format(unit=unit))
            )
        return redirect('{0}{1}'.format(request.get_full_path(), url_hash))

    return render(request, 'organization_unit/unit_detail.html', context)


@login_required
@session_organization_required
def unit_update(request, *args, **kwargs):
    context, organization, unit = get_unit_context(request, kwargs, "my_unit_update")
    if request.method == 'POST':
        form = OrganizationUnitUpdateForm(request.POST, instance=unit)
        if form.is_valid():
            try:
                form.save()
                # Great success!
                messages.success(request, _("%s was saved.") % unit.name)
                return redirect("my_unit", organization.slug, unit.pk)
            except Exception as e:
                errors = form._errors.setdefault(forms.forms.NON_FIELD_ERRORS, ErrorList())
                errors.append(e)
    else:
        # GET
        form = OrganizationUnitUpdateForm(instance=unit)
    context["form"] = form
    return render(request, "organization_unit/unit_update.html", context)


@login_required
@session_organization_required
def unit_factsheet_detail(request, *args, **kwargs):
    context, organization, unit = get_unit_context(request, kwargs, "my_unit_fact_sheet_detail")
    context['page_triggers'] += ["my_unit_fact_sheet"]
    fact_sheet = Questionnaire.objects.get(code="_fact_sheet_organization_unit")
    fact_sheet_detail = fact_sheet.get_detail(target_uuid=unit.uuid)
    context["fact_sheet_detail"] = fact_sheet_detail
    return render(request, "organization_unit/my_unit_fact_sheet_detail.html", context)


@login_required
@session_organization_required
def unit_factsheet_edit(request, *args, **kwargs):
    context, organization, unit = get_unit_context(request, kwargs, "my_unit_fact_sheet_edit")
    context['page_triggers'] += ["my_unit_fact_sheet"]
    questionnaire = Questionnaire.objects.get(code="_fact_sheet_organization_unit")
    # QuestionnaireForm context work, including save on POST
    if questionnaire_form_context_processor(request, context, questionnaire, target=unit):
        # Success
        return redirect("my_unit_fact_sheet_detail",
                        organization_slug=organization.slug,
                        unit_pk=unit.pk)
    else:
        # First time or form errors
        return render(request, "organization_unit/my_unit_fact_sheet_edit.html", context)


@login_required
@session_organization_required
def unit_delete(request, *args, **kwargs):
    context, organization, unit = get_unit_context(request, kwargs, "my_unit_delete")
    context.update({
        "title": _("Delete {}").format(unit.name),
        "message": _("Are you sure you want to delete the organization unit %s?") % unit.name,
        "yes_text": _("Yes, delete %s") % unit.name,
        "no_text": _("Cancel"),
        "no_url": reverse('my_org', args=[organization.slug]),
    })

    if request.method == 'POST':
        unit.delete()
        messages.success(request, _("%s was deleted.") % unit.name)
        return redirect("my_unit_list", organization.slug)

    return render(request, "core/confirmation.html", context)


@login_required
@session_organization_required
def unit_create(request, *args, **kwargs):
    """
    Organization unit create page

    create() is an **Organization view** because it receives an organization_slug and doesn't work with
    an existing OrganizationUnit object
    """
    context, organization = get_organization_context(request, kwargs, "my_unit_create")

    if request.method == 'POST':
        # POST
        form = OrganizationUnitUpdateForm(request.POST)
        if form.is_valid():
            matching_units = organization.units.filter(name=form.cleaned_data["name"]).count()
            if matching_units:
                errors = form._errors.setdefault("name", ErrorList())
                errors.append(_("An organization unit with this name already exists!"))
            else:
                try:
                    new_unit = OrganizationUnit(**form.cleaned_data)
                    new_unit.organization = context["organization"]
                    new_unit.save()
                    # Great success!
                    return redirect("my_unit", organization.slug, new_unit.pk)
                except Exception as e:
                    errors = form._errors.setdefault(forms.forms.NON_FIELD_ERRORS, ErrorList())
                    errors.append(e)
    else:
        # GET
        form = OrganizationUnitUpdateForm()
    context["form"] = form
    return render(request, "organization_unit/unit_create.html", context)
