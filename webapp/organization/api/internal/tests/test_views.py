import json
from django.contrib.auth.models import Group
import faker
from ddt import ddt, unpack, data
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.internal.tests.mixins import APITestSetupMixin, AuthAPIMixin
from assessments.tests.factories import AssessmentFactory
from assessment_types.tests.factories import AssessmentTypeFactory
from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.tests.factories import AssessmentWorkflowFactory
from organization.tests.factories import (
    ProductFactory,
    OrganizationFactory,
    ProductTypeFactory,
    CertificationFactory, OrganizationTypeFactory, OrganizationMembershipFactory, CertificationFileFactory,
    CertificationTypeFactory)

faker = faker.Factory.create()


class TestSetupProductList(APITestCase, APITestSetupMixin):
    def setUp(self):
        self.user = self.create_user()
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.product = ProductFactory.create(organization=self.organization)

        self.url = reverse('organization-products', kwargs={
            'organization_pk': self.organization.pk
        })

        self.token = self.get_api_token()

    def test_list(self):
        """
        Fetch a list of products
        """
        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {0}'.format(self.token))
        data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertNotEqual(data, [], msg='A product list must be non-empty')
        self.assertEqual(len(data), 1, 'A response must have 1 product')

    def test_add(self):
        """
        Add a new product
        """
        trader_org = OrganizationFactory.create()
        product_type = ProductTypeFactory.create()
        product = {
            "harvest_year": 2016,
            "area": 10,
            "product_type": product_type.pk,
            "name": "?",
            "partners": [{
                "organization_uuid": trader_org.pk,
                "contract_number": 789
            }],
            "remarks": ""
        }

        response = self.client.post(
            self.url,
            data=product,
            format='json',
            HTTP_AUTHORIZATION='Token {0}'.format(self.token),
            Organization=self.organization.pk
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.content)

    def test_product_used_in_assessments(self):
        """
        Fetch a list of products, each of which contain a list of assessments
        the product is being used in
        """
        self.assessment = AssessmentFactory(created_by=self.user, last_modified_by=self.user, products=(self.product,))

        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {0}'.format(self.token))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
            msg=(
                u"Unexpected status code when loading '{url}': "
                u"expected {expected} but got {actual}"
            ).format(
                url=self.url,
                expected=status.HTTP_200_OK,
                actual=response.status_code
            )
        )

        data = response.data

        self.assertEqual(len(data), 1, 'A response must have 1 product')

        self.assertTrue('used_in_assessments' in data[0], msg='Must have \'used_in_assessments\' field')
        used_in_assessments = data[0]['used_in_assessments']

        self.assertEqual(len(used_in_assessments), 1, 'A \'used_in_assessments\' property must have 1 assessment')
        assessment = used_in_assessments[0]

        self.assertEqual(
            assessment['uuid'], self.assessment.pk,
            msg=(
                u"expected assessment \'{expected}\' but got \'{actual}\'"
            ).format(
                expected=self.assessment.pk,
                actual=assessment['uuid']
            )
        )

    def test_product_used_in_certificates(self):
        """
        Fetch a list of products, each of which contain a list of certifications
        the product is being used in
        """
        self.certification = CertificationFactory(organization=self.organization, product=self.product)

        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {0}'.format(self.token))

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
            msg=(
                u"Unexpected status code when loading '{url}': "
                u"expected {expected} but got {actual}"
            ).format(
                url=self.url,
                expected=status.HTTP_200_OK,
                actual=response.status_code
            )
        )

        data = response.data

        self.assertEqual(len(data), 1, 'A response must have 1 product')

        self.assertTrue('used_in_certificates' in response.data[0], msg='Must have \'used_in_certificates\' field')
        used_in_certificates = data[0]['used_in_certificates']

        self.assertEqual(len(used_in_certificates), 1, 'A \'used_in_certificates\' property must have 1 certification')
        certification = used_in_certificates[0]

        self.assertEqual(
            certification['uuid'], self.certification.pk,
            msg=(
                u"expected certification \'{expected}\' but got \'{actual}\'"
            ).format(
                expected=self.certification.pk,
                actual=certification['uuid']
            )
        )


@ddt
class TestAssessmentAgreementAPISetup(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa_v5'

    def setUp(self):
        self.user = self.create_user()
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.auditor = self.create_user(
            username=faker.user_name(), password=faker.word()
        )
        self.auditor_organization = OrganizationFactory.create(users=(self.auditor,))
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        self.workflow_context = AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            auditor_user=self.auditor, auditor_organization=self.auditor_organization,
            assessment_type=self.assessment_type, assessment=self.assessment
        )

        self.url = reverse('workflow-agreements', kwargs={
            'organization_pk': self.organization.pk, 'workflow_pk': self.workflow_context.pk
        })

        self.token = self.get_api_token()

    @unpack
    @data({'payload': {'form_data': {'confirm': True, 'start': 3, 'jaNeeHzpc': 'jaa'}}},
          {'payload': {'form_data': {'confirm': True, 'start': 3, 'jaNeeHzpc': 'nee'}}})
    def test_assessment_agreement_save(self, payload):
        """
        Test that a new assessment agreement is saved successfully
        """
        response = self.client.post(
            self.url, data=payload, format='json', HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED,
            msg=(
                u"Unexpected status code when loading '{url}': "
                u"expected {expected} but got {actual}"
            ).format(
                url=self.url,
                expected=status.HTTP_201_CREATED,
                actual=response.status_code
            )
        )


class TestOrganizationAuditors(APITestCase, APITestSetupMixin, AuthAPIMixin):

    GG_CODE = 'globalgap_ifa'
    TN_CODE = 'Tesco_nurture'

    GROWER_USERNAME = faker.user_name()
    INSPECTOR1_FIRST_NAME = faker.user_name()
    INSPECTOR1_LAST_NAME = faker.user_name()
    INSPECTOR1_USERNAME = faker.user_name()

    INSPECTOR2_FIRST_NAME = faker.user_name()
    INSPECTOR2_LAST_NAME = faker.user_name()
    INSPECTOR2_USERNAME = faker.user_name()
    AUDITOR_USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_farm_groups()
        self.grower = self.create_user(username=self.GROWER_USERNAME, password=self.PASSWORD)
        self.internal_inspector1 = self.create_user(
            username=self.INSPECTOR1_USERNAME, password=self.PASSWORD,
            first_name=self.INSPECTOR1_FIRST_NAME, last_name=self.INSPECTOR1_LAST_NAME
        )
        self.internal_inspector2 = self.create_user(
            username=self.INSPECTOR2_USERNAME, password=self.PASSWORD,
            first_name=self.INSPECTOR2_FIRST_NAME, last_name=self.INSPECTOR2_LAST_NAME
        )
        self.internal_auditor = self.create_user(username=self.AUDITOR_USERNAME, password=self.PASSWORD)

        self.primary_organization = OrganizationFactory.create()
        self.grower_organization = OrganizationFactory.create(
            users=(self.grower,)
        )
        self.internal_inspector_organization1 = OrganizationFactory.create(
            users=(self.internal_inspector1,)
        )
        self.internal_inspector_organization2 = OrganizationFactory.create(
            users=(self.internal_inspector2,)
        )
        self.internal_auditor_organization = OrganizationFactory.create(
            users=(self.internal_auditor,)
        )
        organization_type = OrganizationTypeFactory.create()
        self.grower_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.grower_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        self.internal_inspector_membership1 = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_inspector_organization1,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )
        self.internal_inspector_membership2 = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_inspector_organization2,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )
        self.internal_auditor_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_auditor_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_AUDITOR)
        )
        self.product = ProductFactory.create(organization=self.grower_organization)
        self.GG_assessment_type = AssessmentTypeFactory.create(code=self.GG_CODE)
        self.TN_assessment_type = AssessmentTypeFactory.create(code=self.TN_CODE)
        self.create_workflows()

    def create_workflows(self):
        for i in range(4):
            gg_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.GG_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.GG_assessment_type, assessment=gg_assessment,
                auditor_user=self.internal_inspector1, auditor_organization=self.internal_inspector_organization1,
                membership=self.grower_membership, assessment_status='internal_audit_request'
            )

            tn_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.TN_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.TN_assessment_type, assessment=tn_assessment,
                auditor_user=self.internal_inspector2, auditor_organization=self.internal_inspector_organization2,
                membership=self.grower_membership, assessment_status='internal_audit_request'
            )

    def test_internal_auditor_auditor_list(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.AUDITOR_USERNAME, password=self.PASSWORD))
        response = self.client.get(
            reverse('organization-auditors', kwargs={
                'membership_pk': self.internal_auditor_membership.pk
            }),
            HTTP_AUTHORIZATION=token
        )
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), 3)
        for result in response_content:
            if result.get('id') == '-1':
                continue
            self.assertIn(int(result.get('id')), [self.internal_inspector1.id, self.internal_inspector2.id])
            if result.get('id') == self.internal_inspector1.id:
                self.assertEqual(result.get('first_name'), self.internal_inspector1.first_name)
                self.assertEqual(result.get('last_name'), self.internal_inspector1.last_name)

            if result.get('id') == self.internal_inspector2.id:
                self.assertEqual(result.get('first_name'), self.internal_inspector2.first_name)
                self.assertEqual(result.get('last_name'), self.internal_inspector2.last_name)


class TestCertificationAPI(APITestCase, APITestSetupMixin):
    def setUp(self):
        self.user = self.create_user()
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.product = ProductFactory.create(organization=self.organization)

        self.url = reverse('organization-certifications', kwargs={
            'organization_pk': self.organization.pk
        })

        self.certification_type = CertificationTypeFactory(name="test name", code="test code")
        self.certification = CertificationFactory(
            organization=self.organization, product=self.product, certification_type=self.certification_type
        )
        self.certificate_file = CertificationFileFactory(certification=self.certification, file_type="certificate")
        self.audit_file = CertificationFileFactory(certification=self.certification, file_type="audit_report")

        self.token = self.get_api_token()

    def tearDown(self):
        super(TestCertificationAPI, self).tearDown()
        self.certificate_file.file.delete()
        self.audit_file.file.delete()

    def test_certification_list(self):
        """
        Fetch a list of certifications.
        """
        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {0}'.format(self.token))
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertNotEqual(data, [], msg='A certification list must be non-empty')
        self.assertEqual(len(data), 1, 'A response must have 1 certification.')
        self.assertEqual(data[0]['uuid'], self.certification.uuid, 'UUID must match.')

    def test_certification_create(self):
        """
        Fetch a list of certifications.
        """
        certification_data = {
            "certificate_type": self.certification_type.uuid,
            "certificate_number": self.certification.certificate_number,
            "issue_date": "2016-01-23",
            "expiry_date": "2017-01-23",
            "auditor": "Auditor name",
            "certificate_files": [
                {
                    "uuid": self.certificate_file.uuid,
                    "file": self.certificate_file.file,
                    "original_file_name": "certificate_doc.pdf",
                },
            ],
            "audit_report_files":[
                {
                    "uuid": self.audit_file.uuid,
                    "file": self.audit_file.file,
                    "original_file_name": "audit_report.pdf",
                },
            ],
        }
        response = self.client.post(
            self.url, data=certification_data, HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, "A new record must be created.")


class TestOrganizationAPIs(APITestCase, APITestSetupMixin, AuthAPIMixin):
    def setUp(self):
        self.user = self.create_user()
        self.organization = OrganizationFactory.create(users=(self.user,))

        self.url = reverse('organization', kwargs={
            'organization_pk': self.organization.pk
        })

        self.token = self.get_api_token()

    def test_organization_detail(self):
        """
        Fetch a list of products
        """
        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {0}'.format(self.token))
        _data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        fields = (
            'uuid', 'name', 'slug', 'identification_number', 'description',
            'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_province', 'mailing_postal_code',
            'mailing_country', 'map_zoom', 'email', 'url', 'phone', 'mobile',
            'ggn_number', 'gnl_number', 'evidence_sharing'
        )
        for field in fields:
            self.assertEqual(getattr(self.organization, field), _data[field], "{} field mismatched.".format(field))

    def test_organization_detail_for_auditor(self):
        """
        Fetch a list of products
        """
        organization = OrganizationFactory.create(users=())
        self.url = reverse('organization_detail_for_auditor', kwargs={
            'organization_pk': organization.pk
        })
        response = self.client.get(self.url, HTTP_AUTHORIZATION='Token {0}'.format(self.token))
        _data = response.data

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        fields = (
            'uuid', 'name', 'slug', 'identification_number', 'description',
            'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_province', 'mailing_postal_code',
            'mailing_country', 'map_zoom', 'email', 'url', 'phone', 'mobile',
            'ggn_number', 'gnl_number', 'evidence_sharing'
        )
        for field in fields:
            self.assertEqual(getattr(organization, field), _data[field], "{} field mismatched.".format(field))
