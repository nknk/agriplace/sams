from datetime import datetime
import uuid

from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics, permissions
from rest_framework import response
from rest_framework import schemas
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.generics import ListAPIView, RetrieveUpdateDestroyAPIView, ListCreateAPIView
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework_bulk import ListBulkCreateAPIView
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer

from attachments.api.internal.views import AttachmentPagination
from core.utils import get_area
from organization.api.internal.serializers import CertificationWriteSerializer, OrganizationMembershipSerializer
from organization.management.commands._rvo_utils import get_farm_list, get_rvo_data, get_bedrijfspercelen_request_xml
from organization.mixins import OrganizationMixin
from organization.models import (
    Organization, ProductType,
    PlotRVO, StandardProductType,
    CertificationType, CertificationFile, Certification, CertificationCrop, OrganizationMembership,
    get_organization_type_choices)
from rbac.exceptions import PermissionDenied
from sams.view_mixins import OrganizationRequiredPermission
from translations.helpers import tolerant_ugettext
from .serializers import (
    ProductSerializer, ProductTypeSerializer,
    OrganizationSerializer,
    CertificationSerializer, CertificationTypeShortSerializer)


@api_view()
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer, CoreJSONRenderer])
def schema_view(request):
    """
    Core API schema for Agriplace API
    :param request:
    :return:
    """
    generator = schemas.SchemaGenerator(title='AGRIPLACE API')
    return response.Response(generator.get_schema(request=request))


@api_view(['GET'])
def internal_api_organization_root(request, format=None):
    """
    Root for Organization Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'organizationS': reverse('organizations', request=request, format=format),
        'organization-auditors': reverse('organization-auditors', request=request, format=format),
        'organization-traders': reverse('organization-traders', request=request, format=format),
        'product-types': reverse('product-types', request=request, format=format)
    })


# ---------- BEGIN organization -------------


class BaseOrganizationView(OrganizationMixin, APIView):
    """
    Organization base view

    """
    serializer_class = OrganizationSerializer
    permission_classes = (permissions.IsAuthenticated,)


class OrganizationList(BaseOrganizationView, ListAPIView):
    """
    Generic API view - list of all Organizations

    """

    def get_queryset(self):
        return self.request.user.organizations.all()


class ExternalAuditorListView(BaseOrganizationView, ListAPIView):
    """
    List of all organization auditors in the current assessment

    """
    lookup_url_kwarg = 'assessment_pk'

    def get_queryset(self):
        return Organization.objects.get_external_auditors(
            organization=self.get_organization()
        )


class OrganizationTradersList(BaseOrganizationView, ListAPIView):
    """
    List of all traders in the organization

    """

    def get_queryset(self):
        return Organization.objects.filter(groups__name='buyers', )


class OrganizationDetail(BaseOrganizationView, generics.RetrieveAPIView):
    """
    Detail information of the organization

    """
    lookup_url_kwarg = 'organization_pk'

    def get_queryset(self):
        return self.request.user.organizations.all()


class OrganizationDetailForAuditor(BaseOrganizationView, generics.RetrieveAPIView):
    """
    Detail information of the organization

    """
    lookup_url_kwarg = 'organization_pk'
    queryset = Organization.objects.all()


class PartnerOrganizationListView(OrganizationMixin, ListAPIView):
    """
    I am the primary organization. Get the list of all secondary organizations.
    """
    pagination_class = AttachmentPagination
    serializer_class = OrganizationMembershipSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        items = OrganizationMembership.objects.get_partner_organizations(
            organization=self.get_organization(),
            search_term=self.request.query_params.get('organization_name', ''),
            organization_type_name='producer'  # dependency on ORGANIZATION_TYPE_CHOICES
        )

        choices = get_organization_type_choices()
        return [{
                    'organization_uuid': item[0],
                    'organization_name': item[1],
                    'organization_slug': item[2],
                    'organization_city': item[3],
                    'organization_type': dict(choices).get(item[4], ''),
                    'membership_uuid': item[5]
                } for item in items]

# ---------- END organization -------------

# ---------- BEGIN product -------------


class BaseProductView(APIView):
    """
    Product base view
    """
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated, OrganizationRequiredPermission)

    def get_queryset(self):
        return self.request.organization.products.all()


class ProductListCreateView(BaseProductView, ListBulkCreateAPIView):
    """
        DESCRIPTION: Generic List/Create API view - Lists existing and creates new products and product partners
        VERIFIED: afeef 20.09.2016
        TEST COVERAGE: no
    """

    def perform_create(self, serializer):
        serializer.save(organization=self.request.organization)


class ProductRetrieveUpdateDestroyAPIView(BaseProductView, RetrieveUpdateDestroyAPIView):
    """
        DESCRIPTION: Generic Update/Delete API view - Updates or Deletes products and product partners
        VERIFIED: afeef 20.09.2016
        TEST COVERAGE: no
    """
    lookup_url_kwarg = 'product_pk'


# ---------- END product -------------


class ProductTypeListView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, *args, **kwargs):
        product_types_json = ProductTypeSerializer(ProductType.objects.all(), many=True).data

        result = {
            'productTypes': product_types_json
        }
        return Response(result)


#######################################################################
# Plots
#######################################################################


HISTORY_YEAR_LENGTH = 2


class PlotListView(OrganizationMixin, ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        organization = self.get_organization()
        organization_kvk = organization.identification_number

        current_year = datetime.now().year
        organization_plots_records = PlotRVO.objects.filter(kvk=organization_kvk)

        result = {}
        if organization_plots_records.count():
            result['rvo_data'] = {}

            for delta in range(0, HISTORY_YEAR_LENGTH):
                year_to_search = current_year - delta
                start_of_period = datetime(year_to_search, 1, 1, 1, 1, 0)
                end_of_period = datetime(year_to_search, 12, 31, 23, 59, 59)
                plots_of_year_to_search = organization_plots_records \
                    .exclude(end_date__lt=start_of_period) \
                    .exclude(begin_date__gt=end_of_period)
                plots_field_ids = plots_of_year_to_search. \
                    order_by('field_id'). \
                    values_list('field_id', flat=True). \
                    distinct()

                for field_id in plots_field_ids:
                    freshest_record = plots_of_year_to_search.filter(field_id=field_id).order_by('-created_time')[0]
                    if year_to_search not in result['rvo_data'].keys():
                        result['rvo_data'][year_to_search] = []

                    agriplace_product_type_list = ProductType.get_agriplace_product_types(
                        freshest_record.crop_type_code)
                    if agriplace_product_type_list.count():
                        agriplace_product_type = agriplace_product_type_list[0]
                        product_type_to_show = {
                            'uuid': agriplace_product_type.uuid,
                            'name': tolerant_ugettext(agriplace_product_type.name),
                            'color': agriplace_product_type.field_color
                        }
                    else:
                        standard_product_type_list = StandardProductType.objects.filter(
                            standard_code='CL411', product_type_code=freshest_record.crop_type_code
                        )

                        if standard_product_type_list.count():
                            standard_product_type = standard_product_type_list[0]
                            product_type_to_show = {
                                'uuid': "",
                                'name': tolerant_ugettext(standard_product_type.name) + ' (volgens RVO gewaslijst)',
                                'color': '#C0C0C0'
                            }
                        else:
                            product_type_to_show = {
                                'uuid': '',
                                'name': freshest_record.crop_type_code,
                                'color': '#C0C0C0'
                            }

                    result['rvo_data'][year_to_search].append({
                        'field_id': freshest_record.field_id,
                        'product_type': product_type_to_show,
                        'polygon': freshest_record.get_coordinates(),
                        'area': get_area(freshest_record)
                    })

                    if 'last_modified' not in result or freshest_record.modified_time > result['last_modified']:
                        result['last_modified'] = freshest_record.modified_time

        return Response(result)


def save_rvo_crop_fields(kvk):
    current_year = datetime.now().year
    request_text = get_bedrijfspercelen_request_xml(kvk, str(current_year - 1) + "-12-31", str(current_year) + "-12-31")
    response_result = get_rvo_data(request_text)

    existing_rvo_field_list = PlotRVO.objects.all()

    if response_result['success']:
        farm_list_response = get_farm_list(response_result['success'])

        if not farm_list_response['errors']:

            crop_fields = farm_list_response['success']
            for field in crop_fields:
                existing_rvo_field = existing_rvo_field_list.filter(
                    kvk=kvk,
                    field_id=field['crop_field_id'],
                    begin_date=field['begin_date'],
                    end_date=field['end_date'],
                    crop_type_code=field['crop_type_code'],
                    pos_list=field['border']
                )
                if existing_rvo_field.count() > 0:
                    plot_rvo = existing_rvo_field[0]
                else:
                    plot_rvo = PlotRVO(
                        kvk=kvk,
                        field_id=field['crop_field_id'],
                        begin_date=field['begin_date'],
                        end_date=field['end_date'],
                        crop_type_code=field['crop_type_code'],
                        pos_list=field['border']
                    )
                plot_rvo.save()

            return {'success': {'plots_fetched': len(crop_fields)}, 'errors': []}

        return farm_list_response

    else:
        return response_result


class PlotUpdateView(OrganizationMixin, APIView):
    def post(self, request, *args, **kwargs):
        if not (request.data and request.data['organization_KVK']):
            return status.HTTP_400_BAD_REQUEST
        else:
            organization_kvk = request.data['organization_KVK']
            organization = self.get_organization()
            organization.identification_number = organization_kvk

            organization.save()

            result = save_rvo_crop_fields(organization_kvk)
            if result['success']:
                return Response(result, status.HTTP_200_OK)
            else:
                return Response(result, status.HTTP_400_BAD_REQUEST)


class CertificationListCreateView(ListCreateAPIView):
    """
        DESCRIPTION: Generic List/Create API view - Lists existing and creates new certification
    """
    serializer_class = CertificationSerializer
    permission_classes = (permissions.IsAuthenticated, OrganizationRequiredPermission)

    def get_queryset(self):
        return self.request.organization.certifications.all()

    def perform_create(self, serializer):
        certificate_reports = serializer.validated_data.get('certificate_files', [])
        audit_reports = serializer.validated_data.get('audit_report_files', [])
        crops = serializer.validated_data.get('crops', [])
        certification_type = serializer.validated_data.get('certificate_type')
        certificate_number = serializer.validated_data.get('certificate_number')
        issue_date = serializer.validated_data.get('issue_date')
        expiry_date = serializer.validated_data.get('expiry_date')
        issuer = serializer.validated_data.get('auditor')
        organization = self.request.organization

        certificate = Certification.objects.create(
            organization=organization,
            certification_type=certification_type,
            certificate_number=certificate_number,
            issue_date=issue_date,
            expiry_date=expiry_date,
            issuer=issuer
        )

        for certificate_report in certificate_reports:
            certificate_file = CertificationFile(
                original_file_name=certificate_report.get('original_file_name'),
                file_type='certificate',
                certification=certificate
            )
            file_name = "{}-{}".format(str(uuid.uuid4()), certificate_report.get('original_file_name'))
            certificate_file.file.save(file_name, certificate_report.get('file'))
        for audit_report in audit_reports:
            audit_report_file = CertificationFile(
                original_file_name=audit_report.get('original_file_name'),
                file_type='audit_report',
                certification=certificate
            )
            file_name = "{}-{}".format(str(uuid.uuid4()), audit_report.get('original_file_name'))
            audit_report_file.file.save(file_name, audit_report.get('file'))

        for crop in crops:
            crop = CertificationCrop(
                product_type=crop.get('product_type'),
                area=crop.get('area'),
                certification=certificate
            )
            crop.save()

        return certificate

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        headers = self.get_success_headers(serializer.data)
        certificate = self.perform_create(serializer)
        certificate = Certification.objects.get(pk=certificate.pk)
        return Response(CertificationSerializer(certificate).data, status=status.HTTP_201_CREATED, headers=headers)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CertificationWriteSerializer
        else:
            return super(CertificationListCreateView, self).get_serializer_class()


class CertificationRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    """
    Update/Delete API view - Get or Updates or Deletes products

    """
    lookup_url_kwarg = 'certification_pk'
    serializer_class = CertificationSerializer
    permission_classes = (permissions.IsAuthenticated, OrganizationRequiredPermission)

    def get_queryset(self):
        return self.request.organization.certifications.all()

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return self.serializer_class
        else:
            return CertificationWriteSerializer

    def perform_update(self, serializer):
        certificate_reports = serializer.validated_data.get('certificate_files', [])
        audit_reports = serializer.validated_data.get('audit_report_files', [])
        crops = serializer.validated_data.get('crops', [])
        certification_type = serializer.validated_data.get('certificate_type')
        certificate_number = serializer.validated_data.get('certificate_number')
        issue_date = serializer.validated_data.get('issue_date')
        expiry_date = serializer.validated_data.get('expiry_date')
        issuer = serializer.validated_data.get('auditor')

        certification = self.get_object()

        certification.certification_crops.all().delete()

        certification_files_uuids = []

        for certificate_report in certificate_reports:
            if not certificate_report.get('uuid'):
                certificate_file = CertificationFile(
                    original_file_name=certificate_report.get('original_file_name'),
                    file_type='certificate',
                    certification=certification
                )
                file_name = "{}-{}".format(str(uuid.uuid4()), certificate_report.get('original_file_name'))
                certificate_file.file.save(file_name, certificate_report.get('file'))
                certification_files_uuids.append(certificate_file.uuid)
            else:
                certification_files_uuids.append(certificate_report.get('uuid'))

        for audit_report in audit_reports:
            if not audit_report.get('uuid'):
                audit_report_file = CertificationFile(
                    original_file_name=audit_report.get('original_file_name'),
                    file_type='audit_report',
                    certification=certification
                )
                file_name = "{}-{}".format(str(uuid.uuid4()), audit_report.get('original_file_name'))
                audit_report_file.file.save(file_name, audit_report.get('file'))
                certification_files_uuids.append(audit_report_file.uuid)
            else:
                certification_files_uuids.append(audit_report.get('uuid'))

        certification.certification_files.exclude(uuid__in=certification_files_uuids).delete()

        for crop in crops:
            crop = CertificationCrop(
                product_type=crop.get('product_type'),
                area=crop.get('area'),
                certification=certification
            )
            crop.save()

        certification.certification_type = certification_type
        certification.certificate_number = certificate_number
        certification.issue_date = issue_date
        certification.expiry_date = expiry_date
        certification.issuer = issuer
        certification.save()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        certificate = Certification.objects.get(pk=instance.pk)
        return Response(CertificationSerializer(certificate).data)


class CertificationTypesView(ListAPIView):
    queryset = CertificationType.objects.all()
    serializer_class = CertificationTypeShortSerializer
