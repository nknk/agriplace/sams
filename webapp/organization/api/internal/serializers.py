from translations.helpers import tolerant_ugettext
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from rest_framework_bulk import BulkSerializerMixin
from assessment_workflow.models import AssessmentWorkflow

from assessments.api.internal.serializers import UsedInAssessmentsSerializer
from attachments.fields import FileField
from organization.models import (
    Organization,
    Product,
    ProductType,
    ProductPartner, Certification,
    CertificationType, CertificationCrop, CertificationFile, OrganizationType, OrganizationMembership, DEFAULT_LAYOUT)
from translations.fields import TranslatableTextField


class OrganizationSerializer(ModelSerializer):
    mailing_country = serializers.CharField(source='country_name')

    class Meta:
        model = Organization
        fields = (
            'uuid', 'name', 'slug', 'logo', 'identification_number', 'description',
            'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_province', 'mailing_postal_code',
            'mailing_country', 'map_latitude', 'map_longitude', 'map_zoom', 'email', 'url', 'phone', 'mobile',
            'ggn_number', 'gnl_number', 'theme', 'evidence_sharing'
        )
        read_only_fields = ('uuid',)


class UsedInCertificatesSerializer(ModelSerializer):
    class Meta:
        model = Certification
        fields = (
            'uuid',
            'certificate_number'
        )


class ProductTypeSerializer(ModelSerializer):
    name = TranslatableTextField()

    class Meta:
        model = ProductType
        fields = ('uuid', 'latin_name', 'name', 'field_color')
        read_only_fields = ('uuid',)

    def get_default_fields(self):
        translated_fields = {
            'name': serializers.CharField(),
        }
        return dict(super(ProductTypeSerializer, self).get_default_fields().items() + translated_fields.items())


class PartnerSerializer(ModelSerializer):
    class Meta:
        model = ProductPartner
        fields = (
            'organization_uuid',
            'contract_number'
        )


class ProductSerializer(BulkSerializerMixin, ModelSerializer):
    name = TranslatableTextField(source='product_type_name', read_only=True)
    partners = PartnerSerializer(many=True, required=False)
    used_in_assessments = UsedInAssessmentsSerializer(many=True, required=False, read_only=True)
    used_in_certificates = UsedInCertificatesSerializer(many=True, required=False, read_only=True)

    class Meta:
        model = Product
        fields = (
            'uuid',
            'name',
            'product_type',
            'harvest_year',
            'area',
            'location',
            'comment',
            'partners',
            'used_in_assessments',
            'used_in_certificates',
        )
        read_only_fields = ('uuid',)

    def create(self, validated_data):
        partners_data = validated_data.pop('partners') if 'partners' in validated_data else []
        product = Product.objects.create(**validated_data)

        for partner in partners_data:
            ProductPartner.objects.create(product=product, **partner)

        return product

    def update(self, instance, validated_data):
        product = instance

        if 'partners' in validated_data:
            product.partners.all().delete()
            partners_data = validated_data.pop('partners')

            for partner in partners_data:
                ProductPartner.objects.create(product=product, **partner)

        for attr, value in validated_data.items():
            setattr(product, attr, value)

        product.save()
        return product


class AssessmentProductLinkSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = (
            'uuid',
        )


class CertificationTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CertificationType
        fields = (
            'name',
            'code',
            'verified'
        )
        read_only_fields = ('uuid',)


class CertificationTypeShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = CertificationType
        fields = (
            'uuid',
            'name'
        )


class CertificationCropSerializer(serializers.ModelSerializer):
    total_surface = serializers.FloatField(source="area", required=False)
    name = serializers.CharField(source="product_type.name", required=False)

    class Meta:
        model = CertificationCrop
        fields = (
            'uuid',
            'product_type',
            'name',
            'total_surface'
        )
        read_only_fields = ('uuid',)


class CertificationFileWriteSerializer(serializers.ModelSerializer):
    uuid = serializers.CharField(required=False)
    file = FileField()

    class Meta:
        model = CertificationFile
        fields = (
            'uuid',
            'file',
            'original_file_name'
        )


class CertificationFileSerializer(serializers.ModelSerializer):
    file = serializers.SerializerMethodField('get_file_url')

    class Meta:
        model = CertificationFile
        fields = (
            'uuid',
            'file',
            'original_file_name'
        )

    def get_file_url(self, obj):
        return obj.file.url


class CertificationSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    crops = CertificationCropSerializer(source='certification_crops', many=True)
    certificate_files = serializers.SerializerMethodField()
    audit_report_files = serializers.SerializerMethodField()
    certificate_type = CertificationTypeShortSerializer(source='certification_type')
    auditor = serializers.CharField(source='issuer')

    class Meta:
        model = Certification
        fields = (
            'uuid',
            'certificate_type',
            'certificate_number',
            'issue_date',
            'expiry_date',
            'auditor',
            'crops',
            'certificate_files',
            'audit_report_files'
        )
        read_only_fields = ('uuid',)

    def get_certificate_files(self, obj):
        certificates = obj.certification_files.filter(file_type='certificate')
        return CertificationFileSerializer(certificates, many=True).data

    def get_audit_report_files(self, obj):
        audit_reports = obj.certification_files.filter(file_type='audit_report')
        return CertificationFileSerializer(audit_reports, many=True).data


class CertificationWriteSerializer(serializers.ModelSerializer):
    crops = CertificationCropSerializer(many=True, required=False)
    certificate_files = CertificationFileWriteSerializer(many=True, required=False)
    audit_report_files = CertificationFileWriteSerializer(many=True, required=False)
    certificate_type = serializers.PrimaryKeyRelatedField(queryset=CertificationType.objects.all(), required=True)
    auditor = serializers.CharField(required=False)
    issue_date = serializers.DateField()
    expiry_date = serializers.DateField()

    class Meta:
        model = Certification
        fields = (
            'certificate_type',
            'certificate_number',
            'issue_date',
            'expiry_date',
            'crops',
            'certificate_files',
            'audit_report_files',
            'auditor',
        )


class OrganizationMembershipSerializer(serializers.Serializer):
    organization_uuid = serializers.CharField(max_length=200)
    organization_name = serializers.CharField(max_length=200)
    organization_slug = serializers.CharField(max_length=200)
    organization_city = serializers.CharField(max_length=200)
    organization_type = serializers.CharField(max_length=200)
    membership_uuid = serializers.CharField(max_length=200)

    class Meta:
        fields = (
            'organization_uuid',
            'organization_name',
            'organization_slug',
            'organization_city',
            'organization_type',
            'membership_uuid',
        )

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class OrganizationMembershipModelSerializer(serializers.ModelSerializer):
    membership_uuid = serializers.CharField(max_length=100, read_only=True, source="uuid")

    organization_uuid = serializers.CharField(max_length=100, read_only=True, source="secondary_organization.pk")
    organization_name = serializers.CharField(max_length=100, read_only=True, source="secondary_organization.name")
    organization_slug = serializers.CharField(max_length=100, read_only=True, source="secondary_organization.slug")
    organization_type = serializers.CharField(max_length=100, read_only=True,
                                              source="organization_type.get_name_display")
    organization_city = serializers.CharField(max_length=100, read_only=True,
                                              source="secondary_organization.mailing_city")
    identification_number = serializers.CharField(max_length=100, read_only=True, source="membership_number")

    class Meta:
        model = OrganizationMembership
        fields = (
            'membership_uuid',
            'organization_uuid',
            'organization_name',
            'organization_slug',
            'organization_type',
            'organization_city',
            'identification_number'
        )

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class OrganizationMembershipMobileSerializer(serializers.ModelSerializer):
    membership_uuid = serializers.CharField(max_length=36, read_only=True, source="uuid")
    secondary_organization_uuid = serializers.CharField(max_length=1000, read_only=True,
                                                        source="secondary_organization.uuid")
    secondary_organization = serializers.CharField(max_length=1000, read_only=True,
                                                   source="secondary_organization.name")
    primary_organization_uuid = serializers.CharField(max_length=1000, read_only=True,
                                                      source="primary_organization.uuid")
    primary_organization = serializers.CharField(max_length=1000, read_only=True, source="primary_organization.name")
    role = serializers.CharField(max_length=80, read_only=True, source="role.name")
    theme = serializers.SerializerMethodField()

    class Meta:
        model = OrganizationMembership
        fields = (
            "membership_uuid",
            "secondary_organization_uuid",
            "secondary_organization",
            "primary_organization_uuid",
            "primary_organization",
            "role",
            "theme"
        )

    def get_theme(self, obj):
        theme = obj.primary_organization.theme
        layout = theme.layout if theme else DEFAULT_LAYOUT
        return {
            "name": theme.name if theme else "",
            "slug": theme.slug if theme else "",
            "description": theme.description if theme else "",
            "layout": layout
        }


class DashboardAssessmentSerializer(serializers.ModelSerializer):
    workflow_uuid = serializers.CharField(max_length=100, read_only=True, source="uuid")
    preferred_month = serializers.CharField(max_length=100, read_only=True, source="audit_preferred_month")
    organization_name = serializers.CharField(max_length=100, read_only=True, source="author_organization.name")
    organization_uuid = serializers.CharField(max_length=100, read_only=True, source="author_organization.uuid")
    membership_number = serializers.CharField(
        max_length=100, read_only=True, source="membership.membership_number"
    )
    assessment = serializers.SerializerMethodField()
    organization_url = serializers.SerializerMethodField()
    auditor = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = AssessmentWorkflow
        fields = (
            'workflow_uuid',
            'assessment',
            'audit_date_planned',
            'audit_date_actual',
            'preferred_month',
            'organization_name',
            'organization_url',
            'auditor',
            'permissions',
            'organization_uuid',
            'membership_number'
        )

    def get_assessment(self, obj):
        assessment = obj.assessment
        assessment_data = {
            'status': obj.get_assessment_status_display(),
            'type': obj.assessment_type.code,
            u'label': tolerant_ugettext(obj.assessment_type.name)
        }
        if assessment:
            assessment_data.update({
                'uuid': assessment.uuid,
                'url': ("/{}/our/assessments/{}/detail".format(self.context['request'].membership.pk, assessment.uuid)),
            })
        else:
            assessment_data.update({
                'uuid': "",
                'url': "#",
            })
        return assessment_data

    def get_organization_url(self, obj):
        return "#"

    def get_permissions(self, obj):
        state_permission_dict = self.context['state_permission_dict']
        return state_permission_dict.get(obj.assessment_status, [])

    def get_auditor(self, obj):
        if obj.auditor_user:
            return "{}".format(obj.auditor_user.id)
