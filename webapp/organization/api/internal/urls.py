from django.conf.urls import patterns, url

from farm_group.api.internal.views import OrganizationAuditorListView, DashboardView
from organization.api.internal.views import (
    ProductRetrieveUpdateDestroyAPIView, ProductListCreateView, OrganizationList, OrganizationTradersList,
    OrganizationDetail, ProductTypeListView, PlotListView, PlotUpdateView, CertificationTypesView,
    CertificationRetrieveUpdateDestroyAPIView, CertificationListCreateView,
    PartnerOrganizationListView,
    OrganizationDetailForAuditor)

urlpatterns = patterns(
    'organization.api.internal.views',
    url(
        r'^certificate-types/',
        CertificationTypesView.as_view(),
        name='certificate-types'
    ),
    url(
        r'^(?P<organization_pk>.\w+)/dashboard/$',
        DashboardView.as_view(),
        name='dashboard'
    ),
    url(
        r'(?P<organization_pk>.\w+)/partners/$',
        PartnerOrganizationListView.as_view(),
        name='organization-partners'
    ),
    url(
        r'(?P<organization_pk>.\w+)/certifications/(?P<certification_pk>.+)/$',
        CertificationRetrieveUpdateDestroyAPIView.as_view(),
        name='organization-certification'
    ),
    url(
        r'(?P<organization_pk>.\w+)/certifications/$',
        CertificationListCreateView.as_view(),
        name='organization-certifications'
    ),
    url(
        r'(?P<organization_pk>.\w+)/products/(?P<product_pk>.+)/$',
        ProductRetrieveUpdateDestroyAPIView.as_view(),
        name='organization-product'
    ),
    url(
        r'(?P<organization_pk>.\w+)/products/$',
        ProductListCreateView.as_view(),
        name='organization-products'
    ),

    # moved to Assessment API (not yet, later comment this)
    url(
        r'^product-types/$',
        ProductTypeListView.as_view(),
        name='product-types'
    ),
    url(
        r'(?P<organization_pk>.\w+)/plots(?:/(?P<year>\d{4}))?/$',
        PlotListView.as_view(),
        name='plot_list'
    ),
    url(
        r'(?P<organization_pk>.\w+)/plots/fetch/rvo/$',
        PlotUpdateView.as_view(),
        name='rvo_plot_fetch'
    ),
    url(
        r'(?P<organization_pk>.\w+)/auditors/$',
        OrganizationAuditorListView.as_view(),
        name='organization-auditors'
    ),
    url(
        r'traders/$',
        OrganizationTradersList.as_view(),
        name='organization-traders'
    ),
    url(
        r'^api-root/$',
        'internal_api_organization_root',
        name='organization-app-api-list'
    ),
    url(
        r'(?P<organization_pk>.\w+)/detail-for-auditor$',
        OrganizationDetailForAuditor.as_view(),
        name='organization_detail_for_auditor'
    ),
    url(
        r'(?P<organization_pk>.\w+)/$',
        OrganizationDetail.as_view(),
        name='organization'
    ),
    url(
        r'^$',
        OrganizationList.as_view(),
        name='organizations'
    ),
)
