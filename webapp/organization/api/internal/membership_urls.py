from django.conf.urls import patterns, url

from farm_group.api.internal.membership_views import OrganizationAuditorListView, DashboardView

urlpatterns = patterns(
    'organization.api.internal.views',
    url(
        r'^(?P<membership_pk>.+)/dashboard/$',
        DashboardView.as_view(),
        name='dashoard'
    ),
    url(
        r'(?P<membership_pk>.+)/auditors/$',
        OrganizationAuditorListView.as_view(),
        name='organization-auditors'
    ),
)
