from django.core.urlresolvers import reverse
from django.utils import translation
from django.utils.translation import ugettext as _
from organization.models import LAYOUT_CHOICES, DEFAULT_LAYOUT

from sams.views import ICONS, get_org_context


def get_organization_context(request, kwargs, page_name=""):
    """
    Base context objects for organization module
    Expects `organization_slug` in all URLs
    """
    context, organization = get_org_context(request, kwargs, page_name)
    theme = request.membership.primary_organization.theme
    evidence_sharing = request.membership.primary_organization.evidence_sharing
    layout = theme.layout if theme else DEFAULT_LAYOUT
    theme_dict = {
        "name": theme.name if theme else "",
        "slug": theme.slug if theme else "",
        "description": theme.description if theme else "",
        "layout": layout
    }

    context.update({
        'layout_name': layout,
        'theme': theme_dict,
        'evidence_sharing': evidence_sharing,
        'layout_org_tpl': 'core/{}'.format(dict(LAYOUT_CHOICES)[layout]),
        'nav_links': get_organization_nav_links(request.membership.pk),
        'page_triggers': ['organization', page_name],
        'page_title': organization,
        'page_title_secondary': _("organization"),
        'breadcrumbs': []
    })
    language = getattr(request, 'QUERY_PARAMS', {}).get('language', None)
    if language:
        translation.activate(language)
    return context, organization


def get_organization_nav_links(membership_pk):
    """
    Navigation links for organization module menu

    List of nav links organized in categories
    Structure:
    [
      {
          text: category label
          links: {
              name: link name (used for highlighting current page
              text: link text
              url: link url
          }
      }
    ]
    """
    nav_links = [
        # category
        {
            "text": '',
            "links": [
                # link
                {
                    "name": "my_org",
                    "text": _("Profile"),
                    'icon': ICONS['organization'],
                    'id': 'overview-org-link',
                    'url': reverse("my_org_overview", args=[membership_pk]),
                },
                {  # link
                    'name': 'my_org_detail',
                    'text': _("Organization details"),
                    'icon': ICONS['edit'],
                    'id': 'org-details-org-link',
                    'url': reverse('my_org_form_full', args=[membership_pk]),
                    'html_class': ''
                },
            ]
        },
        {
            'text': '',
            'links': [
                {  # link
                    'html_class': '',
                    'icon': ICONS.get('product'),
                    'id': 'products-org-link',
                    'name': 'my_product_list',
                    'text': _("Crops"),
                    'url': reverse('my_product_list', args=[membership_pk]),
                },
                {  # Certifications
                    "name": "my_certification_list",
                    "text": _("Certificates"),
                    "url": reverse("my_certification_list", args=[membership_pk]),
                    "icon": ICONS.get('certification'),
                    'id': 'certificates-org-link',
                    "html_class": ""
                },
            ]
        },
    ]
    return nav_links
