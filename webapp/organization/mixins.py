from rest_framework.exceptions import PermissionDenied

from organization.models import Organization
from sams.helpers import get_object_for_user


class UserMixin(object):
    def get_object_for_user(self, cls, action='write', **kwargs):
        obj = cls.objects.get(**kwargs)

        if not getattr(obj, "check_permissions", None):
            raise Exception("Model %s doesn't implement check_permissions()" % obj)
        if not obj.check_permissions(self.request.user, action):
            raise PermissionDenied("Permission denied")
        return obj


class OrganizationMixin(UserMixin, object):
    def get_organization(self, slug=''):
        """
        Retrieves organization object based on `organization_pk` URL parameter
        """
        if slug:
            organization = self.get_object_for_user(
                cls=Organization,
                slug=slug
            )
        else:
            organization = self.get_object_for_user(
                cls=Organization,
                pk=self.kwargs.get('organization_pk')
            )

        if organization not in self.request.user.organizations.all():
            raise PermissionDenied("User does not have access to organization.")
        else:
            return organization

    @classmethod
    def get_organization_from_param(cls, user, kwargs):
        """
        Get base context for current organization. Expects `organization_slug` in all URLs
        Args:
            user(User): Instance of currently logged in user
            kwargs(Dict): keyword arguments containing 'organization_slug' in current request.

        Returns:
            Organization instance
        """
        if not kwargs.get('organization_slug'):
            raise Exception('No organization context has been specified')

        organization = get_object_for_user(
            user=user,
            object_class=Organization,
            slug=kwargs.get('organization_slug')
        )

        return organization
