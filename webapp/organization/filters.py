from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models.aggregates import Count

from assessment_workflow.models import AssessmentWorkflow
from assessments.models import PLATFORM_CHOICES, AssessmentType, HARVEST_YEAR_CHOICES
from organization.models import OrganizationMembership, Organization


class HarvestYearFilter(admin.SimpleListFilter):
    title = 'Harvest year'
    parameter_name = 'harvest_year'

    def lookups(self, request, model_admin):
        return HARVEST_YEAR_CHOICES

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(assessment_workflow_organization__harvest_year=int(self.value()))
        else:
            return queryset


class AssessmentWorkFlowFilter(admin.SimpleListFilter):
    title = 'Workflow'
    parameter_name = 'has_assessment_workflow'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """

        return (
            ('without', 'Without any'),
            ('with', 'With one or more')

        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value():
            if self.value() == 'with':
                return queryset.annotate(workflow_count=Count('assessment_workflow_organization')).filter(workflow_count__gt=0)
            else:
                return queryset.annotate(workflow_count=Count('assessment_workflow_organization')).filter(workflow_count=0)
        else:
            return queryset


class AssessmentTypeFilter(admin.SimpleListFilter):
    title = 'Assessment Type'
    parameter_name = 'assessment_type'

    def lookups(self, request, model_admin):
        if get_user_model().objects.filter(pk=request.user.pk, groups__name='abp_manager').exists():
            sisters = OrganizationMembership.get_sisters_by_user(request.user)
            assessment_types = AssessmentWorkflow.objects.filter(
                author_organization__in=sisters,
                assessment_type__kind='assessment'
            ).distinct().values_list('assessment_type__uuid', 'assessment_type__name').order_by('assessment_type__name')
            return assessment_types
        else:
            return AssessmentType.objects.filter(kind='assessment').values_list('uuid', 'name').order_by('name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(assessment_workflow_organization__assessment_type=self.value())
        else:
            return queryset


class MembershipRoleFilter(admin.SimpleListFilter):
    title = 'Membership role'
    parameter_name = 'role'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """

        current_user = request.user
        groups = Group.objects.exclude(role_organization__isnull=True)

        if get_user_model().objects.filter(pk=current_user.pk, groups__name='abp_manager').exists():
            primary_organization_uuid_list = OrganizationMembership.objects.filter(
                secondary_organization__in=current_user.organizations.all()
            ).values_list('primary_organization__uuid', flat=True).distinct()
            roles = OrganizationMembership.objects.filter(primary_organization__pk__in=primary_organization_uuid_list).values_list('role__id', flat=True).distinct()
            groups = groups.filter(pk__in=roles)

        return [(role.id, role.name) for role in groups]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value():
            secondary_organizations = OrganizationMembership.objects.filter(
                role__pk=self.value()).values_list('secondary_organization', flat=True).distinct()
            return queryset.filter(uuid__in=secondary_organizations)
        else:
            return queryset


class FarmgroupFilter(admin.SimpleListFilter):
    title = 'Farm group'
    parameter_name = 'farm_group'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        current_user = request.user
        if get_user_model().objects.filter(pk=current_user.pk, groups__name='abp_manager').exists():
            # TODO ------ manager methods --------
            primary_organization_uuid_list = OrganizationMembership.objects.filter(
                secondary_organization__in=current_user.organizations.all()
            ).values_list('primary_organization__uuid', flat=True).distinct()
            # ------------------------------------
            return [(org.uuid, org.name) for org in Organization.objects.filter(uuid__in=primary_organization_uuid_list)]
        else:
            return OrganizationMembership.objects.select_related('primary_organization').values_list(
                'primary_organization__uuid', 'primary_organization__name').distinct()

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            secondary_organizations = OrganizationMembership.objects.filter(
                primary_organization=self.value()).values_list('secondary_organization', flat=True).distinct()
            return queryset.filter(uuid__in=secondary_organizations)
        else:
            return queryset
