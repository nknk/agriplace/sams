# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0024_auto_20170524_1051'),
        ('organization', '0024_certificationfile_original_file_name'),
    ]

    operations = [
    ]
