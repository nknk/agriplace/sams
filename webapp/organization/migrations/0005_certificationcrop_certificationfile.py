# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0004_auto_20160816_1427'),
    ]

    operations = [
        migrations.CreateModel(
            name='CertificationCrop',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(default=b'?', max_length=1000, verbose_name='Name', blank=True)),
                ('area', models.FloatField(null=True, verbose_name='Area in hectares', blank=True)),
                ('certification', models.ForeignKey(related_name='certification_crops', verbose_name='Certification', blank=True, to='organization.Certification', null=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CertificationFile',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('file', models.FileField(max_length=255, upload_to=b'organization/certification/files')),
                ('file_type', models.CharField(default=b'', max_length=50, verbose_name='Certification file type', blank=True, choices=[(b'certificate', 'Certificate'), (b'audit_report', 'Audit report')])),
                ('certification', models.ForeignKey(related_name='certification_files', verbose_name='Certification', blank=True, to='organization.Certification')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
            },
        ),
    ]
