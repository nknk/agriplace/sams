# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0005_auto_20161103_1130'),
        ('organization', '0006_auto_20161026_1135'),
    ]

    operations = [
    ]
