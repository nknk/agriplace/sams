# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0014_organizationmembership_role'),
        ('organization', '0014_auto_20170103_0703'),
        ('organization', '0014_organization_gui_selection'),
    ]

    operations = [
    ]
