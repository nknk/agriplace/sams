# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0026_auto_20170815_1442'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='license_end_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='organization',
            name='license_start_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
