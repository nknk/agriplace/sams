# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def migrate_certification_crop(apps, schema_editor):
    """Migrate the crop from """

    Certification = apps.get_model("organization", "Certification")
    CertificationCrop = apps.get_model("organization", "CertificationCrop")

    CertificationCrop.objects.all().delete()

    certifications = Certification.objects.all()
    for certification in certifications:
        product = certification.product
        if product:
            CertificationCrop.objects.create(
                product_type=certification.product.product_type,
                area=certification.product.area,
                certification=certification
            )


def revert_certification_crop(apps, schema_editor):
    """Backward migration is not needed."""
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0010_auto_20161116_0854'),
    ]

    operations = [
        migrations.RunPython(migrate_certification_crop, revert_certification_crop)
    ]
