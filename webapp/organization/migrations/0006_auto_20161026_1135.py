# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from django.template.defaultfilters import truncatechars


def get_product_type_name(product):
    if product.remarks:
        product_type_name = "{type} ({remarks})".format(
            type=product.product_type,
            remarks=truncatechars(product.remarks, 64)
        )
    else:
        product_type_name = product.product_type.name
    return product_type_name


def migrate_certification_crop(apps, schema_editor):
    """Migrate the crop from """
    
    Certification = apps.get_model('organization', 'Certification')
    CertificationCrop = apps.get_model('organization', 'CertificationCrop')
    
    certifications = Certification.objects.all().prefetch_related('product')
    for certification in certifications:
        product = certification.product
        if product and product.product_type:
            name = get_product_type_name(certification.product)
            area = certification.product.area
            CertificationCrop.objects.create(
                name=name,
                area=area,
                certification=certification
            )


def revert_certification_crop(apps, schema_editor):
    """Backward migration is not needed."""
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0005_certificationcrop_certificationfile'),
    ]

    operations = [
        migrations.RunPython(migrate_certification_crop, revert_certification_crop)
    ]
