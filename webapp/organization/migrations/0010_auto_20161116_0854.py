# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0009_auto_20161114_1054'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='certificationcrop',
            name='name',
        ),
        migrations.AddField(
            model_name='certificationcrop',
            name='product_type',
            field=models.ForeignKey(to='organization.ProductType', null=True),
        ),
    ]
