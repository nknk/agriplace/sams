# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0026_auto_20170815_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='theme',
            field=models.ForeignKey(related_name='themes', blank=True, to='organization.OrganizationTheme', null=True),
        ),
    ]
