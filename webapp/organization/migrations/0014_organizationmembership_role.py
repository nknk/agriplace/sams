# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0013_auto_20161206_0834'),
    ]

    operations = [
        migrations.AddField(
            model_name='organizationmembership',
            name='role',
            field=models.ForeignKey(related_name='role_organization', verbose_name='role', blank=True, to='auth.Group', null=True),
        ),
    ]
