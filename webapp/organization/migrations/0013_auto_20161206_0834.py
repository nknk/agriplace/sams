# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0012_auto_20161130_0518'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizationmembership',
            name='membership_number',
            field=models.CharField(max_length=50, null=True, verbose_name='Membership number', blank=True),
        ),
    ]
