# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0015_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='gui_selection',
            field=models.CharField(choices=[(b'hzpc', b'HZPC'), (b'agriplace_white_label', b'Agriplace_White_Label'), (b'agriplace_group_white_label', b'Agriplace_Group_White_label')], max_length=100, blank=True, help_text=b'The default GUI layout to be used.', null=True, verbose_name=b'GUI Selection'),
        ),
    ]
