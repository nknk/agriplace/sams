# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0021_auto_20170123_1425'),
        ('organization', '0021_organization_evidence_sharing'),
    ]

    operations = [
    ]
