# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0023_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='certificationfile',
            name='original_file_name',
            field=models.CharField(max_length=255, null=True, verbose_name='Original file name', blank=True),
        ),
    ]
