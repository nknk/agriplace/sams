# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0025_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='identification_number',
            field=models.CharField(max_length=50, null=True, verbose_name='Chamber of Commerce #', blank=True),
        ),
    ]
