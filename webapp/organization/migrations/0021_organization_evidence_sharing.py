# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0020_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='evidence_sharing',
            field=models.BooleanField(default=False, verbose_name='Evidence sharing'),
        ),
    ]
