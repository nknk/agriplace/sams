# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0007_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='certification',
            name='certificate_number',
            field=models.CharField(max_length=1000, null=True, verbose_name='Certificate number', blank=True),
        ),
    ]
