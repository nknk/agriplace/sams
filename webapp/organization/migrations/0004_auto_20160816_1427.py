# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0003_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='certification',
            options={'ordering': ('uuid',), 'permissions': (('view_certification', 'Can view certification'),)},
        ),
        migrations.AlterModelOptions(
            name='certificationtype',
            options={'ordering': ('uuid',), 'permissions': (('view_certificationtype', 'Can view certification type'),)},
        ),
        migrations.AlterModelOptions(
            name='organization',
            options={'ordering': ['name'], 'permissions': (('view_organization', 'Can view organization'),)},
        ),
        migrations.AlterModelOptions(
            name='organizationreport',
            options={'ordering': ('uuid',), 'permissions': (('view_organizationreport', 'Can view organization report'),)},
        ),
        migrations.AlterModelOptions(
            name='plotrvo',
            options={'ordering': ('uuid',), 'permissions': (('view_plotrvo', 'Can view plot rvo'),)},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['harvest_year', 'name'], 'permissions': (('view_product', 'Can view product'),)},
        ),
        migrations.AlterModelOptions(
            name='producttype',
            options={'ordering': ('uuid',), 'permissions': (('view_producttype', 'Can view product type'),)},
        ),
        migrations.AlterModelOptions(
            name='standardproducttype',
            options={'ordering': ('uuid',), 'permissions': (('view_standardproducttype', 'Can view standard product type'),)},
        ),
    ]
