# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import ckeditor.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0011_auto_20161116_0854'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrganizationType',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False,
                                                                    max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=1000, verbose_name='Type name',
                                          choices=[(b'auditor', 'Auditor'), (b'buyer', 'Buyer'),
                                                   (b'licensee', 'Licensee / Brand owner'), (b'other', 'Other'),
                                                   (b'processor', 'Processor / Manufacturer'),
                                                   (b'coop', 'Producer group / Cooperative'),
                                                   (b'producer', 'Producer/Farmer'), (b'trader', 'Trader')])),
                ('slug', models.SlugField(unique=True, max_length=1000, verbose_name='Slug name')),
                ('description', ckeditor.fields.RichTextField(default=b'', verbose_name='Description', blank=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'OrganizationType',
            },
        ),
        migrations.CreateModel(
            name='OrganizationMembership',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('membership_number', models.CharField(unique=True, null=True, blank=True, max_length=50, verbose_name='Membership number')),
                ('primary_organization', models.ForeignKey(related_name='primary_organization', to='organization.Organization')),
                ('secondary_organization', models.ForeignKey(related_name='secondary_organization', to='organization.Organization')),
                ('organization_type', models.ForeignKey(related_name='organization_type', to='organization.OrganizationType')),
            ],
            options={
                'db_table': 'OrganizationMembership',
            },
        ),
        migrations.AddField(
            model_name='organization',
            name='memberships',
            field=models.ManyToManyField(to='organization.Organization', verbose_name='Memberships', through='organization.OrganizationMembership', blank=True),
        ),
    ]
