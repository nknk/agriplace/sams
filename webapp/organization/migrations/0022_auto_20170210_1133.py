# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0021_auto_20170123_1425'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='groups',
            field=models.ManyToManyField(help_text=b"Please don't use the 'Groups' (to add for instance external auditor\n                                    role). You have to add groups in the OrganizationMembership model.", related_name='organizations', verbose_name='Groups', to='auth.Group', blank=True),
        ),
    ]
