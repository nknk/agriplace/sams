# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0013_auto_20161206_0834'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='harvest_year',
            field=models.IntegerField(default=2017, verbose_name='Harvest year', choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018)]),
        ),
    ]
