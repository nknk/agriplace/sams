# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0017_auto_20170110_0845'),
    ]

    operations = [
        migrations.AddField(
            model_name='organizationtheme',
            name='layout',
            field=models.CharField(max_length=50, unique=True, null=True, verbose_name='Layout', choices=[(b'hzpc', b'farm_group/layout_org.html'), (b'agriplace', b'default/layout_org.html'), (b'farm_group', b'default/layout_org.html')]),
        ),
    ]
