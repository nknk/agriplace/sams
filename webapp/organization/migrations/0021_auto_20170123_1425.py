# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0020_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizationtype',
            name='name',
            field=models.CharField(max_length=1000, verbose_name='Type name', choices=[(b'auditor', 'Auditor'), (b'buyer', 'Buyer'), (b'internal_auditor', 'Internal Auditor'), (b'internal_inspector', 'Internal Inspector'), (b'licensee', 'Licensee / Brand owner'), (b'other', 'Other'), (b'processor', 'Processor / Manufacturer'), (b'coop', 'Producer group / Cooperative'), (b'producer', 'Producer/Farmer'), (b'trader', 'Trader')]),
        ),
    ]
