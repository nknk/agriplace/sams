# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0013_auto_20161206_0834'),
    ]

    operations = [
        migrations.AddField(
            model_name='organization',
            name='gui_selection',
            field=models.CharField(choices=[(b'hzpc', b'HZPC')], max_length=100, blank=True, help_text=b'The default GUI layout to be used.', null=True, verbose_name=b'GUI Selection'),
        ),
    ]
