# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizationaccessinvitation',
            name='email',
            field=models.EmailField(max_length=254),
        ),
        migrations.AlterField(
            model_name='product',
            name='harvest_year',
            field=models.IntegerField(default=2016, verbose_name='Harvest year', choices=[(2015, 2015), (2016, 2016), (2017, 2017)]),
        ),
        migrations.AlterField(
            model_name='producttype',
            name='questionnaires',
            field=models.ManyToManyField(related_name='product_types', verbose_name='questionnaires', to='assessments.Questionnaire', blank=True),
        ),
    ]
