# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0023_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organization',
            name='parent_organization',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='producer_number',
        ),
        migrations.RemoveField(
            model_name='organization',
            name='type',
        ),
    ]
