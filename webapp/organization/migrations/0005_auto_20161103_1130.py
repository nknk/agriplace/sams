# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0004_auto_20160816_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='organization',
            name='reference_number',
        ),
        migrations.AddField(
            model_name='organization',
            name='gnl_number',
            field=models.CharField(max_length=50, null=True, verbose_name='GNL', blank=True),
        ),
        migrations.AlterField(
            model_name='organization',
            name='producer_number',
            field=models.CharField(max_length=50, null=True, verbose_name='MEMBERSHIP#', blank=True),
        ),
    ]
