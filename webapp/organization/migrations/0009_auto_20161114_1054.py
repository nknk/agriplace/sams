# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0008_auto_20161114_1018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='certification',
            name='issuer',
            field=models.CharField(max_length=1024, null=True, verbose_name='Issuer', blank=True),
        ),
    ]
