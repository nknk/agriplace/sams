# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0018_organizationtheme_layout'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizationtheme',
            name='layout',
            field=models.CharField(max_length=50, unique=True, null=True, verbose_name='Layout', choices=[(b'agriplace', b'layouts/agriplace.html'), (b'farm_group', b'layouts/farm_group.html'), (b'hzpc', b'layouts/hzpc.html')]),
        ),
    ]
