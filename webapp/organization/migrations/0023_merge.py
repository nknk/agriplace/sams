# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0022_auto_20170210_1133'),
        ('organization', '0022_merge'),
    ]

    operations = [
    ]
