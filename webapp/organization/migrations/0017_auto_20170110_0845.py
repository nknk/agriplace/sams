# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import ckeditor.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0016_auto_20170110_0752'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrganizationTheme',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(default=b'Agriplace White Label', unique=True, max_length=50, verbose_name='Name')),
                ('slug', models.SlugField(unique=True, max_length=100, verbose_name='Slug')),
                ('description', ckeditor.fields.RichTextField(verbose_name='Description', blank=True)),
            ],
            options={
                'db_table': 'OrganizationTheme',
            },
        ),
        migrations.RemoveField(
            model_name='organization',
            name='gui_selection',
        ),
        migrations.AddField(
            model_name='organization',
            name='theme',
            field=models.ForeignKey(related_name='themes', to='organization.OrganizationTheme', null=True),
        ),
    ]
