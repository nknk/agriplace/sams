# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import ckeditor.fields
from django.conf import settings
import tagging_autocomplete.models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Certification',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('certificate_number', models.CharField(max_length=1000, verbose_name='Certificate number', blank=True)),
                ('issue_date', models.DateField(null=True, verbose_name='Issue date', blank=True)),
                ('expiry_date', models.DateField(null=True, verbose_name='Expiry date', blank=True)),
                ('issuer', models.CharField(default=b'', max_length=1024, verbose_name='Issuer', blank=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'Certification',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CertificationType',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(default=b'', max_length=1024, blank=True)),
                ('code', models.CharField(default=b'', max_length=255, blank=True)),
                ('verified', models.BooleanField(default=False, help_text=b'Has this certification been verified by P4E?')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'CertificationType',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=1000, verbose_name='organization name')),
                ('slug', models.SlugField(help_text='http://agriplace.com/[organization short name]', unique=True, max_length=1000, verbose_name='URL name')),
                ('logo', models.ImageField(upload_to=b'organization-logos', null=True, verbose_name='logo', blank=True)),
                ('identification_number', models.CharField(max_length=50, null=True, verbose_name='KvK number', blank=True)),
                ('type', models.CharField(max_length=1000, verbose_name='type', choices=[(b'auditor', 'Auditor'), (b'buyer', 'Buyer'), (b'licensee', 'Licensee / Brand owner'), (b'other', 'Other'), (b'processor', 'Processor / Manufacturer'), (b'coop', 'Producer group / Cooperative'), (b'producer', 'Producer/Farmer'), (b'trader', 'Trader')])),
                ('description', models.TextField(verbose_name='description', blank=True)),
                ('mailing_address1', models.CharField(max_length=1000, verbose_name='address (line 1)', blank=True)),
                ('mailing_address2', models.CharField(max_length=1000, verbose_name='Address (line 2)', blank=True)),
                ('mailing_city', models.CharField(max_length=1000, verbose_name='City', blank=True)),
                ('mailing_province', models.CharField(max_length=1000, verbose_name='Province', blank=True)),
                ('mailing_postal_code', models.CharField(max_length=1000, verbose_name='Postal code', blank=True)),
                ('mailing_country', models.CharField(default=b'NL', max_length=1000, verbose_name='Country', blank=True, choices=[(b'AF', 'Afghanistan'), (b'AX', 'Aland Islands'), (b'AL', 'Albania'), (b'DZ', 'Algeria'), (b'AS', 'American Samoa'), (b'AD', 'Andorra'), (b'AO', 'Angola'), (b'AI', 'Anguilla'), (b'AQ', 'Antarctica'), (b'AG', 'Antigua and Barbuda'), (b'AR', 'Argentina'), (b'AM', 'Armenia'), (b'AW', 'Aruba'), (b'AU', 'Australia'), (b'AT', 'Austria'), (b'AZ', 'Azerbaijan'), (b'BS', 'Bahamas'), (b'BH', 'Bahrain'), (b'BD', 'Bangladesh'), (b'BB', 'Barbados'), (b'BY', 'Belarus'), (b'BE', 'Belgium'), (b'BZ', 'Belize'), (b'BJ', 'Benin'), (b'BM', 'Bermuda'), (b'BT', 'Bhutan'), (b'BO', 'Bolivia'), (b'BA', 'Bosnia and Herzegovina'), (b'BW', 'Botswana'), (b'BV', 'Bouvet Island'), (b'BR', 'Brazil'), (b'IO', 'British Indian Ocean Territory'), (b'BN', 'Brunei Darussalam'), (b'BG', 'Bulgaria'), (b'BF', 'Burkina Faso'), (b'BI', 'Burundi'), (b'KH', 'Cambodia'), (b'CM', 'Cameroon'), (b'CA', 'Canada'), (b'CV', 'Cape Verde'), (b'KY', 'Cayman Islands'), (b'CF', 'Central African Republic'), (b'TD', 'Chad'), (b'CL', 'Chile'), (b'CN', 'China'), (b'CX', 'Christmas Island'), (b'CC', 'Cocos (Keeling) Islands'), (b'CO', 'Colombia'), (b'KM', 'Comoros'), (b'CG', 'Congo'), (b'CD', 'Congo, The Democratic Republic of the'), (b'CK', 'Cook Islands'), (b'CR', 'Costa Rica'), (b'CI', "Cote d'Ivoire"), (b'HR', 'Croatia'), (b'CU', 'Cuba'), (b'CY', 'Cyprus'), (b'CZ', 'Czech Republic'), (b'DK', 'Denmark'), (b'DJ', 'Djibouti'), (b'DM', 'Dominica'), (b'DO', 'Dominican Republic'), (b'EC', 'Ecuador'), (b'EG', 'Egypt'), (b'SV', 'El Salvador'), (b'GQ', 'Equatorial Guinea'), (b'ER', 'Eritrea'), (b'EE', 'Estonia'), (b'ET', 'Ethiopia'), (b'FK', 'Falkland Islands (Malvinas)'), (b'FO', 'Faroe Islands'), (b'FJ', 'Fiji'), (b'FI', 'Finland'), (b'FR', 'France'), (b'GF', 'French Guiana'), (b'PF', 'French Polynesia'), (b'TF', 'French Southern Territories'), (b'GA', 'Gabon'), (b'GM', 'Gambia'), (b'GE', 'Georgia'), (b'DE', 'Germany'), (b'GH', 'Ghana'), (b'GI', 'Gibraltar'), (b'GR', 'Greece'), (b'GL', 'Greenland'), (b'GD', 'Grenada'), (b'GP', 'Guadeloupe'), (b'GU', 'Guam'), (b'GT', 'Guatemala'), (b'GG', 'Guernsey'), (b'GN', 'Guinea'), (b'GW', 'Guinea-Bissau'), (b'GY', 'Guyana'), (b'HT', 'Haiti'), (b'HM', 'Heard Island and McDonald Islands'), (b'VA', 'Holy See (Vatican City State)'), (b'HN', 'Honduras'), (b'HK', 'Hong Kong'), (b'HU', 'Hungary'), (b'IS', 'Iceland'), (b'IN', 'India'), (b'ID', 'Indonesia'), (b'IR', 'Iran, Islamic Republic of'), (b'IQ', 'Iraq'), (b'IE', 'Ireland'), (b'IM', 'Isle of Man'), (b'IL', 'Israel'), (b'IT', 'Italy'), (b'JM', 'Jamaica'), (b'JP', 'Japan'), (b'JE', 'Jersey'), (b'JO', 'Jordan'), (b'KZ', 'Kazakhstan'), (b'KE', 'Kenya'), (b'KI', 'Kiribati'), (b'KP', "Korea, Democratic People's Republic of"), (b'KR', 'Korea, Republic of'), (b'KW', 'Kuwait'), (b'KG', 'Kyrgyzstan'), (b'LA', "Lao People's Democratic Republic"), (b'LV', 'Latvia'), (b'LB', 'Lebanon'), (b'LS', 'Lesotho'), (b'LR', 'Liberia'), (b'LY', 'Libyan Arab Jamahiriya'), (b'LI', 'Liechtenstein'), (b'LT', 'Lithuania'), (b'LU', 'Luxembourg'), (b'MO', 'Macao'), (b'MK', 'Macedonia, The Former Yugoslav Republic of'), (b'MG', 'Madagascar'), (b'MW', 'Malawi'), (b'MY', 'Malaysia'), (b'MV', 'Maldives'), (b'ML', 'Mali'), (b'MT', 'Malta'), (b'MH', 'Marshall Islands'), (b'MQ', 'Martinique'), (b'MR', 'Mauritania'), (b'MU', 'Mauritius'), (b'YT', 'Mayotte'), (b'MX', 'Mexico'), (b'FM', 'Micronesia, Federated States of'), (b'MD', 'Moldova'), (b'MC', 'Monaco'), (b'MN', 'Mongolia'), (b'ME', 'Montenegro'), (b'MS', 'Montserrat'), (b'MA', 'Morocco'), (b'MZ', 'Mozambique'), (b'MM', 'Myanmar'), (b'NA', 'Namibia'), (b'NR', 'Nauru'), (b'NP', 'Nepal'), (b'NL', 'Netherlands'), (b'AN', 'Netherlands Antilles'), (b'NC', 'New Caledonia'), (b'NZ', 'New Zealand'), (b'NI', 'Nicaragua'), (b'NE', 'Niger'), (b'NG', 'Nigeria'), (b'NU', 'Niue'), (b'NF', 'Norfolk Island'), (b'MP', 'Northern Mariana Islands'), (b'NO', 'Norway'), (b'OM', 'Oman'), (b'PK', 'Pakistan'), (b'PW', 'Palau'), (b'PS', 'Palestinian Territory, Occupied'), (b'PA', 'Panama'), (b'PG', 'Papua New Guinea'), (b'PY', 'Paraguay'), (b'PE', 'Peru'), (b'PH', 'Philippines'), (b'PN', 'Pitcairn'), (b'PL', 'Poland'), (b'PT', 'Portugal'), (b'PR', 'Puerto Rico'), (b'QA', 'Qatar'), (b'RE', 'Reunion'), (b'RO', 'Romania'), (b'RU', 'Russian Federation'), (b'RW', 'Rwanda'), (b'BL', 'Saint Barthelemy'), (b'SH', 'Saint Helena'), (b'KN', 'Saint Kitts and Nevis'), (b'LC', 'Saint Lucia'), (b'MF', 'Saint Martin'), (b'PM', 'Saint Pierre and Miquelon'), (b'VC', 'Saint Vincent and the Grenadines'), (b'WS', 'Samoa'), (b'SM', 'San Marino'), (b'ST', 'Sao Tome and Principe'), (b'SA', 'Saudi Arabia'), (b'SN', 'Senegal'), (b'RS', 'Serbia'), (b'SC', 'Seychelles'), (b'SL', 'Sierra Leone'), (b'SG', 'Singapore'), (b'SK', 'Slovakia'), (b'SI', 'Slovenia'), (b'SB', 'Solomon Islands'), (b'SO', 'Somalia'), (b'ZA', 'South Africa'), (b'GS', 'South Georgia and the South Sandwich Islands'), (b'ES', 'Spain'), (b'LK', 'Sri Lanka'), (b'SD', 'Sudan'), (b'SR', 'Suriname'), (b'SJ', 'Svalbard and Jan Mayen'), (b'SZ', 'Swaziland'), (b'SE', 'Sweden'), (b'CH', 'Switzerland'), (b'SY', 'Syrian Arab Republic'), (b'TW', 'Taiwan, Province of China'), (b'TJ', 'Tajikistan'), (b'TZ', 'Tanzania, United Republic of'), (b'TH', 'Thailand'), (b'TL', 'Timor-Leste'), (b'TG', 'Togo'), (b'TK', 'Tokelau'), (b'TO', 'Tonga'), (b'TT', 'Trinidad and Tobago'), (b'TN', 'Tunisia'), (b'TR', 'Turkey'), (b'TM', 'Turkmenistan'), (b'TC', 'Turks and Caicos Islands'), (b'TV', 'Tuvalu'), (b'UG', 'Uganda'), (b'UA', 'Ukraine'), (b'AE', 'United Arab Emirates'), (b'GB', 'United Kingdom'), (b'US', 'United States'), (b'UM', 'United States Minor Outlying Islands'), (b'UY', 'Uruguay'), (b'UZ', 'Uzbekistan'), (b'VU', 'Vanuatu'), (b'VE', 'Venezuela'), (b'VN', 'Viet Nam'), (b'VG', 'Virgin Islands, British'), (b'VI', 'Virgin Islands, U.S.'), (b'WF', 'Wallis and Futuna'), (b'EH', 'Western Sahara'), (b'YE', 'Yemen'), (b'ZM', 'Zambia'), (b'ZW', 'Zimbabwe')])),
                ('map_latitude', models.DecimalField(default=0, max_digits=10, decimal_places=5)),
                ('map_longitude', models.DecimalField(default=0, max_digits=10, decimal_places=5)),
                ('map_zoom', models.IntegerField(default=3)),
                ('email', models.EmailField(max_length=1000, verbose_name='E-mail', blank=True)),
                ('url', models.CharField(max_length=1000, verbose_name='Website', blank=True)),
                ('phone', models.CharField(max_length=1000, verbose_name='Phone', blank=True)),
                ('mobile', models.CharField(max_length=1000, verbose_name='Mobile', blank=True)),
                ('ggn_number', models.CharField(max_length=50, null=True, verbose_name='GGN', blank=True)),
                ('reference_number', models.CharField(max_length=50, null=True, verbose_name='Reference number', blank=True)),
                ('producer_number', models.CharField(max_length=50, null=True, verbose_name='Producer/Farmer', blank=True)),
                ('tags', tagging_autocomplete.models.TagAutocompleteField(max_length=255, blank=True)),
                ('parent_organization', models.ForeignKey(related_name='children_organizations', verbose_name='parent organization', blank=True, to='organization.Organization', null=True)),
            ],
            options={
                'ordering': ['name'],
                'abstract': False,
                'db_table': 'Organization',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganizationAccessInvitation',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('email', models.EmailField(max_length=75)),
                ('timestamp', models.DateTimeField()),
                ('organization', models.ForeignKey(to='organization.Organization')),
                ('requested_by', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'OrganizationAccessInvitation',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganizationRelationship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(default=b'partner', max_length=100)),
                ('receiver_organization', models.ForeignKey(related_name='received_partnerships', to='organization.Organization')),
                ('sender_organization', models.ForeignKey(related_name='sent_partnerships', to='organization.Organization')),
            ],
            options={
                'db_table': 'OrganizationRelationship',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganizationReport',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('report_document', models.FileField(max_length=255, upload_to=b'report_documents')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'OrganizationReport',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganizationUnit',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=1024, verbose_name='Name')),
                ('slug', models.CharField(max_length=1024, verbose_name='Slug', blank=True)),
                ('type', models.CharField(default=b'other', max_length=1024, verbose_name='Type', choices=[(b'administrative', 'Administrative facility'), (b'farm', 'Farm'), (b'field', 'Field'), (b'other', 'Other'), (b'processing', 'Processing facility'), (b'production', 'Production facility'), (b'storage', 'Storage facility')])),
                ('organization', models.ForeignKey(related_name='units', verbose_name='Organization', to='organization.Organization')),
            ],
            options={
                'ordering': ['name'],
                'abstract': False,
                'db_table': 'OrganizationUnit',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartnerInvitation',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('text', models.TextField(blank=True)),
                ('status', models.CharField(default=b'open', max_length=1000)),
                ('receiver', models.ForeignKey(related_name='received_invitations', to='organization.Organization')),
                ('sender', models.ForeignKey(related_name='sent_invitations', to='organization.Organization')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'PartnerInvitation',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlotRVO',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('kvk', models.CharField(max_length=50, verbose_name='Organization KVK number')),
                ('field_id', models.CharField(max_length=50, verbose_name='RVO field id')),
                ('begin_date', models.DateTimeField(null=True, verbose_name='RVO begin date', blank=True)),
                ('end_date', models.DateTimeField(null=True, verbose_name='RVO end date', blank=True)),
                ('crop_type_code', models.CharField(max_length=10, null=True, verbose_name='RVO crop type code', blank=True)),
                ('pos_list', models.TextField(null=True, verbose_name='RVO field geo data', blank=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'PlotRVO',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(default=b'?', max_length=1000, verbose_name='Name', blank=True)),
                ('slug', models.CharField(default=b'', max_length=1000, verbose_name='Slug', blank=True)),
                ('harvest_year', models.IntegerField(default=2016, max_length=4, verbose_name='Harvest year', choices=[(2015, 2015), (2016, 2016), (2017, 2017)])),
                ('area', models.FloatField(null=True, verbose_name='Area in hectares', blank=True)),
                ('location', models.CharField(max_length=200, null=True, verbose_name='Location', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('organization', models.ForeignKey(related_name='products', verbose_name='Organization', to='organization.Organization')),
                ('organization_unit', models.ForeignKey(related_name='products', verbose_name='Organization unit', blank=True, to='organization.OrganizationUnit', null=True)),
            ],
            options={
                'ordering': ['harvest_year', 'name'],
                'abstract': False,
                'db_table': 'Product',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductIdentifier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier_type', models.CharField(default='', max_length=1024, choices=[(b'internal', 'Internal'), (b'gs1', b'GS1'), (b'upc', b'UPC'), (b'other', 'Other')])),
                ('identifier_type_name', models.CharField(default='', max_length=1024, null=True)),
                ('value', models.CharField(default='', max_length=1024)),
                ('product', models.ForeignKey(related_name='identifiers', to='organization.Product')),
            ],
            options={
                'db_table': 'ProductIdentifier',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductPartner',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('contract_number', models.CharField(max_length=200, null=True, verbose_name='Contract number', blank=True)),
                ('organization_uuid', models.ForeignKey(to='organization.Organization', max_length=200)),
                ('product', models.ForeignKey(related_name='partners', to='organization.Product')),
            ],
            options={
                'db_table': 'ProductPartner',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductType',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=1000, verbose_name='Name')),
                ('latin_name', models.CharField(max_length=1000, verbose_name='Latin name', blank=True)),
                ('synonyms', models.CharField(max_length=1000, verbose_name='Synonyms', blank=True)),
                ('source', models.CharField(max_length=1000, verbose_name='Source', blank=True)),
                ('help_text', models.CharField(max_length=1000, verbose_name='Help text', blank=True)),
                ('standards', models.CharField(max_length=1000, verbose_name='Standards', blank=True)),
                ('code', models.CharField(default=b'', max_length=100, verbose_name='Code', blank=True)),
                ('field_color', models.CharField(default=b'#6cb33f', max_length=30, verbose_name='Field color', blank=True)),
                ('edi_crop_code', models.CharField(default=b'', max_length=100, verbose_name='EDI-Crop code', blank=True)),
                ('gg_code', models.CharField(default=b'', max_length=100, verbose_name='GLOBALG.A.P. Code', blank=True)),
                ('rvo_code', models.CommaSeparatedIntegerField(max_length=1000, verbose_name='RVO list of codes', blank=True)),
                ('questionnaires', models.ManyToManyField(related_name='product_types', null=True, verbose_name='questionnaires', to='assessments.Questionnaire', blank=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'ProductType',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductTypeGroup',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=1000, verbose_name='Name', db_column=b'name_nl')),
                ('help_text', ckeditor.fields.RichTextField(max_length=1000, verbose_name='Help text', db_column=b'help_text_nl', blank=True)),
                ('code', models.CharField(default=b'', max_length=100, verbose_name='Code', blank=True)),
                ('product_types', models.ManyToManyField(related_name='product_type_groups', verbose_name='Product types', to='organization.ProductType', blank=True)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'ProductTypeGroup',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StandardProductType',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('standard_code', models.CharField(max_length=50, verbose_name='Standard code')),
                ('name', models.CharField(max_length=1000, verbose_name='Name')),
                ('product_type_code', models.CharField(default=b'', max_length=100, verbose_name='Product type code', blank=True)),
                ('begin_date', models.DateField(null=True, verbose_name='Begin date', blank=True)),
                ('end_date', models.DateField(null=True, verbose_name='End date', blank=True)),
                ('landscape_element', models.BooleanField(default=False, verbose_name='Landscape element')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'StandardProductType',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='product',
            name='product_type',
            field=models.ForeignKey(related_name='products', verbose_name='Product type', to='organization.ProductType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='organization',
            name='partners',
            field=models.ManyToManyField(related_name='incoming_partners', verbose_name='Partners', to='organization.Organization', through='organization.OrganizationRelationship', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='organization',
            name='users',
            field=models.ManyToManyField(related_name='organizations', verbose_name='Users', to=settings.AUTH_USER_MODEL, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='certification',
            name='certification_type',
            field=models.ForeignKey(related_name='certifications', blank=True, to='organization.CertificationType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='certification',
            name='organization',
            field=models.ForeignKey(related_name='certifications', blank=True, to='organization.Organization', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='certification',
            name='organization_unit',
            field=models.ForeignKey(related_name='certifications', blank=True, to='organization.OrganizationUnit', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='certification',
            name='product',
            field=models.ForeignKey(related_name='certifications', blank=True, to='organization.Product', null=True),
            preserve_default=True,
        ),
    ]
