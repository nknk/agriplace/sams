from collections import OrderedDict
from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import transaction
from django.template.defaultfilters import slugify

from assessments.models import AssessmentType
from core import countries
from organization.models import Organization, OrganizationMembership, ProductType
from tagging.models import Tag, TaggedItem
from translations.helpers import tolerant_ugettext

CHANNEL = 'Channel'
MASS_MAIL = 'Mass mail'
WEB_REGISTRATION = 'Web registration'
MANUALLY_CREATED = 'Manually created'
# -----------------
PRODUCER_GROUP = 'Producer group'
AGRICO = 'agrico'
AUDITOR = 'auditor'
BEST_OF_FOUR = 'best of four'
HZPC = 'hzpc'
JONIKA = 'jonika'
MONIE = 'monie'
NEDATO = 'nedato'
THE_GREENERY = 'the greenery'
VAN_NATURE = 'van nature'
WATERMAN_ONIONS = 'waterman onions'
UNDEFINED = 'undefined'
# -----------------
ACCOUNT_TYPE = 'Account type'
SINGLE_ORGANIZATION = 'single organization'
MULTIPLE_ORGANIZATIONS = 'multiple organizations'
# -----------------
SECTOR = 'Sector'
AGRICULTURE = 'agriculture'
HORTICULTURE = 'horticulture'
# -----------------

ORGANIZATION_LABEL_TAXONOMY = {
    CHANNEL: set([
        MASS_MAIL,
        WEB_REGISTRATION,
        MANUALLY_CREATED
    ]),
    PRODUCER_GROUP: set([
        AGRICO,
        AUDITOR,
        BEST_OF_FOUR,
        HZPC,
        JONIKA,
        MONIE,
        NEDATO,
        THE_GREENERY,
        VAN_NATURE,
        WATERMAN_ONIONS,
        UNDEFINED
    ]),
    ACCOUNT_TYPE: set([
        SINGLE_ORGANIZATION,
        MULTIPLE_ORGANIZATIONS
    ]),
    SECTOR: set([
        AGRICULTURE,
        HORTICULTURE
    ])
}

ORGANIZATION_USER_FILE_FIELDS = ('EMAIL', 'FIRST_NAME', 'LAST_NAME', 'GENDER', 'DATE_OF_BIRTH', 'PHONE_NO',
                                 'MEMBERSHIP_NO', 'ORGANIZATION_NAME', 'ADDRESS_LINE_1', 'ADDRESS_LINE2',
                                 'POSTAL_CODE', 'CITY', 'COUNTRY', 'PHONE', 'MOBILE', 'WEBSITE', 'GGN_NO',
                                 'CHAMBER_OF_COMMERCE', 'GLN_NO')

ORGANIZATION_USER_FILE_MANDATORY_FIELDS = ('ORGANIZATION_NAME', 'ADDRESS_LINE_1', 'CITY', 'COUNTRY', 'EMAIL',
                                           'FIRST_NAME', 'LAST_NAME')

ORGANIZATION_USER_DATE_FIELDS = ('DATE_OF_BIRTH',)


def collect_organizations_data():
    """
    Collect data about organizations, users and assessments

    Can be used to do a csv export
    """

    a_types = AssessmentType.objects.all().values_list('name', flat=True)

    def get_a_types(organization_assessment_types):
        initial = {
            key: None
            for key in a_types
        }
        for oat in organization_assessment_types:
            initial[oat] = True

        return initial

    organizations = []
    for organization in Organization.objects.all().prefetch_related('users'):
        user = organization.users.filter(
            is_superuser=False,
            is_staff=False
        ).order_by('-last_login').first()

        assessments_open = organization.assessments.filter(state='open').count()
        assessments_closed = organization.assessments.filter(state='closed').count()
        assessment_types = organization.assessments.values_list('assessment_type__name', flat=True)
        attachments = organization.assessments.values('attachment_list').count()
        answers = organization.assessments.values('answers').count()
        shares = organization.assessments.values('shares').count()

        membership = OrganizationMembership.objects.filter(secondary_organization=organization).first()
        d = OrderedDict([
            ('email', organization.email),
            ('name', organization.name),
            ('primary_organization', membership.primary_organization.name if membership else None),
            (
                'license_start_date',
                organization.license_start_date.strftime('%d-%m-%Y') if organization.license_start_date else None
            ),
            (
                'license_end_date',
                organization.license_end_date.strftime('%d-%m-%Y') if organization.license_end_date else None
            ),
            ('mailing_address1', organization.mailing_address1),
            ('mailing_address2', organization.mailing_address2),
            ('mailing_postal_code', organization.mailing_postal_code),
            ('mailing_city', organization.mailing_city),
            ('mailing_province', organization.mailing_province),
            ('mailing_country', organization.mailing_country),
            ('phone', organization.phone),
            ('theme', organization.get_organisation_theme_for_export(membership)),
            ('assessments_open', assessments_open),
            ('assessments_closed', assessments_closed),
            ('assessment_shares', shares),
            ('questions_answered', answers),
            ('attachments', attachments),
            ('created_at', date_format(organization.created_time)),
            ('modified_at', date_format(organization.modified_time)),
            ('test_organization', organization.is_test),
            ('user', user.username if user else None),
            ('first_name', user.first_name if user else None),
            ('last_name', user.last_name if user else None),
            ('active', user.is_active if user else None),
            ('last_login', get_user_login(user) if user else None),
        ])
        d.update(get_org_taxonomy(organization))
        d.update(get_a_types(assessment_types))
        organizations.append(d)

    return organizations


def collect_product_types_data():
    """
    Collect data about product types.

    Can be used to do a csv export
    """
    product_types = []
    for product_type in ProductType.objects.all():
        d = OrderedDict([
            ('UUID', product_type.uuid),
            ('Code', product_type.code),
            ('EDI Crop Code', product_type.edi_crop_code),
            ('GLOBALG.A.P. code', product_type.gg_code),
            ('RVO list of codes', product_type.rvo_code),
            ('Name(English)', tolerant_ugettext(product_type.name, given_lang='en')),
            ('Name(Spanish)', tolerant_ugettext(product_type.name, 'es')),
            ('Name(Dutch)', tolerant_ugettext(product_type.name, 'nl'))
        ])
        product_types.append(d)

    return product_types


def get_user_login(user):
    if user.last_login:
        return date_format(user.last_login)
    else:
        return None


def date_format(input_date):
    return input_date.strftime("%Y-%m-%d %H:%M:%S")


def get_org_taxonomy(org):
    output = {}
    for label, values in ORGANIZATION_LABEL_TAXONOMY.iteritems():
        try:
            tag = Tag.objects.get_for_object(org).values_list('name', flat=True).get(
                name__in=ORGANIZATION_LABEL_TAXONOMY[label]
            )
        except Tag.DoesNotExist:
            output[label] = None
        else:
            output[label] = tag
    return output


def get_org_channel(org):
    try:
        tag = Tag.objects.get_for_object(org).values_list('name', flat=True).get(
            name__in=ORGANIZATION_LABEL_TAXONOMY[CHANNEL]
        )
    except Tag.DoesNotExist:
        return None
    else:
        return tag


def set_org_taxonomy(org, **kwargs):
    tags = set().union(*[ORGANIZATION_LABEL_TAXONOMY[k] for k in kwargs.keys()])
    new_tags = ['"{}"'.format(v) for v in kwargs.values() if v]

    with transaction.atomic():
        ctype = ContentType.objects.get_for_model(org)
        TaggedItem._default_manager.filter(
            content_type__pk=ctype.pk,
            object_id=org.pk,
            tag__name__in=tags).delete()
        for nt in new_tags:
            Tag.objects.add_tag(org, nt)


def slug_value(name):
    return slugify(name)


def get_organization_by_slug(slug):
    return Organization.objects.filter(slug=slug).first()


def get_organization_slug(name, city, country):
    return u'{}-{}-{}'.format(slug_value(name), slug_value(city), slug_value(country))


def clean_data(data):
    if isinstance(data, unicode) or isinstance(data, str):
        return data.strip()
    elif isinstance(data, float):
        return '{}'.format(int(data))
    else:
        return data


def clean_organization_user_data(row):
    for key in ORGANIZATION_USER_FILE_FIELDS:
        row[key] = clean_data(row[key])


def validate_user_email(email):
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False


def check_organization_user_data(row):
    errors = []
    dob_value = row['DATE_OF_BIRTH']
    user = get_user_model().objects.filter(email=row['EMAIL']).first()
    email = row['EMAIL']
    if user:
        errors.append(u'User with this email already exists')

    if dob_value:
        try:
            dob = datetime.strptime(dob_value, '%d/%m/%Y')
        except ValueError:
            errors.append(u"Invalid format: Format of date of birth is dd/mm/yyyy")

    for key in ORGANIZATION_USER_FILE_MANDATORY_FIELDS:
        if not row[key]:
            errors.append(u"{} is mandatory field".format(key))

    if email and not validate_user_email(email):
        errors.append(u'Invalid email address format')

    country = row['COUNTRY']
    code = None
    for key, name in countries.COUNTRIES:
        if country.lower() == name.lower():
            code = key
            break

    if code and not errors:
        row['COUNTRY'] = code
    elif not code:
        errors.append(u"Invalid country name: {}".format(country))

    return ' | '.join(errors)
