from __future__ import absolute_import
import unicodecsv as csv

from celery.task import task
from celery.utils.log import get_task_logger
from django.core.files import File
from datetime import datetime
from organization.models import OrganizationReport

from organization.utils import collect_organizations_data

logger = get_task_logger(__name__)


@task()
def organization_report_task():
    today_datetime = datetime.now()
    file_name = "{}--organization_report.csv".format(today_datetime.strftime("%Y-%m-%d--%H-%M-%S"))
    data = collect_organizations_data()

    with open("/tmp/{}".format(file_name), 'wb+') as f:
        writer = csv.DictWriter(f, fieldnames=data[0].keys())
        writer.writeheader()
        for d in data:
            writer.writerow(d)

        weekly_report = OrganizationReport()
        weekly_report.report_document.save(file_name, File(f))
