from django.core.management.base import BaseCommand, CommandError
from datetime import datetime

from organization.models import PlotRVO
from ._rvo_utils import get_bedrijfspercelen_request_xml, get_rvo_data


class Command(BaseCommand):
    args = '<kvk kvk ...>'
    help = 'Import RVO data'

    can_import_settings = True

    def handle(self, *args, **options):
        for kvk_number in args:
            request_text = get_bedrijfspercelen_request_xml(kvk_number, '2014-01-01', '2015-01-01')
            response_result = get_rvo_data(request_text)
            if response_result['success']:
                print(response_result['success'])
            else:
                print(response_result['errors'])

