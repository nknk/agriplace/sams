import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()
from datetime import datetime

from django.conf import settings
import requests
import xml.etree.ElementTree as ET
from django.template.loader import render_to_string


# rvo settings
RVO_SETTINGS = {
    'base_url': getattr(settings, 'RVO_BASE_URL', ''),
    'rvo_id':  getattr(settings, 'RVO_ID', ''),
    'rvo_username':  getattr(settings, 'RVO_USERNAME', ''),
    'rvo_password':  getattr(settings, 'RVO_PASSWORD', ''),
    'rvo_type':  getattr(settings, 'RVO_TYPE', ''),
    'rvo_edicrop_version':  getattr(settings, 'RVO_EDICROP_VERSION', ''),
    'rvo_message_type_version':  getattr(settings, 'RVO_MESSAGE_TYPE_VERSION', ''),
    'rvo_epsg':  getattr(settings, 'RVO_EPSG', ''),
    'rvo_test': getattr(settings, 'RVO_TEST', True)
}

#
# Helpers
#

RVO_RESPONSE_MOCK = """
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
   <S:Body>
      <ns10:OpvragenBedrijfspercelenResponse xmlns:ns10="http://www.minez.nl/ws/edicrop/1.0/OpvragenBedrijfspercelen" xmlns:ns13="http://www.minez.nl/ws/edicrop/1.0/Field" xmlns:ns16="http://www.minez.nl/ws/edicrop/1.0/GMLTypes" xmlns:ns19="http://www.minez.nl/ws/edicrop/1.0/ExchangedDocument" xmlns:ns20="http://www.minez.nl/ws/edicrop/1.0/SpecifiedDataset" xmlns:ns21="http://www.minez.nl/ws/edicrop/1.0/CropField" xmlns:ns23="http://www.minez.nl/ws/edicrop/1.0/Farm">

         <ns10:Farm>
            <ns23:ThirdPartyFarmID>091959990000</ns23:ThirdPartyFarmID>
            <ns23:LastUpdateDate>2015-09-08T09:37:42.780+02:00</ns23:LastUpdateDate>
            <ns23:Field>
               <ns13:FieldID>RVO27378529CFD0000000081397</ns13:FieldID>
               <ns13:CropField>
                  <ns21:CropFieldID>RVO27378529CFD0000000081397</ns21:CropFieldID>
                  <ns21:CropFieldVersion>003</ns21:CropFieldVersion>
                  <ns21:BeginDate>2014-01-01T01:00:00.000+01:00</ns21:BeginDate>
                  <ns21:EndDate>2015-01-01T01:00:00.000+01:00</ns21:EndDate>
                  <ns21:Country>NL</ns21:Country>
                  <ns21:CropTypeCode listID="CL411">2645</ns21:CropTypeCode>
                  <ns21:UseTitleCode listID="CL412">02</ns21:UseTitleCode>
                  <ns21:Border>
                     <ns16:exterior>
                        <ns16:LinearRing>
                           <ns16:posList srsName="EPSG:28992">252349.5650 444726.1940 252305.0550 444615.4410 252272.1160 444627.8210 252242.5830 444654.7250 252273.4840 444746.7240 252316.6470 444750.4350 252316.8870 444753.2950 252349.5650 444726.1940</ns16:posList>
                        </ns16:LinearRing>
                     </ns16:exterior>
                  </ns21:Border>
               </ns13:CropField>
            </ns23:Field>
           <ns23:Field>
               <ns13:FieldID>RVO27378529CFD0000000785098</ns13:FieldID>
               <ns13:CropField>
                  <ns21:CropFieldID>RVO27378529CFD0000000785098</ns21:CropFieldID>
                  <ns21:CropFieldVersion>003</ns21:CropFieldVersion>
                  <ns21:BeginDate>2014-01-01T01:00:00.000+01:00</ns21:BeginDate>
                  <ns21:EndDate>2015-01-01T01:00:00.000+01:00</ns21:EndDate>
                  <ns21:Country>NL</ns21:Country>
                  <ns21:CropTypeCode listID="CL411">259</ns21:CropTypeCode>
                  <ns21:UseTitleCode listID="CL412">02</ns21:UseTitleCode>
                  <ns21:Border>
                     <ns16:exterior>
                        <ns16:LinearRing>
                           <ns16:posList srsName="EPSG:28992">252500.6770 444767.7690 252447.1680 444750.5590 252448.3230 444756.4920 252447.9660 444764.3620 252447.1470 444771.0000 252429.6750 444832.6940 252426.7370 444839.3430 252452.2330 444861.4150 252471.1770 444879.7970 252481.3060 444889.7380 252499.5010 444911.1220 252519.3840 444935.5060 252573.0400 444990.5510 252642.7440 444869.3310 252500.6770 444767.7690</ns16:posList>
                        </ns16:LinearRing>
                     </ns16:exterior>
                  </ns21:Border>
               </ns13:CropField>
            </ns23:Field>
            <ns23:Field>
               <ns13:FieldID>RVO27378529CFD0000000323993</ns13:FieldID>
               <ns13:CropField>
                  <ns21:CropFieldID>RVO27378529CFD0000000323993</ns21:CropFieldID>
                  <ns21:CropFieldVersion>003</ns21:CropFieldVersion>
                  <ns21:BeginDate>2014-01-01T01:00:00.000+01:00</ns21:BeginDate>
                  <ns21:EndDate>2015-01-01T01:00:00.000+01:00</ns21:EndDate>
                  <ns21:Country>NL</ns21:Country>
                  <ns21:CropTypeCode listID="CL411">259</ns21:CropTypeCode>
                  <ns21:UseTitleCode listID="CL412">02</ns21:UseTitleCode>
                  <ns21:Border>
                     <ns16:exterior>
                        <ns16:LinearRing>
                           <ns16:posList srsName="EPSG:28992">252338.4910 444548.4000 252347.6110 444548.7090 252493.1340 444529.7500 252494.7670 444496.3510 252358.7860 444515.8130 252299.1450 444523.9740 252300.0240 444527.6150 252307.4320 444535.5260 252315.4680 444540.5480 252327.5220 444544.8170 252338.4910 444548.4000</ns16:posList>
                        </ns16:LinearRing>
                     </ns16:exterior>
                  </ns21:Border>
               </ns13:CropField>
            </ns23:Field>
            <ns23:Field>
               <ns13:FieldID>RVO27378529CFD0000000306047</ns13:FieldID>
               <ns13:CropField>
                  <ns21:CropFieldID>RVO27378529CFD0000000306047</ns21:CropFieldID>
                  <ns21:CropFieldVersion>005</ns21:CropFieldVersion>
                  <ns21:BeginDate>2014-01-01T01:00:00.000+01:00</ns21:BeginDate>
                  <ns21:EndDate>2015-04-01T02:00:00.000+02:00</ns21:EndDate>
                  <ns21:Country>NL</ns21:Country>
                  <ns21:CropTypeCode listID="CL411">259</ns21:CropTypeCode>
                  <ns21:UseTitleCode listID="CL412">02</ns21:UseTitleCode>
                  <ns21:Border>
                     <ns16:exterior>
                        <ns16:LinearRing>
                           <ns16:posList srsName="EPSG:28992">252401.6890 444677.4540 252319.4300 444610.0390 252305.0550 444615.4410 252349.5650 444726.1940 252316.8870 444753.2940 252320.8210 444800.0690 252399.2150 444811.6660 252422.6660 444835.9860 252425.8100 444830.3750 252441.8900 444776.7210 252443.3160 444759.1750 252440.0350 444749.5080 252401.6890 444677.4540</ns16:posList>
                        </ns16:LinearRing>
                     </ns16:exterior>
                  </ns21:Border>
               </ns13:CropField>
            </ns23:Field>
         </ns10:Farm>
      </ns10:OpvragenBedrijfspercelenResponse>
   </S:Body>
</S:Envelope>
"""


# list of namespaces which will be used in parsing and requesting
NS_RESPONCE_LIST = {
    'S': 'http://schemas.xmlsoap.org/soap/envelope/',
    'env': 'http://schemas.xmlsoap.org/soap/envelope/',
    'exc': "http://www.minez.nl/ws/edicrop/1.0/ExchangedDocument",
    'spec': "http://www.minez.nl/ws/edicrop/1.0/SpecifiedDataset",
    'ns10': 'http://www.minez.nl/ws/edicrop/1.0/OpvragenBedrijfspercelen',
    'ns13': 'http://www.minez.nl/ws/edicrop/1.0/Field',
    'ns27': 'http://www.minez.nl/ws/edicrop/1.0/CropField',
    'ns23': 'http://www.minez.nl/ws/edicrop/1.0/Farm',
    'ns16': 'http://www.minez.nl/ws/edicrop/1.0/GMLTypes',
}

#
# Requests
#

# Request OpvragenBedrijfspercelen
def get_bedrijfspercelen_request_xml(farm_id, date_from, date_end):
    epoch = datetime(1970, 1, 1)
    request_data = {
        'rvo_username': RVO_SETTINGS['rvo_username'],
        'rvo_password': RVO_SETTINGS['rvo_password'],
        'rvo_document_id': int((datetime.now()-epoch).total_seconds()),
        'rvo_type': RVO_SETTINGS['rvo_type'],
        'rvo_edicrop_version': RVO_SETTINGS['rvo_edicrop_version'],
        'rvo_message_type_version': RVO_SETTINGS['rvo_message_type_version'],
        'rvo_id': RVO_SETTINGS['rvo_id'],
        'rvo_date_from': date_from,
        'rvo_date_to': date_end,
        'rvo_farm_id': farm_id
    }
    return render_to_string('organization/rvo/opvragen_bedrijfspercelen.xml', request_data)


def get_rvo_data(request_message):
    response_text = ""
    errors = []
    try:
        response_text = requests.post(RVO_SETTINGS['base_url'], data=request_message).text
    except (
        requests.exceptions.ConnectionError,
        requests.exceptions.Timeout,
        requests.exceptions.RequestException,
        requests.exceptions.HTTPError,
        requests.exceptions.URLRequired,
        requests.exceptions.TooManyRedirects
    ) as e:
        errors.append({
            'code': "rvo_0001",
            'msg': "Het lukt op dit moment niet om een verbinding te leggen met de RVO. Probeer het later opnieuw."

        })
    return {
        'success': response_text,
        'errors': errors
    }


def get_elem(parent, name):
    child = parent.find(name, NS_RESPONCE_LIST)
    if child is not None:
        return child.text

    return None


def parse_response(root):
    response_text=""
    errors = []

    if root.find('S:Body', NS_RESPONCE_LIST) is not None:
        response_body = root.find('S:Body', NS_RESPONCE_LIST)

        if response_body.find('ns10:OpvragenBedrijfspercelenResponse', NS_RESPONCE_LIST) is not None:
            # continue with farms
            response_body_farms = response_body.find('ns10:OpvragenBedrijfspercelenResponse', NS_RESPONCE_LIST).findall('ns10:Farm', NS_RESPONCE_LIST)
            farm_list = []
            for farm in response_body_farms:
                response_fields = farm.findall('ns23:Field', NS_RESPONCE_LIST)
                for field in response_fields:
                    result_field = {}
                    if field:
                        response_crop_field = field.find('ns13:CropField', NS_RESPONCE_LIST)
                        if response_crop_field:
                            result_field['crop_field_id'] = get_elem(response_crop_field, 'ns27:CropFieldID')

                            result_field['begin_date'] = get_elem(response_crop_field, 'ns27:BeginDate')

                            result_field['end_date'] = get_elem(response_crop_field, 'ns27:EndDate')

                            result_field['crop_type_code'] = get_elem(response_crop_field, 'ns27:CropTypeCode')

                            result_field['border'] = response_crop_field \
                                .find('ns27:Border', NS_RESPONCE_LIST)\
                                .find('ns16:exterior', NS_RESPONCE_LIST)\
                                .find('ns16:LinearRing', NS_RESPONCE_LIST)\
                                .find('ns16:posList', NS_RESPONCE_LIST).text
                            farm_list.append(result_field)
                    # get('srsName') for later get this attribute and use it like an input for mapnik
            response_text = farm_list  # {'plots_fetched': len(farm_list)}

        elif root.find('env:Body', NS_RESPONCE_LIST) is not None:
            # error from rvo
            errors.append({
                'code': "rvo_0003",
                'msg': "Het lukt op dit moment niet om de data op te halen bij de RVO. Probeer het later opnieuw."
            })

    return {'success': response_text, 'errors': errors}

# description: parse rvo xml-message and return list of farms
# rvo_response = response of OpvragenBedrijfspercelenResponse(see RVO API)
def get_farm_list(rvo_response):

    rvo_response_text = rvo_response
    if RVO_SETTINGS['rvo_test']:
        rvo_response_text = RVO_RESPONSE_MOCK

    root = ET.fromstring(rvo_response_text)
    result = {}

    try:
        result = parse_response(root)
    except:
        # parse error
        result['success'] = ""
        result['errors'] = ({
            'code': "rvo_0004",
            'msg': "Het lukt niet om het antwoord van RVO te interpreteren. Probeer het later opnieuw."
        },)

    return result
