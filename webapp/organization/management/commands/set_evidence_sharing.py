from django.core.management.base import BaseCommand
from django.db import transaction
from organization.models import OrganizationMembership, AGRIPLACE, HZPC
from sams_project.settings import AGRIPLACE_FARM_GROUP_SLUG


class Command(BaseCommand):
    args = ''
    help = 'Set the evidence sharing of all the primary organization'

    can_import_settings = True

    def handle(self, *args, **options):
        self.run_migration()

    @transaction.atomic
    def run_migration(self):
        memberships = OrganizationMembership.objects.all()
        for membership in memberships:
            primary_organization = membership.primary_organization
            if primary_organization.theme.layout == AGRIPLACE or primary_organization.theme.layout == HZPC:
                primary_organization.evidence_sharing = False
            else:
                primary_organization.evidence_sharing = True
            primary_organization.save()
        print('migration completed successfully')
