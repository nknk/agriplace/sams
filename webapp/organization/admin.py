import StringIO
from datetime import datetime

import unicodecsv as csv
from django import forms
from django.contrib import admin
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import transaction
from django.forms import ModelChoiceField
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import patterns, url
from import_export import fields
from import_export import resources

from assessment_workflow.models import AssessmentWorkflow
from core.admin import BaseAdmin, BaseTabularInline, TranslationMixin, BaseAbpAdminMixin
from organization.filters import (
    MembershipRoleFilter,
    FarmgroupFilter,
    HarvestYearFilter, AssessmentTypeFilter, AssessmentWorkFlowFilter)
from organization.utils import (
    collect_organizations_data, ORGANIZATION_LABEL_TAXONOMY, CHANNEL, PRODUCER_GROUP, get_org_taxonomy,
    set_org_taxonomy, ACCOUNT_TYPE, SECTOR,
    collect_product_types_data)
from organization.utils import (
    get_organization_by_slug, get_organization_slug, clean_data,
    check_organization_user_data, clean_organization_user_data
)
from registration.models import RegistrationProfile
from sams.models import UserProfile
from tagging.models import Tag
from .forms import ProductTypeAdminForm, BulkWorkflowCreateForm
from .models import (
    Certification,
    CertificationType,
    Organization,
    OrganizationType,
    OrganizationMembership,
    OrganizationRelationship,
    OrganizationUnit,
    PartnerInvitation,
    Product,
    ProductPartner,
    ProductType,
    StandardProductType,
    ProductTypeGroup,
    PlotRVO,
    OrganizationReport, CertificationFile, CertificationCrop, OrganizationTheme)

ORGANIZATION_USER_RESOURCE_FIELDS = ('uuid', 'slug', 'name', 'mailing_address1',
                                     'mailing_address2', 'mailing_postal_code', 'mailing_city', 'mailing_country',
                                     'phone', 'mobile', 'url', 'ggn_number', 'identification_number', 'gnl_number',
                                     'user_email', 'user_first_name', 'user_last_name', 'user_gender',
                                     'user_phone_number', 'user_date_of_birth')

TODAY = datetime.now()


class CertificationAdmin(BaseAdmin):
    model = Certification
    list_display = ('organization', 'certification_type',)
    search_fields = ('organization__name', 'certification_type__name',)
    list_filter = ('certification_type',)


class CertificationTypeAdmin(BaseAdmin):
    model = CertificationType
    list_display = ('name', 'code', 'verified')
    search_fields = ('name', 'code',)
    list_filter = ('verified',)


class CertificationFileAdmin(BaseAdmin):
    model = CertificationFile
    list_display = ('uuid', 'file', 'file_type')
    search_fields = ('file', 'file_type')


class CertificationCropAdmin(BaseAdmin):
    model = CertificationCrop
    list_display = ('uuid', 'product_type', 'area')
    search_fields = ('product_type__name', 'area')


class OrganizationAdminForm(forms.ModelForm):
    """
    Custom organization admin form

    Will add fields according to the taxonomy (1 level deep)
    """

    class Meta:
        model = Organization
        exclude = ['groups', 'remarks', 'description']

    def __init__(self, *args, **kwargs):
        super(OrganizationAdminForm, self).__init__(*args, **kwargs)
        #  taxonomy label -> current value
        org_taxonomy = get_org_taxonomy(kwargs['instance']) if kwargs.get('instance') else None
        # Add the fields and populate the choices and initial values
        for label, values in ORGANIZATION_LABEL_TAXONOMY.iteritems():
            self.fields[label.lower()] = forms.ChoiceField(
                choices=[(None, '')] + zip(values, values),
                required=False,
                initial=org_taxonomy[label] if org_taxonomy else None
            )


class OrganizationMembershipInlineAdmin(BaseAbpAdminMixin, admin.TabularInline):
    model = OrganizationMembership
    extra = 0
    can_delete = True
    fk_name = 'secondary_organization'

    def get_fields(self, request, obj=None):
        if self.is_abp_manager(request):
            return ['primary_organization', 'role', 'membership_number']
        return ['primary_organization', 'organization_type', 'role', 'membership_number', 'created_time', 'modified_time']

    def get_readonly_fields(self, request, obj=None):
        if self.is_abp_manager(request):
            return ['primary_organization', 'created_time', 'modified_time']
        return ['created_time', 'modified_time']

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super(OrganizationMembershipInlineAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == 'role':
            if self.is_abp_manager(request):
                field.queryset = field.queryset.filter(
                    name__in=['external_auditor', 'internal_inspector', 'internal_auditor', 'grower',
                              'agriplace_grower']
                )
        return field


class UserInlineAdmin(BaseAbpAdminMixin, admin.TabularInline):
    model = Organization.users.through
    model._meta.verbose_name_plural = "Users"
    max_num = 1
    can_delete = False

    def get_fields(self, request, obj=None):
        return ['get_username', 'get_email', 'get_gender', 'get_language', 'get_name']

    def get_readonly_fields(self, request, obj=None):
        return ['get_username', 'get_email', 'get_gender', 'get_language', 'get_name']

    def get_username(self, obj):
        return mark_safe("<a href='{}'>{}</a>".format(
            reverse('admin:{0}_{1}_change'.format('auth', 'user'), args=(obj.user.pk,)),
            obj.user.username
        ))
    get_username.short_description = 'User name'
    get_username.allow_tags = True

    def get_email(self, obj):
        return "{}".format(obj.user.email)
    get_email.short_description = 'Email'

    def get_gender(self, obj):
        try:
            return u"{}".format(obj.user.profile.get_gender_display())
        except MultipleObjectsReturned:
            return "Many profiles founded"
    get_gender.short_description = 'Gender'

    def get_language(self, obj):
        try:
            return u"{}".format(obj.user.profile.get_language_display())
        except MultipleObjectsReturned:
            return "Many profiles founded"
    get_language.short_description = 'Language'

    def get_name(self, obj):
        return u"{} {}".format(obj.user.first_name, obj.user.last_name)
    get_name.short_description = 'Full name'


class AssessmentWorkflowInlineAdmin(BaseAbpAdminMixin, BaseTabularInline):
    model = AssessmentWorkflow
    max_num = 1
    can_delete = False
    fk_name = 'author_organization'

    def get_fields(self, request, obj=None):
        return ['get_assessment_type_link', 'assessment_status', 'harvest_year', 'created_time', 'modified_time']

    def get_readonly_fields(self, request, obj=None):
        return ['get_assessment_type_link', 'assessment_status', 'harvest_year', 'created_time', 'modified_time']

    def get_assessment_type_link(self, obj):
        return mark_safe("<a href='{}'>{}</a>".format(
            reverse('admin:{0}_{1}_change'.format('assessment_workflow', 'assessmentworkflow'), args=(obj.pk,)),
            obj.assessment_type
        ))
    get_assessment_type_link.short_description = 'Assessment type'
    get_assessment_type_link.allow_tags = True


class OrganizationAdmin(BaseAbpAdminMixin, BaseAdmin):
    model = Organization
    search_fields = ('name', 'tags')

    date_hierarchy = 'created_time'

    list_filter = [FarmgroupFilter, MembershipRoleFilter, HarvestYearFilter, AssessmentTypeFilter,
                   AssessmentWorkFlowFilter]

    filter_horizontal = ('users', 'partners', 'groups')

    form = OrganizationAdminForm

    inlines = [UserInlineAdmin, AssessmentWorkflowInlineAdmin, OrganizationMembershipInlineAdmin]

    def get_readonly_fields(self, request, obj=None):
        return ['created_time', 'modified_time']

    def get_fields(self, request, obj=None):
        form = self.get_form(request, obj, fields=None)
        base_fields = list(form.base_fields)
        readonly_fields = list(self.get_readonly_fields(request, obj))

        if self.is_abp_manager(request):
            base_fields = [
                'name', 'logo', 'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_country',
                'phone', 'mobile', 'email', 'url', 'identification_number', 'ggn_number'
            ]
        return base_fields + readonly_fields

    def get_actions(self, request):
        common_actions = super(OrganizationAdmin, self).get_actions(request)
        actions = ['bulk_create_workflow', 'export_to_csv', 'migrate_to_new_buyer_group',
                   'migrate_to_new_auditor_group']
        for action_name in actions:
            action = getattr(self, action_name)
            common_actions[action_name] = (action, action_name, getattr(action, 'short_description', ''))
        if self.is_abp_manager(request):
            del common_actions['delete_selected']
            del common_actions['migrate_to_new_auditor_group']
            del common_actions['migrate_to_new_buyer_group']
            del common_actions['export_to_csv']
        return common_actions

    def get_list_display(self, request):
        if self.is_abp_manager(request):
            return ['name', 'get_membership', 'mailing_city', 'created_time', 'get_users']
        else:
            return ['name', 'created_time', 'modified_time', 'get_channel', 'get_producer_group',
                    'get_account_type', 'get_sector']

    def get_queryset(self, request):
        organizations = Organization.objects.all()
        if self.is_abp_manager(request):
            return organizations.filter(uuid__in=OrganizationMembership.get_sisters_by_user(request.user))
        return organizations

    def save_model(self, request, obj, form, change):
        # Also save the taxonomy/tags
        with transaction.atomic():
            obj.save()
            set_org_taxonomy(obj, **{
                label: form.cleaned_data[label.lower()]
                for label in ORGANIZATION_LABEL_TAXONOMY.keys()
                })

    def get_membership(self, obj):
        membership_list = u""
        for membership in OrganizationMembership.objects.filter(secondary_organization=obj):
            membership_list += u"<strong>{}</strong> as {}({})<br/>".format(
                getattr(membership.primary_organization, 'name', ""),
                getattr(membership, 'role', ""),
                getattr(membership, 'membership_number', "")
            )
        return membership_list
    get_membership.short_description = 'Memberships'
    get_membership.allow_tags = True

    def get_users(self, obj):
        user_list = ""
        for user in obj.users.all():
            # TODO remove duplicates in user_profiles
            user_list += "<a href='{}'>{}</a><br/>".format(
                reverse('admin:{0}_{1}_change'.format(
                    get_user_model()._meta.app_label, get_user_model()._meta.model_name
                ), args=(user.pk,)),
                user.username
            )
        return user_list
    get_users.short_description = 'Users'
    get_users.allow_tags = True

    def get_fieldsets(self, request, obj=None):
        # Hack to get the dynamic field working in the admin
        fieldsets = super(OrganizationAdmin, self).get_fieldsets(request, obj)
        if not self.is_abp_manager(request):
            if hasattr(request, "_gfs_marker"):
                fieldsets[0][1]['fields'] += ('channel', 'producer group', 'account type', 'sector')
            setattr(request, "_gfs_marker", 1)
        return fieldsets

    def get_channel(self, obj):
        # Show the channel from the taxonomy
        try:
            return Tag.objects.get_for_object(obj).values_list('name', flat=True).get(
                name__in=ORGANIZATION_LABEL_TAXONOMY[CHANNEL])
        except Tag.DoesNotExist:
            return ''

    get_channel.short_description = 'Channel'

    def get_producer_group(self, obj):
        try:
            return Tag.objects.get_for_object(obj).values_list('name', flat=True).get(
                name__in=ORGANIZATION_LABEL_TAXONOMY[PRODUCER_GROUP])
        except Tag.DoesNotExist:
            return ''

    get_producer_group.short_description = 'Producer group'

    def get_account_type(self, obj):
        try:
            return Tag.objects.get_for_object(obj).values_list('name', flat=True).get(
                name__in=ORGANIZATION_LABEL_TAXONOMY[ACCOUNT_TYPE])
        except Tag.DoesNotExist:
            return ''

    get_account_type.short_description = 'Account type'

    def get_sector(self, obj):
        try:
            return Tag.objects.get_for_object(obj).values_list('name', flat=True).get(
                name__in=ORGANIZATION_LABEL_TAXONOMY[SECTOR])
        except Tag.DoesNotExist:
            return ''

    get_sector.short_description = 'Sector'

    def export_to_csv(self, model_admin, request, queryset):
        """
        Export interesting organization, users and assessment data to csv
        """
        data = collect_organizations_data()
        f = StringIO.StringIO()

        writer = csv.DictWriter(f, fieldnames=data[0].keys())
        writer.writeheader()
        for d in data:
            writer.writerow(d)

        f.seek(0)
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="agri_data.csv"'
        return response
    export_to_csv.short_description = "Export to csv"

    def migrate_to_new_buyer_group(self, request, queryset):
        buyer_organizations = queryset.filter(type='buyer')
        buyer_group, __ = Group.objects.get_or_create(name='buyers')

        for org in buyer_organizations:
            org.groups.add(buyer_group)

        self.message_user(request, 'Migration is completed successfully')
    migrate_to_new_buyer_group.short_description = "Migrate to new buyer group"

    def migrate_to_new_auditor_group(self, request, queryset):
        auditor_organizations = queryset.filter(type='auditor')
        auditor_group, __ = Group.objects.get_or_create(name='external_auditor')
        for org in auditor_organizations:
            org.groups.add(auditor_group)
        self.message_user(request, "Migration is completed successfully")
    migrate_to_new_auditor_group.short_description = "Migrate to new auditor group"

    def bulk_create_workflow(self, modeladmin, request, queryset):
        form = None

        if self.is_abp_manager(request):
            parent_organization_ids = OrganizationMembership.get_parents_by_user(request.user)
        else:
            parent_organization_ids = OrganizationMembership.objects.all().values_list(
                'primary_organization', flat=True).distinct()

        if 'apply' in request.POST:
            form = BulkWorkflowCreateForm(parent_organization_ids, request.POST)
            if form.is_valid():
                errors = []
                cleaned_data = form.cleaned_data
                try:
                    producer_type = OrganizationType.objects.get(slug='producer')
                except (ObjectDoesNotExist, MultipleObjectsReturned):
                    errors.append("Can't find Producer as Organization Type. Please, contact support")
                else:
                    for org in queryset:
                        """
                        Once the assessments are organization centric then the below loop should be removed
                        and only one workflow context per organization should be created for a given harvest year and
                        given assessment type.
                        """
                        try:
                            membership = OrganizationMembership.objects.get(
                                primary_organization=form.cleaned_data['farm_group'],
                                secondary_organization=org,
                                organization_type=producer_type
                            )
                        except ObjectDoesNotExist:
                            errors.append(
                                u"Organization: {} doesn't belongs to selected Farm Group({})".format(
                                    org,
                                    cleaned_data["farm_group"]
                                )
                            )
                            continue

                        user = org.users.first()  # why author still required field?
                        try:
                            workflow, created = AssessmentWorkflow.objects.get_or_create(
                                author=user,
                                author_organization=org,
                                assessment_type=cleaned_data['assessment_type'],
                                harvest_year=form.cleaned_data['harvest_year'],
                                membership=membership,
                                configuration=form.cleaned_data['configuration'],
                                label_configuration=form.cleaned_data['label_configuration'],
                                selected_workflow=form.cleaned_data['selected_workflow']
                            )
                        except Exception as exp:
                            errors.append(
                                u"There is some error on adding workflow context for "
                                u"(user=\"{}\", org=\"{}\", assessment type=\"{}\" and harvest year=\"{}\") "
                                u"combination. {}".format(
                                    user,
                                    org,
                                    cleaned_data["assessment_type"],
                                    cleaned_data["harvest_year"],
                                    exp.message
                                )
                            )
                        else:
                            if not created:
                                errors.append(
                                    u"Workflow context already exists for (user=\"{}\", org=\"{}\", assessment type=\"{}\" "
                                    u"and harvest year=\"{}\") combination.".format(
                                        user,
                                        org,
                                        cleaned_data["assessment_type"],
                                        cleaned_data["harvest_year"]
                                    )
                                )

                if not errors:
                    level = messages.INFO
                    total_organizations = len(queryset)
                    msg = "Successfully created {} workflow{}.".format(
                        total_organizations, "s" if total_organizations > 1 else ""
                    )
                    self.message_user(request, msg, level=level)
                else:
                    level = messages.ERROR
                    if len(errors) < len(queryset):
                        self.message_user(
                            request,
                            "{} of {} failed while creating workflow contexts. See the detail below.".format(
                                len(errors), len(queryset)
                            ),
                            level=level
                        )
                    else:
                        self.message_user(
                            request,
                            "All workflow creation failed. See the detail below.",
                            level=level
                        )
                    for error in errors:
                        self.message_user(request, error, level=level)

                return HttpResponseRedirect(request.get_full_path())

        if not form:
            form = BulkWorkflowCreateForm(
                parent_organization_ids,
                initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)}
            )

        return render_to_response(
            'admin/bulk_create_workflow.html',
            {'form': form, 'organizations': queryset, 'opts': self.model._meta, 'title': _("Add workflow"),},
            context_instance=RequestContext(request)
        )

    bulk_create_workflow.short_description = "Create workflow"


class ProductPartnerInline(BaseTabularInline):
    model = ProductPartner
    fields = ('organization_uuid', 'contract_number')


class ProductAdmin(BaseAdmin):
    model = Product
    list_display = ('name', 'product_type', 'organization', 'created_time', 'modified_time')
    search_fields = ('name',)
    list_filter = ('organization', 'product_type')
    inlines = [
        ProductPartnerInline
    ]


class ProductTypeAdmin(BaseAdmin, TranslationMixin):
    form = ProductTypeAdminForm
    list_display = ('name',)
    search_fields = ('name',)
    filter_horizontal = ('questionnaires',)

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'name',
            'synonyms',
            'help_text',
        ]
        return super(ProductTypeAdmin, self).__init__(*args, **kwargs)

    def export(self, request):
        data = collect_product_types_data()
        f = StringIO.StringIO()

        writer = csv.DictWriter(f, fieldnames=data[0].keys())
        writer.writeheader()
        for d in data:
            writer.writerow(d)

        f.seek(0)
        response = HttpResponse(f, content_type='text/csv')
        filename = "product_type_{}.csv".format(str(datetime.now()))
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(filename)
        return response

    def get_urls(self):
        urls = super(ProductTypeAdmin, self).get_urls()
        my_urls = patterns("", url(r"^export/$", self.export))
        return my_urls + urls


class StandardProductTypeAdmin(BaseAdmin, TranslationMixin):
    form = ProductTypeAdminForm
    list_display = ('name',)
    search_fields = ('name',)

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'name'
        ]
        return super(StandardProductTypeAdmin, self).__init__(*args, **kwargs)


class ProductTypeGroupAdmin(BaseAdmin, TranslationMixin):
    model = ProductTypeGroup
    list_display = ('name', 'get_product_types')
    search_fields = ('name',)
    filter_horizontal = ('product_types',)

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'name',
            'help_text',
        ]
        return super(ProductTypeGroupAdmin, self).__init__(*args, **kwargs)

    def get_product_types(self, obj):
        return "hello"
        # return ', '.join(self.product_types.all())

    get_product_types.short_description = _("Product types")


class PlotRVOAdmin(BaseAdmin):
    list_display = ('kvk', 'field_id', 'begin_date', 'end_date')
    search_fields = ('kvk', 'field_id')
    date_hierarchy = 'modified_time'


class OrganizationReportAdmin(BaseAdmin):
    model = OrganizationReport
    list_display = ('report_document', 'created_time')
    ordering = ('-created_time',)


class OrganizationUserResource(resources.ModelResource):
    def __init__(self, language, farm_group, role):
        self.language = language
        self.farm_group = farm_group
        self.role = role
        super(OrganizationUserResource, self).__init__()

    membership_number = fields.Field(
        column_name='MEMBERSHIP_NO',
        attribute='membership_number')

    name = fields.Field(
        column_name='ORGANIZATION_NAME',
        attribute='name')

    mailing_address1 = fields.Field(
        column_name='ADDRESS_LINE_1',
        attribute='mailing_address1')

    mailing_address2 = fields.Field(
        column_name='ADDRESS_LINE2',
        attribute='mailing_address2')

    mailing_postal_code = fields.Field(
        column_name='POSTAL_CODE',
        attribute='mailing_postal_code')

    mailing_city = fields.Field(
        column_name='CITY',
        attribute='mailing_city')

    mailing_country = fields.Field(
        column_name='COUNTRY',
        attribute='mailing_country')

    phone = fields.Field(
        column_name='PHONE',
        attribute='phone')

    mobile = fields.Field(
        column_name='MOBILE',
        attribute='mobile')

    url = fields.Field(
        column_name='WEBSITE',
        attribute='url')

    ggn_number = fields.Field(
        column_name='GGN_NO',
        attribute='ggn_number')

    identification_number = fields.Field(
        column_name='CHAMBER_OF_COMMERCE',
        attribute='identification_number')

    gnl_number = fields.Field(
        column_name='GLN_NO',
        attribute='gnl_number')

    user_email = fields.Field(
        column_name='EMAIL',
        attribute='user_email')

    user_first_name = fields.Field(
        column_name='FIRST_NAME',
        attribute='user_first_name')

    user_last_name = fields.Field(
        column_name='LAST_NAME',
        attribute='user_last_name')

    user_gender = fields.Field(
        column_name='GENDER',
        attribute='user_gender')

    user_date_of_birth = fields.Field(
        column_name='DATE_OF_BIRTH',
        attribute='user_date_of_birth')

    user_phone_number = fields.Field(
        column_name='PHONE_NO',
        attribute='user_phone_number')

    def before_import_row(self, row, **kwargs):
        clean_organization_user_data(row)
        errors = check_organization_user_data(row)
        if errors:
            raise ValueError(errors)

        slug = get_organization_slug(row['ORGANIZATION_NAME'], row['CITY'], row['COUNTRY'])

        organization = get_organization_by_slug(slug)

        if organization:
            row['uuid'] = organization.uuid
        else:
            row['uuid'] = ''
            row['slug'] = slug

    def create_user(self, row):
        email = clean_data(row['EMAIL'])
        first_name = clean_data(row['FIRST_NAME'])
        last_name = clean_data(row['LAST_NAME'])
        gender = clean_data(row['GENDER'])
        dob_value = clean_data(row['DATE_OF_BIRTH'])
        phone_number = clean_data(row['PHONE_NO'])

        user_data = {
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "username": email
        }
        user = get_user_model().objects.create_user(**user_data)

        profile_data = {
            "user": user,
            "gender": gender,
            "phone": phone_number,
            "language": self.language
        }
        if dob_value:
            dob = datetime.strptime(dob_value, '%d/%m/%Y')
            profile_data['date_of_birth'] = dob

        UserProfile.objects.create(**profile_data)
        RegistrationProfile.objects.create_profile(user=user)
        return user

    def after_import_row(self, row, row_result, **kwargs):
        if not row_result.errors:
            organization = Organization.objects.get(uuid=row_result.object_id)
            tags = {'Sector': u'', 'Account type': u'', 'Producer group': u'', 'Channel': u'Mass mail'}
            set_org_taxonomy(organization, **tags)

            organization_type, _ = OrganizationType.objects.get_or_create(name='producer', slug='producer')
            OrganizationMembership.objects.create(
                primary_organization=self.farm_group,
                secondary_organization=organization,
                organization_type=organization_type,
                membership_number=row['MEMBERSHIP_NO'],
                role=self.role
            )

            user = self.create_user(row)
            if user:
                organization.users.add(user)

    class Meta:
        fields = ORGANIZATION_USER_RESOURCE_FIELDS
        export_order = ORGANIZATION_USER_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = Organization


class OrganizationMembershipAdmin(BaseAbpAdminMixin, BaseAdmin):
    list_display = (
        'primary_organization', 'secondary_organization', 'organization_type', 'membership_number', 'role'
    )
    search_fields = (
        'primary_organization__name', 'secondary_organization__name', 'membership_number', 'role__name'
    )

    def get_model_perms(self, request):
        if self.is_abp_manager(request):
            return {}
        return super(OrganizationMembershipAdmin, self).get_model_perms(request)


admin.site.register(Certification, CertificationAdmin)
admin.site.register(CertificationType, CertificationTypeAdmin)
admin.site.register(CertificationFile, CertificationFileAdmin)
admin.site.register(CertificationCrop, CertificationCropAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(OrganizationType)
admin.site.register(OrganizationMembership, OrganizationMembershipAdmin)
admin.site.register(OrganizationTheme)
admin.site.register(OrganizationRelationship)
#admin.site.register(OrganizationUnit)
#admin.site.register(PartnerInvitation)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductType, ProductTypeAdmin)
admin.site.register(StandardProductType, StandardProductTypeAdmin)
#admin.site.register(ProductTypeGroup, ProductTypeGroupAdmin)
admin.site.register(PlotRVO, PlotRVOAdmin)
admin.site.register(OrganizationReport, OrganizationReportAdmin)
