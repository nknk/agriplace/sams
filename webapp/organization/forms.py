from datetime import datetime

from django import forms
from django.conf import settings
from django.contrib.auth.models import Group
from django.db import transaction
from django.forms.models import inlineformset_factory
from django.forms.util import ErrorList
from django.forms.widgets import HiddenInput
from django.utils.translation import ugettext_lazy as _

from assessments.models import DynamicEvidenceConfiguration, AssessmentType, PLATFORM_CHOICES, DynamicLabelConfiguration
from assessments.models import HARVEST_YEAR_CHOICES
from organization.widgets import ProductTypeSelect
from organization.models import (
    Certification,
    CertificationType,
    Organization,
    OrganizationUnit,
    Product,
    ProductType,
    ProductIdentifier
)
from .widgets import ColorPickerWidget

TODAY = datetime.now()


class BulkWorkflowCreateForm(forms.Form):
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput, required=True)
    farm_group = forms.ModelChoiceField(None, empty_label=None)
    assessment_type = forms.ModelChoiceField(AssessmentType.objects.filter(kind='assessment'), empty_label=None)
    harvest_year = forms.ChoiceField(choices=HARVEST_YEAR_CHOICES, initial=TODAY.year)
    selected_workflow = forms.ChoiceField(choices=PLATFORM_CHOICES, required=True)
    configuration = forms.ModelChoiceField(DynamicEvidenceConfiguration.objects.filter().order_by('name'), required=False)
    label_configuration = forms.ModelChoiceField(DynamicLabelConfiguration.objects.filter().order_by('name'), required=False)

    def __init__(self, primary_organization_ids, *args, **kwargs):
        super(BulkWorkflowCreateForm, self).__init__(*args, **kwargs)
        self.fields['farm_group'].queryset = Organization.objects.filter(pk__in=primary_organization_ids)


class OrganizationCreateForm(forms.ModelForm):
    class Meta:
        model = Organization
        fields = ['name', 'mailing_country']

    def __init__(self, *args, **kwargs):
        super(OrganizationCreateForm, self).__init__(*args, **kwargs)
        # Mark this field to be slugified into #id_slug
        self.fields['name'].widget.attrs['data-slugify-to'] = 'id_slug'
        # self.fields['type'].widget.attrs['class'] = 'extended-select'
        # self.fields['mailing_country'].widget.attrs['class'] = 'countries'


class OrganizationAbpCreateForm(forms.ModelForm):
    farm_group = forms.ModelChoiceField(queryset=None, empty_label=None)
    role = forms.ModelChoiceField(queryset=Group.objects.filter(
        name__in=['external_auditor', 'internal_inspector', 'internal_auditor', 'grower',
                  'agriplace_grower']
    ), empty_label=None)

    def __init__(self, primary_organization_ids, *args, **kwargs):
        super(OrganizationAbpCreateForm, self).__init__(*args, **kwargs)
        self.fields['farm_group'].queryset = Organization.objects.filter(pk__in=primary_organization_ids)

    class Meta:
        model = Organization
        fields = ['farm_group', 'name', 'role', 'mailing_address1', 'mailing_address2', 'mailing_city',
                  'mailing_province', 'mailing_postal_code', 'mailing_country', 'email',
                  'url', 'phone', 'mobile', 'ggn_number']


class OrganizationUpdateForm(forms.ModelForm):
    """
    Organization form
    """

    class Meta:
        model = Organization
        fields = ['name', 'logo',
                  'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_province', 'mailing_postal_code',
                  'mailing_country', 'email', 'phone', 'url', 'identification_number', 'ggn_number']


class OrganizationBasicsForm(forms.ModelForm):
    """
    Organization form
    """

    class Meta:
        model = Organization
        fields = [
            'name', 'logo',
        ]


class OrganizationLocationForm(forms.ModelForm):
    """
    Organization location form
    """

    class Meta:
        model = Organization
        fields = [
            'map_latitude', 'map_longitude', 'map_zoom'
        ]


class OrganizationContactForm(forms.ModelForm):
    """
    Organization location form
    """

    class Meta:
        model = Organization
        fields = [
            'email', 'phone', 'url',
            'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_province', 'mailing_postal_code',
            'mailing_country'
        ]


class OrganizationPeopleForm(forms.ModelForm):
    """
    Organization form
    """

    class Meta:
        model = Organization
        fields = ['name', 'logo', 'description',
                  'mailing_address1', 'mailing_address2', 'mailing_city', 'mailing_province', 'mailing_postal_code',
                  'mailing_country', 'email', 'phone', 'url']


class OrganizationDetailsForm(forms.ModelForm):
    """
    Organization fact sheet form
    """
    # Basic
    official_name = forms.CharField(required=True, label=_("Organization name"))
    short_name = forms.CharField(required=False, label=_("Organization short name, if needed"))
    description = forms.CharField(required=False, label=_("Short description of organization"), widget=forms.Textarea())
    email = forms.CharField(required=False, label=_("E-mail address"))
    website = forms.CharField(required=False, label=_("Website"))

    # Contact
    mailing_address_line1 = forms.CharField(required=False, label=_("Address line 1"))
    mailing_address_line2 = forms.CharField(required=False, label=_("Address line 2"))
    mailing_address_postal_code = forms.CharField(required=False, label=_("Postal code"))
    mailing_address_coords = forms.CharField(required=False, label=_("Coordinates"))
    mailing_address_city = forms.CharField(required=False, label=_("City"))
    mailing_address_state = forms.CharField(required=False, label=_("State/Province"))
    # TODO Use countries dropdown
    mailing_address_country = forms.CharField(required=False, label=_("Country"))
    mailing_address_phone = forms.CharField(required=False, label=_("Phone number"))

    # Visiting
    visiting_address_country = forms.CharField(required=False, label=_("Visiting address country (optional)"))
    visiting_address1 = forms.CharField(required=False, label=_("Visiting address"))
    visiting_address2 = forms.CharField(required=False, label=_("Visiting address (line 2)"))
    visiting_address_postal_code = forms.CharField(required=False, label=_("Visiting address postal code"))
    visiting_address_coords = forms.CharField(required=False, label=_("Visiting address coordinates (on map)"))
    visiting_address_phone = forms.CharField(required=False, label=_("Visiting address phone number"))
    visiting_address_city = forms.CharField(required=False, label=_("Mailing address city"))
    visiting_address_state = forms.CharField(required=False, label=_("Mailing address state/region"))
    visiting_address_country = forms.CharField(required=False, label=_("Mailing address country"))

    # Legal
    tax_number = forms.CharField(required=False, label=_("Tax number"))
    registration_number = forms.CharField(required=False, label=_("Registration number"))


class OrganizationUnitDetailsForm(forms.ModelForm):
    """
    Organization unit fact sheet form
    """
    contact_person_name = forms.CharField(required=True, label=_("Contact person name"))
    contact_person_phone = forms.CharField(required=True, label=_("Contact person phone"))
    contact_person_email = forms.CharField(required=True, label=_("Contact person email"))

    address_country = forms.CharField(required=False, label=_("Visiting address country (optional)"))
    address = forms.CharField(required=False, label=_("Visiting address"), widget=forms.Textarea())
    address_postal_code = forms.CharField(required=False, label=_("Visiting address postal code"))
    address_coords = forms.CharField(required=False, label=_("Visiting address coordinates (on map)"))
    address_phone = forms.CharField(required=False, label=_("Visiting address phone number"))

    class Meta:
        model = OrganizationUnit
        fields = (
            'contact_person_name',
            'contact_person_phone',
            'contact_person_email',
            'address_country',
            'address',
            'address_postal_code',
            'address_coords',
            'address_phone'
        )


class OrganizationUnitBasicsForm(forms.ModelForm):
    """
    OrganizationUnit basic details form
    """

    class Meta:
        model = OrganizationUnit
        fields = ('name', 'type')


class OrganizationUnitUpdateForm(forms.ModelForm):
    """
    OrganizationUnit form
    """

    class Meta:
        model = OrganizationUnit
        fields = ('name', 'type')


class ProductUpdateForm(forms.ModelForm):
    """
    Product update form
    """

    class Meta:
        model = Product
        fields = (
            'remarks',
        )
        widgets = {
            'product_type': ProductTypeSelect
        }


class ProductBasicsForm(forms.ModelForm):
    """
    Product basics section form
    """

    class Meta:
        model = Product
        fields = (
            'product_type',
            'remarks'
        )
        required = (
            'product_type',
        )
        labels = {
            'product_type': _("Crop type"),
            'remarks': _("Remarks")
        }
        widgets = {
            'product_type': ProductTypeSelect
        }


class ProductIdentifierForm(forms.ModelForm):
    class Meta:
        model = ProductIdentifier
        widgets = {
            "uuid": HiddenInput()
        }
        exclude = ("product",)

    def __init__(self, *args, **kwargs):
        super(ProductIdentifierForm, self).__init__(*args, **kwargs)
        self.fields['identifier_type'].widget.attrs.update(
            {'data-bind': 'value: identifier_type'}
        )
        if self.data and self.data['identifier_type'] != 'other':
            self.fields.pop('identifier_type_name')


ProductIdentifierFormset = inlineformset_factory(
    Product,
    ProductIdentifier,
    fields=(
        'identifier_type',
        'identifier_type_name',
        'value'
    )
)


class ChoiceFieldOther(forms.ChoiceField):
    """
    Choice field with 'Other' option. Validates to proper type or 'other' string.
    """

    def get_choices(self):
        choices = self.choices + [('other', _("Other..."))]
        return choices

    def __init__(self, *args, **kwargs):
        super(ChoiceFieldOther, self).__init__(*args, **kwargs)
        self.choices += [('other', _("Other..."))]

        print
        print 'choices', self.choices
        print

    def to_python(self, value):
        # If the value is 'other', return it like this, skipping regular checking and type transform
        if value == 'other':
            return value
        else:
            obj = super(ChoiceFieldOther, self).to_python(value)
            return obj


class CertificationForm(forms.ModelForm):
    certification_type_pk = forms.ChoiceField(choices=[], label=_('Certificate type'))
    issue_date = forms.DateField(
        required=False,
        widget=forms.DateInput(attrs={'class': 'datepicker'}),
        input_formats=settings.DATE_INPUT_FORMATS,
        label=_("Issue date"),
        localize=True
    )
    expiry_date = forms.DateField(
        required=False,
        widget=forms.DateInput(attrs={'class': 'datepicker'}),
        input_formats=settings.DATE_INPUT_FORMATS,
        label=_("Expiry date"),
        localize=True
    )

    class Meta:
        model = Certification
        fields = [
            'certification_type_pk',
            'certificate_number',
            'issue_date',
            'expiry_date',
            'issuer',
        ]

    def __init__(self, *args, **kwargs):
        super(CertificationForm, self).__init__(*args, **kwargs)
        certification_type_choices = [
            (certification_type.pk, certification_type.name)
            for certification_type in CertificationType.objects.filter(verified=True).order_by('name').all()
        ]
        self.fields['certification_type_pk'].choices = certification_type_choices

    def clean_certification_type_pk(self):
        # Convert certification_type_pk into certification_type if it's not 'other'
        # If the ID doesn't match a CertificationType, this will raise an exception
        pk = self.cleaned_data['certification_type_pk']
        certification_type = CertificationType.objects.get(pk=pk)
        self.cleaned_data['certification_type'] = certification_type
        result = certification_type
        return result

    def clean_new_certification_name(self):
        # Don't allow duplicating validated certifications (already in the dropdown list)
        data = self.cleaned_data.get('new_certification_name')
        if data:
            existing = CertificationType.objects.filter(name=data).filter(verified=True)
            if existing.exists():
                raise forms.ValidationError(_("Certification type already exists. Please select it from the list."))
        return data

    def clean(self):
        # Do not allow 'other' option without specifying a new certification name
        if self.cleaned_data.get('certification_type_pk') == 'other':
            if not self.cleaned_data['new_certification_name']:
                errors = self._errors.setdefault("new_certification_name", ErrorList())
                errors.append(_('Certification name is required'))
        return self.cleaned_data

    @transaction.atomic
    def save(self, commit=False):
        # If needed, create CertificationType first
        if self.cleaned_data['certification_type_pk'] == 'other':
            new_cert_name = self.cleaned_data['new_certification_name']
            try:
                # Certification type with this name already exists
                certification_type = CertificationType.objects.get(name=new_cert_name)
            except CertificationType.DoesNotExist:
                # New certification type name, create a new object
                certification_type = CertificationType(name=new_cert_name, verified=False)
                certification_type.save()
            self.cleaned_data['certification_type'] = certification_type
        else:
            certification_type = self.cleaned_data['certification_type']
        # Save certification
        certification = super(CertificationForm, self).save(commit=False)
        # Assign certification_type manually because this form's certification_type field is custom and not
        # bound to the object field.
        certification.certification_type = certification_type
        certification.save()
        return certification


class OrgCertificationForm(CertificationForm):
    def __init__(self, org_slug=None, *args, **kwargs):
        super(OrgCertificationForm, self).__init__(**kwargs)


class OrgUnitCertificationForm(CertificationForm):
    class Meta:
        model = Certification
        fields = CertificationForm.Meta.fields + ['organization_unit']

    def __init__(self, org_slug=None, *args, **kwargs):
        super(OrgUnitCertificationForm, self).__init__(**kwargs)
        self.fields['organization_unit'] = forms.ModelChoiceField(
            queryset=OrganizationUnit.objects.filter(
                organization__slug=org_slug),
            label=_("Organization Unit")
        )


class ProductCertificationForm(CertificationForm):
    class Meta:
        model = Certification
        fields = CertificationForm.Meta.fields + ['product']

    def __init__(self, org_slug=None, *args, **kwargs):
        super(ProductCertificationForm, self).__init__(**kwargs)
        self.fields['product'] = forms.ModelChoiceField(
            label=_("Crop"),
            required=True,
            queryset=Product.objects.filter(organization__slug=org_slug),
        )


class ProductTypeAdminForm(forms.ModelForm):
    class Meta:
        model = ProductType
        exclude = ()
        widgets = {
            'field_color': ColorPickerWidget
        }
        exclude = ()
