import logging

from rest_framework.renderers import JSONRenderer
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _

from assessments.models import Questionnaire
from organization.models import Product, ProductIdentifier, Organization
from sams.constants import ICONS
from sams.context_processors import questionnaire_form_context_processor
from sams.flipsection import FlipSectionManager
from ap_extras.item_list import ItemListModel, ItemListColumn
from sams.decorators import session_organization_required, organization_required, membership_required

from .api.internal.serializers import ProductTypeSerializer
from .forms import (
    ProductUpdateForm,
    ProductIdentifierForm,
    ProductBasicsForm
)
from .models import (
    ProductType
)
from organization.views import get_organization_context

logger = logging.getLogger(__name__)

"""
Views for organization section, product pages
"""


#################################################################################
# Common
#################################################################################


def get_product_context(request, kwargs, page_name=""):
    context, organization = get_organization_context(request, kwargs, page_name)
    assert 'product_pk' in kwargs
    try:
        product = organization.products.get(pk=kwargs['product_pk'])
    except Product.DoesNotExist:
        raise Http404(_("Product does not exist."))
    unit = product.organization_unit
    context.update({
        'product': product,
        'unit': unit,
        'page_title': product.__unicode__,
        'page_title_secondary': _("crop"),
        'page_icon': ICONS['product'],
        'breadcrumbs': [{
            'text': _("Products"),
            'url': reverse('my_product_list', args=[organization.slug]),
            'icon': ICONS['product']
        }, {
            'text': product.name,
            'icon': ICONS['product']
        }],
    })
    context['page_triggers'] += ['my_product_list']
    return context, organization, product


def get_identifier_context(request, kwargs, page_name=""):
    try:
        identifier = ProductIdentifier.objects.get(pk=kwargs['identifier_pk'])
    except ProductIdentifier.DoesNotExist:
        raise Http404(_("Product identifier does not exist."))
    context, organization, product = get_product_context(
        request, kwargs, "edit_product_identifier")
    context['identifier'] = identifier
    return context, organization, product, identifier


def get_product(organization_slug, unit_pk, product_pk):
    """
    Retrieves product by organization/unit/product IDs.
    :returns: (organization, unit, product) tuple
    """
    organization = get_object_or_404(Organization, slug=organization_slug)
    try:
        unit = organization.units.get(pk=unit_pk)
    except:
        raise Http404("Unit %s not found" % unit_pk)
    try:
        product = unit.products.get(pk=product_pk)
    except:
        raise Http404("Product %s not found" % unit_pk)
    return (organization, unit, product)


#################################################################################
# Views
#################################################################################


@login_required
@membership_required
def product_list(request, *args, **kwargs):
    """
    Product list view
    """
    context, organization = get_organization_context(request, kwargs, "my_product_list")
    context['breadcrumbs'].append({
        'text': _("Products"),
        'icon': ICONS['product']
    })
    context.update({
        'page_title': _("Crops"),
        'page_title_secondary': None,
        'page_title_url': None,
        'page_icon': ICONS['product']
    })

    products_model = ItemListModel()
    products_model.id = "products"
    products_model.items = organization.products.all()
    products_model.columns = [
        ItemListColumn(
            header=_("Crop"),
            text=lambda product: product.__unicode__,
            url=lambda product: reverse(
                'my_product', kwargs={'membership_pk': request.membership.pk, 'product_pk': product.pk}
            ),
            icon_class=lambda assessment: ICONS['product']
        ),
        ItemListColumn(
            header=_("Remarks"),
            text=lambda product: product.remarks or '',
        )
    ]
    products_model.hide_toolbar = True
    products_model.no_items_text = _("No crops available.")
    products_model.refresh()

    context['product_type_list_json'] = get_product_type_list()
    context['products_model'] = products_model

    return render(request, "product/product_list.html", context)


@login_required
def product_home(request, *args, **kwargs):
    """
    Product section entry point
    """
    return redirect("my_product_detail", *args, **kwargs)


@login_required
@session_organization_required
def product_overview(request, *args, **kwargs):
    """
    Product Overview page
    """
    page_name = "my_product_overview"
    context, organization, product = get_product_context(request, kwargs)
    context["page_triggers"] += [page_name]
    context["product_details"] = []
    return render(request, "product/product_overview.html", context)


@login_required
@session_organization_required
def product_detail(request, *args, **kwargs):
    """
    Organization detail (view organization profile) + edit
    """
    context, organization, product = get_product_context(request, kwargs, 'my_product_detail')
    section_models = context['section_models'] = {
        'basics': {
            'form_class': ProductBasicsForm,
            'title': _("Crop details"),
            'url': reverse('my_product_update', args=(organization.pk, product.pk,))
        },
    }
    context['identifiers'] = ProductIdentifier.objects.filter(product=product)
    flip_manager = FlipSectionManager(product, section_models)
    context['product_type_list_json'] = get_product_type_list()

    if request.method == 'POST':
        (success, url_hash) = flip_manager.save(request.POST, request.FILES)
        if success:
            messages.success(request, _("{product} saved.").format(product=product))
        else:
            messages.error(
                request,
                _("Could not save {product}. Please fix the errors below.".format(product=product.name))
            )
        return redirect("{0}{1}".format(request.get_full_path(), url_hash))
    return render(request, 'product/product_detail.html', context)


@login_required
@session_organization_required
def product_update(request, *args, **kwargs):
    """
    Product update page
    """
    context, organization, product = get_product_context(request, kwargs, page_name='my_product_update')
    product_form = ProductUpdateForm(instance=product)

    if request.method == "POST":
        product_form = ProductUpdateForm(request.POST, instance=product)
        if product_form.is_valid():
            product = product_form.save()
            messages.success(request, _("%s was saved.") % product.name)
            return redirect('my_product_detail', organization.slug, product.pk)
    else:
        # GET
        context['product_form'] = product_form
    return render(request, 'product/product_update.html', context)


@login_required
@session_organization_required
def product_factsheet_detail(request, *args, **kwargs):
    """
    Product fact sheet detail page
    """
    context, organization, product = get_product_context(request, kwargs, page_name="my_product_fact_sheet_detail")
    # Generic my_product_fact_sheet trigger is shared between ~_detail and ~_edit views
    context["page_triggers"] += ["my_product_fact_sheet"]

    fact_sheet = Questionnaire.objects.get(code="_fact_sheet_product")
    fact_sheet_detail = fact_sheet.get_detail(target_uuid=product.uuid)
    context["fact_sheet_detail"] = fact_sheet_detail
    return render(request, "product/product_factsheet_detail.html", context)


@login_required
@session_organization_required
def product_factsheet_edit(request, *args, **kwargs):
    """
    Product fact sheet edit page
    """
    context, organization, product = get_product_context(request, kwargs, page_name="my_product_fact_sheet_edit")
    # Generic my_product_fact_sheet trigger is shared between ~_detail and ~_edit views
    context["page_triggers"] += ["my_product_fact_sheet"]

    questionnaire = Questionnaire.objects.get(code="_fact_sheet_product")
    # QuestionnaireForm context work, including save on POST
    if questionnaire_form_context_processor(request, context, questionnaire, target=product):
        # Success
        return redirect("my_product_fact_sheet_detail", product_pk=product.pk)
    else:
        # First time or form errors
        return render(request, "product/product_factsheet_edit.html", context)


@login_required
@session_organization_required
def product_delete(request, *args, **kwargs):
    """
    Product delete page
    """
    context, organization, product = get_product_context(request, kwargs, page_name="my_product_delete")
    context.update({
        "title": _("Delete %s") % unicode(product),
        "message": _("Are you sure you want to delete the product %s?") % unicode(product),
        "yes_text": _("Yes, delete %s") % unicode(product),
        "no_text": _("Cancel"),
        "no_url": product.get_absolute_url()
    })

    if request.method == 'POST':
        product.delete()
        messages.success(request, _("Product deleted."))
        return redirect("my_product_list", organization.slug)
    else:
        # GET
        return render(request, "core/confirmation.html", context)


def get_product_type_list():
    product_types = ProductType.objects.all()
    product_type_list = {
        'product_types': ProductTypeSerializer(product_types, many=True).data,
    }
    product_type_list_json = JSONRenderer().render(product_type_list)
    return product_type_list_json


@login_required
@session_organization_required
def product_create(request, *args, **kwargs):
    """
    Product create view
    """
    context, organization = get_organization_context(request, kwargs, "my_product_create")
    context['product_type_list_json'] = get_product_type_list()

    if request.method == 'POST':
        # POST
        form = ProductBasicsForm(request.POST)
        if form.is_valid():
            new_product = Product(**form.cleaned_data)
            new_product.organization = organization
            new_product.save()
            messages.success(request, _("The product was created."))
            # Great success!
            return redirect("my_product_list", organization.slug)
    else:
        # GET, etc
        form = ProductBasicsForm()
    context['form'] = form
    return render(request, "product/product_create.html", context)


@login_required
@session_organization_required
def create_product_identifier(request, *args, **kwargs):
    context, organization, product = get_product_context(
        request, kwargs, "create_product_identifier")
    if request.method == 'GET':
        form = ProductIdentifierForm()
    elif request.method == 'POST':
        form = ProductIdentifierForm(request.POST)
        if form.is_valid():
            identifier = form.save(commit=False)
            identifier.product = product
            identifier.save()
            messages.success(request, _("The product identifier was created."))
            return redirect("my_product", organization.slug, product.pk)

    context['form'] = form

    return render(request, "product/product_identifier_create.html", context)


@login_required
@session_organization_required
def edit_product_identifier(request, *args, **kwargs):
    context, organization, product, identifier = get_identifier_context(
        request, kwargs, "edit_product_identifier")
    if request.method == 'GET':
        context['form'] = ProductIdentifierForm(instance=identifier)
    if request.method == 'POST':
        form = ProductIdentifierForm(request.POST, instance=identifier)
        if form.is_valid():
            identifier = form.save(commit=False)
            identifier.product = product
            identifier.save()
            messages.success(request, _("The product identifier was created."))
            return redirect("my_product", organization.slug, product.pk)
    return render(request, "product/product_identifier_edit.html", context)


@login_required
def delete_product_identifier(request, *args, **kwargs):
    context, organization, product, identifier = get_identifier_context(
        request, kwargs, "delete_product_identifier")
    context.update({
        "title": _("Delete product identifier") % identifier,
        "page_icon": ICONS["certification"],
        "message": _(
            "Are you sure you want to delete the product identifier <strong>&lt;%s&gt;</strong>?") % identifier,
        "yes_text": _("Yes, delete"),
        "no_text": _("Cancel"),
        "no_url": reverse("my_product", args=[organization.slug, product.pk])
    })
    if request.method == 'POST':
        ProductIdentifier.objects.get(pk=identifier.pk).delete()
        messages.success(request, _("Product identifier deleted."))
        return redirect("my_product", organization.slug, product.pk)
    else:
        # GET
        return render(request, "default/../core/templates/core/confirmation.html", context)
