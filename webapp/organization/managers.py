from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from core.constants import EXTERNAL_AUDITOR


def get_organization_type_choices():
    """
    Get organization type choices, sorted dynamically by translated label
    """
    ORGANIZATION_TYPE_CHOICES = (
        ('producer', _("Producer/Farmer")),
        ('coop', _("Producer group / Cooperative")),
        ('processor', _("Processor / Manufacturer")),
        ('trader', _("Trader")),
        ('licensee', _("Licensee / Brand owner")),
        ('auditor', _("Auditor")),
        ('other', _("Other")),
        ('buyer', _("Buyer")),
    )
    return sorted(ORGANIZATION_TYPE_CHOICES, key=lambda choice: choice[1])


class OrganizationThemeManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class OrganizationTypeManager(models.Manager):
    def get_by_natural_key(self, slug):
        return self.get(slug=slug)


class OrganizationManager(models.Manager):
    def get_users_in_organization(self, name):
        from organization.models import OrganizationMembership  # <--- WHY IMPORT IS HERE?

        users_context = []
        primary_organization = self.get(name=name)
        secondary_ids = OrganizationMembership.objects.filter(
            primary_organization=primary_organization).distinct().values_list(
            'secondary_organization', flat=True
        )

        for secondary_id in secondary_ids:
            users_context.append(User.objects.filter(organizations__uuid=secondary_id).get())

        return users_context

    def get_user_in_organization(self, organization_name, user_id=None, email=None):

        if user_id is not None:
            users_context = self.get_users_in_organization(organization_name)
            user = next((u for u in users_context if u.pk == int(user_id)), None)

        if email is not None:
            users_context = self.get_users_in_organization(organization_name)
            print(len(users_context))  # <-- DEBUG?
            user = next((u for u in users_context if u.email == email), None)

        return user

    def count_users_in_organization(self, name):
        from organization.models import OrganizationMembership  # <--- WHY IMPORT IS HERE?
        users_context = []
        primary_organization = self.get(name=name)
        secondary_ids = OrganizationMembership.objects.filter(
            primary_organization=primary_organization).distinct().values_list(
            'secondary_organization', flat=True
        )

        for secondary_id in secondary_ids:
            users_context.append(list(User.objects.filter(organizations__uuid=secondary_id)))

        return len(users_context)

    def get_external_auditors(self, organization):
        return self.filter(
            groups__name='external_auditor',
            mailing_country=organization.mailing_country
        )


class PlotRVOManager(models.Manager):
    def get_queryset(self):
        return super(PlotRVOManager, self).get_queryset()


class OrganizationMembershipManager(models.Manager):
    def get_partner_organizations(self, organization, search_term='', organization_type_name=None):
        primary_organization_ids = self.filter(
            secondary_organization=organization,
        ).distinct().values_list('primary_organization', flat=True)

        query = self.filter(
            primary_organization__uuid__in=primary_organization_ids
        )

        if organization_type_name:
            query = query.filter(organization_type__name=organization_type_name)

        if search_term:
            query = query.filter(secondary_organization__name__icontains=search_term)

        query = query.exclude(secondary_organization=organization).distinct().values_list(
            'secondary_organization',
            'secondary_organization__name',
            'secondary_organization__slug',
            'secondary_organization__mailing_city',
            'organization_type__name',
            "uuid"
        )
        return query

    def get_external_auditors(self, membership):
        from organization.models import Organization
        user_organization = membership.secondary_organization
        farmgroup = membership.primary_organization
        ids = self.filter(
            role__name=EXTERNAL_AUDITOR,
            secondary_organization__mailing_country=user_organization.mailing_country,
            primary_organization=farmgroup
        ).values_list('secondary_organization', flat=True)
        return Organization.objects.filter(uuid__in=ids)
