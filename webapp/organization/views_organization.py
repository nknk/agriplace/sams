import logging

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from translations.helpers import tolerant_ugettext
from assessments.models import Questionnaire
from organization.models import OrganizationAccessInvitation
from sams.constants import ICONS
from sams.context_processors import questionnaire_form_context_processor
from sams.decorators import membership_required
from app_settings.utils import get_help_text_block
from organization.forms import *
from organization.views import get_organization_context

logger = logging.getLogger(__name__)

"""
Views for organization section
"""


###
# COMMON
###

def PLACEHOLDER(request, pk=1):
    return render(request, "placeholder.html")


def get_organization_overview_context(request, kwargs, page_name=""):
    context, organization = get_organization_context(request, kwargs, page_name)
    context['page_triggers'] += ('my_org',)
    return context, organization


#################################################################################
# Organizations
#################################################################################

@login_required
@membership_required
def organization_home(request, *args, **kwargs):
    """
    Organization section entry point
    """
    return redirect('my_org_overview', membership_pk=request.membership.pk)


@login_required
@membership_required
def organization_overview(request, *args, **kwargs):
    """
    Organization overview (home page)
    """
    context, organization = get_organization_overview_context(request, kwargs, "my_org_overview")

    # Assessments
    assessments = organization.assessments.all()
    context["num_assessments"] = assessments.count()
    context["num_open_assessments"] = assessments.filter(state="open").count()
    context["num_closed_assessments"] = assessments.filter(state="closed").count()
    context["recent_assessments"] = assessments.order_by("modified_time")[:5]

    context['num_units'] = organization.units.count()
    context['num_products'] = organization.products.count()
    context['num_partners'] = organization.partners.count()

    context['breadcrumbs'] += [
        {
            'text': _("Organization"),
            'icon': ICONS['organization']
        }
    ]

    organization_details = []

    context['product_name_list'] = []
    for product in organization.products.all():
        context['product_name_list'].append(tolerant_ugettext(product.product_type_name()))

    context["organization_details"] = organization_details
    context['help_link_url'] = get_help_text_block("profile")

    return render(request, "organization/organization_overview.html", context)


@login_required
@membership_required
def organization_delete(request, *args, **kwargs):
    """
    Organization delete
    """
    context, organization = get_organization_context(request, kwargs, "my_org_delete")
    # Variables for generic confirmation template
    context.update({
        "title": _("Delete {}").format(organization),
        "page_icon": ICONS["organization"],
        "message": _("Are you sure you want to delete {}?").format(organization),
        "yes_text": _("Yes, delete {}").format(organization),
        "no_text": _("Cancel"),
        "no_url": reverse("my_org_detail", kwargs={"organization_slug": organization.slug})
    })
    if request.method == "POST":
        organization.delete()
        messages.success(request, _("Deleted {}.").format(organization.name))
        return redirect("organization_list")

    return render(request, "core/confirmation.html", context)


@login_required
@membership_required
def organization_detail(request, *args, **kwargs):
    """
    Organization detail (view organization profile) + edit
    """
    context, organization = get_organization_context(
        request, kwargs, "my_org_detail")

    context['organization_basics_form'] = OrganizationBasicsForm(instance=organization)
    context['organization_contact_form'] = OrganizationContactForm(instance=organization)

    context['form_sections'] = {
        'basics': {
            'form': OrganizationBasicsForm(instance=organization),
            'title': _("Organization"),
            'url': reverse('my_org_form_basics', args=(organization.pk,))
        },
        'location': {
            'form': OrganizationContactForm(instance=organization),
            'title': _("Contact details"),
            'url': reverse('my_org_form_contact', args=(organization.pk,))
        },
    }
    return render(request, 'organization/organization_detail.html', context)


@login_required
@membership_required
def organization_form_basics(request, *args, **kwargs):
    context, organization = get_organization_context(
        request, kwargs, 'my_org_detail')
    form_class = OrganizationBasicsForm

    if request.method == 'POST':
        # POST form
        form = form_class(request.POST, request.FILES, instance=organization)
        if form.is_valid():
            form.save()
            return redirect('my_org_detail', organization.pk)

    else:
        # GET
        form = form_class(instance=organization)

    context['form'] = form
    return render(request, 'organization/organization_form.html', context)


@login_required
@membership_required
def organization_form_contact(request, *args, **kwargs):
    context, organization = get_organization_context(
        request, kwargs, 'my_org_detail')
    form_class = OrganizationContactForm

    if request.method == 'POST':
        # POST form
        form = OrganizationContactForm(request.POST, instance=organization)
        if form.is_valid():
            form.save()
            return redirect('my_org_detail', organization.pk)

    else:
        # GET
        form = OrganizationContactForm(instance=organization)

    context['form'] = form
    return render(request, 'organization/organization_form.html', context)


@login_required
@membership_required
def organization_update(request, *args, **kwargs):
    """
    Organization update (edit) page
    """
    context, organization = get_organization_context(request, kwargs, 'my_org_detail')
    if request.method == 'POST':
        form = OrganizationUpdateForm(request.POST, request.FILES, instance=organization)
        if form.is_valid():
            form.save()
            messages.success(request, _("{} saved.").format(organization.name))
            return redirect("my_org_overview", membership_pk=request.membership.pk)
        else:
            messages.error(request, _("The organization was not saved. Please fix the errors below."))
    else:
        # GET
        form = OrganizationUpdateForm(instance=organization)
    context['form'] = form
    return render(request, 'organization/organization_form.html', context)


@login_required
@membership_required
def organization_factsheet_detail(request, *args, **kwargs):
    """
    Organization fact sheet detail page
    """
    context, organization = get_organization_context(request, kwargs, "my_org_fact_sheet_detail")
    context['page_triggers'] += ["my_org_fact_sheet"]
    fact_sheet = Questionnaire.objects.get(code="_fact_sheet_organization")
    context["fact_sheet"] = fact_sheet
    fact_sheet_detail = fact_sheet.get_detail(target_uuid=organization.uuid)
    context["fact_sheet_detail"] = fact_sheet_detail
    # questionnaire_items
    return render(request, "organization/organization_fact_sheet_detail.html", context)


@login_required
@membership_required
def organization_factsheet_edit(request, *args, **kwargs):
    context, organization = get_organization_context(request, kwargs, "my_org_fact_sheet_detail")
    context['page_triggers'] += ["my_org_fact_sheet"]
    context["questionnaire_target"] = organization
    questionnaire = Questionnaire.objects.get(code="_fact_sheet_organization")
    context["back_url"] = reverse("my_org_fact_sheet_detail", kwargs={'organization_slug': organization.slug})

    # QuestionnaireForm context work, including save on POST
    if questionnaire_form_context_processor(request, context, questionnaire, target=organization):
        # Success
        return redirect("my_org_fact_sheet_detail", organization_slug=organization.slug)
    else:
        # First time or form errors
        return render(request, "organization/organization_fact_sheet_edit.html", context)


def update_organization_access(organization, access_levels,
                               current_user, add_request_message,
                               build_absolute_uri=lambda url: url):
    for (email, access) in access_levels.items():
        if not email or not access:
            continue
        # Find user that matches this email
        try:
            user = get_user_model().objects.get(email=email)
        except:
            user = None

        if access == "none":
            # Removing access
            if user == current_user:
                add_request_message(messages.WARNING,
                                    _(
                                        "You cannot remove your own access rights from organization %s!") % organization.name
                                    )
            else:
                organization.users.remove(user)
                add_request_message(messages.SUCCESS,
                                    _("User %(user)s was removed from organization %(organization)s.") %
                                    {"user": email, "organization": organization.name}
                                    )
        if access == "full":
            # Adding access
            # If the email corresponds to a user in the system, add
            if user:
                if not user in organization.users.all():
                    # Only add new users
                    organization.users.add(user)
                    add_request_message(messages.SUCCESS,
                                        _("User %(user)s was added to organization %(organization)s.") %
                                        {"user": email, "organization": organization.name}
                                        )
                    message = EmailMessage()
                    message.from_email = settings.SAMS_EMAIL_FROM
                    message.to = [user.email]
                    message.subject = "SAMS - welcome to %s" % organization.name
                    message.body = (
                                       "Hi %(full_name)s!\n\n"
                                       "You have been added to %(org_name)s.\n\n") % {
                                       'full_name': user.get_full_name(),
                                       'org_name': organization.name,
                                   }
                    message.send()
            else:
                add_request_message(messages.SUCCESS,
                                    _("User %(email)s is not in the database, so an "
                                      "invitation email has been sent.") % {"email": email, }
                                    )
                invitation = OrganizationAccessInvitation.objects.create(
                    organization=organization,
                    email=email,
                    requested_by=current_user)
                message = EmailMessage()
                message.from_email = settings.SAMS_EMAIL_FROM
                message.to = [email]
                message.subject = (
                                      "%(user_name)s invites you to join "
                                      "%(org_name)s on SAMS") % {
                                      'user_name': current_user.get_full_name(),
                                      'org_name': organization.name,
                                  }
                message.body = (
                                   "Hi %(email)s!\n\n"
                                   "%(user_name)s added you to %(org_name)s, but you don't "
                                   "have an account yet. Create one at %(url)s!\n\n") % {
                                   'user_name': current_user.get_full_name(),
                                   'email': email,
                                   'org_name': organization.name,
                                   'url': build_absolute_uri(reverse('auth_login')),
                                   # TODO we should link to signup page, not login page!
                               }
                message.send()
    return messages


@login_required
@membership_required
def organization_access(request, *args, **kwargs):
    """
    Organization access page
    """
    context, organization = get_organization_context(request, kwargs, "my_org_access")

    context.update({
        'page_title': _("Access rights"),
        'page_title_secondary': None,
        'page_title_url': None,
        'page_icon': ICONS['access']
    })

    if request.method == 'POST':
        # Save access

        # Turn the form variables back into a dict
        # 	because Django only returns the last value for multivalues.
        form_dict = dict(request.POST.lists())
        access_levels = dict(zip(form_dict['user_email'], form_dict['user_access']))

        # Email addresses of people not registered with the application.
        # Addresses should be remembered and users should receive an invitation message.
        emails_to_invite = []

        def add_message(*args):
            messages.add_message(request, *args)

        update_organization_access(organization, access_levels,
                                   request.user, add_message,
                                   request.build_absolute_uri)
        organization.save()
    else:
        # GET
        context['access_invitations'] = OrganizationAccessInvitation.objects.filter(organization=organization)
    return render(request, "organization/organization_access.html", context)


@receiver(post_save, sender=get_user_model())
def grant_invited_access(sender, instance, **kwargs):
    for invitation in (OrganizationAccessInvitation.objects
                               .filter(email=instance.email)):
        invitation.organization.users.add(instance)
        invitation.delete()


@login_required
@membership_required
def my_database(request, *args, **kwargs):
    context, organization = get_organization_context(request, kwargs, 'my_database')
    return render(request, 'organization/my_database.html', context)
