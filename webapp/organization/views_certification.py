import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _
from django.views.generic import View

from app_settings.utils import get_text_blocks
from organization.models import (
    Certification,
    Organization,
    OrganizationUnit,
    Product,
)
from sams.constants import ICONS
from sams.decorators import session_organization_required, membership_required
from sams.view_mixins import SessionOrgRequired
from organization.forms import (
    OrgUnitCertificationForm,
    ProductCertificationForm
)
from organization.views import get_organization_context
from organization.forms import CertificationForm, OrgCertificationForm

logger = logging.getLogger(__name__)


class CertificateCreator(SessionOrgRequired, View):
    target_type = None
    form_class = None

    def get_context(self, form):
        return dict(form=form)

    def render(self, request, form):
        return render(
            request,
            "organization/certification/certification_create.html",
            self.get_context(form))

    def get(self, request, *args, **kwargs):
        form = self.form_class(org_slug=kwargs.get('organization_slug'))
        return self.render(request, form)

    def post(self, request, *args, **kwargs):
        form = self.form_class(
            data=request.POST,
            org_slug=kwargs.get('organization_slug')
        )
        if form.is_valid():
            certification = form.save(commit=False)
            certification.organization = get_object_or_404(
                Organization, slug=kwargs.get('organization_slug'))
            certification.save()
            return redirect(
                "my_certification_list",
                organization_slug=kwargs.get('organization_slug'),
            )
        else:
            messages.warning(request, _("Form not valid."))
        return self.render(request, form)


class OrgCertificateCreator(CertificateCreator):
    target_type = Organization
    form_class = OrgCertificationForm

    def get_context(self, form):
        context, _ = get_organization_context(self.request, self.kwargs, 'my_org_certification_create')
        context['form'] = form
        return context


class OrgUnitCertificateCreator(CertificateCreator):
    target_type = OrganizationUnit
    form_class = OrgUnitCertificationForm

    def get_context(self, form):
        context, organization = get_organization_context(
            self.request, self.kwargs, 'my_orgunit_certification_create')
        context['form'] = form
        return context


class ProductCertificateCreator(CertificateCreator):
    target_type = Product
    form_class = ProductCertificationForm

    def get_context(self, form):
        context, _ = get_organization_context(
            self.request, self.kwargs, 'my_product_certification_create')
        context['form'] = form
        return context


def get_page_title_url(certification):
    page_title_view = {
        Product: dict(
            name="my_product_detail",
            args=[certification.organization.slug, certification.target.pk]
        ),
        OrganizationUnit: dict(
            name="my_unit_detail",
            args=[certification.organization.slug, certification.target.pk]
        ),
        Organization: dict(
            name="my_org",
            args=[certification.organization.slug]
        )
    }
    view = page_title_view.get(
        certification.target.__class__,
        page_title_view.get('Organization')
    )
    return reverse(view.get('name'), args=view.get('args'))


def get_certification_context(request, kwargs, page_name=""):
    context, organization = get_organization_context(request, kwargs, page_name)
    assert 'certification_pk' in kwargs
    try:
        certification = organization.certifications.get(pk=kwargs['certification_pk'])
    except Certification.DoesNotExist:
        raise Http404(_("Certification does not exist"))
    # Url args used by product views
    certification_kwargs = {
        'organization_slug': organization.slug,
        'certification_pk': certification.pk
    }
    context.update({
        'certification': certification,
        'page_title': certification.certification_type.name,
        'page_title_url': get_page_title_url(certification),
        'page_icon': ICONS['certification'],
        'page_title_secondary': _("certificate"),
        'page_triggers': ['organization', page_name]
    })
    context['breadcrumbs'] += [
        {
            'text': _("Certificates"),
            'icon': ICONS['certification'],
            'url': reverse('my_certification_list', args=[organization.slug])
        },
        {
            'text': certification.certification_type,
            'icon': ICONS['certification']
        }
    ]
    return context, organization, certification


@login_required
@membership_required
def certification_list(request, *args, **kwargs):
    """
    Organization certifications page. Also used for OrganizationUnit and Product.
    """
    context, organization = get_organization_context(request, kwargs, 'my_certification_list')
    context['certifications'] = organization.certifications.exclude(
        certification_type__isnull=True
    ).order_by('certification_type__name')

    context['page_triggers'] = ['organization', 'my_certification_list']
    context['breadcrumbs'].append(
        {
            'text': _("Certificates"),
            'icon': ICONS['certification']
        }
    )
    context['text_blocks'] = get_text_blocks(('certificate_list_intro',))
    return render(request, "organization/certification/certification_list.html", context)


@login_required
@session_organization_required
def certification_detail(request, *args, **kwargs):
    """
    Certification detail
    """
    context, _, certification = get_certification_context(request, kwargs, 'my_certification_detail')
    context['details'] = [
        (_("Certificate type"), certification.certification_type.name),
        (_("Certificate target"), "%s" % (certification.target)),
        (_("Issue date"), certification.issue_date or ''),
        (_("Expiry date"), certification.expiry_date or ''),
        (_("Issued by"), certification.issuer),
    ]
    return render(request, "organization/certification/certification_detail.html", context)


def certification_create(request, *args, **kwargs):
    """
    Certification create
    """
    context = CertificateCreator(request, *args, **kwargs).context
    return render(request, "organization/certification/certification_create.html", context)


@login_required
@session_organization_required
def certification_edit(request, *args, **kwargs):
    """
    Certification edit view
    """
    context, organization, certification = get_certification_context(request, kwargs, 'my_certification_edit')
    certification = context['certification']
    if request.method == 'POST':
        # POST
        form = CertificationForm(request.POST, instance=certification)
        if form.is_valid():
            form.save()
            messages.success(request, _("Certificate saved."))
            return redirect("my_certification_detail",
                            organization_slug=organization.slug, certification_pk=certification.pk)
    else:
        # GET etc
        form = CertificationForm(instance=certification)
    context['form'] = form
    return render(request, "organization/certification/certification_edit.html", context)


@login_required
@session_organization_required
def certification_delete(request, *args, **kwargs):
    """
    Certification delete view
    Uses generic confirmation template.
    """
    context, organization, certification = get_certification_context(request, kwargs, "my_certification_delete")
    # Add variables for generic confirmation template
    context.update({
        "title": _("Delete certificate"),
        "page_icon": ICONS["certification"],
        "message": _("Are you sure you want to delete the certificate <strong>&lt;%s&gt;</strong>?") % certification,
        "yes_text": _("Yes, delete"),
        "no_text": _("Cancel"),
        "no_url": reverse("my_certification_detail", args=[
            organization.slug, certification.pk
        ])
    })
    if request.method == 'POST':
        Certification.objects.get(pk=certification.pk).delete()
        messages.success(request, _("Certificate deleted."))
        return redirect("my_certification_list", organization_slug=organization.slug)
    else:
        # GET
        return render(request, "core/confirmation.html", context)
