//
// certification_create
//

// Page view model for Knockout
function ViewModel() {
    this.identifier_type = ko.observable('');
    this.identifier_type_name = ko.observable('');
    this.is_visible = ko.computed(function(){
        return this.identifier_type() == "other";
    }, this);
}
$(document).ready(function() {
    "use strict";
    ko.applyBindings(new ViewModel());
});
