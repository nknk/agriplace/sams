//
// certification_create
//

// Page view model for Knockout
var ViewModel = function() {
    var self = this;
    this.certification_type = ko.observable('');
    this.new_certification_type = ko.observable('');
    this.is_field_visible = function(field_name) {
        switch(field_name) {
            case 'new_certification_name':
                // Show new cert name only if certification is Other
                return self.certification_type() == 'other';
                break;
            default:
                // All other fields
                return true;
        }
    };
}
view_model = new ViewModel;

$(document).ready(function() {
    "use strict";

    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    ko.applyBindings(view_model);
});
