//
// JavaScript for My Organization module
//

(function() {
    ViewModel = function() {
        var self = this;

        this.hash = ko.observable('');

        this.sections = {
            business_card: {
                state: ko.observable('view')
            },
            contact: {
                state: ko.observable('view')
            }
        }

        // Functions

        this.is_state = function(section_name, state) {
            return this.sections[section_name].state() == state;
        }

        this.edit_section = function(name) {
            for(key in this.sections) {
                var section = this.sections[key];
                if(key == name) {
                    section.state('edit');
                }
                else {
                    section.state('view');
                }
            }
        }

        // Main hash changed event handler
        // Hash should be like #SECTION/view or #SECTION/edit
        this.hash.subscribe(function() {
            // Close all editors
            this.edit_section();
            if(this.hash() == '') {
                // Blank hash, do nothing.
                return;
            }
            var hash_segments = this.hash().split('/');
            var section = hash_segments[0];
            var mode = hash_segments[1];
            switch(mode) {
                case 'edit':
                    this.edit_section(section);
                    break;
                case 'view':
                    // Hide all edit sections
                    this.edit_section();
                    break;
            }
            // Scroll to given section
            // but don't scroll for business_card because it's the first one
            if(section != 'business_card') {
                $('#'+section)[0].scrollIntoView(true);
            }
        }, self);
    }

    // Event handler for URL hash change window event
    // Should only copy the hash into the view_model, KO does the rest
    var on_hash_changed = function() {
        // Keep the hash but remove the # to make it easier to combine later
        view_model.hash(window.location.hash.replace('#', ''));
    }

    // Watch out: not supported by IE7 and older
    window.onhashchange = function() {
        on_hash_changed();
    };

    $(document).ready(function() {
        view_model = new ViewModel();
        ko.applyBindings(view_model);
        // Apply initial URL hash
        on_hash_changed();
    });
})();
