#
# Organization API
#

module = angular.module('agriplace.organization.api', [
    'ngResource',
    'agriplace.jsContext'
])

# Products
module.factory('productsApi', [
    '$resource',
    'jsContext',
    ($resource, jsContext) ->
        url = "#{jsContext.API_URL}organization/organizations/:organizationUuid/products/"
        return $resource(url)
])


