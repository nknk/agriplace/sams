/*! AgriPlace - v0.1.1 - * Copyright (c) 2014 ; Licensed  */
(function() {
  var module;

  module = angular.module('agriplace.organization.api', ['ngResource', 'agriplace.jsContext']);

  module.factory('productsApi', [
    '$resource', 'jsContext', function($resource, jsContext) {
      var url;
      url = "" + jsContext.API_URL + "organization/organizations/:organizationUuid/products/";
      return $resource(url);
    }
  ]);

}).call(this);

//# sourceMappingURL=organization.js.map

(function() {
  var module;

  module = angular.module('agriplace.organization', []);

  module.service('productsManager', [
    function($modal) {
      var ProductsManager;
      return ProductsManager = (function() {
        function ProductsManager() {}

        ProductsManager.createProduct = function() {
          var modalDefaults, modalOptions;
          modalOptions = {};
          modalDefaults = {
            templateUrl: 'VIRTUAL/organization/product_create.html',
            controller: function($scope, $modalInstance) {}
          };
          return this.show(modalDefaults, modalOptions);
        };

        return ProductsManager;

      })();
    }
  ]);

}).call(this);

//# sourceMappingURL=organization_common.js.map

angular.module('agriplace.organization').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('VIRTUAL/organization/product_create.html',
    "<h1>Product create</h1>"
  );

}]);
