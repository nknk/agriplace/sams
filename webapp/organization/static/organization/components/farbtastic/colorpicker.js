var jQuery = django.jQuery;

var picker_widget = "<div id='colorpicker'></div>";

jQuery(document).ready(function() {
    var id_color = jQuery('#id_field_color');
    jQuery(picker_widget).appendTo('#id_color_picker');
    jQuery('#id_color_picker').bind('click', function() {
        var offset = id_color.offset();
        var helper_style = {
            'top': offset.top + id_color.height() + 3,
            'left': offset.left,
            'opacity': 0.9,
        };
        jQuery('#colorpicker').css(helper_style).toggle(400);
   });
    jQuery('#colorpicker').hide().farbtastic(id_color);
});