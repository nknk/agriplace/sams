import logging
from datetime import datetime

from ckeditor.fields import RichTextField
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save
from django.contrib.auth.models import Group
from django.conf import settings
from django.dispatch import receiver
from django.template.defaultfilters import slugify, truncatechars
from django.utils.text import Truncator
from django.utils.translation import ugettext_lazy as _
from mapnik import Coord, Projection, ProjTransform

from tagging_autocomplete.models import TagAutocompleteField

from assessments.models import Questionnaire
from core import countries
from core.constants import ORGANIZATION_TYPE_CHOICES, EXTERNAL_AUDITOR
from core.models import UuidModel
from translations.helpers import tolerant_ugettext
from .managers import OrganizationManager, PlotRVOManager, get_organization_type_choices, OrganizationMembershipManager, \
    OrganizationThemeManager, OrganizationTypeManager
from translations.templatetags.translation_filters import ugettext_filter_with_en

logger = logging.getLogger(__name__)

HARVEST_YEAR_CHOICES = []
for y in range(2015, (datetime.now().year + 2)):
    HARVEST_YEAR_CHOICES.append((y, y))

TODAY = datetime.now()

CERTIFICATION_FILE_TYPES = [
    ("certificate", _("Certificate")),
    ("audit_report", _("Audit report"))
]


def upload_to_org(instance, filename):
    return u'{organization}/{file}'.format(
        organization=instance.organization.slug,
        file=filename
    )


AGRIPLACE = 'agriplace'
FARM_GROUP = 'farm_group'
HZPC = 'hzpc'
DEFAULT_LAYOUT = AGRIPLACE
LAYOUT_CHOICES = (
    (AGRIPLACE, 'layouts/agriplace.html'),
    (FARM_GROUP, 'layouts/farm_group.html'),
    (HZPC, 'layouts/hzpc.html')
)


class OrganizationTheme(UuidModel):
    name = models.CharField(verbose_name=_("Name"), default='Agriplace White Label', max_length=50, unique=True)
    slug = models.SlugField(verbose_name=_("Slug"), max_length=100, unique=True)
    description = RichTextField(verbose_name=_("Description"), blank=True)
    layout = models.CharField(
        verbose_name=_("Layout"), choices=LAYOUT_CHOICES, max_length=50, unique=True, null=True
    )

    objects = OrganizationThemeManager()

    class Meta:
        db_table = 'OrganizationTheme'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)  # set the slug explicitly
        super(OrganizationTheme, self).save(*args, **kwargs)  # call Django's save()

    def natural_key(self):
        return self.slug,


class OrganizationType(UuidModel):
    name = models.CharField(
        verbose_name=_("Type name"), max_length=1000,
        choices=sorted(ORGANIZATION_TYPE_CHOICES, key=lambda choice: choice[1])
    )
    slug = models.SlugField(verbose_name=_("Slug name"), max_length=1000, unique=True)
    description = RichTextField(verbose_name=_("Description"), default='', blank=True)

    objects = OrganizationTypeManager()

    class Meta(UuidModel.Meta):
        db_table = 'OrganizationType'

    def natural_key(self):
        return self.slug,


class Organization(UuidModel):
    """
    Organization
    """
    name = models.CharField(_("organization name"), max_length=1000)
    slug = models.SlugField(_("URL name"), max_length=1000, unique=True,
                            help_text=_("http://agriplace.com/[organization short name]"))
    logo = models.ImageField(_("logo"), upload_to='organization-logos', blank=True, null=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   verbose_name=_("Users"),
                                   related_name='organizations',
                                   blank=True)
    groups = models.ManyToManyField(Group,
                                    verbose_name=_("Groups"),
                                    related_name='organizations',
                                    help_text="""Please don't use the 'Groups' (to add for instance external auditor
                                    role). You have to add groups in the OrganizationMembership model.""",
                                    blank=True)
    identification_number = models.CharField(_("Chamber of Commerce #"), max_length=50, blank=True, null=True)
    memberships = models.ManyToManyField(
        "self", through="OrganizationMembership", verbose_name=_("Memberships"),
        blank=True, symmetrical=False
    )

    partners = models.ManyToManyField(
        "self",
        through="OrganizationRelationship",
        symmetrical=False,
        related_name="incoming_partners",
        blank=True,
        verbose_name=_("Partners"),
    )

    # Organization business card (public)
    description = models.TextField(_("description"), blank=True)
    # Mailing address
    mailing_address1 = models.CharField(
        _("address (line 1)"),
        max_length=1000,
        blank=True
    )
    mailing_address2 = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_("Address (line 2)"),
    )
    mailing_city = models.CharField(
        max_length=1000,
        blank=True,
        verbose_name=_("City"),
    )
    mailing_province = models.CharField(
        max_length=1000,
        verbose_name=_("Province"),
        blank=True
    )
    mailing_postal_code = models.CharField(
        max_length=1000,
        verbose_name=_("Postal code"),
        blank=True
    )
    mailing_country = models.CharField(
        max_length=1000,
        choices=countries.COUNTRIES,
        default="NL",
        verbose_name=_("Country"),
        blank=True
    )
    map_latitude = models.DecimalField(max_digits=10, decimal_places=5, default=0)
    map_longitude = models.DecimalField(max_digits=10, decimal_places=5, default=0)
    map_zoom = models.IntegerField(default=3)
    email = models.EmailField(
        max_length=1000,
        verbose_name=_("E-mail"),
        blank=True
    )
    url = models.CharField(
        max_length=1000,
        verbose_name=_("Website"),
        blank=True
    )
    phone = models.CharField(
        max_length=1000,
        verbose_name=_("Phone"),
        blank=True
    )

    mobile = models.CharField(
        max_length=1000,
        verbose_name=_("Mobile"),
        blank=True
    )

    ggn_number = models.CharField(_("GGN"), max_length=50, blank=True, null=True)

    gnl_number = models.CharField(_("GNL"), max_length=50, blank=True, null=True)

    tags = TagAutocompleteField()

    theme = models.ForeignKey(OrganizationTheme, null=True, related_name="themes", blank=True)

    evidence_sharing = models.BooleanField(_("Evidence sharing"), default=False)

    license_start_date = models.DateField(null=True, blank=True)

    license_end_date = models.DateField(null=True, blank=True)

    objects = OrganizationManager()

    class Meta(UuidModel.Meta):
        db_table = 'Organization'
        ordering = ["name"]
        permissions = (
            ('view_organization', 'Can view organization'),
        )

    def check_permissions(self, user, action):
        return user in self.users.all()

    @property
    def products(self):
        products = Product.objects.filter(organization_unit__organization=self)
        return products

    @property
    def public_profile_url(self):
        return u"www.agriplace.org/{0}".format(self.slug)

    def get_organization(self):
        return self

    def get_organization_role(self):
        membership = self.secondary_organization.first()
        if membership:
            return membership.role
        return None

    def save(self, *args, **kwargs):
        super(Organization, self).save(*args, **kwargs)
        # TODO: Update address, etc. as answers

    @property
    def mailing_address_one_line(self):
        # print self.mailing_country
        try:
            country_name = unicode(dict(countries.COUNTRIES)[self.mailing_country])
        except:
            country_name = self.mailing_country
        address_parts = [
            self.mailing_address1,
            self.mailing_address2,
            self.mailing_city,
            self.mailing_province,
            self.mailing_postal_code,
            # self.mailing_country,
            country_name
        ]
        # Concatenate, ignoring empty fields
        return ', '.join(
            [p for p in address_parts if p]
        )

    @property
    def country_name(self):
        try:
            country_name = unicode(dict(countries.COUNTRIES)[self.mailing_country])
        except:
            country_name = self.mailing_country
        return country_name

    @country_name.setter
    def country_name(self, value):
        self.mailing_country = value

    def get_organisation_theme_for_export(self, membership):
        if self.theme:
            return self.theme.name
        else:
            if membership and membership.primary_organization.theme:
                return membership.primary_organization.theme.name
            else:
                try:
                    default_theme = OrganizationTheme.objects.get(layout=DEFAULT_LAYOUT)
                    return default_theme.name
                except ObjectDoesNotExist:
                    return None


class OrganizationMembership(UuidModel):
    primary_organization = models.ForeignKey(Organization, related_name='primary_organization')
    secondary_organization = models.ForeignKey(Organization, related_name='secondary_organization')
    organization_type = models.ForeignKey(OrganizationType, related_name='organization_type')
    membership_number = models.CharField(_("Membership number"), max_length=50, null=True, blank=True)
    role = models.ForeignKey(Group, verbose_name=_("role"), related_name='role_organization',
                             null=True, blank=True)

    objects = OrganizationMembershipManager()

    class Meta:
        db_table = 'OrganizationMembership'

    def __unicode__(self):
        return u"[{}] has [{}] as a [{}]".format(
            self.primary_organization.name, self.secondary_organization.name, self.organization_type.name
        )

    @classmethod
    def get_parents_by_user(cls, user):
        """ returns list of uuid of all 'parents' for user's organizations
            parents == primary organizations
        """
        return cls.objects.filter(
            secondary_organization__in=user.organizations.all()
        ).values_list('primary_organization__uuid', flat=True).distinct()

    @classmethod
    def get_parents_by_organization(cls, organization):
        """ returns list of uuid of all 'parents' for organization
            parents == primary organizations
        """
        return cls.objects.filter(
            secondary_organization=organization
        ).values_list('primary_organization__uuid', flat=True).distinct()

    @classmethod
    def get_sisters_by_user(cls, user):
        """ returns list of uuid of all 'sisters' for user's organizations
            sisters == organizations with same primary organizations
        """
        return cls.objects.filter(
            primary_organization__in=cls.get_parents_by_user(user)
        ).values_list('secondary_organization__uuid', flat=True).distinct()

    def get_organization_dropdown_value(self):
        role_en = self.role.name
        role = ugettext_filter_with_en(self.role.name) if self.role else ''
        if role_en == EXTERNAL_AUDITOR:
            return u'{} ({})'.format(
                Truncator(self.secondary_organization).chars(25),
                role
            )
        else:
            return u'{} - {} ({})'.format(
                Truncator(self.secondary_organization).chars(25),
                Truncator(self.primary_organization).chars(25),
                role
            )


class OrganizationRelationship(models.Model):
    sender_organization = models.ForeignKey('Organization', related_name='sent_partnerships')
    receiver_organization = models.ForeignKey('Organization', related_name='received_partnerships')
    RELATIONSHIP_CHOICES = (
        ('partner', _("Generic partner")),
        ('buyer', _("Buyer")),
        ('supplier', _("Supplier"))
    )
    relationship = models.CharField(max_length=100, default='partner')

    class Meta:
        db_table = 'OrganizationRelationship'

    def __unicode__(self):
        return u"[{}] says [{}] is a [{}]".format(self.sender_organization.name, self.receiver_organization.name,
                                                  self.relationship)


class OrganizationAccessInvitation(UuidModel):
    email = models.EmailField()
    organization = models.ForeignKey(Organization)
    requested_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    timestamp = models.DateTimeField()

    class Meta(UuidModel.Meta):
        db_table = 'OrganizationAccessInvitation'

    def save(self, *args, **kwargs):
        # Update timestamp
        if not self.pk:
            self.timestamp = datetime.today()
        return super(OrganizationAccessInvitation, self).save(*args, **kwargs)


class PartnerInvitation(UuidModel):
    """
    Partnership invitation
    The invitation is independent from the actual partnership status of mentioned organizations
    """
    sender = models.ForeignKey('Organization', related_name="sent_invitations")
    receiver = models.ForeignKey('Organization', related_name="received_invitations")
    text = models.TextField(blank=True)
    STATUS_CHOICES = (
        ('open', _("Open")),
        ('accepted', _("Accepted")),
        ('rejected', _("Rejected"))
    )
    status = models.CharField(max_length=1000, default='open')

    class Meta(UuidModel.Meta):
        db_table = 'PartnerInvitation'

    def __unicode__(self):
        return u"{0} invited {1}".format(self.sender, self.receiver)


def get_unit_type_choices():
    """
    Get unit type choices, sorted dynamically by translated label.
    """
    UNIT_TYPE_CHOICES = (
        ('farm', _("Farm")),
        ('field', _("Field")),
        ('production', _("Production facility")),
        ('storage', _("Storage facility")),
        ('processing', _("Processing facility")),
        ('administrative', _("Administrative facility")),
        ('other', _("Other")),
    )
    return sorted(UNIT_TYPE_CHOICES, key=lambda choice: choice[1])


class OrganizationUnit(UuidModel):
    """
    Organization unit
    """
    organization = models.ForeignKey(
        "Organization",
        related_name="units",
        verbose_name=_("Organization")
    )
    # parent = TreeForeignKey("self", related_name="organization_units", blank=True, null=True)
    name = models.CharField(
        max_length=1024,
        verbose_name=_("Name")
    )
    slug = models.CharField(
        max_length=1024,
        verbose_name=_("Slug"),
        blank=True
    )

    type = models.CharField(
        max_length=1024,
        choices=get_unit_type_choices(),
        verbose_name=_("Type"),
        default='other'
    )

    # related_names:
    # products ~ Product

    #	class MPTTMeta:
    #		order_insertion_by = ["name"]

    class Meta(UuidModel.Meta):
        db_table = 'OrganizationUnit'
        ordering = ["name"]

    def check_permissions(self, user, action):
        return user in self.organization.users.all()

    def get_organization(self):
        return self.organization


class ProductTypeGroup(UuidModel):
    """
    Group of product types. The same ProductType can be in multiple groups.
    """
    name = models.CharField(_("Name"), max_length=1000, db_column='name_nl')
    help_text = RichTextField(_("Help text"), max_length=1000, blank=True, db_column='help_text_nl')
    product_types = models.ManyToManyField('ProductType', related_name='product_type_groups', blank=True,
                                           verbose_name=_("Product types"))
    code = models.CharField(_("Code"), max_length=100, default='', blank=True)

    class Meta(UuidModel.Meta):
        db_table = "ProductTypeGroup"

    def __unicode__(self):
        return self.name

    def get_agriplace_product_types(self, x):
        return self.objects.filter(
            Q(rvo_code__startswith=x + ',') | Q(rvo_code__endswith=',' + x) | Q(
                rvo_code__contains=',{0},'.format(x)) | Q(
                rvo_code__exact=x))


class ProductType(UuidModel):
    name = models.CharField(_("Name"), max_length=1000)
    latin_name = models.CharField(_("Latin name"), max_length=1000, blank=True)
    synonyms = models.CharField(_("Synonyms"), max_length=1000, blank=True)
    source = models.CharField(_("Source"), max_length=1000, blank=True)
    help_text = models.CharField(_("Help text"), max_length=1000, blank=True)
    standards = models.CharField(_("Standards"), max_length=1000, blank=True)
    code = models.CharField(_("Code"), max_length=100, default='', blank=True)
    field_color = models.CharField(_("Field color"), max_length=30, default='#6cb33f', blank=True)
    edi_crop_code = models.CharField(_("EDI-Crop code"), max_length=100, default='', blank=True)
    gg_code = models.CharField(_("GLOBALG.A.P. Code"), max_length=100, default='', blank=True)
    rvo_code = models.CommaSeparatedIntegerField(_("RVO list of codes"), max_length=1000, blank=True)
    questionnaires = models.ManyToManyField(
        Questionnaire,
        verbose_name=_("questionnaires"),
        related_name='product_types',
        blank=True
    )

    def __unicode__(self):
        return self.name or _("Unnamed") + " ProductType"

    class Meta(UuidModel.Meta):
        db_table = 'ProductType'
        permissions = (
            ('view_producttype', 'Can view product type'),
        )


class StandardProductType(UuidModel):
    standard_code = models.CharField(_("Standard code"), max_length=50)
    name = models.CharField(_("Name"), max_length=1000)
    product_type_code = models.CharField(_("Product type code"), max_length=100, default='', blank=True)
    begin_date = models.DateField(_("Begin date"), blank=True, null=True)
    end_date = models.DateField(_("End date"), blank=True, null=True)
    landscape_element = models.BooleanField(_("Landscape element"), default=False)

    def __unicode__(self):
        return self.name or _("Unnamed") + " ProductType"

    class Meta(UuidModel.Meta):
        db_table = 'StandardProductType'
        permissions = (
            ('view_standardproducttype', 'Can view standard product type'),
        )


class Product(UuidModel):
    organization = models.ForeignKey(
        "Organization",
        related_name="products",
        verbose_name=_("Organization")
    )
    organization_unit = models.ForeignKey(
        "OrganizationUnit",
        related_name="products",
        verbose_name=_("Organization unit"),
        blank=True,
        null=True
    )
    name = models.CharField(
        _("Name"),
        max_length=1000,
        default="?",
        blank=True
    )
    slug = models.CharField(
        max_length=1000,
        blank=True,
        default='',
        verbose_name=_("Slug"),
    )
    product_type = models.ForeignKey(
        'ProductType',
        null=True,
        related_name='products',
        verbose_name=_("Product type"),
    )

    harvest_year = models.IntegerField(_('Harvest year'),
                                       choices=HARVEST_YEAR_CHOICES,
                                       default=datetime.now().year)
    area = models.FloatField(_('Area in hectares'), blank=True, null=True)
    location = models.CharField(_('Location'), max_length=200, blank=True, null=True)
    comment = models.TextField(_('Comment'), blank=True, null=True)

    class Meta(UuidModel.Meta):
        db_table = 'Product'
        ordering = ["harvest_year", "name"]
        permissions = (
            ('view_product', 'Can view product'),
        )

    def __unicode__(self):
        try:
            if self.product_type:
                return tolerant_ugettext(unicode(self.product_type))
            elif self.name:
                return tolerant_ugettext(self.name)
            else:
                return _("Unnamed product")
        except:
            return "?"

    def check_permissions(self, user, action):
        return self.organization in user.organizations.all()

    def get_absolute_url(self):
        return reverse('my_product_detail', args=[self.organization.slug, self.pk])

    def product_type_name(self):
        if self.remarks:
            product_type_name = u"{type} ({remarks})".format(
                type=self.product_type,
                remarks=truncatechars(self.remarks, 64)
            )
        else:
            product_type_name = self.product_type.name
        return product_type_name

    def product_area(self):
        if self.area > 0:
            return int(round(self.area))
        else:
            return 0

    def get_history(self):
        return self.history.all()

    def get_partners(self):
        return self.partners.all()

    def used_in_assessments(self):
        related_assessments = self.assessments.all().prefetch_related('assessments')

        assessments = [{
            'uuid': assessment.uuid,
            'name': assessment.name
        } for assessment in related_assessments]

        return assessments

    def used_in_certificates(self):
        related_certifications = self.certifications.all()

        certifications = [{
            'uuid': certification.uuid,
            'certificate_number': certification.certificate_number
        } for certification in related_certifications]

        return certifications


class ProductPartner(UuidModel):
    product = models.ForeignKey(Product, related_name="partners")
    organization_uuid = models.ForeignKey(Organization, max_length=200)
    contract_number = models.CharField(_('Contract number'), max_length=200, blank=True, null=True)

    class Meta:
        db_table = 'ProductPartner'


class ProductIdentifier(models.Model):
    """
    Product identifier
    """
    product = models.ForeignKey("Product", related_name="identifiers")
    TYPE_CHOICES = (
        ("internal", _("Internal")),
        ("gs1", "GS1"),
        ("upc", "UPC"),
        ("other", _("Other")),
    )
    identifier_type = models.CharField(max_length=1024, choices=TYPE_CHOICES, default=u"")
    identifier_type_name = models.CharField(max_length=1024, default=u"", blank=False, null=True)
    value = models.CharField(max_length=1024, default=u'')

    def __unicode__(self):
        return u"%(type)s:%(id)s [%(org)s %(unit)s %(product)s]" % {
            "type": self.identifier_type,
            "id": self.value,
            "product": self.product.name,
            "unit": getattr(self.product.organization_unit, 'name', '-'),
            "org": self.product.organization.name
        }

    class Meta():
        db_table = 'ProductIdentifier'


class CertificationType(UuidModel):
    """
    Definition of certification type
    """
    name = models.CharField(max_length=1024, default='', blank=True)
    code = models.CharField(max_length=255, default='', blank=True)
    verified = models.BooleanField(default=False, help_text="Has this certification been verified by P4E?")

    class Meta(UuidModel.Meta):
        db_table = 'CertificationType'
        permissions = (
            ('view_certificationtype', 'Can view certification type'),
        )


class Certification(UuidModel):
    """
    Existing certification of organization/unit/product
    """
    certification_type = models.ForeignKey("CertificationType", related_name="certifications", blank=True, null=True)

    # All relevant fields filled
    # Eg. for product also fill in organization_unit and organization
    organization = models.ForeignKey("Organization", related_name="certifications", blank=True, null=True)
    organization_unit = models.ForeignKey("OrganizationUnit", related_name="certifications", blank=True, null=True)
    product = models.ForeignKey("Product", related_name="certifications", blank=True, null=True)
    certificate_number = models.CharField(_("Certificate number"), max_length=1000, blank=True, null=True)

    issue_date = models.DateField(_("Issue date"), blank=True, null=True)
    expiry_date = models.DateField(_("Expiry date"), blank=True, null=True)
    issuer = models.CharField(_("Issuer"), max_length=1024, blank=True, null=True)

    class Meta(UuidModel.Meta):
        db_table = 'Certification'
        permissions = (
            ('view_certification', 'Can view certification'),
        )

    @property
    def target(self):
        target = self.product or self.organization_unit or self.organization
        if not target:
            # raise Exception("Certification missing target object")
            logger.warning("Certification %s missing target object" % self.pk)
        return target

    @property
    def target_type(self):
        if self.product:
            return ("product", _("Crop"))
        elif self.organization_unit_id:
            return ("organization_unit", _("Organization unit"))
        elif self.organization:
            return ("organization", _("Organization"))
        else:
            raise Exception("Certification missing target object")

    def __unicode__(self):
        return u"%s / %s" % (self.target, self.certification_type)

    def check_permissions(self, user, action):
        return self.target.check_permissions(user, action)


class CertificationCrop(UuidModel):
    product_type = models.ForeignKey('ProductType', null=True)
    area = models.FloatField(_('Area in hectares'), blank=True, null=True)
    certification = models.ForeignKey(
        "Certification",
        verbose_name=_("Certification"),
        related_name="certification_crops",
        blank=True,
        null=True
    )

    def save(self, *args, **kwargs):
        if not self.product_type:
            raise Exception("The product type is required.")
        return super(CertificationCrop, self).save(*args, **kwargs)


class CertificationFile(UuidModel):
    file = models.FileField(max_length=255, upload_to='organization/certification/files')
    file_type = models.CharField(
        _("Certification file type"),
        max_length=50,
        choices=CERTIFICATION_FILE_TYPES,
        default="",
        blank=True
    )
    certification = models.ForeignKey(
        Certification,
        verbose_name=_("Certification"),
        related_name='certification_files',
        blank=True
    )
    original_file_name = models.CharField(_("Original file name"), max_length=255, null=True, blank=True)


#
# Model-related signal receivers
#

@receiver(pre_save, sender=Organization)
def set_organization_slug(sender, instance, **kwargs):
    if not getattr(instance, 'slug', None):
        instance.slug = slugify(instance.name)


class PlotRVO(UuidModel):
    kvk = models.CharField(_("Organization KVK number"), max_length=50)
    field_id = models.CharField(_("RVO field id"), max_length=50)
    begin_date = models.DateTimeField(_("RVO begin date"), blank=True, null=True)
    end_date = models.DateTimeField(_("RVO end date"), blank=True, null=True)
    crop_type_code = models.CharField(_("RVO crop type code"), max_length=10, blank=True, null=True)
    pos_list = models.TextField(_("RVO field geo data"), blank=True, null=True)

    objects = PlotRVOManager()

    def get_coordinates(self):
        polygon = []

        amersfoort_rd_new = Projection(
            "+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +towgs84=565.417,50.3319,465.552,-0.398957,0.343988,-1.8774,4.0725 +units=m +no_defs")
        EPSG_4326 = Projection('+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs')

        coordinates = self.pos_list.split(" ")
        coordinates.append(None)

        for long_lat in zip(coordinates[::2], coordinates[1::2]):
            point = Coord(float(long_lat[0]), float(long_lat[1]))
            converted_long_lat = ProjTransform(amersfoort_rd_new, EPSG_4326).forward(point)
            polygon.append([converted_long_lat.y, converted_long_lat.x])
        return polygon

    def __unicode__(self):
        return u"%s" % (self.kvk,)

    class Meta(UuidModel.Meta):
        db_table = 'PlotRVO'
        permissions = (
            ('view_plotrvo', 'Can view plot rvo'),
        )


class OrganizationReport(UuidModel):
    report_document = models.FileField(max_length=255, upload_to="report_documents")

    class Meta(UuidModel.Meta):
        db_table = 'OrganizationReport'
        permissions = (
            ('view_organizationreport', 'Can view organization report'),
        )
