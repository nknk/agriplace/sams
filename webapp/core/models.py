"""
Core models, common to most apps
"""

import logging
import uuid
from datetime import datetime
from django_extensions.db.fields import ShortUUIDField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.base_convert import base_encode
from django.utils.text import slugify
logger = logging.getLogger(__name__)


def get_uuid(prefix=''):
    """
    Generates UUID and encodes it to whole alphabet for minimum size
    :return: Uniquely identifying (short) string
    """
    code = base_encode(uuid.uuid4().int)
    if prefix:
        code = '%s-%s' % (prefix, code)
    return code


class UuidModel(models.Model):
    """
    Base model with UUID primary key
    """
    uuid = ShortUUIDField(max_length=36, primary_key=True)
    remarks = models.TextField(_("Remarks"), default='', blank=True)
    is_test = models.BooleanField(default=False)

    modified_time = models.DateTimeField(default=datetime.today)
    created_time = models.DateTimeField(default=datetime.today)

    class Meta:
        abstract=True
        ordering = ('uuid',)

    def __unicode__(self):
        if self.__dict__.get("name"):
            return self.name
        else:
            return self.uuid

    def save(self, *args, **kwargs):
        # Update timestamp
        if not self.pk:
            self.created_time = datetime.today()
        self.modified_time = datetime.today()
        return super(UuidModel, self).save(*args, **kwargs)


class KeyValue(UuidModel):
    key = models.CharField(max_length=1000)
    value = models.CharField(max_length=1000, default='', blank=True)

    class Meta:
        db_table = 'KeyValue'

    def __unicode__(self):
        return "{self.key}={self.value}".format(self=self)


class ContentPush(UuidModel):

    class Meta:
        db_table = 'ContentPush'
        verbose_name_plural = "Content pushes"
        ordering = ('-created_time',)
        permissions = (
            ('view_contentpush', 'Can view content push'),
        )

    def __unicode__(self):
        return 'Pushed on {self.created_time}'.format(self=self)

    @staticmethod
    def get_latest_key():
        """
        :return: Returns a key based on the created time of the latest content push.
        Returns blank string if no ContentPush exists..
        """
        try:
            latest = ContentPush.objects.latest('created_time')
            key = str(latest.created_time)
        except ContentPush.DoesNotExist:
            key = ''
        return key

