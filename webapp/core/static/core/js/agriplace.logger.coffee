#
# Simple logger module
#
module = angular.module('agriplace.logger', [])

module.service('logger', () ->
    class Logger
        @log: (level = 'DEBUG', message) ->
            if console?
                console.log(arguments)
)
