#
# Notification system wrapper
#

module = angular.module('agriplace.notifier', ['toaster', 'gettext'])

module.factory('notifier', [
    'toaster',
    'gettextCatalog',
    (toaster, gt) ->
        return new class AgriPlaceNotifier
            notify: (level, msg) ->
                toaster.pop(level, null, msg)
                if console?
                    console.log("* notification", level, msg)
#                toaster.pop(level, gt.getString(level), msg)

            success: (msg) ->
                @notify('success', msg)

            error: (msg) ->
                @notify('error', msg)

            info: (msg) ->
                @notify('info', msg)

])
