(function() {

  var module = angular.module('agriplace.jsContext', []);

  module.service('jsContext', function() {
    // jsContext service shadows window.jsContext global variable
    // set by Django
    var Service = function() {
      var _this = this;
      _.extend(this, window.jsContext);
    }
    return new Service;
  });

})();
