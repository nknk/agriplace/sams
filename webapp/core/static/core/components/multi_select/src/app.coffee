# Multi-select directive
#
# Originally based on:
# Multiple-select directive for AngularJS
# (c) 2013 Alec LaLonde (https://github.com/alalonde/angular-multi-select)
# License: MIT

# Multi-select should act as a regular <select multiple>
# items: available items
# ng-model: selected items


app = angular.module('agriplace.multiSelect', ['agriplace.listView'])

# Appends `selected` property to all items in list
appendSelected = (list) ->
    for item in list
        item.selected = false



app.directive('multiSelect', [
    '$q', '$parse',
    ($q, $parse) ->
        return {
        restrict: 'AE'
        require: 'ngModel'
        scope:
            availableLabel: '@'
            selectedLabel: '@'
            # available items (source)
            items: '='
            # model (selected items)
            selected: '=ngModel'
            # config object
            config: '='
            # name of input element
            htmlName: '@'
            # icon class for all items (optional)
            itemCaption: '@'
            # value used in <input>
            itemValue: '@'
            itemTooltip: '@'
        #  generator expression for icon class (optional)
            itemIconClass: '@'
        templateUrl: 'VIRTUAL/multi_select/multi_select.html'


        # Link
        link: (scope, elm, attrs, controllers) ->
            # internal lists
            scope.available = []

            # Refresh internal lists based on data bindings
            scope.refreshLists = () ->
                scope.available = _.difference(scope.items, scope.selected)
                scope.checked = {
                    available: []
                    selected: []
                }

            # When items model changes, update internal lists

            scope.$watchCollection('[items, selected]', (items) ->
                scope.refreshLists()
            )

            # Parse expression
            parseExpression = (item, expr) ->
                expressionComponents = expr.match(/(.+)\s+as\s+(.+)/)
                expressionScope = {}
                expressionScope[expressionComponents[1]] = item
                return $parse(expressionComponents[2])(expressionScope)

            scope.isSelected = (item) ->
                return _.contains(scope.selected, item)

            # Moves checked items from available into selected
            scope.add = () ->
                scope.selected = scope.selected.concat(scope.checked.available)

            # Move checked items from selected back to available
            scope.remove = () ->
                for index, item of scope.checked.selected
                    scope.selected = _.without(scope.selected, item)

            # Renders item using given expression attr of the list
            scope.renderAttr = (item, listAttrName) ->
                if attrs[listAttrName]?
                    value = parseExpression(item, attrs[listAttrName])
                    return value
                else
                    return ""
        }
])


app.filter('objectToArray', () ->
    return (input) ->
        return _.toArray(input)
)

