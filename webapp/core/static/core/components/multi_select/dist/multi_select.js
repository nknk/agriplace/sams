/*! AgriPlace - v0.1.1 - * Copyright (c) 2014 ; Licensed  */
(function() {
  var app, appendSelected;

  app = angular.module('agriplace.multiSelect', ['agriplace.listView']);

  appendSelected = function(list) {
    var item, _i, _len, _results;
    _results = [];
    for (_i = 0, _len = list.length; _i < _len; _i++) {
      item = list[_i];
      _results.push(item.selected = false);
    }
    return _results;
  };

  app.directive('multiSelect', [
    '$q', '$parse', function($q, $parse) {
      return {
        restrict: 'AE',
        require: 'ngModel',
        scope: {
          availableLabel: '@',
          selectedLabel: '@',
          items: '=',
          selected: '=ngModel',
          config: '=',
          htmlName: '@',
          itemCaption: '@',
          itemValue: '@',
          itemTooltip: '@',
          itemIconClass: '@'
        },
        templateUrl: 'VIRTUAL/multi_select/multi_select.html',
        link: function(scope, elm, attrs, controllers) {
          var parseExpression;
          scope.available = [];
          scope.refreshLists = function() {
            scope.available = _.difference(scope.items, scope.selected);
            return scope.checked = {
              available: [],
              selected: []
            };
          };
          scope.$watchCollection('[items, selected]', function(items) {
            return scope.refreshLists();
          });
          parseExpression = function(item, expr) {
            var expressionComponents, expressionScope;
            expressionComponents = expr.match(/(.+)\s+as\s+(.+)/);
            expressionScope = {};
            expressionScope[expressionComponents[1]] = item;
            return $parse(expressionComponents[2])(expressionScope);
          };
          scope.isSelected = function(item) {
            return _.contains(scope.selected, item);
          };
          scope.add = function() {
            return scope.selected = scope.selected.concat(scope.checked.available);
          };
          scope.remove = function() {
            var index, item, _ref, _results;
            _ref = scope.checked.selected;
            _results = [];
            for (index in _ref) {
              item = _ref[index];
              _results.push(scope.selected = _.without(scope.selected, item));
            }
            return _results;
          };
          return scope.renderAttr = function(item, listAttrName) {
            var value;
            if (attrs[listAttrName] != null) {
              value = parseExpression(item, attrs[listAttrName]);
              return value;
            } else {
              return "";
            }
          };
        }
      };
    }
  ]);

  app.filter('objectToArray', function() {
    return function(input) {
      return _.toArray(input);
    };
  });

}).call(this);

//# sourceMappingURL=app.js.map

angular.module('agriplace.multiSelect').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('VIRTUAL/multi_select/multi_select.html',
    "<div class=\"multiSelect row\">\n" +
    "\n" +
    "    <div class=\"select col-sm-4\">\n" +
    "        <!-- Available -->\n" +
    "        <label class=\"control-label\" for=\"multiSelectAvailable\">\n" +
    "            {{ availableLabel }}\n" +
    "        </label>\n" +
    "\n" +
    "        <div list-view\n" +
    "             items=\"available\"\n" +
    "             ng-model=\"checked.available\"\n" +
    "\n" +
    "             item-caption=\"i as i.name\"\n" +
    "             item-tooltip=\"i as i.uuid\"\n" +
    "             item-icon-class=\"fa fa-leaf\"\n" +
    "             item-value=\"i as i.uuid\"\n" +
    "                >\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- Buttons -->\n" +
    "    <div class=\"select buttons col-sm-2\">\n" +
    "        <!-- Add -->\n" +
    "        <div>\n" +
    "            <button class=\"btn btn-default mover left\" ng-click=\"add()\"\n" +
    "                    title=\"{{ 'Add selected'|translate }}\"\n" +
    "                    ng-disabled=\"!checked.available.length\">\n" +
    "                <i class=\"fa fa-arrow-right\"></i>\n" +
    "                <span translate>Add</span>\n" +
    "            </button>\n" +
    "        </div>\n" +
    "        <!-- Remove -->\n" +
    "        <div>\n" +
    "            <button class=\"btn btn-default mover right\" ng-click=\"remove()\"\n" +
    "                    title=\"Remove selected\"\n" +
    "                    ng-disabled=\"!checked.selected.length\">\n" +
    "                <i class=\"fa fa-arrow-left\"></i>\n" +
    "                <span translate>Remove</span>\n" +
    "            </button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- Selected -->\n" +
    "    <div class=\"select col-sm-4 highlighted\">\n" +
    "        <label class=\"control-label\" for=\"multiSelectSelected\">\n" +
    "            {{ selectedLabel }}\n" +
    "        </label>\n" +
    "\n" +
    "        <div list-view\n" +
    "             items=\"selected\"\n" +
    "             ng-model=\"checked.selected\"\n" +
    "             item-caption=\"i as i.name\"\n" +
    "             item-tooltip=\"i as i.uuid\"\n" +
    "             item-icon-class=\"fa fa-leaf\"\n" +
    "             item-value=\"i as i.uuid\"\n" +
    "             >\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<!-- Read only select -->\n" +
    "<select multiple name=\"{{ htmlName }}\" style=\"display: none;\">\n" +
    "    <option ng-repeat=\"item in items\" ng-selected=\"isSelected(item)\"\n" +
    "            value=\"{{ renderAttr(item, 'itemValue') }}\">\n" +
    "        {{ renderAttr(item, 'itemCaption') }}\n" +
    "    </option>\n" +
    "</select>\n" +
    "\n"
  );

}]);
