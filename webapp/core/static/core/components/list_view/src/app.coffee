# List view directive
#

# For forms, a list view acts as a regular <select [multiple]>
# items: available items
# ng-model: selected items (with checkboxes)


app = angular.module('agriplace.listView', [])


app.directive('listView', [
    '$q', '$parse',
    ($q, $parse) ->
        return {
        restrict: 'AE'
        require: 'ngModel'
        scope:
            # available items (source)
            items: '='
            # model (selected items)
            selected: '=ngModel'
            # config object
            config: '='
            # name of input element
            htmlName: '@'
#            itemCaption: '@'
            # value used in <input>
            itemValue: '@'
            itemTooltip: '@'
            #  generator expression for icon class (optional)
            itemIconClass: '@'
        templateUrl: 'VIRTUAL/list_view/list_view.html'

        # Link
        link: (scope, elm, attrs, controllers) ->

            # We don't want to modify original objects
            # so we can have multiple listViews showing the same objects
            # therefore we keep the checked status in an internal array.
            scope.checked = []
            isChecked = (index) ->
                if index < scope.checked.length and scope.checked[index]
                    return true
                else
                    return false

            # Update selected collection when a checkbox changes
            scope.onItemChecked = (item) ->
                itemIndex = _.indexOf(scope.items, item)
                if isChecked(itemIndex)
                    scope.selected = _.union(scope.selected, [item])
                else
                    scope.selected = _.without(scope.selected, item)

            # Update check list when external model changes
            scope.refreshChecked = () ->
                for item, index in scope.items
                    scope.checked[index] = _.contains(scope.selected, item)
               #

            # Returns items in `list` that are checked (true) in corresponding position in `checklist`
            scope.checkedItems = (list, checklist) ->
                if !list? or !checklist?
                    return []
                result = []
                for item, index of list
                    if index < checklist.length and checklist[index]
                        result.push(item)
                return result

            # Refresh on changing either the item or selected lists
            scope.$watchCollection('[items,selected]', (items) ->
                # Update our checklist to match indexes again
                scope.refreshChecked()
            )

            # Parse expression
            # eg. parseExpression(item, `i as i.foo`) returns item.foo
            parseExpression = (item, expression) ->
                expressionParts = expression.match(/(.+)\s* as \s*(.+)/)
                expressionScope = {}
                expressionScope[expressionParts[1]] = item
                result = $parse(expressionParts[2])(expressionScope)
                return result

            console.log('attrs', attrs)

            # Renders item using given expression attr of the list
            scope.renderAttr = (item, listAttrName) ->
                if attrs[listAttrName]?
                    value = parseExpression(item, attrs[listAttrName])
                    console.log(listAttrName, attrs[listAttrName], 'for', item, '=', value)
                    return value
                else
                    console.log(listAttrName, 'NO')
                    return ""

        }
])


app.filter('objectToArray', () ->
    return (input) ->
        return _.toArray(input)
)


# TODO Single select
# TODO Validate min/max number of selected items
