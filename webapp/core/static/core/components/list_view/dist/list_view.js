/*! AgriPlace - v0.1.1 - * Copyright (c) 2014 ; Licensed  */
(function() {
  var app;

  app = angular.module('agriplace.listView', []);

  app.directive('listView', [
    '$q', '$parse', function($q, $parse) {
      return {
        restrict: 'AE',
        require: 'ngModel',
        scope: {
          items: '=',
          selected: '=ngModel',
          config: '=',
          htmlName: '@',
          itemValue: '@',
          itemTooltip: '@',
          itemIconClass: '@'
        },
        templateUrl: 'VIRTUAL/list_view/list_view.html',
        link: function(scope, elm, attrs, controllers) {
          var isChecked, parseExpression;
          scope.checked = [];
          isChecked = function(index) {
            if (index < scope.checked.length && scope.checked[index]) {
              return true;
            } else {
              return false;
            }
          };
          scope.onItemChecked = function(item) {
            var itemIndex;
            itemIndex = _.indexOf(scope.items, item);
            if (isChecked(itemIndex)) {
              return scope.selected = _.union(scope.selected, [item]);
            } else {
              return scope.selected = _.without(scope.selected, item);
            }
          };
          scope.refreshChecked = function() {
            var index, item, _i, _len, _ref, _results;
            _ref = scope.items;
            _results = [];
            for (index = _i = 0, _len = _ref.length; _i < _len; index = ++_i) {
              item = _ref[index];
              _results.push(scope.checked[index] = _.contains(scope.selected, item));
            }
            return _results;
          };
          scope.checkedItems = function(list, checklist) {
            var index, item, result;
            if ((list == null) || (checklist == null)) {
              return [];
            }
            result = [];
            for (item in list) {
              index = list[item];
              if (index < checklist.length && checklist[index]) {
                result.push(item);
              }
            }
            return result;
          };
          scope.$watchCollection('[items,selected]', function(items) {
            return scope.refreshChecked();
          });
          parseExpression = function(item, expression) {
            var expressionParts, expressionScope, result;
            expressionParts = expression.match(/(.+)\s* as \s*(.+)/);
            expressionScope = {};
            expressionScope[expressionParts[1]] = item;
            result = $parse(expressionParts[2])(expressionScope);
            return result;
          };
          console.log('attrs', attrs);
          return scope.renderAttr = function(item, listAttrName) {
            var value;
            if (attrs[listAttrName] != null) {
              value = parseExpression(item, attrs[listAttrName]);
              console.log(listAttrName, attrs[listAttrName], 'for', item, '=', value);
              return value;
            } else {
              console.log(listAttrName, 'NO');
              return "";
            }
          };
        }
      };
    }
  ]);

  app.filter('objectToArray', function() {
    return function(input) {
      return _.toArray(input);
    };
  });

}).call(this);

//# sourceMappingURL=app.js.map

angular.module('agriplace.listView').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('VIRTUAL/list_view/list_view.html',
    "<div class=\"list-view\">\n" +
    "\n" +
    "    <div class=\"select-box\">\n" +
    "        <table class=\"table table-condensed\">\n" +
    "            <tbody>\n" +
    "            <tr ng-repeat=\"item in items\" ng-class=\"{checked:checked[$index]}\">\n" +
    "                <td>\n" +
    "                    <label class=\"checkbox\" title=\"{{ renderAttr(item, 'itemTooltip') }}\">\n" +
    "                        <input type=\"checkbox\" ng-model=\"checked[$index]\"\n" +
    "                               ng-change=\"onItemChecked(item)\">\n" +
    "                        <span ng-class=\"iconClass\"></span>\n" +
    "                        {{ renderAttr(item, 'itemCaption') }}\n" +
    "                    </label>\n" +
    "                </td>\n" +
    "            </tbody>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<!-- Read only hidden select for forms -->\n" +
    "<select multiple name=\"{{ htmlName }}\" style=\"display: none;\">\n" +
    "    <option ng-repeat=\"item in items\" ng-selected=\"checked[$index]\" ng-value=\"renderAttr(item, 'itemValue')\">\n" +
    "        {{ renderAttr(item, 'itemCaption') }}\n" +
    "    </option>\n" +
    "</select>\n" +
    "<!-- / -->\n"
  );

}]);
