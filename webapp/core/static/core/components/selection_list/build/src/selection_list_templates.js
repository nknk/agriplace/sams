angular.module('agriplace.selectionList').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('VIRTUAL/selectionList/selection_list.html',
    "XX selection list XX\n" +
    "<div class=\"selection-list-container\">\n" +
    "    <div class=\"selection-list-item\" ng-repeat=\"item in model.items\">\n" +
    "        <input type=\"checkbox\" />\n" +
    "        <div>{{ item.label }}</div>\n" +
    "    </div>\n" +
    "</div>"
  );

}]);
