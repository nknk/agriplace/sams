/*! AgriPlace - v0.1.1 - * Copyright (c) 2014 ; Licensed  */
// https://github.com/vitalets/checklist-model
// 2014-08-18
// License: MIT

/**
 * Checklist-model
 * AngularJS directive for list of checkboxes
 */

angular.module('checklist-model', [])
.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
  // contains
  function contains(arr, item) {
    if (angular.isArray(arr)) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], item)) {
          return true;
        }
      }
    }
    return false;
  }

  // add
  function add(arr, item) {
    arr = angular.isArray(arr) ? arr : [];
    for (var i = 0; i < arr.length; i++) {
      if (angular.equals(arr[i], item)) {
        return arr;
      }
    }
    arr.push(item);
    return arr;
  }

  // remove
  function remove(arr, item) {
    if (angular.isArray(arr)) {
      for (var i = 0; i < arr.length; i++) {
        if (angular.equals(arr[i], item)) {
          arr.splice(i, 1);
          break;
        }
      }
    }
    return arr;
  }

  // http://stackoverflow.com/a/19228302/1458162
  function postLinkFn(scope, elem, attrs) {
    // compile with `ng-model` pointing to `checked`
    $compile(elem)(scope);

    // getter / setter for original model
    var getter = $parse(attrs.checklistModel);
    var setter = getter.assign;

    // value added to list
    var value = $parse(attrs.checklistValue)(scope.$parent);

    // watch UI checked change
    scope.$watch('checked', function(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }
      var current = getter(scope.$parent);
      if (newValue === true) {
        setter(scope.$parent, add(current, value));
      } else {
        setter(scope.$parent, remove(current, value));
      }
    });

    // watch original model change
    scope.$parent.$watch(attrs.checklistModel, function(newArr, oldArr) {
      scope.checked = contains(newArr, value);
    }, true);
  }

  return {
    restrict: 'A',
    priority: 1000,
    terminal: true,
    scope: true,
    compile: function(tElement, tAttrs) {
      if (tElement[0].tagName !== 'INPUT' || !tElement.attr('type', 'checkbox')) {
        throw 'checklist-model should be applied to `input[type="checkbox"]`.';
      }

      if (!tAttrs.checklistValue) {
        throw 'You should provide `checklist-value`.';
      }

      // exclude recursion
      tElement.removeAttr('checklist-model');

      // local scope var storing individual checkbox model
      tElement.attr('ng-model', 'checked');

      return postLinkFn;
    }
  };
}]);
(function() {
  var SelectionListModel, app, module;

  app = angular.module('agriplace', []);

  module = angular.module('agriplace.selectionList', ['ui.bootstrap', 'gettext', 'agriplace.jsContext', 'agriplace.logger', 'agriplace.notifier', 'checklist-model']).config([
    '$httpProvider', function($httpProvider) {
      $httpProvider.defaults.xsrfCookieName = 'csrftoken';
      return $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }
  ]);


  /*
     Selection List model object
   */

  SelectionListModel = (function() {
    function SelectionListModel() {}

    SelectionListModel.isSearchVisible = true;

    SelectionListModel.isMultiselect = true;

    SelectionListModel.items = [];

    return SelectionListModel;

  })();

}).call(this);

//# sourceMappingURL=app.js.map

(function() {
  var module;

  module = angular.module('agriplace.selectionList');

  module.directive('selectionList', [
    'jsContext', function(jsContext) {
      return {
        restrict: 'E',
        scope: {
          model: '='
        },
        templateUrl: 'VIRTUAL/selection_list/selection_list.html'
      };
    }
  ]);

  module.controller('SelectionListController', ['$http', '$scope', '$rootScope', '$interval', 'gettextCatalog', 'logger', 'jsContext', 'notifier', function($http, $scope, $rootScope, $interval, gettextCatalog, logger, jsContext, notifier) {}]);

}).call(this);

//# sourceMappingURL=directive.js.map

angular.module('agriplace.selectionList').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('VIRTUAL/selection_list/selection_list.html',
    "<div ng-controller=\"SelectionListController\">\n" +
    "    <pre>\n" +
    "        sl model: {{ model }}\n" +
    "    </pre>\n" +
    "    <div class=\"selection-list-container\">\n" +
    "        <table class=\"table table-bordered table-striped datatable\">\n" +
    "            <thead>\n" +
    "            <tr>\n" +
    "                <th></th>\n" +
    "            </tr>\n" +
    "            </thead>\n" +
    "            <tbody>\n" +
    "            <tr class=\"selection-list-item\"\n" +
    "                ng-repeat=\"item in model.items\"\n" +
    "                ng-class=\"{'selected':item.selected}\">\n" +
    "                <td>\n" +
    "                    <label>\n" +
    "                        <input type=\"checkbox\"\n" +
    "                               ng-model=\"item.selected\"\n" +
    "                                />\n" +
    "                        {{ item.label }}\n" +
    "                    </label>\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "            </tbody>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<!--\n" +
    "{#                               checklist-model=\"model.selectedItems\"#}\n" +
    "{#                               checklist-value=\"item.pk\"#}\n" +
    "-->\n"
  );

}]);

(function() {


}).call(this);

//# sourceMappingURL=service.js.map
