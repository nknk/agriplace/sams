###
    Selection List component
###

app = angular.module('agriplace', [])

#
# Application module
#
module = angular.module('agriplace.selectionList', [
    'ui.bootstrap',
    'gettext',
    'agriplace.jsContext',
    'agriplace.logger',
    'agriplace.notifier',
    'checklist-model'
])
.config [
    '$httpProvider',
    ($httpProvider) ->
        $httpProvider.defaults.xsrfCookieName = 'csrftoken'
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
]

###
   Selection List model object
###
class SelectionListModel
    # options
    @isSearchVisible = true
    @isMultiselect = true
    # data
    @items = []

