#
# <selection-list> directive
#

module = angular.module('agriplace.selectionList')


module.directive('selectionList', [
    'jsContext',
    (jsContext) ->
        return {
            restrict: 'E',
            scope: {
                model: '='
            },
            templateUrl: 'VIRTUAL/selection_list/selection_list.html',
#            link: ($scope, element, attrs) ->
#                return
        }
])


#
# QuestionnaireEditorCtrl controller
#
module.controller('SelectionListController', [
    '$http', '$scope', '$rootScope', '$interval', 'gettextCatalog',
    'logger', 'jsContext', 'notifier',
    ($http, $scope, $rootScope, $interval,
     gettextCatalog, logger, jsContext, notifier) ->
        # $scope.model: injected by directive
        return
])
