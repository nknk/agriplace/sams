# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.db import migrations, models


def clean_content_types(apps, schema_editor):
    """
    Removes all instances of hzpc, hzpc_workflows and hzpc_notifications from content_types table
    """
    deleted_apps = ["hzpc", "hzpc_workflows", "hzpc_notifictions"]
    deleted_models = [
        "HzpcAssessmentAgreement",
        "HzpcAssessmentTypeList",
        "HzpcImportUsers",
        "HzpcNotificationJournal",
        "HzpcNotificationSettings",
        "HzpcWorkflowContext",
        "HzpcWorkflowContextFiles",
        "MutationLog"
    ]

    ct = ContentType.objects.all().order_by("app_label", "model")

    for c in ct:
        if (c.app_label in deleted_apps) or (c.model in deleted_models):
            print "Deleting Content Type %s %s" % (c.app_label, c.model)
            c.delete()


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0002_auto_20160816_1330'),
    ]

    operations = [
        migrations.RunPython(clean_content_types),

        migrations.RunSQL('''
            DROP TABLE IF EXISTS "HzpcAssessmentTypeList";
            DROP TABLE IF EXISTS "HzpcAssessmentAgreement";            
            DROP TABLE IF EXISTS "HzpcImportUsers";
            DROP TABLE IF EXISTS "HzpcNotificationJournal";
            DROP TABLE IF EXISTS "HzpcNotificationSettings";
            DROP TABLE IF EXISTS "HzpcWorkflowContextFiles";
            DROP TABLE IF EXISTS "MutationLog";
            DROP TABLE IF EXISTS "HzpcWorkflowContext";
            DELETE FROM auth_permission WHERE content_type_id IN (SELECT id FROM django_content_type WHERE app_label IN ('{app_label}'));
            DELETE FROM django_admin_log WHERE content_type_id IN (SELECT id FROM django_content_type WHERE app_label IN ('{app_label}'));
            DELETE FROM django_content_type WHERE app_label IN ('{app_label}');
            DELETE FROM django_migrations WHERE app IN ('{app_label}');
            '''.format(app_label='hzpc, hzpc_workflows, hzpc_notifications'))
    ]
