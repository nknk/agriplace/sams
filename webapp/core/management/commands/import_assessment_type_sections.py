from django.core.management.base import BaseCommand
from django.db import transaction

from assessments.models import AssessmentType, AssessmentTypeSection


@transaction.atomic
class Command(BaseCommand):
    help = 'Script to bootstrap farm group'

    def handle(self, *args, **options):

        print('importing assessment type sections')

        sections = [
            {
                'code': 'globalgap_ifa_v5',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'globalgap_ifa',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'ah',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'eu-bio',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,sharing'
            },
            {
                'code': 'tesco',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'vv-a',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'grasp_v1.3',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'vv-ak',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'vv-za',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'vv-su',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'vv-gzp',
                'platform': 'agriplace',
                'sections': 'overview,products,evidence,questionnaires,sharing'
            },
            {
                'code': 'vv-a',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'eu-bio',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,review'
            },
            {
                'code': 'vv-su',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'tesco',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'vv-ak',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'vv-gzp',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'grasp_v1.3',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'globalgap_ifa_v5',
                'platform': 'farm_group',
                'sections': 'overview,products,evidence,questionnaires,review'
            },
            {
                'code': 'grasp_v1.3',
                'platform': 'hzpc',
                'sections': 'overview,products,questionnaires,review'
            },
            {
                'code': 'globalgap_ifa_v5',
                'platform': 'hzpc',
                'sections': 'overview,products,questionnaires,review'
            },
            {
                'code': 'F2F-V14',
                'platform': 'agriplace',
                'sections': 'overview,products,questionnaires,sharing'
            },
            {
                'code': 'GGO2-V5',
                'platform': 'agriplace',
                'sections': 'overview,products,questionnaires,sharing'
            },
            {
                'code': 'BRC-V7',
                'platform': 'agriplace',
                'sections': 'overview,products,questionnaires,sharing'
            },
            {
                'code': 'F2F-V14',
                'platform': 'farm_group',
                'sections': 'overview,products,questionnaires,review'
            },
            {
                'code': 'GGO2-V5',
                'platform': 'farm_group',
                'sections': 'overview,products,questionnaires,review'
            },
            {
                'code': 'BRC-V7',
                'platform': 'farm_group',
                'sections': 'overview,products,questionnaires,review'
            },
        ]

        for section in sections:
            if AssessmentType.objects.filter(code=section['code']).exists():
                assessment_type = AssessmentType.objects.get(code=section['code'])
                if AssessmentTypeSection.objects.filter(
                        assessment_type__code=section['code'],
                        platform=section['platform']
                ).exists():
                    print(
                        'Record Found: Updating sections for : assessment={}, platform={}, added sections=({})'.format(
                            assessment_type.code, section['platform'], section['sections']
                        )
                    )
                    AssessmentTypeSection.objects.filter(
                        assessment_type__code=section['code'],
                        platform=section['platform']
                    ).update(
                        sections_list=section['sections']
                    )
                else:
                    print(
                        'Record Not found. Importing new sections for : assessment={}, platform={}, '
                        'added sections=({})'.format(assessment_type.code, section['platform'], section['sections'])
                    )
                    AssessmentTypeSection.objects.get_or_create(
                        assessment_type=assessment_type,
                        platform=section['platform'],
                        sections_list=section['sections']
                    )
            else:
                print('Assessment standard {} not found.'.format(section['code']))

        print('Finished importing assessment type sections')
