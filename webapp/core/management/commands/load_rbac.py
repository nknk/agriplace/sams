from django.contrib.auth.models import Group
from django.core import management
from django.core.management.base import BaseCommand
from django.db import transaction

from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR


class Command(BaseCommand):
    help = 'Script to bootstrap rbac permissions'

    @transaction.atomic
    def handle(self, *args, **options):

        group_names = [
            GROWER,
            INTERNAL_INSPECTOR,
            INTERNAL_AUDITOR
        ]

        if Group.objects.filter(name__in=group_names).exists():
            print("RBAC permissions loading")
            management.call_command("loaddata", "rbac_data.json")
            print("RBAC permissions loaded")

        else:
            print ("Groups missing. Please run 'init_farm_group' command first")
