from django.core.management.base import BaseCommand
from django.db import transaction

from assessment_workflow.models import AssessmentWorkflowFile
from core.utils import MemorySavingQuerysetIterator
from farm_group.models import AssessmentMutationLog
from hzpc.models import MutationLog
from hzpc_workflows.models import HzpcWorkflowContextFiles


@transaction.atomic
class Command(BaseCommand):
    help = 'Fix the workflow file notification type and assessment log times'

    def handle(self, *args, **options):
        self.fix_mutation_logs_time()
        self.fix_notification_type()

    def fix_mutation_logs_time(self):
        print('Fixing mutation logs')
        counter = 0
        for hzpc_mutation_log in MemorySavingQuerysetIterator(MutationLog.objects.all()):
            counter += 1
            print('Mutation counter: {}'.format(counter))
            assessment_mutation_log = AssessmentMutationLog.objects.get(pk=hzpc_mutation_log.pk)
            assessment_mutation_log.mutation_date = hzpc_mutation_log.mutation_date
            assessment_mutation_log.save()

        print('Mutation logs fix completed successfully')

    def fix_notification_type(self):
        print('Fixing workflow file notification type')
        counter = 0
        for hzpc_notfication_file in MemorySavingQuerysetIterator(HzpcWorkflowContextFiles.objects.all()):
            counter += 1
            print('Assessment workflow file counter: {}'.format(counter))
            assessment_notification_file = AssessmentWorkflowFile.objects.get(pk=hzpc_notfication_file.pk)
            if hzpc_notfication_file.notification_type == 'obtaining_sertificate':
                notification_type = 'obtaining_certificate'
            else:
                notification_type = hzpc_notfication_file.notification_type
            assessment_notification_file.notification_type = notification_type
            assessment_notification_file.save()

        print('Workflow file notification type fix completed successfully')
