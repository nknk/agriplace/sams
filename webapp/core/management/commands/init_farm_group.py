from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand
from django.db import transaction

from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR, EXTERNAL_AUDITOR, AGRIPLACE_GROWER


@transaction.atomic
class Command(BaseCommand):
    help = 'Script to bootstrap farm group'

    def handle(self, *args, **options):

        get_user_model().objects.filter(username__startswith='farm_test_').delete()

        # 0 creating groups
        print("Creating groups ...")

        Group.objects.get_or_create(
            name=GROWER,
        )
        Group.objects.get_or_create(
            name=INTERNAL_INSPECTOR,
        )
        Group.objects.get_or_create(
            name=INTERNAL_AUDITOR,
        )
        Group.objects.get_or_create(
            name=EXTERNAL_AUDITOR
        )
        Group.objects.get_or_create(
            name=AGRIPLACE_GROWER
        )
        print("Finished creating groups")
