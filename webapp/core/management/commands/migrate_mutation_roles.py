from django.core.management.base import BaseCommand
from django.db import transaction

from core.constants import GROWER
from core.utils import MemorySavingQuerysetIterator
from farm_group.models import AssessmentMutationLog
from organization.models import OrganizationMembership


@transaction.atomic
class Command(BaseCommand):
    help = 'set the roles of mutation logs from the data'

    def handle(self, *args, **options):
        print('for each mutation log')
        counter = 0
        for mutation_log in MemorySavingQuerysetIterator(AssessmentMutationLog.objects.all()):
            counter += 1
            workflow = mutation_log.assessment_workflow
            print('{} get workflow: {}'.format(counter, workflow.pk))
            primary_organization = workflow.membership.primary_organization
            print('get primary organization: {}'.format(primary_organization.pk))
            if not mutation_log.role:
                if mutation_log.mutation_type == 1:
                    mutation_log.role = workflow.membership.role
                    print('mutation role: {}'.format(mutation_log.role.name))
                else:
                    user_organizations = mutation_log.user.organizations.all()
                    print('get all user organizations')
                    memberships = OrganizationMembership.objects.filter(
                        primary_organization=primary_organization,
                        secondary_organization=user_organizations
                    ).exclude(
                        role__name=GROWER
                    )

                    print('get all memberships')
                    if len(memberships) == 1:
                        membership = memberships[0]
                        mutation_log.role = membership.role
                        print('membership role: {}'.format(mutation_log.role.name))
                    else:
                        raise BaseException("Multiple memberships found")
                mutation_log.save()
                print('save the updated mutation log')

        print('migration completed successfully')
