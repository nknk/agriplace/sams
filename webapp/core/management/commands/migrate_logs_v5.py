from django.core.management import BaseCommand

from django.db import transaction

from assessment_workflow.models import AssessmentWorkflow
from core.utils import MemorySavingQuerysetIterator
from farm_group.models import AssessmentMutationLog
from hzpc.models import MutationLog


@transaction.atomic
class Command(BaseCommand):
    help = 'Schema and data migration script'

    def handle(self, *args, **options):
        self.migrate_to_assessment_mutation_log()

    def migrate_to_assessment_mutation_log(self):
        counter = 0
        print('Started migration of mutation log ... ')
        for log in MemorySavingQuerysetIterator(MutationLog.objects.all()):
            counter += 1
            _, created = AssessmentMutationLog.objects.get_or_create(
                pk=log.pk,
                assessment_workflow=AssessmentWorkflow.objects.get(pk=log.workflow_context),
                assessment=log.assessment,
                question=log.question,
                user=log.user,
                defaults={
                    'question_text': log.question_text,
                    'mutation': log.mutation,
                    'mutation_date': log.mutation_date,
                    'assessment_status': log.assessment_status,
                    'priority': log.priority,
                    'mutation_type': log.mutation_type
                }
            )
            if created:
                print('New assessment mutation log created ... {}'.format(counter))
            else:
                print('Existing assessment mutation log updated ...{}'.format(counter))

        print('Finished migration of mutation log')
