import os
import sys

import requests
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.core.management.base import BaseCommand
from django.db import transaction

from assessments.models import AssessmentType
from sams_project.settings import STATIC_URL


class Command(BaseCommand):
    help = 'Script to bootstrap farm group'
    args = '<hostname>'  # i.e https://agriplace-qa1.herokuapp.com

    def handle(self, *args, **options):

        if len(args):
            hostname = args[0]
            migrate_assessment_type_logos(hostname=hostname.rstrip('/'))
        else:
            print('please provide hostname in the format \'https://agriplace-qa.herokuapp.com\'')


@transaction.atomic
def migrate_assessment_type_logos(hostname):
    assessment_types = AssessmentType.objects.filter(has_logo=True)

    for assessment_type in assessment_types:

        if not assessment_type.code:
            continue

        print('Importing logo for assessment type -> {}'.format(assessment_type.code))

        url_large = "{0}{1}assessments/img/logo-{2}-large.jpg".format(hostname, STATIC_URL, assessment_type.code)
        url_small = "{0}{1}assessments/img/logo-{2}-small.jpg".format(hostname, STATIC_URL, assessment_type.code)

        lf = save_image_to_temp_file(url_large)
        sf = save_image_to_temp_file(url_small)

        if lf is not None:
            assessment_type.logo_large.save(
                os.path.basename(url_large),
                File(lf)
            )
            print('Saved logo for assessment type -> {}'.format(assessment_type.code))

        if sf is not None:
            assessment_type.logo_small.save(
                os.path.basename(url_small),
                File(sf)
            )

        assessment_type.save()


def save_image_to_temp_file(url):
    request = requests.get(url, stream=True)
    print(url)
    if request.status_code != requests.codes.ok:
        return

    lf = NamedTemporaryFile()
    for block in request.iter_content(1024 * 8):
        if not block:
            break
        lf.write(block)
    return lf
