from django.core.management import BaseCommand
from django.db import transaction

from organization.models import OrganizationTheme, AGRIPLACE, FARM_GROUP, HZPC


@transaction.atomic
class Command(BaseCommand):
    help = 'Import agriplace users into AGRIPLACE_FARM_GROUP_SLUG defined in the environment variable '

    def handle(self, *args, **options):
        __, created = OrganizationTheme.objects.get_or_create(layout=AGRIPLACE, defaults={
            'name': 'Agriplace White Label'
        })
        if created:
            print('Created theme: Agriplace White Label')
        else:
            print('Already exists theme: Agriplace White Label')
        __, created = OrganizationTheme.objects.get_or_create(layout=FARM_GROUP, defaults={
            'name': 'Agriplace Group White Label'
        })
        if created:
            print('Created theme: Agriplace Group White Label')
        else:
            print('Already exists theme: Agriplace Group White Label')
        __, created = OrganizationTheme.objects.get_or_create(layout=HZPC, defaults={
            'name': 'HZPC'
        })
        if created:
            print('Created theme: HZPC')
        else:
            print('Already exists theme: HZPC')
