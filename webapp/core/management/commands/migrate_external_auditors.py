from django.contrib.auth.models import Group
from django.core.management import BaseCommand
from django.db import transaction
from django.utils.text import slugify
from core.constants import EXTERNAL_AUDITOR
from organization.models import Organization, OrganizationMembership, OrganizationType
from sams_project.settings import AGRIPLACE_FARM_GROUP_SLUG


@transaction.atomic
class Command(BaseCommand):
    help = 'External auditors data migration script'

    def handle(self, *args, **options):
        external_auditor_organization_type, __ = OrganizationType.objects.get_or_create(
            name=EXTERNAL_AUDITOR,
            slug=slugify(EXTERNAL_AUDITOR)
        )
        auditor_grp, __ = Group.objects.get_or_create(
            name=EXTERNAL_AUDITOR,
        )
        external_auditor_organizations = Organization.objects.filter(
            groups__name=EXTERNAL_AUDITOR
        )
        for organization in external_auditor_organizations:
            auditor_membership = OrganizationMembership.objects.get(
                primary_organization__slug=AGRIPLACE_FARM_GROUP_SLUG,
                secondary_organization=organization
            )
            auditor_membership.organization_type = external_auditor_organization_type
            auditor_membership.role = auditor_grp
            auditor_membership.save()

        print("Migration is completed successfully")
