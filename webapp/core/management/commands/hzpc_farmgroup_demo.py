from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management import BaseCommand
from django.db import transaction
from django.utils.timezone import now

from assessment_workflow.models import AssessmentWorkflow
from assessments.models import AssessmentType
from core.constants import INTERNAL_AUDITOR, INTERNAL_INSPECTOR, GROWER, EXTERNAL_AUDITOR
from organization.models import (
    Organization, OrganizationTheme, FARM_GROUP, HZPC, OrganizationType, OrganizationMembership
)
from core.admin import slugify
from sams.models import UserProfile
from sams_project.settings import AGRIPLACE_FARM_GROUP_SLUG


class FarmgroupCreation:
    def __init__(self, name, auditors, inspectors, growers, theme):
        self.name = name
        self.auditors = auditors
        self.inspectors = inspectors
        self.growers = growers
        self.theme = theme
        self.farmgroup_username_slug = name.lower().replace(" ", "_")
        self.farmgroup_org_slug = slugify(name)
        self.auditor_name = 'Internal auditor'
        self.auditor_username = 'internal_auditor'
        self.inspector_name = 'Internal inspector'
        self.inspector_username = 'internal_inspector'
        self.grower_name = 'Grower'
        self.grower_username = 'grower'

    def create_single_user(self, email, first_name, last_name):
        user = get_user_model().objects.create_user(
            email,
            email,
            'test',
            last_name=last_name,
            first_name=first_name,
            last_login=now()
        )
        UserProfile.objects.get_or_create(user=user)
        return user

    def create_organization(self, name, slug, theme):
        return Organization.objects.create(
            name=name,
            slug=slug,
            theme=theme
        )

    def create_users(self):

        # delete farmgroup if exists
        Organization.objects.filter(slug=self.farmgroup_org_slug).delete()

        # create farmgroup
        farmgroup = self.create_organization(
            name=self.name, slug=self.farmgroup_org_slug, theme=self.theme
        )

        # Get assessment types
        assessment_type_list = AssessmentType.objects.filter(code__in=[
            'TN-V10',
            'globalgap_ifa_v5'
        ])

        # create or get the organization type to be used in membership
        internal_auditor_organization_type, __ = OrganizationType.objects.get_or_create(
            name=INTERNAL_AUDITOR,
            slug=slugify(INTERNAL_AUDITOR)
        )

        internal_inspector_organization_type, __ = OrganizationType.objects.get_or_create(
            name=INTERNAL_INSPECTOR,
            slug=slugify(INTERNAL_INSPECTOR)
        )

        grower_organization_type, __ = OrganizationType.objects.get_or_create(
            name='producer',
            slug=slugify('producer')
        )

        # Create or get groups
        grower_grp, __ = Group.objects.get_or_create(
            name=GROWER,
        )
        inspector_grp, __ = Group.objects.get_or_create(
            name=INTERNAL_INSPECTOR,
        )
        auditor_grp, __ = Group.objects.get_or_create(
            name=INTERNAL_AUDITOR,
        )

        # create email suffix according to the choice of farmgroup
        if self.theme.layout == FARM_GROUP:
            email_suffix = '@agriplace.com'
            selected_workflow = 'farm_group'
        else:
            email_suffix = '@hzpc.nl'
            selected_workflow = 'hzpc'

        print("Creating Users for {}".format(farmgroup))
        for user_type in ('grower', 'auditor', 'inspector'):
            if user_type == 'grower':
                user_role = grower_grp
                counter = self.growers
                organization_type = grower_organization_type
                username = self.grower_username
                name = self.grower_name
            elif user_type == 'auditor':
                user_role = auditor_grp
                counter = self.auditors
                organization_type = internal_auditor_organization_type
                username = self.auditor_username
                name = self.auditor_name
            elif user_type == 'inspector':
                user_role = inspector_grp
                counter = self.inspectors
                organization_type = internal_inspector_organization_type
                username = self.inspector_username
                name = self.inspector_name

            for i in range(1, counter + 1):
                email = "{}{}_{}{}".format(
                    username, i, self.farmgroup_username_slug, email_suffix
                )
                organization_name = "{}{} {} organization".format(
                    name, i, self.name
                )
                organization_slug = slugify(organization_name)

                # delete organization if exists
                Organization.objects.filter(slug=organization_slug).delete()

                # delete user of exists
                get_user_model().objects.filter(email=email).delete()

                user = self.create_single_user(email, username, i)
                organization = self.create_organization(
                    name=organization_name, slug=organization_slug, theme=self.theme
                )
                organization.users.add(user)
                membership, __ = OrganizationMembership.objects.get_or_create(
                    primary_organization=farmgroup,
                    secondary_organization=organization,
                    organization_type=organization_type,
                    role=user_role
                )
                if user_type == 'grower':
                    AssessmentWorkflow.objects.filter(
                        author_organization=organization
                    ).delete()
                    for assessment_type in assessment_type_list:
                        AssessmentWorkflow.objects.create(
                            author=user,
                            author_organization=organization,
                            assessment_type=assessment_type,
                            membership=membership,
                            selected_workflow=selected_workflow
                        )


@transaction.atomic
class Command(BaseCommand):
    help = 'Script to initialize farmgroup test users'

    def handle(self, *args, **options):
        farmgroup_theme, __ = OrganizationTheme.objects.get_or_create(
            name='Agriplace Group White Label',
            slug='agriplace-group-white-label',
            layout=FARM_GROUP
        )

        hzpc_theme, __ = OrganizationTheme.objects.get_or_create(
            name='HZPC',
            slug='hzpc',
            layout=HZPC
        )

        test_farmgroup_name = 'Test Farmgroup'
        test_framgroup = FarmgroupCreation(
            name=test_farmgroup_name, auditors=1, inspectors=2, growers=4, theme=farmgroup_theme
        )
        test_framgroup.create_users()

        test_hzpc_name = 'HZPC Testgroup'
        test_hzpc = FarmgroupCreation(
            name=test_hzpc_name, auditors=1, inspectors=2, growers=4, theme=hzpc_theme
        )
        test_hzpc.create_users()

        print("Creating external auditor")
        email = 'test_external_auditor@agriplace.com'
        get_user_model().objects.filter(email=email).delete()
        test_auditor = get_user_model().objects.create_user(
            email,
            email,
            'test',
            last_name='Test',
            first_name='Auditor',
            last_login=now()
        )
        UserProfile.objects.get_or_create(
            user=test_auditor,
        )
        external_organization_name = 'test auditor organization'
        external_organization_slug = 'test-auditor-organization'
        auditor_group, __ = Group.objects.get_or_create(name=EXTERNAL_AUDITOR)
        external_auditor_org_type, __ = OrganizationType.objects.get_or_create(
            name=EXTERNAL_AUDITOR,
            slug=slugify(EXTERNAL_AUDITOR)
        )
        agriplace_farmgroup = get_or_create_agriplace_farmgroup()
        Organization.objects.filter(slug=external_organization_slug).delete()
        external_organization = Organization.objects.create(
            name=external_organization_name,
            slug=external_organization_slug,
        )
        OrganizationMembership.objects.create(
            primary_organization=agriplace_farmgroup,
            secondary_organization=external_organization,
            organization_type=external_auditor_org_type,
            role=auditor_group
        )
        external_organization.users.add(test_auditor)
        external_organization.groups.add(auditor_group)

        print("Script has been run successfully")


def get_or_create_agriplace_farmgroup():
    farmgroup = Organization.objects.filter(
        slug=AGRIPLACE_FARM_GROUP_SLUG
    ).first()
    if not farmgroup:
        farmgroup = Organization.objects.create(
            name='Agriplace Farmgroup',
            slug=AGRIPLACE_FARM_GROUP_SLUG,
        )
    return farmgroup
