import time
from datetime import datetime

from django.core.management.base import BaseCommand

from django.db import transaction

from assessment_workflow.models import AssessmentWorkflow
from assessment_workflow.managers import INTERNAL_AUDIT_REQUEST
from notifications.models import REMINDER
from notifications.utils import send_email_notification


@transaction.atomic
class Command(BaseCommand):
    help = 'Send subscription reminder emails'

    def add_arguments(self, parser):
        parser.add_argument('emails', type=str)

    def handle(self, *args, **options):
        today_date = datetime.now().date()
        email_data = options.get('emails', "")
        emails = email_data.split(',')
        failed_emails = []
        missing_workflow_emails = []
        success_count = 0
        for email in emails:
            email = email.strip()
            workflow = AssessmentWorkflow.objects.filter(
                harvest_year=today_date.year,
                assessment_status=INTERNAL_AUDIT_REQUEST,
                selected_workflow='hzpc',
                assessment_type__code='globalgap_ifa_v5',
                author__username=email
            ).first()
            if workflow:
                status = send_email_notification(REMINDER, workflow)
                if status:
                    success_count += 1
                    print(u"Email sent successfully to {}".format(email))
                else:
                    failed_emails.append(email)
                    print(u"Email failed to {}".format(email))
                time.sleep(1)
            else:
                missing_workflow_emails.append(email)
                print(u"Workflow doesn't exist for {}".format(email))

        total_emails = len(emails)
        print(u"\n\nSummary:")
        print(u"\nTotal emails count: {}".format(total_emails))
        print(u"\nSuccessful emails count: {}".format(success_count))
        print(u"\nFailed emails count: {}".format(total_emails-success_count))
        if failed_emails:
            print(u"\nFailed emails due to exception:\n{}".format(u",".join(failed_emails)))
        if missing_workflow_emails:
            print(u"\nFailed emails due to missing workflow:\n{}".format(u",".join(missing_workflow_emails)))

        print(u"\nCommand is completed")
