from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core import management
from django.core.management import BaseCommand
from django.db import transaction

from assessment_workflow.models import AssessmentWorkflow, AssessmentWorkflowFile
from assessments.models import Assessment
from core.admin import slugify
from core.constants import (
    GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR, AGRIPLACE_GROWER, HZPC_GROWER, HZPC_AUDITOR, HZPC_COORDINATOR
)
from farm_group.models import AssessmentAgreement
from hzpc.models import HzpcAssessmentAgreement
from hzpc_notifications.models import (
    NotificationSettings as HzpcNotificationSettings, NotificationJournal as HzpcNotificationJournal
)
from hzpc_workflows.models import HzpcWorkflowContext, HzpcWorkflowContextFiles
from notifications.models import NotificationSettings, NotificationJournal
from organization.models import (
    Organization, OrganizationTheme, FARM_GROUP, HZPC, AGRIPLACE, OrganizationMembership, OrganizationType
)
from sams_project.settings import AGRIPLACE_FARM_GROUP_SLUG

User = get_user_model()


@transaction.atomic
class Command(BaseCommand):
    help = 'Schema and data migration script'

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--hzpc',
            action='store_true',
            dest=HZPC,
            default=False,
            help='Migrate hzpc',
        )

    def handle(self, *args, **options):
        selected_workflow = FARM_GROUP

        if options[HZPC]:
            selected_workflow = HZPC

        print('Pre-requisite: migrate the schema')
        management.call_command("migrate")

        print('Started migrating to new schema/apps')
        print("1 - Create farm groups")
        management.call_command("init_farm_group")

        print ('Create themes')
        management.call_command("create_themes")

        grower_organization_type, _ = OrganizationType.objects.get_or_create(
            name='producer',
            defaults={
                'slug': slugify('producer')
            }
        )

        print("2 - Load RBAC")
        management.call_command("load_rbac")

        print("3 - Migrate: hzpc_notifications ---> notifications")
        self.migrate_to_notification_settings()
        self.migrate_to_notification_journal()

        print("4 - Migrate: hzpc_workflows ---> assessment_workflow")
        self.migrate_to_farm_group(selected_workflow, grower_organization_type)

        if selected_workflow == FARM_GROUP:
            self.migrate_to_agriplace(grower_organization_type)
            self.migrate_to_agriplace_null_profile(grower_organization_type)
            self.migrate_to_agriplace_null_platform_context(grower_organization_type)

        print("5 - Migrate: hzpc ---> farm_group")
        self.migrate_to_assessment_agreement()

    def migrate_to_farm_group(self, selected_workflow, grower_organization_type):
        auditor_organization_type, _ = OrganizationType.objects.get_or_create(
            name=INTERNAL_INSPECTOR,
            defaults={
                'slug': slugify(INTERNAL_INSPECTOR)
            }
        )
        coordinator_organization_type, _ = OrganizationType.objects.get_or_create(
            name=INTERNAL_AUDITOR,
            defaults={
                'slug': slugify(INTERNAL_AUDITOR)
            }
        )
        print("Started migrating user who have platform_context={} ... ".format(selected_workflow))

        if selected_workflow == HZPC:
            farm_group_users = User.objects.all()
        else:
            farm_group_users = User.objects.filter(profile__platform_context=selected_workflow)
        auditor_index = 0
        coordinator_index = 0
        for user in farm_group_users:
            print('Processing auditors and coordinators...')
            organization = user.organizations.first()
            print ('Current user: {}'.format(user.username))
            if not user.groups.count():
                continue

            if HZPC_AUDITOR == user.groups.first().name:
                auditor_index += 1
                auditor_role = Group.objects.get(name=INTERNAL_INSPECTOR)
                self.set_farm_group_theme(organization, selected_workflow)
                self.migrate_farm_group_users(
                    auditor_organization_type, auditor_role, organization, user, auditor_index
                )
            elif HZPC_COORDINATOR == user.groups.first().name:
                coordinator_index += 1
                coordinator_role = Group.objects.get(name=INTERNAL_AUDITOR)
                self.set_farm_group_theme(organization, selected_workflow)
                self.migrate_farm_group_users(
                    coordinator_organization_type, coordinator_role, organization, user, coordinator_index
                )

        for user in farm_group_users:
            print('Processing growers...')
            organization = user.organizations.first()
            print ('Current user: {}'.format(user.username))
            if not user.groups.count():
                continue

            if HZPC_GROWER == user.groups.first().name:
                farm_group = organization.parent_organization
                self.set_farm_group_theme(farm_group, selected_workflow)
                membership, __ = OrganizationMembership.objects.get_or_create(
                    primary_organization=farm_group,
                    secondary_organization=organization,
                    organization_type=grower_organization_type,
                    role=Group.objects.get(name=GROWER),
                    membership_number=organization.producer_number
                )

                self.migrate_to_assessment_workflow(membership, organization, selected_workflow)

        print("Finished migrating user who have platform_context={}".format(selected_workflow))

    def set_farm_group_theme(self, farm_group, selected_workflow):
        farm_group.theme = OrganizationTheme.objects.get(layout=selected_workflow)
        farm_group.save()

    def migrate_farm_group_users(self, organization_type, role, farm_group, user, index):
        organization, __ = Organization.objects.get_or_create(
            slug='{}{}'.format(role, index),
            defaults={
                'name': '{}{}'.format(role, index)
            }
        )
        __, __ = OrganizationMembership.objects.get_or_create(
            primary_organization=farm_group,
            secondary_organization=organization,
            organization_type=organization_type,
            role=role,
            membership_number=organization.producer_number
        )
        print('added user: {} to {}'.format(user.username, organization))
        organization.users.add(user)
        print('removing user: {} from {}'.format(user.username, farm_group))
        farm_group.users.remove(user)

    def migrate_to_assessment_workflow(self, membership, organization, selected_workflow):
        workflows = HzpcWorkflowContext.objects.filter(author_organization=organization)
        for ctx in workflows:
            print('Create/update assessment workflow {}'.format(ctx.pk))
            auditor_organization = ctx.auditor_user.organizations.first() if ctx.auditor_user else None
            assessment_workflow, created = AssessmentWorkflow.objects.get_or_create(
                pk=ctx.pk,
                defaults={
                    'membership': membership,
                    'remarks': ctx.remarks,
                    'is_test': ctx.is_test,
                    'modified_time': ctx.modified_time,
                    'created_time': ctx.created_time,
                    'author': ctx.author,
                    'author_organization': ctx.author_organization,
                    'assessment_type': ctx.assessment_type,
                    'assessment': ctx.assessment,
                    'assessment_status': ctx.assessment_status,
                    'harvest_year': ctx.harvest_year,
                    'auditor_organization': auditor_organization,
                    'auditor_user': ctx.auditor_user,
                    'audit_preferred_month': ctx.audit_preferred_month,
                    'audit_date_planned': ctx.audit_date_planned,
                    'audit_date_actual': ctx.audit_date_actual,
                    'shared_with_auditor': ctx.shared_with_auditor,
                    'shared_with_coordinator': ctx.shared_with_coordinator,
                    'workflow_status': ctx.workflow_status,
                    'judgement': ctx.judgement,
                    're_audit': ctx.reaudit,
                    'action_required_notification_status': ctx.action_required_notification_status,
                    'overview_description': ctx.overview_description,
                    'internal_description': ctx.internal_description,
                    'document_review_date': ctx.document_review_date,
                    'action_required_date': ctx.action_required_date,
                    'certification_date': ctx.certification_date,
                    'selected_workflow': selected_workflow
                }
            )
            if created:
                print('New assessment workflow created ...')
            else:
                print('Existing assessment workflow updated ...')

            self.migrate_to_assessment_workflow_file(assessment_workflow)

    def migrate_to_assessment_workflow_file(self, assessment_workflow):
        ctx_files = HzpcWorkflowContextFiles.objects.filter(hzpc_workflow=assessment_workflow.pk)
        print('Migrating HzpcWorkflowContextFiles count: {}'.format(ctx_files.count()))
        for ctx_file in ctx_files:
            print('get new workflow: {}'.format(assessment_workflow.pk))
            _, created = AssessmentWorkflowFile.objects.get_or_create(
                pk=ctx_file.uuid,
                assessment_workflow=assessment_workflow,
                defaults={
                    'remarks': ctx_file.remarks,
                    'is_test': ctx_file.is_test,
                    'modified_time': ctx_file.modified_time,
                    'created_time': ctx_file.created_time,
                    'file': ctx_file.file,
                    'attachment_type': ctx_file.attachment_type,
                    'workflow_status': ctx_file.workflow_status,
                    'notification_type': ctx_file.notification_type
                }
            )
            if created:
                print('New assessment workflow file created ...')
            else:
                print('Existing assessment workflow updated updated ...')

    def migrate_to_agriplace(self, grower_organization_type):
        print("Started migrating core platform users who have platform_context=agriplace ... ")
        agriplace_users = User.objects.filter(profile__platform_context='agriplace')
        self.migrate_to_core_platform(
            users=agriplace_users, grower_organization_type=grower_organization_type,
            agriplace_grower_role=Group.objects.get(name=AGRIPLACE_GROWER),
            agriplace_theme=OrganizationTheme.objects.get(layout=AGRIPLACE)
        )

        print("Finished migrating core platform users who have platform_context=agriplace")

    def migrate_to_agriplace_null_profile(self, grower_organization_type):
        print("Started migrating core platform users who have don't have a profile ... ")
        users_null_profile = User.objects.filter(profile__isnull=True)
        self.migrate_to_core_platform(
            users=users_null_profile, grower_organization_type=grower_organization_type,
            agriplace_grower_role=Group.objects.get(name=AGRIPLACE_GROWER),
            agriplace_theme=OrganizationTheme.objects.get(layout=AGRIPLACE)
        )
        print("Finished migrating core platform users who have don't have a profile")

    def migrate_to_agriplace_null_platform_context(self, grower_organization_type):
        print("Started migrating core platform users who don't have a platform_context set in profile ... ")
        users_null_platform_context = User.objects.filter(profile__platform_context__isnull=True)
        self.migrate_to_core_platform(
            users=users_null_platform_context, grower_organization_type=grower_organization_type,
            agriplace_grower_role=Group.objects.get(name=AGRIPLACE_GROWER),
            agriplace_theme=OrganizationTheme.objects.get(layout=AGRIPLACE)
        )
        print("Finished migrating core platform users who don't have a platform_context set in profile")

    def migrate_to_core_platform(self, users, grower_organization_type, agriplace_grower_role, agriplace_theme):
        for user in users:
            print('Processing ...')
            for organization in user.organizations.all():
                if OrganizationMembership.objects.filter(primary_organization=organization).exists():
                    continue

                agriplace_farm_group_organization, created = Organization.objects.get_or_create(
                    slug=AGRIPLACE_FARM_GROUP_SLUG,
                    defaults={
                        'name': AGRIPLACE_FARM_GROUP_SLUG,
                        'theme': agriplace_theme
                    }
                )
                membership, created = OrganizationMembership.objects.get_or_create(
                    primary_organization=agriplace_farm_group_organization,
                    secondary_organization=organization,
                    organization_type=grower_organization_type,
                    role=agriplace_grower_role,
                    membership_number=organization.producer_number
                )

                assessments = Assessment.objects.filter(organization=organization)
                for assessment in assessments:
                    if AssessmentWorkflow.objects.filter(assessment=assessment).exists() or (
                                assessment.assessment_type.kind != 'assessment'
                    ):
                        continue
                    else:
                        assessment_workflow = AssessmentWorkflow.objects.initialize_workflow(
                            assessment.created_by, assessment.assessment_type, None, membership, AGRIPLACE
                        )
                        assessment_workflow.assign_assessment(assessment)

    def migrate_to_assessment_agreement(self):
        hzpc_agreements = HzpcAssessmentAgreement.objects.all()
        print('Started migration ... ')
        for agreement in hzpc_agreements:
            _, created = AssessmentAgreement.objects.get_or_create(
                pk=agreement.pk,
                defaults={
                    'remarks': agreement.remarks,
                    'is_test': agreement.is_test,
                    'modified_time': agreement.modified_time,
                    'created_time': agreement.created_time,
                    'harvest_year': agreement.harvest_year,
                    'assessment_type': agreement.assessment_type,
                    'user': agreement.hzpc_user,
                    'form_data': agreement.form_data
                }
            )
            if created:
                print('New assessment agreement created ...')
            else:
                print('Existing assessment agreement updated ...')

        print('Finished migration.')

    def migrate_to_notification_settings(self):
        hzpc_notification_settings = HzpcNotificationSettings.objects.all()
        print('Started migration ... ')
        for hzpc_notification_setting in hzpc_notification_settings:
            _, created = NotificationSettings.objects.get_or_create(
                pk=hzpc_notification_setting.uuid,
                defaults={
                    'remarks': hzpc_notification_setting.remarks,
                    'is_test': hzpc_notification_setting.is_test,
                    'modified_time': hzpc_notification_setting.modified_time,
                    'created_time': hzpc_notification_setting.created_time,
                    'active': hzpc_notification_setting.active,
                    'farm_group_portal_url': hzpc_notification_setting.hzpc_portal_url,
                    'no_reply_address': hzpc_notification_setting.no_reply_address,
                    'gg_subscription_notification_date': hzpc_notification_setting.gg_subscription_notification_date,
                    'gg_subscription_reminder_date': hzpc_notification_setting.gg_subscription_reminder_date,
                    'farm_group_phone': hzpc_notification_setting.hzpc_phone,
                    'coordinator_name': hzpc_notification_setting.coordinator_name,
                    'coordinator_email': hzpc_notification_setting.coordinator_email,
                    'coordinator_signature': hzpc_notification_setting.coordinator_signature,
                    'farm_group_logo': hzpc_notification_setting.hzpc_logo,
                    'certificate': hzpc_notification_setting.certificate,
                }
            )
            if created:
                print('New notification settings created ...')
            else:
                print('Existing notification settings updated ...')

        print('Finished migration.')

    def migrate_to_notification_journal(self):
        hzpc_notification_journals = HzpcNotificationJournal.objects.all()
        print('Started migration ... ')
        for hzpc_notification_journal in hzpc_notification_journals:
            _, created = NotificationJournal.objects.get_or_create(
                pk=hzpc_notification_journal.uuid,
                defaults={
                    'remarks': hzpc_notification_journal.remarks,
                    'is_test': hzpc_notification_journal.is_test,
                    'modified_time': hzpc_notification_journal.modified_time,
                    'created_time': hzpc_notification_journal.created_time,
                    'created_date_time': hzpc_notification_journal.created_date_time,
                    'notification_type': hzpc_notification_journal.notification_type,
                    'notification_channel': hzpc_notification_journal.notification_channel,
                    'target': hzpc_notification_journal.target,
                    'status': hzpc_notification_journal.status,
                    'description': hzpc_notification_journal.description
                }
            )
            if created:
                print('New notification journal created ...')
            else:
                print('Existing notification journal updated ...')
