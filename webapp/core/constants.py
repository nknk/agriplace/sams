from django.utils.translation import ugettext_lazy as _

GROWER = 'grower'
INTERNAL_INSPECTOR = 'internal_inspector'
INTERNAL_AUDITOR = 'internal_auditor'
EXTERNAL_AUDITOR = 'external_auditor'
AGRIPLACE_GROWER = 'agriplace_grower'
FARM_GROUPS = (
    GROWER,
    INTERNAL_INSPECTOR,
    INTERNAL_AUDITOR,
)

# Old group names used for data migrations
HZPC_GROWER = 'hzpc_grower'
HZPC_AUDITOR = 'hzpc_auditor'
HZPC_COORDINATOR = 'hzpc_coordinator'

ORGANIZATION_TYPE_CHOICES = (
    ('producer', _("Producer/Farmer")),
    ('coop', _("Producer group / Cooperative")),
    ('processor', _("Processor / Manufacturer")),
    ('trader', _("Trader")),
    ('licensee', _("Licensee / Brand owner")),
    ('auditor', _("Auditor")),
    ('other', _("Other")),
    ('buyer', _("Buyer")),
    ('internal_auditor', _('Internal Auditor')),
    ('internal_inspector', _('Internal Inspector')),
)
