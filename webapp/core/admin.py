from django.conf.urls import url
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin.util import flatten_fieldsets, reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _

from assessment_workflow.forms import WorkflowAbpCreateForm
from assessment_workflow.models import AssessmentWorkflow
from assessments.helpers import create_pending_translation_report, create_excel_response
from core.models import *
from organization.forms import OrganizationAbpCreateForm
from organization.models import OrganizationMembership, OrganizationType, Organization
from sams.forms import UserAbpCreateForm


def has_read_only_permission(request, ct):
    if request.user.has_perm(
                    '%s.view_%s' % (ct.app_label, ct.model)) and not request.user.has_perm(
                    '%s.change_%s' % (ct.app_label, ct.model)) and not request.user.has_perm(
                    '%s.add_%s' % (ct.app_label, ct.model)) and not request.user.has_perm(
                    '%s.delete_%s' % (ct.app_label, ct.model)):
        return True
    else:
        return False


class AdminViewPermissionMixin(object):

    def has_change_permission(self, request, obj=None):
        ct = ContentType.objects.get_for_model(self.model)
        result = False
        if request.user.is_superuser:
            result = True
        else:
            if request.user.has_perm('%s.view_%s' % (ct.app_label, ct.model)):
                result = True
            else:
                if request.user.has_perm('%s.change_%s' % (ct.app_label, ct.model)):
                    result = True
        return result

    def get_readonly_fields(self, request, obj=None):
        ct = ContentType.objects.get_for_model(self.model)
        if not request.user.is_superuser and has_read_only_permission(request, ct):
            if self.declared_fieldsets:
                return flatten_fieldsets(self.declared_fieldsets)
            else:
                return list(set(
                    [field.name for field in self.opts.local_fields] +
                    [field.name for field in self.opts.local_many_to_many]
                ))
        return self.readonly_fields


class BaseAdmin(AdminViewPermissionMixin, admin.ModelAdmin):
    """
    Base class for admin objects
    """
    show_fields = ()
    use_fields = ()


class BaseTabularInline(AdminViewPermissionMixin, admin.TabularInline):
    """
    Base class for tabular inlines
    """
    extra = 0


class BaseAbpAdminMixin(object):
    add_organization_template_name = 'core/abp_manager/add_organization.html'
    add_workflow_template_name = 'core/abp_manager/add_workflow.html'
    add_user_template_name = 'core/abp_manager/add_user.html'

    def get_model_info(self):
        # module_name is renamed to model_name in Django 1.8
        app_label = self.model._meta.app_label
        try:
            return (app_label, self.model._meta.model_name,)
        except AttributeError:
            return (app_label, self.model._meta.module_name,)

    def is_abp_manager(self, request):
        return True if get_user_model().objects.filter(
            pk=request.user.pk, groups__name='abp_manager'
        ).exists() else False

    def get_urls(self):
        urls = super(BaseAbpAdminMixin, self).get_urls()
        info = self.get_model_info()
        abp_manager_urls = [
            url(r'^abp_add_organization/$',
                self.admin_site.admin_view(self.add_organization),
                name='%s_%s_abp_add_organization' % info),
            url(r'^abp_add_workflow/(?P<organization_pk>.+)/$',
                self.admin_site.admin_view(self.add_workflow),
                name='%s_%s_abp_add_workflow' % info),
            url(r'^abp_add_user/(?P<organization_pk>.+)/$',
                self.admin_site.admin_view(self.add_user),
                name='%s_%s_abp_add_user' % info),
        ]
        return abp_manager_urls + urls

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        if self.is_abp_manager(request):
            _, model_name = self.get_model_info()
            if model_name in ['organization']:
                self.change_list_template = 'core/abp_manager/{}_change_list.html'.format(model_name)
        else:
            self.change_list_template = None
        return super(BaseAbpAdminMixin, self).changelist_view(request, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        if self.is_abp_manager(request):
            _, model_name = self.get_model_info()
            if model_name in ['organization', 'assessmentworkflow']:
                self.change_form_template = 'core/abp_manager/{}_change_form.html'.format(model_name)
        else:
            self.change_form_template = None
        return super(BaseAbpAdminMixin, self).change_view(
            request, object_id, form_url, extra_context=extra_context
        )

    def add_view(self, request, form_url='', extra_context=None):
        self.change_form_template = None
        return super(BaseAbpAdminMixin, self).add_view(request, form_url, extra_context)

    def add_organization(self, request, *args, **kwargs):
        form_class = OrganizationAbpCreateForm
        parent_organization_ids = OrganizationMembership.get_parents_by_user(request.user)
        if request.method == 'POST':
            form = form_class(parent_organization_ids, request.POST, request.FILES)
            if form.is_valid():
                try:
                    new_organization = form.save()
                    # remove when after OrganizationMembership model update
                    dummy_type = OrganizationType.objects.get(slug='producer')
                    OrganizationMembership.objects.create(
                        primary_organization=form.cleaned_data['farm_group'],
                        secondary_organization=new_organization,
                        role=form.cleaned_data['role'],
                        organization_type=dummy_type
                    )
                except IntegrityError as ex:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        u"Organization {} can't be added. Reason - {}".format(form.cleaned_data['name'], ex))
                    return HttpResponseRedirect(
                        reverse('admin:{0}_{1}_changelist'.format(*self.get_model_info()))
                    )
                messages.add_message(request, messages.SUCCESS, u'Organization {} added'.format(new_organization.name))
                return HttpResponseRedirect(
                    reverse('admin:{0}_{1}_change'.format(*self.get_model_info()), args=(new_organization.pk,))
                )
        context = {
            'title': _("Add organization"),
            'form': form_class(parent_organization_ids),
            'opts': self.model._meta
        }
        return TemplateResponse(request, [self.add_organization_template_name], context)

    def add_workflow(self, request, *args, **kwargs):
        form_class = WorkflowAbpCreateForm
        parent_organization_ids = OrganizationMembership.get_parents_by_user(request.user)
        if request.method == 'POST':
            form = form_class(parent_organization_ids, request.POST, request.FILES)
            if form.is_valid():
                try:
                    target_organization = Organization.objects.get(pk=kwargs['organization_pk'])
                except ObjectDoesNotExist as ex:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "Workflow can't be added. Reason - {}".format(ex))
                    return HttpResponseRedirect(
                        reverse('admin:{0}_{1}_changelist'.format(*self.get_model_info()))
                    )
                try:
                    role = 'agriplace_grower' if form.cleaned_data['farm_group'].slug == 'agriplace' else 'grower'
                    target_membership = OrganizationMembership.objects.get(
                        primary_organization=form.cleaned_data['farm_group'],
                        secondary_organization=target_organization,
                        role=Group.objects.get(name=role)
                    )
                except ObjectDoesNotExist as ex:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "Workflow can't be added. Reason - {}".format(ex))
                    return HttpResponseRedirect(
                        reverse('admin:{0}_{1}_changelist'.format(*self.get_model_info()))
                    )
                try:
                    new_workflow = AssessmentWorkflow.objects.create(
                        assessment_type=form.cleaned_data['assessment_type'],
                        harvest_year=form.cleaned_data['harvest_year'],
                        author_organization=target_organization,
                        author=target_organization.users.all().first(),
                        membership=target_membership,
                        selected_workflow=form.cleaned_data['selected_workflow'],
                    )
                except IntegrityError as ex:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "Workflow can't be added. Reason - {}".format(ex))
                    return HttpResponseRedirect(
                        reverse('admin:{0}_{1}_changelist'.format(*self.get_model_info()))
                    )
                messages.add_message(request, messages.SUCCESS,
                                     u'Workflow {} added'.format(new_workflow.assessment_type.name))
                return HttpResponseRedirect(
                    reverse('admin:{0}_{1}_change'.format(
                        'assessment_workflow', 'assessmentworkflow'), args=(new_workflow.pk,))
                )
        context = {
            'title': _("Add workflow"),
            'form': form_class(parent_organization_ids),
            'opts': self.model._meta
        }
        return TemplateResponse(request, [self.add_workflow_template_name], context)

    def add_user(self, request, *args, **kwargs):
        form_class = UserAbpCreateForm
        if request.method == 'POST':
            form = form_class(request.POST, request.FILES)
            if form.is_valid():
                try:
                    target_organization = Organization.objects.get(pk=kwargs['organization_pk'])
                except ObjectDoesNotExist as ex:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "User can't be added. Reason - {}".format(ex))
                    return HttpResponseRedirect(
                        reverse('admin:{0}_{1}_changelist'.format(*self.get_model_info()))
                    )
                try:
                    new_user = get_user_model().objects.create_user(
                        username=form.cleaned_data['email'],
                        email=form.cleaned_data['email'],
                        password=get_user_model().objects.make_random_password(),
                        first_name=form.cleaned_data['first_name'],
                        last_name=form.cleaned_data['last_name']
                    )
                    target_organization.users.add(new_user)
                except IntegrityError as ex:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        "User {} can't be added. Reason - {}".format(form.cleaned_data['email'], ex))
                    return HttpResponseRedirect(
                        reverse('admin:{0}_{1}_changelist'.format(*self.get_model_info()))
                    )
                messages.add_message(request, messages.SUCCESS,
                                     u"User {} added. Please don't forget to set password".format(new_user.username))
                return HttpResponseRedirect(
                    reverse('admin:{0}_{1}_change'.format('auth', 'user'), args=(new_user.pk,))
                )
        context = {
            'title': _("Add User"),
            'form': form_class(),
            'opts': self.model._meta
        }
        return TemplateResponse(request, [self.add_user_template_name], context)


class ExtendedActionsMixin(object):
    # actions that can be executed with no items selected on the admin change list.
    # The filtered queryset displayed to the user will be used instead
    extended_actions = []

    def changelist_view(self, request, extra_context=None):
        # if a extended action is called and there's no checkbox selected, select one with
        # invalid id, to get an empty queryset
        if 'action' in request.POST and request.POST['action'] in self.extended_actions:
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                post.update({admin.ACTION_CHECKBOX_NAME: 0})
                request._set_post(post)
        return super(ExtendedActionsMixin, self).changelist_view(request, extra_context)

    def get_changelist_instance(self, request):
        """
        Returns a simple ChangeList view instance of the current ModelView.
        (It's a simple instance since we don't populate the actions and list filter
        as expected since those are not used by this class)
        """
        list_display = self.get_list_display(request)
        list_display_links = self.get_list_display_links(request, list_display)
        list_filter = self.get_list_filter(request)
        search_fields = self.get_search_fields(request)
        list_select_related = self.get_list_select_related(request)

        ChangeList = self.get_changelist(request)

        return ChangeList(
            request, self.model, list_display,
            list_display_links, list_filter, self.date_hierarchy,
            search_fields, list_select_related, self.list_per_page,
            self.list_max_show_all, self.list_editable, self,
        )

    def get_filtered_queryset(self, request):
        """
        Returns a queryset filtered by the URLs parameters
        """
        cl = self.get_changelist_instance(request)
        return cl.get_queryset(request)


class TranslationMixin(object):
    actions = ['generate_pending_translations']

    def generate_pending_translations(self, request, queryset):
        workbook = create_pending_translation_report(self.trans_fields, queryset)
        return create_excel_response(workbook, self.__class__.__name__)


admin.site.register(ContentPush)
