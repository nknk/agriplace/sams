import copy


def poly_area(poly):
    total = 0.0
    n = len(poly)
    for i in range(n):
        v1 = poly[i]
        v2 = poly[(i + 1) % n]
        total += v1[0] * v2[1] - v1[1] * v2[0]
    return abs(total / 2)


def get_area(crop_field):
    coordinates = crop_field.pos_list.split(" ")
    coordinates.append(None)

    coord_list = []
    for long_lat in zip(coordinates[::2], coordinates[1::2]):
        coord_list.append((float(long_lat[1]), float(long_lat[0])))

    polygon_area = poly_area(coord_list) / 10000

    if polygon_area > 0.1:
        return polygon_area
    else:
        return 0.1


class MemorySavingQuerysetIterator(object):
    def __init__(self, queryset, max_obj_num=1000):
        self._base_queryset = queryset
        self._generator = self._setup()
        self.max_obj_num = max_obj_num

    def _setup(self):
        for i in xrange(0, self._base_queryset.count(), self.max_obj_num):
            # By making a copy of of the queryset and using that to actually access
            # the objects we ensure that there are only `max_obj_num` objects in
            # memory at any given time
            smaller_queryset = copy.deepcopy(self._base_queryset)[i:i + self.max_obj_num]
            for obj in smaller_queryset.iterator():
                yield obj

    def __iter__(self):
        return self

    def next(self):
        return self._generator.next()
