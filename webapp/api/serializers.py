from rest_framework import serializers


class UuidModelSerializer(serializers.ModelSerializer):

    def get_identity(self, data):
        try:
            return data.get('uuid', None)
        except AttributeError:
            return None

