"""
The internal API application includes all `api` packages of individual applications
under single URL scheme.
"""

from django.conf.urls import patterns, url, include

# Login URLs for the browseable API.
urlpatterns = patterns(
    '',
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'accounts/', include('accounts.api.external.urls')),
)
