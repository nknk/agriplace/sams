from rest_framework import serializers

from assessments.models import (
    Questionnaire,
    QuestionnaireHeading,
    Question,
    PossibleAnswer,
    Answer
)


class QuestionnaireDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Questionnaire
        fields = ('elements',)


class QuestionnaireHeadingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = QuestionnaireHeading


class PossibleAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = PossibleAnswer


class QuestionSerializer(serializers.ModelSerializer):
    possible_answers = PossibleAnswerSerializer(many=True)

    class Meta:
        model = Question


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
