"""
The internal API application includes all `api` packages of individual applications
under single URL scheme.
"""

from django.conf.urls import patterns, url, include

# Login URLs for the browseable API.
from api.internal.views import schema_view
from assessment_types.api.internal.views import AssessmentTypeRetrieveAPIView, AssessmentTypesStandards

urlpatterns = patterns(
    '',

    # Login and logout views for the browsable API
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # Auth tokens creation
    url(
        r'^api-token-auth/',
        'rest_framework.authtoken.views.obtain_auth_token',
        name='api-token-auth'
    ),

    # Swagger
    url('^api-schema/', schema_view),

    # Assessment types
    url(r'^memberships/(?P<membership_pk>.+)/assessment-types/', include('assessment_types.api.internal.urls')),
    # Organization assessments
    url(r'^organizations/(?P<organization_pk>.\w+)/assessments/', include('assessments.api.internal.urls')),

    # Organization assessments
    url(r'^memberships/(?P<membership_pk>.+)/assessments/', include('assessments.api.internal.membership_urls')),
    url(r'^v2/memberships/(?P<membership_pk>.+)/assessments/', include('assessments.api.internal.urls_v2')),

    # Organization attachments
    url(r'^organizations/(?P<organization_pk>.\w+)/attachments/', include('attachments.api.internal.urls')),
    url(r'^organizations/(?P<organization_pk>.\w+)/shared_attachments/',
        include('attachments.api.internal.shared_attachments_urls')),

    # Farm group workflows
    url(r'^organizations/(?P<organization_pk>.\w+)/workflows/', include('farm_group.api.internal.urls')),

    # Farm group workflows
    url(r'^memberships/(?P<membership_pk>.+)/workflows/', include('farm_group.api.internal.membership_urls')),

    # Organization
    url(r'^organizations/', include('organization.api.internal.urls')),

    # Organization
    url(r'^memberships/', include('organization.api.internal.membership_urls')),

    # RBAC
    url(r'^rbac/', include('rbac.api.internal.urls')),

    # Forms
    url(r'^forms/', include('form_builder.api.internal.urls')),

    # extras
    url(r'^app/', include('app_settings.api.internal.urls')),

    # Platform context
    url(r'^platform-context/', 'api.internal.views.platform_context'),

    # user memberships
    url(r'^user-info/', 'api.internal.views.user_info', name="user-memberships-info"),

    # Assessment standards open APIs
    url(r'^assessment-standard/editions/$', AssessmentTypesStandards.as_view(), name='assessments-types-versions'),
    url(
        r'^assessment-standard/(?P<pk>.\w+)/(?P<lang>.\w+)/$', AssessmentTypeRetrieveAPIView.as_view(),
        name='assessment-standard'
        ),
)

