from django.contrib.auth.models import Group
import faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from api.internal.tests.mixins import UserAPIFactoryMixin, APITestSetupMixin
from core.constants import GROWER
from organization.models import OrganizationTheme, AGRIPLACE, FARM_GROUP, HZPC
from organization.tests.factories import OrganizationFactory, OrganizationTypeFactory, OrganizationMembershipFactory, \
    OrganizationThemeFactory

faker = faker.Factory.create()


class TestAPILogin(APITestCase, UserAPIFactoryMixin):
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def test_api_token_auth(self):
        """
        test api login and get authorization token in return
        :param username: default=tester01
        :param password: default=Prod!
        :return Authorization token as string:
        """
        self.create_user(username=self.USERNAME, password=self.PASSWORD)

        response = self.client.post(
            reverse('api-token-auth'), {'username': self.USERNAME, 'password': self.PASSWORD}, follow=True
        )
        response = response.client._handle_redirects(response)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('token' in response.data, msg='The response must a \'token\' field')
        self.assertNotEqual(response.data['token'], '', msg='The token field must not be empty')


class AssessmentMembershipInfoTest(APITestCase, APITestSetupMixin):
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_themes()
        self.setup_memberships()
        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def create_themes(self):
        self.agriplace_theme = OrganizationThemeFactory.create(layout=AGRIPLACE, name = 'Agriplace White Label')
        self.farmgroup_theme = OrganizationThemeFactory.create(layout=FARM_GROUP, name = 'Agriplace Group White Label')
        self.hzpc_theme = OrganizationThemeFactory.create(layout=HZPC, name = 'HZPC')
        self.themes = [self.agriplace_theme, self.farmgroup_theme, self.hzpc_theme]

    def setup_memberships(self):
        grower_group, created = Group.objects.get_or_create(name=GROWER)
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.primary_organizations = []
        self.memberships = []
        self.secondary_organizations = []
        for theme in self.themes:
            primary_organization = OrganizationFactory.create(
                theme=theme
            )
            secondary_organization = OrganizationFactory.create(
                users=(self.user,)
            )
            organization_type = OrganizationTypeFactory.create(name=GROWER)
            membership = OrganizationMembershipFactory.create(
                primary_organization=primary_organization,
                secondary_organization=secondary_organization,
                organization_type=organization_type,
                role=grower_group,
            )
            self.primary_organizations.append(primary_organization)
            self.secondary_organizations.append(secondary_organization)
            self.memberships.append(membership)

    def test_user_membership_info(self):
        response = self.client.get(
            reverse('user-memberships-info'),
            HTTP_AUTHORIZATION=self.token
        )
        data = response.data
        memberships = data['memberships']
        current_membership = data['current_membership_uuid']
        self.assertEqual(len(memberships), 3)
        self.assertIn(current_membership, [membership.uuid for membership in self.memberships])
