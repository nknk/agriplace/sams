import faker
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.reverse import reverse

from assessment_types.tests.factories import AssessmentTypeFactory
from assessment_workflow.models import AssessmentWorkflow
from assessments.tests.factories import AssessmentFactory, AssessmentDocumentLinkFactory
from attachments.tests.mixins import AttachmentAPIFactoryMixin
from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.tests.factories import GroupFactory
from organization.models import AGRIPLACE
from organization.tests.factories import (
    OrganizationFactory, ProductFactory, OrganizationTypeFactory,
    OrganizationMembershipFactory
)
from organization.tests.factories import UserFactory

faker = faker.Factory.create()
USERNAME = 'tester01'
PASSWORD = 'Prod!'


class AuthAPIMixin(object):
    def get_api_token(self, username=USERNAME, password=PASSWORD):
        """
        Login and get Authorization token in return
        :param username: default=tester01
        :param password: default=Prod!
        :return Authorization token as string:
        """
        response = self.client.post(
            reverse('api-token-auth'), {'username': username, 'password': password}, follow=True
        )

        response = response.client._handle_redirects(response)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('token' in response.data, msg='The response must a \'token\' field')
        self.assertNotEqual(response.data['token'], '', msg='The token field must not be empty')

        return response.data['token']


class UserAPIFactoryMixin(object):
    """
    Mixin to allow creation of users

    """

    @staticmethod
    def create_user(username=USERNAME, password=PASSWORD, **kwargs):
        """
        Create a user as identified by username, email, password and is_staff.
        """
        return UserFactory.create(
            username=username, password=password, **kwargs
        )


class AssessmentAPIFactoryMixin(object):
    """
    Mixin to create assessments

    """

    @staticmethod
    def create_assessments(organization, user, assessment_type, product=None, size=1):
        return AssessmentFactory.create_batch(
            size=size, organization=organization, created_by=user, assessment_type=assessment_type,
            products=(product,), last_modified_by=user
        )

    @staticmethod
    def create_assessment(organization, user, assessment_type, product=None):
        return AssessmentFactory.create(
            organization=organization, created_by=user, assessment_type=assessment_type, products=(product,),
            last_modified_by=user
        )

    @staticmethod
    def create_assessment_document_link(user, assessment, attachment, document_type):
        return AssessmentDocumentLinkFactory.create(
            author=user, assessment=assessment, attachment=attachment, document_type_attachment=document_type
        )

    @staticmethod
    def create_assessment_with_workflow(organization, user, auditor, membership, assessment_type, product=None):

        auditor.groups.add(Group.objects.get(name=INTERNAL_INSPECTOR))
        auditor_organization = OrganizationFactory.create(users=(auditor,))
        auditor_primary_organization = OrganizationFactory.create()
        auditor_primary_organization_type = OrganizationTypeFactory.create(name=INTERNAL_INSPECTOR)
        OrganizationMembershipFactory.create(
            primary_organization=auditor_primary_organization,
            secondary_organization=auditor_organization,
            organization_type=auditor_primary_organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )

        assessment = AssessmentFactory.create(
            organization=organization, created_by=user, assessment_type=assessment_type, products=(product,),
            last_modified_by=user
        )
        assessment_workflow = AssessmentWorkflow.objects.initialize_workflow(
            user, assessment_type, 2015, membership, AGRIPLACE
        )
        assessment_workflow.assign_assessment(assessment)
        assessment_workflow.auditor_user = auditor
        assessment_workflow.auditor_organization = auditor.organizations.first()

        return assessment


class GroupFactoryMixin(object):
    """
    Mixin to create farm groups
    """

    @staticmethod
    def create_farm_groups():
        GroupFactory.create(name=GROWER)
        GroupFactory.create(name=INTERNAL_INSPECTOR)
        GroupFactory.create(name=INTERNAL_AUDITOR)


class AssessmentWorkflowMixin(UserAPIFactoryMixin, GroupFactoryMixin, AssessmentAPIFactoryMixin):
    """
    Mixin to create assessment workflows
    """

    def create_workflow(self, username, password, code):
        self.create_farm_groups()
        user = self.create_user(username=username, password=password)
        user.groups.add(Group.objects.get(name=GROWER))
        primary_organization = OrganizationFactory.create()
        organization = OrganizationFactory.create(users=(user,))
        organization_type = OrganizationTypeFactory.create()
        membership = OrganizationMembershipFactory.create(
            primary_organization=primary_organization,
            secondary_organization=organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        auditor = self.create_user(
            username=faker.user_name(), password=faker.word()
        )
        auditor.groups.add(Group.objects.get(name=INTERNAL_INSPECTOR))
        auditor_organization = OrganizationFactory.create(users=(auditor,))
        auditor_primary_organization = OrganizationFactory.create()
        auditor_primary_organization_type = OrganizationTypeFactory.create(name=INTERNAL_INSPECTOR)
        auditor_membership = OrganizationMembershipFactory.create(
            primary_organization=auditor_primary_organization,
            secondary_organization=auditor_organization,
            organization_type=auditor_primary_organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )
        assessment_type = AssessmentTypeFactory.create(code=code)
        product = ProductFactory.create(organization=organization)
        assessment = self.create_assessment(
            organization, user, assessment_type, product
        )

        assessment_workflow = AssessmentWorkflow.objects.initialize_workflow(
            user, assessment_type, 2015, membership, AGRIPLACE
        )
        assessment_workflow.assign_assessment(assessment)
        assessment_workflow.auditor_user = auditor
        assessment_workflow.auditor_organization = auditor.organizations.first()

        return {
            'user': user, 'organization': organization, 'auditor': auditor,
            'auditor_organization': auditor_organization,
            'assessment_type': assessment_type, 'product': product, 'assessment': assessment,
            'assessment_workflow': assessment_workflow,
            'membership': membership, 'auditor_membership': auditor_membership
        }


class APITestSetupMixin(AuthAPIMixin, AssessmentWorkflowMixin, AttachmentAPIFactoryMixin):
    pass
