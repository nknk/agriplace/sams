from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404
from rest_framework import response
from rest_framework import schemas
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import CoreJSONRenderer
from rest_framework.reverse import reverse
from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework import status
from rest_framework.exceptions import (
    APIException,
    ParseError,
    AuthenticationFailed,
    NotAuthenticated,
    MethodNotAllowed,
    UnsupportedMediaType,
    Throttled
)
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework.compat import coreapi
from rest_framework.request import clone_request
from rest_framework.views import APIView

from organization.api.internal.serializers import OrganizationMembershipMobileSerializer
from organization.models import OrganizationMembership
from sams.utils import get_user_default_membership, get_layout, get_user_memberships


class SchemaGenerator(schemas.SchemaGenerator):

    def __init__(self, title=None, url=None, patterns=None, urlconf=None):
        super(SchemaGenerator, self).__init__(title, url, patterns, urlconf)

    def get_schema(self, request=None):
        if self.endpoints is None:
            self.endpoints = self.get_api_endpoints(self.patterns)

        links = []
        for path, method, category, action, callback in self.endpoints:
            view = callback.cls()
            for attr, val in getattr(callback, 'initkwargs', {}).items():
                setattr(view, attr, val)
            view.args = ()
            view.kwargs = {}
            view.format_kwarg = None

            actions = getattr(callback, 'actions', None)
            if actions is not None:
                if method == 'OPTIONS':
                    view.action = 'metadata'
                else:
                    view.action = actions.get(method.lower())

            if request is not None:
                view.request = clone_request(request, method)
                try:
                    view.check_permissions(view.request)
                except Exception:
                    pass
            else:
                view.request = None

            link = self.get_link(path, method, callback, view)
            links.append((category, action, link))

        if not links:
            return None

        # Generate the schema content structure, eg:
        # {'users': {'list': Link()}}
        content = {}
        for category, action, link in links:
            if category is None:
                content[action] = link
            elif category in content:
                content[category][action] = link
            else:
                content[category] = {action: link}

        # Return the schema document.
        return coreapi.Document(title=self.title, content=content, url=self.url)


@api_view()
@staff_member_required
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer, CoreJSONRenderer])
def schema_view(request):
    """
    Core API schema for Agriplace API
    :param request:
    :return:
    """
    generator = SchemaGenerator(title='AGRIPLACE API')
    return response.Response(generator.get_schema(request=request))


@api_view(['GET'])
def internal_api_root(request, format=None):  # TODO provide list of all root endpoints
    """
    Root for Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'assessments-app': reverse('assessments-app-api-list', request=request, format=format),
        'app-settings-app': reverse('app-settings-app-api-list', request=request, format=format),
        'organization-app': reverse('organization-app-api-list', request=request, format=format),
        'attachments-app': reverse('attachments-app-api-list', request=request, format=format),
        'rbac-app': reverse('rbac-app-api-list', request=request, format=format),
        'form_builder-app-api-list': reverse('form_builder-app-api-list', request=request, format=format)
    })


def agriplace_api_exception_handler(exc):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data['status_code'] = response.status_code

    else:
        data = dict()
        data['errors'] = []
        data['errors'].append(exc.message)
        http_status_code = status.HTTP_200_OK
        if isinstance(exc, APIException):
            http_status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        if isinstance(exc, ParseError) \
                or isinstance(exc, AuthenticationFailed) \
                or isinstance(exc, NotAuthenticated) \
                or isinstance(exc, MethodNotAllowed) \
                or isinstance(exc, UnsupportedMediaType) \
                or isinstance(exc, Throttled):
            http_status_code = status.HTTP_400_BAD_REQUEST
        response = Response(
            data,
            status=http_status_code,
            template_name=None,
            headers=None,
            content_type=None
        )
    return response


class PlatformContextView(APIView):
    permission_classes = ()

    def get(self, request):
        default_organization = None
        if request.user.is_authenticated():
            default_membership = get_user_default_membership(request.user)
            if default_membership:
                default_organization = default_membership.primary_organization
            else:
                default_organization = request.user.organizations.first()
        layout = get_layout(default_organization)
        return Response({
            'platform_context': layout
        })


class UserInfoView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        memberships = get_user_memberships(request.user)
        data = OrganizationMembershipMobileSerializer(memberships, many=True).data
        default_membership = get_user_default_membership(request.user)
        return Response({
            'memberships': data,
            "current_membership_uuid": default_membership.uuid
        })

user_info = UserInfoView.as_view()
platform_context = PlatformContextView.as_view()
