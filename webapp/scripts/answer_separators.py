
# We want to achieve, for all answers:
#   - text: value = JSON(text)
#   - buttons, checkboxes, radio: value = JSON(array of PA in PA-set uuids)

import itertools
import json
import os
import sys
from termcolor import colored

import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

from assessments.models import Answer, PossibleAnswer

#
# Answer type: checkboxes
#


class Indent:

    indent_level = 0

    def indent(self):
        for _ in itertools.repeat(None, self.indent_level):
            print ' ',

    def log(self, *args, **kwargs):
        self.indent()
        for arg in args:
            print arg,
        print

    def right(self):
        self.indent_level += 1

    def left(self):
        self.indent_level -= 1


def check_pa_by_values(values, field, answer):
    """
    Given an answer and a list of possible answer field values (uuid or value)
    generate a list of correct uuids from the question's set
    """
    answer_values = []  # uuids

    i = Indent()
    i.right()

    for value in values:
        # i.log("value=<{%s}>" % value.('ascii',  'ignore'))
        i.right()

        # ignore blank segments
        if value == '':
            i.log('Blank, ignoring.')
            i.left()
            continue

        #   - get this pa by <field> from PA set
        try:
            pa = answer.question.possible_answer_set.possible_answers.get(**{field: value})
            answer_values.append(pa.uuid)
            i.log("Possible answer %s found in question's set." % pa.uuid)
        except PossibleAnswer.DoesNotExist:
            # this pa is not part of the question's set
            i.log("Possible answer <%s> NOT FOUND in question's set." % value)
            try:
                # find the pa
                pa = PossibleAnswer.objects.get(**{field: value})
                i.log("Possible answer %s found outside question." % pa.__dict__[field])
                # match this pa against the set by value
                try:
                    matching_pa = answer.question.possible_answer_set.possible_answers.get(value=pa.value)
                    i.log("Match %s found in question's set by value." % matching_pa.uuid)
                    answer_values.append(matching_pa.uuid)
                except PossibleAnswer.DoesNotExist:
                    i.log("!!!!!! No matching possible answer found in question's set by value %s" % pa.value)
            except PossibleAnswer.DoesNotExist:
                i.log("No possible answer found anywhere with {field} {value}".format(field=field, value=value))
        i.left()
    i.left()
    return answer_values


# (1: yes) [question's set: (2: yes) (3: no))
#
#  answer: [4] -> [2]


def checkboxes_no_caret():
    """
    Checks case: answers with no ^
    Returns a dict like {answerid: new_value}


    Case: no ^
    current: comma separated PA uuids
    do:
      - check that each uuid in PA set
      - if uuid not in PA set, get PA and match to PA-set by value
      - replace uuid
      - append PA
      - value = JSON(array of PA in PA-set uuids)
    """

    # value like ',234242344,23r232e2f2,23423dd,23d2d1232'

    new_answers = {}

    answers = Answer.objects.\
        filter(question__answer_type='checkboxes')\
        .filter(value__startswith=',') \
        .exclude(value__contains='^')\
        .all()

    i = Indent()

    for answer in answers:
        # print
        i.indent()
        # print("answer=<{answer}>".format(answer=answer))
        i.right()
        # Ignore answers without a value, nothing to do here
        if not answer.value:
            i.log("Answer has no value. Skipping.")
            i.left()
            continue
        # We know: all answers start with a , and are comma separated
        #   - check that each uuid in PA set
        value_segments = answer.value.split(',')
        answer_values = check_pa_by_values(value_segments, 'uuid', answer)
        new_answers[answer.uuid] = answer_values
        i.left()
    return new_answers



def checkboxes_with_caret():
    """
    Case: starts with , contains ^
    current: ,uuid,uuid^value, oh value^value
    do
      - split on ,
      - split each value on ^
      - for every resulting value
          - check whether it's uuid of PA
              - yes: check PA in PA-set
                    - yes: append PA.
                    - no: it's uuid of PA outside set, find PA and match against PA in set by value. append PA.
              - no: check whether it's value of PA in set
                  - yes: append PA
                  - no: dump
      - value = JSON(array of PA in PA-set uuids)
    """

    new_answers = {}
    answers = Answer.objects \
        .filter(question__answer_type='checkboxes') \
        .filter(value__startswith=',') \
        .filter(value__contains='^') \
        .all()

    i = Indent()

    for answer in answers:
        print
        print
        i.indent()
        # print("answer=<{answer}>".format(answer=answer))
        # print '--------------------------------------------------------------------'
        i.right()
        # Ignore answers without a value, nothing to do here
        if not answer.value:
            i.log("Answer has no value. Skipping.")
            i.left()
            continue
        # We know: all answers start with a , and are comma separated
        #   - check that each uuid in PA set
        pa_uuids = []

        # value is like: ",uuid,uuid^value, oh value^value"
        # We want to chop answer in half: first part is comma separated uuids, after first caret it's caret separated values (with COMMAS!)

        caret_segments = answer.value.split('^')
        # first segment is special, it should be comma separated, chop it off the beginning
        comma_separated_value, caret_segments = caret_segments[0], caret_segments[1:]
        comma_segments = comma_separated_value.split(',')

        i.log("COMMA SEGMENTS", comma_segments)
        i.log("CARET SEGMENTS", caret_segments)

        # print
        i.log('comma segments')
        i.log('==============')
        answer_values_comma = check_pa_by_values(comma_segments, 'uuid', answer)
        i.log('ANSWER VALUES COMMA', answer_values_comma)

        # print
        i.log('caret segments')
        i.log('==============')
        answer_values_caret = check_pa_by_values(caret_segments, 'value', answer)
        i.log('ANSWER VALUES CARET', answer_values_caret)


        answer_values = answer_values_comma + answer_values_caret
        # no duplicates
        answer_values = list(set(answer_values))
        # print
        i.log('result')
        i.log('======')

        new_answers[answer.uuid] = answer_values
        i.log(">>> value=", answer_values)
        i.left()
    return new_answers


def checkboxes_only_caret():
    # Case: starts with ^
    # current: ^value^value
    # do:
    #   - split on ^
    #   - for every resulting value:
    #       - check whether it's value of PA in PA-set
    #           - yes: append PA
    #           - no: dump
    #   - value = JSON(array of PA in PA-set uuids)

    new_answers = {}
    answers = Answer.objects. \
        filter(question__answer_type='checkboxes') \
        .filter(value__startswith='^') \
        .all()

    i = Indent()

    for answer in answers:
        print
        i.indent()
        print("answer=<{answer}>".format(answer=answer))
        i.right()
        # Ignore answers without a value, nothing to do here
        if not answer.value:
            i.log("Answer has no value. Skipping.")
            i.left()
            continue
        # We know: all answers start with a ^ and are ^ separated and they consist of pa values (not uuids)
        value_segments = answer.value.split('^')
        answer_values = check_pa_by_values(value_segments, 'value', answer)
        new_answers[answer.uuid] = answer_values
        i.log(">>> value=", answer_values)
        i.left()
    return new_answers


def radios():
    # Answer type: radio
    # current: value of PA
    # do:
    #   - find PA by value in set
    #   - value = JSON(PA uuid)

    new_answers = {}
    answers = Answer.objects. \
        filter(question__answer_type='radio') \
        .all()

    i = Indent()

    for answer in answers:
        print
        i.indent()
        print("answer=<{answer}>".format(answer=answer))
        i.right()
        # Ignore answers without a value, nothing to do here
        if not answer.value:
            i.log("Answer has no value. Skipping.")
            i.left()
            continue
        # We know: all answers are PA values, no separators
        answer_values = check_pa_by_values([answer.value], 'value', answer)
        new_value = json.dumps(answer_values[0])
        new_answers[answer.uuid] = new_value
        i.log("*** new value=", new_value)
        i.left()
    return new_answers


# Answer type: text
# current: value of text
# do:
#   - value = JSON(text)

def texts():
    answers = Answer.objects.filter(question__answer_type='text').all()
    new_answers = {}

    i = Indent()

    for answer in answers:
        print
        i.indent()
        print("answer=<{answer}>".format(answer=answer))
        i.right()
        # Ignore answers without a value, nothing to do here
        if not answer.value:
            i.log("Answer has no value. Skipping.")
            i.left()
            continue
        # We know: all answers are raw values
        new_value = json.dumps(answer.value)
        new_answers[answer.uuid] = new_value
        i.log("*** new value:", new_value)
        i.left()
    return new_answers


# result_checkboxes_only_caret = checkboxes_only_caret()

# Format 1: checkboxes with comma and caret
# Format 2: checkboxes with caret
# Format 3: checkboxes with only caret

# We parse format 1 and format 2
# and convert them to format 3

print "==="

new_answers = {}
result_checkboxes_with_caret = checkboxes_with_caret()
result_checkboxes_no_caret = checkboxes_no_caret()

new_answers.update(result_checkboxes_with_caret)
new_answers.update(result_checkboxes_no_caret)

num_saved = 0

for (answer_uuid, possible_answer_uuids) in new_answers.items():
    try:
        answer = Answer.objects.get(uuid=answer_uuid)
        print "\nLooking at:", answer, possible_answer_uuids, type(possible_answer_uuids)

        possible_answer_values = []
        for possible_answer_uuid in possible_answer_uuids:
            # Retrieve possible answers one by one to be able to tell when we can't find one
            possible_answer = PossibleAnswer.objects.get(uuid=possible_answer_uuid)
            possible_answer_values.append(possible_answer.value)

        possible_answer_values.sort()
        # Format 3: checkboxes with only caret
        # using this format for all answers
        new_value = '^'.join(possible_answer_values)
    except Answer.DoesNotExist:
        print '[ERROR] Answer {} does not exist.', format(answer_uuid)
    except PossibleAnswer.DoesNotExist:
        print '[ERROR] Possible answer {} does not exist.'.format(possible_answer_uuid)

    print '[OK] answer:', answer.uuid
#    print 'old value:', answer.value
    print 'computed PA uuids:', possible_answer_uuids, type(possible_answer_uuids)
#    print 'new value:', new_value
    print

    answer.value_old = answer.value
    answer.value_uuids = ','.join(possible_answer_uuids)
    answer.value = new_value
    answer.save()

    num_saved += 1 

print "len(result_checkboxes_with_caret) = ", len(result_checkboxes_with_caret)
print "len(result_checkboxes_no_caret) = ", len(result_checkboxes_no_caret)

print "num_saved = ", num_saved

