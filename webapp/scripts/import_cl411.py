"""
CL411 product list importer
File format:
"StandardCode","ProductCode","NameNL","DateFrom","DateTo","Infrastracture"
"""
import unicodecsv
import os
import sys

import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

from django.db import transaction
from organization.models import StandardProductType

@transaction.atomic
def import_csv(filename):
    result = {
        "errors": [],
        "success": False
    }

    # Open file
    print("Opening file...")
    try:
        csvfile = open(filename, "rU")
    except StandardError as error:
        error_text = "Could not open file %s. Error: %s" % (filename, error)
        print(error_text)
        result["errors"].append(error_text)
        return result
    reader = unicodecsv.DictReader(csvfile)
    print("...done.")

    EXPECTED_COLUMNS = [
        "StandardCode",
        "ProductCode",
        "NameNL",
        "DateFrom",
        "DateTo",
        "Infrastructure"
    ]

    print "* Checking CSV columns..."
    is_ok = True
    print(reader.fieldnames)
    for column_name in EXPECTED_COLUMNS:
        if column_name not in reader.fieldnames:
            print("*** ERROR: csv missing <{column_name}> column!".format(column_name=column_name))
            is_ok = False
    if not is_ok:
        exit(1)

    for row_id, row in enumerate(reader):
        print('-- Row {row_id}'.format(row_id=row_id))

        updated_value = {
            'standard_code': row['StandardCode'],
            'product_type_code': row['ProductCode'],
            'name_en': row['NameNL'],
            'name_nl': row['NameNL'],
            'begin_date': row['DateFrom'] if not row['DateFrom']=='leeg' else None,
            'end_date': row['DateTo'] if not row['DateTo']=='leeg' else None,
            'landscape_element': row['Infrastructure'] == "J"
            }

        obj, created = StandardProductType.objects.update_or_create(
            standard_code=row['StandardCode'], product_type_code=row['ProductCode'], defaults=updated_value)

    result["success"] = True
    return result


# ###############################################################################

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

if __name__ == '__main__':
    if sys.argv == 1:
        print "Specify file name"
        exit(1)
    else:
        filename = sys.argv[1]
        print('Importing {}'.format(filename))
        import_csv(sys.argv[1])