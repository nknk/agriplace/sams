import os
import sys
import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()


from django.db import transaction
from assessments.models import AssessmentType, AssessmentTypeSection, PLATFORM_CHOICES


@transaction.atomic
def migrate_assessment_type_sections():

    platforms = PLATFORM_CHOICES

    for platform in platforms:
        assessment_types = AssessmentType.objects.filter(kind='assessment')
        for assessment_type in assessment_types:
            assessment_type_section_instance, created = AssessmentTypeSection.objects.get_or_create(
                platform=platform[0],
                assessment_type=assessment_type
            )
            if created:
                if platform[0] == 'hzpc':
                    assessment_type_sections_list = "overview,products,questionnaires,review"
                else:
                    assessment_type_sections_list = assessment_type.assessment_sections
                    sections_list = assessment_type_sections_list.split(',')
                    if 'products' not in sections_list:
                        assessment_type_sections_list = ",".join(sections_list[:1] + ['products'] + sections_list[2:])
                assessment_type_section_instance.sections_list = assessment_type_sections_list
                assessment_type_section_instance.save()


if __name__ == '__main__':
    migrate_assessment_type_sections()
