#!/usr/bin/env python
"""
Updates AttachmentLinks for FileAttachments. If AttachmentLink contains link for blank(1b) file
searches for other FileAttachments with same original_file_name and suggest variants
"""
import os
import sys

import django
from django.conf import settings

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

project_media = settings.MEDIA_ROOT + '/'

from assessments.models import AssessmentDocumentLink
from attachments.models import Attachment, FileAttachment


# helpers

def ask_attachment_number(max_number):
    try:
        selected = int(raw_input("What do you want to choose(default 0, for skip editing): "))
        if selected > max_number:
            selected = 0
    except ValueError:
        selected = 0
    return selected


def print_attachment_info(attachment):
    absolute_path_to_file = project_media + attachment.file.name
    if not os.path.exists(absolute_path_to_file):
        file_size = 'not_exists'
    else:
        file_size = attachment.file.size
    return u"%s | %s | %s | %s | %s" % (
        attachment.uuid,
        attachment.title[0:30],
        file_size,
        attachment.for_deletion_mark,
        [(assessment['assessment_uuid'], assessment['is_closed']) for assessment in attachment.used_in_assessments()]
    )

# first global step - we are walk thru all AttachmentLinks
all_attachments = FileAttachment.objects.all()
report = {
    'skipped': [],
    'not_found': []
}
print("\nFirst let's check all AttachmentsLinks")
print("======================================")

for documentLink in AssessmentDocumentLink.objects.all():
    attachment = Attachment.objects.get_subclass(pk=documentLink.attachment.uuid)
    if attachment.get_attachment_type() == 'FileAttachment':
        absolute_path_to_file = project_media + attachment.file.name
        # 1) attachment is blank(1b)
        if not os.path.exists(absolute_path_to_file):
            print "!!! Bad news this file not exists: ", absolute_path_to_file, "in attachment: ", attachment.uuid
        else:
            if attachment.file.size == 1:
                same_attachments = all_attachments.filter(
                    original_file_name=attachment.original_file_name,
                    organization=attachment.organization
                ).exclude(uuid=attachment.uuid)
                print(u'\nBlank Attachment founded: %s' % print_attachment_info(attachment))
                print(u'Organization: %s(%s)' % (documentLink.assessment.organization, documentLink.assessment.organization.uuid))
                print(u">>> Same attachments:")
                # 2) we have something same and have suggestion for update AttachmentLink
                if same_attachments.count() > 0:
                    for counter, item in enumerate(same_attachments):
                        print counter+1, "::", print_attachment_info(item)

                    print("Recommendation:")
                    best_choice = same_attachments.filter(for_deletion_mark=False)  # .exclude(file__size=0)
                    bad_choice = same_attachments.filter(for_deletion_mark=True)  # .exclude(file__size=0)
                    if best_choice.count() > 1:
                        print "Any of: ", [attachment.uuid for attachment in best_choice]
                    elif best_choice.count() == 1:
                        print "No choice: ", best_choice[0].uuid, " - we don't want to have duplicates at all"
                    else:
                        if bad_choice.count() > 0:
                            print "Any of: ", [attachment.uuid for attachment in best_choice]
                        else:
                            print('42!!!')

                    attachment_number = ask_attachment_number(same_attachments.count())
                    if attachment_number > 0:
                        # 0) current attachment not visible
                        attachment.for_deletion_mark = True
                        attachment.save()
                        # 1) take selected file attachment
                        new_attachment = same_attachments[attachment_number-1]
                        # 2) set for_deletion_mark = False
                        new_attachment.for_deletion_mark = False
                        new_attachment.save()
                        # 3) update AttachmentLink documentLink.attachment = same_attachments[selected]
                        documentLink.attachment = new_attachment
                        documentLink.save()
                        print(u"You choice: %s" % print_attachment_info(new_attachment))
                    else:
                        # save information about AttachmentLink and Attachment in report, in skipped part
                        report['skipped'].append(attachment)
                # 3) Looks like we are in troubles, nothing similar
                else:
                    report['not_found'].append(attachment)

print("Results: ")
print("Skipped", len(report['skipped']), "\n", report['skipped'], "\n")
print("Not found", len(report['Not found']), "\n", report['not_found'], "\n")

# second global step - we are walk thru all blank files in archive (size = 1b, for_deletion_mark = False)

