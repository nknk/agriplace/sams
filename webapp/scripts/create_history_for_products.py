#!/usr/bin/env python
import os
import sys

import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

import datetime
from organization.models import Product

for item in Product.objects.all():
    item.harvest_year=datetime.datetime.today().year
    item.save()
