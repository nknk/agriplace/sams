#!/usr/bin/env python
"""
Converts possible answers to possible answer sets.
"""
import os
import sys

import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

from django.contrib.auth.models import User
from assessments.models import Question, PossibleAnswerSet, PossibleAnswer

questions = Question.objects.all()

# def get_clean_possible_answer_dict(possible_answer):
#     pa_dict = possible_answer.__dict__
#     # clean all underscores
#     for key in pa_dict:
#         if key[0] == '_':
#             pa_dict.pop(key)
#     pa_dict.pop('question_id')
#     pa_dict.pop('uuid')
#     pa_dict.pop('background_color')
#     pa_dict.pop('created_time')
#     pa_dict.pop('modified_time')
#     pa_dict.pop('code')
#     pa_dict.pop('order_index')
#     return pa_dict

def possible_answers_eq(pa1, pa2):
    keys_to_compare = (
        'requires_justification',
        'text_en',
        'text_nl',
        'value'
    )
    same = True
    for key in keys_to_compare:
        if pa1.__dict__[key] != pa2.__dict__[key]:
            same = False
            break
    return same


for question in questions:
    # check whether we already have a set with these exact possible answers
    # if we do, connect it
    # if we do not, create one from these possible answers

    # do this here to make sure it's up to date every time
    if question.questionnaire:
        possible_answer_sets = question.questionnaire.assessment_type.possible_answer_sets.all()

        question_possible_answers = question.possible_answers.all()

        # try to find ANY matching possible answer set for this question
        found_matching_possible_answer_set = False
        for possible_answer_set in possible_answer_sets:
            # check whether our question matches this PA set:

            # 1. make sure ALL possible answers in question have a match in set

            # for every question PA, find a matching PA in set
            # if we find one that doesn't match, we can stop.
            all_question_answers_have_match = True
            for question_possible_answer in question_possible_answers:

                # try to find ANY possible answer in set that matches this question pa
                question_answer_has_match_in_set = False
                for set_possible_answer in possible_answer_set.possible_answers.all():
                    if possible_answers_eq(question_possible_answer, set_possible_answer):
                        question_answer_has_match_in_set = True
                        break  # don't check any other set PAs
                if not question_answer_has_match_in_set:
                    # we reached the end and couldn't find a matching answer in set for the current one
                    all_question_answers_have_match = False
                    # so the question doesn't match the set
                    break  # don't check any other question PAs

            # 2. make sure ALL possible answers in set have a match in question

            all_set_answers_have_match = True
            for set_possible_answer in possible_answer_set.possible_answers.all():

                # try to find ANY possible answer in question that matches the set pa
                set_answer_has_match_in_question = False
                for question_possible_answer in question.possible_answers.all():
                    if possible_answers_eq(set_possible_answer, question_possible_answer):
                        set_answer_has_match_in_question = True
                        break
                if not set_answer_has_match_in_question:
                    all_set_answers_have_match = False
                    break

            # we checked both ways. if they are both ok, then the question and set match.
            if all_question_answers_have_match and all_set_answers_have_match:
                # the question matches this set, connect them
                possible_answer_set.questions.add(question)
                print "[connected] Connected <%s> to <%s>" % (question, possible_answer_set)
                found_matching_possible_answer_set = True
                break  # don't check any other sets

    if not found_matching_possible_answer_set:
        # create set from this question's answers
        pas = PossibleAnswerSet(
            name='Auto from ' + question.code,
            assessment_type=question.questionnaire.assessment_type
        )
        pas.save()
        for possible_answer in question.possible_answers.all():
            pas.possible_answers.add(possible_answer)
        question.possible_answer_set = pas
        question.save()
        print "[created] Created possible answer set: %s" % pas
