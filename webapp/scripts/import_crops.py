"""
Product list importer
Imports products from CSV-exported Excel file
File format:
EDI-Crop code;GLOBALG.A.P. Code;English name;Dutch name;Scientific name;Global GAP;FV;CC;Tesco Nurture;EU-Organic;VV AK;VV Aardappelen;Zetmeelaardappelen;VV GZP;VV Suikerbieten;GRASP;AH Protocol;SAI checklist
"""
import unicodecsv
import os
import sys

import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

from django.utils.text import slugify
from django.db import transaction
from organization.models import ProductType, Product


@transaction.atomic
def import_csv(filename):
    result = {
        "errors": [],
        "success": False
    }

    # Open file
    print("Opening file...")
    try:
        csvfile = open(filename, "rU")
    except StandardError as error:
        error_text = "Could not open file %s. Error: %s" % (filename, error)
        print(error_text)
        result["errors"].append(error_text)
        return result
    reader = unicodecsv.DictReader(csvfile)
    print("...done.")

    EXPECTED_COLUMNS = [
        'EDI-Crop code',
        'GLOBALG.A.P. Code',
        'RVO Code',
        'English name',  # name_en
        'Dutch name',  # name_nl
        'Scientific name',  # latin_name
        # permissions
        'Global GAP',
        'FV',
        'CC',
        'Tesco Nurture',
        'EU-Organic',
        'VV AK',
        'VV Aardappelen',
        'Zetmeelaardappelen',
        'VV GZP',
        'VV Suikerbieten',
        'GRASP',
        'AH Protocol',
        'SAI checklist'
    ]

    print "* Checking CSV columns..."
    is_ok = True
    print(reader.fieldnames)
    for column_name in EXPECTED_COLUMNS:
        if column_name not in reader.fieldnames:
            print("*** ERROR: csv missing <{column_name}> column!".format(column_name=column_name))
            is_ok = False
    if not is_ok:
        exit(1)

    TEST_ORGANIZATIONS = [
        'African Development Bank',
        'Agrico',
        'Agrifirm',
        'Agriplace',
        'Akkerbouwbedrijf Green',
        'auditor',
        'dennis',
        'FarmVille',
        'Gerrits Testbedrijf',
        'Ghana Auditor',
        'GLOBALG.A.P.',
        'GLOBALG.A.P. Ghana',
        'Handelaarsmaatschapij',
        "John's Farm",
        'Kracht',
        'Kuapa Kokoo',
        "Laury's Farm",
        "Laury's Farm 2",
        'Leen Klaassen',
        "Oles' Farm",
        'Orex Export',
        'Ruigwerk',
        'RVO',
        'SAWAG',
        'STAM',
        'test',
        'Testakkerbouwbedrijf WUR',
        'Tester2 Organization',
        'test gj',
        'Testorganisatie',
        'Test organization',
        'VAK'
    ]

    product_types_to_import = []
    deleted_product_types = []
    used_in_updates = []

    for row_id, row in enumerate(reader):
        # print('-- Row {row_id}'.format(row_id=row_id))
        # print row

        product_type = {
            'product_edi_crop_code': row['EDI-Crop code'],
            'product_gg_code': row['GLOBALG.A.P. Code'],
            'product_rvo_code': row['RVO Code'].replace(" ", ""),
            'product_name': row['English name'],
            'product_latin_name': row['Scientific name'],
            'permissions': {
                'assessment_types': {
                    'gg': row['Global GAP'],
                    'tn': row['Tesco Nurture'],
                    'eo': row['EU-Organic'],
                    'vv_ak': row['VV AK'],
                    'vv_aa': row['VV Aardappelen'],
                    'ze': row['Zetmeelaardappelen'],
                    'vv_gzp': row['VV GZP'],
                    'vv_suik': row['VV Suikerbieten'],
                    'grasp': row['GRASP'],
                    'ah': row['AH Protocol'],
                    'sai': row['SAI checklist']
                },
                'questionnaires': {
                    'fv': row['FV'],
                    'cc': row['CC']
                }
            },
            "id": row_id
        }
        product_types_to_import.append(product_type)

    existing_product_types = list(ProductType.objects.all())
    for product_type in existing_product_types:
        product_list = Product.objects.filter(product_type=product_type)

        # if product_type "in" product_types_to_import
        update_info = get_update_info(product_type, product_types_to_import)
        if update_info:
            # update
            update_product_type(product_type, update_info)
            used_in_updates.append(update_info['product_name'])
        else:
            # delete
            if not product_list:
                # it never used, so we can delete this product_type
                print(u"* Deleted product type <%s>" % (slugify(product_type.name)))
                deleted_product_types.append(product_type)
                product_type.delete()
            else:
                # checking how it using
                print("We need to delete this product type  <%s>, but it used:" % (u''+product_type.name))
                all_test_flag = True
                for product in product_list:
                    if product.organization.name not in TEST_ORGANIZATIONS:
                        all_test_flag = False
                    assessment_list = product.assessments.all()
                    print("** by %s" % (product.organization.name,))
                    if assessment_list:
                        for assessment in assessment_list:
                            print("**** in %s(%s)" % (assessment.name, assessment.uuid))
                    if all_test_flag:
                        product.assessments.clear()
                        product.delete()
                        deleted_product_types.append(product_type)
                        if product_type.uuid:
                            product_type.delete()


                print("** used only in test organizations %s", all_test_flag)
                # if query_yes_no(" Do you want to delete this product type and all connected objects?"):
                #     # clean up links for all assessments which using this product
                #     pass

    # now time to create new product_types
    created_product_types = []
    for product_type in product_types_to_import:
        if product_type['product_name'] not in used_in_updates:
            create_product_type(product_type)
            created_product_types.append(product_type['product_name'])

    print("> existing_product_types %s", len(existing_product_types))
    print("> %s product_types from file", len(product_types_to_import))
    print("> used in updates %s", len(used_in_updates))
    print("> created product types %s", len(created_product_types))
    print("** %s" % created_product_types)
    print("> deleted product types %s", len(deleted_product_types))


    result["success"] = True
    return result

# ###############################################################################


def get_update_info(product_type, product_types_to_import):
    product_type_name = (u"" + product_type.name).lower()

    for item in product_types_to_import:
        #if (item['product_name_en'].lower() == product_type_name_en) or (item['product_name_nl'].lower() == product_type_name_nl):
        # only nl names
        if item['product_name'].lower() == product_type_name:
            return item
    return ""


def update_product_type(product_type, update_info):
    pt = ProductType.objects.get(pk=product_type.uuid)
    pt.edi_crop_code = update_info['product_edi_crop_code']
    pt.gg_code = update_info['product_gg_code']
    pt.rvo_code = update_info['product_rvo_code']
    pt.name = update_info['product_name']
    # pt.name_nl = update_info['product_name_nl']
    pt.latin_name = update_info['product_latin_name']
    # saving
    pt.save()
    print("* Updated product type <%s>" % pt)


def create_product_type(product_type):
    product_type_obj = ProductType(
        edi_crop_code=product_type['product_edi_crop_code'],
        gg_code=product_type['product_gg_code'],
        rvo_code = product_type['product_rvo_code'],
        name=product_type['product_name'],
        # name_nl=product_type['product_name_nl'],
        latin_name=product_type['product_latin_name']
    )
    # saving
    product_type_obj.save()
    print("* Created product type <%s>" % product_type_obj)


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

if __name__ == '__main__':
    if sys.argv == 1:
        print "Specify file name"
        exit(1)
    else:
        filename = sys.argv[1]
        print('Importing {}'.format(filename))
        import_csv(sys.argv[1])
