#!/usr/bin/env python
"""
Updates AttachmentLinks for FileAttachments. If AttachmentLink contains link for blank(1b) file
searches for other FileAttachments with same original_file_name and suggest variants
"""
import os
import sys
import random

import django
from django.conf import settings

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()

project_media = settings.MEDIA_ROOT + '/'

from organization.models import ProductType


product_type_list = ProductType.objects.all()

r = lambda: random.randint(0, 255)
for item in product_type_list:
    item.field_color = '#%02X%02X%02X' % (r(), r(), r())
    item.save()
