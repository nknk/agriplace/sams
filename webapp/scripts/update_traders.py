#!/usr/bin/env python
import os
import sys
import django

project_root = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(project_root)
os.environ["DJANGO_SETTINGS_MODULE"] = "sams_project.settings"
django.setup()


from organization.models import Organization

buyers_organization_names = [
    "Agrico",
    "AgriFirm",
    "Avebe",
    "Aviko",
    "CZAV",
    "FarmFrites",
    "Goud Biervliet BV",
    "Gourmet BV",
    "Hagranop",
    "Heyboer",
    "Holland Bean",
    "HZPC",
    "Jonika",
    "Laarakker",
    "LambWeston/Meijer",
    "Landjuweel",
    "Marbo",
    "McCain",
    "Nedato",
    "PekaKroef",
    "Pepsico",
    "SuikerUnie",
    "Top",
    "TV De Schakel",
    "Van Rijn",
    "VNK Herbs",
    "Waterman Onions",
    "Wemeldinge",
    "Wiskerke Onions",
    "The Greenery",
    "Van Nature",
    "Best of Four",
    "Den Hartigh"
]

for item in buyers_organization_names:
    organization = Organization.objects.filter(name__iexact=item.lower())
    if organization.exists():
        org = Organization.objects.get(uuid=organization[0].uuid)
        org.type = 'buyer'
        org.save()
        print(org.name, " updated to ", org.type)
    else:
        org = Organization(name=item, type='buyer')
        org.save()
