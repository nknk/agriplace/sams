import os

from django.conf import settings
from django.db.models.base import ModelBase
from django.utils.datastructures import SortedDict
from django.utils.translation import get_language
import polib
import xlrd
from xlwt import Workbook

from translations.fields import TranslatableTextField


class Translation(object):
    """ Translation object class
    """
    def __init__(self, language_code, *args, **kwargs):
        self.language_code = language_code
        self.filename = settings.LANGUAGE_FILES.format(language_code)
        self.file = self.filename
        make_sure_translation_exists(self.file)

    def save(self, *args, **kwargs):
        """ Save the translation file to disk
        """
        if not isinstance(self.file, basestring):
            with open(self.filename, 'w') as f:
                f.write(self.file.read())
                self.file = f.name
            TranslatableTextField.refresh_cache(self.language_code)
        return self


class TranslationMeta(ModelBase):
    """ Support translate definition in model Meta classes for defining translatable content fields
    """
    def __new__(cls, name, bases, attrs):
        fields = None
        attrs = SortedDict(attrs)
        if 'Meta' in attrs and hasattr(attrs['Meta'], 'translate'):
            fields = attrs['Meta'].translate
            delattr(attrs['Meta'], 'translate')

        new_class = super(TranslationMeta, cls).__new__(cls, name, bases, attrs)
        if hasattr(new_class, '_meta'):
            new_class._meta.translatable_fields = fields
        return new_class


def make_sure_translation_exists(filename):
    """ Write basic .po files for each enabled language to disk with correct headers
    """
    if not os.path.exists(filename):
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        pofile = polib.POFile()
        pofile.metadata = {
            'MIME-Version': '1.0',
            'Content-Type': 'text/plain; charset=utf-8',
            'Content-Transfer-Encoding': '8bit',
        }
        pofile.save(filename)


_pofiles = {}


def get_pofile(language_code):
    """ Get the PO file from disk

    :param language_code: the language code to get the PO file for
    """
    global _pofiles
    if language_code not in _pofiles:
        filename = settings.LANGUAGE_FILES.format(language_code)
        make_sure_translation_exists(filename)
        pofile = polib.pofile(filename)
        pofile.filename = filename
        pofile.changed = False
        _pofiles[language_code] = pofile
    return _pofiles[language_code]


def _add_strings_to_translation_files(instance, fields, occurrence_prefix):
    """ Write translatable string to all enabled language PO files
    """
    for language_code, language_name in settings.LANGUAGES[1:]:
        pofile = get_pofile(language_code)
        for field in fields:
            msgid = getattr(instance, field)
            if not msgid:
                # Don't create empty entries
                continue
            # Strip \r\n stuff
            if msgid.endswith(u'\r\n'):
                msgid = msgid[:-2]
            elif msgid.endswith(u'\n'):
                msgid = msgid[:-1]
            entry = pofile.find(msgid)
            occurrence = u'{}:{}'.format(occurrence_prefix, field)
            if entry is None:
                pofile.append(
                    polib.POEntry(
                        msgid=msgid,
                        tcomment=occurrence,
                    )
                )
                pofile.changed = True
            else:
                if occurrence not in entry.tcomment:
                    entry.tcomment += u'\n{}'.format(occurrence)
                    pofile.changed = True


def flush_pofiles():
    """ Re-compile the .mo files and refresh Translation class cache
    """
    for language_code, language_name in settings.LANGUAGES:
        pofile = get_pofile(language_code)
        if pofile.changed:
            pofile.save(pofile.filename)
            filename = settings.COMPILED_LANGUAGE_FILES.format(language_code)
            if not os.path.exists(os.path.dirname(filename)):
                os.makedirs(os.path.dirname(filename))
            pofile.save_as_mofile(filename)
            TranslatableTextField.refresh_cache(language_code)


def update_translation(language_code, msgid, msgstr):
    """ Function to add translation to PO file, if it doesn't exist yet.
    Doesn't overwrite existing translations.
    """
    msgid = msgid.strip()
    msgstr = msgstr.strip()
    if not msgid:
        return
    # Fix potential \r\n or \n problems
    if msgstr:
        if msgid.endswith(u'\r\n'):
            if not msgstr.endswith(u'\r\n'):
                msgstr = msgstr + u'\r\n'
        elif msgid.endswith(u'\n'):
            if not msgstr.endswith(u'\n'):
                msgstr = msgstr + u'\n'
        if msgstr.endswith(u'\r\n'):
            if not msgid.endswith(u'\r\n'):
                msgstr = msgstr[:-2]
        elif msgstr.endswith(u'\n'):
            if not msgid.endswith(u'\n'):
                msgstr = msgstr[:-1]
    pofile = get_pofile(language_code)
    entry = pofile.find(msgid)
    if entry is None:
        # msgid didn't exist yet, create new entry
        pofile.append(
            polib.POEntry(
                msgid=msgid,
                msgstr=msgstr,
            )
        )
        pofile.changed = True
    else:
        # msgid exists, update if applicable
        if not entry.msgstr:
            entry.msgstr = msgstr
            pofile.changed = True


def tolerant_ugettext(obj, given_lang=None):
    """
    Helper function that translates strings in a tolerant way regarding \r\n and \n differences
    """
    if not obj:
        return obj
    obj = obj.strip()
    language = given_lang or get_language()
    if language and language != 'en':
        if language not in TranslatableTextField._cache:
            TranslatableTextField.refresh_cache(language)
        translated_obj = TranslatableTextField._cache[language].ugettext(obj)
        if obj == translated_obj:
            if obj.endswith('\r\n'):
                obj = obj[:-2]
            elif obj.endswith('\n'):
                obj = obj[:-1]
            translated_obj = TranslatableTextField._cache[language].ugettext(obj)
    else:
        translated_obj = obj
    return translated_obj


def tolerant_ugettext_with_en(obj):
    """
    Helper function that translates strings in a tolerant way regarding \r\n and \n differences
    """
    if not obj:
        return obj
    obj = obj.strip()
    language = get_language()
    if language not in TranslatableTextField._cache:
        TranslatableTextField.refresh_cache(language)
    translated_obj = TranslatableTextField._cache[language].ugettext(obj)
    if obj == translated_obj:
        if obj.endswith('\r\n'):
            obj = obj[:-2]
        elif obj.endswith('\n'):
            obj = obj[:-1]
        translated_obj = TranslatableTextField._cache[language].ugettext(obj)
    return translated_obj



def _add_translation_for_language(language_code, translation_file):
    upload_success = True

    report_book = Workbook()
    report_sheet = report_book.add_sheet('Sheet 1')

    try:
        file_content = translation_file.read()
        pofile = get_pofile(language_code)
        translation_book = xlrd.open_workbook(file_contents=file_content)
        translation_sheet = translation_book.sheet_by_index(0)

        headers = [str(cell.value) for cell in translation_sheet.row(0)]
        report_header_row = report_sheet.row(0)
        for col in xrange(0, len(headers), 2):
            start = (col/2)*3
            report_header_row.write(start, headers[col])
            report_header_row.write(start+1, headers[col+1])
            report_header_row.write(start+2, "Result")

        for row in range(translation_sheet.nrows)[1:]:

            report_row = report_sheet.row(row)

            for col in xrange(0, translation_sheet.ncols, 2):
                base_lang_text = translation_sheet.cell(row, col).value
                trans_lang_text = translation_sheet.cell(row, col+1).value

                if base_lang_text.endswith(u'\r\n'):
                    base_lang_text = base_lang_text[:-2]
                elif base_lang_text.endswith(u'\n'):
                    base_lang_text = base_lang_text[:-1]

                if base_lang_text:
                    msgid = base_lang_text
                    msgstr = trans_lang_text.strip()
                    entry = pofile.find(msgid)

                    iteration = (col/2)*3
                    report_row.write(iteration, base_lang_text)
                    report_row.write(iteration+1, trans_lang_text)

                    if entry:
                        entry.msgstr = msgstr
                        pofile.changed = True
                        report_row.write(iteration+2, "success")
                    else:
                        report_row.write(iteration+2, "base language entry not found")
                        upload_success = False

        flush_pofiles()
        report_file = open('translation-report.xls', 'w+b')
        report_book.save(report_file)

    except Exception as e:
        upload_success = False
        report_file = open('error.txt', 'w+b')
        report_file.write(e.message)

    return upload_success, report_file


def _get_language_pofile(language_code):
    return get_pofile(language_code)


def _check_identifier_existance(msgid, pofile):
    entry = pofile.find(msgid)
    if entry:
        if entry.msgstr:
            return True
        else:
            return False
    return True
