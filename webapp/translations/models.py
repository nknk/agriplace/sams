import os

from django.conf import global_settings
from django.contrib.auth.models import Group
from django.db import models
from django.db.models.signals import post_save
from django.utils.translation import ugettext_lazy as _

from core.models import UuidModel
from app_settings.models import TextBlock, HelpTextBlock
from assessments.models import (
    QuestionTrigger,
    QuestionLevel,
    PossibleAnswer,
    Questionnaire,
    AssessmentType,
    Question,
    QuestionnaireHeading,
    QuestionnaireParagraph,
    QuestionnaireImage,
)
from attachments.models import DocumentType as AttachmentDocumentType
from organization.models import (
    ProductType,
    ProductTypeGroup,
    StandardProductType)
from sams_project.settings import TRANSLATION_SIGNALS
from translations.helpers import (
    _add_strings_to_translation_files,
    flush_pofiles,
)


TRANSLATION_STATUSES = [
    ("pending", _("Pending")),
    ("success", _("Success")),
    ("error", _("Error"))
]


class LanguageTranslation(UuidModel):
    translation_file = models.FileField(_("Translation File"), max_length=255, upload_to='agriplace/translations')
    target_language = models.CharField(_("Language of Translation"), max_length=10,
                                       choices=global_settings.LANGUAGES, default='nl')
    status = models.CharField(_("Translation status"), max_length=50, choices=TRANSLATION_STATUSES, default="pending")
    report_file = models.FileField(_("Translation Report"), max_length=255,
                                   upload_to='agriplace/translations/reports', null=True, blank=True)

    class Meta:
        db_table = 'LanguageTranslation'
        permissions = (
            ('view_languagetranslation', 'Can view language translation'),
        )


def add_strings_to_translation_files(instance, sender, created, **kwargs):
    """ Post save signal that adds translatable strings from the database to the PO file
    The PO file then is available for translation

    :param instance: database object that gets saved
    :param sender: object type
    :param created: boolean if the object is created or updated
    :param kwargs:
    """
    occurrence_prefix = u'Model: {}'.format(
        sender.__name__,
    )
    if sender in [
        AssessmentType
    ]:
        fields = [
            'name',
            'description',
            'sharing_conditions'
        ]
    elif sender in [
        AttachmentDocumentType
    ]:
        fields = [
            'name',
            'description',
        ]
    elif sender in [
        TextBlock
    ]:
        fields = [
            'text',
            'description',
        ]
    elif sender in [
        HelpTextBlock
    ]:
        fields = [
            'title',
            'text',
        ]
    elif sender in [
        QuestionTrigger
    ]:
        fields = [
            'target_justification',
        ]
    elif sender in [
        QuestionLevel
    ]:
        fields = [
            'title',
        ]
    elif sender in [
        PossibleAnswer
    ]:
        fields = [
            'text',
        ]
    elif sender in [
        Questionnaire
    ]:
        fields = [
            'name',
        ]
    elif sender in [
        ProductType
    ]:
        fields = [
            'name',
            'synonyms',
            'help_text',
        ]
    elif sender in [
        Question
    ]:
        fields = [
            'criteria', 'justification_placeholder',
            'text', 'guidance',
        ]
    elif sender in [
        QuestionnaireHeading,
        QuestionnaireParagraph,
        QuestionnaireImage,
    ]:
        fields = [
            'text',
            'guidance',
        ]
    elif sender == ProductTypeGroup:
        fields = [
            'name',
            'help_text',
        ]
    elif sender == StandardProductType:
        fields = [
            'name'
        ]
    elif sender == Group:
        fields = [
            'name'
        ]
    else:
        raise NotImplementedError(sender.__name__)
    if sender in (
            AssessmentType,
            PossibleAnswer,
            Question,
            QuestionLevel,
            Questionnaire,
            QuestionnaireHeading,
            QuestionnaireParagraph,
            QuestionnaireImage,
            Questionnaire,
            TextBlock,
    ):
        occurrence_prefix = u'{}, ID {}'.format(
            occurrence_prefix,
            instance.code,
        )
    elif sender in (
            HelpTextBlock,
    ):
        occurrence_prefix = u'{}, ID {}'.format(
            occurrence_prefix,
            instance.internal_code,
        )
    elif sender in (
            ProductType,
            ProductTypeGroup,
            StandardProductType,
            Group
    ):
        occurrence_prefix = u'{}, Name {}'.format(
            occurrence_prefix,
            instance.name,
        )
    _add_strings_to_translation_files(instance, fields, occurrence_prefix)
    if not kwargs.get('skip_flush', False):
        flush_pofiles()

if TRANSLATION_SIGNALS:
    post_save.connect(add_strings_to_translation_files, sender=TextBlock)
    post_save.connect(add_strings_to_translation_files, sender=HelpTextBlock)
    post_save.connect(add_strings_to_translation_files, sender=QuestionTrigger)
    post_save.connect(add_strings_to_translation_files, sender=QuestionLevel)
    post_save.connect(add_strings_to_translation_files, sender=PossibleAnswer)
    post_save.connect(add_strings_to_translation_files, sender=Questionnaire)
    post_save.connect(add_strings_to_translation_files, sender=AssessmentType)
    post_save.connect(add_strings_to_translation_files, sender=AttachmentDocumentType)
    post_save.connect(add_strings_to_translation_files, sender=ProductType)
    post_save.connect(add_strings_to_translation_files, sender=ProductTypeGroup)
    post_save.connect(add_strings_to_translation_files, sender=StandardProductType)
    post_save.connect(add_strings_to_translation_files, sender=Question)
    post_save.connect(add_strings_to_translation_files, sender=QuestionnaireHeading)
    post_save.connect(add_strings_to_translation_files, sender=QuestionnaireParagraph)
    post_save.connect(add_strings_to_translation_files, sender=QuestionnaireImage)
    post_save.connect(add_strings_to_translation_files, sender=Group)
