from gettext import GNUTranslations
from django.conf import settings
from rest_framework import serializers


class FakeGNUTranslations(object):
    """ Mock fake GNU translations for IO errors
    """

    def ugettext(self, value):
        return value


class TranslatableTextField(serializers.CharField):
    """ This is the translation serializer field that get's the translation from the .mo file
    at read time. It takes the active django language setting

    This could also be replaced with a user setting accessible through request.user.[language field name]
    """
    _cache = {}

    @classmethod
    def refresh_cache(cls, language=None):
        """ Refresh the translations class cache for performance

        :param language: language code
        """
        if language:
            languages = [language]
        else:
            languages = [item[0] for item in settings.LANGUAGES]
        for language in languages:
            try:
                with open(settings.COMPILED_LANGUAGE_FILES.format(language), 'r') as f:
                    cls._cache[language] = GNUTranslations(f)
            except IOError:
                cls._cache[language] = FakeGNUTranslations()

    def to_representation(self, value):
        """ Transform database value into serializable translated field value

        :param value: field value from database
        :return: translated value from translation file or translations cache
        """
        from .helpers import tolerant_ugettext
        response = super(TranslatableTextField, self).to_representation(value)
        response = tolerant_ugettext(response)
        return response
