from django import template

from translations.helpers import tolerant_ugettext, tolerant_ugettext_with_en

register = template.Library()


@register.filter
def ugettext_filter(text):
    return tolerant_ugettext(text)


@register.filter
def ugettext_with_lang_filter(text, given_lang):
    return tolerant_ugettext(text, given_lang=given_lang)


@register.filter
def ugettext_filter_with_en(text):
    return tolerant_ugettext_with_en(text)
