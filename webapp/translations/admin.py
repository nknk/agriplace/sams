import logging

from django.contrib import admin
from django.core.files import File

from core.admin import BaseAdmin
from .models import LanguageTranslation
from translations.helpers import _add_translation_for_language

logger = logging.getLogger(__name__)


class LanguageTranslationAdmin(BaseAdmin):
    list_display = ('uuid', 'translation_file', 'target_language', 'created_time', 'status')
    actions = ['generate_translation']

    def generate_translation(self, request, queryset):
        try:
            for data in queryset:
                success, report = _add_translation_for_language(data.target_language, data.translation_file.file)
                if success:
                    data.status = 'success'
                    data.report_file.delete()
                else:
                    data.report_file.save(report.name, File(report))
                    data.status = 'error'
                data.save()
                report.close()
            self.message_user(request, "Result: {}".format("Succeeded"))
        except Exception as ex:
            self.message_user(request, "Result: {}".format("An error occurred during the translation generation"))
            logger.warning(ex)
    
admin.site.register(LanguageTranslation, LanguageTranslationAdmin)
