import os
import tarfile
from datetime import datetime
from django.core.mail import EmailMessage
from django.core.management import BaseCommand
from sams_project.settings import PROJECT_DIR


class Command(BaseCommand):
    help = 'Generate a tarfile for all translations and email it'

    def handle(self, *args, **kwargs):
        # gzip locale directory to StringIO stream
        # email dir to tech@agriplace.com
        print "Generating tar package for translations"
        build_stamp = datetime.now().strftime("%Y%m%d%H%I")
        file_name = '/tmp/locale_{}.tar.gz'.format(build_stamp)
        tar = tarfile.open(file_name, 'w:gz')
        full_dir = os.path.join(PROJECT_DIR, 'locale')
        tar.add(full_dir, arcname='locale')
        tar.close()

        mail = EmailMessage(
            subject="Translation build {}".format(build_stamp),
            body="Attached is the translations package with build stamp {}\n\nAppend the contents of this package to "
                 "the build files to include the translations in the new Docker container\n\nRegards,\n\n"
                 "- Translations management command".format(build_stamp),
            to=['tech@agriplace.com'],
            from_email='translations@agriplace.com'
        )

        mail.attach_file(file_name, mimetype='application/x-tar')
        mail.send()
        print "Translation package sent to info@stamkracht.com"
