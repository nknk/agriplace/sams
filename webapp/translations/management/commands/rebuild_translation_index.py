from django.core.management import BaseCommand
from app_settings.models import TextBlock
from app_settings.models import HelpTextBlock
from assessments.models import QuestionLevel, PossibleAnswer, Questionnaire, AssessmentType, Question, QuestionnaireHeading, QuestionnaireParagraph, QuestionnaireImage
from assessments.models import QuestionTrigger
from attachments.models import DocumentType as AttachmentDocumentType
from organization.models import ProductTypeGroup, ProductType, StandardProductType
from translations.helpers import flush_pofiles
from translations.models import add_strings_to_translation_files


class Command(BaseCommand):
    help = 'Render translation files from database content'

    def handle(self, *args, **kwargs):

        for item in ProductTypeGroup.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in StandardProductType.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in ProductType.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in AttachmentDocumentType.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in QuestionTrigger.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in QuestionLevel.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in PossibleAnswer.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in Questionnaire.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in AssessmentType.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in TextBlock.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in HelpTextBlock.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in Question.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in QuestionnaireHeading.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in QuestionnaireParagraph.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        for item in QuestionnaireImage.objects.all():
            add_strings_to_translation_files(item, item.__class__, False, skip_flush=True)

        flush_pofiles()
        self.stdout.write('Successfully updated all translations strings')
