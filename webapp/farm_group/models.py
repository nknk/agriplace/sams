from django.contrib.auth.models import Group
import json_field
from datetime import datetime
from uuid import uuid4

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from assessments.models import HARVEST_YEAR_CHOICES, Assessment, AssessmentType, Question
from core.models import UuidModel
from assessment_workflow.models import AssessmentWorkflow
from organization.models import Organization


class AssessmentAgreement(UuidModel):
    harvest_year = models.IntegerField(
        _('Harvest year'), choices=HARVEST_YEAR_CHOICES, default=datetime.now().year
    )
    assessment_type = models.ForeignKey(
        AssessmentType, related_name="assessment_agreement", limit_choices_to={'kind': 'assessment'}
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    form_data = json_field.JSONField(
        help_text='Form data', default={},
    )

    def __unicode__(self):
        return u"{} - {}".format(self.user, self.assessment_type.name)

    def save(self, *args, **kwargs):
        super(AssessmentAgreement, self).save(*args, **kwargs)

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentAgreement'
        permissions = (
            ('view_assessment_agreement', 'Can view assessment agreement'),
        )


def upload_to_farm_group_bucket(instance, filename):
    return 'farm_group/imports/{uuid}/{file}'.format(
        uuid=str(uuid4()),
        file=filename
    )


class AssessmentMutationLog(UuidModel):
    ASSESSMENT_MUTATION = 1
    JUDGEMENT_MUTATION = 2

    MUTATION_TYPES = [
        (ASSESSMENT_MUTATION, _("Assessment")),
        (JUDGEMENT_MUTATION, _("Judgement"))
    ]

    ASSESSMENT_STATUSES = [
        ("internal_audit_done", _("Internal audit done")),
        ("document_review_done", _("Document review done")),
        ("audit_done", _("Audit done")),
        ("reaudit_required", _("Reaudit required")),
        ("action_required", _("Action required")),
        ("action_done", _("Action done")),
        ("certified", _("Certified")),
        ("rejected", _("Rejected"))
    ]
    ASSESSMENT_ELIGIBLE_STATUSES = [
        'internal_audit_done',
        'document_review_done',
        'audit_done',
        'reaudit_required',
        'action_required',
        'action_done',
        'certified',
        'rejected'
    ]
    JUDGEMENT_ELIGIBLE_STATUSES = [
        'audit_done',
        'reaudit_required',
        'action_required',
        'action_done',
        'certified',
        'rejected'
    ]
    ACTION_DEFAULT_PRIORITY = 400

    assessment_workflow = models.ForeignKey(AssessmentWorkflow)
    assessment = models.ForeignKey(Assessment, related_name='assessments')
    question = models.ForeignKey(Question, null=True)
    question_text = models.CharField(max_length=100, verbose_name='question_text')
    mutation = models.CharField(max_length=255, verbose_name='mutation')
    mutation_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='mutation_by_user',
                             blank=True, null=True)
    role = models.ForeignKey(Group, verbose_name=_("role"), related_name='mutation_by_role',
                             null=True, blank=True)
    assessment_status = models.CharField(
        _("Assessment status"),
        max_length=50,
        choices=ASSESSMENT_STATUSES,
        blank=True
    )

    priority = models.PositiveIntegerField(default=ACTION_DEFAULT_PRIORITY, null=True, blank=True, db_index=True)
    mutation_type = models.PositiveIntegerField(choices=MUTATION_TYPES, default=ASSESSMENT_MUTATION)

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentMutationLog'
        ordering = ('-mutation_date', 'priority')

    def save(self, *args, **kwargs):
        super(AssessmentMutationLog, self).save(*args, **kwargs)

    @classmethod
    def is_eligible_for_assessment_logs(cls, assessment_status):
        return assessment_status in cls.ASSESSMENT_ELIGIBLE_STATUSES

    @classmethod
    def is_eligible_for_judgement_logs(cls, assessment_status):
        return assessment_status in cls.JUDGEMENT_ELIGIBLE_STATUSES
