from django.conf.urls import patterns, url

from farm_group.views import assessment_workflow_agreement, create_assessment_from_workflow

urlpatterns = patterns(
    "farm_group.views",
    url(
        r'^(?P<assessment_workflow_pk>.+)/agreements$',
        assessment_workflow_agreement,
        name="assessment-workflow-agreement"
    ),
    url(
        r'^(?P<assessment_workflow_pk>.+)/new$',
        create_assessment_from_workflow,
        name="create-assessment-from-workflow"
    ),
)
