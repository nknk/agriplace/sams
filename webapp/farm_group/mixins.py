class AssessmentMutationLogModelMixin:
    @property
    def modified_by_user(self):
        return self._modified_by_user

    @modified_by_user.setter
    def modified_by_user(self, value):
        self._modified_by_user = value

    @property
    def modified_by_role(self):
        return self._modified_by_role

    @modified_by_role.setter
    def modified_by_role(self, value):
        self._modified_by_role = value

    def __init__(self, *args, **kwargs):
        self._modified_by_user = None
        self._modified_by_role = None
