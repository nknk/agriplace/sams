from celery import shared_task
from celery.utils.log import get_task_logger
from django.db.models.loading import get_model

from notifications.models import AUDIT_DATE_PLANNED
from notifications.utils import send_email_notification

logger = get_task_logger(__name__)


@shared_task
def send_email_date_change(workflow_id):
    workflow_instance = get_model('assessment_workflow', 'AssessmentWorkflow').objects.get(uuid=workflow_id)
    send_email_notification(AUDIT_DATE_PLANNED, workflow_instance)
