from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from organization.models import Organization, OrganizationMembership, OrganizationType

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from core.admin import slugify


@api_view(['GET', 'POST'])
def exchange(request, vendor="", relation_number=""):
    if request.method == 'POST':
        return _save_exchange_user(vendor, request.data)
    else:
        return _get_exchange_user(vendor, relation_number)


def _get_exchange_user(vendor, user_id):
    user = Organization.objects.get_user_in_organization(
        organization_name=vendor,
        user_id=user_id
    )
    if not user:
        return Response({
            "success": False,
            "message": _("User does not exist")
        })

    organization = user.organizations.first()

    data = {
        "organization_name": organization.name,
        "relation_number": organization.uuid,
        "contactperson_first_name": user.first_name,
        "contactperson_last_name": user.last_name,
        "address": organization.mailing_address1,
        "postcode": organization.mailing_postal_code,
        "city": organization.mailing_city,
        "country": organization.mailing_country,
        "phone": organization.phone,
        "mobile": "",
        "fax": "",
        "email": organization.email,
        "ggn": organization.ggn_number,
        "certificates": []
    }
    context = {
        "success": True,
        "data": data
    }

    return Response(context)


def _save_exchange_user(vendor, data):
    producer = get_user_model().objects.create_user(
        data.get('email'),
        data.get('email'),
        '',
        first_name=data.get('contactperson_first_name'),
        last_name=data.get('contactperson_last_name'),
    )

    grower_grp = Group.objects.get(name='grower')
    grower_grp.user_set.add(producer)

    parent_org = Organization.objects.get(name=vendor)

    org, created = Organization.objects.get_or_create(
        name=data.get('organization_name'),
        defaults=dict(
            mailing_address1=data.get('address'),
            mailing_postal_code=data.get('postcode'),
            mailing_city=data.get('city'),
            mailing_country=data.get('country'),
            phone=data.get('phone'),
            mobile=data.get('mobile'),
            email=data.get('email'),
            ggn_number=data.get('ggn')
        )
    )
    if not created:
        org.delete()
    else:
        org.users.add(producer)
        organization_type, _ = OrganizationType.objects.get_or_create(
            name='producer',
            slug=slugify('producer')
        )
        OrganizationMembership.objects.get_or_create(
            primary_organization=parent_org,
            secondary_organization=org,
            organization_type=organization_type,
            membership_number=data.get('producer_number'),
        )
        org.save()

    result = {
        "success": True,
        "organization": org.name
    }

    return Response(result, status.HTTP_201_CREATED)
