import json
from datetime import datetime
from unittest import skip

import faker
from dateutil import parser
from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from api.internal.tests.mixins import APITestSetupMixin, AuthAPIMixin
from assessment_types.tests.factories import AssessmentTypeFactory
from assessment_workflow.models import AssessmentWorkflow
from assessments.models import Assessment
from assessments.tests.factories import (
    QuestionLevelFactory, QuestionnaireFactory, PossibleAnswerSetFactory, QuestionFactory, JudgementFactory
)
from attachments.tests.factories import DocumentTypeFactory, AttachmentFactory
from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.models import AssessmentMutationLog
from farm_group.tests.factories import AssessmentWorkflowFactory
from organization.tests.factories import (
    OrganizationFactory, OrganizationTypeFactory, OrganizationMembershipFactory, ProductFactory
)
from ddt import ddt, unpack, data

faker = faker.Factory.create()

@ddt
class AuditorDashboardApiTest(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        response = self.create_workflow(
            username=self.USERNAME, password=self.PASSWORD, code=self.CODE
        )

        self.auditor = response['auditor']
        self.auditor_membership = response['auditor_membership']
        self.assessment = response['assessment']
        self.assessment_workflow = response['assessment_workflow']
        self.assessment_workflow.share_with_auditor()

        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    @unpack
    @data({'audit_date_url': 'workflow-planned-audit-date', 'audit_date_key': 'audit_date_planned',
           'audit_date_value': '2017-04-26T20:30:00'},
          {'audit_date_url': 'workflow-audit-start-date', 'audit_date_key': 'audit_date_actual_start',
           'audit_date_value': '2017-04-26T20:30:00'},
          {'audit_date_url': 'workflow-audit-end-date', 'audit_date_key': 'audit_date_actual_end',
           'audit_date_value': '2017-04-26T20:30:00'})
    def test_auditor_set_audit_date(self, audit_date_url, audit_date_key, audit_date_value):
        assessment_workflow = AssessmentWorkflow.objects.get(assessment=self.assessment.pk)
        if audit_date_key == 'audit_date_planned':
            self.assertEqual(None, assessment_workflow.audit_date_planned)
        if audit_date_key == 'audit_date_actual_start':
            self.assertEqual(None, assessment_workflow.audit_date_actual_start)
        if audit_date_key == 'audit_date_actual_end':
            self.assertEqual(None, assessment_workflow.audit_date_actual_end)

        self.client.force_authenticate(self.auditor)
        url = reverse(audit_date_url, kwargs={
            'membership_pk': self.auditor_membership.pk,
            'workflow_pk': assessment_workflow.uuid
        })
        result = self.client.put(url, {
            audit_date_key: audit_date_value
        }, HTTP_AUTHORIZATION=self.token)

        self.assertEqual(200, result.status_code)
        self.assertDictContainsSubset({
            'status': 'success'
        }, result.data)

        assessment_workflow = AssessmentWorkflow.objects.get(assessment=self.assessment.pk)
        if audit_date_key == 'audit_date_planned':
            self.assertEqual(datetime(2017, 4, 26, 20, 30), assessment_workflow.audit_date_planned)
        if audit_date_key == 'audit_date_actual_start':
            self.assertEqual(datetime(2017, 4, 26, 20, 30), assessment_workflow.audit_date_actual_start)
        if audit_date_key == 'audit_date_actual_end':
            self.assertEqual(datetime(2017, 4, 26, 20, 30), assessment_workflow.audit_date_actual_end)

    def test_auditor_set_audit_date_wrong_format(self):
        self.client.force_authenticate(self.auditor)

        assessment_workflow = AssessmentWorkflow.objects.get(assessment=self.assessment.pk)

        url = reverse('workflow-planned-audit-date', kwargs={
            'membership_pk': self.auditor_membership.pk,
            'workflow_pk': assessment_workflow.uuid
        })

        result = self.client.put(url, {
            'audit_date_planned': '2015-30-12'
        }, HTTP_AUTHORIZATION=self.token)

        self.assertEqual(400, result.status_code)

        self.assertDictContainsSubset({
            'audit_date_planned': [
                u'Datetime has wrong format. Use one of these formats instead: YYYY-MM-DDThh:mm[:ss[.uuuuuu]][+HH:MM|-HH:MM|Z].']
        }, result.data)


# testing
@skip('APP-1625')
class MutationLogApiTest(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        response = self.create_workflow(
            username=self.USERNAME, password=self.PASSWORD, code=self.CODE
        )

        self.organization = response['organization']
        self.auditor = response['auditor']
        self.assessment = response['assessment']
        self.assessment_type = response['assessment_type']
        self.assessment_workflow = response['assessment_workflow']
        self.assessment_workflow.share_with_auditor()

        if self.assessment_workflow.assessment_status != 'internal_audit_done':
            self.assertNotEqual(
                self.assessment_workflow.assessment_status, 'internal_audit_done',
                'Workflow assessment status must be "internal audit done"'
            )

        if not self.assessment.is_eligible_for_mutation_log():
            self.assertFalse(
                self.assessment.is_eligible_for_mutation_log(), "Assessment not eligible for mutation log"
            )

        question_level1 = QuestionLevelFactory.create(assessment_type=self.assessment_type)
        question_level2 = QuestionLevelFactory.create(assessment_type=self.assessment_type)
        questionnaire1 = QuestionnaireFactory.create(
            code='AF', assessment_type=self.assessment_type, order_index=1, name='All Farm Base Module'
        )
        questionnaire2 = QuestionnaireFactory.create(
            code='FV', assessment_type=self.assessment_type, order_index=3, name='Module Fruit & Vegetables'
        )
        possible_answer_set1 = PossibleAnswerSetFactory.create(assessment_type=self.assessment_type)
        possible_answer_set2 = PossibleAnswerSetFactory.create(assessment_type=self.assessment_type)
        self.document_type_attachment1 = DocumentTypeFactory.create(code='EV0', name='Policy Statement Food Safety')
        document_type_attachment2 = DocumentTypeFactory.create(code='EV1', name='Policy Statement Food Safety')
        question1 = QuestionFactory.create(
            level_object=question_level1, possible_answer_set=possible_answer_set1,
            document_types_attachments=(self.document_type_attachment1,), questionnaire=questionnaire1
        )
        question2 = QuestionFactory.create(
            level_object=question_level2, possible_answer_set=possible_answer_set2,
            document_types_attachments=(document_type_attachment2,), questionnaire=questionnaire2
        )

        JudgementFactory.create(question=question1, assessment=self.assessment)
        JudgementFactory.create(question=question2, assessment=self.assessment)
        AttachmentFactory.create(organization=self.organization)

        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_assessment_not_eligible_for_mutation(self):

        assessment = Assessment.objects.get(pk=self.assessment.pk)
        assessment_workflow = assessment.get_workflow_context()

        assessment_workflow.assessment_status = 'certified'
        assessment_workflow.save()

        is_eligible = assessment.is_eligible_for_mutation_log()

        self.assertFalse(is_eligible, msg="Assessment in 'certified' status should not be eligible for mutation")

    def test_text_attachments_delete(self):
        test_attachment_text = "text attachment reference test"

        url = reverse('assessment-attachment-links', kwargs={
            'organization_pk': self.organization.pk,
            'assessment_pk': self.assessment.pk
        })

        response = self.client.get(
            url, HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        attachment_links = [
            {
                "assessment": self.assessment.pk,
                "document_type_attachment": self.document_type_attachment1.pk,
                "attachment": {
                    "title": test_attachment_text,
                    "attachment_type": "TextReferenceAttachment",
                    "modified_time": datetime.now(),
                    "created_time": datetime.now(),
                    "organization": "",
                    "description": test_attachment_text
                },
                "is_done": True
            }
        ]

        response = self.client.post(
            url, data=attachment_links, format='json', HTTP_AUTHORIZATION=self.token,
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.content)

        attachment_link_id = response.data[0]["id"]
        url = "{0}{1}/".format(url, int(attachment_link_id))

        response = self.client.delete(
            url, format='json', HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        log = AssessmentMutationLog.objects.latest('mutation_date')

        self.assertEqual("Evidence reference '{0}' deleted".format(test_attachment_text), log.mutation)


class DashboardFilterApiTest(APITestCase, APITestSetupMixin, AuthAPIMixin):
    GG_CODE = 'globalgap_ifa'
    TN_CODE = 'Tesco_nurture'

    GROWER1_USERNAME = faker.user_name()
    GROWER2_USERNAME = faker.user_name()
    INSPECTOR_USERNAME = faker.user_name()
    AUDITOR_USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_farm_groups()
        self.grower1 = self.create_user(username=self.GROWER1_USERNAME, password=self.PASSWORD)
        self.grower2 = self.create_user(username=self.GROWER2_USERNAME, password=self.PASSWORD)
        self.internal_inspector = self.create_user(username=self.INSPECTOR_USERNAME, password=self.PASSWORD)
        self.internal_auditor = self.create_user(username=self.AUDITOR_USERNAME, password=self.PASSWORD)

        self.primary_organization = OrganizationFactory.create()
        self.grower_organization1 = OrganizationFactory.create(
            users=(self.grower1,)
        )
        self.grower_organization2 = OrganizationFactory.create(
            users=(self.grower1,)
        )
        self.internal_inspector_organization = OrganizationFactory.create(
            users=(self.internal_inspector,)
        )
        self.internal_auditor_organization = OrganizationFactory.create(
            users=(self.internal_auditor,)
        )
        organization_type = OrganizationTypeFactory.create()
        self.grower_membership1 = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.grower_organization1,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        self.grower_membership2 = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.grower_organization2,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        self.internal_inspector_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_inspector_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )
        self.internal_auditor_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_auditor_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_AUDITOR)
        )
        self.product = ProductFactory.create(organization=self.grower_organization1)
        self.product = ProductFactory.create(organization=self.grower_organization2)
        self.GG_assessment_type = AssessmentTypeFactory.create(code=self.GG_CODE)
        self.TN_assessment_type = AssessmentTypeFactory.create(code=self.TN_CODE)
        self.create_workflows()

    def create_workflows(self):
        for i in range(4):
            gg_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization1, self.grower1, self.GG_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower1, author_organization=self.grower_organization1,
                assessment_type=self.GG_assessment_type, assessment=gg_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership1, assessment_status='internal_audit_request', harvest_year='2016',
                audit_date_planned='2016-10-22'
            )

            tn_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization2, self.grower2, self.TN_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower2, author_organization=self.grower_organization2,
                assessment_type=self.TN_assessment_type, assessment=tn_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership2, assessment_status='internal_audit_done', harvest_year='2017',
                audit_date_planned='2017-10-22'
            )

    def year_data_checks(self, response_content, audit_date_planned):
        self.assertEqual(response_content.get('count'), 4)
        results = response_content.get('results')
        for result in results:
            self.assertEqual(parser.parse(result.get('audit_date_planned')).strftime('%Y-%m-%d'), audit_date_planned)

    def test_year_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year={}'.format(url, 2016)

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.year_data_checks(response_content, '2016-10-22')

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year={}'.format(url, 2017)

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.year_data_checks(response_content, '2017-10-22')

    def test_year_start_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year_start={}&year={}'.format(url, '2016-01-20', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 8)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year_start={}&year={}'.format(url, '2017-01-20', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year_start={}&year={}'.format(url, '2018-01-20', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 0)

    def test_year_end_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year_end={}&year={}'.format(url, '2018-01-20', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 8)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year_end={}&year={}'.format(url, '2017-01-20', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?year_end={}&year={}'.format(url, '2016-01-20', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 0)

    def test_assessment_type_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_types={}&year={}'.format(url, self.GG_assessment_type.pk, 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_types={}&year={}'.format(url, self.TN_assessment_type.pk, 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_types={},{}&year={}'.format(
            url, self.GG_assessment_type.pk, self.TN_assessment_type.pk, 'All'
        )

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 8)

    def test_assessment_states_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_states={}&year={}'.format(url, 'internal_audit_request', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_states={}&year={}'.format(url, 'internal_audit_done', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_states={},{}&year={}'.format(
            url, 'internal_audit_done', 'internal_audit_request', 'All'
        )

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 8)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?assessment_states={}&year={}'.format(url, 'document_review_done', 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 0)

    def test_inspectors_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?inspectors={}&year={}'.format(url, self.internal_inspector.pk, 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 8)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?inspectors={}&year={}'.format(url, self.grower1.pk, 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 0)

    def test_search_filter(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?search_str={}&year={}'.format(url, self.grower_organization1.name, 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)

        url = reverse('dashoard', kwargs={
            'membership_pk': self.internal_inspector_membership.pk
        })
        url = '{}?search_str={}&year={}'.format(url, self.grower_organization2.name, 'All')

        response = self.client.get(url, HTTP_AUTHORIZATION=token)

        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)
        self.assertEqual(response_content.get('count'), 4)
