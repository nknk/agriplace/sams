from django.conf.urls import patterns, url

from farm_group.api.internal.membership_views import (
    AuditorUpdateView, PlannedAuditDateView, OverviewDescriptionUpdateView, InternalDescriptionUpdateView,
    JudgementUpdateView, ReAuditUpdateView, WorkflowAgreementCreateView,
    AuditStartDateView, AuditEndDateView)

urlpatterns = patterns(
    '',
    url(
        r'(?P<workflow_pk>.+)/agreements/$',
        WorkflowAgreementCreateView.as_view(),
        name='save-assessment-workflow-agreement'
    ),
    url(
        r'(?P<workflow_pk>.*)/audit-date/$',
        PlannedAuditDateView.as_view(),
        name='workflow-planned-audit-date'
    ),
    url(
        r'(?P<workflow_pk>.*)/audit-start-date/$',
        AuditStartDateView.as_view(),
        name='workflow-audit-start-date'
    ),
    url(
        r'(?P<workflow_pk>.*)/audit-end-date/$',
        AuditEndDateView.as_view(),
        name='workflow-audit-end-date'
    ),
    url(
        r'(?P<workflow_pk>.*)/auditor/$',
        AuditorUpdateView.as_view(),
        name='workflow-auditor-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/judgement/$',
        JudgementUpdateView.as_view(),
        name='workflow-judgement-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/re-audit/$',
        ReAuditUpdateView.as_view(),
        name='workflow-re-audit-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/overview-description/$',
        OverviewDescriptionUpdateView.as_view(),
        name='workflow-overview-description-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/internal-description/$',
        InternalDescriptionUpdateView.as_view(),
        name='workflow-internal-description-update'
    ),
)
