from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework import generics, permissions
from rest_framework import response, status
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated

from assessment_workflow.models import AssessmentWorkflow
from core.constants import INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.api.internal.serializers import AssessmentAgreementSerializer
from farm_group.api.internal.serializers import (
    SetAuditDateSerializer, SetAuditorSerializer, AuditorSerializer, SetJudgementSerializer, SetReauditSerializer,
    DescriptionSerializer
)
from farm_group.models import AssessmentAgreement
from farm_group.tasks import send_email_date_change
from farm_group.utils import get_all_data_objects_per_state
from organization.mixins import OrganizationMixin


class DashboardView(OrganizationMixin, GenericAPIView):
    """
    Api view for retrieving dashboard data for auditor and coordinator
    """

    def get_queryset(self):
        return self.get_organization()

    def get(self, request, *args, **kwargs):
        serialized = list()
        is_auditor = request.user.groups.filter(name=INTERNAL_INSPECTOR).exists()
        is_coordinator = request.user.groups.filter(name=INTERNAL_AUDITOR).exists()

        if is_auditor or is_coordinator:
            state_permission_dict = get_all_data_objects_per_state(request)
            assessments = AssessmentWorkflow.objects.get_assessments(
                organization=self.get_queryset(), is_auditor=is_auditor, is_coordinator=is_coordinator
            )

            for wf_ctx in assessments:
                assessment_dict = self.dashboard_assessment_dict(wf_ctx, request.user)
                assessment_dict['permissions'] = state_permission_dict.get(wf_ctx.assessment_status, [])
                serialized.append(assessment_dict)

        else:
            self.permission_denied(request)
        return response.Response(serialized)

    def dashboard_assessment_dict(self, workflow_ctx, user):
        """
        Create dict representation for dashboard

        :param workflow_ctx: assessment workflow context
        :param user: current logged in user
        :return: dict() of assessments for auditor / coordinator dashboard
        """
        assessment = workflow_ctx.assessment
        org = workflow_ctx.author_organization
        result = dict()

        result['workflow_uuid'] = workflow_ctx.uuid
        result['assessment'] = {
            'status': workflow_ctx.assessment_status,
            'type': workflow_ctx.assessment_type.code,
            'label': workflow_ctx.assessment_type.name
        }
        if assessment:
            result['assessment'].update({
                'uuid': assessment.uuid,
                'url': ("/{}/our/assessments/{}/detail".format(workflow_ctx.pk, assessment.uuid)),
            })
        else:
            result['assessment'].update({
                'uuid': "",
                'url': "#",
            })
        result.update({
            'organization_name': org.name,
            'organization_url': '#',
            'membership_number': workflow_ctx.membership.membership_number,
            'audit_date_actual': workflow_ctx.audit_date_actual,
            'audit_date_planned': workflow_ctx.audit_date_planned,
            'preferred_month': workflow_ctx.audit_preferred_month,
        })
        if workflow_ctx.auditor_organization:
            result['auditor'] = "%s" % workflow_ctx.auditor_user.id
        return result


class OrganizationAuditorListView(OrganizationMixin, ListAPIView):
    serializer_class = AuditorSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        try:
            list_of_auditors = AssessmentWorkflow.objects.get_organization_auditors(self.get_organization())
        except ObjectDoesNotExist:
            list_of_auditors = []
        return list_of_auditors


class AuditorUpdateView(generics.GenericAPIView):
    serializer_class = SetAuditorSerializer

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                try:
                    auditor_user_id = serializer.data['auditor']
                    if not auditor_user_id:
                        wf_ctx.assign_auditor(None, None)
                        return response.Response({'status': 'success'})
                    else:
                        auditor_user = get_user_model().objects.get(pk=auditor_user_id)
                        auditor_organization_list = auditor_user.organizations.all()
                        if auditor_organization_list.count():
                            auditor_organization = auditor_organization_list[0]
                            wf_ctx.assign_auditor(auditor_user.id, auditor_organization.uuid)
                            return response.Response({'status': 'success'})
                except ObjectDoesNotExist:
                    return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuditDateView(generics.GenericAPIView):
    """
    Api view for setting the audit date
    """
    serializer_class = SetAuditDateSerializer

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_planned_audit_date(serializer.validated_data['audit_date_planned'])
                send_email_date_change.delay(workflow_pk)
                return response.Response({'status': 'success'})

        except AssessmentWorkflow.DoesNotExist:
            raise Http404()

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class JudgementUpdateView(generics.GenericAPIView):
    """
    Api view for setting the judgement
    """
    serializer_class = SetJudgementSerializer

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_judgement(serializer.data['judgement'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReAuditUpdateView(generics.GenericAPIView):
    """
    Api view for setting the reaudit checkbox

    """
    serializer_class = SetReauditSerializer

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_reaudit(serializer.data['reaudit'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InternalDescriptionUpdateView(generics.GenericAPIView):
    """
    Api view for setting the Internal Description

    """
    serializer_class = DescriptionSerializer

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_internal_description(serializer.data['text'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OverviewDescriptionUpdateView(generics.GenericAPIView):
    """
    Api view for setting the Overview Description

    """
    serializer_class = DescriptionSerializer

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_overview_description(serializer.data['text'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WorkflowAgreementCreateView(OrganizationMixin, generics.CreateAPIView):
    model = AssessmentAgreement
    serializer_class = AssessmentAgreementSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid():
            try:
                workflow_context = AssessmentWorkflow.objects.select_related('assessment_type').get(
                    pk=self.kwargs.get('workflow_pk', '')
                )
            except ObjectDoesNotExist:
                raise Http404("The workflow does not exists.")

            obj = AssessmentAgreement()
            obj.harvest_year = workflow_context.harvest_year
            obj.user = self.request.user
            obj.form_data = self.request.data.get('form_data')
            obj.assessment_type = workflow_context.assessment_type
            obj.save()

            form_data = obj.form_data
            answer = form_data.get("jaNeeHzpc", "")
            ggn_number = form_data.get("ggn", "")
            audit_month = form_data.get("start", "")

            # analyze and proceed form data
            if answer in ['jaa', 'nee']:
                # create workflow
                if answer == 'jaa':
                    workflow_context.audit_preferred_month = int(audit_month)
                    workflow_context.assessment_status = 'audit_hzpc'
                elif answer == 'nee':
                    workflow_context.assessment_status = 'self_managed'
                    user_organization = self.get_organization()
                    user_organization.ggn_number = ggn_number
                    user_organization.save()
                workflow_context.save()


class WorkflowAgreementCreateView(OrganizationMixin, generics.CreateAPIView):
    model = AssessmentAgreement
    serializer_class = AssessmentAgreementSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid():
            try:
                workflow_context = AssessmentWorkflow.objects.select_related('assessment_type').get(
                    pk=self.kwargs.get('workflow_pk', '')
                )
            except ObjectDoesNotExist:
                raise Http404("The workflow does not exists.")

            obj = AssessmentAgreement()
            obj.harvest_year = workflow_context.harvest_year
            obj.user = self.request.user
            obj.form_data = self.request.data.get('form_data')
            obj.assessment_type = workflow_context.assessment_type
            obj.save()

            form_data = obj.form_data
            answer = form_data.get("jaNeeHzpc", "")
            ggn_number = form_data.get("ggn", "")
            audit_month = form_data.get("start", "")

            # analyze and proceed form data
            if answer in ['jaa', 'nee']:
                # create workflow
                if answer == 'jaa':
                    workflow_context.audit_preferred_month = int(audit_month)
                    workflow_context.assessment_status = 'audit_hzpc'
                elif answer == 'nee':
                    workflow_context.assessment_status = 'self_managed'
                    hzpc_user_organization = self.get_organization()
                    hzpc_user_organization.ggn_number = ggn_number
                    hzpc_user_organization.save()
                workflow_context.save()