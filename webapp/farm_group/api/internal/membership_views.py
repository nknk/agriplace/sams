import datetime
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.query_utils import Q
from django.http import Http404
from rest_framework import pagination
from rest_framework import permissions
from rest_framework import response, status
from rest_framework.generics import GenericAPIView, ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param
from rest_framework_bulk import generics

from assessment_workflow.models import AssessmentWorkflow
from core.constants import INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.api.internal.serializers import (
    AuditorSerializer, SetAuditorSerializer, SetAuditDateSerializer, DescriptionSerializer, SetJudgementSerializer,
    SetReauditSerializer, AssessmentAgreementSerializer,
    SetAuditStartDateSerializer, SetAuditEndDateSerializer)
from farm_group.models import AssessmentAgreement
from farm_group.tasks import send_email_date_change
from farm_group.utils import get_all_data_objects_per_state
from organization.api.internal.serializers import DashboardAssessmentSerializer
from sams.view_mixins import MembershipRequired, MembershipRequiredPermission


class AssessmentsPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10

    def get_first_link(self):
        url = self.request.build_absolute_uri()
        return replace_query_param(url, self.page_query_param, 1)

    def get_last_link(self):
        url = self.request.build_absolute_uri()
        return replace_query_param(url, self.page_query_param, self.page.paginator.num_pages)

    def get_paginated_response(self, data):
        return Response({
            'first': self.get_first_link(),
            'last': self.get_last_link(),
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'results': data
        })


class DashboardView(ListAPIView):
    """
    Api view for retrieving dashboard data for auditor and coordinator
    """
    pagination_class = AssessmentsPagination
    serializer_class = DashboardAssessmentSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def apply_filters(self, work_flows):
        harvest_year = self.request.query_params.get('year')
        audit_date_start = self.request.query_params.get('year_start')
        audit_date_end = self.request.query_params.get('year_end')
        assessment_types = self.request.query_params.get('assessment_types')
        assessment_states = self.request.query_params.get('assessment_states')
        inspectors = self.request.query_params.get('inspectors')
        search_str = self.request.query_params.get('search_str')

        current_year = datetime.datetime.now().year
        harvest_year = harvest_year or current_year

        if harvest_year and harvest_year != 'All':
            harvest_year = int(harvest_year)
            work_flows = work_flows.filter(harvest_year=harvest_year)
        if audit_date_start:
            work_flows = work_flows.filter(audit_date_planned__gte=audit_date_start)
        if audit_date_end:
            audit_date_end = datetime.datetime.strptime(audit_date_end, '%Y-%m-%d').date()
            audit_date_end = datetime.datetime.combine(audit_date_end, datetime.time.max)
            work_flows = work_flows.filter(audit_date_planned__lte=audit_date_end)
        if assessment_types:
            assessment_types = assessment_types.split(",")
            work_flows = work_flows.filter(assessment_type__in=assessment_types)
        if assessment_states:
            assessment_states = assessment_states.split(",")
            work_flows = work_flows.filter(assessment_status__in=assessment_states)
        if inspectors:
            inspectors = inspectors.split(",")
            if "-1" in inspectors:
                inspectors.remove("-1")
                work_flows = work_flows.filter(
                    Q(auditor_user__in=inspectors) |
                    Q(auditor_user__isnull=True)
                )
            else:
                work_flows = work_flows.filter(auditor_user__in=inspectors)
        if search_str:
            work_flows = work_flows.filter(
                Q(membership__secondary_organization__name__icontains=search_str) |
                Q(membership__membership_number__icontains=search_str)
            )
        work_flows = work_flows.distinct()
        return list(work_flows)

    def get_queryset(self):
        organization_role = self.request.membership.role
        is_auditor = INTERNAL_INSPECTOR == organization_role.name
        is_coordinator = INTERNAL_AUDITOR == organization_role.name

        assessments = AssessmentWorkflow.objects.get_assessments(
            membership=self.request.membership, is_auditor=is_auditor, is_coordinator=is_coordinator
        )
        assessments = self.apply_filters(assessments)
        return assessments

    def get_serializer_context(self):
        state_permission_dict = get_all_data_objects_per_state(self.request)
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'state_permission_dict': state_permission_dict
        }


class OrganizationAuditorListView(ListAPIView):
    serializer_class = AuditorSerializer
    permission_classes = (permissions.IsAuthenticated, MembershipRequiredPermission)

    def get_serializer_context(self):
        return {
            "year": self.request.query_params.get('year')
        }

    def get_queryset(self):
        try:
            list_of_auditors = AssessmentWorkflow.objects.get_organization_auditors(self.request.membership, )
        except ObjectDoesNotExist:
            list_of_auditors = []
        return list_of_auditors

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        list_of_auditors = self.get_serializer(queryset, many=True).data
        open_workflows_count = self.get_open_workflows()
        open_workflow = {
            "first_name": "Open", "last_name": "", "work_flow_count": open_workflows_count, "id": "-1"
        }
        list_of_auditors.insert(0, open_workflow)
        return Response(list_of_auditors)

    def get_open_workflows(self):
        organization_role = self.request.membership.role
        is_auditor = INTERNAL_INSPECTOR == organization_role.name
        is_coordinator = INTERNAL_AUDITOR == organization_role.name

        year = self.request.query_params.get("year")
        assessments = AssessmentWorkflow.objects.get_assessments(
            membership=self.request.membership, is_auditor=is_auditor, is_coordinator=is_coordinator
        )
        if year and year != "All":
            assessments = assessments.filter(
                auditor_user__isnull=True,
                harvest_year=year
            )
        else:
            assessments = assessments.filter(
                auditor_user__isnull=True
            )
        return assessments.distinct().count()


class AuditorUpdateView(generics.GenericAPIView):
    serializer_class = SetAuditorSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                try:
                    auditor_user_id = serializer.data['auditor']
                    if not auditor_user_id:
                        wf_ctx.assign_auditor(None, None)
                        return response.Response({'status': 'success'})
                    else:
                        auditor_user = get_user_model().objects.get(pk=auditor_user_id)
                        auditor_organization_list = auditor_user.organizations.all()
                        if auditor_organization_list.count():
                            auditor_organization = auditor_organization_list[0]
                            wf_ctx.assign_auditor(auditor_user.id, auditor_organization.uuid)
                            return response.Response({'status': 'success'})
                except ObjectDoesNotExist:
                    return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PlannedAuditDateView(generics.GenericAPIView):
    """
    Api view for setting the audit date
    """
    serializer_class = SetAuditDateSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_planned_audit_date(serializer.validated_data['audit_date_planned'])
                send_email_date_change.delay(workflow_pk)
                return response.Response({'status': 'success'})

        except AssessmentWorkflow.DoesNotExist:
            raise Http404()

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuditStartDateView(generics.GenericAPIView):
    """
    Api view for setting the audit date
    """
    serializer_class = SetAuditStartDateSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_audit_date_actual_start(serializer.validated_data['audit_date_actual_start'])
                return response.Response({'status': 'success'})

        except AssessmentWorkflow.DoesNotExist:
            raise Http404()

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuditEndDateView(generics.GenericAPIView):
    """
    Api view for setting the audit date
    """
    serializer_class = SetAuditEndDateSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_audit_date_actual_end(serializer.validated_data['audit_date_actual_end'])
                return response.Response({'status': 'success'})

        except AssessmentWorkflow.DoesNotExist:
            raise Http404()

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OverviewDescriptionUpdateView(generics.GenericAPIView):
    """
    Api view for setting the Overview Description

    """
    serializer_class = DescriptionSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_overview_description(serializer.data['text'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InternalDescriptionUpdateView(generics.GenericAPIView):
    """
    Api view for setting the Internal Description

    """
    serializer_class = DescriptionSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_internal_description(serializer.data['text'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class JudgementUpdateView(generics.GenericAPIView):
    """
    Api view for setting the judgement
    """
    serializer_class = SetJudgementSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_judgement(serializer.data['judgement'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReAuditUpdateView(generics.GenericAPIView):
    """
    Api view for setting the reaudit checkbox

    """
    serializer_class = SetReauditSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def put(self, request, workflow_pk, *args, **kwargs):
        try:
            wf_ctx = AssessmentWorkflow.objects.get(pk=workflow_pk)
        except AssessmentWorkflow.DoesNotExist:
            raise Http404()
        else:
            serializer = self.get_serializer(data=request.data)
            if serializer.is_valid():
                wf_ctx.set_reaudit(serializer.data['reaudit'])
                return response.Response({'status': 'success'})

        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WorkflowAgreementCreateView(CreateAPIView):
    model = AssessmentAgreement
    serializer_class = AssessmentAgreementSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def perform_create(self, serializer):
        serializer = self.get_serializer(data=self.request.data)
        if serializer.is_valid():
            try:
                workflow_context = AssessmentWorkflow.objects.select_related('assessment_type').get(
                    pk=self.kwargs.get('workflow_pk', '')
                )
            except ObjectDoesNotExist:
                raise Http404("The workflow does not exists.")

            obj = AssessmentAgreement()
            obj.harvest_year = workflow_context.harvest_year
            obj.user = self.request.user
            obj.form_data = self.request.data.get('form_data')
            obj.assessment_type = workflow_context.assessment_type
            obj.save()

            form_data = obj.form_data
            answer = form_data.get("jaNeeHzpc", "")
            ggn_number = form_data.get("ggn", "")
            audit_month = form_data.get("start", "")

            # analyze and proceed form data
            if answer in ['jaa', 'nee']:
                # create workflow
                if answer == 'jaa':
                    workflow_context.audit_preferred_month = int(audit_month)
                    workflow_context.assessment_status = 'audit_hzpc'
                elif answer == 'nee':
                    workflow_context.assessment_status = 'self_managed'
                    hzpc_user_organization = self.request.membership.secondary_organization
                    hzpc_user_organization.ggn_number = ggn_number
                    hzpc_user_organization.save()
                workflow_context.save()
