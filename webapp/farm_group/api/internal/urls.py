from django.conf.urls import patterns, url

from farm_group.api.internal.views import (
    AuditDateView, AuditorUpdateView, JudgementUpdateView, ReAuditUpdateView, InternalDescriptionUpdateView,
    OverviewDescriptionUpdateView, WorkflowAgreementCreateView
)

urlpatterns = patterns(
    '',
    url(
        r'(?P<workflow_pk>.+)/agreements/$',
        WorkflowAgreementCreateView.as_view(),
        name='workflow-agreements'
    ),
    url(
        r'(?P<workflow_pk>.*)/audit-date/$',
        AuditDateView.as_view(),
        name='workflow-audit-date'
    ),
    url(
        r'(?P<workflow_pk>.*)/auditor/$',
        AuditorUpdateView.as_view(),
        name='workflow-auditor-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/judgement/$',
        JudgementUpdateView.as_view(),
        name='workflow-judgement-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/re-audit/$',
        ReAuditUpdateView.as_view(),
        name='workflow-re-audit-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/internal-description/$',
        InternalDescriptionUpdateView.as_view(),
        name='workflow-internal-description-update'
    ),
    url(
        r'(?P<workflow_pk>.*)/overview-description/$',
        OverviewDescriptionUpdateView.as_view(),
        name='workflow-overview-description-update'
    ),
)
