from django.contrib.auth import get_user_model
from django.contrib.auth.models import User, Group
from rest_framework import serializers

from assessments.models import Question
from farm_group.models import AssessmentMutationLog, AssessmentAgreement


class AuditorSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField('get_id_as_string')
    work_flow_count = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'work_flow_count']

    def get_id_as_string(self, obj):
        return "%s" % obj.pk

    def get_work_flow_count(self, obj):
        year = self.context.get("year")
        workflow_count = obj.assessment_workflow_auditor
        if year and year != "All":
            workflow_count = workflow_count.filter(harvest_year=year)
        return workflow_count.count()


class SetAuditDateSerializer(serializers.Serializer):
    audit_date_planned = serializers.DateTimeField()


class SetAuditStartDateSerializer(serializers.Serializer):
    audit_date_actual_start = serializers.DateTimeField()


class SetAuditEndDateSerializer(serializers.Serializer):
    audit_date_actual_end = serializers.DateTimeField()


class SetAuditorSerializer(serializers.Serializer):
    auditor = serializers.CharField(required=False)


class SetJudgementSerializer(serializers.Serializer):
    judgement = serializers.CharField()


class DescriptionSerializer(serializers.Serializer):
    text = serializers.CharField(required=False)


class SetReauditSerializer(serializers.Serializer):
    reaudit = serializers.BooleanField()


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ['uuid', 'questionnaire']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['name']


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)

    class Meta:
        model = get_user_model()
        fields = ['username', 'groups']


class MutationLogSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    question = QuestionSerializer(read_only=True)

    class Meta:
        model = AssessmentMutationLog


class AssessmentAgreementSerializer(serializers.ModelSerializer):
    form_data = serializers.ReadOnlyField()

    class Meta:
        model = AssessmentAgreement
        fields = ('form_data',)
