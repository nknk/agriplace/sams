from django.contrib import admin

from core.admin import BaseAdmin
from farm_group.models import AssessmentAgreement


@admin.register(AssessmentAgreement)
class AssessmentAgreementAdmin(BaseAdmin):
    search_fields = (
        'assessment_type',
    )
