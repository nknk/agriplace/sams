from django.apps import AppConfig


class FarmGroupConfig(AppConfig):
    name = 'farm_group'
    verbose_name = 'Farm Group Application'

    def ready(self):
        import farm_group.signals
