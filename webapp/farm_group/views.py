import logging
from operator import methodcaller
import datetime

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.db import transaction, DatabaseError
from django.http import Http404, HttpResponseBadRequest, HttpResponseForbidden
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView, View

from ap_extras.item_list import ItemListModel, ItemListColumn
from app_settings.utils import get_help_text_block
from assessment_workflow.models import AssessmentWorkflow, CLOSED_STATUSES
from assessments.models import AssessmentType, Assessment, Questionnaire, BackgroundEvidenceCopy
from assessments.views import get_assessments_context, get_assessment_context
from attachments.models import DocumentType
from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR, EXTERNAL_AUDITOR
from farm_group.models import AssessmentAgreement
from farm_group.utils import get_user_info, BLANK_JUDGEMENT_ERROR_MSG
from notifications.models import ACTION_REMINDER, OBTAINING_CERTIFICATE
from notifications.utils import send_email_for_notification_type
from organization.models import HZPC, AGRIPLACE
from organization.views import get_organization_context
from sams.constants import PAGINATION_ITEMS_PER_PAGE, ICONS
from sams.decorators import membership_required
from sams.utils import get_layout_context
from sams.view_mixins import MembershipRequired
from sams.views import get_common_context
from translations.helpers import tolerant_ugettext

logger = logging.getLogger(__name__)


def get_common_dasboard_context(request, kwargs, context):
    organization = request.membership.secondary_organization
    context['organization'] = organization
    context["page_name"] = "home"
    context["page_title"] = _("Organization homepage")
    context["page_triggers"] = [context["page_name"]]
    context["help_url"] = "organization.html#" + context["page_name"]
    return context


def get_grower_context(request, kwargs, context):
    organization = request.membership.secondary_organization
    context["hide_breadcrumbs"] = True
    context['organization'] = organization
    context["current_organization_name"] = organization.name
    context["current_organization_product_count"] = organization.products.count()
    context["current_organization_partners_count"] = organization.partners.count()

    # Layout options
    context["hide_navbar"] = True

    assessment_types = AssessmentWorkflow.objects.filter(
        membership=request.membership
    ).exclude(assessment_status__in=CLOSED_STATUSES)

    # TODO the itemlistmodel doesnt play nice in these conditions (almost hating it)
    predefined_assessments_model = ItemListModel()
    predefined_assessments_model.id = "assessments"
    predefined_assessments_model.items = assessment_types

    predefined_assessments_model.columns = [
        ItemListColumn(
            header=_("Assessment"),
            text=methodcaller('get_text'),
            url=methodcaller('get_url'),
            icon_class=methodcaller('get_icon_class')
        ),
        ItemListColumn(
            header=_("Progress"),
            slug='progress',
            text=methodcaller('get_progress')
        ),
        ItemListColumn(
            header=_("Status"),
            slug='status',
            text=methodcaller('get_status')
        ),
        ItemListColumn(
            header=_("Date of audit"),
            text=methodcaller('get_audit_date')
        )
    ]
    predefined_assessments_model.hide_toolbar = True
    predefined_assessments_model.hide_bottom_toolbar = False
    predefined_assessments_model.bottom_button_url = reverse("my_assessment_list_closed",
                                                             kwargs={"membership_pk": request.membership.pk})
    predefined_assessments_model.bottom_button_text = _("Certification history")
    predefined_assessments_model.no_items_text = _("No assessments available.")
    predefined_assessments_model.refresh()
    context['assessment_list'] = predefined_assessments_model

    list_of_assessments = organization.assessments.filter(assessment_type__kind='assessment')
    if list_of_assessments:
        context['last_assessment'] = list_of_assessments.order_by("-created_time")[0]

    return context


def get_auditor_context(membership, context):
    wf_contexts = AssessmentWorkflow.objects.auditor_assessments(membership=membership)
    context['wf_contexts'] = wf_contexts
    return context


def get_coordinator_context(request, context):
    wf_contexts = AssessmentWorkflow.objects.filter(
        shared_with_coordinator=True,
    ).exclude(
        workflow_status='error'
    ).select_related('assessment').select_related('assessment__organization')
    context['wf_contexts'] = wf_contexts
    context['auditors'] = get_user_model().objects.filter(
        groups__name=INTERNAL_INSPECTOR)

    return context


class DashboardView(MembershipRequired, TemplateView):
    def get_template_names(self):
        return get_user_info(self.request)['home_template_name']

    def get_context_data(self, **kwargs):
        request = self.request
        context = super(DashboardView, self).get_context_data(**kwargs)
        organization_context, __ = get_organization_context(request, kwargs, '')
        context.update(organization_context)
        context.update(get_common_context(request))
        context.update(get_common_dasboard_context(request, kwargs, context))

        if AGRIPLACE == get_layout_context(self.request):
            return self.get_agriplace_context(context=context)
        else:
            return self.get_farm_group_context(context=context)

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        if context.get('external_auditor', None):
            return redirect('shared_assessment_list', self.request.membership.pk)
        return self.render_to_response(context)

    def get_agriplace_context(self, context):
        organization = self.request.membership.secondary_organization
        # new layout
        context["hide_breadcrumbs"] = True
        context["current_organization_name"] = organization.name
        context["current_organization_product_count"] = organization.products.count()
        context["current_organization_partners_count"] = organization.partners.count()

        # if organization type auditor -> redirect to shared assessments
        if self.request.membership.role.name == EXTERNAL_AUDITOR:
            context['external_auditor'] = True

        # Layout options
        context["hide_navbar"] = True

        organizations = self.request.user.organizations.all().order_by("name")
        context['organizations'] = organizations

        if not organizations.count():
            context["show_welcome_message"] = True

        list_of_assessments = organization.assessments.filter(assessment_type__kind='assessment')
        if list_of_assessments:
            context['last_assessment'] = list_of_assessments.order_by("-created_time")[0]

        return context

    def get_farm_group_context(self, context):
        user_info = get_user_info(self.request)

        if GROWER == user_info['group']:
            context.update(get_grower_context(self.request, self.kwargs, context))
        elif INTERNAL_INSPECTOR == user_info['group']:
            context['dashboard'] = True
            context.update(get_auditor_context(
                membership=self.request.membership, context=context)
            )
        elif INTERNAL_AUDITOR == user_info['group']:
            context['dashboard'] = True
            context.update(get_coordinator_context(self.request, context))

        return context


class AssessmentListView(MembershipRequired, TemplateView):
    closed = False

    def get_template_names(self):
        if AGRIPLACE == get_layout_context(self.request):
            if self.closed:
                return 'assessments/assessment_list_closed.html'

            return 'assessments/assessment_list.html'
        else:
            return 'farm_group/assessments/list.html'

    def get_context_data(self, **kwargs):
        if AGRIPLACE == get_layout_context(self.request):
            if self.closed:
                return self.get_agriplace_context_closed()

            return self.get_agriplace_context()
        else:
            return self.get_farm_group_context()

    def get_farm_group_context(self):
        request = self.request
        context = get_common_dasboard_context(
            request,
            self.kwargs,
            super(AssessmentListView, self).get_context_data(**self.kwargs)
        )
        context.update(get_common_context(request))
        context['page_triggers'] = ['my_assessments']
        organization = context['organization']
        context.update(super(AssessmentListView, self).get_context_data(**self.kwargs))
        context.update({
            'page_title': _("Certification"),
            'page_title_url': reverse("my_assessment_list", args=[request.membership.pk]),
        })

        wfc_list = AssessmentWorkflow.objects.filter(
            membership=request.membership
        )
        # Check whether to show the open or closed assessments
        if self.closed:
            context['closed'] = True
            wfc_list = wfc_list.filter(
                assessment_status__in=CLOSED_STATUSES
            )
        else:
            wfc_list = wfc_list.exclude(
                assessment_status__in=CLOSED_STATUSES
            )

        # Active assessments item list model
        assessments_model = ItemListModel()
        assessments_model.id = "active_assessments"
        assessments_model.items = wfc_list

        assessments_model.columns = [
            ItemListColumn(
                header=_("Assessment"),
                text=methodcaller('get_text'),
                url=methodcaller('get_url'),
                icon_class=methodcaller('get_icon_class')
            ),
            ItemListColumn(
                header=_("Progress"),
                slug='progress',
                text=methodcaller('get_progress')
            ),
            ItemListColumn(
                header=_("Status"),
                text=methodcaller('get_status')
            ),
            ItemListColumn(
                header=_("Date of audit"),
                text=methodcaller('get_audit_date')
            )
        ]
        assessments_model.hide_bottom_toolbar = True
        if self.closed:
            assessments_model.no_items_text = _("No closed assessments available.")
        else:
            assessments_model.no_items_text = _("No open assessments available.")
        assessments_model.refresh()
        context['assessment_list'] = assessments_model
        context['help_link_url'] = get_help_text_block('certification')
        return context

    def get_agriplace_context(self):
        context, organization = get_assessments_context(self.request, self.kwargs, page_name='my_assessment_list')
        context.update({
            'page_title': _("Certification"),
            'page_title_url': reverse("my_assessment_list", args=[self.request.membership.pk]),
        })
        context['breadcrumbs'] += [{
            'text': _("Assessments")
        }, ]

        paginator = Paginator(
            organization.assessments.order_by("-created_time"),
            PAGINATION_ITEMS_PER_PAGE)
        page = self.request.GET.get('page')
        try:
            assessments = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            assessments = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page
            # of results.
            assessments = paginator.page(paginator.num_pages)

        # Tree view
        context["list"] = {
            "items": assessments,
            "rows": []
        }
        assessment_ids = [x.uuid for x in assessments]
        reusing_assessments = BackgroundEvidenceCopy.objects.filter(
            target_assessment__in=assessment_ids, status="pending"
        ).values_list('target_assessment', flat=True)

        for assessment in assessments:
            context["list"]["rows"].append({
                "cells": [{
                    "icon": ICONS["assessment"],
                    "text": assessment,
                    "url": reverse("my_assessment_detail", args=[self.request.membership.pk, assessment.pk])
                }, {
                    "text": naturaltime(assessment.created_time)
                }]
            })

        # TODO fetch related items, they are accessed in the template
        context['closed_assessments'] = (organization.assessments
                                         .filter(state="closed"))
        context['assessment_icon_class'] = ICONS.get("assessment")

        # Active assessments item list model
        open_assessments_model = ItemListModel()
        open_assessments_model.id = "active_assessments"
        open_assessments_model.items = organization.assessments.filter(state="open", assessment_type__kind="assessment")

        open_assessments_model.columns = [
            ItemListColumn(
                header=_("Assessment"),
                text=lambda assessment: assessment.__unicode__,
                url=lambda assessment: assessment.get_membership_absolute_url(self.request.membership),
                icon_class=lambda assessment: (ICONS["assessment_reuse"] if assessment.uuid in reusing_assessments else ICONS["assessment"])
            ),
            ItemListColumn(
                header=_("Type"),
                text=lambda assessment: assessment.assessment_type.name,
            ),
            ItemListColumn(
                header=_("Crops"),
                text=lambda assessment: assessment.get_product_list
            )
        ]
        open_assessments_model.hide_bottom_toolbar = True
        open_assessments_model.create_url = reverse("my_assessment_create",
                                                    kwargs={"membership_pk": self.request.membership.pk})
        open_assessments_model.create_text = _("Start a new assessment")
        open_assessments_model.no_items_text = _("No open assessments available.")
        open_assessments_model.refresh()
        context['assessment_list'] = open_assessments_model
        context['help_link_url'] = get_help_text_block('certification')

        return context

    def get_agriplace_context_closed(self):
        context, organization = get_assessments_context(self.request, self.kwargs, page_name='my_assessment_list')
        context.update({
            'page_title': _("Certification"),
            'page_title_url': reverse("my_assessment_list", args=[self.request.membership.pk]),
        })
        context['breadcrumbs'] += [
            {
                'text': _("Assessments")
            },
        ]

        # TODO fetch related items, they are accessed in the template
        context['closed_assessments'] = organization.assessments.filter(state='closed',
                                                                        assessment_type__kind="assessment")
        context['assessment_icon_class'] = ICONS.get("assessment")

        # Active assessments item list model
        closed_assessments_model = ItemListModel()
        closed_assessments_model.id = "active_assessments"
        closed_assessments_model.hide_toolbar = True
        closed_assessments_model.items = organization.assessments.filter(state='closed',
                                                                         assessment_type__kind="assessment")

        closed_assessments_model.columns = [
            ItemListColumn(
                header=_("Assessment"),
                text=lambda assessment: assessment.__unicode__,
                url=lambda assessment: assessment.get_membership_absolute_url(self.request.membership),
                icon_class=lambda assessment: ICONS['assessment']
            ),
            ItemListColumn(
                header=_("Type"),
                text=lambda assessment: assessment.assessment_type.name,
            ),
            ItemListColumn(
                header=_("Crops"),
                text=lambda assessment: assessment.get_product_list
            )
        ]
        closed_assessments_model.no_items_text = _("No closed assessments available.")

        closed_assessments_model.hide_bottom_toolbar = True
        closed_assessments_model.refresh()
        context['closed_assessments_model'] = closed_assessments_model

        return context


class AssessmentDetailView(MembershipRequired, TemplateView):
    def get_template_names(self):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return 'assessments/assessment_detail.html'

        return 'farm_group/assessments/detail.html'

    def get_context_data(self, **kwargs):
        context = super(AssessmentDetailView, self).get_context_data(**kwargs)
        ctx, organization, assessment = get_assessment_context(self.request, kwargs, 'my_assessment_detail')
        context.update(ctx)
        assessment_workflow = AssessmentWorkflow.objects.get(assessment=assessment)
        context['wf_context'] = assessment_workflow

        if AGRIPLACE == get_layout_context(self.request):
            return self.get_agriplace_context(context=context, assessment=assessment)
        else:
            return self.get_farm_group_context(context=context, assessment_workflow=assessment_workflow)

    def get_farm_group_context(self, context, assessment_workflow):
        context['assessment_state_display'] = tolerant_ugettext(assessment_workflow.get_assessment_status_display())

        # sections
        if GROWER == self.request.membership.role.name:
            if assessment_workflow.assessment_status in [
                'audit_hzpc',
                'self_managed',
                'internal_audit_open',
                'internal_audit_done'
            ]:
                while 'review' in context["assessment_sections"]:
                    context["assessment_sections"].remove('review')
        if not assessment_workflow.has_privilege(self.request.membership, 'Read', 'Crops app'):
            while 'products' in context["assessment_sections"]:
                context["assessment_sections"].remove('products')

        # Check if we can edit the questions
        can_edit = assessment_workflow.has_privilege(self.request.membership, 'Update', 'Assessment answers')
        context.update({
            'read_only': not can_edit
        })
        return context

    def get_agriplace_context(self, context, assessment):
        context['page_triggers'] += ['my_assessment_detail']
        context['product_name_list'] = []
        for product in assessment.products.all():
            context['product_name_list'].append({
                'name': tolerant_ugettext(product.product_type_name()),
                'area': product.product_area()
            })
        context['assessment_description'] = tolerant_ugettext(assessment.assessment_type.description)
        context['assessment_state_display'] = tolerant_ugettext(assessment.get_state_display())

        result_query = assessment.results
        if result_query.exists():
            context['result'] = assessment.results.latest('created_time')
        # readonly
        context["read_only"] = assessment.state == 'closed'
        return context


class AssessmentQuestionnaireEditView(MembershipRequired, TemplateView):
    def get_template_names(self):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return 'assessments/questionnaire_edit.html'

        return 'farm_group/assessments/questionnaire_edit.html'

    def get_context_data(self, **kwargs):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return self.get_agriplace_context()
        else:
            return self.get_farm_group_context()

    def get_farm_group_context(self):
        context = super(AssessmentQuestionnaireEditView, self).get_context_data(**self.kwargs)
        ctx, organization, assessment = get_assessment_context(self.request, self.kwargs, 'my_questionnaire_edit')
        context.update(ctx)
        wf_context = AssessmentWorkflow.objects.get(assessment=assessment)
        context['wf_context'] = wf_context
        questionnaire = get_object_or_404(Questionnaire, pk=self.kwargs['questionnaire_pk'])
        context['questionnaire'] = questionnaire
        context["page_triggers"] += ["my_assessment_questionnaires"]
        # Check if we can edit the questions
        can_edit = wf_context.has_privilege(self.request.membership, 'Update', 'Assessment answers')
        context.update({
            'read_only': not can_edit
        })
        # sections
        if GROWER in self.request.user.groups.all().values_list('name', flat=True):
            if wf_context.assessment_status in [
                'audit_hzpc',
                'self_managed',
                'internal_audit_open',
                'internal_audit_done'
            ]:
                while 'review' in context["assessment_sections"]:
                    context["assessment_sections"].remove('review')
        if not wf_context.has_privilege(self.request.membership, 'Read', 'Crops app'):
            while 'products' in context["assessment_sections"]:
                context["assessment_sections"].remove('products')
        return context

    def get_agriplace_context(self):
        context, organization, assessment = get_assessment_context(self.request, self.kwargs, 'my_questionnaire_edit')
        questionnaire = get_object_or_404(Questionnaire, pk=self.kwargs['questionnaire_pk'])
        context.update({
            'breadcrumbs': [
                {
                    'text': _("Assessments"),
                    'url': reverse('my_assessment_list', args=[self.request.membership.pk])

                },
                {
                    'text': _("Assessment %s") % assessment.name,
                    'url': reverse('my_assessment_detail', args=[self.request.membership.pk, assessment.pk]),
                }
            ],
            'back_url': reverse('my_assessment_detail', args=[self.request.membership.pk, assessment.pk]),
            'questionnaire': questionnaire,
            'read_only': assessment.state == 'closed'
        })
        # readonly
        context["read_only"] = assessment.state == 'closed'

        context["page_triggers"] += ["my_assessment_questionnaires"]
        return context


class AssessmentQuestionnaireView(MembershipRequired, TemplateView):
    def get_template_names(self):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return 'assessments/assessment_questionnaires.html'

        return 'farm_group/assessments/questionnaires.html'

    def get_context_data(self, **kwargs):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return self.get_agriplace_context()

        return self.get_farm_group_context()

    def get_farm_group_context(self):
        context = super(AssessmentQuestionnaireView, self).get_context_data(**self.kwargs)
        ctx, organization, assessment = get_assessment_context(self.request, self.kwargs,
                                                               'my_assessment_questionnaires')
        questionnaires_query = assessment.assessment_type.questionnaires.select_related('questionnaire_states').all()
        if questionnaires_query.count() == 1:
            return redirect(
                reverse("questionnaire_edit", kwargs={
                    "membership_pk": self.request.membership.pk,
                    "assessment_pk": assessment.pk,
                    "questionnaire_pk": questionnaires_query[0].pk
                })
            )
        context.update(ctx)
        wf_context = AssessmentWorkflow.objects.get(assessment=assessment)
        context['wf_context'] = wf_context
        # sections
        if GROWER in self.request.user.groups.all().values_list('name', flat=True):
            if wf_context.assessment_status in [
                'audit_hzpc',
                'self_managed',
                'internal_audit_open',
                'internal_audit_done'
            ]:
                while 'review' in context["assessment_sections"]:
                    context["assessment_sections"].remove('review')
        # Check if we can edit the questions
        can_edit = wf_context.has_privilege(self.request.membership, 'Update', 'Assessment answers')
        context.update({
            'read_only': not can_edit
        })
        return context

    def get_agriplace_context(self):
        context, organization, assessment = get_assessment_context(
            self.request, self.kwargs, page_name='my_assessment_questionnaires'
        )
        context["page_triggers"] += ["my_assessment_questionnaires"]
        result_query = assessment.results
        if result_query.count():
            context["result"] = assessment.results.latest("created_time")

        questionnaires_query = assessment.assessment_type.questionnaires.select_related('questionnaire_states').all()
        context["read_only"] = assessment.state == 'closed'

        if questionnaires_query.count() == 1:
            return redirect(reverse(
                "questionnaire_edit", kwargs={
                    "membership_pk": self.request.membership.pk,
                    "assessment_pk": assessment.pk,
                    "questionnaire_pk": questionnaires_query[0].pk
                }
            ))
        else:
            return context


class AssessmentReviewView(MembershipRequired, TemplateView):
    def get_template_names(self):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return 'assessments/assessment_review.html'

        return 'farm_group/assessments/review.html'

    def get_context_data(self, **kwargs):
        layout = get_layout_context(self.request)
        if layout == AGRIPLACE:
            return self.get_agriplace_context()

        return self.get_farm_group_context()

    def get_farm_group_context(self):
        context = super(AssessmentReviewView, self).get_context_data(**self.kwargs)
        ctx, organization, assessment = get_assessment_context(
            self.request, self.kwargs, page_name='my_assessment_review'
        )
        context.update(ctx)
        wf_context = AssessmentWorkflow.objects.get(assessment=assessment)
        context['wf_context'] = wf_context
        # sections
        if GROWER in self.request.user.groups.all().values_list('name', flat=True):
            if wf_context.assessment_status in ['audit_hzpc', 'self_managed', 'internal_audit_open',
                                                'internal_audit_done']:
                while 'review' in context["assessment_sections"]:
                    context["assessment_sections"].remove('review')
        # Check if we can edit the questions
        can_edit = wf_context.has_privilege(self.request.membership, 'Update', 'Assessment answers')
        if not wf_context.has_privilege(self.request.membership, 'Read', 'Crops app'):
            while 'products' in context["assessment_sections"]:
                context["assessment_sections"].remove('products')
        context.update({
            'read_only': not can_edit
        })
        return context

    def get_agriplace_context(self):
        context, _, _ = get_assessment_context(self.request, self.kwargs, page_name='my_assessment_review')
        return context


class AssessmentShareWithAuditorView(MembershipRequired, View):
    def get(self, request, *args, **kwargs):
        try:
            ctx, organization, assessment = get_assessment_context(
                request, kwargs, page_name='my_assessment_review'
            )
            result = AssessmentWorkflow.objects.share_with_auditor(request.membership, assessment.pk)
            if not result:
                return HttpResponseBadRequest()

            result.date_shared = datetime.date.today()
            result.save()
            return redirect(reverse('home', args=(request.membership.pk,)))
        except PermissionDenied:
            return HttpResponseForbidden()


class AssessmentShareWithCoordinatorView(MembershipRequired, View):
    def get(self, request, *args, **kwargs):
        try:
            ctx, organization, assessment = get_assessment_context(
                request, kwargs, page_name='my_assessment_review')
            result = AssessmentWorkflow.objects.share_with_coordinator(request.membership, assessment.pk)
            if not result:
                return HttpResponseBadRequest()
        except PermissionDenied:
            return HttpResponseForbidden()
        else:
            return redirect(reverse('home', args=(request.membership.pk,)))


class AssessmentDocumentReviewDoneView(MembershipRequired, View):
    def get(self, request, *args, **kwargs):
        try:
            ctx, organization, assessment = get_assessment_context(
                request, kwargs, page_name='my_assessment_review'
            )
            result = AssessmentWorkflow.objects.perform_document_review(request.membership, assessment.pk)
            if not result:
                return HttpResponseBadRequest()
        except PermissionDenied:
            return HttpResponseForbidden()
        else:
            return redirect(reverse('home', args=(request.membership.pk,)))


class ConfirmJudgementView(MembershipRequired, View):
    def get(self, request, *args, **kwargs):
        try:
            ctx, organization, assessment = get_assessment_context(
                request, kwargs, page_name='my_assessment_review'
            )
            result = AssessmentWorkflow.objects.confirm_judgement(request.user, request.membership, assessment.pk)
            if not result:
                return HttpResponseBadRequest()
            elif result == BLANK_JUDGEMENT_ERROR_MSG:
                messages.add_message(request, messages.ERROR, BLANK_JUDGEMENT_ERROR_MSG)
                return redirect(reverse('my_assessment_review', args=[request.membership.pk, assessment.uuid]))
            else:
                wf_context = AssessmentWorkflow.objects.get(assessment=assessment)
                if wf_context.assessment_status == 'action_required':
                    send_email_for_notification_type(ACTION_REMINDER, wf_context.uuid, request)
                elif wf_context.assessment_status == 'certified':
                    send_email_for_notification_type(OBTAINING_CERTIFICATE, wf_context.uuid, request)

        except PermissionDenied:
            return HttpResponseForbidden()
        else:
            return redirect(reverse('home', args=(request.membership.pk,)))


#
# steps from 1 to 3
#

def create_and_attach_assessment_to_workflow(workflow_context, author, organization):
    try:
        with transaction.atomic():
            if not workflow_context.assessment:
                assessment = Assessment(
                    assessment_type=workflow_context.assessment_type,
                    created_by=author,
                    organization=organization
                )
                assessment.save()
                workflow_context.assign_assessment(assessment)
            else:
                assessment = workflow_context.assessment
            return True, assessment
    except DatabaseError:
        return False, None


@login_required
@membership_required
def create_assessment_from_workflow(request, *args, **kwargs):
    context, organization = get_assessments_context(request, kwargs, page_name='create_assessment_from_workflow')
    try:
        assessment_workflow = AssessmentWorkflow.objects.select_related('assessment_type').get(
            pk=kwargs.get('assessment_workflow_pk', '')
        )
        assessment_type = assessment_workflow.assessment_type
        context['assessment_type'] = assessment_type
    except AssessmentType.DoesNotExist:
        raise Http404('Workflow context does not exists.')

    if assessment_type.code.lower() in ['globalgap_ifa', 'globalgap_ifa_v5', 'gg-v5.1_nl'] and \
                    assessment_workflow.selected_workflow == HZPC:
        agreement = AssessmentAgreement.objects.filter(
            harvest_year=assessment_workflow.harvest_year,
            assessment_type=assessment_type,
            user=request.user
        )
        if not agreement.exists():
            return redirect('assessment-workflow-agreement', request.membership.pk, assessment_workflow.pk)

    created, assessment = create_and_attach_assessment_to_workflow(assessment_workflow, request.user, organization)
    if created:
        return redirect('my_assessment_detail', request.membership.pk, assessment.pk)
    else:
        return redirect('home', request.membership.pk)


@login_required
@membership_required
def assessment_workflow_agreement(request, *args, **kwargs):
    context, organization = get_assessments_context(request, kwargs, page_name='assessment-workflow-agreement')

    try:
        assessment_workflow = AssessmentWorkflow.objects.select_related('assessment_type').get(
            pk=kwargs.get('assessment_workflow_pk')
        )

        assessment_type = assessment_workflow.assessment_type
        context['assessment_type'] = assessment_type
    except AssessmentWorkflow.DoesNotExist:
        raise Http404('Workflow context does not exists.')

    context['elementId'] = 'app-assessment-agreement'
    context['onSuccessUrl'] = reverse(
        'create-assessment-from-workflow', args=[request.membership.pk, assessment_workflow.pk]
    )
    context['onFailUrl'] = reverse('home', args=[request.membership.pk])
    context['workflowContextUUID'] = assessment_workflow.pk

    return render(
        request, 'core/assessments/assessment_workflow_agreement.html', context
    )
