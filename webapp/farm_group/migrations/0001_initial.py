# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import farm_group.models
from django.conf import settings
import json_field.fields
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0013_merge'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('organization', '0014_auto_20170103_0703'),
        ('assessment_workflow', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssessmentAgreement',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('harvest_year', models.IntegerField(default=2017, verbose_name='Harvest year', choices=[(2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018)])),
                ('form_data', json_field.fields.JSONField(default={}, help_text=b'Form data')),
                ('assessment_type', models.ForeignKey(related_name='assessment_agreement', to='assessments.AssessmentType')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'AssessmentAgreement',
                'permissions': (('view_assessment_agreement', 'Can view assessment agreement'),),
            },
        ),
        migrations.CreateModel(
            name='AssessmentMutationLog',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('question_text', models.CharField(max_length=100, verbose_name=b'question_text')),
                ('mutation', models.CharField(max_length=255, verbose_name=b'mutation')),
                ('mutation_date', models.DateTimeField(auto_now_add=True)),
                ('assessment_status', models.CharField(blank=True, max_length=50, verbose_name='Assessment status', choices=[(b'internal_audit_done', 'Internal audit done'), (b'document_review_done', 'Document review done'), (b'audit_done', 'Audit done'), (b'reaudit_required', 'Reaudit required'), (b'action_required', 'Action required'), (b'action_done', 'Action done'), (b'certified', 'Certified'), (b'rejected', 'Rejected')])),
                ('priority', models.PositiveIntegerField(default=400, null=True, db_index=True, blank=True)),
                ('mutation_type', models.PositiveIntegerField(default=1, choices=[(1, 'Assessment'), (2, 'Judgement')])),
                ('assessment', models.ForeignKey(related_name='assessments', to='assessments.Assessment')),
                ('assessment_workflow', models.ForeignKey(to='assessment_workflow.AssessmentWorkflow')),
                ('question', models.ForeignKey(to='assessments.Question', null=True)),
                ('user', models.ForeignKey(related_name='mutation_by_user', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-mutation_date', 'priority'),
                'db_table': 'AssessmentMutationLog',
            },
        ),
    ]
