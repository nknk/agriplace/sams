# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('farm_group', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmentmutationlog',
            name='role',
            field=models.ForeignKey(related_name='mutation_by_role', verbose_name='role', blank=True, to='auth.Group', null=True),
        ),
    ]
