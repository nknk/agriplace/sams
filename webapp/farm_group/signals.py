from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.dispatch import receiver
from django.utils.translation import ugettext as _

from assessment_workflow.models import AssessmentWorkflow
from assessments.models import AssessmentDocumentLink, Answer, Judgement, Question
from farm_group.models import AssessmentMutationLog

# Assessment Logs Priorities

ANSWER_JUSTIFICATION_ADDED = 5
ANSWER_JUSTIFICATION_DELETED = 10
ANSWER_ADDED = 15
ANSWER_DELETED = 20
AGRIFORM_ADDED = 25
AGRIFORM_UPDATED = 30
EVIDENCE_ADDED = 35
EVIDENCE_DELETED = 40
EVIDENCE_UPDATED = 45

# Judgement Logs Priorities

FINDING_ADDED = 50
FINDING_DELETED = 55
AUDIT_JUDGEMENT_ADDED = 60
AUDIT_JUDGEMENT_DELETED = 65
END_JUDGEMENT_ADDED = 70
END_JUDGEMENT_DELETED = 75


def is_eligible_for_assessment_logs(workflow_ctxt):
    if not workflow_ctxt or not AssessmentMutationLog.is_eligible_for_assessment_logs(workflow_ctxt.assessment_status):
        return False
    return True


def is_eligible_for_judgement_logs(workflow_ctxt):
    if not workflow_ctxt or not AssessmentMutationLog.is_eligible_for_judgement_logs(workflow_ctxt.assessment_status):
        return False
    return True


@receiver(models.signals.post_save, sender=AssessmentDocumentLink)
def post_save_assessment_document_link_callback(sender, instance, **kwargs):
    if kwargs.get('raw', True):
        return

    assessment = instance.assessment
    workflow_ctxt = AssessmentWorkflow.get_workflow_by_assessment(assessment)
    if not assessment or not is_eligible_for_assessment_logs(workflow_ctxt):
        return

    created = kwargs.get('created')
    action = _("added") if created else _("updated")
    if hasattr(instance.attachment, 'agriformattachment'):
        priority = AGRIFORM_ADDED if created else AGRIFORM_UPDATED
    else:
        priority = EVIDENCE_ADDED if created else EVIDENCE_UPDATED
    log_mutation_on_attachment(workflow_ctxt, assessment, instance, instance.attachment, action, priority,
                               AssessmentMutationLog.ASSESSMENT_MUTATION)


@receiver(models.signals.post_delete, sender=AssessmentDocumentLink)
def post_delete_assessment_document_link_callback(sender, instance, **kwargs):
    try:
        assessment = instance.assessment
    except:
        return
    workflow_ctxt = AssessmentWorkflow.get_workflow_by_assessment(assessment)
    if not assessment or not is_eligible_for_assessment_logs(workflow_ctxt):
        return

    attachment = ''
    priority = EVIDENCE_DELETED
    if hasattr(instance.attachment, 'textreferenceattachment'):
        attachment = getattr(instance.attachment, 'textreferenceattachment', '')
    elif hasattr(instance.attachment, 'agriformattachment'):
        attachment = getattr(instance.attachment, 'agriformattachment', '')
        priority = AGRIFORM_UPDATED
    elif hasattr(instance.attachment, 'fileattachment'):
        attachment = getattr(instance.attachment, 'fileattachment', '')

    if attachment:
        log_mutation_on_attachment(workflow_ctxt, assessment, instance, attachment, _("deleted"), priority,
                                   AssessmentMutationLog.ASSESSMENT_MUTATION)


@receiver(models.signals.pre_save, sender=Answer)
def pre_save_answer_callback(sender, instance, **kwargs):
    if kwargs.get('raw', True):
        return

    assessment = instance.assessment
    workflow_ctxt = AssessmentWorkflow.get_workflow_by_assessment(assessment)
    if not assessment or not is_eligible_for_assessment_logs(workflow_ctxt):
        return

    not_applicable = _("Not_applicable")  # Don't remove this line. this is used for translating in translation files
    answer_added = _("Answer '{0}' added")
    answer_justification_added = _("Answer justification '{0}' added")

    answer_matched = False
    if getattr(instance, 'prev_answers', None):
        for prev_answer in instance.prev_answers:
            if prev_answer.value == instance.value:
                answer_matched = True
    if instance.value and not answer_matched:
        mutation = answer_added.format(_(instance.value.capitalize()))
        log_mutation(
            workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.ASSESSMENT_MUTATION,
            priority=ANSWER_ADDED
        )

    justification_matched = False
    if getattr(instance, 'prev_answers', None):
        for prev_answer in instance.prev_answers:
            if prev_answer.justification == instance.justification:
                justification_matched = True
    if instance.justification and not justification_matched:
        mutation = answer_justification_added.format(instance.justification)
        log_mutation(
            workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.ASSESSMENT_MUTATION,
            priority=ANSWER_JUSTIFICATION_ADDED
        )


@receiver(models.signals.post_delete, sender=Answer)
def post_delete_answer_callback(sender, instance, **kwargs):
    try:
        assessment = instance.assessment
    except:
        return
    workflow_ctxt = AssessmentWorkflow.get_workflow_by_assessment(assessment)
    if not assessment or not is_eligible_for_assessment_logs(workflow_ctxt):
        return

    answer_deleted = _("Answer '{0}' deleted")
    answer_justification_deleted = _("Answer justification '{0}' deleted")

    answer_matched = False
    if getattr(instance, 'new_answer', None) and instance.new_answer.value == instance.value:
        answer_matched = True
    if instance.value and not answer_matched:
        mutation = answer_deleted.format(_(instance.value.capitalize()))
        log_mutation(
            workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.ASSESSMENT_MUTATION,
            priority=ANSWER_DELETED
        )

    justification_matched = False
    if getattr(instance, 'new_answer', None) and instance.new_answer.justification == instance.justification:
        justification_matched = True

    if instance.justification and not justification_matched:
        mutation = answer_justification_deleted.format(instance.justification)
        log_mutation(
            workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.ASSESSMENT_MUTATION,
            priority=ANSWER_JUSTIFICATION_DELETED
        )


@receiver(models.signals.pre_save, sender=Judgement)
def pre_save_judgement_callback(sender, instance, **kwargs):
    if kwargs.get('raw', True):
        return

    assessment = instance.assessment
    workflow_ctxt = AssessmentWorkflow.get_workflow_by_assessment(assessment)
    if not assessment or not is_eligible_for_judgement_logs(workflow_ctxt):
        return

    audit_judgement_deleted = _("Audit judgement '{0}' deleted")
    audit_judgement_added = _("Audit judgement '{0}' added")
    finding_deleted = _("Finding '{0}' deleted")
    finding_added = _("Finding '{0}' added")

    try:

        judgement = Judgement.objects.get(uuid=instance.pk)

        if judgement and judgement.value != instance.value:
            if judgement.value:
                mutation = audit_judgement_deleted.format(_(judgement.value.capitalize()))
                log_mutation(
                    workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
                    priority=FINDING_DELETED
                )
            if instance.value:
                mutation = audit_judgement_added.format(_(instance.value.capitalize()))
                log_mutation(
                    workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
                    priority=AUDIT_JUDGEMENT_ADDED
                )

        if judgement and judgement.justification != instance.justification:
            if judgement.justification:
                mutation = finding_deleted.format(judgement.justification)
                log_mutation(
                    workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
                    priority=FINDING_DELETED
                )

            if instance.justification:
                mutation = finding_added.format(instance.justification)
                log_mutation(
                    workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
                    priority=FINDING_ADDED
                )

    except ObjectDoesNotExist:
        if instance.value:
            mutation = audit_judgement_added.format(_(instance.value.capitalize()))
            log_mutation(
                workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
                priority=AUDIT_JUDGEMENT_ADDED
            )

        if instance.justification:
            mutation = finding_added.format(instance.justification)
            log_mutation(
                workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
                priority=FINDING_ADDED
            )


@receiver(models.signals.post_delete, sender=Judgement)
def post_delete_judgement_callback(sender, instance, **kwargs):
    try:
        assessment = instance.assessment
    except:
        return
    workflow_ctxt = AssessmentWorkflow.get_workflow_by_assessment(assessment)
    if not assessment or not is_eligible_for_judgement_logs(workflow_ctxt):
        return

    audit_judgement_deleted = _("Audit judgement '{0}' deleted")
    finding_deleted = _("Finding '{0}' deleted")

    if instance.value:
        mutation = audit_judgement_deleted.format(_(instance.value.capitalize()))
        log_mutation(
            workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
            priority=AUDIT_JUDGEMENT_DELETED
        )

    if instance.justification:
        mutation = finding_deleted.format(instance.justification)
        log_mutation(
            workflow_ctxt, assessment, instance, mutation, AssessmentMutationLog.JUDGEMENT_MUTATION,
            priority=FINDING_DELETED
        )


def log_mutation_on_attachment(assessment_workflow, assessment, instance, attachment, action, priority, mutation_type):

    dec = assessment_workflow.get_configuration()

    questions = Question.objects.filter(configurations__configuration=dec,
                                        configurations__document_type=instance.document_type_attachment
                                        ).all().prefetch_related('level_object')


    for question in questions:
        title = question.level_object.title
        code = question.code.split('-').pop()
        code_prefix = question.questionnaire.code
        try:
            mutation = attachment.get_mutation_name(action)
            question_text = u'{0} {1} ({2})'.format(code_prefix, code, title)

            save_mutation_log(
                assessment_workflow,
                assessment,
                question,
                question_text,
                mutation,
                instance.modified_by_user,
                instance.modified_by_role,
                mutation_type,
                priority=priority
            )
        except NotImplementedError:
            pass


def log_mutation(
        assessment_workflow, assessment, instance, mutation, mutation_type,
        priority=AssessmentMutationLog.ACTION_DEFAULT_PRIORITY
):
    question = instance.question

    title = question.level_object.title if question.level_object is not None else ''
    code = question.code.split('-').pop()
    code_prefix = question.questionnaire.code
    question_text = ''

    if not title:
        question_text = '{0}{1}'.format(code_prefix, code)
    else:
        question_text = u'{0} {1} ({2})'.format(code_prefix, code, title)

    save_mutation_log(
        assessment_workflow,
        assessment,
        question,
        question_text,
        mutation,
        instance.modified_by_user,
        instance.modified_by_role,
        mutation_type,
        priority=priority
    )


def save_mutation_log(
        assessment_workflow, assessment, question, question_text, mutation, author, role, mutation_type, priority
):
    mutation_log = AssessmentMutationLog(
        assessment_workflow=assessment_workflow,
        assessment=assessment,
        question=question,
        question_text=question_text,
        mutation=mutation,
        user=author,
        role=role,
        assessment_status=assessment_workflow.assessment_status,
        mutation_type=mutation_type,
        priority=priority
    )

    mutation_log.save()


def get_judgement(wfl_ctxt):
    """
        Get the judgement from the workflow instance.
        This is a work around for confirm_judgement method.
        confirm_judgement changes the judgement to '' if the judgement is negative.
    """
    return _("negative") if wfl_ctxt.assessment_status == 'action_required' and \
                            wfl_ctxt.judgement == '' else _(wfl_ctxt.judgement)
