from datetime import datetime

from django.contrib.auth.models import Group
from factory import DjangoModelFactory
from factory import SubFactory, sequence

from assessments.tests.factories import AssessmentFactory, QuestionFactory
from assessment_types.tests.factories import AssessmentTypeFactory
from assessment_workflow.models import AssessmentWorkflow
from farm_group.models import AssessmentAgreement, AssessmentMutationLog
from organization.tests.factories import OrganizationFactory, UserFactory


class AssessmentAgreementFactory(DjangoModelFactory):
    harvest_year = 2016
    user = 49
    assessment_type = SubFactory(AssessmentTypeFactory)
    form_data = sequence(lambda n: "{\"jaNeeHzpc\": \"jaa\", \"start\": 4, \"confirm\": true}")
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    remarks = sequence(lambda n: "Remarks {}".format(n))

    class Meta:
        model = AssessmentAgreement


class AssessmentWorkflowFactory(DjangoModelFactory):
    harvest_year = datetime.now().year
    shared_with_auditor = None
    author = SubFactory(UserFactory)
    author_organization = SubFactory(OrganizationFactory)
    auditor_user = SubFactory(UserFactory)
    auditor_organization = SubFactory(OrganizationFactory)
    audit_date_planned = None
    assessment_type = SubFactory(AssessmentTypeFactory)
    modified_time = datetime.now()
    audit_preferred_month = None
    configuration = None
    label_configuration = None
    is_test = False
    created_time = datetime.now()
    shared_with_coordinator = None
    workflow_status = "initial"
    remarks = sequence(lambda n: "Remarks {}".format(n))
    audit_date_actual = None
    assessment = SubFactory(AssessmentFactory)
    assessment_status = ""

    class Meta:
        model = AssessmentWorkflow


class GroupFactory(DjangoModelFactory):
    name = sequence(lambda n: "group_name_{}".format(n))

    class Meta:
        model = Group


class AssessmentMutationLogFactory(DjangoModelFactory):
    question_text = sequence(lambda n: "TEST-{}".format(n))
    mutation_date = datetime.now()
    assessment_status = sequence(lambda n: n)  # "internal_audit_done"
    question = SubFactory(QuestionFactory)
    mutation = sequence(lambda n: n)  # "Evidence reference 'test' updated"
    workflow_context = SubFactory(AssessmentWorkflowFactory)
    assessment = SubFactory(AssessmentFactory)
    user = SubFactory(UserFactory)

    class Meta:
        model = AssessmentMutationLog
