from django.contrib.auth.models import User
from django.db import IntegrityError
from django.utils.translation import gettext_lazy as _

from core.constants import GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from organization.models import OrganizationMembership

try:
    from django.apps import apps

    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model

from rbac.models import DataObject, DataObjectState, Grant, GroupRole
from sams.utils import get_layout_context

from urllogin.models import Key
from urllogin.utils import create

BLANK_JUDGEMENT_ERROR_MSG = _(
    "In order to confirm your judgement you first need to select a choice from the "
    "judgement dropdown list below. Please select an option and try again."
)


def get_organization_role(request):
    organization = getattr(request, 'organization', None)
    if organization:
        membership = organization.secondary_organization.first()
        if membership:
            return membership.role
    return None


def get_user_info(request):
    layout = get_layout_context(request)
    if request.user.is_superuser:
        current_user_groups = request.user.groups.values_list("name", flat=True)
    else:
        current_user_groups = []
        role = get_organization_role(request)
        if role:
            current_user_groups = [role.name]

    layout_ctx = {}
    if GROWER in current_user_groups:
        layout_ctx['group'] = GROWER
        layout_ctx['home_template_name'] = 'farm_group/dashboards/grower.html'
    elif INTERNAL_INSPECTOR in current_user_groups:
        layout_ctx['group'] = INTERNAL_INSPECTOR
        layout_ctx['home_template_name'] = 'farm_group/dashboards/auditor.html'
    elif INTERNAL_AUDITOR in current_user_groups:
        layout_ctx['group'] = INTERNAL_AUDITOR
        layout_ctx['home_template_name'] = 'farm_group/dashboards/coordinator.html'
    else:
        layout_ctx['group'] = ""
        layout_ctx['home_template_name'] = "core/home.html"
    layout_ctx.update(
        GROWER=GROWER,
        AUDITOR=INTERNAL_INSPECTOR,
        COORDINATOR=INTERNAL_AUDITOR,
        user_groups=current_user_groups
    )
    layout_ctx['layout_name'] = layout
    return layout_ctx


def get_all_data_objects_by_state(state_name, user_role_ids):
    result = []
    for data_object_name in DataObject.objects.get_data_objects_names_by_state_name(state_name):
        permissions = {
            "name": data_object_name.lower(),
            "actions": set()
        }
        for grant in Grant.objects.filter(
                from_role__id__in=user_role_ids,
                assignment__contains='{"state": "%s", "data_object": "%s"}' % (state_name, data_object_name)
        ).values_list('to_role__slug', flat=True):
            permissions["actions"].add(grant.upper())
        result.append(permissions)
    return result


def get_all_data_objects_per_state(request):
    result = dict()
    if getattr(request, 'membership', None):
        group = request.membership.role
    else:
        group = request.user.groups.all()
    user_role_ids = GroupRole.objects.filter(
        group=group).values_list("role__id", flat=True)

    data_object_state_names = DataObjectState.objects.get_data_object_state_names()
    for state_name in data_object_state_names:
        result[state_name] = get_all_data_objects_by_state(state_name, user_role_ids)
    return result


def create_sso_key(users=None):
    user_count = 0
    users_with_keys = Key.objects.exclude(key__isnull=True).values_list("user__id", flat=True)
    for user in users:
        organizations = user.organizations.all()
        membership_numbers = OrganizationMembership.objects.filter(secondary_organization__in=organizations).exclude(
            membership_number="").values_list('membership_number', flat=True).distinct()
        if len(membership_numbers) == 1 and user.id not in users_with_keys:
            try:
                user_count += 1
                create(user, membership_numbers[0])
            except IntegrityError:
                pass
    return user_count
