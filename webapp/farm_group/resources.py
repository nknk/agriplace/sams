from django.contrib.auth.models import Group

from import_export import resources

from assessment_workflow.resources import UUID_MODEL_FIELDS
from farm_group.models import AssessmentMutationLog
from import_export import fields
from import_export.widgets import ForeignKeyWidget
from django.conf import settings

ASSESSMENT_MUTATION__LOG_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'assessment_workflow', 'assessment', 'question', 'question_text', 'mutation',
    'mutation_date', 'user', 'assessment_status', 'priority', 'mutation_type'
)


# Import/Export resources for assessment mutation log
class AssessmentMutationLogResource(resources.ModelResource):
    user = fields.Field(column_name='user', attribute='user',
                        widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))
    role = fields.Field(column_name='role', attribute='role',
                        widget=ForeignKeyWidget(Group, 'name'))

    class Meta:
        fields = ASSESSMENT_MUTATION__LOG_RESOURCE_FIELDS
        export_order = ASSESSMENT_MUTATION__LOG_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentMutationLog
