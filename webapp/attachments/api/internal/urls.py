from django.conf.urls import patterns, url

from attachments.api.internal.views import (
    AttachmentListCreateView, AttachmentRetrieveUpdateDestroyView, AttachmentListByTypeView, DocumentTypeView,
    AttachmentTypes)

urlpatterns = patterns(
    'attachments.api.internal.views',
    url(
        r'^api-root/$',
        'internal_api_attachments_root',
        name='attachments-app-api-list'
    ),
    url(
        r'types/(?P<attachment_type>.+)/$',
        AttachmentListByTypeView.as_view(),
        name='attachment-type-list'
    ),
    url(
        r'document_types/$',
        DocumentTypeView.as_view(),
        name='document_type_view'
    ),
    url(
        r'attachment_types/$',
        AttachmentTypes.as_view(),
        name='attachment_type_view'
    ),
    url(
        r'(?P<attachment_pk>.\w+)/$',
        AttachmentRetrieveUpdateDestroyView.as_view(),
        name='attachment-retrieve-update-delete'
    ),
    url(
        r'^$',
        AttachmentListCreateView.as_view(),
        name='attachment-list-create'
    )
)
