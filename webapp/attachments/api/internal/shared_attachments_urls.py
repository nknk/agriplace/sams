from django.conf.urls import patterns, url

from attachments.api.internal.views import (
    SharedAttachmentListView,
    SharedAttachmentDetailView, SharedAttachmentListCreateView,
    CopySharedAttachmentToArchiveView, CopySharedAttachmentToAssessmentsView)

urlpatterns = patterns(
    'attachments.api.internal.views',
    url(
        r'^$',
        SharedAttachmentListView.as_view(),
        name='organization-partners'
    ),
    url(
        r'^(?P<sharing_uuid>.\w+)/attachments/$',
        SharedAttachmentDetailView.as_view(),
        name='attachment-shared-by-org'
    ),
    url(
        r'share/$',
        SharedAttachmentListCreateView.as_view(),
        name='attachment-shared'
    ),
    url(
        r'^(?P<sharing_uuid>.\w+)/copy_to_archive/$',
        CopySharedAttachmentToArchiveView.as_view(),
        name='copy-shared-attachment-to-archive'
    ),
    url(
        r'^(?P<sharing_uuid>.\w+)/copy_to_assessments/$',
        CopySharedAttachmentToAssessmentsView.as_view(),
        name='copy-shared-attachment-to-assessment'
    )
)

