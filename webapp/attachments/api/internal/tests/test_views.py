import json

import faker

from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from api.internal.tests.mixins import APITestSetupMixin
from assessment_types.tests.factories import AssessmentTypeFactory
from assessments.tests.factories import QuestionnaireFactory, QuestionFactory
from attachments.models import SharedAttachment, SharedAttachmentList
from farm_group.utils import GROWER
from organization.tests.factories import OrganizationFactory, ProductFactory, OrganizationTypeFactory, \
    OrganizationMembershipFactory

faker = faker.Factory.create()


class TestShareAttachmentAPI(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_farm_groups()
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.auditor = self.create_user(username=faker.user_name(), password=faker.word())

        self.primary_organization = OrganizationFactory.create()
        self.sharing_organization = OrganizationFactory.create(users=(self.user,))
        self.share_with_organization = OrganizationFactory.create(users=(self.user,))

        organization_type = OrganizationTypeFactory.create(name='producer')

        self.sharing_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.sharing_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        self.shared_with_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.share_with_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )

        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.sharing_organization)

        self.sharing_org_assessment = self.create_assessment_with_workflow(
            self.sharing_organization, self.user, self.auditor, self.sharing_membership, self.assessment_type, self.product
        )

        self.shared_org_assessment = self.create_assessment_with_workflow(
            self.share_with_organization, self.user, self.auditor, self.shared_with_membership, self.assessment_type, self.product
        )

        self.document_type = self.create_document_type(assessment_type=self.assessment_type)

        self.questionnaire = QuestionnaireFactory(assessment_type=self.assessment_type)
        self.question = QuestionFactory(questionnaire=self.questionnaire)
        self.question.document_types_attachments.add(self.document_type)

        self.original_attachment = self.create_text_reference_attachment(organization=self.sharing_organization)
        self.document_link = self.create_assessment_document_link(
            user=self.user, assessment=self.sharing_org_assessment,
            attachment=self.original_attachment, document_type=self.document_type
        )

        self.shared_attachment = SharedAttachment.objects.create(
            organization_membership=self.shared_with_membership,
            shared_by_organization=self.sharing_organization,
            permission='can_view'
        )
        self.share_attachment_list = SharedAttachmentList.objects.create(
            shared_attachment=self.shared_attachment, attachment=self.original_attachment
        )

        self.attachments_url = reverse('assessment-attachment-links', kwargs={
            'organization_pk': self.share_with_organization.pk,
            'assessment_pk': self.shared_org_assessment.pk
        })

        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_attachment_shared_with_org_API(self):
        response = self.client.get(
            self.attachments_url,
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)
        self.assertEqual(len(response_content), 0)

        url = reverse('copy-shared-attachment-to-assessment', kwargs={
            "organization_pk": self.share_with_organization.pk,
            "sharing_uuid": self.shared_attachment.pk
        })
        data = [
            {
                "assessment_type_uuid": self.assessment_type.pk,
                "share_attachment_list_uuids": [self.share_attachment_list.pk]
            }
        ]

        response = self.client.post(
            url,
            data=data,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(
            self.attachments_url,
            HTTP_AUTHORIZATION=self.token
        )

        response_content = json.loads(response.content)
        self.assertEqual(len(response_content), 1)
