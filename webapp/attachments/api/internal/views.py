import datetime
from datetime import time
from django.conf import settings
from django.db.models.aggregates import Count

from django.db.models.loading import get_model
from django.db.models.query_utils import Q
from django.utils.translation import ugettext as _
from rest_framework import pagination
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import (
    ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView, UpdateAPIView,
    CreateAPIView)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.utils.urls import replace_query_param
from rest_framework.views import APIView
from assessments.models import AssessmentType, Assessment, Answer
from assessments.utils import create_shared_attachment_copy, get_bucket

from attachments.api.internal.serializers import (
    ATTACHMENT_TYPES, AttachmentSerializer, DocumentTypeShortSerializer,
    SharedAttachmentListSerializaer, SharedAttachmentSerializer, SharedAttachmentWithAttachmentsSerializer)
from attachments.models import Attachment, NotApplicableAttachment, SharedAttachment, DocumentType, SharedAttachmentList
from organization.mixins import OrganizationMixin
from organization.models import Organization
from sams.view_mixins import OrganizationRequired

ARCHIVE_ATTACHMENT_TYPES = [
    {
        "model": get_model('attachments', 'TextReferenceAttachment'),
        "name": _("Text reference"),
    }, {
        "model": get_model('attachments', 'FileAttachment'),
        "name": _("Upload"),
    }, {
        "model": get_model('attachments', 'AgriformAttachment'),
        "name": _("Agriform"),
    },
]

VALID_ATTACHMENT_TYPES = []
for att_type in ARCHIVE_ATTACHMENT_TYPES:
    VALID_ATTACHMENT_TYPES.append(att_type.get('model').__name__.lower())


@api_view(['GET'])
def internal_api_attachments_root(request, format=None):
    """
    Root for Attachments Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'attachment-list-create': reverse('attachment-list-create', request=request, format=format)
    })


class BaseAttachmentView(OrganizationRequired, APIView):
    """
    Base API View to attachment views
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = AttachmentSerializer


class AttachmentPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10

    def get_first_link(self):
        url = self.request.build_absolute_uri()
        return replace_query_param(url, self.page_query_param, 1)

    def get_last_link(self):
        url = self.request.build_absolute_uri()
        return replace_query_param(url, self.page_query_param, self.page.paginator.num_pages)

    def get_paginated_response(self, data):
        return Response({
            'first': self.get_first_link(),
            'last': self.get_last_link(),
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': self.page.paginator.count,
            'results': data
        })


class AttachmentListCreateView(BaseAttachmentView, ListCreateAPIView):
    pagination_class = AttachmentPagination

    def get_queryset(self):

        attachments = Attachment.objects.filter(
            for_deletion_mark=False, organization=self.request.organization
        ).exclude(
            uuid__in=NotApplicableAttachment.objects.all()
        ).select_subclasses()
        modified_range_start = self.request.query_params.get('modified_range_start')
        modified_range_end = self.request.query_params.get('modified_range_end')
        title = self.request.query_params.get('title')
        used_in_standard = self.request.query_params.get('used_in_standard')
        usable_for_standard = self.request.query_params.get('usable_for_standard')
        attachment_type = self.request.query_params.get('attachment_type')
        evidence_type = self.request.query_params.get('evidence_type')
        include_optional_evidence = self.request.query_params.get('include_optional_evidence')
        try:
            include_optional_evidence = int(include_optional_evidence)
        except:
            include_optional_evidence = 0

        if modified_range_start and modified_range_end:
            modified_range_end = datetime.datetime.strptime(modified_range_end, "%Y-%m-%d")
            modified_range_end = modified_range_end + datetime.timedelta(days=1)
            attachments = attachments.filter(modified_time__range=[modified_range_start, modified_range_end])
        elif modified_range_start:
            attachments = attachments.filter(modified_time__gte=modified_range_start)
        elif modified_range_end:
            modified_range_end = datetime.datetime.strptime(modified_range_end, "%Y-%m-%d")
            modified_range_end = datetime.datetime.combine(modified_range_end, time.max)
            attachments = attachments.filter(modified_time__lte=modified_range_end)
        if title:
            attachments = attachments.filter(
                (Q(title__icontains=title) & Q(agriformattachment__agriform_reference__isnull=True)) |
                (
                    Q(agriformattachment__assessment__assessment_type__name__icontains=title) &
                    Q(agriformattachment__agriform_reference__isnull=True)
                ) |
                (
                    Q(agriformattachment__agriform_reference__icontains=title)
                )
            )
        if used_in_standard:
            attachments = attachments.filter(
                assessment_links_attachments__assessment__assessment_type__uuid=used_in_standard
            )
        if usable_for_standard:
            attachments = attachments.filter(
                assessment_links_attachments__document_type_attachment__questions_attachments__questionnaire__assessment_type__uuid=usable_for_standard
            )
        if not include_optional_evidence:
            attachments = attachments.filter(
                Q(assessment_links_attachments__isnull=True) |
                Q(assessment_links_attachments__document_type_attachment__is_optional=False)
            )
        if attachment_type and attachment_type in VALID_ATTACHMENT_TYPES:
            attachment_type_filter = "{}__isnull".format(attachment_type)
            attachments = attachments.exclude(**{attachment_type_filter: True})

        if evidence_type:
            attachments = attachments.filter(
                assessment_links_attachments__document_type_attachment__uuid=evidence_type
            )

        attachments = attachments.distinct().order_by('-modified_time')

        return attachments

    def post(self, request, *args, **kwargs):
        if not (request.data and request.data['attachment_type']):
            return Response(status.HTTP_400_BAD_REQUEST)
        else:
            result = save_attachment_data(data=request.data)
        if result['success']:
            return Response(get_attachment_data(result['success']), status.HTTP_200_OK)
        else:
            return Response(result['errors'], status.HTTP_400_BAD_REQUEST)


class AttachmentListByTypeView(BaseAttachmentView, ListAPIView):
    def get_attachment_type(self):
        attachment_type = self.kwargs.get('attachment_type', '')
        attachment_model = ATTACHMENT_TYPES.get(attachment_type, '')
        return attachment_model

    def get_queryset(self):
        organization = self.request.organization
        attachment_model = self.get_attachment_type()

        if attachment_model:
            attachments = attachment_model['model'].objects.filter(
                for_deletion_mark=False, organization=organization
            ).order_by('-modified_time')
        else:
            return Response(status.HTTP_400_BAD_REQUEST)

        return attachments


class AttachmentRetrieveUpdateDestroyView(BaseAttachmentView, RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        attachment_pk = self.kwargs['attachment_pk']

        attachment = Attachment.objects.filter(
            for_deletion_mark=False, organization=self.request.organization
        ).get_subclass(
            uuid=attachment_pk
        )
        return attachment

    def get(self, request, *args, **kwargs):
        try:
            attachment = self.get_queryset()
            return Response(get_attachment_data(attachment))

        except Attachment.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def put(self, request, *args, **kwargs):
        try:
            attachment = self.get_queryset()

            if attachment.is_frozen is True:
                return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
            else:
                attachment.title = request.data['title']
                attachment.save()
                return Response(get_attachment_data(attachment), status.HTTP_200_OK)

        except Attachment.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, *args, **kwargs):
        try:
            attachment = self.get_queryset()

            if attachment.is_frozen is True:
                return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)
            else:
                attachment.for_deletion_mark = True
                attachment.save()
                return Response(get_attachment_data(attachment), status.HTTP_200_OK)

        except Attachment.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


def get_attachment_data(attachment):
    return ATTACHMENT_TYPES[attachment.get_attachment_type()]['serializer'](attachment).data


def save_attachment_data(data, attachment=False):
    if attachment:
        attachment_type = ATTACHMENT_TYPES[attachment.get_attachment_type()]
        serializer = attachment_type['serializer'](attachment, data=data, partial=True)
    else:
        attachment_type = ATTACHMENT_TYPES[data['attachment_type']]
        attachment = create_attachment_object(data, data['organization'])
        serializer = attachment_type['serializer'](data=attachment)

    if serializer.is_valid():
        try:
            result = serializer.save()
            return {
                'success': result,
                'errors': []
            }
        except Exception as ex:
            return {
                'success': False,
                'errors': [{'exception': ex, 'detail': ex}]
            }
    else:
        return {
            'success': False,
            'errors': serializer.errors
        }


def create_attachment_object(not_saved_attachment, current_organization_uuid):
    attachment = None
    current_organization = Organization.objects.get(pk=current_organization_uuid)

    attachment_type_name = not_saved_attachment['attachment_type']
    if attachment_type_name == "AgriformAttachment":
        attachment = {
            'title': not_saved_attachment['title'],
            'organization': current_organization.uuid,
            'agriformUuid': not_saved_attachment['agriformUuid']
        }

    elif attachment_type_name == "FormAttachment":
        attachment = {
            'title': not_saved_attachment['title'],
            'organization': current_organization.uuid,
            'form_data': not_saved_attachment['form_data']
        }

    elif attachment_type_name == "TextReferenceAttachment":
        attachment = {
            'title': not_saved_attachment['title'],
            'description': not_saved_attachment['title'],
            'organization': current_organization.uuid
        }

    elif attachment_type_name == "NotApplicableAttachment":
        attachment = {
            'title': not_saved_attachment['title'],
            'organization': current_organization.uuid
        }

    elif attachment_type_name == "FileAttachment":
        attachment = {
            'attachment_type': "FileAttachment",
            'title': not_saved_attachment['title'],
            'file': not_saved_attachment['file'],
            'original_file_name': not_saved_attachment['original_file_name'],
            'organization': current_organization.uuid
        }

        if not_saved_attachment['expiration_date']:
            attachment['expiration_date'] = not_saved_attachment['expiration_date']

    return attachment


class DocumentTypeView(ListAPIView):
    serializer_class = DocumentTypeShortSerializer
    queryset = DocumentType.objects.all()


class AttachmentTypes(APIView):
    def get(self, request, *args, **kwargs):
        data = []
        for attachment_type in ARCHIVE_ATTACHMENT_TYPES:
            data.append({
                "uuid": attachment_type.get('model').__name__.lower(),
                "name": _(attachment_type.get('name'))
            })
        return Response(data)


class SharedAttachmentBaseView(OrganizationMixin, APIView):
    """
    Shared Attachment Base view

    """
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, shared_attachment):
        return {
            "shared_attachment_uuid": shared_attachment.pk,
            "attachment_uuid": shared_attachment.attachment.pk,
            "organization_uuid": shared_attachment.organization_membership.secondary_organization.pk,
            "organization_name": shared_attachment.organization_membership.secondary_organization.name,
            "organization_slug": shared_attachment.organization_membership.secondary_organization.slug,
            "organization_city": shared_attachment.organization_membership.secondary_organization.mailing_city,
            "organization_type": shared_attachment.organization_membership.organization_type.name,
            "permission": shared_attachment.permission
        }


class SharedAttachmentListView(OrganizationMixin, ListAPIView):
    """
    Return the list of organization that has shared any attachment with given organization.
    """
    serializer_class = SharedAttachmentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        curr = self.get_organization()
        shared_attachment = SharedAttachment.objects.filter(
            organization_membership__secondary_organization=curr
        ).select_related('organization_membership__secondary_organization')

        return shared_attachment


class SharedAttachmentDetailView(SharedAttachmentBaseView, ListAPIView):

    serializer_class = SharedAttachmentWithAttachmentsSerializer

    def get(self, request, *args, **kwargs):
        sharing_uuid = kwargs.get('sharing_uuid')
        self.queryset = SharedAttachment.objects.filter(uuid=sharing_uuid)

        return super(SharedAttachmentDetailView, self).get(request, *args, **kwargs)


class SharedAttachmentListCreateView(SharedAttachmentBaseView, ListCreateAPIView):
    from organization.api.internal.serializers import OrganizationMembershipModelSerializer

    serializer_class = OrganizationMembershipModelSerializer

    def get_queryset(self):
        from organization.models import OrganizationMembership

        attachment_uuids = self.request.query_params.get('attachment_uuids', '')
        current_org = self.get_organization()
        attachment_uuids = attachment_uuids.split(",") if attachment_uuids else []
        
        share_attachments = SharedAttachmentList.objects.filter(
            attachment__uuid__in=attachment_uuids,
            shared_attachment__shared_by_organization=current_org,
        ).distinct()
        share_attachments = share_attachments.values('shared_attachment').annotate(
            attachments_count=Count('attachment')
        ).filter(attachments_count=len(attachment_uuids))

        shares = [share['shared_attachment'] for share in share_attachments]

        share_with_organizations = OrganizationMembership.objects.filter(
            shared_organization_memberships__uuid__in=shares,
        ).distinct()

        return share_with_organizations

    def post(self, request, *args, **kwargs):
        current_org = self.get_organization()
        attachment_uuids = request.data.get('attachment_uuids', '')
        if attachment_uuids and isinstance(attachment_uuids, basestring):
            attachment_uuids = attachment_uuids.split(",")
        membership_uuids = request.data.get('membership_uuids', '')
        if membership_uuids and isinstance(membership_uuids, basestring):
            membership_uuids = membership_uuids.split(",")

        attachments = Attachment.objects.filter(uuid__in=attachment_uuids)
        if len(attachments) != len(attachment_uuids):
            response_status = status.HTTP_404_NOT_FOUND
            result = {"success": False, "errors": "Some attachments don't exist."}
        else:
            SharedAttachment.objects.get_or_create_shared_attachments(
                current_org,
                attachments,
                membership_uuids=membership_uuids
            )
            response_status = status.HTTP_200_OK
            result = {"success": True, "errors": []}

        return Response(result, status=response_status)


class CopySharedAttachmentToArchiveView(SharedAttachmentBaseView, CreateAPIView):

    serializer_class = SharedAttachmentListSerializaer
    queryset = SharedAttachment.objects.none()

    def post(self, request, *args, **kwargs):
        files_not_copied = []
        result = {"success": True, "errors": []}
        response_status = status.HTTP_200_OK
        curr_org = self.get_organization()

        data = self.request.data

        share_attachment_list_uuids = data.get('share_attachment_list_uuids')
        if isinstance(share_attachment_list_uuids, basestring):
            share_attachment_list_uuids = share_attachment_list_uuids.split(',')
        if not share_attachment_list_uuids:
            result["success"] = False
            result["errors"].append("Select at least one attachment.")
            response_status = status.HTTP_404_NOT_FOUND
        else:
            share_attachment_list = SharedAttachmentList.objects.filter(uuid__in=share_attachment_list_uuids)
            bucket = get_bucket()
            shared_attachment = None
            for obj in share_attachment_list:
                shared_attachment = obj.shared_attachment
                if not obj.attachment_copy:
                    try:
                        attachment_copy = create_shared_attachment_copy(
                            obj.attachment, curr_org, request.user, target_assessment=None, bucket=bucket
                        )
                        obj.attachment_copy = attachment_copy
                        obj.save()
                    except:
                        files_not_copied.append(obj.attachment.title)
            if shared_attachment:
                shared_attachment.state = 'processed'
                shared_attachment.remarks = ', '.join(files_not_copied)
                shared_attachment.save()

            result = {"success": True, "errors": []}

        return Response(result, status=response_status)


class CopySharedAttachmentToAssessmentsView(SharedAttachmentBaseView, CreateAPIView):

    serializer_class = SharedAttachmentListSerializaer
    queryset = SharedAttachment.objects.none()

    def post(self, request, *args, **kwargs):
        result = {"success": True, "errors": []}
        response_status = status.HTTP_200_OK

        data = self.request.data
        curr_org = self.get_organization()
        bucket = get_bucket()
        files_not_copied = dict()
        share_attachment = None
        for record in data:
            assessment_type_uuid = record.get('assessment_type_uuid')
            share_attachment_list_uuids = record.get('share_attachment_list_uuids')

            share_attachment_list = SharedAttachmentList.objects.filter(uuid__in=share_attachment_list_uuids)
            assessment_type = AssessmentType.objects.get(uuid=assessment_type_uuid)
            if share_attachment_list and not share_attachment:
                share_attachment = share_attachment_list[0].shared_attachment
            self.attach_to_assessment(
                share_attachment_list, assessment_type, curr_org, request.user, bucket, files_not_copied
            )
        if share_attachment:
            share_attachment.state = 'processed'
            share_attachment.remarks = ','.join(files_not_copied.values())
            share_attachment.save()

        return Response(result, status=response_status)

    def get_attachment_copy(self, share_attachment_list_obj, curr_org, user, bucket):
        if share_attachment_list_obj.attachment_copy:
            attachment_copy = share_attachment_list_obj.attachment_copy
        else:
            try:
                attachment_copy = create_shared_attachment_copy(
                    share_attachment_list_obj.attachment, curr_org, user, target_assessment=None, bucket=bucket
                )
                share_attachment_list_obj.attachment_copy = attachment_copy
                share_attachment_list_obj.save()
            except:
                attachment_copy = None

        return attachment_copy

    def attach_to_assessment(self, share_attachment_list, assessment_type, curr_org, user, bucket, files_not_copied):
        from assessments.models import AssessmentDocumentLink

        for obj in share_attachment_list:
            attachment_copy = self.get_attachment_copy(obj, curr_org, user, bucket)
            if not attachment_copy:
                files_not_copied[obj.attachment.uuid] = obj.attachment.title
                continue
            # Get the document types this attachment was being used in.
            document_types = DocumentType.objects.linked_to_attachment(obj.attachment)
            assessments = Assessment.objects.get_open_assessments_of_type(curr_org, assessment_type)
            for assessment in assessments:
                assessment_doc_types = assessment.assessment_type.get_document_types().values_list("uuid", flat=True)
                for document_type in document_types:
                    if document_type.uuid not in assessment_doc_types:
                        continue

                    update_answer = True

                    # If these attachment was already copied to archive.
                    existing_links_with_no_assessments = AssessmentDocumentLink.objects.filter(
                        assessment=None, attachment=attachment_copy, organization=curr_org,
                        document_type_attachment=document_type).first()
                    if existing_links_with_no_assessments:
                        existing_links_with_no_assessments.assessment = assessment
                        existing_links_with_no_assessments.save()
                    else:
                        # Check if already linked.
                        existing_links = AssessmentDocumentLink.objects.filter(
                            assessment=assessment, attachment=attachment_copy, organization=curr_org,
                            document_type_attachment=document_type).exists()
                        if not existing_links:
                            old_link = AssessmentDocumentLink.objects.filter(
                                assessment__assessment_type=assessment.assessment_type,
                                attachment=obj.attachment,
                                document_type_attachment=document_type
                            ).order_by('-assessment__modified_time').first()
                            AssessmentDocumentLink.objects.create(
                                assessment=assessment, attachment=attachment_copy,
                                author=user, document_type_attachment=document_type, organization=curr_org,
                                timestamp=datetime.datetime.today(),
                                is_done=old_link.is_done if old_link else False
                            )
                        else:
                            update_answer = False
                    if update_answer:
                        self.answer_questions(assessment, document_type, user)

    def answer_questions(self, assessment, document_type, user):
        questions = assessment.get_question_covered_by_doc_type(document_type)
        for question in questions:
            answers = Answer.objects.filter(
                assessment=assessment,
                question=question,
            )
            if answers:
                answers.update(value="yes", justification="See attachment")
            else:
                Answer.objects.create(
                    assessment=assessment,
                    question=question,
                    author=user,
                    value="yes",
                    justification="See attachment"
                )
