from django.utils.translation import ugettext as _

from assessments.api.internal.fields import DateField

try:
    from django.apps import apps

    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model

from rest_framework import serializers

from attachments.models import (
    Attachment,
    FileAttachment,
    TextReferenceAttachment,
    AgriformAttachment,
    FormAttachment,
    NotApplicableAttachment,
    DocumentType,
    SharedAttachment, SharedAttachmentList)
from assessments.models import AssessmentDocumentLink, QuestionLevel, Assessment
from attachments.fields import FileField
from translations.fields import TranslatableTextField
from form_builder.api.internal.serializers import GenericFormDataSerializer, GenericFormSerializer


class AssessmentDataAttachmentSerializer(serializers.Serializer):
    attachment_type = serializers.CharField(source='get_attachment_type', read_only=True)

    def to_representation(self, obj):
        child_object = Attachment.objects.get_subclass(pk=obj.pk)
        if isinstance(child_object, FileAttachment):
            return AssessmentDataFileSerializer(child_object).data
        if isinstance(child_object, TextReferenceAttachment):
            return AssessmentDataTextReferenceSerializer(child_object).data
        if isinstance(child_object, AgriformAttachment):
            return AssessmentDataAgriformSerializer(child_object).data
        if isinstance(child_object, FormAttachment):
            return AssessmentDataFormSerializer(child_object).data
        if isinstance(child_object, NotApplicableAttachment):
            return AssessmentDataNotApplicableSerializer(child_object).data
        return super(AssessmentDataAttachmentSerializer, self).to_native(obj)

    def to_internal_value(self, data):
        if 'uuid' in data:
            attachment = Attachment.objects.get_subclass(uuid=data['uuid'])
        else:
            serializer = ATTACHMENT_TYPES[data['attachment_type']]['serializer'](data=data)
            if serializer.is_valid():
                attachment = serializer.save()
            else:
                raise serializers.ValidationError(serializer.errors)
        return attachment


class AttachmentSerializer(AssessmentDataAttachmentSerializer):

    def to_representation(self, obj):
        child_object = Attachment.objects.get_subclass(pk=obj.pk)
        if isinstance(child_object, FileAttachment):
            return FileSerializer(child_object).data
        if isinstance(child_object, TextReferenceAttachment):
            return TextReferenceSerializer(child_object).data
        if isinstance(child_object, AgriformAttachment):
            return AgriformSerializer(child_object).data
        if isinstance(child_object, FormAttachment):
            return FormSerializer(child_object).data
        if isinstance(child_object, NotApplicableAttachment):
            return NotApplicableSerializer(child_object).data
        return super(AttachmentSerializer, self).to_native(obj)


class AssessmentDataFileSerializer(serializers.ModelSerializer):
    attachment_type = serializers.CharField(source='get_attachment_type', read_only=True)
    file = FileField()
    expiration_date = DateField(required=False, allow_null=True)
    modified_time = DateField(read_only=True, required=False)
    is_frozen = serializers.BooleanField(read_only=True)

    class Meta:
        model = FileAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'file',
                  'original_file_name',
                  'expiration_date',
                  'attachment_type',
                  'modified_time',
                  'is_frozen'
                  )

    def get_identity(self, data):
        try:
            return data.get('uuid', None)
        except AttributeError:
            return None


class FileSerializer(AssessmentDataFileSerializer):
    used_in_document_types = serializers.JSONField(read_only=True, required=False)
    usable_for_assessments = serializers.JSONField(read_only=True, required=False)
    used_in = serializers.CharField(source='used_in_document_types', read_only=True)
    used_in_assessments = serializers.JSONField(read_only=True, required=False)

    class Meta:
        model = FileAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'file',
                  'original_file_name',
                  'expiration_date',
                  'attachment_type',
                  'modified_time',
                  'used_in',
                  'used_in_assessments',
                  'used_in_document_types',
                  'usable_for_assessments',
                  'is_frozen'
                  )


class AssessmentDataTextReferenceSerializer(serializers.ModelSerializer):
    attachment_type = serializers.CharField(source='get_attachment_type', read_only=True)
    modified_time = DateField(read_only=True, required=False)
    is_frozen = serializers.BooleanField(read_only=True)

    class Meta:
        model = TextReferenceAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'description',
                  'attachment_type',
                  'modified_time',
                  'is_frozen'
                  )


class TextReferenceSerializer(AssessmentDataTextReferenceSerializer):
    used_in_document_types = serializers.JSONField(read_only=True, required=False)
    usable_for_assessments = serializers.JSONField(read_only=True, required=False)
    used_in_assessments = serializers.JSONField(read_only=True, required=False)

    class Meta:
        model = TextReferenceAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'description',
                  'attachment_type',
                  'modified_time',
                  'used_in_assessments',
                  'used_in_document_types',
                  'usable_for_assessments',
                  'is_frozen'
                  )


class AssessmentDataNotApplicableSerializer(serializers.ModelSerializer):
    attachment_type = serializers.CharField(source='get_attachment_type', read_only=True)
    modified_time = DateField(read_only=True, required=False)
    is_frozen = serializers.BooleanField(read_only=True)

    class Meta:
        model = NotApplicableAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'attachment_type',
                  'modified_time',
                  'is_frozen'
                  )


class NotApplicableSerializer(AssessmentDataNotApplicableSerializer):
    used_in_document_types = serializers.JSONField(read_only=True, required=False)
    usable_for_assessments = serializers.JSONField(read_only=True, required=False)
    used_in_assessments = serializers.JSONField(read_only=True, required=False)

    class Meta:
        model = NotApplicableAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'attachment_type',
                  'modified_time',
                  'used_in_assessments',
                  'used_in_document_types',
                  'usable_for_assessments',
                  'is_frozen'
                  )


class AssessmentDataAgriformSerializer(serializers.ModelSerializer):
    attachment_type = serializers.CharField(source='get_attachment_type', read_only=True)
    title = serializers.CharField(source='get_agriform_title', read_only=True)
    agriformUuid = serializers.PrimaryKeyRelatedField(
        source='assessment',
        many=False,
        queryset=Assessment.objects.filter(assessment_type__kind='agriform')
    )
    modified_time = DateField(read_only=True, required=False)
    is_frozen = serializers.BooleanField(read_only=True)

    class Meta:
        model = AgriformAttachment
        fields = ('uuid',
                  'title',
                  'agriform_reference',
                  'organization',
                  'agriformUuid',
                  'attachment_type',
                  'modified_time',
                  'is_frozen'
                  )


class AgriformSerializer(AssessmentDataAgriformSerializer):
    used_in_document_types = serializers.JSONField(read_only=True, required=False)
    usable_for_assessments = serializers.JSONField(read_only=True, required=False)
    used_in_assessments = serializers.JSONField(read_only=True, required=False)

    class Meta:
        model = AgriformAttachment
        fields = ('uuid',
                  'title',
                  'agriform_reference',
                  'organization',
                  'agriformUuid',
                  'attachment_type',
                  'modified_time',
                  'used_in_assessments',
                  'used_in_document_types',
                  'usable_for_assessments',
                  'is_frozen'
                  )


class AssessmentDataFormSerializer(serializers.ModelSerializer):
    attachment_type = serializers.CharField(source='get_attachment_type', read_only=True)
    modified_time = DateField(read_only=True, required=False)
    is_frozen = serializers.BooleanField(read_only=True)

    class Meta:
        model = FormAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'attachment_type',
                  'modified_time',
                  'is_frozen'
                  )


class FormSerializer(AssessmentDataFormSerializer):
    used_in_document_types = serializers.JSONField(read_only=True, required=False)
    usable_for_assessments = serializers.JSONField(read_only=True, required=False)
    used_in_assessments = serializers.JSONField(read_only=True, required=False)

    class Meta:
        model = FormAttachment
        fields = ('uuid',
                  'title',
                  'organization',
                  'attachment_type',
                  'modified_time',
                  'used_in_assessments',
                  'used_in_document_types',
                  'usable_for_assessments',
                  'is_frozen'
                  )


ATTACHMENT_TYPES = {
    'FileAttachment': {'model': get_model('attachments', 'FileAttachment'), 'serializer': FileSerializer},
    'TextReferenceAttachment': {'model': get_model('attachments', 'TextReferenceAttachment'),
                                'serializer': TextReferenceSerializer},
    'AgriformAttachment': {'model': get_model('attachments', 'AgriformAttachment'), 'serializer': AgriformSerializer},
    'FormAttachment': {'model': get_model('attachments', 'FormAttachment'), 'serializer': FormSerializer},
    'NotApplicableAttachment': {'model': get_model('attachments', 'NotApplicableAttachment'),
                                'serializer': NotApplicableSerializer}
}


class DocumentTypeSerializer(serializers.ModelSerializer):
    template = serializers.SerializerMethodField()
    level_object_dict = serializers.SerializerMethodField('get_level')
    name = TranslatableTextField()
    description = TranslatableTextField()

    def get_default_fields(self):
        translated_fields = {
            'name': serializers.CharField(),
            'description': serializers.CharField(),
        }
        return dict(super(DocumentTypeSerializer, self).get_default_fields().items() + translated_fields.items())

    def get_level(self, obj):
        return obj.get_level()

    def get_template(self, obj):
        help_documents = obj.help_documents.all()  # from separate HelpDocument model
        if help_documents:
            result = dict()
            for doc in help_documents:
                if doc.language in result.keys():
                    result[doc.language].append(doc.help_document.url)
                else:
                    result[doc.language] = [doc.help_document.url]
            return result
        else:
            template = obj.get_template()  # from DocumentType property
            if template:
                return {"*": template}
            else:
                return dict()

    class Meta:
        model = DocumentType
        fields = (
            'uuid',
            'code',
            'name',
            'assessment_type',
            'template',
            'description',
            'level_object_dict',
            'assessment_types_where_optional',
            'is_optional'
        )


class DocumentTypeShortSerializer(serializers.ModelSerializer):
    name = TranslatableTextField()

    class Meta:
        model = DocumentType
        fields = (
            'uuid',
            'code',
            'name',
            'assessment_type',
            'is_optional'
        )


class QuestionLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionLevel


class AssessmentDocumentTypeSerializer(serializers.HyperlinkedModelSerializer):
    """
    DocumentType of assessment, with attachments collection
    """
    attachments = serializers.SerializerMethodField('get_attachments')
    level = serializers.SerializerMethodField('get_level')
    template = AttachmentSerializer()

    class Meta:
        model = DocumentType
        fields = (
            'uuid', 'code',
            'name', 'description',
            'template', 'attachments', 'level'
        )

    def __init__(self, assessment_pk, *args, **kwargs):
        self.assessment_pk = assessment_pk
        super(AssessmentDocumentTypeSerializer, self).__init__(*args, **kwargs)

    def get_attachments(self, obj):
        attachments_links = AssessmentDocumentLink.objects.filter(
            assessment__pk=self.assessment_pk, document_type_attachment=obj)
        attachments = [link.attachment for link in attachments_links]
        result = AttachmentSerializer(
            attachments, many=True).data
        return result

    def get_level(self, obj):
        return obj.get_level()


class DocumentSimpleSerializer(serializers.ModelSerializer):
    """
    Simple Document serializer
    """

    class Meta:
        model = Attachment
        fields = ('uuid', 'title')
        read_only_fields = ('title',)

    def __init__(self, *args, **kwargs):
        self.answer = kwargs.pop('answer', None)
        super(DocumentSimpleSerializer, self).__init__(*args, **kwargs)

    def get_identity(self, data):
        """
        Override the pk attribute for bulk update.
        """
        try:
            return data.get('uuid', None)
        except AttributeError:
            return None


class SharedAttachmentSerializer(serializers.ModelSerializer):
    shared_with_organization = serializers.SerializerMethodField()
    sharing_date = serializers.SerializerMethodField(read_only=True)
    sharing_organization = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()

    class Meta:
        model = SharedAttachment
        fields = (
            'uuid', 'sharing_organization', 'shared_with_organization', 'sharing_date', 'state', 'permission', 'remarks'
        )

    def get_state(self, obj):
        return _(obj.get_state_display())

    def get_sharing_date(self, obj):
        return obj.created_time.date()

    def get_shared_with_organization(self, obj):
        from organization.api.internal.serializers import OrganizationSerializer
        shared_with_organization = OrganizationSerializer(obj.organization_membership.secondary_organization).data
        return shared_with_organization

    def get_sharing_organization(self, obj):
        from organization.api.internal.serializers import OrganizationSerializer
        sharing_organization = OrganizationSerializer(obj.shared_by_organization).data
        return sharing_organization


class SharedAttachmentListSerializaer(serializers.ModelSerializer):
    attachment = AttachmentSerializer(read_only=True)
    share_attachment_list_uuids = serializers.ListField(child=serializers.CharField(), write_only=True)
    attachment_copy = serializers.CharField(source='attachment_copy.uuid', read_only=True)

    class Meta:
        model = SharedAttachmentList
        fields = (
            'uuid', 'attachment', 'share_attachment_list_uuids',
            'attachment_copy'
        )


class SharedAttachmentWithAttachmentsSerializer(serializers.ModelSerializer):

    shared_with_organization = serializers.SerializerMethodField()
    sharing_date = serializers.SerializerMethodField(read_only=True)
    sharing_organization = serializers.SerializerMethodField()
    attachments = serializers.SerializerMethodField()

    class Meta:
        model = SharedAttachment
        fields = (
            'uuid', 'sharing_organization', 'shared_with_organization', 'sharing_date', 'state', 'permission', 'attachments'
        )

    def get_sharing_date(self, obj):
        return obj.created_time.date()

    def get_shared_with_organization(self, obj):
        from organization.api.internal.serializers import OrganizationSerializer
        shared_with_organization = OrganizationSerializer(obj.organization_membership.secondary_organization).data
        return shared_with_organization

    def get_sharing_organization(self, obj):
        from organization.api.internal.serializers import OrganizationSerializer
        sharing_organization = OrganizationSerializer(obj.shared_by_organization).data
        return sharing_organization

    def get_attachments(self, obj):

        attachments = SharedAttachmentListSerializaer(obj.shared_attachments.all(), many=True).data
        return attachments

