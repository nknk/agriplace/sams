from django import forms
from django.db.models import Q
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import ugettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportActionModelAdmin
from import_export.widgets import ForeignKeyWidget
from import_export import fields

from core.admin import BaseAdmin, TranslationMixin
from .models import *

admin.site.register(Attachment)
admin.site.register(HelpDocument)

DOCUMENT_TYPE_RESOURCE_FIELDS = ('uuid', 'code', 'name', 'description', 'assessment_type', 'is_optional')


class FileAttachmentAdmin(BaseAdmin):
    pass


admin.site.register(FileAttachment, FileAttachmentAdmin)


class TextReferenceAttachmentAdmin(BaseAdmin):
    pass


admin.site.register(TextReferenceAttachment, TextReferenceAttachmentAdmin)


class AgriformAttachmentAdmin(BaseAdmin):
    pass


admin.site.register(AgriformAttachment, AgriformAttachmentAdmin)


class FormAttachmentAdmin(BaseAdmin):
    pass


admin.site.register(FormAttachment, FormAttachmentAdmin)


class NotApplicableAttachmentAdmin(BaseAdmin):
    pass


admin.site.register(NotApplicableAttachment, NotApplicableAttachmentAdmin)


class DocumentTypeResource(resources.ModelResource):
    assessment_type = fields.Field(
        column_name='assessment_type',
        attribute='assessment_type',
        widget=ForeignKeyWidget(AssessmentType, 'code'))

    class Meta:
        fields = DOCUMENT_TYPE_RESOURCE_FIELDS
        export_order = DOCUMENT_TYPE_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        model = DocumentType


class DocumentTypeAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DocumentTypeAdminForm, self).__init__(*args, **kwargs)
        if 'assessment_type' in self.fields:
            self.fields['assessment_type'].queryset = AssessmentType.objects.filter(
                Q(kind='agriform') | Q(kind='agriform2'))

        if self.instance.pk:
            all_questions = self.instance.questions_attachments.all()
            all_assessment_types_uuids = []
            all_assessment_types_uuids += (item.questionnaire.assessment_type.uuid for item in all_questions)
            all_assessment_types = AssessmentType.objects.filter(uuid__in=all_assessment_types_uuids)
            self.fields['assessment_types_where_optional'].queryset = all_assessment_types

    class Meta:
        model = DocumentType
        fields = '__all__'
        widgets = {
            'assessment_types_where_optional': FilteredSelectMultiple(
                verbose_name=_('Assessment types'),
                is_stacked=False),
        }


class DocumentTypeAdmin(ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    list_display = ('code', 'name', 'help_document')
    search_fields = ('code', 'name')
    list_filter = ('assessment_type', 'is_optional')
    resource_class = DocumentTypeResource
    form = DocumentTypeAdminForm

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'name',
            'description',
        ]
        return super(DocumentTypeAdmin, self).__init__(*args, **kwargs)


@admin.register(SharedAttachment)
class SharedAttachmentAdmin(BaseAdmin):
    class Meta:
        model = SharedAttachment


admin.site.register(DocumentType, DocumentTypeAdmin)
