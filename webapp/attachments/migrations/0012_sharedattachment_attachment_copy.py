# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0011_sharedattachment_shared_by_organization'),
    ]

    operations = [
        migrations.AddField(
            model_name='sharedattachment',
            name='attachment_copy',
            field=models.OneToOneField(related_name='original_shared_attachment', null=True, blank=True, to='attachments.Attachment'),
        ),
    ]
