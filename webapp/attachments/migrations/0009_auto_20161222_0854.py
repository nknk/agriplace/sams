# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0008_sharedattachment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sharedattachment',
            options={'ordering': ('uuid',), 'permissions': (('view_shared_attachment', 'Can view shared attachment'), ('edit_shared_attachment', 'Can edit shared attachment'))},
        ),
    ]
