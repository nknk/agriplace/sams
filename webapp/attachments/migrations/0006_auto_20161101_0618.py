# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0005_documenttype_is_optional'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documenttype',
            name='code',
            field=models.CharField(unique=True, max_length=1000, verbose_name='Internal code'),
        ),
    ]
