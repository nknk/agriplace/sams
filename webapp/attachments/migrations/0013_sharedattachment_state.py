# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0012_sharedattachment_attachment_copy'),
    ]

    operations = [
        migrations.AddField(
            model_name='sharedattachment',
            name='state',
            field=models.CharField(default=b'open', max_length=100, choices=[(b'open', 'Open'), (b'inprogress', 'In progress'), (b'processed', 'Processed')]),
        ),
    ]
