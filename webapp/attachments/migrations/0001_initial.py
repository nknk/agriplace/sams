# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import ckeditor.fields
import attachments.models
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('form_builder', '0001_initial'),
        ('assessments', '0001_initial'),
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('for_deletion_mark', models.BooleanField(default=False, verbose_name='For deletion')),
            ],
            options={
                'db_table': 'Attachment',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AgriformAttachment',
            fields=[
                ('attachment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='attachments.Attachment')),
                ('assessment', models.ForeignKey(related_name='attachments', to='assessments.Assessment')),
            ],
            options={
                'db_table': 'AgriformAttachment',
            },
            bases=('attachments.attachment',),
        ),
        migrations.CreateModel(
            name='DocumentType',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('code', models.CharField(max_length=1000, verbose_name='Internal code')),
                ('name', models.CharField(max_length=1000, verbose_name='Type name', db_column=b'name')),
                ('description', ckeditor.fields.RichTextField(max_length=1000, null=True, verbose_name='Description', db_column=b'description', blank=True)),
                ('help_document', models.FileField(max_length=255, null=True, upload_to=b'help_documents', blank=True)),
                ('assessment_type', models.ForeignKey(related_name='agriform', blank=True, to='assessments.AssessmentType', null=True)),
                ('assessment_types_where_optional', models.ManyToManyField(related_name='assessment_types_where_optional_document_types', null=True, to='assessments.AssessmentType', blank=True)),
            ],
            options={
                'ordering': ('code',),
                'abstract': False,
                'db_table': 'DocumentTypeAttachment',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FileAttachment',
            fields=[
                ('attachment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='attachments.Attachment')),
                ('file', models.FileField(max_length=255, upload_to=attachments.models.upload_to_organization_folder)),
                ('original_file_name', models.CharField(max_length=255, verbose_name='Original file name', blank=True)),
                ('expiration_date', models.DateField(null=True, verbose_name='Expiration date', blank=True)),
                ('file_type', models.CharField(max_length=255, verbose_name='File type', blank=True)),
            ],
            options={
                'db_table': 'FileAttachment',
            },
            bases=('attachments.attachment',),
        ),
        migrations.CreateModel(
            name='FormAttachment',
            fields=[
                ('attachment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='attachments.Attachment')),
                ('generic_form_data', models.ForeignKey(related_name='attachments', to='form_builder.GenericFormData')),
            ],
            options={
                'db_table': 'FormAttachment',
            },
            bases=('attachments.attachment',),
        ),
        migrations.CreateModel(
            name='NotApplicableAttachment',
            fields=[
                ('attachment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='attachments.Attachment')),
            ],
            options={
                'db_table': 'NotApplicableAttachment',
            },
            bases=('attachments.attachment',),
        ),
        migrations.CreateModel(
            name='TextReferenceAttachment',
            fields=[
                ('attachment_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='attachments.Attachment')),
                ('description', models.CharField(max_length=1024, verbose_name='Description')),
            ],
            options={
                'db_table': 'TextReferenceAttachment',
            },
            bases=('attachments.attachment',),
        ),
        migrations.AddField(
            model_name='documenttype',
            name='template',
            field=models.ForeignKey(related_name='template', verbose_name=b'Template(not used, see help_document)', blank=True, to='attachments.Attachment', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attachment',
            name='organization',
            field=models.ForeignKey(related_name='attachments', blank=True, to='organization.Organization', null=True),
            preserve_default=True,
        ),
    ]
