# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0006_auto_20161101_0618'),
    ]

    operations = [
        migrations.AddField(
            model_name='agriformattachment',
            name='agriform_reference',
            field=models.CharField(max_length=250, null=True, blank=True),
        ),
    ]
