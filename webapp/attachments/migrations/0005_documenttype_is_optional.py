# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0004_auto_20160816_1317'),
    ]

    operations = [
        migrations.AddField(
            model_name='documenttype',
            name='is_optional',
            field=models.BooleanField(default=False, verbose_name='Optional?'),
        ),
    ]
