# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documenttype',
            name='assessment_types_where_optional',
            field=models.ManyToManyField(related_name='assessment_types_where_optional_document_types', to='assessments.AssessmentType', blank=True),
        ),
    ]
