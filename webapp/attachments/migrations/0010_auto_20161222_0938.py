# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0009_auto_20161222_0854'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sharedattachment',
            options={'ordering': ('uuid',)},
        ),
        migrations.AddField(
            model_name='sharedattachment',
            name='permission',
            field=models.CharField(default='', max_length=50, choices=[(b'can_view', 'Can view'), (b'can_edit', 'Can edit')]),
            preserve_default=False,
        ),
    ]
