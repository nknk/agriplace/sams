# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0013_sharedattachment_state'),
    ]

    operations = [
        migrations.CreateModel(
            name='SharedAttachmentList',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('attachment', models.ForeignKey(related_name='attachment_shared', to='attachments.Attachment')),
                ('attachment_copy', models.OneToOneField(related_name='original_shared_attachment', null=True, blank=True, to='attachments.Attachment')),
            ],
        ),
        migrations.RemoveField(
            model_name='sharedattachment',
            name='attachment',
        ),
        migrations.RemoveField(
            model_name='sharedattachment',
            name='attachment_copy',
        ),
        migrations.AddField(
            model_name='sharedattachmentlist',
            name='shared_attachment',
            field=models.ForeignKey(related_name='shared_attachments', to='attachments.SharedAttachment'),
        ),
        migrations.AddField(
            model_name='sharedattachment',
            name='attachments',
            field=models.ManyToManyField(related_name='shared_attachments', through='attachments.SharedAttachmentList', to='attachments.Attachment'),
        ),
        migrations.AlterUniqueTogether(
            name='sharedattachmentlist',
            unique_together=set([('shared_attachment', 'attachment')]),
        ),
    ]
