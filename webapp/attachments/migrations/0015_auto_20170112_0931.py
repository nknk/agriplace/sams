# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0014_auto_20170109_1054'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='sharedattachmentlist',
            table='SharedAttachmentList',
        ),
    ]
