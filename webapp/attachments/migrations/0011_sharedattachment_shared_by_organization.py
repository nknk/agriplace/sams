# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0013_auto_20161206_0834'),
        ('attachments', '0010_auto_20161222_0938'),
    ]

    operations = [
        migrations.AddField(
            model_name='sharedattachment',
            name='shared_by_organization',
            field=models.ForeignKey(related_name='shared_by_organizations', default=None, to='organization.Organization'),
        ),
    ]
