# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0015_auto_20170112_0931'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sharedattachment',
            options={'ordering': ('-created_time',)},
        ),
    ]
