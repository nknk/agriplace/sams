# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0002_helpdocument'),
        ('attachments', '0002_auto_20160615_2340'),
    ]

    operations = [
    ]
