# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0013_auto_20161206_0834'),
        ('attachments', '0007_agriformattachment_agriform_reference'),
    ]

    operations = [
        migrations.CreateModel(
            name='SharedAttachment',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('attachment', models.ForeignKey(related_name='shared_attachments', to='attachments.Attachment')),
                ('organization_membership', models.ForeignKey(related_name='shared_organization_memberships', to='organization.OrganizationMembership')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'SharedAttachment',
                'permissions': (('view_sharedattachment', 'Can view shared attachment'), ('edit_sharedattachment', 'Can edit shared attachment')),
            },
        ),
    ]
