# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0003_merge'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='agriformattachment',
            options={'permissions': (('view_agriformattachment', 'Can view agriform attachment'),)},
        ),
        migrations.AlterModelOptions(
            name='documenttype',
            options={'ordering': ('code',), 'permissions': (('view_documenttypeattachment', 'Can view document type (attachment)'),)},
        ),
        migrations.AlterModelOptions(
            name='fileattachment',
            options={'permissions': (('view_fileattachment', 'Can view file attachment'),)},
        ),
        migrations.AlterModelOptions(
            name='formattachment',
            options={'permissions': (('view_formattachment', 'Can view form attachment'),)},
        ),
        migrations.AlterModelOptions(
            name='helpdocument',
            options={'permissions': (('view_helpdocument', 'Can view help document'),)},
        ),
        migrations.AlterModelOptions(
            name='notapplicableattachment',
            options={'permissions': (('view_notapplicableattachment', 'Can view notapplicable attachment'),)},
        ),
        migrations.AlterModelOptions(
            name='textreferenceattachment',
            options={'permissions': (('view_textreferenceattachment', 'Can view text reference attachment'),)},
        ),
    ]
