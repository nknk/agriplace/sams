import base64
import uuid

from django.core.files.base import ContentFile
from django.core.files.uploadedfile import UploadedFile
from rest_framework import serializers


class Base64ImageField(serializers.ImageField):
    def to_internal_value(self, data):
        if isinstance(data, basestring) and data.startswith('data:image'):
            format, imgstr = data.split(';base64,')  # format ~= data:image/X,
            ext = format.split('/')[-1]  # guess file extension
            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
        return super(Base64ImageField, self).to_internal_value(data)


class FileField(serializers.FileField):

    def to_internal_value(self, data):
        """
        Checks to see if data is a base64 encoded file, or a subclass of Django's
        UploadedFile and processes it.
        :data: Either an string or an subclass of UploadedFile
        """
        if isinstance(data, basestring):
            if ';base64,' not in data:
                data += ';base64,'
            header, data = data.split(';base64,')
            # print("[DEBUG] header, data : %s %s" % (header, data))
            decoded = ' '
            if data:
                # Try to base64 decode the data url.
                try:
                    decoded = base64.b64decode(data)
                except TypeError:
                    raise serializers.ValidationError('Not a valid file')
            file_name, file_ext = get_file_name_ext(header)

            # TODO maybe we need this additional checking in future
            #if file_ext not in settings.VALID_FILE_EXTENSIONS:
            #    raise serializers.ValidationError('Invalid file type.')

            data_file = ContentFile(decoded, name=file_name+file_ext)
            return super(FileField, self).to_internal_value(data_file)

        elif isinstance(data, UploadedFile):

            # TODO maybe we need this additional checking in future
            #if not valid_file_extension(str(data), settings.VALID_FILE_EXTENSIONS) \
            #    or not valid_file_mimetype(data, settings.VALID_FILE_MIMETYPES):
            #    raise serializers.ValidationError('Invalid file type.')

            file_name, file_ext = get_file_name_ext(data)
            data._name = file_name

            return super(FileField, self).from_native(data)

        else:
            raise serializers.ValidationError('Invalid file uploaded.')


def get_file_name_ext(header):
    # TODO improve this. How to guess file type.
    if header == 'data:':
        ext = ''
    else:
        ext = '.' + header.split('/')[-1]
    return (str(uuid.uuid4())[:12], ext)