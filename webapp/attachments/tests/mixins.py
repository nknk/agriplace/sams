from attachments.tests.factories import DocumentTypeFactory, TextReferenceAttachmentFactory


class AttachmentAPIFactoryMixin(object):
    """
    Mixin to create attachments and document types
    """

    @staticmethod
    def create_document_type(assessment_type):
        return DocumentTypeFactory.create(
            assessment_type=assessment_type
        )

    @staticmethod
    def create_text_reference_attachment(organization):
        return TextReferenceAttachmentFactory.create(
            organization=organization
        )
