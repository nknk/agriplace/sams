from datetime import datetime

from factory import DjangoModelFactory
from factory import SubFactory
from factory import sequence

from assessment_types.tests.factories import AssessmentTypeFactory
from attachments.models import DocumentType, Attachment, TextReferenceAttachment
from organization.tests.factories import OrganizationFactory


class DocumentTypeFactory(DjangoModelFactory):
    code = sequence(lambda n: "EV{}".format(n))
    assessment_type = SubFactory(AssessmentTypeFactory)
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    description = sequence(lambda n: "Description {}".format(n))
    template = None
    remarks = sequence(lambda n: "Remarks {}".format(n))
    name = sequence(lambda n: "Document type name {}".format(n))
    help_document = sequence(lambda n: "help_documents/document_name_{}.xlsx".format(n))

    class Meta:
        model = DocumentType


class AttachmentFactory(DjangoModelFactory):
    title = sequence(lambda n: "Attachment title {}".format(n))
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    remarks = sequence(lambda n: "Remarks {}".format(n))
    organization = SubFactory(OrganizationFactory)
    for_deletion_mark = False

    class Meta:
        model = Attachment


class TextReferenceAttachmentFactory(AttachmentFactory):
    description = sequence(lambda n: "Text reference description {}".format(n))

    class Meta:
        model = TextReferenceAttachment
