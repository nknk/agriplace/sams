from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.aggregates import Count

from organization.models import OrganizationMembership


class SharedAttachmentManager(models.Manager):

    def is_already_shared(self, membership, attachments, current_org):
        share_with_organizations = OrganizationMembership.objects.filter(
            uuid=membership.uuid,
            shared_organization_memberships__shared_by_organization=current_org,
            shared_organization_memberships__attachments__attachment_shared__attachment__in=attachments,
        ).distinct()
        share_with_organizations = share_with_organizations.annotate(
            num_attachments=Count('shared_organization_memberships__attachments')
        )
        share_with_organizations = share_with_organizations.filter(num_attachments=len(attachments))

        return share_with_organizations

    def share_attachments(self, membership, attachments, current_org, permission='can_view'):
        from attachments.models import SharedAttachmentList

        shared_attachment = self.create(
            organization_membership=membership,
            shared_by_organization=current_org,
            permission=permission
        )
        for attachment in attachments:
            attachment_shared = SharedAttachmentList(
                shared_attachment=shared_attachment,
                attachment=attachment
            )
            attachment_shared.save()

    def get_or_create_shared_attachments(
            self, current_organization, attachments, membership_uuids, permission='can_view'
    ):
        for membership_uuid in membership_uuids:
            try:
                organization_membership = OrganizationMembership.objects.get(pk=membership_uuid)
            except ObjectDoesNotExist:
                raise Exception("Membership {} does not exists".format(membership_uuid))
            else:

                if not self.is_already_shared(organization_membership, attachments, current_organization):
                    self.share_attachments(organization_membership, attachments, current_organization)

        return True


class DocumentTypeManager(models.Manager):

    def linked_to_attachment(self, attachment):
        return self.filter(assessment_document_links_attachments__attachment=attachment).distinct()
