import os.path
from uuid import uuid4

from django.conf import settings, global_settings
from django.db import models
from django.utils.translation import ugettext as _

from ckeditor.fields import RichTextField
from model_utils.managers import InheritanceManager

from assessments.models import Assessment, AssessmentType, QuestionLevel, Question, AssessmentDocumentLink
from attachments.managers import SharedAttachmentManager, DocumentTypeManager
from form_builder.models import GenericFormData
from core.models import UuidModel
from organization.models import Organization
from translations.helpers import tolerant_ugettext


def check_create_folder(path):
    folder_path = os.path.join(settings.MEDIA_ROOT, path)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)


def upload_to_organization_folder(instance, filename):
    return '{organization}/attachments/{uuid}/{file}'.format(
        organization=instance.organization.uuid,
        uuid=str(uuid4()),
        file=filename
    )


class Attachment(UuidModel):
    title = models.CharField(_("Title"), max_length=255)
    for_deletion_mark = models.BooleanField(_("For deletion"), default=False)
    organization = models.ForeignKey(Organization, related_name='attachments', blank=True, null=True)
    # is_frozen = models.BooleanField(_("Is frozen?"), default=False)

    objects = InheritanceManager()

    def check_permissions(self, user, action):
        return user in self.organization.users.all()

    def is_frozen(self):
        return any((assessment['is_closed'] for assessment in self.used_in_assessments()))

    def used_in_assessments(self):
        assessment_links = self.assessment_links_attachments.all().prefetch_related('assessment')
        list_of_assessments = []
        assessments_container = []
        for assessment_link in assessment_links:
            assessment = assessment_link.assessment
            if assessment and assessment.assessment_type.kind == 'assessment' and assessment not in assessments_container:
                assessments_container.append(assessment)
                list_of_assessments.append(
                    {
                        'uuid': assessment.uuid,
                        'name': assessment.name,
                        'is_closed': assessment.state == 'closed',
                        'url': assessment.get_absolute_url()
                    }
                )
        return list_of_assessments

    @classmethod
    def get_used_in_assessments(cls, attachment_uuids):
        links = AssessmentDocumentLink.objects.filter(
            attachment_id__in=attachment_uuids, assessment__assessment_type__kind='assessment'
        ).select_related('assessment')

        used_in = {}
        for link in links:
            used_in.setdefault(link.attachment_id, []).append(
                {
                    "uuid": link.assessment.uuid,
                    'name': link.assessment.name,
                    'is_closed': link.assessment.state == 'closed',
                    'url': link.assessment.get_absolute_url()
                }
            )

        return used_in

    @classmethod
    def get_used_in_document_types(cls, attachment_uuids):
        links = AssessmentDocumentLink.objects.filter(
            attachment_id__in=attachment_uuids
        ).select_related('document_type_attachment')

        used_in = {}
        for link in links:
            used_in.setdefault(link.attachment_id, []).append(
                {
                    'uuid': link.document_type_attachment.uuid,
                    'name': tolerant_ugettext(link.document_type_attachment.name),
                    'code': link.document_type_attachment.code
                }
            )

        return used_in

    def used_in_document_types(self):
        document_links = self.assessment_links_attachments.all().prefetch_related('document_type_attachment')
        list_of_document_types = []
        document_types_container = []
        for document_link in document_links:
            document_type = document_link.document_type_attachment
            if document_type and document_type not in document_types_container:
                document_types_container.append(document_type)
                list_of_document_types.append(
                    {
                        'uuid': document_type.uuid,
                        'name': tolerant_ugettext(document_type.name),
                        'code': document_type.code
                    }
                )
        return list_of_document_types

    def usable_for_assessments(self):
        document_links = self.assessment_links_attachments.all().prefetch_related('document_type_attachment')
        list_of_assessment_types = []
        assessment_types_container = []
        for document_link in document_links:
            questions = Question.objects.filter(document_types_attachments=document_link.document_type_attachment)
            for question in questions:
                assessment_type = AssessmentType.objects.filter(
                    questionnaires=question.questionnaire, is_public=True, kind="assessment"
                ).first()
                if assessment_type and assessment_type not in assessment_types_container:
                    assessment_types_container.append(assessment_type)
                    list_of_assessment_types.append(
                        {
                            'uuid': assessment_type.uuid,
                            'name': tolerant_ugettext(assessment_type.name),
                            'code': assessment_type.code,
                            "kind": assessment_type.kind
                        }
                    )
        return list_of_assessment_types

    # TODO: maybe better return key from . serializers.ATTACHMENT_TYPES
    def get_attachment_type(self):
        return self.__class__.__name__

    def get_mutation_name(self, action):
        return _("Evidence reference '{0}' {1}").format(self.title, action)

    class Meta:
        db_table = 'Attachment'

    def __unicode__(self):
        if self.title:
            return u"{0}".format(self.title)
        else:
            return u"{0}".format(self.uuid)

    def get_pdf_export_text(self):
        child_object = Attachment.objects.get_subclass(pk=self.pk)
        if isinstance(child_object, FileAttachment):
            return child_object.uuid+'_'+child_object.original_file_name
        if isinstance(child_object, TextReferenceAttachment):
            return child_object.description
        if isinstance(child_object, AgriformAttachment):
            return child_object.get_agriform_title()


class FileAttachment(Attachment):
    file = models.FileField(max_length=255, upload_to=upload_to_organization_folder)
    original_file_name = models.CharField(_("Original file name"), max_length=255, blank=True)
    expiration_date = models.DateField(_("Expiration date"), blank=True, null=True)
    file_type = models.CharField(_("File type"), max_length=255, blank=True)

    def get_mutation_name(self, action):
        return _("Evidence '{0}' {1}").format(self.original_file_name, action)

    class Meta:
        db_table = 'FileAttachment'
        permissions = (
            ('view_fileattachment', 'Can view file attachment'),
        )


class TextReferenceAttachment(Attachment):
    description = models.CharField(_("Description"), max_length=1024)

    def get_mutation_name(self, action):
        return _("Evidence reference '{0}' {1}").format(self.description, action)

    class Meta:
        db_table = 'TextReferenceAttachment'
        permissions = (
            ('view_textreferenceattachment', 'Can view text reference attachment'),
        )


class NotApplicableAttachment(Attachment):
    def get_mutation_name(self, action):
        raise NotImplementedError()

    class Meta:
        db_table = 'NotApplicableAttachment'
        permissions = (
            ('view_notapplicableattachment', 'Can view notapplicable attachment'),
        )


class AgriformAttachment(Attachment):
    assessment = models.ForeignKey(Assessment, related_name='attachments')
    agriform_reference = models.CharField(max_length=250, null=True, blank=True)

    def get_mutation_name(self, action):
        return _("Agriform '{0}' {1}").format(self.assessment.name, action)

    def get_agriform_title(self):
        modified_time = self.assessment.modified_time
        name = self.agriform_reference if self.agriform_reference else \
            tolerant_ugettext(self.assessment.assessment_type.name)
        return u"%s_%s" % (
            name,
            '{:%d/%m/%Y}'.format(modified_time)
        )

    class Meta:
        db_table = 'AgriformAttachment'
        permissions = (
            ('view_agriformattachment', 'Can view agriform attachment'),
        )

    def save(self, *args, **kwargs):
        super(AgriformAttachment, self).save(*args, **kwargs)


class FormAttachment(Attachment):
    generic_form_data = models.ForeignKey(GenericFormData, related_name='attachments')

    def get_mutation_name(self, action):
        raise NotImplementedError()

    class Meta:
        db_table = 'FormAttachment'
        permissions = (
            ('view_formattachment', 'Can view form attachment'),
        )


class DocumentType(UuidModel):

    objects = DocumentTypeManager()

    code = models.CharField(_("Internal code"), max_length=1000, unique=True)
    name = models.CharField(_("Type name"), max_length=1000, db_column='name')
    description = RichTextField(_("Description"), max_length=1000, blank=True, null=True, db_column='description')
    template = models.ForeignKey(
        Attachment,
        related_name='template',
        verbose_name="Template(not used, see help_document)",
        blank=True, null=True)
    help_document = models.FileField(max_length=255, upload_to="help_documents", blank=True, null=True)
    assessment_type = models.ForeignKey(AssessmentType, related_name='agriform', blank=True, null=True)
    assessment_types_where_optional = models.ManyToManyField(
        AssessmentType,
        related_name='assessment_types_where_optional_document_types',
        blank=True
    )
    is_optional = models.BooleanField(_("Optional?"), default=False)

    class Meta(UuidModel.Meta):
        db_table = 'DocumentTypeAttachment'
        ordering = ('code',)
        permissions = (
            ('view_documenttypeattachment', 'Can view document type (attachment)'),
        )

    def __unicode__(self):
        return u"%s %s" % (self.code, self.name)

    def get_level(self):
        questions = self.questions_attachments.all(). \
            select_related('questionnaire__assessment_type'). \
            select_related('level_object')
        level_objects_by_assessment_type = [(question.questionnaire.assessment_type.uuid, question.level_object) for
                                            question in questions]
        max_levels = {}
        level_objects_by_aseessment_type_grouped = {}
        for item in level_objects_by_assessment_type:
            if item[0] in level_objects_by_assessment_type:
                level_objects_by_aseessment_type_grouped[item[0]].append(item[1])
            else:
                level_objects_by_aseessment_type_grouped[item[0]] = [item[1]]

        for assessment_type_uuid, level_objects in level_objects_by_aseessment_type_grouped.iteritems():
            max_level = sorted(level_objects, key=lambda x: x.order_index, reverse=False)[0]
            max_levels[assessment_type_uuid] = max_level.uuid
        return max_levels

    def get_template(self):
        result = ""
        if self.help_document:
            result = self.help_document.url
        return result

    def get_help_documents(self):
        return HelpDocument.objects.filter(document_type=self)


class HelpDocument(UuidModel):
    language = models.CharField(_("Language of document"), max_length=10, choices=global_settings.LANGUAGES)
    document_type = models.ForeignKey(DocumentType, related_name='help_documents')
    help_document = models.FileField(max_length=255, upload_to="help_documents")

    def __unicode__(self):
        return u"%s(%s)" % (self.document_type, self.language)

    class Meta:
        db_table = 'HelpDocument'
        permissions = (
            ('view_helpdocument', 'Can view help document'),
        )


class SharedAttachment(UuidModel):
    """
    Shared attachments: instance of attachment shared with one membership
    """

    PERMISSION_CHOICES = (
        ('can_view', _('Can view')),
        ('can_edit', _('Can edit'))
    )

    SHARING_STATES = (
        ('open', _('Open')),
        ('inprogress', _('In progress')),
        ('processed', _('Processed'))
    )

    attachments = models.ManyToManyField(
        'Attachment',
        related_name='shared_attachments',
        through='SharedAttachmentList',
        through_fields=('shared_attachment', 'attachment')
    )
    organization_membership = models.ForeignKey(
        'organization.OrganizationMembership', related_name='shared_organization_memberships'
    )
    shared_by_organization = models.ForeignKey(Organization, related_name='shared_by_organizations', default=None)
    permission = models.CharField(max_length=50, choices=PERMISSION_CHOICES)

    state = models.CharField(max_length=100, choices=SHARING_STATES, default='open')

    objects = SharedAttachmentManager()

    class Meta(UuidModel.Meta):
        db_table = 'SharedAttachment'
        ordering = ('-created_time', )

    def __unicode__(self):
        return u"<{}> shared <{}> with <{}>".format(
            self.shared_by_organization.name,
            self.attachments,
            self.organization_membership.secondary_organization.name
        )


class SharedAttachmentList(UuidModel):

    shared_attachment = models.ForeignKey(SharedAttachment, related_name='shared_attachments')
    attachment = models.ForeignKey('Attachment', related_name='attachment_shared')
    attachment_copy = models.OneToOneField(
        'Attachment', related_name='original_shared_attachment', null=True, blank=True
    )

    class Meta:
        db_table = "SharedAttachmentList"
        unique_together = ('shared_attachment', 'attachment')
