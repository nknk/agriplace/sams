import urllib2

from django.shortcuts import get_object_or_404, render
from django.core.servers.basehttp import FileWrapper
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from organization.views import get_organization_context
from sams.decorators import membership_required
from attachments.models import FileAttachment, Attachment


@login_required
def serve(request, file_attachment_uuid, file_name):
    attachment = get_object_or_404(FileAttachment, uuid=file_attachment_uuid)
    uquoted_file_name = urllib2.unquote(file_name)
    wrapper = FileWrapper(attachment.file)
    response = HttpResponse(wrapper, content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment; filename*=UTF-8\'\'%s' % (
        urllib2.quote(uquoted_file_name.encode('utf-8')),
    )
    response['Content-Length'] = attachment.file.size
    return response


@login_required
@membership_required
def index(request, *args, **kwargs):
    context, _ = get_organization_context(request, kwargs, "my_docs_list")

    # Get documents
    documents = Attachment.objects.all()

    context.update({
        'documents': documents,
    })
    return render(request, 'attachments/index.html', context)
