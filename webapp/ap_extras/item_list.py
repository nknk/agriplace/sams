"""
AP Extras
Library of small reusable Django components
"""

from django.utils.translation import ugettext as _


class ItemListColumn:
    """
    Represents data model for an item cell
    """
    header = ''
    text = lambda self, item: None  # lambda text generator
    url = lambda self, item: None  # lambda url generator
    icon_class = lambda self, item: None  # lambda icon generator
    slug = None

    def __init__(self, *args, **kwargs):
        # Initialize from args/kwargs, if provided
        for key in kwargs:
            setattr(self, key, kwargs[key])


class ItemModel:
    """
    Represents data model for a single ItemList item
    """
    model = None
    name = None
    url = None
    icon_class = None
    cells = []  # list of ItemCellModel

    def __unicode__(self):
        return self.name


class ItemListModel:
    """
    Represents data model for an item list.
    You must provide `items` and `columns`, then call refresh(), then you can access `rows`
    """

    def __init__(self, *args, **kwargs):
        # Defaults
        self.id = "?"
        self.items = []  # collection of items to display
        self.columns = []  # column definition dict
        self.rows = []

        self.no_items_text = _("No items available")  # text displayed if no items are present

        self.create_text = _("Create new item")  # text of 'Create a new item' link
        self.create_tooltip = _("Create new item")  # tooltip of 'Create new item' link
        self.create_url = None  # url of 'Create new item' button

        self.columns = []  # list of ItemListColumn. First column always required.

        # Initialize from args/kwargs, if provided
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def refresh(self):
        # generate item_instances collection based on items and columns
        if not self.columns:
            raise Exception("You must defined at least one column before you refresh the list.")
        self.rows = []
        for item in self.items:
            item_wrapper = {
                'cells': []
            }
            for column in self.columns:
                item_wrapper['cells'].append({
                    'text': column.text(item),
                    'url': column.url(item),
                    'icon_class': column.icon_class(item),
                    'slug': column.slug
                })
            self.rows.append(item_wrapper)
