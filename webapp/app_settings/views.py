from django.views.generic.detail import DetailView

from .models import HelpTextBlock


class HelpTextBlockDetail(DetailView):

    model = HelpTextBlock
    slug_field = 'internal_code'

    def get_context_data(self, **kwargs):
        context = super(HelpTextBlockDetail, self).get_context_data(**kwargs)
        return context