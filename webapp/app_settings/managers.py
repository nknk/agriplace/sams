# -*- encoding: utf-8 -*-
from django.db import models


class LegalDocumentManager(models.Manager):

    def get_documents_by_language(self, language='en'):
        return self.filter(
            language=language
        )

    def get_documents_by_type(self, document_type_list):
        return self.filter(
            type__in=document_type_list
        )


class HelpTextBlockManager(models.Manager):
    def get_by_natural_key(self, title, internal_code):
        return self.get(title=title, internal_code=internal_code)


class TextBlockManager(models.Manager):
    def get_by_natural_key(self, code):
        return self.get(code=code)
