# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_settings', '0002_helptextblock_assessment_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='helptextblock',
            options={'permissions': (('view_helptextblock', 'Can view help text block'),)},
        ),
        migrations.AlterModelOptions(
            name='textblock',
            options={'permissions': (('view_textblock', 'Can view text block'),)},
        ),
    ]