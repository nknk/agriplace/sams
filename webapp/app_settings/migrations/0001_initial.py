# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='HelpTextBlock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('internal_code', models.SlugField(unique=True, max_length=255, verbose_name='Internal code')),
                ('text', ckeditor.fields.RichTextField(default=b'', verbose_name='Text', blank=True)),
            ],
            options={
                'db_table': 'HelpContentBlock',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TextBlock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=255, verbose_name='Code')),
                ('text', ckeditor.fields.RichTextField(default=b'', verbose_name='Text', blank=True)),
                ('description', models.TextField(default=b'', verbose_name='Description', blank=True)),
            ],
            options={
                'db_table': 'ContentBlock',
            },
            bases=(models.Model,),
        ),
    ]
