# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0001_initial'),
        ('app_settings', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='helptextblock',
            name='assessment_type',
            field=models.ManyToManyField(related_name='helptextblocks', verbose_name='Assessment type', to='assessments.AssessmentType', blank=True),
            preserve_default=True,
        ),
    ]
