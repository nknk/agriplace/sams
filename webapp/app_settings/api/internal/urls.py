from django.conf.urls import patterns, url

from app_settings.api.internal.views import HelpTextBlockList

urlpatterns = patterns(
    'app_settings.api.internal.views',

    url(
        r'help-text-blocks/$',
        HelpTextBlockList.as_view(),
        name='help-text-block-list'
    ),
    url(
        r'^api-root/$',
        'internal_api_app_settings_root',
        name='app-settings-app-api-list'
    ),
)
