from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse

from app_settings.api.internal.serializers import HelpTextBlockSerializer
from app_settings.models import HelpTextBlock
from assessments.models import Assessment


@api_view(['GET'])
def internal_api_app_settings_root(request, format=None):
    """
    Root for App_Settings Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'help-text-block-list': reverse('help-text-block-list', request=request, format=format),
    })


class HelpTextBlockList(generics.ListAPIView):
    """
    DESCRIPTION: Generic API view - list of all Help Text Blocks
    VERIFIED: ajk 15.09.2016
    TEST COVERAGE: no

    :param assessment_uuid - optional, used to filter by assessment -> assessment-type-uuid
    """
    serializer_class = HelpTextBlockSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned HelpTextBlock,
        by filtering against a `assessment_type_uuid` query parameter in the URL.
        """
        queryset = HelpTextBlock.objects.all()
        assessment_uuid = self.request.query_params.get('assessment-uuid')
        if assessment_uuid is not None:
            try:
                assessment = Assessment.objects.get(uuid=assessment_uuid)
                queryset = queryset.filter(assessment_type__in=[assessment.assessment_type.uuid])
            except:
                queryset = []
        return queryset
