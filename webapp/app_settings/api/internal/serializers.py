from rest_framework import serializers

from app_settings.models import HelpTextBlock


class HelpTextBlockSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        app_settings.api.internal.views.HelpTextBlockList
        )
    VERIFIED: ajk 15.09.2016
    """
    url = serializers.CharField(source='get_absolute_url')

    class Meta:
        fields = ('title', 'internal_code', 'url')
        model = HelpTextBlock
