# -*- encoding: utf-8 -*-

LEGAL_DOCUMENT_TC = 'Terms and conditions'
LEGAL_DOCUMENT_PP = 'Privacy policy'

LEGAL_DOCUMENT_CHOICES = (
    (1, LEGAL_DOCUMENT_TC),
    (2, LEGAL_DOCUMENT_PP)
)
