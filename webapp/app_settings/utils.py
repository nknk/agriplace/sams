from app_settings.constants import LEGAL_DOCUMENT_TC, LEGAL_DOCUMENT_PP, LEGAL_DOCUMENT_CHOICES

try:
    from django.apps import apps
    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model

from .models import TextBlock, HelpTextBlock, LegalDocument
from translations.helpers import tolerant_ugettext


def get_text_blocks(codes):
    """
    Get text blocks.
    :param codes: List of text block codes to look up
    :return: Dict {code: text}
    """
    text_blocks_query = TextBlock.objects.filter(code__in=codes)
    text_blocks = {
        tb.code: tolerant_ugettext(tb.text)
        for tb in text_blocks_query
    }
    return text_blocks


def get_help_text_block(title, assessment_type_uuid=None):
    """
    Get help text blocks.
    :params title: Title for help block to look up; assessment_type_uuid Uuid of assessment type
    :return: absolute url of founded HelpTextBlock instance or blank line
    """
    help_link_url = ""
    if assessment_type_uuid:
        try:
            assessment_type_model = get_model('assessments', 'AssessmentType')
            assessment_type = assessment_type_model.objects.get(pk=assessment_type_uuid)
            help_links = assessment_type.helptextblocks.all().filter(title=title)
            if help_links:
                help_link_url = help_links[0].get_absolute_url()
        except assessment_type_model.DoesNotExist:
                pass
    else:
        help_links = HelpTextBlock.objects.filter(title=title)
        if help_links:
            help_link_url = help_links[0].get_absolute_url()
    return help_link_url


def get_legal_document(language='nl', fall_down_language='en'):
    legal_documents = {
        LEGAL_DOCUMENT_TC: None,
        LEGAL_DOCUMENT_PP: None
    }
    legal_choices_dict = {value: key for key, value in LEGAL_DOCUMENT_CHOICES}
    actual_values = [legal_choices_dict[document_type] for document_type in legal_documents.keys()]

    requested_documents = LegalDocument.objects.get_documents_by_type(document_type_list=actual_values)

    for document_type in legal_documents.keys():
        document_list = requested_documents.filter(type=legal_choices_dict[document_type])
        if document_list.exists():
            if document_list.filter(language=language).exists():
                legal_documents[document_type] = document_list.get(language=language)
            elif language != fall_down_language and document_list.filter(language=fall_down_language).exists():
                legal_documents[document_type] = document_list.get(language=fall_down_language)
    return legal_documents
