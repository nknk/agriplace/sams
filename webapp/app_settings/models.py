# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from app_settings.managers import LegalDocumentManager, TextBlockManager, HelpTextBlockManager

"""
Settings app models
"""
from ckeditor.fields import RichTextField
from django.conf import global_settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse

from app_settings.constants import LEGAL_DOCUMENT_CHOICES
from assessments.models import AssessmentType


class TextBlock(models.Model):
    """
    Static content (text) block to be used in user interface.
    """

    code = models.CharField(_("Code"), max_length=255, unique=True)
    text = RichTextField(_("Text"), default='', blank=True)
    description = models.TextField(_("Description"), default='', blank=True)

    objects = TextBlockManager()

    class Meta:
        db_table = 'ContentBlock'
        permissions = (
            ('view_textblock', 'Can view text block'),
        )

    def natural_key(self):
        return self.code,

    def __unicode__(self):
        return self.code.replace('_', ' ').replace('-', ' ')


class HelpTextBlock(models.Model):
    """
    Static content (text) block to be used in help popups.
    """

    # TODO change model - title + assessment_type should be unique together

    title = models.CharField(_("Title"), max_length=255, unique=False)
    internal_code = models.SlugField(_("Internal code"), max_length=255, unique=True)
    text = RichTextField(_("Text"), default='', blank=True)

    # restrictions
    assessment_type = models.ManyToManyField(
        AssessmentType,
        verbose_name=_("Assessment type"),
        related_name='helptextblocks',
        blank=True
    )

    objects = HelpTextBlockManager()

    class Meta:
        db_table = 'HelpContentBlock'
        permissions = (
            ('view_helptextblock', 'Can view help text block'),
        )

    def natural_key(self):
        return self.title, self.internal_code

    def get_absolute_url(self):
        return reverse('help_text_block_detail',  kwargs={'slug': self.internal_code})

    def __unicode__(self):
        return slugify(self.title)


@python_2_unicode_compatible
class LegalDocument(models.Model):
    """
    Legal documents, like Privacy policy and Terms and conditions, to show in different parts of app
    """
    language = models.CharField(_("Language of document"), max_length=10, choices=global_settings.LANGUAGES)
    document = models.FileField(max_length=255, upload_to="additional_documents")
    type = models.PositiveIntegerField("Type of document", choices=LEGAL_DOCUMENT_CHOICES, default=1)

    objects = LegalDocumentManager()

    class Meta:
        unique_together = (("language", "type"),)
        verbose_name = 'Legal document'
        verbose_name_plural = 'Legal documents'
        permissions = (
            ('view_legaldocument', 'Can view legal document'),
        )

    def __str__(self):
        return u'{}({})'.format(self.get_type_display(), self.language)
