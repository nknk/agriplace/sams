from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.widgets import FilteredSelectMultiple

from core.admin import TranslationMixin, BaseAdmin
from .models import TextBlock, HelpTextBlock, LegalDocument


class HelpTextBlockAdminForm(forms.ModelForm):

    class Meta:
        model = HelpTextBlock
        fields = '__all__'
        widgets = {
            'assessment_type': FilteredSelectMultiple(
                verbose_name=_('Assessment type'),
                is_stacked=False),
        }


class HelpTextBlockAdmin(BaseAdmin, TranslationMixin):
    list_display = ('title', 'internal_code')
    search_fields = ('title', 'internal_code')
    form = HelpTextBlockAdminForm

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'title',
            'text',
        ]
        return super(HelpTextBlockAdmin, self).__init__(*args, **kwargs)


class TextBlockAdmin(BaseAdmin, TranslationMixin):
    list_display = ('code',)
    search_fields = ('code',)

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'text',
            'description',
        ]
        return super(TextBlockAdmin, self).__init__(*args, **kwargs)


admin.site.register(HelpTextBlock, HelpTextBlockAdmin)
admin.site.register(TextBlock, TextBlockAdmin)
admin.site.register(LegalDocument)
