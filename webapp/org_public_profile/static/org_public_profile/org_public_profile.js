ViewModel = function(data){
    var self = this;
    this.sections = {};

    // Add observable for each section
    // Must initialize this before visible_sections_csv gets to run,
    // otherwise it won't detect dependencies
    var all_sections_list = _org_public_profile_model.all_sections.split(',');
    for(var i in all_sections_list) {
        var section = all_sections_list[i];
        self.sections[section] = ko.observable(false);
    }

    // Mark visible sections based on initial data
    var visible_sections = _org_public_profile_model.visible_sections.split(',');
    for(var i in visible_sections) {
        if(visible_sections[i] in self.sections) {
            self.sections[visible_sections[i]](true);
        }
    }
}

$(document).ready(function() {
    view_model = new ViewModel();
    ko.applyBindings(view_model);
});
