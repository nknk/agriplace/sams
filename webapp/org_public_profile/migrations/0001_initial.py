# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import organization.models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrganizationPublicProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('visible_sections', models.CharField(default=b'business_card', max_length=1000, blank=True)),
                ('settings_json', models.TextField(default=b'{}')),
                ('is_public', models.BooleanField(default=True)),
                ('profile_photo', models.ImageField(upload_to=organization.models.upload_to_org, null=True, verbose_name='Profile photo', blank=True)),
                ('organization', models.OneToOneField(related_name='public_profile', to='organization.Organization')),
            ],
            options={
                'db_table': 'OrganizationPublicProfile',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfilePhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(upload_to=organization.models.upload_to_org, null=True, verbose_name='Photo', blank=True)),
                ('profile', models.ForeignKey(related_name='photos', to='org_public_profile.OrganizationPublicProfile')),
            ],
            options={
                'db_table': 'ProfilePhoto',
            },
            bases=(models.Model,),
        ),
    ]
