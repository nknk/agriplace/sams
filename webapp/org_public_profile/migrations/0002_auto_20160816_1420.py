# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('org_public_profile', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='organizationpublicprofile',
            options={'permissions': (('view_organizationpublicprofile', 'Can view organization public profile'),)},
        ),
        migrations.AlterModelOptions(
            name='profilephoto',
            options={'permissions': (('view_profilephoto', 'Can view profile photo'),)},
        ),
    ]
