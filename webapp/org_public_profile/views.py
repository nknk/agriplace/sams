import json
import logging

from organization.models import CertificationType

logger = logging.getLogger(__name__)

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext as _

from organization.views_organization import get_organization_context
from sams.constants import ICONS
from sams.decorators import session_organization_required

from .forms import ProfileFieldsForm
from .models import *


@login_required
@session_organization_required
def own_profile(request, *args, **kwargs):
    context, organization = get_organization_context(request, kwargs, 'own_profile')
    profile, created = OrganizationPublicProfile.objects.get_or_create(organization=organization)
    context['target_organization'] = organization
    context.update({
        "page_title": context['organization'].name,
        "page_title_secondary": _("organization"),
        'is_editable': True,
        'all_sections': OrganizationPublicProfile.SECTIONS_CHOICES,
        'visible_sections': profile.visible_sections,
        'profile': profile,
        # 'body_class': 'gray'
        'certificate_types': CertificationType.objects.exclude(code='').distinct().values_list('code', flat=True)
    })
    context['breadcrumbs'] += [
        {
            'text': _("Public profile"),
            'icon': ICONS['organization_profile']
        }
    ]

    section_names = [i[0] for i in OrganizationPublicProfile.SECTIONS_CHOICES]

    if request.method == 'POST':
        # POST
        form = ProfileFieldsForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            messages.success(request, _("Saved."))
    else:
        # GET, etc.
        form = ProfileFieldsForm(instance=profile)
    visible_sections = profile.visible_sections.strip().split(',')
    visible_sections_dict = {
        section: section in visible_sections
        for section in section_names
    }
    visible_sections_json = json.dumps(visible_sections_dict)
    context.update({
        'form': form,
        'visible_sections': visible_sections,
        'visible_sections_data': visible_sections_json
    })
    return render(request, 'org_public_profile/own_profile.html', context)


@login_required
@session_organization_required
def own_profile_sharing(request, *args, **kwargs):
    """
    Turns public profile of organization is_public on/off based on form `public` variable
    """
    context,organization = get_organization_context(request, kwargs, 'profile_sharing')
    try:
        public_profile = organization.public_profile
    except OrganizationPublicProfile.DoesNotExist:
        public_profile = OrganizationPublicProfile(
            organization=organization
        )
    if request.method == 'POST':
        if request.POST.get('public') == 'true':
            is_public = True
        elif request.POST.get('public') == 'false':
            is_public = False
        else:
            raise Exception("Bad is_public value")
        organization.public_profile.is_public = is_public
        organization.public_profile.save()
        return redirect('org_profile_edit', organization_slug=organization.slug)
    else:
        raise Exception("POST only!")


@login_required
@session_organization_required
def other_profile(request, *args, **kwargs):
    """
    Public profile
    """
    context,organization = get_organization_context(request, kwargs, 'other_profile')
    target_slug = kwargs['target_slug']
    target_organization = get_object_or_404(Organization, slug=target_slug)
    profile,created = OrganizationPublicProfile.objects.get_or_create(organization=target_organization)
    context['profile'] = profile
    context["target_organization"] = target_organization
    context.update({
        'page_title': target_organization.name,
        'page_title_secondary': _('organization'),
        # 'body_class': 'gray'
    })
    return render(request, 'org_public_profile/profile.html', context)
