from django.contrib import admin

from core.admin import BaseAdmin
from .models import *


class ProfilePhotoAdmin(BaseAdmin):
    list_display = ('profile', 'photo')
    ordering = ('-id',)

admin.site.register(ProfilePhoto, ProfilePhotoAdmin)


class OrganizationPublicProfileAdmin(BaseAdmin):
    list_display = ('organization', 'is_public', 'visible_sections')
    ordering = ('-id',)

admin.site.register(OrganizationPublicProfile, OrganizationPublicProfileAdmin)
