from django import forms
from django.utils.translation import ugettext_lazy as _
from org_public_profile.models import OrganizationPublicProfile


class ProfileFieldsForm(forms.Form):
    """
    Profile settings form.
    """
    business_card = forms.BooleanField(
        label=_("Business card"),
        required=False,
        initial=True,
    )
    photos = forms.BooleanField(
        label=_("Photos"),
        required=False,
        initial=True,
        widget=forms.CheckboxInput(attrs={
            "ng-model": "visibleSections.photos"
        })
    )
    certifications = forms.BooleanField(
        label=_("Certifications"),
        required=False,
        widget=forms.CheckboxInput(attrs={
            "ng-model": "visibleSections.certifications"
        })
    )
    products = forms.BooleanField(
        label=_("Products"),
        required=False,
        widget=forms.CheckboxInput(attrs={
            "ng-model": "visibleSections.products"
        })
    )
    map = forms.BooleanField(
        label=_("Location on map"),
        required=False,
        widget=forms.CheckboxInput(attrs={
            "ng-model": "visibleSections.map"
        })
    )

    def __init__(self, *args, **kwargs):
        # OrganizationPublicProfile instance
        try:
            self.instance = kwargs.pop('instance')
            self.initial = {}
            for visible_section in self.instance.visible_sections:
                self.initial[visible_section] = True
        except KeyError:
            pass
        super(ProfileFieldsForm, self).__init__(*args, **kwargs)

    def save(self):
        if self.instance:
            visible_sections_list = []
            section_names = [i[0] for i in OrganizationPublicProfile.SECTIONS_CHOICES]
            for key, value in self.cleaned_data.items():
                if key == 'business_card':
                    value = True
                if key in section_names and value:
                    visible_sections_list.append(key)
            self.instance.visible_sections = ','.join(visible_sections_list)
            self.instance.save()



