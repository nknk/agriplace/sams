from django.db import models
from django.utils.translation import ugettext_lazy as _

from organization.models import Organization, upload_to_org


class OrganizationPublicProfile(models.Model):

    organization = models.OneToOneField(Organization, related_name='public_profile')
    # comma separated list of section IDs to display, not including business_card
    SECTIONS_CHOICES = (
        ('business_card', _("Organization business card")),
        ('certifications', _("Certification list")),
        ('map', _("Location on map")),
        ('photos', _("Photos")),
        ('products', _("Product list"))
    )
    visible_sections = models.CharField(max_length=1000, default='business_card', blank=True)
    settings_json = models.TextField(blank=False, default='{}')
    is_public = models.BooleanField(default=True)
    profile_photo = models.ImageField(
        verbose_name=_(u'Profile photo'),
        upload_to=upload_to_org,
        blank=True, null=True)

    class Meta:
        db_table = 'OrganizationPublicProfile'
        permissions = (
            ('view_organizationpublicprofile', 'Can view organization public profile'),
        )

    def __unicode__(self):
        return u"[{}] public profile settings".format(self.organization.name)

    @property
    def visible_sections_list(self):
        return self.visible_sections.split(',')


class ProfilePhoto(models.Model):
    """
    Photo used in photo gallery on the organization public profile.
    """
    photo = models.ImageField(
        verbose_name=_(u'Photo'),
        upload_to=upload_to_org,
        blank=True, null=True)
    profile = models.ForeignKey('OrganizationPublicProfile', related_name='photos')

    class Meta:
        db_table = 'ProfilePhoto'
        permissions = (
            ('view_profilephoto', 'Can view profile photo'),
        )

    def __unicode__(self):
        return u"%s" % (self.id)

    @property
    def organization(self):
        return self.profile.organization
