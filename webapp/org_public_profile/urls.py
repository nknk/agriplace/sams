from django.conf.urls import patterns, url

urlpatterns = patterns(
    'org_public_profile.views',
    url(r'^our/profile/edit/$', 'own_profile', name='org_profile_edit'),
    url(r'^our/profile/edit/public/$', 'own_profile_sharing', name='org_profile_sharing'),
    url(r'^network/(?P<target_slug>.+)/$', 'other_profile', name='org_other_profile'),
)
