"""
Custom authentication models
Keeping them in separate module to avoid the module they're in importing other modules that call get_user_model()
"""

from django.contrib.auth.models import AbstractUser, AbstractBaseUser, BaseUserManager
from django.db import models



# class UserProfile(models.Model):
#     user = models.OneToOneField('User', related_name='profile')
#     language = models.CharField(max_length=100, blank=True)
#
#     def __unicode__(self):
#         return "{user} profile".format(user=self.user)

