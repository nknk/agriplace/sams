
import requests
import requests.exceptions

from rest_framework import status

class TeamcityError(Exception):

    def __init__(self):
        return

class Build(object):

    def __init__(self, json):
        self.__dict__ = json

class TeamcityClient(object):

    def __init__(self, base_url, username, password):
        self.base_url = base_url
        self.username = username
        self.password = password

    def _get_api_url(self, api_url):
        return self.base_url + '/httpAuth/app/rest' + api_url

    def _get_call(self, api_url, params={}):
        headers = { 'Accept': 'application/json' }

        try:
            r = requests.get(self._get_api_url(api_url), params=params, auth=(self.username, self.password), headers=headers)
        except RequestException:
            raise TeamcityError()
        except ConnectionError:
            raise TeamcityError()
        except HTTPError:
            raise TeamcityError()
        except URLRequired:
            raise TeamcityError()
        except TooManyRedirects:
            raise TeamcityError()
        except ConnectTimeout:
            raise TeamcityError()
        except ReadTimeout:
            raise TeamcityError()
        except Timeout:
            raise TeamcityError()

        if r.status_code != status.HTTP_200_OK:
            raise TeamcityError()

        return r.json()

    def builds(self, **kwargs):
        params = {}

        if 'branch' in kwargs:
            params['locator'] = "branch:%s" % kwargs['branch']

        return map(lambda json_build: Build(json_build),
            self._get_call('/builds/', params).get('build', []))

    def build(self, id):
        return Build(self._get_call("/builds/id:%s/" % id))

    def artifacts(self, id):
        return self._get_call("/builds/id:%s/artifacts/children/" % id)

    def stream(self, url):
        try:
            r = requests.get(url, auth=(self.username, self.password), stream=True)
        except RequestException:
            raise TeamcityError()
        except ConnectionError:
            raise TeamcityError()
        except HTTPError:
            raise TeamcityError()
        except URLRequired:
            raise TeamcityError()
        except TooManyRedirects:
            raise TeamcityError()
        except ConnectTimeout:
            raise TeamcityError()
        except ReadTimeout:
            raise TeamcityError()
        except Timeout:
            raise TeamcityError()

        if r.status_code != status.HTTP_200_OK:
            raise TeamcityError()

        return r
        
