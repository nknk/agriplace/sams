function add_row() {
	var old_row = $(".last-input-row");
	var new_row = $(".last-input-row").clone();
	old_row.toggleClass("last-input-row");
	new_row.find("input").val("");
	$("table").append(new_row);
}

//
// Makes sure there is always an empty input row at the end
// besides the one currently edited.
//
function check_rows(e) {
	console.log('check rows', this, a, b);
	var rows = $(".user-input-row");
	if(this == rows.last()) {
		add_row();
	}
	//rows.each(function(index, row) {
	//});
}

$(function() {
	$("table").bind('focusin', '.last-input-row', add_row);
});