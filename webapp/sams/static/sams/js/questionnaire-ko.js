'use strict';

if(typeof context == "undefined") {
	var context = {};
}

// //
// // Toggles other questions based on given question's current value
// //
// context.triggerOtherQuestions = function(context, eventData) {
	// var questionName = eventData.currentTarget.name;
	// var question = context.questions[questionName]; 
	// console.log("trigger questions. value:", question.answer());
	// var currentTriggers = question.triggers[question.answer()];
	// console.log("current triggers", currentTriggers);
	// if(typeof currentTriggers != undefined) {
		// for(var triggerId in currentTriggers) {
			// console.log("Triggering question.", currentTriggers[triggerId]);
			// context.questions["question-" + currentTriggers[triggerId]].state("active");
		// }		
	// }
// }
$(document).ready(function() {
    // Fixes bootstrap toggle button state based on input:radio state
    $(':input:checked').parent('.btn').addClass('active');
});


//
// Triggers question and its children recursivelly 
//
function triggerQuestion(questionName) {
	var question = context.questions[questionName];
	question.state("active");
	var triggers = question.triggers[question.answer()]
	// check whether this answer toggles other questions
	if(typeof triggers != "undefined") {
		// it does
		for(var tid=0; tid<triggers.length; tid++) {
			var triggeredQuestionName = "question-" + triggers[tid];
			console.log("triggering", triggeredQuestionName);
			triggerQuestion(triggeredQuestionName);
		}
	}
}

//
// Updates question status starting from initial questions.
//
context.updateQuestionStates = function(c, eventData) {
	// Hide all questions
	for(var qid in context.questions) {
		var question = context.questions[qid];
		if(!question.initially_visible) {
			question.state("inactive");
		}
	}
	// Trigger initial questions, recursivelly what depends on them
	for(var qid in context.questions) {
		var question = context.questions[qid];
		if(question.initially_visible) {
			triggerQuestion(question.name);
		}
	}
}


//
// Initializes view model
//
// @param questions JSON question data
context.initViewModel = function(questionData) {
	context.questions = {};
	for(var qid in questionData) {
		// start with bootstrapped questionData objects
		// 	{id, initially_visible, triggers[]}
		var question = questionData[qid];
		question.name = "question-" + question.pk;
		question.answer = ko.observable(question.value);
		question.state = ko.observable("inactive");
		question.helpTextVisible = ko.observable(false);
		if(questionData[qid].initially_visible) {
			question.state("active");
		}
		// question collection indexed by field name, like "question-ID"
		context.questions["question-" + question.pk] = question;
	}
}



context.clearQuestionnaire = function() {
	for(var qid in context.questions) {
		context.questions[qid].answer("");
	}
	context.updateQuestionStates();
	return false;
}


context.toggleHelpText = function(questionName) {
	var value = context.questions[questionName].helpTextVisible();
	context.questions[questionName].helpTextVisible(!value);
}


$(function() {
	context.initViewModel(questionData);
	
	// $(".question [data-initial='False']").parent().hide();
	
	// $(".question").change(function(a,b,c) {
		// console.log(a,b,c);
	// })
	ko.applyBindings(context);
});