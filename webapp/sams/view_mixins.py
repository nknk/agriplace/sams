from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import permissions

from organization.models import OrganizationUnit
from sams.decorators import session_organization_required, organization_required, membership_required
from sams.helpers import has_organization_permission, has_membership_permission


class SessionOrgRequired(object):

    @classmethod
    def as_view(cls, **initkwargs):
        view = super(SessionOrgRequired, cls).as_view(**initkwargs)
        return session_organization_required(view)


class OrganizationRequired(object):

    @classmethod
    def as_view(cls, **kwargs):
        view = super(OrganizationRequired, cls).as_view(**kwargs)
        return organization_required(view)


class MembershipRequired(object):

    @classmethod
    def as_view(cls, **kwargs):
        view = super(MembershipRequired, cls).as_view(**kwargs)
        return membership_required(view)


class GetUrlObjectsMixin:
    """
    Mixin for injecting into view classes objects based on url parameters
    Url parameters are set in urlpatterns.
    """
    ignore_url_args = ()


    def get_url_objects(self, url_args):
        if not self.request.user:
            raise Exception("No user session")

        # Organization by organization_slug
        if "organization_slug" not in self.ignore_url_args and url_args.has_key("organization_slug"):
            try:
                self.organization = self.request.user.organizations.get(slug=url_args["organization_slug"])
            except:
                raise Http404("User does not contain organization %s" % url_args["organization_slug"])

        # OrganizationUnit by unit_pk
        if "unit_pk" not in self.ignore_url_args and url_args.has_key("unit_pk"):
            unit = get_object_or_404(OrganizationUnit, pk=url_args["unit_pk"])
            if unit.organization not in self.request.user.organizations:
                raise Exception("Access denied to organization unit %s" % url_args["unit_pk"])

            self.unit = self.organization.units.get(pk=url_args["unit_pk"])

        # Product by product_pk
        if "product_pk" not in self.ignore_url_args and url_args.has_key("product_pk"):
            if not self.unit:
                raise Exception("Cannot get product without knowing organization unit")
            try:
                self.product = self.unit.products.get(pk=url_args["product_pk"])
            except:
                raise Http404("Organization unit does not have product %s" % url_args["product_pk"])

        # Assessment by assessment_pk
        if "assessment_pk" not in self.ignore_url_args and url_args.has_key("assessment_pk"):
            if not self.organization:
                raise Exception("Cannot get assessment without knowing organization")
            try:
                self.assessment = self.organization.assessments.get(pk=url_args["assessment_pk"])
            except:
                raise Http404("Organization does not have assessment %s" % url_args["assessment_pk"])


class OrganizationRequiredPermission(permissions.BasePermission):
    message = 'user organization is required'

    def has_permission(self, request, view):
        return has_organization_permission(request, view.kwargs)


class MembershipRequiredPermission(permissions.BasePermission):
    message = 'user membership is required'

    def has_permission(self, request, view):
        return has_membership_permission(request, view.kwargs)