from functools import wraps
from django.shortcuts import redirect, get_object_or_404
from organization.models import Organization, OrganizationMembership
from sams.helpers import get_object_for_user, PermissionDenied
from assessments.models import BackgroundEvidenceCopy


def session_organization_required(view_func):
    """ """
    @wraps(view_func)
    def decorator(request, *args, **kwargs):
        if not request.session.has_key('organization_pk'):
            if request.user.organizations.count() == 1:
                organization = request.user.organizations.all()[0]
                request.session['organization_pk'] = organization.pk
            else:
                return redirect('organization_list')
        return view_func(request, *args, **kwargs)
    return decorator


def organization_required(view_func):
    """ """
    @wraps(view_func)
    def decorator(request, *args, **kwargs):
        if kwargs.get('organization_slug'):
            organization = get_object_for_user(
                user=request.user,
                object_class=Organization,
                slug=kwargs.get('organization_slug')
            )
        elif kwargs.get('organization_pk'):
            organization = get_object_for_user(
                user=request.user,
                object_class=Organization,
                pk=kwargs.get('organization_pk')
            )
        else:
            raise PermissionDenied("Organization does not exist")

        if organization not in request.user.organizations.all():
            raise PermissionDenied("User does not have access to organization.")
        else:
            request.organization = organization
        return view_func(request, *args, **kwargs)
    return decorator


def membership_required(view_func):
    """ """
    @wraps(view_func)
    def decorator(request, *args, **kwargs):
        if kwargs.get('membership_pk'):
            membership = get_object_or_404(OrganizationMembership, pk=kwargs.get('membership_pk'))
            organization = get_object_for_user(
                user=request.user,
                object_class=Organization,
                pk=membership.secondary_organization.pk
            )
        else:
            raise PermissionDenied("Membership does not exist")

        if organization not in request.user.organizations.all():
            raise PermissionDenied("User does not have access to organization.")
        else:
            request.membership = membership
            request.organization = membership.secondary_organization
            user_profile = request.user.profile
            if user_profile.default_membership != request.membership:
                user_profile.default_membership = request.membership
                user_profile.save()
        return view_func(request, *args, **kwargs)
    return decorator


def block_for_background_copying(url_key=None, exception_type=None):
    def wrap_func(view_func):
        @wraps(view_func)
        def decorator(request, *args, **kwargs):
            assessment_id = None
            if url_key:
                if not kwargs.get(url_key) and len(kwargs.get(url_key,'')) != 22:
                    raise PermissionDenied("Given url is not valid.")
                assessment_id = kwargs.get(url_key,'')
            elif kwargs.get("assessment_pk"): assessment_id = kwargs.get("assessment_pk",'')

            if assessment_id and BackgroundEvidenceCopy.objects.filter(
                    target_assessment__uuid=assessment_id, status="pending").exists():
                if exception_type: raise exception_type
                raise PermissionDenied("Assessment reuse running in background.")
            return view_func(request, *args, **kwargs)
        return decorator
    return wrap_func
