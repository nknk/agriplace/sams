"""
Context processors
"""

import json

import re
from django.conf import settings
from django.contrib import messages
from django.utils.translation import ugettext as _

from assessments.models import Question
from farm_group.utils import get_user_info
from sams.forms import QuestionnaireForm


def questionnaire_form_context_processor(request, context, questionnaire,
                                         target=None, assessment=None):
    """
    Adds needed context variables (questions_json) for questionnaire edit
    partial based on QuestionnaireForm data.

    !! If called during a POST request, tries to save form data.

    :param: context: Context dict to process
    :param: questionnaire: Questionnaire to edit
    :param target: Target object for answers
    :param assessment: Assessment, if applicable
    :raises: Exception
    :returns: True for success, you should redirect. False: not done, render
    the form (first time or errors)
    """
    context["questionnaire"] = questionnaire
    context["target"] = target
    context["assessment"] = assessment
    context["known_editors"] = [choice[0] for choice in
                                Question.ANSWER_TYPE_CHOICES]  # (code, label)

    if request.method == "POST":
        # POST
        form = QuestionnaireForm(request.POST, questionnaire=questionnaire,
                                 target=target, assessment=assessment)
        if form.is_valid():
            form.save()
            messages.success(request, _("Answers saved."))
            return True
        else:
            print 'form not valid!'
    else:
        # GET
        form = QuestionnaireForm(questionnaire=questionnaire, target=target,
                                 assessment=assessment)

    form_boundfields = {bf.name: bf for bf in form}

    # Collection of actual element objects (subclasses of QuestionnaireElement) ordered by order_index
    # Display this in sequence to get questionnaire
    context['questionnaire_elements'] = questionnaire.element_objects
    # Patch form fields (when available) directly into element object, to make it easier to work with in template
    for element in context['questionnaire_elements']:
        try:
            field_name = 'question-' + str(element.pk)
            element.__dict__['bound_field'] = form_boundfields[field_name]
        except KeyError:
            pass
    # Index fields by element so we can find them in the template
    context['form_fields_by_element_pk'] = {el: form.fields.get('question-'+str(el.pk))
                                            for el in context['questionnaire_elements']}

    questions_json = []
    for question in questionnaire.questions.select_related('possible_answers').all():
        q = {
            "pk": question.pk,
            "initially_visible": question.initially_visible,
            "triggers": {},  # {'value': [qids]},
            "value": form.initial.get("question-%s" % question.pk)
        }
        #for possible_answer in question.possible_answers.all():
        #    questions = possible_answer.triggered_questions.all()
        #    for triggered_question in questions:
        #        triggers = q["triggers"].setdefault(possible_answer.id, [])
        #        triggers.append(triggered_question.id)
        questions_json.append(q)
    context["form"] = form
    context["questions_json"] = json.dumps(questions_json)
    return False


def piwik(request):
    return {'PIWIK': settings.PIWIK}

def google_analytics(request):
    context = {'GA_PROPERTY_ID': settings.GA_PROPERTY_ID}

    if request.session.get('activated'):
        context['USER_ACTIVATED'] = 'yes'
        del request.session['activated']
        
    return context

def sentry_dsn(request):
    if getattr(settings, 'RAVEN_CONFIG', None):
        dsn = settings.RAVEN_CONFIG['dsn']
        js_dsn = re.sub(r'^(http[s]?://[^:]+):[^@]+(@.*)$', r'\1\2', dsn)
        return {'SENTRY_JS_DSN': js_dsn}
    else:
        return {}

def agriplace_global_context(request):
    global_options = set([
      'AGRIPLACE_ENV_NAME',
      'RVO_ENABLED',
    ])

    #
    # Take only selected options into the global context
    #
    selected_setting_names = filter(lambda key: hasattr(settings, key), global_options)
    return dict(map(lambda key: (key, getattr(settings, key)), selected_setting_names))

def agriplace_global_config(request):
    global_options = set([
      'GA_PROPERTY_ID',
      'AGRIPLACE_ENV_NAME',
      'RVO_ENABLED',
    ])

    #
    # Take only selected options into the global config
    #
    selected_setting_names = filter(lambda key: hasattr(settings, key), global_options)
    selected_settings = dict(map(lambda key: (key, getattr(settings, key)), selected_setting_names))
    return { 'agriplace_global_config': 'var agriplace_global_config = ' + json.dumps(selected_settings) + ';' }


def layout(request):
    return get_user_info(request)


