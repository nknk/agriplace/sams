from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from import_export import fields
from import_export import resources

from urllogin.models import Key


class UserResource(resources.ModelResource):
    organization_list = fields.Field()
    sso_key = fields.Field()
    activation_link = fields.Field()

    def dehydrate_organization_list(self, user):
        return u','.join([organization.name for organization in user.organizations.all()])

    def dehydrate_sso_key(self, user):
        try:
            return Key.objects.get(user=user).key
        except ObjectDoesNotExist:
            return ""

    def dehydrate_activation_link(self, user):
        try:
            if user.registration_profile.activation_key == "ALREADY_ACTIVATED":
                return "User already activated"
            domain = Site.objects.get_current().domain

            return 'https://%s%s' % (domain, reverse(
                'registration_activate',
                kwargs=dict(
                    activation_key=user.registration_profile.activation_key,
                )
            ))
        except ObjectDoesNotExist:
            return "Activation link not available for this user"

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'is_active', 'organization_list', 'sso_key',
                  'activation_link')
        export_order = ('username', 'first_name', 'last_name', 'is_active', 'organization_list', 'sso_key',
                        'activation_link')
