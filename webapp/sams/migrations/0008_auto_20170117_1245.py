# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sams', '0007_auto_20170113_0725'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='default_membership',
            field=models.ForeignKey(related_name='profile', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='organization.OrganizationMembership', null=True),
        ),
    ]
