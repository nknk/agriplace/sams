# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sams', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='platform_context',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'agriplace', b'Agriplace'), (b'hzpc', b'Hzpc')]),
            preserve_default=True,
        ),
    ]
