# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0019_auto_20170113_0725'),
        ('sams', '0006_userprofile_default_organization'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='default_organization',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='default_membership',
            field=models.ForeignKey(related_name='profile', blank=True, to='organization.OrganizationMembership', null=True),
        ),
    ]
