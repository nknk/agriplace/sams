# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sams', '0002_userprofile_platform_context'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='platform_context',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'agriplace', b'Agriplace'), (b'hzpc', b'Hzpc'), (b'farm_group', b'FarmGroup')]),
        ),
    ]
