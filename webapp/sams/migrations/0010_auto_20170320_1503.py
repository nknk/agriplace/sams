# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sams', '0009_auto_20170320_1500'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='language',
            field=models.CharField(default=b'nl', max_length=100, choices=[(b'en', b'English'), (b'nl', b'Nederlands'), (b'es', 'Espa\xf1ol')]),
        ),
    ]
