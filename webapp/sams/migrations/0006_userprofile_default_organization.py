# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0014_organizationmembership_role'),
        ('sams', '0005_auto_20161103_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='default_organization',
            field=models.ForeignKey(related_name='profile', blank=True, to='organization.Organization', null=True),
        ),
    ]
