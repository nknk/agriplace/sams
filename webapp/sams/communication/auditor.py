import os
import requests

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.utils.translation import get_language, ugettext as _
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from sekizai.context import SekizaiContext

from rest_framework import status

from raven.contrib.django.raven_compat.models import client
from core.constants import EXTERNAL_AUDITOR
from organization.models import OrganizationMembership

from sams.mailgun import Mailgun, MailgunError
from sams_project.settings import AGRIPLACE_FARM_GROUP_SLUG


def assessment_shared(request, assessment, auditor):
    auditor_membership = OrganizationMembership.objects.get(
        primary_organization=request.membership.primary_organization,
        secondary_organization=auditor,
        role__name=EXTERNAL_AUDITOR
    )
    organization = assessment.organization

    template_html = 'communication/auditor/assessment_shared.html'
    template_text = 'communication/auditor/assessment_shared.txt'

    environment = os.environ.get('AGRIPLACE_ENV_NAME', 'devel')

    to = auditor.email if environment == 'production' else settings.SANDBOX_EMAIL
    from_email = "AgriPlace Team <%s>" % settings.DEFAULT_FROM_EMAIL

    subject = "[AgriPlace] " + _("Assessment shared")

    location = reverse('shared_assessment_detail', args=[auditor_membership.uuid, assessment.uuid])

    organization_data = (
        organization.name + '\n'
        + ((organization.mailing_address1 + '\n') if organization.mailing_address1 else '')
        + ((organization.mailing_address2 + '\n') if organization.mailing_address2 else '')
        + ((organization.mailing_city + '\n') if organization.mailing_city else '')
        + ((organization.mailing_province + '\n') if organization.mailing_province else '')
        + ((organization.mailing_postal_code + '\n') if organization.mailing_postal_code else '')
        + ((organization.mailing_country + '\n') if organization.mailing_country else '')
    )

    context = {
        'assessment_url': request.build_absolute_uri(location),
        'assessment': assessment,
        'organization': organization,
        'organization_data': organization_data,
        'auditor': auditor
    }

    text = render_to_string(template_text, context)
    html = render_to_string(template_html, SekizaiContext(context))

    try:
        mailgun = Mailgun()
        mailgun.send(from_email, to, subject, text=text)
    except MailgunError:
        client.captureException()
