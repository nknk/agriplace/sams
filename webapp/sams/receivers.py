from django.conf import settings
from django.contrib.auth.signals import user_logged_in
from django.dispatch import receiver
from django.utils import translation

@receiver(user_logged_in)
def on_user_logged_in(sender, request, **kwargs):
    """
    Login signal receiver
    """
    try:
        request.session[translation.LANGUAGE_SESSION_KEY] = request.user.profile.language
    except:
        print "Could not set language."
    # print "on user logged in: LANGUAGE ", request.user.profile.language, request.session[translation.LANGUAGE_SESSION_KEY]

