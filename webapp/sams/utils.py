from django.db.models import Count
from core.constants import EXTERNAL_AUDITOR
from organization.models import OrganizationMembership, DEFAULT_LAYOUT


def id_to_hex(id):
    return hex(id)[2:]


def hex_to_id(hex):
    return int(hex, 16)


def get_layout_context(request):
    organization = None
    if getattr(request, 'membership', None):
        organization = request.membership.primary_organization
    elif getattr(request.user, 'profile', None) and getattr(request.user.profile, 'default_membership', None):
        organization = request.user.profile.default_membership.primary_organization

    if getattr(organization, 'theme', None):
        return organization.theme.layout
    else:
        return DEFAULT_LAYOUT


def get_user_memberships(user):
    external_auditor_organizations = OrganizationMembership.objects.filter(
        secondary_organization__users=user, role__name=EXTERNAL_AUDITOR
    ).values('secondary_organization').annotate(dcount=Count('uuid'))
    memberships = list(OrganizationMembership.objects.filter(
        secondary_organization__users=user
    ).exclude(
        role__name=EXTERNAL_AUDITOR
    ))
    for organization in external_auditor_organizations:
        external_membership = OrganizationMembership.objects.filter(
            secondary_organization=organization.get('secondary_organization'),
            role__name=EXTERNAL_AUDITOR
        ).first()
        memberships.append(external_membership)
    memberships = sorted(memberships, key=lambda x: x.secondary_organization)
    return memberships


def get_layout(organization):
    if getattr(organization, 'theme', None):
        return organization.theme.layout
    else:
        return DEFAULT_LAYOUT


def get_user_default_membership(user):
    user_profile = user.profile
    default_membership = user_profile.default_membership if user_profile else None
    default_membership = default_membership or OrganizationMembership.objects.filter(
        secondary_organization__in=user.organizations.all()
    ).first()
    return default_membership
