# -*- encoding: utf-8 -*-
from collections import defaultdict

from django import forms
from django.contrib.admin import site
from django.contrib.admin.widgets import ForeignKeyRawIdWidget
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from assessments.models import PossibleAnswer, Question, Answer
from organization.models import OrganizationMembership, Organization
from sams.models import UserProfile


class UserAbpCreateForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = ['email', 'first_name', 'last_name', ]


class AssessmentRequestForm(forms.Form):
    product = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)


class RegistrationInvitationForm(forms.Form):
    ''' Invitation to this application '''
    name = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)

#    subject = forms.CharField(max_length=100)
#    message = forms.CharField()
#    sender = forms.EmailField()
#    cc_myself = forms.BooleanField(required=False)


class QuestionnaireForm(forms.Form):
    """
    Questionnaire form

    Questionnaire form for generic Assessment/Answer models.
    Initializes self with fields based on given questionnaire and values based on given assessment+target.

    Kwargs:
        questionnaire: Questionnaire, required
        target: Questionnaire target or None
        assessment: Assessment or None
    """

    def __init__(self, *args, **kwargs):
        self.questionnaire = kwargs.pop("questionnaire")
        self.target = kwargs.pop("target")
        self.assessment = kwargs.pop("assessment")
        super(QuestionnaireForm, self).__init__(*args, **kwargs)
        # Can start adding fields only after initializing Form object
        self.add_question_fields()
        self.add_initial_values()


    def add_question_fields(self):
        """
        Adds the given questionnaire's questions as form fields.

        We are only adding the questions to the form, ignoring the other questionnaire elements
        because the role of this form is to prepare the initial values and especially to
        process the answer values coming back.
        For rendering the full questionnaire, the views look directly at the questionnaire elements collection.
        """
        # A bit of hoop jumping to get questions and possible answers with a single query
        # instead of one query / question!
        answers = PossibleAnswer.objects.select_related('question').filter(question__questionnaire=self.questionnaire)
        # Dict of question: list of possible answers
        question_answers_dict = defaultdict(list)
        for answer in answers:
            question_answers_dict[answer.question.pk].append(answer)

        for question in self.questionnaire.questions:
            possible_answers = question_answers_dict[question.pk]
            # Compile a list of possible answers for questions that have them
            # For every possible answer make a (value, label) tuple that will
            # be used by the editor

            possible_answers_tuples = (
                (pa.value, pa.text) for pa in possible_answers
            )
            possible_answers = [("", "---")]
            for answer_tuple in possible_answers_tuples:
                possible_answers.append(answer_tuple)
            boolean_possible_answers = (
                ("", "---"),
                ("yes", _("Yes")),
                ("no", _("No"))
            )
            # (field_class, {kwargs})
            form_field_mapping = {
                "dropdown": (forms.ChoiceField, {"choices": possible_answers}),
                "list_single": (forms.ChoiceField, {"choices": possible_answers}),
                "memo": (forms.CharField, {"widget": forms.Textarea}),
                "checkboxes": (forms.MultipleChoiceField, {"choices": possible_answers}),
                "numeric": (forms.DecimalField, {}),
                "radio": (forms.ChoiceField, {"choices": possible_answers}),
                "scale": (forms.IntegerField, {}),
                "text": (forms.CharField, {}),
                "yesno": (forms.ChoiceField, {"choices": boolean_possible_answers})
            }
            # Get field definition from above options
            field_definition = (form_field_mapping.get(question.answer_type)
                                or form_field_mapping.get("text"))
            # Create field instance
            field = field_definition[0](**field_definition[1])
            # Set attributes of field for KnockoutJS
            field.widget.attrs = {
                "data-bind": (
                    u"value: context.questions['question-'+{0}].answer, ".format(question.pk) +
                    "event: {change: updateQuestionStates}"
                )
            }
            field.label = question.text
            field.help_text = question.criteria
            field.question = question


            # Initial answer: existing answer or default or None

            field.required = False
            field_id = "question-%s" % question.pk
            self.fields[field_id] = field

            # Justification field:
            # We also need an 'answer.justification' field for every 'answer.value' field.
            justification_field_id = u'justification-{question.pk}'.format(question=question)
            self.fields[justification_field_id] = forms.CharField()


    def add_initial_values(self):
        """
        Adds existing answers as initial values to current form
        """
        # TODO Check what's going on here
        if self.assessment:
            questionnaire_detail = self.questionnaire.get_detail(self.assessment)
        else:
            questionnaire_detail = {
                "questions": {}
            }
        # fields in the form are named question-ID
        for detail in questionnaire_detail["questions"].values():
            field_id = "question-%s" % detail["question_object"].pk
            self.initial[field_id] = detail["answer_value"]

    def save(self):
        """
        Saves answers to Answer objects connected to Questionnaire[+Assessment]
        """
        # Get all possible answers of this questionnaire
        #possible_answers = (PossibleAnswer.objects
        #                .filter(question__questionnaire=self.questionnaire))

        # Get all answers of this assessment/questionnaire combo
        if self.target:
            target_uuid = self.target.uuid
        else:
            target_uuid = None
        # Get existing answers to this questionnaire for the given
        # target+assessment combination
        questionnaire_detail = self.questionnaire.get_detail(self.assessment,
                                                          target_uuid)

        # We want a {question_pk: answer_object} dict
        # but questionnaire_detail["questions"] is indexed by question code
        # so we transform one dict into another
        # TODO: Can we just use question codes in the html form?
        answers_by_question_pk = {
            str(qd["question_object"].pk): qd["answer_value"]
            for qd in questionnaire_detail["questions"].values()
        }

        for key, form_value in self.cleaned_data.iteritems():
            # form contains answers in items named like 'question-QUESTION_PK'
            if not key.startswith("question-"):
                continue
            # Null values don't count as answers, but empty strings do
            if not form_value:
                continue
            # so if we remove the 'question-' part, we're left with the
            # question PK
            question_pk = key.replace("question-", "")
            question = Question.objects.get(pk=question_pk)

            # if question.possible_answers.count():
            #     # TODO: Make sure this is using a pre-fetched collection and
            #     # isn't making one query / object
            #     possible_answer = possible_answers.get(code=form_value)
            #     new_answer_value = possible_answer.value
            # else:
            #     new_answer_value = form_value

            # The form_value of a field is the actual value for regular editors
            # or the PossibleAnswer code for questions that have possible
            # answers.

            old_answer_value = answers_by_question_pk.get(question_pk)

            # Create a new answer if we don't have one OR if we are saving
            # a different value
            if form_value != old_answer_value:
                answer = Answer(
                    question=question,
                    target_uuid=target_uuid,
                    assessment=self.assessment,
                    value=form_value
                )
                answer.save()



class AnswersForm(forms.Form):
    """
    Form that gets its initial values from and saves answers to Answers.
    """

    def __init__(self, *args, **kwargs):
        assert 'target_uuid' in kwargs.keys()


class UserUpdateForm(forms.Form):
    first_name = forms.CharField(label=_("Full name"), required=True)
    phone_number = forms.CharField(required=False, max_length=100,
                                   label=_("Phone number"), widget=forms.TextInput(attrs={'class': "form-control"}))
    LANGUAGE_CHOICES = (
        ('nl', "Nederlands"),
        ('en', "English"),
        #("fr", "Francais"),
        ('es', u"Español"),
        # ('ru', u"русский"),
        # ('id', u"Bahasa indonesia")
    )
    language = forms.ChoiceField(choices=LANGUAGE_CHOICES, widget=forms.Select(attrs={'class': "form-control"}))

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('instance', None)
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        if user:
            self.user = user
            self.initial = {
                'first_name': user.first_name,
                'language': user.profile.language,
                'phone_number': user.profile.phone
            }

    def save(self):
        assert self.user
        self.user.first_name = self.cleaned_data['first_name']
        self.user.save()
        profile = self.user.profile
        profile.language = self.cleaned_data['language']
        profile.phone = self.cleaned_data['phone_number']
        profile.save()


class EmailChangeForm(forms.ModelForm):
    email = forms.EmailField(label=_("New email address"))
    class Meta:
        model = get_user_model()
        fields = ('email', )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(EmailChangeForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        self.user.email = self.cleaned_data['email']
        if commit:
            self.user.save()
        return self.user


class UserProfileAdminForm(forms.ModelForm):
    default_membership = forms.ModelChoiceField(
        queryset=OrganizationMembership.objects.all(),
        widget=ForeignKeyRawIdWidget(UserProfile._meta.get_field('default_membership').rel, site),
        required=False
    )

    class Meta:
        model = UserProfile
        fields = '__all__'
