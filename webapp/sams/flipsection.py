from django.contrib import messages
from django.core.urlresolvers import reverse
from django.forms.models import model_to_dict
from django.shortcuts import render, redirect


class FlipSectionManager:

    def __init__(self, instance, section_models):
        """
        Expected section_models dict looks like this:
        {
            {SECTION ID}: {
                'form_class': {SECTION CLASS},
                'title': {SECTION_TITLE}
            }, ...
        }
        """
        self.instance = instance
        self.section_models = section_models
        self.process_flip_section_models()

    def process_flip_section_models(self):
        """
        Processes section_models dict for use with flip_section templates for a given instance.


        This function does the following for each section:
        * creates `form` object, using the given `instance` object
        * creates `form_detail` list of all fields and their displayed value label (also for ChoiceFields),
        to be used as a detail view.

        :return:
        The returned enriched model will look like this:
        {
            <SECTION ID>: {
                'form_class': <SECTION CLASS>,
                'form': <FORM OBJECT>,
                'form_detail': [
                    {
                        'name': <FIELD NAME>,
                        'label': <FIELD LABEL>,
                        'value': <FIELD VALUE>
                    }
                ],
                'title': {SECTION_TITLE}
            }, ...
        }
        """
        # Add instances of all form classes to the context
        for section_name, section_model in self.section_models.items():
            # Real form for edit section
            form = section_model['form'] = section_model['form_class'](instance=self.instance)
            # Temporary form to generate detail view. Never gets rendered.
            instance_dict = model_to_dict(self.instance)
            temp_form = section_model['form_class'](instance_dict)
            temp_form.is_valid() # Validating form creates cleaned_data dict


            # Replace field names with verbose_names
            section_model['form_detail'] = []
            # Follow the order of the form's fields
            for field_name in temp_form.Meta.fields:
                if field_name in temp_form.errors.keys():
                    # Some fields will not validate (like slug, because uniqueness check).
                    # We don't care about that, so just get the value directly from the instance dict.
                    field_value = instance_dict[field_name]
                else:
                    field_value = temp_form.cleaned_data[field_name]
                section_model['form_detail'].append({
                    'name': field_name,
                    'label': temp_form.fields[field_name].label,
                    'value': field_value
                })

    def save(self, post_data, post_files):
        """
        Saves flip section data on POST.
        :return: (success, url_hash) The hash to redirect to.
        """
        section_name = post_data['section']
        section_model = self.section_models[section_name]
        # Section name is a valid key in dict, otherwise this will raise an exception
        form_class = section_model['form_class']
        form = form_class(post_data, post_files, instance=self.instance)
        section_model['form'] = form
        if form.is_valid():
            form.save()
            success = True
            url_hash = "#ok"
        else:
            success = False
            url_hash = u'#s/{0}/edit'.format(section_name)
        return (success, url_hash)


