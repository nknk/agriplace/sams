import sys
import json
from optparse import make_option
from netrc import netrc
from urlparse import urljoin
from tempfile import mkstemp
from getpass import getpass

import requests
from tabulate import tabulate
from heroku3.api import Heroku
from clint.textui import progress
from django.core.management.base import BaseCommand, CommandError

from sams.teamcity import *

BASE_HOST = 'builds.agriplace.com'

def http_resource(self, method, resource, params=None, headers=None, data=None):
    """Makes an HTTP request."""

    url = self._url_for(*resource)

    r = self._session.request(method, url, params=params, headers=headers, data=data)

    if r.status_code == 422:
        http_error = RuntimeError('%s Client Error: %s' % (r.status_code, r.content))
        http_error.response = r
        raise http_error

    r.raise_for_status()

    return r

class Command(BaseCommand):
    help = ('Deploy specified build')
    args = ""

    option_list = BaseCommand.option_list + (
            make_option('--branch', '-b',
                dest='branch',
                help=('Branch to query releases from')),
            )

    def _select_app(self, heroku, environment):
      if not environment:
        environment = 'agriplace-testing'

      app_name = "%s" % environment

      for app in heroku.apps():
        if app.name == app_name:
          return app
      raise RuntimeError('No suitable app found')

    def _announce_slug(self, app, celery):
      print "Announcing slug to heroku app %s" % app.name

      headers = { 'Content-Type': 'application/json', 'Accept': 'application/vnd.heroku+json; version=3' }
      if celery:
        data = '{"process_types":{"web":"PYTHONPATH=webapp .heroku/python/bin/waitress-serve --port=$PORT sams_project.wsgi:application", "worker": "PYTHONPATH=webapp .heroku/python/bin/celery worker --app=sams_project.celery.app -B -l info"}}'
      else:
        data = '{"process_types":{"web":"PYTHONPATH=webapp .heroku/python/bin/waitress-serve --port=$PORT sams_project.wsgi:application"}}'

      r = http_resource(
          app._h,
          "POST",
          ('apps', app.name, 'slugs'),
          headers=headers,
          data=data
      )

      return r.json()

    def _pipe_slug(self, teamcity, from_url, to_url, length):
      print "Downloading slug from TeamCity"

      r_from = teamcity.stream(from_url)

      fd, path = mkstemp('apdeploy')

      total_length = int(r_from.headers.get('content-length', length))

      with open(path, 'wb') as f:
        for chunk in progress.bar(r_from.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1):
            if chunk:
                f.write(chunk)
                f.flush()

      print "Uploading the slug to Heroku"

      with open(path, 'rb') as f:
        r_to = requests.put(to_url, data=f, headers={'Content-Type':''})

    def handle(self, *args, **options):
        try:
            credentials = netrc().authenticators(BASE_HOST)

            if credentials == None:
              heroku_credentials = netrc().authenticators('api.heroku.com')

              if heroku_credentials != None:
                username = raw_input("Your TeamCity login name (%s): " % heroku_credentials[0]).strip()

                if username == '':
                  username = heroku_credentials[0]

                password = getpass("Your TeamCity password: ")
              else:
                username = raw_input("Your TeamCity login name: ").strip()
                password = getpass("Your TeamCity password: ")

              credentials = (username, None, password)

            teamcity_base_url = 'http://' + BASE_HOST

            teamcity = TeamcityClient(teamcity_base_url, credentials[0], credentials[2])

            #
            # when no args are speciifed, simply list all builds
            #
            if len(args) == 0:
              builds = teamcity.builds(branch=options['branch']) if 'branch' in options and options['branch'] else teamcity.builds()

              builds.sort(key=lambda build: build.id, reverse=False)

              print tabulate(map(lambda build: [build.id, '#' + build.number, build.branchName, build.status], builds),
                  headers=['Id', '#', 'Branch Name', 'Status'])

              sys.exit(0)

            is_branch_name_passed = '-' in args[0]

            #
            # Get last build id if user passed branchname
            #
            if is_branch_name_passed:
              builds = teamcity.builds(branch=args[0])
              if not builds:
                print "No builds with branch name '%s' found" % args[0]
                sys.exit(0)

              builds.sort(key=lambda build: build.id, reverse=False)

              last_build = builds[-1]

              build_id = str(last_build.id);
            else:
              build_id = args[0]

            build = teamcity.build(build_id)

            if build == None:
              print "No build with id '%s' found" % build_id

            print "Fetching artifact metadata"

            artifact = teamcity.artifacts(build_id)

            credentials = netrc().authenticators('api.heroku.com')

            if credentials == None:
              raise RuntimeError('Your are not logged in to Heroku. Please login to Heroku first');

            heroku = Heroku()
            heroku.authenticate(credentials[2])

            app = self._select_app(heroku, args[1] if len(args) > 1 else 'agriplace-testing')

            celery = len(args) > 2 and args[2] == 'celery' or False

            slug = self._announce_slug(app, celery)

            from_url = urljoin(teamcity_base_url, artifact['file'][0]['content']['href'])

            self._pipe_slug(teamcity, from_url, slug['blob']['url'], artifact['file'][0]['size']);

            print "Releasing the slug"

            headers = { 'Content-Type': 'application/json', 'Accept': 'application/vnd.heroku+json; version=3' }
            data = { "slug": slug['id'], 'description': build.branchName + ':#' + build.number }

            release = http_resource(
                app._h,
                "POST",
                ('apps', app.name, 'releases'),
                headers=headers,
                data=json.dumps(data)
            )

            print "Done."
        except TeamcityError:
            raise CommandError(error_text % 'Unable to fetch builds from TeamCity.')
