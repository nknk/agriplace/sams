"""
Initialize SAMS database objects
Creates needed SAMS platform database objects, such as fact sheets and assessment types. To be ran once on new database.
"""

from termcolor import colored

from django.core.management.base import BaseCommand, CommandError

from assessments.models import AssessmentType, Questionnaire


class Command(BaseCommand):
    args = ""
    help = "Creates needed SAMS system objects. Run once on new database."

    def handle(self, *args, **options):
        self.create_assessment_types()
        self.create_fact_sheets()

    def create_assessment_types(self):
        """
        Creates AssessmentType objects
        """
        self.stdout.write("\nCreating assessment types...")
        self.stdout.write("----------------------------")
        assessment_types = [
            {"code":"sai", "name":"SAI Assessment"},
            {"code":"fourstyles", "name":"Four Styles Assessment"},
            {"code":"gat", "name":"Gap Assessment Tool"}
        ]
        for at_data in assessment_types:
            # Don't recreate existing records
            if AssessmentType.objects.filter(code=at_data["code"]).count():
                self.stdout.write(colored(u"E AssessmentType %s" % at_data["name"], "white"))
                continue
            AssessmentType(
                code=at_data["code"],
                name=at_data["name"]
            ).save()
            self.stdout.write(colored(u"\u2713 AssessmentType %s" % at_data["name"], "green"))

    def create_fact_sheets(self):
        """
        Creates factsheet questionnaires
        """
        self.stdout.write("\nCreating fact sheets...")
        self.stdout.write("-----------------------")
        fact_sheet_questionnaires = [
            {
                "code": "_fact_sheet_organization",
                "name": "Organization fact sheet"
            },
            {
                "code": "_fact_sheet_organization_unit",
                "name": "Organization unit fact sheet"
            },
            {
                "code": "_fact_sheet_product",
                "name": "Product fact sheets"
            }
        ]
        for fact_sheet in fact_sheet_questionnaires:
            # Don't recreate existing records
            if Questionnaire.objects.filter(code=fact_sheet["code"]).count():
                self.stdout.write(colored(u"E Questionnaire %s" % fact_sheet["name"], "white"))
                continue
            questionnaire = Questionnaire(
                code=fact_sheet["code"],
                name=fact_sheet["name"]
            )
            questionnaire.save()
            self.stdout.write(colored(u"\u2713 Questionnaire %s" % fact_sheet["name"], "green"))