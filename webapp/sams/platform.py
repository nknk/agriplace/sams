import json

from .models import PlatformEvent


def platform_event(event_type, text, url_data=None, organization=None, user=None):
    """
    Create platform event

    Raises:
        Exception if organization not found
    """
    if url_data:
        url_json = json.dumps(url_data)
    event = PlatformEvent(**{
        "event_type": event_type,
        "organization": organization,
        "user": user,
        "text": text,
        "url_json": url_json
    })
    event.save()
