from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from core.constants import AGRIPLACE_GROWER
from organization.models import OrganizationType, Organization, OrganizationMembership
from sams_project.settings import AGRIPLACE_FARM_GROUP_SLUG


class PermissionDenied(Exception):
    """ User can't access the object. """
    pass


def get_object_for_user(user, object_class, action='write', **kwargs):
    obj = object_class.objects.get(**kwargs)
    # Can the model check permissions?
    if not getattr(obj, "check_permissions", None):
        raise Exception("Model %s doesn't implement check_permissions()"
                        % obj)
    if not obj.check_permissions(user, action):
        raise PermissionDenied("Permission denied")
    return obj


def get_object_for_organization(organization, object_class, action='write', **kwargs):
    obj = object_class.objects.get(**kwargs)
    if not getattr(obj, "check_permissions_organization", None):
        raise Exception("Model %s doesn't implement check_permissions_organization()"
                        % obj)
    if not obj.check_permissions_organization(organization, action):
        raise PermissionDenied("Permission denied")
    return obj


def create_grower_organization_relationship(new_organization):
    grower_grp, created = Group.objects.get_or_create(name=AGRIPLACE_GROWER)
    org_type = OrganizationType.objects.get(slug='producer')
    agriplace_farmgroup = Organization.objects.get(slug=AGRIPLACE_FARM_GROUP_SLUG)
    OrganizationMembership.objects.create(
        primary_organization=agriplace_farmgroup,
        secondary_organization=new_organization,
        organization_type=org_type,
        role=grower_grp
    )


def has_organization_permission(request, kwargs):
    if kwargs.get('organization_slug'):
        organization = get_object_for_user(
            user=request.user,
            object_class=Organization,
            slug=kwargs.get('organization_slug')
        )
    elif kwargs.get('organization_pk'):
        organization = get_object_for_user(
            user=request.user,
            object_class=Organization,
            pk=kwargs.get('organization_pk')
        )
    else:
        raise PermissionDenied("Organization does not exist")

    if organization not in request.user.organizations.all():
        raise PermissionDenied("User does not have access to organization.")
    else:
        request.organization = organization
    return True


def has_membership_permission(request, kwargs):
    if kwargs.get('membership_pk'):
        membership = get_object_or_404(OrganizationMembership, pk=kwargs.get('membership_pk'))
        organization = get_object_for_user(
            user=request.user,
            object_class=Organization,
            pk=membership.secondary_organization.pk
        )
    else:
        raise PermissionDenied("Membership does not exist")

    if organization not in request.user.organizations.all():
        raise PermissionDenied("User does not have access to organization.")
    else:
        request.membership = membership
        request.organization = membership.secondary_organization
    return True
