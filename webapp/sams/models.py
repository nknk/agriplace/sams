import datetime
import json

from django.conf import settings
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _

from core.models import UuidModel
from organization.models import Organization, OrganizationMembership
from assessments.models import PLATFORM_CHOICES
from sams_project.settings import LANGUAGES

GENDER_CHOICES = (
    ("m", _("Male")),
    ("f", _("Female")),
)


class PlatformEvent(UuidModel):
    """
    Platform event model, for feed.
    """
    timestamp = models.DateTimeField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="platform_events", blank=True, null=True)
    organization = models.ForeignKey(Organization, related_name="platform_events", blank=True, null=True)
    event_type = models.CharField(max_length=1000)
    text = models.CharField(max_length=1000)
    url_json = models.CharField(max_length=1000, blank=True)

    class Meta:
        db_table = 'PlatformEvent'

    def __unicode__(self):
        return u"[%s] %s" % (self.timestamp, self.text)

    def url(self):
        """
        Generates URL based on url_json.
        If organization_slug=='__current__', an organization_slug attribute is expected
        to be present in this object, to be used in the URL.
        :return: URL
        """
        if self.url_json:
            url_data = json.loads(self.url_json)
            view_name = url_data.pop("view")
            if url_data.get('organization_slug') == '__current__':
                assert self.organization_slug
                url_data['organization_slug'] = self.organization_slug
            return reverse(view_name, kwargs=url_data)
        else:
            return None

    def save(self, *args, **kwargs):
        # Update timestamp
        if not self.pk:
            self.timestamp = datetime.datetime.today()
        return super(PlatformEvent, self).save(*args, **kwargs)


class UserProfile(models.Model):
    user = models.OneToOneField('auth.User', related_name='profile')
    language = models.CharField(max_length=100, default='nl', choices=LANGUAGES)
    phone = models.CharField(max_length=100, blank=True, null=True)
    platform_context = models.CharField(max_length=50, choices=PLATFORM_CHOICES, blank=True, null=True)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)
    default_membership = models.ForeignKey(
        OrganizationMembership, related_name='profile', blank=True, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        permissions = (
            ('view_userprofile', 'Can view user profile'),
        )

    def __unicode__(self):
        return u"{user} profile".format(user=self.user)

    def send_creation_email(self, site):
        """
        Send account creation confirmation email.

        The activation email will make use of two templates:

        ``registration/creation_email_subject.txt``
            This template will be used for the subject line of the
            email. Because it is used as the subject line of an email,
            this template's output **must** be only a single line of
            text; output longer than one line will be forcibly joined
            into only a single line.

        ``registration/creation_email.txt``
            This template will be used for the body of the email.

        These templates will each receive the following context
        variables:

        ``user``
            Newly created User object.

        ``site``
            An object representing the site on which the user
            registered; depending on whether ``django.contrib.sites``
            is installed, this may be an instance of either
            ``django.contrib.sites.models.Site`` (if the sites
            application is installed) or
            ``django.contrib.sites.models.RequestSite`` (if
            not). Consult the documentation for the Django sites
            framework for details regarding these objects' interfaces.
        """
        context = {
            "activation_key": self.activation_key,
            "expiration_days": settings.ACCOUNT_ACTIVATION_DAYS,
            "site": site
        }
        subject = render_to_string("registration/activation_email_subject.txt", context)
        # Email subject *must not* contain newlines
        subject = "".join(subject.splitlines())
        message = render_to_string('registration/activation_email.txt', context)
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, (self.user.email,))

from django.contrib.auth.models import User
# User profile created automatically on access if it doesn't exist
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])