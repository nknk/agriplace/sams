import logging
import traceback

from django.conf import settings
from django.http import Http404, JsonResponse, HttpResponsePermanentRedirect
from django.template.response import TemplateResponse
from raven.contrib.django.raven_compat.models import sentry_exception_handler
from rest_framework import status

from sams.helpers import PermissionDenied
from sams.views import InvalidContextException, proxy

logger = logging.getLogger(__name__)

HSTS_AGE = 31536000


class AcceptsMiddleware(object):
    def process_request(self, request):
        acc = [a.split(';')[0] for a in request.META['HTTP_ACCEPT'].split(',')] if 'HTTP_ACCEPT' in request.META else []
        setattr(request, 'accepted_types', acc)
        request.accepts = lambda content_type: content_type in request.accepted_types
        return None


class RouteMappingMiddleware(object):
    def process_request(self, request):
        META = request.META

        if ('HTTP_HOST' in request.META) and (META['HTTP_HOST'] in settings.FORCE_HTTPS):
            if not request.is_secure():
                url = request.build_absolute_uri(request.get_full_path())
                return HttpResponsePermanentRedirect('https:' + url[5:])

        for (url_from, url_to) in settings.URL_REDIRECT:
            url = META['wsgi.url_scheme'] + '://' + META['HTTP_HOST']
            if url == url_from:
                new_url = url_to + META['PATH_INFO']
                if META['QUERY_STRING']:
                    new_url += '?' + META['QUERY_STRING']
                return HttpResponsePermanentRedirect(new_url)

        for (path_from, url_to) in settings.URL_PROXY:
            if META['PATH_INFO'].startswith(path_from):
                return proxy(url_to + META['PATH_INFO'][len(path_from):])

    def process_response(self, request, response):
        if ('HTTP_HOST' in request.META) and (request.META['HTTP_HOST'] in settings.FORCE_HTTPS):
            if request.is_secure():
                response["strict-transport-security"] = 'max-age=%d' % HSTS_AGE

        return response


class ExceptionMiddleware(object):
    def _convert_exception_to_context(self, exception):
        if type(exception) is Http404:
            return {
                'success': False,
                'message': exception.__str__()
            }
        if type(exception) is InvalidContextException:
            return {
                'success': False,
                'message': exception.__str__()
            }
        if type(exception) is PermissionDenied:
            return {
                'success': False,
                'message': exception.__str__()
            }
        return {
            'success': False,
            'message': 'Internal Server Error'
        }

    def _convert_exception_to_status(self, exception):
        if type(exception) is InvalidContextException:
            return status.HTTP_400_BAD_REQUEST
        if type(exception) is PermissionDenied:
            return status.HTTP_403_FORBIDDEN
        if type(exception) is Http404:
            return status.HTTP_404_NOT_FOUND
        return status.HTTP_500_INTERNAL_SERVER_ERROR

    def process_exception(self, request, exception):
        traceback.print_exc(exception)
        logger.exception(exception)
        sentry_exception_handler(request=request)
        context = self._convert_exception_to_context(exception)
        status = self._convert_exception_to_status(exception)
        if request.accepts('application/json'):
            return JsonResponse(context, status=status)
        return TemplateResponse(request, 'error.html', context, status=status)
