from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db.models import Q

from core.constants import GROWER
from organization.models import Organization, OrganizationMembership


class FarmgroupFilter(admin.SimpleListFilter):
    title = 'Farm group'
    parameter_name = 'farm_group'

    def lookups(self, request, model_admin):

        if get_user_model().objects.filter(pk=request.user.pk, groups__name='abp_manager').exists():
            return Organization.objects.filter(
                uuid__in=OrganizationMembership.get_parents_by_user(request.user)
            ).values_list('uuid', 'name').distinct()
        else:
            return OrganizationMembership.objects.select_related('primary_organization').values_list(
                'primary_organization__uuid', 'primary_organization__name').distinct()

    def queryset(self, request, queryset):
        if self.value():
            secondary_organizations = OrganizationMembership.objects.filter(
                primary_organization=self.value()).values_list('secondary_organization', flat=True).distinct()
            return queryset.filter(
                Q(organizations__uuid__in=secondary_organizations) |
                Q(organizations__uuid__exact=self.value())
            )
        else:
            return queryset


class SsoWorkflowFilter(admin.SimpleListFilter):
    title = 'SSO Workflow'
    parameter_name = 'sso_workflow'

    def lookups(self, request, model_admin):
        return [(True, 'Without sso/workflow')]

    def queryset(self, request, queryset):
        if self.value():
            grower_grp, created = Group.objects.get_or_create(
                name=GROWER,
            )
            grower_users = queryset.filter(groups__name=grower_grp.name)
            filtered_ids = []
            for user in grower_users:
                if user.hzpc_workflow_author.exists() and user.key_set.exists():
                    filtered_ids.append(user.id)
            return grower_users.exclude(id__in=filtered_ids)
        else:
            return queryset

