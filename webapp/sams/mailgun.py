
import requests
import requests.exceptions

from django.conf import settings

from rest_framework import status

MAILGUN_BASE_URL = 'https://api.mailgun.net'

class MailgunError(Exception):

    def __init__(self):
        return

class Mailgun(object):

    def __init__(self):
        self.base_url = MAILGUN_BASE_URL
        self.domain = settings.MAILGUN_DOMAIN
        self.api_key = settings.MAILGUN_API_KEY

    def _get_api_url(self, api_url):
        return self.base_url + '/v3' + api_url

    def _post_call(self, api_url, data={}):
        try:
          r = requests.post(api_url, auth=("api", self.api_key), data=data)
        except RequestException:
            raise MailgunError()
        except ConnectionError:
            raise MailgunError()
        except HTTPError:
            raise MailgunError()
        except URLRequired:
            raise MailgunError()
        except TooManyRedirects:
            raise MailgunError()
        except ConnectTimeout:
            raise MailgunError()
        except ReadTimeout:
            raise MailgunError()
        except Timeout:
            raise MailgunError()

        return r

    def send(self, from_email, to, subject, **kwargs):
        data = kwargs
        data['from'] = from_email
        data['to'] = to
        data['subject'] = subject
        return self._post_call(self._get_api_url("/%s/messages" % self.domain), data=data)
