from django.conf import settings
from django.conf.urls import patterns, url
from django.contrib.auth.views import password_reset_done

from app_settings.views import HelpTextBlockDetail
from farm_group.views import DashboardView

urlpatterns = patterns(
    "sams.views",
    url(r"^$", "root", name="root"),
    url(r"^memberships/(?P<membership_pk>[\w-]*)/home$", DashboardView.as_view(), name="home"),

    # Organizations
    url(r"^organizations/$", "organization_list", name="organization_list"),
    url(r"^organizations/new$", "organization_create", name="organization_create"),

    url(r"^error$", "error", name="error"),
    url(r"^confirm$", "confirmation", name="confirmation"),
    url(r"^user$", "profile", name="my_profile"),
    url(r"^user/password_reset_done", password_reset_done, name="password_reset_done"),
    url(r"^user/change_email", "change_email", name="change_email"),

    url(r"^help/(?P<file>.*)/(?P<anchor>.*)$", "help", name="help"),
    url(r"^help/(?P<file>.*)$", "help", name="help"),

    # Debug
    url(r"^in/ping$", "ping", name="ping"),
    url(r"^in/crashme$", "crashme", name="crashme"),
    url(r"^in/throw/(?P<error>.+)$", "throw", name="throw"),
)

urlpatterns += patterns(
        '',
        url(r'^help-blocks/(?P<slug>[-\w]+)$', HelpTextBlockDetail.as_view(), name='help_text_block_detail')
    )

if settings.DEFAULT_FILE_STORAGE == 'storages.backends.s3boto.S3BotoStorage':
    urlpatterns += patterns(
        "sams.views",
        # Media
        url(r"^media/(?P<path>.*)$", "media_proxy", name="media_proxy"),
    )
