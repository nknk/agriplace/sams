import logging

import requests
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.response import TemplateResponse
from django.utils import translation
from django.utils.translation import get_language, ugettext as _
from django.views.decorators.csrf import csrf_protect
from django.views.generic import View

from app_settings.constants import LEGAL_DOCUMENT_PP
from app_settings.constants import LEGAL_DOCUMENT_TC
from app_settings.utils import get_legal_document
from sams.decorators import membership_required
from sams.utils import get_user_memberships, get_user_default_membership
from sams_project.utils import *
from sams.constants import *
from sams.forms import *
from sams.forms import EmailChangeForm
from sams.helpers import PermissionDenied, get_object_for_user, create_grower_organization_relationship
from sams.models import *
from organization.models import Organization, OrganizationUnit, OrganizationTheme, AGRIPLACE
from organization.forms import OrganizationCreateForm

logger = logging.getLogger(__name__)


class InvalidContextException(Exception):
    pass


def proxy(url):
    resp = requests.get(url)
    return HttpResponse(
        resp.content,
        status=resp.status_code,
        content_type=resp.headers.get('content-type'),
    )


class SamsView(View):
    # attributes to override
    page_name = ""
    context = {}

    def dispatch(self, request, *args, **kwargs):
        """
        Generic dispatch. Calls view function flow.
        """
        # Let's keep this here, it's useful for everybody else
        self.request = request

        # Common URL objects
        self.load_common_url_objects(kwargs)
        # Overridden URL objects
        self.load_url_objects(kwargs)

        # Common context
        self.context = self.get_common_context()
        # Overridden context
        self.context.update(self.get_context_data(**kwargs))

        # Pre-action
        self.pre(*args, **kwargs)
        # Action!
        return super(SamsView, self).dispatch(request, *args, **kwargs)

    def load_common_url_objects(self, kwargs):
        # Current organization comes from session
        # If organization_slug present in URL, first change organization

        # Get organization from session
        get_session_organization(self.request)

        # Get organization from URL.
        if "organization_slug" in kwargs.keys():
            self.organization = get_object_for_user(
                self.request.user,
                Organization,
                slug=kwargs["organization_slug"]
            )
            # Make sure this is also the organization_pk in session.
            self.request.session["organization_pk"] = self.organization.pk

    def load_url_objects(self, kwargs):
        pass

    def get_common_context(self):
        context = get_common_context(self.request)
        context.update({
            "page_triggers": self.page_triggers,
            "help_url": self.help_url,
            "organization": self.organization  # if set, otherwise None
        })
        if hasattr(self, "nav_links"):
            context["nav_links"] = self.nav_links()
        return context

    @property
    def page_triggers(self):
        """
        """
        return [self.page_name]

    @property
    def help_url(self):
        """
        Page Help URL
        """
        return "organization.html#" + self.page_name

    def get_context_data(self, **kwargs):
        """
        Basic context data
        """
        context = {}
        return context

    def pre(self, **kwargs):
        """
        Pre-action
        """
        pass


def get_common_context(request):
    """
    Common context variables for base layout.
    """
    legal_documents = get_legal_document(language=get_language())
    context = {
        "session": request.session,
        "ICONS": ICONS,
        "page_name": "",
        "page_category": "",
        "BUILD_NUMBER": getattr(settings, 'BUILD_NUMBER', 'Unknown'),
        "BUILD_TIME": getattr(settings, 'BUILD_TIME', 'Unknown'),
        "showBuildMeta": settings.AGRIPLACE_ENV_NAME.lower() not in [
            'production',
            'staging',
            'hzpc-staging',
            'hzpc-production'
        ],
        "page_title": "%%% PAGE TITLE %%%",
        "help_url": "",
        "please_dont": please_dont(),
        'DEBUG': settings.DEBUG,
        'cheatmode': request.GET.get('cheatmode', False),
        'language': get_language(),
        'isPartnersAdd': settings.IS_PARTNER_ADD,
        'privacy_policy': legal_documents[LEGAL_DOCUMENT_PP],
        'terms_conditions': legal_documents[LEGAL_DOCUMENT_TC],
        'memberships': get_user_memberships(request.user),
        'membership': getattr(request, 'membership', None)
    }
    return context


def get_org_context(request, kwargs, page_name=''):
    request.membership = get_user_default_membership(request.user)
    organization = request.membership.secondary_organization
    context = get_common_context(request)
    context.update({
        "organization": organization,
        "breadcrumbs": [],
        "page_name": page_name,
        "page_title": "",
        "page_title_secondary": "",
        "page_icon": ICONS.get("organization"),
        "page_title_url": reverse(
            "my_org_overview",
            kwargs={'membership_pk': request.membership.pk}
        ),
        "help_url": "organization.html#" + page_name,
        "page_triggers": [page_name],
    })
    return context, organization


#
# Framework
#
def error(request):
    return render(request, "error.html")


def help(request, file, anchor=""):
    if anchor != "":
        anchor = "#" + anchor
    return render(request, "help/%s.html%s" % (file, anchor))


#
# Functionality
#

def root(request):
    if request.user.is_authenticated():
        return redirect("organization_list")
    else:
        return redirect("auth_login")


@login_required
@membership_required
def home(request, *args, **kwargs):
    """
    Home page

    User organizations
    """
    context = get_common_context(request)
    context["page_name"] = "home"
    context["page_title"] = _("Organization homepage")
    context["page_triggers"] = [context["page_name"]]
    context["help_url"] = "organization.html#" + context["page_name"]
    context["hide_breadcrumbs"] = True

    # new layout
    organization = request.organization
    context['organization'] = organization
    context["current_organization_name"] = organization.name
    context["current_organization_product_count"] = organization.products.count()
    context["current_organization_partners_count"] = organization.partners.count()

    # if organization type auditor -> redirect to shared assessments
    if organization.type == 'auditor':
        return redirect('shared_assessment_list', request.membership.pk)

    # Layout options
    context["hide_navbar"] = True

    organizations = request.user.organizations.all().order_by("name")
    context['organizations'] = organizations

    if not organizations.count():
        context["show_welcome_message"] = True

    list_of_assessments = organization.assessments.filter(assessment_type__kind='assessment')
    if list_of_assessments:
        context['last_assessment'] = list_of_assessments.order_by("-created_time")[0]

    if request.method == 'POST':
        request.session['organization_pk'] = organization.pk
        return redirect('home', request.membership.pk)

    return render(request, 'core/home.html', context)


@login_required
def confirmation(request):
    msg = request.session.get("confirmation_message")
    return render(request, 'core/confirmation.html', msg)


@login_required
def profile(request):
    """
    User profile
    """
    user = request.user
    context = get_common_context(request)
    context.update({
        'page_name': 'user_profile',
        'page_title': _('User profile'),
        'page_title_secondary': '',
        'page_icon': ICONS.get('user'),
        'help_url': 'user.html#' + 'user_profile',
        'user': user
    })

    if request.method == "POST":
        form = UserUpdateForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            request.session[translation.LANGUAGE_SESSION_KEY] = form.cleaned_data['language']
            translation.activate(form.cleaned_data['language'])
            messages.success(request, _("Saved."))
            return redirect('organization_list')
    else:
        # GET
        form = UserUpdateForm(instance=user)
    context["form"] = form
    return render(request, 'core/user_profile.html', context)


@login_required
def organization_list(request):
    """
    User organizations
    """
    context = get_common_context(request)
    context["page_name"] = "organization_list"
    context["page_title"] = _("Organization Switchboard")
    context["page_triggers"] = ["organization", context["page_name"]]
    context["help_url"] = "organization.html#" + context["page_name"]

    # Layout options
    context["hide_navbar"] = True

    # If we have more than one organization,
    # select the first
    # and redirect to home page.
    #
    # We now use this view only if the user has no organization.
    memberships = context['memberships']
    if memberships:
        user_profile = request.user.profile
        if user_profile.default_membership:
            membership = user_profile.default_membership
        else:
            membership = memberships[0]
        return redirect('home', membership_pk=membership.uuid)

    if not len(memberships):
        context["page_title"] = _("Welcome to AgriPlace")
        context["show_welcome_message"] = True

    return render(request, 'organization/organization_list.html', context)


@login_required
def organization_create(request):
    """
    Create new organization
    """
    context = get_common_context(request)

    memberships = context['memberships']
    if memberships:
        membership = memberships[0]
        return redirect('home', membership_pk=membership.uuid)

    context["page_title"] = _("Register new organization")
    context["breadcrumbs"] = [
        {
            "name": _('Organizations'),
            "url": reverse("organization_list"),
        },
    ]

    # Layout options
    context["hide_organization_slice"] = True

    ok = False
    if request.method == "POST":
        form = OrganizationCreateForm(request.POST)
        if form.is_valid():
            matching_organizations = Organization.objects.filter(
                name=form.cleaned_data["name"]).count()
            if matching_organizations:
                errors = form._errors.setdefault(
                    "name", forms.util.ErrorList())
                errors.append(
                    _("An organization with this name already exists!")
                )
            else:
                try:
                    theme, __ = OrganizationTheme.objects.get_or_create(layout=AGRIPLACE)
                    new_organization = form.save()
                    new_organization.users.add(request.user)
                    new_organization.theme=theme
                    new_organization.save()
                    create_grower_organization_relationship(new_organization)
                    new_organization_hq = OrganizationUnit(
                        organization=new_organization,
                        name=_("Headquarters"))
                    new_organization_hq.save()
                    ok = True
                except Exception as ex:
                    logger.exception("Could not save organization! \n%s" % ex)
                    raise
        else:
            logger.debug(
                'Create organization form invalid.' + str(form.errors))
    else:
        # GET
        form = OrganizationCreateForm()

    context["form"] = form
    if ok:
        return redirect("organization_list")
    else:
        return render(
            request, 'organization/organization_create.html', context)


def get_session_organization(request):
    """
    Get current organization from session["organization_pk"] if present.
    If not present or error, clear session variable.
    """
    # Get organization from session
    organization_pk = request.session.get("organization_pk")
    if organization_pk:
        try:
            organization = get_object_for_user(
                request.user, Organization, pk=organization_pk)
            return organization
        except:
            # We couldn't retrieve the organization,
            # so at least clear the pk from the session
            request.session.pop("organization_pk")
            logger.info(
                "Cannot get organization from session pk %s" % organization_pk)
            return None


@csrf_protect
@login_required
def change_email(request):
    if request.method == "POST":
        form = EmailChangeForm(request.user, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, _("Email changed."))
            return redirect("my_profile")
    else:
        form = EmailChangeForm(request.user)
    context = {
        'form': form,
    }
    return TemplateResponse(request, 'core/email_change_form.html', context)


def ping(request):
    assert len(User.objects.all()) > -1
    return HttpResponse('agriplace running smoothly')


@csrf_protect
@staff_member_required
def crashme(request):
    if request.method == "POST":
        raise RuntimeError("Crashing, as requested.")
    return TemplateResponse(request, 'crashme.html', {})


@login_required
def media_proxy(request, path):
    assert '../' not in path
    return proxy(
        'https://s3-eu-west-1.amazonaws.com/%s/%s' %
        (settings.AWS_STORAGE_BUCKET_NAME, path)
    )


def ping(request):
    assert len(User.objects.all()) > -1
    return HttpResponse('agriplace running smoothly')


@csrf_protect
@staff_member_required
def crashme(request):
    if request.method == "POST":
        raise RuntimeError("Crashing, as requested.")
    return TemplateResponse(request, 'crashme.html', {})


def throw(request, error):
    actions = {
        '400': lambda: InvalidContextException('This error is intentionally thrown'),
        '403': lambda: PermissionDenied('This error is intentionally thrown'),
    }
    raise actions.get(error, lambda: Exception('This error is intentionally thrown'))()
