from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ExportActionModelAdmin
from rest_framework.authtoken.models import Token

from core.admin import BaseAdmin, BaseAbpAdminMixin
from farm_group.utils import create_sso_key
from registration.models import RegistrationProfile
from sams.filters import SsoWorkflowFilter, FarmgroupFilter
from sams.forms import UserProfileAdminForm
from sams.models import *
from sams.resources import UserResource
from urllogin.models import Key


class UserProfileInlineAdmin(BaseAbpAdminMixin, admin.StackedInline):
    model = UserProfile
    form = UserProfileAdminForm
    max_num = 1
    can_delete = False

    def get_fields(self, request, obj=None):
        fields = ['language', 'phone', 'gender', 'date_of_birth']
        if not self.is_abp_manager(request):
            fields.append('default_membership')
        return fields

    def get_readonly_fields(self, request, obj=None):
        return ['default_membership']


class CustomUserAdmin(UserAdmin, BaseAdmin, BaseAbpAdminMixin, ExportActionModelAdmin):
    search_fields = ('username', 'first_name', 'last_name', 'email')
    resource_class = UserResource

    def get_readonly_fields(self, request, obj=None):
        if self.is_abp_manager(request):
            return ['username', 'date_joined', 'last_login']
        return ['date_joined', 'last_login']

    def get_fieldsets(self, request, obj=None):
        if self.is_abp_manager(request):
            return ((None, {'fields': ('username', 'password', 'first_name', 'last_name', 'email', 'is_active',
                                       'last_login', 'date_joined')}),)
        else:
            return super(CustomUserAdmin, self).get_fieldsets(request, obj)

    def get_inline_instances(self, request, obj=None):
        inlines = [UserProfileInlineAdmin] if obj else []
        return [inline_class(self.model, self.admin_site) for inline_class in inlines]

    def get_list_display(self, request):
        common_list_display = ['username', 'get_full_name', 'get_organizations', 'is_active', 'get_sso_key',
                               'get_registration_profile']
        if get_user_model().objects.filter(pk=request.user.pk, groups__name='abp_manager').exists():
            pass
        return common_list_display

    def get_queryset(self, request):
        users = get_user_model().objects.all()
        if get_user_model().objects.filter(pk=request.user.pk, groups__name='abp_manager').exists():
            return users.filter(
                organizations__in=OrganizationMembership.get_sisters_by_user(request.user)
            )
        return users

    def get_list_filter(self, request):
        list_filter = [FarmgroupFilter, 'is_active']
        if not self.is_abp_manager(request):
            list_filter += ['is_staff', 'is_superuser', 'groups', SsoWorkflowFilter]
        return list_filter

    def get_actions(self, request):
        actions = super(CustomUserAdmin, self).get_actions(request)
        for action_name in ['generate_sso_keys']:
            action = getattr(self, action_name)
            actions[action_name] = (action, action_name, getattr(action, 'short_description', ''))

        if self.is_abp_manager(request):
            del actions['delete_selected']
        return actions

    def get_sso_key(self, obj):
        try:
            return Key.objects.get(user=obj).key
        except ObjectDoesNotExist:
            return ""
    get_sso_key.short_description = 'SSO key'

    def get_registration_profile(self, obj):
        if hasattr(obj, 'registration_profile'):
            profile = RegistrationProfile.objects.get(user=obj)
            key = profile.activation_key
            if key == RegistrationProfile.ACTIVATED:
                return 'User already activated'
            else:
                return key
        else:
            return 'Activation link not available for this user'
    get_registration_profile.short_description = 'Activation key'

    def get_full_name(self, obj):
        return u'{} {}'.format(obj.first_name, obj.last_name)
    get_full_name.short_description = 'Full name'

    def get_organizations(self, obj):
        return u', '.join([organization.name for organization in obj.organizations.all()])
    get_organizations.short_description = 'Organization list'
    get_organizations.allow_tags = True

    def generate_sso_keys(self, modeladmin, request, queryset):
        selected_count = len(queryset)
        created_sso_count = create_sso_key(queryset)
        reason_description = 'Possible reasons: producer number field is blank; this SSO key already in use.'
        result = "Result: SSO key created for {} from {} selected. {}".format(
            created_sso_count,
            selected_count,
            reason_description if not created_sso_count == selected_count else ''
        )
        self.message_user(request, result)

    generate_sso_keys.short_description = _("Generate SSO keys")


class SamsTokenAdmin(BaseAdmin):
    list_display = ('key', 'user', 'created')
    fields = ('user',)
    ordering = ('-created',)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(PlatformEvent)
admin.site.unregister(Token)
admin.site.register(Token, SamsTokenAdmin)
