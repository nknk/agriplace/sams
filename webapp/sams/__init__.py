# Signal receivers
import receivers

from django.contrib.auth import models as auth_models
from django.conf import settings
from django.db.models.signals import pre_migrate, post_migrate
from django.dispatch import receiver
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Permission

from rest_framework.authtoken.models import Token


def add_view_permissions(sender, **kwargs):
    """
    This syncdb hooks takes care of adding a view permission too all our
    content types.
    """
    # for each of our content types
    for content_type in ContentType.objects.all():
        # build our permission slug
        codename = "view_%s" % content_type.model

        # if it doesn't exist..
        if not Permission.objects.filter(content_type=content_type,
                                         codename=codename):
            # add it
            Permission.objects.create(content_type=content_type,
                                      codename=codename,
                                      name="Can view %s" %
                                           content_type.name)
            print("Added view permission for %s" % content_type.name)


# check for all our view permissions before migrations (because we moved to django 1.8)
post_migrate.connect(add_view_permissions)


# custom user related permissions
@receiver(post_migrate, sender=auth_models)
def add_user_permissions(sender, **kwargs):
    content_type = ContentType.objects.get_for_model(settings.AUTH_USER_MODEL)
    Permission.objects.get_or_create(codename='view_user', name='View user', content_type=content_type)


# custom user related permissions
@receiver(post_migrate, sender=Token)
def add_token_permissions(sender, **kwargs):
    content_type = ContentType.objects.get_for_model(Token)
    Permission.objects.get_or_create(codename='view_token', name='View token', content_type=content_type)
