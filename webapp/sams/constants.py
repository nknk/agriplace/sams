# Icons

ICONS = {
    # Modules
    'library':              'fa-folder',
    'messages':             'fa-envelope',
    'notifications':        'fa-flag',

    # Platform objects
    'assessment':           'fa-bar-chart-o',
    'assessment_reuse':     'fa-spinner fa-spin',
    'assessment_type':      'fa-bar-chart-o',
    'certification':        'fa-certificate',
    'documents':            'fa-file',
    'document':             'fa-file-o',
    'network':              'fa-globe',
    'organization':         'fa-briefcase',
    'organization_unit':    'fa-sitemap',
    'product':              'fa-leaf',
    'partner':              'fa-globe',
    'organization_profile': 'fa-bookmark',
    'questionnaire':        '',
    'form':                 'fa-list-alt',

    # Things
    'access':               'fa-lock',
    'action':               'fa-cog',
    'controls':             'fa-cogs',
    'address':              'fa-envelope-o',
    'attachment':           'fa-paperclip',
    'caret':                'fa-caret-down',
    'create':               'fa-plus',
    'database':             'fa-question',
    'delete':               'fa-trash-o',
    'edit':                 'fa-pencil',
    'email':                'fa-envelope-o',
    'feedback':             'fa-bullhorn',
    'help':                 'fa-info-circle',
    'home':                 'fa-home',
    'link':                 'fa-link',
    'link_icon':            'fa-chevron-right',
    'list':                 'fa-list',
    'icons':                'fa-th-large',
    'phone':                'fa-phone',
    'print':                'fa-print',
    'share':                'fa-share-square-o',
    'user':                 'fa-user',
    'table':                'fa-table',

    # Actions
    'back':                 'fa-arrow-left',
    'download':             'fa-download',
    'left':                 'fa-arrow-left',
    'next':                 'fa-arrow-right',
    'right':                'fa-arrow-right',
    'share':                'fa-share-alt',
    'success':              'fa-check',
    'top':                  'fa-caret-up',
    'upload':               'fa-upload',
    'angle_right':          'fa-angle-right',

    # States
    'checked':              'fa-check-square',
    'unchecked':            'fa-square-o',
    'locked':               'fa-lock',
    'unlocked':             'fa-unlock',

    'yes':              'fa-check',
    'no':              'fa-times',
}

for (name,classes) in ICONS.items():
    ICONS[name] += u' fa icon icon-{name}'.format(name=name)

PAGINATION_ITEMS_PER_PAGE = 20
