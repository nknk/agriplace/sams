from django import template
register = template.Library()


@register.filter("truncate_chars")
def truncate_chars(text, max_length):
    text = str(text)
    if len(text) <= max_length:
        return text
 
    truncated_text = text[:max_length]
    if text[max_length] != " ":
        rightmost_space = truncated_text.rfind(" ")
        if rightmost_space != -1:
            truncated_text = truncated_text[:rightmost_space]
    return truncated_text + "..."


@register.filter("no_none")
def no_none(text):
    if text == None:
        text = ""
    return text


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def class_name(obj):
    classname = obj.__class__.__name__
    return classname
