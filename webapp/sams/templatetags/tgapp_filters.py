from django import template

register = template.Library()

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def get_attr(obj, attr_name):
    if not obj:
        return None
    else:
        return obj.__dict__[attr_name]