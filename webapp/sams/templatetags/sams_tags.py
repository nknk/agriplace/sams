from django import template

register = template.Library()


@register.inclusion_tag("parts/_questionnaire_detail.html")
def questionnaire_detail(questionnaire, assessment=None, target=None):
    """
    Questionnaire detail tag

    Args:
        questionnaire: Questionnaire to display.
        assessment: Assessment this questionnaire is part of, if applicable.
        target: Target (org, unit, product) of this questionnaire, if applicable.
    """
    context = dict()
    context["questionnaire"] = questionnaire
    context["assessment"] = assessment
    context["target"] = target
    context["answers"] = questionnaire.get_answers(assessment, target)
    return context