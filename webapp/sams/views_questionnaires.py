#
# Gap Assessment Tool
#
# Questionnaire views
#

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from django.shortcuts import   get_object_or_404
from django.utils.translation import ugettext as _

import logging
logger = logging.getLogger(__name__)

from sams.views import PAGINATION_ITEMS_PER_PAGE, get_common_context


def  questionnaire_detail(request, questionnaire_pk, target_type, target_pk):
	"""
	Questionnaire detail
	"""
	context = get_common_context(request)
	return render(request, "questionnaire_detail.html", context)
