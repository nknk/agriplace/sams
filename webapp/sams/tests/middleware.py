
from rest_framework import status
from rest_framework.test import APITestCase

import json

class TestExceptionMiddleware(APITestCase):

    def test_400_json(self):
        response = self.client.get('/in/throw/400', HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.content)

        data = json.loads(response.content)

        self.assertTrue(type(data) is dict, msg='A reply to "throw" query must be an object')
        self.assertTrue('success' in data, msg='"success" field must be in the reply')        
        self.assertFalse(data['success'], msg='"success" field must be set to false')        

    def test_403_json(self):
        response = self.client.get('/in/throw/403', HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.content)

        data = json.loads(response.content)

        self.assertTrue(type(data) is dict, msg='A reply to "throw" query must be an object')
        self.assertTrue('success' in data, msg='"success" field must be in the reply')        
        self.assertFalse(data['success'], msg='"success" field must be set to false')        

    def test_400_html(self):
        response = self.client.get('/in/throw/400')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, msg=response.content)

        self.assertInHTML('<h2>An error occured!</h2>', response.content, msg_prefix='A reply to "throw" query must be an object')

    def test_403_html(self):
        response = self.client.get('/in/throw/403')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response.content)

        self.assertInHTML('<h2>An error occured!</h2>', response.content, msg_prefix='A reply to "throw" query must be an object')
