from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from filebrowser.sites import site
from accounts.views import login

admin.autodiscover()

urlpatterns = patterns(
    '',

    # Honeypot admin
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    # Real Django admin
    url(r'^in/admin/', include(admin.site.urls)),
    (r'^in/admin/filebrowser/', include(site.urls)),

    # CKeditor
    (r'^ckeditor/', include('ckeditor.urls')),
    url(r'^login/$', login, name="login"),

    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^accounts/registration/', include('registration.urls')),
    url(r'', include('registration.urls_auth')),
    url(r'', include('outside.urls')),

    # Internal API
    url(r'^internal-api/$', 'api.internal.views.internal_api_root'),
    url(r'^internal-api/', include('api.internal.urls')),

    # External API
    url(r'^external-api/', include('api.external.urls')),

    # Common SAMS
    url(r'', include("sams.urls")),
    url(r'^sso/', include('urllogin.urls')),
    url(r'^feedback/', include('feedback.urls')),
    # My Organization
    url(r'^(?P<membership_pk>.+)/our/', include('organization.urls')),
    # Assessments
    url(r'^(?P<membership_pk>.+)/our/assessments/', include("assessments.urls")),
    url(r'^(?P<membership_pk>.*)/our/assessments/', include("farm_group.urls")),

    url(r'^(?P<membership_pk>.+)/shared/assessments/', include("assessments.urls_shared")),
    # Partners/network
    url(r'^(?P<organization_slug>.+)/our/partners/', include("partners.urls")),
    # Forms
    url(r'^forms/', include("form_builder.urls")),
    # attachment download
    url(r'^in/downloads/(?P<file_attachment_uuid>\w+)/(?P<file_name>.+)/$', 'attachments.views.serve',
        name='attachment_download'),
    # rbac
    url(r'^in/rbac/', include("rbac.urls")),
    # Public organization profiles
    # Must be last. Contains catch all organization name to public profile.
    url(r'^(?P<organization_slug>.+)/p/', include('org_public_profile.urls')),
    # Tagging autocomplete
    url(r'^tagging_autocomplete/', include('tagging_autocomplete.urls')),
    # Translation
    url(r'^in/rosetta/', include('rosetta.urls')),
)

if settings.DEBUG:
    # serve media files from media_url
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )

admin.site.site_header = 'Agriplace Management Portal'
admin.site.site_title = 'Management Portal'
