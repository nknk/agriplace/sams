import random

from django.utils.functional import lazy
from django.core.urlresolvers import reverse


# Workaround for using reverse with success_url in class based generic views
# because direct usage of it throws an exception.
reverse_lazy = lambda name=None, *args: lazy(reverse, str)(name, args=args)


def please_dont():
    messages = [
        "Please do not walk on the grass.",
        "Please do not feed the animals.",
        "Please do not litter.",
        "Please do not disturb.",
        "Please do not park in front of garage.",
        "Please do not block the driveway.",
        "Please do not pet the animals.",
        "Please do not touch.",
        "Please do not smoke.",
        "Please do not waste water.",
        "Please do not forget to flush.",
        "Please do not climb on the rocks.",
        "Please do not let the cat out.",
        "Please do not touch the exponates.",
        "Please do not use engine brake."
    ]
    return messages[random.randint(0, len(messages) - 1)]


