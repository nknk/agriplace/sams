#
# Autobuild-specific settings
#

import os
from .base import *

ALLOWED_HOSTS = ('.agriplace.com',)
DEBUG = False
TEMPLATE_DEBUG = DEBUG
SECRET_KEY = "verysecretkey"

# translation signals
TRANSLATION_SIGNALS = False
