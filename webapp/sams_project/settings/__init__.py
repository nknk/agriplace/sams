import os

if os.environ.get('AGRIPLACE_HEROKU', '').lower() in ['on', 'true', 'yes', '1']:
    from .heroku import *
elif os.environ.get('AGRIPLACE_AUTOBUILD', '').lower() in ['on', 'true', 'yes', '1']:
    from .autobuild import *
elif os.environ.get('AGRIPLACE_STANDALONE', '').lower() in ['on', 'true', 'yes', '1']:
    from .standalone import *
else:
    from .local_settings import *
