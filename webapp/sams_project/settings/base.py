# coding=utf-8
import os
import re
import environ

from unipath import Path
from django.core.exceptions import ImproperlyConfigured

env = environ.Env()
ugettext = lambda s: s

DEBUG = True
TEMPLATE_DEBUG = DEBUG
ADMINS = ()
MANAGERS = ADMINS

TIME_ZONE = "Europe/Amsterdam"

LANGUAGES = (
    ('en', "English"),
    ('nl', "Nederlands"),
    ('es', u"Español")
)
BASE_LANGUAGE = 'en'
LANGUAGE_CODE = 'en'
LANGUAGE_COOKIE_NAME = 'agriplace-language'

DATE_INPUT_FORMATS = ['%d-%m-%Y', '%m-%d-%Y']

SITE_ID = 1

USE_I18N = True
USE_L10N = True
USE_TZ = False

PROJECT_DIR = Path(__file__).absolute().ancestor(3)

STATIC_ROOT = PROJECT_DIR.parent.child("static")

MEDIA_ROOT = PROJECT_DIR.child('media')

LOCALE_PATHS = (
    PROJECT_DIR.child('locale'),
)

TESTSUITE_ROOT = PROJECT_DIR

TEST_RUNNER = 'testsuite.utils.DiscoveryRunner'

STATIC_URL = '/static/'

MEDIA_URL = '/media/'

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'compressor.finders.CompressorFinder',
)

# Dynamic content translations
LANGUAGE_FILES = os.path.join(PROJECT_DIR, 'locale/{}/LC_MESSAGES/content.po')
COMPILED_LANGUAGE_FILES = os.path.join(PROJECT_DIR, 'locale/{}/LC_MESSAGES/content.mo')

TEMPLATE_LOADERS = (
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "sekizai.context_processors.sekizai",
    "sams.context_processors.piwik",
    "sams.context_processors.google_analytics",
    "sams.context_processors.agriplace_global_config",
    "sams.context_processors.agriplace_global_context",
    "sams.context_processors.layout",
)

if not DEBUG:
    TEMPLATE_CONTEXT_PROCESSORS += "sams.context_processors.sentry_dsn"

MIDDLEWARE_CLASSES = (
    "sams.middleware.RouteMappingMiddleware",
    "sams.middleware.AcceptsMiddleware",
    "sams.middleware.ExceptionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = "sams_project.urls"

WSGI_APPLICATION = "sams_project.wsgi.application"

TEMPLATE_DIRS = (
    PROJECT_DIR.child("templates"),
)

INSTALLED_APPS = (
    "django.contrib.contenttypes",
    "template_overrides",
    "admin_honeypot",
    "rbac",

    "django.contrib.admin",
    "django.contrib.auth",

    "django.contrib.humanize",
    "django.contrib.messages",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "django_extensions",
    'storages',
    "tagging",

    "ap_extras",
    "bootstrapform",
    "ckeditor",
    "compressor",
    "lib.colorfield",
    "corsheaders",
    "django_user_agents",
    "registration",
    "rest_framework",
    "rest_framework.authtoken",
    'rest_framework_swagger',
    "rosetta",
    "sekizai",
    'filebrowser',
    'grappelli.dashboard',
    'import_export',
    'platform_manager',

    "accounts",
    "assessment_types",
    "assessments",
    "reporting",
    "user_import",
    "core",
    "attachments",
    "form_builder",
    "feedback",
    "frontend",
    "org_public_profile",
    "organization",
    "outside",
    "partners",

    "sams",
    "app_settings",
    "datasync",
    'translations',
    'djcelery',
    'urllogin',
    'wkhtmltopdf',
    'farm_group',
    'assessment_workflow',
    'notifications'
)

if not DEBUG:
    INSTALLED_APPS += 'raven.contrib.django.raven_compat'

INSTALLED_APPS = ("longerusername",) + INSTALLED_APPS

# grappelli
GRAPPELLI_ADMIN_TITLE = "AgriPlace Admin"
GRAPPELLI_INDEX_DASHBOARD = 'sams_project.dashboard.CustomIndexDashboard'

# filebrowser
FILEBROWSER_DIRECTORY = 'uploads/'
FILEBROWSER_VERSIONS_BASEDIR = '_versions'
FILEBROWSER_NORMALIZE_FILENAME = True
FILEBROWSER_SELECT_FORMATS = {
    'file': ['Folder', 'Image', 'Document'],
    'image': ['Image'],
    'document': ['Document', 'Image'],
}

# for django-locking
LOCK_TIMEOUT = 600  # seconds

LOG_DIR = PROJECT_DIR.ancestor(1).child('log')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# Custom user model
AUTH_USER_MODEL = 'auth.User'
AUTH_PROFILE_MODULE = "sams.UserProfile"
LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/organizations'
APPEND_SLASH = True

# Django Registration settings
ACCOUNT_ACTIVATION_DAYS = 7
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False

REGISTRATION_AUTO_LOGIN = True
REGISTRATION_EMAIL_HTML = True
REGISTRATION_DEFAULT_FROM_EMAIL = 'support@agriplace.com'

BIRT_SERVICE_URL = 'http://s1.agriplace.org:5000/birt/sai_assessment_results'

CMS_ROOT_URL = 'http://cms.agriplace.com/'


#
# Get secret variables from environment
#

def get_env(var_name):
    """
    Gets the environment variable or raises exception

    Raises:
        ImproperlyConfigured
    """
    try:
        return os.environ[var_name]
    except KeyError:
        raise ImproperlyConfigured("ERROR: The %s environment variable is not set" % var_name)


# SECRET_KEY

SESSION_COOKIE_NAME = 'agriplace-sessionid'

SAMS_EMAIL_FROM = 'platform@agriplace.org'

ROSETTA_POFILENAMES = ('django.po', 'djangojs.po', 'strings.po', 'content.po')

CKEDITOR_CONFIGS = {
    'default': {
        'autoParagraph': False,
        'toolbar': [
            ["Format", "RemoveFormat", "Bold", "Italic", "Underline", "Strike", "SpellChecker"],
            ['NumberedList', 'BulletedList', "Indent", "Outdent", 'JustifyLeft', 'JustifyCenter', 'JustifyRight',
             'JustifyBlock'],
            ["Image", "Table", "Link", "Unlink", "Anchor", "SectionLink", "Subscript", "Superscript"],
            ['Undo', 'Redo'], ["Source"],
            ["Maximize"]
        ],
        'width': 900,
    }
}
CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_JQUERY_URL = '/static/lib/jquery-1.10.2.min.js'

#
# API REST Framework
#

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'PAGINATE_BY': None
}

CORS_ORIGIN_WHITELIST = (
    'localhost:8002',
    'localhost:3000',
    '127.0.0.1:8002',
    'agriplace.com',
    'www.agriplace.com',
)

REGISTRATION_OPEN = True

#
# django-pipeline-i18next
#

PIPELINE_I18NEXT_NAMESPACE = 'Locale'

PIPELINE_COMPILERS = (
    'pipeline_i18next.i18next.I18nextCompiler',
)

# add the message files to PIPELINE_JS
PIPELINE_JS = {
    'bundle': {
        'source_filenames': (
            'webapp/locale/nl/LC_MESSAGES/django.po',
        ),
        'output_filename': 'webapp/frontend/locale/bundle.js',
    }
}

PIWIK = None
GA_PROPERTY_ID = None

URL_REDIRECT = []
URL_PROXY = []

FORCE_HTTPS = []

# Email
if 'SMTP' in os.environ:
    pattern = (
        r'^'
        r'(?P<ssl>ssl::)?'
        r'(?P<user>[^:]+)'
        r':(?P<password>[^@]+)'
        r'@(?P<host>[^:]+)'
        r'(:(?P<port>\d+))?'
        r'$'
    )
    m = re.match(pattern, os.environ['SMTP'])
    EMAIL_HOST = m.group('host')
    EMAIL_HOST_USER = m.group('user')
    EMAIL_HOST_PASSWORD = m.group('password')
    EMAIL_PORT = int(m.group('port') or 25)
    if m.group('ssl'):
        EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'

DEFAULT_FROM_EMAIL = 'support@agriplace.com'
FEEDBACK_EMAILS = ('support@agriplace.com',)
HZPC_DEFAULT_FROM_EMAIL = 'globalgap@hzpc.nl'
HZPC_FEEDBACK_EMAILS = ('globalgap@hzpc.nl',)
SANDBOX_EMAIL = 'sandbox@agriplace.com'

# django-compressor
COMPRESS_ENABLED = True
# we have to run ./manage.py compress manually
COMPRESS_OFFLINE = True
#  we put the resulting files in /frontend/static/frontend/ (CACHE)
COMPRESS_ROOT = PROJECT_DIR.child('frontend').child('static').child('frontend')
# we read them back from /static/frontend where they have been collected
COMPRESS_URL = '/static/frontend/'
COMPRESS_STORAGE='compressor.storage.GzipCompressorFileStorage'

# RVO client settings
RVO_ENABLED = os.environ.get('RVO_ENABLED', '').lower() in ['on', 'true', 'yes', '1']
RVO_BASE_URL = os.environ.get('RVO_BASE_URL', 'https://webapplicaties.agro.nl/edicrop/EdiCropService')
RVO_ID = os.environ.get('RVO_ID', 'RVO')
RVO_USERNAME = os.environ.get('RVO_USERNAME', '')
RVO_PASSWORD = os.environ.get('RVO_PASSWORD', '')
RVO_TYPE = 'CRPRQBP'
RVO_EDICROP_VERSION = 'CRP4.0'
RVO_MESSAGE_TYPE_VERSION = '4.0'
RVO_EPSG = '28992'
RVO_TEST = os.environ.get('RVO_TEST', '').lower() in ['on', 'true', 'yes', '1']

IS_PARTNER_ADD = True

AGRIPLACE_ENV_NAME = os.environ.get('AGRIPLACE_ENV_NAME', '')

NOTIFICATIONS_EMAIL_STATUS = False
NOTIFICATIONS_EMAIL_TEST_MODE = True
NOTIFICATIONS_EMAIL_TEST_ADDRESS = "demo-hzpc@agriplace.com"

# url login
URL_LOGIN_AVAILABLE = False
URL_LOGIN_PRIVATE_KEY = ""


SWAGGER_SETTINGS = {
    'DOC_EXPANSION': 'list',
    'APIS_SORTER': 'alpha',
    'JSON_EDITOR': True,
    'USE_SESSION_AUTH': True,
    'SECURITY_DEFINITIONS': {
        'basic': {
            'type': 'basic'
        }
    },
    'OPERATIONS_SORTER': 'alpha',
    'SHOW_REQUEST_HEADERS': False,
    'SUPPORTED_SUBMIT_METHODS': [
        'get',
        'post',
        'put',
        'delete',
        'patch'
    ],
}

AGRIPLACE_FARM_GROUP_SLUG = env('AGRIPLACE_FARM_GROUP_SLUG', default='agriplace-farm-group')

# translation signals
TRANSLATION_SIGNALS = True

# HZPC fifth questionnaire
HZPC_FIFTH_QUESTIONNAIRE_CODE = os.environ.get('HZPC_FIFTH_QUESTIONNAIRE_CODE', 'AOSHZPC')
HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_NL = os.environ.get('HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_EN', 'GG-V5.1_NL-AOSHZPC')
HZPC_FIFTH_QUESTIONNAIRE_CODES = (HZPC_FIFTH_QUESTIONNAIRE_CODE, HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_NL)
HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL = os.environ.get(
    'HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL', 'GG-V5-L1-Suikerbieten'
)
HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL_5_1_NL = os.environ.get(
    'HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL_5_1_NL', 'GG-V5.1_NL-QL-Suikerbieten'
)

HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVELS = (
    HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL, HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL_5_1_NL
)

HZPC_EMAIL_HOST = env('HZPC_EMAIL_HOST', default='localhost')
HZPC_EMAIL_PORT = env('HZPC_EMAIL_PORT', default=1025)
HZPC_EMAIL_HOST_USER = env('HZPC_EMAIL_HOST_USER', default='')
HZPC_EMAIL_HOST_PASSWORD = env('HZPC_EMAIL_HOST_PASSWORD', default='')

VISIBLE_ASSESSMENT_REPORT_ASSESSMENT_CODES = os.environ.get(
    'VISIBLE_ASSESSMENT_REPORT_ASSESSMENT_CODES', 'globalgap_ifa_v5,GG-V5_ES,GG-V5.1_NL,GG-V5.1_ES,GG-V5.1_EN'
)
