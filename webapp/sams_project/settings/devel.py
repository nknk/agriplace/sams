from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG


SITE_ID = 1

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': Path(PROJECT_DIR, 'db', 'agriplace_devel.sqlite3'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

INSTALLED_APPS += (
    'debug_toolbar',
    'fixture_magic'
)

MIDDLEWARE_CLASSES += (
    "debug_toolbar.middleware.DebugToolbarMiddleware",
)

# Django Debug Toolbar
INTERNAL_IPS = ("127.0.0.1",)
DEBUG_TOOLBAR_CONFIG = {
    "DISABLE_PANELS": [],
    "SHOW_TOOLBAR_CALLBACK": 'sams_project.django_toolbar.show_toolbar'
}

# Don't break urls please
# http://django-debug-toolbar.readthedocs.org/en/1.0/installation.html#explicit-setup
DEBUG_TOOLBAR_PATCH_SETTINGS = False

SECRET_KEY = "secret"

# Use signed cookies for sessions so we don't keep changing the database.
SESSION_ENGINE='django.contrib.sessions.backends.signed_cookies'

# Django Registration settings
ACCOUNT_ACTIVATION_DAYS = 3

ROSETTA_ENABLE_TRANSLATION_SUGGESTIONS = True
ROSETTA_MESSAGES_PER_PAGE = 20

YANDEX_TRANSLATE_KEY = 'trnsl.1.1.20140915T161803Z.42b9c5162ea5695c.c43f2bbad9ef11197c4600e75e99e619de9724f9'

LOG_ROOT = 'log/'

COMPRESS_ENABLED = False

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'TIMEOUT': 60,
        'OPTIONS': {
            'MAX_ENTRIES': 1000
        }
    }
}

MAILGUN_DOMAIN = 'sandbox70f1732caef8481f98e972bd9ce339a8.mailgun.org'
MAILGUN_API_KEY = 'key-b2449a475cbf50610de97e2dd8803575'
