#
# Standalone instance settings
# (Note: it's a copy of Heroku settings so far)
#

import os
import re
import dj_database_url

_TRUE_VALUES = ['on', 'true', 'yes', '1']

if os.environ.get('DEVEL', '').lower() in _TRUE_VALUES:
    from .devel import *
else:
    from .base import *

DEBUG = os.environ.get('DEBUG', 'off').lower() in _TRUE_VALUES
TEMPLATE_DEBUG = DEBUG


SECRET_KEY = os.environ['SECRET_KEY']

DATABASES = {
    'default': dj_database_url.parse(os.environ['DATABASE_URL']),
}

if 'CMS_ROOT_URL' in os.environ:
  CMS_ROOT_URL = os.environ['CMS_ROOT_URL']


if 'SMTP' in os.environ:
    pattern = (
        r'^'
        r'(?P<ssl>ssl::)?'
        r'(?P<user>[^:]+)'
        r':(?P<password>[^@]+)'
        r'@(?P<host>[^:]+)'
        r'(:(?P<port>\d+))?'
        r'$'
    )
    m = re.match(pattern, os.environ['SMTP'])
    EMAIL_HOST = m.group('host')
    EMAIL_HOST_USER = m.group('user')
    EMAIL_HOST_PASSWORD = m.group('password')
    EMAIL_PORT = int(m.group('port') or 25)
    if m.group('ssl'):
        EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'


SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
FORCE_HTTPS = os.environ.get('FORCE_HTTPS', '').split()

if 'SENTRY_DSN' in os.environ:
    RAVEN_CONFIG = {
        'dsn': os.environ['SENTRY_DSN'],
    }

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_QUERYSTRING_AUTH = False

ALLOWED_HOSTS = ['*']

if 'PIWIK_URL' in os.environ:
    PIWIK = {}
    PIWIK['server'], PIWIK['site_id'] = \
        os.environ['PIWIK_URL'].split('piwik.php?idsite=')

if 'GA_PROPERTY_ID' in os.environ:
    GA_PROPERTY_ID=os.environ['GA_PROPERTY_ID']

URL_REDIRECT = [p.split('|') for p in os.environ.get('URL_REDIRECT', '').split()]
URL_PROXY = [p.split('|') for p in os.environ.get('URL_PROXY', '').split()]

REGISTRATION_OPEN = os.environ.get('REGISTRATION_OPEN', '').lower() in _TRUE_VALUES

MAILGUN_DOMAIN = os.environ.get('MAILGUN_DOMAIN', '')
MAILGUN_API_KEY = os.environ.get('MAILGUN_API_KEY', '')
