#
# Heroku-specific settings
#

import os
import dj_database_url

try:
    from .build_stats import BUILD_NUMBER, BUILD_TIME
except ImportError:
    BUILD_NUMBER = 'Unknown'
    BUILD_TIME = 'Unknown'

_TRUE_VALUES = ['on', 'true', 'yes', '1']

if os.environ.get('DEVEL', '').lower() in _TRUE_VALUES:
    from .devel import *
else:
    from .base import *

from .celery import *

DEBUG = os.environ.get('DEBUG', 'off').lower() in _TRUE_VALUES
TEMPLATE_DEBUG = DEBUG

SECRET_KEY = os.environ['SECRET_KEY']

DATABASES = {
    'default': dj_database_url.parse(os.environ['DATABASE_URL']),
}

if 'CMS_ROOT_URL' in os.environ:
    CMS_ROOT_URL = os.environ['CMS_ROOT_URL']

if 'SMTP' in os.environ:
    pattern = (
        r'^'
        r'(?P<ssl>ssl::)?'
        r'(?P<user>[^:]+)'
        r':(?P<password>[^@]+)'
        r'@(?P<host>[^:]+)'
        r'(:(?P<port>\d+))?'
        r'$'
    )
    m = re.match(pattern, os.environ['SMTP'])
    EMAIL_HOST = m.group('host')
    EMAIL_HOST_USER = m.group('user')
    EMAIL_HOST_PASSWORD = m.group('password')
    EMAIL_PORT = int(m.group('port') or 25)
    if m.group('ssl'):
        EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'

if ('MAILGUN_API_KEY' in os.environ) and ('MAILGUN_DOMAIN' in os.environ):
    EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
    MAILGUN_ACCESS_KEY = os.environ['MAILGUN_API_KEY']
    MAILGUN_SERVER_NAME = os.environ['MAILGUN_DOMAIN']

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
FORCE_HTTPS = os.environ.get('FORCE_HTTPS', '').split(',')

if 'SENTRY_DSN' in os.environ:
    RAVEN_CONFIG = {
        'dsn': os.environ['SENTRY_DSN'],
    }

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_QUERYSTRING_AUTH = False

ALLOWED_HOSTS = ['*']

if 'PIWIK_URL' in os.environ:
    PIWIK = {}
    PIWIK['server'], PIWIK['site_id'] = \
        os.environ['PIWIK_URL'].split('piwik.php?idsite=')

if 'GA_PROPERTY_ID' in os.environ:
    GA_PROPERTY_ID = os.environ['GA_PROPERTY_ID']

URL_REDIRECT = [p.split('|') for p in os.environ.get('URL_REDIRECT', '').split()]
URL_PROXY = [p.split('|') for p in os.environ.get('URL_PROXY', '').split()]

REGISTRATION_OPEN = os.environ.get('REGISTRATION_OPEN', '').lower() in _TRUE_VALUES

IS_PARTNER_ADD = os.environ.get('IS_PARTNER_ADD', 'True').lower() in _TRUE_VALUES

MAILGUN_DOMAIN = os.environ.get('MAILGUN_DOMAIN', '')
MAILGUN_API_KEY = os.environ.get('MAILGUN_API_KEY', '')

# url login
URL_LOGIN_AVAILABLE = os.environ.get('SSO_LOGIN_AVAILABLE', 'True').lower() in _TRUE_VALUES
URL_LOGIN_PRIVATE_KEY = os.environ.get('SSO_LOGIN_PRIVATE_KEY', "")

if URL_LOGIN_AVAILABLE:
    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'urllogin.backends.LoginUrlBackend'
    )

# hzpc settings
HZPC = os.environ.get('HZPC', '').lower() in _TRUE_VALUES

# hzpc notifications
NOTIFICATIONS_EMAIL_STATUS = os.environ.get('NOTIFICATIONS_EMAIL_STATUS', 'True').lower() in _TRUE_VALUES
NOTIFICATIONS_EMAIL_TEST_MODE = os.environ.get('NOTIFICATIONS_EMAIL_TEST_MODE', 'True').lower() in _TRUE_VALUES
NOTIFICATIONS_EMAIL_TEST_ADDRESS = os.environ.get('NOTIFICATIONS_EMAIL_TEST_ADDRESS', 'demo-hzpc@agriplace.com')

WKHTMLTOPDF_CMD = '/app/wkhtmltox/bin/wkhtmltopdf'

# translation signals
TRANSLATION_SIGNALS = os.environ.get('TRANSLATION_SIGNALS', '').lower() in _TRUE_VALUES

# HZPC fifth questionnaire
HZPC_FIFTH_QUESTIONNAIRE_CODE = os.environ.get('HZPC_FIFTH_QUESTIONNAIRE_CODE', 'AOSHZPC')
HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_NL = os.environ.get('HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_EN', 'GG-V5.1_NL-AOSHZPC')
HZPC_FIFTH_QUESTIONNAIRE_CODES = (HZPC_FIFTH_QUESTIONNAIRE_CODE, HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_NL)
HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL = os.environ.get(
    'HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL', 'GG-V5-L1-Suikerbieten'
)
HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL_5_1_NL = os.environ.get(
    'HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL_5_1_NL', 'GG-V5.1_NL-QL-Suikerbieten'
)

HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVELS = (
    HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL, HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVEL_5_1_NL
)

VISIBLE_ASSESSMENT_REPORT_ASSESSMENT_CODES = os.environ.get(
    'VISIBLE_ASSESSMENT_REPORT_ASSESSMENT_CODES', 'globalgap_ifa_v5,GG-V5_ES,GG-V5.1_NL,GG-V5.1_ES,GG-V5.1_EN'
)