"""
Test suite settings
"""

from .base import *

TEST_RUNNER = 'teamcity.django.TeamcityDjangoRunner'
# TEST_DISCOVER_TOP_LEVEL = project_dir
# TEST_DISCOVER_ROOT = project_dir
# TEST_DISCOVER_PATTERN = "test*"

# In-memory test database
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
        'TEST': {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": ":memory:",
        }
    },
}

SECRET_KEY = "secret"
SOUTH_TESTS_MIGRATE = False

HOUR_OF_CRONJOB = 0
MIN_OF_CRONJOB = 0
CELERY_ALWAYS_EAGER = True
