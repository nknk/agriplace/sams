// let webpack = require('webpack');
const webpackConfig = require('./gulp/webpack.development.config.js');
// Karma configuration
// Generated on Mon Jun 01 2015 15:50:21 GMT+0300 (EEST)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      // {pattern: '__tests__/AssessmentDocumentTypeTest.js', watched: false, included: true, served: true}
      { pattern: '__tests__/index.js', watched: false, included: true, served: true },
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      '__tests__/**.js': ['webpack', 'sourcemap'],
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['mocha'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // mochaReporter: {
    //   output: 'minimal',
    // },


    webpack: {
      module: webpackConfig.module,
      devtool: 'eval',

      // These externals set due to React 0.13 to avoid enzyme CLI wrnings
      // Details https://github.com/airbnb/enzyme/issues/47
      externals: {
        'react-dom': true,
        'react-dom/server': true,
        'react-addons-test-utils': true,
      },
    },

    webpackMiddleware: {
      stats: 'errors-only',
    },

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,
  });
};
