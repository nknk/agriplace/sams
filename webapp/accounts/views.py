import re

from django.contrib.auth import views as auth_views
from django.contrib.auth import authenticate
from django.contrib.auth import login as direct_login
from django.contrib.auth import update_session_auth_hash

from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.safestring import mark_safe

from django_user_agents.utils import get_user_agent
from django.shortcuts import render, redirect, render_to_response
from django.template import RequestContext

from app_settings.constants import LEGAL_DOCUMENT_PP, LEGAL_DOCUMENT_TC
from app_settings.utils import get_legal_document
from organization.models import OrganizationMembership
from .forms import AccountActivationForm
from registration.models import RegistrationProfile


SHA1_RE = re.compile('^[a-f0-9]{40}$')


def login(request, *args, **kwargs):
    context = {}

    # supported browsers
    supported_browsers = {
        'Firefox': 17,
        'Internet Explorer': 10,
        'IE': 10,
        'Chrome': 20,
        # 'Chromium': 130
    }
    user_agent = get_user_agent(request)
    context['user_agent'] = user_agent
    min_supported_version = supported_browsers.get(user_agent.browser.family)
    if not min_supported_version:
        context['browser_unknown'] = True
    else:
        if user_agent.browser.version[0] < min_supported_version:
            context['browser_old'] = True

    return auth_views.login(request, extra_context=context)


def activate(request, activation_key=""):
    context = {}
    if activation_key and SHA1_RE.search(activation_key):
        try:
            user_profile = RegistrationProfile.objects.get(activation_key=activation_key)

            if user_profile.is_activated:
                return render(request, 'accounts/activate_complete.html', {})

            if request.method == "POST":
                form = AccountActivationForm(request.POST)
                if form.is_valid():
                    existing = RegistrationProfile.objects.filter(
                        user__username__iexact=request.POST["email"],
                        activation_key="")
                    if not existing.exists():

                        result = user_profile.activate_user(
                            request.POST["email"],
                            request.POST['password']
                        )

                        request.session["activated"] = True

                        if result:
                            user = authenticate(
                                username=request.POST["email"],
                                password=request.POST['password']
                                )

                            if user is not None:
                                if user.is_active:
                                    direct_login(request, user)
                                    return redirect("organization_list")
                        else:
                            context["error_msg"] = _("We can't update your profile now, please try again later.")
                    else:
                        context["error_msg"] = _("User with such email already activated.")
            else:
                form = AccountActivationForm(initial={'email': user_profile.user.email})
                organizations = user_profile.user.organizations

                # legal documents
                legal_documents = get_legal_document(language=get_language())
                privacy_policy = _(u'AgriPlace Privacy Policy')
                if legal_documents[LEGAL_DOCUMENT_PP]:
                    privacy_policy = u"<a target='_blank' href='{}'>{}</a>".format(
                        legal_documents[LEGAL_DOCUMENT_PP].document.url,
                        privacy_policy
                    )

                terms_and_conditions = _(u'AgriPlace Terms and Conditions')
                if legal_documents[LEGAL_DOCUMENT_TC]:
                    terms_and_conditions = u"<a target='_blank' href='{}'>{}</a>".format(
                        legal_documents[LEGAL_DOCUMENT_TC].document.url,
                        terms_and_conditions
                    )

                form.fields['agree_terms'].label = mark_safe(
                    u"{} {} {} {}".format(
                        _('I agree with the'),
                        terms_and_conditions,
                        _('and the'),
                        privacy_policy
                    )
                )

                if organizations.count() != 0:
                    membership = OrganizationMembership.objects.get(secondary_organization=organizations.first())
                    form.fields['agree_sharing'].label = u"{} {} {}".format(
                        _(u"I authorize"),
                        membership.primary_organization.name,
                        _(u"to share my email address, organization name and organization location with AgriPlace")
                    )

            context['form'] = form
        except Exception as ex:
            print ex
            context["bad_key"] = True
    else:
        context["bad_key"] = True
    return render(request, 'accounts/activate_account.html', context)


import os
from django.conf import settings
class UnsupportedMediaPathException(Exception):
    pass

def fetch_resources(uri, rel):
    """
    Callback to allow xhtml2pdf/reportlab to retrieve Images,Stylesheets, etc.
    `uri` is the href attribute from the html link element.
    `rel` gives a relative path, but it's not used here.
    """
    if settings.MEDIA_URL and uri.startswith(settings.MEDIA_URL):
        path = os.path.join(settings.MEDIA_ROOT,
                            uri.replace(settings.MEDIA_URL, ""))
    elif settings.STATIC_URL and uri.startswith(settings.STATIC_URL):
        path = os.path.join(settings.STATIC_ROOT,
                            uri.replace(settings.STATIC_URL, ""))
        if not os.path.exists(path):
            for d in settings.STATICFILES_DIRS:
                path = os.path.join(d, uri.replace(settings.STATIC_URL, ""))
                if os.path.exists(path):
                    break
    elif uri.startswith("http://") or uri.startswith("https://"):
        path = uri
    else:
        raise UnsupportedMediaPathException(
                                'media urls must start with %s or %s' % (
                                settings.MEDIA_URL, settings.STATIC_URL))
    return path
