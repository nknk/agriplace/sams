# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _, get_language
from django.utils.safestring import mark_safe

from app_settings.constants import LEGAL_DOCUMENT_PP, LEGAL_DOCUMENT_TC
from app_settings.utils import get_legal_document

User = get_user_model()


class AccountActivationForm(forms.Form):
    MIN_LENGTH = 8

    email = forms.EmailField(label=_("E-mail"), help_text=_("This e-mail address will be used as your login name"))
    password = forms.CharField(
        widget=forms.PasswordInput,
        label=_("Create password"),
        help_text=_("The new password must be at least 8 characters long "
                    "and contain at least one letter and at least one digit or "
                    "punctuation character")
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput,
        label=_("Confirm password")
    )

    agree_terms = forms.BooleanField(
        label=mark_safe(_(u"I agree with the AgriPlace Terms and Conditions and the AgriPlace Privacy Policy"))
    )

    agree_sharing = forms.BooleanField(
        label=_(u"I authorize Agriplace to share my email address, organization name and organization "
                u"location with AgriPlace​")
    )

    def __init__(self, *args, **kwargs):
        super(AccountActivationForm, self).__init__(*args, **kwargs)

        # legal documents
        legal_documents = get_legal_document(language=get_language())
        privacy_policy = _(u'AgriPlace Privacy Policy')
        if legal_documents[LEGAL_DOCUMENT_PP]:
            privacy_policy = u"<a target='_blank' href='{}'>{}</a>".format(
                legal_documents[LEGAL_DOCUMENT_PP].document.url,
                privacy_policy
            )

        terms_and_conditions = _(u'AgriPlace Terms and Conditions')
        if legal_documents[LEGAL_DOCUMENT_TC]:
            terms_and_conditions = u"<a target='_blank' href='{}'>{}</a>".format(
                legal_documents[LEGAL_DOCUMENT_TC].document.url,
                terms_and_conditions
            )

        agree_terms = mark_safe(
            u"{} {} {} {}".format(
                _('I agree with the'),
                terms_and_conditions,
                _('and the'),
                privacy_policy
                )
            )
        self.fields['agree_terms'].label = agree_terms


    def clean_password(self):
        password = self.cleaned_data.get('password')

        # At least MIN_LENGTH long
        if len(password) < self.MIN_LENGTH:
            raise forms.ValidationError("", code='invalid_password')

        # At least one letter and one non-letter
        first_isalpha = password[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password):
            raise forms.ValidationError("", code='invalid_password')
        return password

    def clean_agree_terms(self):
        agree_terms = self.cleaned_data.get('agree_terms')
        if not agree_terms:
            raise forms.ValidationError("", code='disagree_terms')
        return agree_terms

    def clean_agree_sharing(self):
        agree_sharing = self.cleaned_data.get('agree_sharing')
        if not agree_sharing:
            raise forms.ValidationError("", code='disagree_sharing')
        return agree_sharing

    def clean(self):
        if (self.cleaned_data.get("password") and self.cleaned_data.get("password2")
            and self.cleaned_data.get("password") != self.cleaned_data.get("password2")):
            raise forms.ValidationError(_("The two password fields didn't match."), code='password_mismatch')
        return self.cleaned_data
