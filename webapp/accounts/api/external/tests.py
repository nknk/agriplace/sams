from unittest import skip

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from api.internal.tests.mixins import AuthAPIMixin


# TODO: Need to rewrite these tests using factory boy and organization membership table

class TestExchangeAPI(APITestCase, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'test_hzpc.json',
        'test_hzpc_users.json'
    ]

    def setUp(self):
        self.test_vendor = 'HZPC grower organization'
        self.test_user_id = '53'

        self.exchange_url = reverse('exchange', kwargs={
            'vendor': self.test_vendor,
            'relation_number': self.test_user_id
        })

        self.token = 'Token {}'.format(self.get_api_token())

    @skip("APP-1187")
    def test_account_exchange_exists(self):
        response = self.client.get(
            self.exchange_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('success' in response.data, msg='A response to an assessment detail query must have a uuid')
        self.assertEqual(response.data['success'], True, msg='Exchange User must exist')

    @skip("APP-1187")
    def test_account_exchange_do_not_exist(self):
        self.token = 'Token {}'.format(self.get_api_token())

        self.test_user_id = '16'

        self.exchange_url = reverse('exchange', kwargs={
            'vendor': self.test_vendor,
            'relation_number': self.test_user_id
        })

        response = self.client.get(
            self.exchange_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('success' in response.data, msg='A response to an assessment detail query must have a uuid')
        self.assertEqual(response.data['success'], False, msg='Exchange User must not exist')

    @skip("APP-1187")
    def test_account_exchange_save(self):
        producer = {
            "organization_name": "ACME Farmer B.V.",
            "contactperson_first_name": "John",
            "contactperson_last_name": "Doe",
            "address": "206 Oostenburgermiddenstraat",
            "postcode": "1018 LL",
            "city": "Amsterdam",
            "country": "nl",
            "phone": "+31 (0) 854 897 333",
            "mobile": "+31 (0) 854 897 333",
            "email": "info2@agriplace.com",
            "ggn": "123323",
        }

        response = self.client.post(
            self.exchange_url,
            data=producer,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.status_code)
