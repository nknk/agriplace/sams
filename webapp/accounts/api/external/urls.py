from django.conf.urls import patterns, url, include

urlpatterns = patterns(
    'farm_group.api.external.views',

    url(r'exchange/(?P<vendor>.+)/(?P<relation_number>.+)/$','exchange', name='exchange'),
)
