from django.conf.urls import \
    patterns, \
    url

urlpatterns = patterns(
    'accounts.views',

    # Activation
    url(r"^activate(?:/(?P<activation_key>\w+))?/$", 'activate', name="account_activate"),
)
