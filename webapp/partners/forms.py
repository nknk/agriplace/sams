from django import forms
from organization.models import OrganizationRelationship


class OrganizationSearchForm(forms.Form):
    name = forms.CharField()


class InvitationReplyForm(forms.Form):
    invitation = forms.CharField()
    status = forms.CharField()


class OrganizationRelationshipForm(forms.ModelForm):
    relationship = forms.CharField(widget=forms.RadioSelect)

    class Meta:
        model = OrganizationRelationship
        exclude = []
