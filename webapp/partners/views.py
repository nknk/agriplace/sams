import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from assessments.models import AssessmentShare
from organization.models import PartnerInvitation, Organization, OrganizationRelationship
from organization.views import get_organization_context
from sams.constants import ICONS
from sams.helpers import get_object_for_user
from ap_extras.item_list import ItemListModel, ItemListColumn
from sams.views import get_common_context, SamsView
from sams.decorators import session_organization_required

from partners.forms import OrganizationRelationshipForm

logger = logging.getLogger(__name__)


def home(request, organization_slug):
    return redirect("partner_list", organization_slug=organization_slug)


def get_partners_context(request, kwargs, page_name=''):
    # Context based on organization context because we always get the current org slug
    context, organization = get_organization_context(request, kwargs, page_name)
    context['page_triggers'] = ['partners', page_name]
    context['invitations'] = PartnerInvitation.objects.filter(receiver=organization).filter(status="open").all()
    return context, organization


class PartnersView(SamsView):
    """
    Base partners module view class.
    """

    def get_common_context(self):
        context = super(PartnersView, self).get_common_context()
        context["organization"] = self.organization
        return context

    @property
    def page_triggers(self):
        return ["partners", self.page_name]

    @property
    def help_url(self):
        return "partners.html#%s" % self.page_name

    @property
    def organization_kwargs(self):
        return {
            "organization_slug": self.organization.slug
        }


def partners_context(request, page_name="PAGE", triggers=[]):
    """
    Common context for organization views
    """
    context = get_common_context(request)
    organization = context["current_organization"]
    context["organization"] = organization
    context["page_name"] = page_name
    context["page_title"] = organization.name
    context["page_title_secondary"] = _("organization")
    context["page_title_url"] = reverse("my_org_overview")
    context["page_secondary_title"] = _("%s details") % context["organization"].name
    context["page_icon"] = "i i-organization"
    context["page_triggers"] = ["organization", page_name] + triggers
    context["help_url"] = "organization.html#" + page_name
    return context


@login_required
@session_organization_required
def list(request, *args, **kwargs):
    context, organization = get_partners_context(request, kwargs, 'partners_list')
    context.update({
        'page_title': _("Partner organizations"),
        'page_title_secondary': None,
        'page_title_url': None,
        'page_icon': ICONS['partner'],
    })

    # Active assessments item list model
    partners_model = ItemListModel()
    partners_model.id = 'partners'
    partners_model.items = organization.partners.all()
    partners_model.columns = [
        ItemListColumn(
            header=_("Name"),
            text=lambda assessment: assessment.__unicode__,
            url=lambda partner: reverse('org_other_profile', args=[organization.slug, partner.slug]),
            icon_class=lambda assessment: ICONS['organization']
        ),
        ItemListColumn(
            header=_("Country"),
            text=lambda partner: partner.mailing_country or '',
        ),
    ]
    partners_model.create_text = _("Add new partner")
    partners_model.create_url = reverse(
        "partner_search", kwargs={'organization_slug': organization.slug}
    )

    partners_model.no_items_text = _("No partners available.")
    partners_model.refresh()
    context['partners_model'] = partners_model

    return render(request, "partners/list.html", context)


@login_required
@session_organization_required
def search(request, *args, **kwargs):
    """
    Search results page
    """
    context, organization = get_partners_context(request, kwargs, 'partners_search')
    context['page_triggers'] = ['partners']
    context.update({
        'page_title': _("Partner organization search"),
        'page_icon': ICONS.get('partner')
    })
    name_query = request.GET.get("name")
    context["name_query"] = name_query
    if name_query:
        context["results"] = Organization.objects.filter(name__icontains=name_query).exclude(pk=organization.pk).all()
    else:
        context["results"] = []
    return render(request, "partners/search.html", context)


@login_required
@session_organization_required
def add_partner(request, target_slug, *args, **kwargs):
    """
    Partner add
    """
    context, organization = get_partners_context(request, kwargs, 'partner_add')
    if request.method == 'POST':
        context["name_query"] = target_slug
        # Find partner by slug
        partner = Organization.objects.get(slug=target_slug)

        # Add partner if not already there
        if partner not in organization.partners.all():
            relationship = OrganizationRelationship(
                sender_organization=organization,
                receiver_organization=partner
            )
            relationship.save()

        # Confirm all incoming invitations from them
        for invitation in organization.received_invitations.filter(sender=partner):
            invitation.status = "accepted"
            invitation.save()

        # Send invitation if they don't already have us as partner and there isn't an open one
        if organization not in partner.partners.all():
            # We aren't partners
            # invitation_query = self.organization.sent_invitations.filter(receiver=partner)
            invitation_query = partner.received_invitations.filter(sender=organization).filter(status="open")
            if not invitation_query.count():
                # No open invitation from us, so send an invitation
                invitation = PartnerInvitation(
                    sender=organization,
                    receiver=partner
                )
                invitation.save()
                messages.success(request, _("Invited partner %s.") % partner)
        messages.success(request, _("Added partner %s.") % partner)
        return redirect("partner_list", organization_slug=organization.slug)
    else:
        form = OrganizationRelationshipForm()
        context['form'] = form
        context['partner'] = Organization.objects.get(slug=target_slug)
        # GET
        return render(request, "partners/add.html", context)


@login_required
@session_organization_required
@transaction.atomic
def remove_partner(request, *args, **kwargs):
    """
    Remove partner action (POST only)
    """
    context, organization = get_partners_context(request, kwargs, 'partner_remove')
    if request.method == 'POST':
        partner_slug = request.POST.get("partner")
        # Keep name_query so we can pass it back to list
        context['name_query'] = partner_slug
        partner = Organization.objects.get(slug=partner_slug)
        # Remove partner
        partnership = OrganizationRelationship.objects.get(sender_organization=organization,
                                                           receiver_organization=partner)
        partnership.delete()
        messages.success(request, _("Removed partner {0}.").format(partner))
        return redirect("partner_list", organization_slug=organization.slug)


@login_required
@session_organization_required
def partner_profile(request, *args, **kwargs):
    """
    Profile of a partner organization
    """
    context, organization = get_partners_context(request, kwargs, 'partner_profile')
    target_organization = get_object_for_user(request.user, Organization, slug=kwargs['target_slug'])

    context['target_organization'] = target_organization
    context.update({
        'page_title': target_organization.name,
        'page_title_secondary': _("organization")
    })

    incoming_assessment_shares = AssessmentShare.objects.filter(
        assessment__organization=target_organization
    ).filter(
        partner=organization
    ).all()

    context['assessment_shares'] = incoming_assessment_shares
    return render(request, 'partners/partner_profile.html', context)


class AssessmentDetailView(PartnersView):
    """
    Detail of a partner organization's assessment

    URL Params:
        assessment_pk: PK of assessment to display
    """
    page_name = "partner_assessment_detail"

    def get(self, *args, **kwargs):
        try:
            assessment_share = self.organization.incoming_assessment_shares.get(assessment__pk=kwargs["assessment_pk"])
        except ObjectDoesNotExist:
            raise Http404("Could not find assessment in ones shared with you.")
        self.context["page_title"] = assessment_share.assessment.name
        self.context["page_title_secondary"] = _("assessment")
        self.context["page_icon"] = ICONS["assessment"]
        self.context["share_permissions"] = assessment_share.share_permissions.values_list("permission_name", flat=True)
        return render(self.request, "partners/partner_assessment_detail.html", self.context)


class ReceivedInvitationsView(PartnersView):
    page_name = "received_invitations"

    def get(self, *args, **kwargs):
        invitations = PartnerInvitation.objects.filter(receiver=self.organization).filter(status="open").all()
        self.context["invitations"] = invitations
        return render(self.request, "partners/invitations.html", self.context)


class InvitationReplyView(PartnersView):
    page_name = "invitation_reply"

    def post(self, *args, **kwargs):
        invitations = PartnerInvitation.objects.filter(receiver=self.organization).filter(status="open").all()
        self.context["invitations"] = invitations
        return render(self.request, "partners/invitations.html", self.context)
