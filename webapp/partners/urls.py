from django.conf.urls import patterns, url

from partners.views import ReceivedInvitationsView, AssessmentDetailView

urlpatterns = patterns(
    "partners.views",
    url(
        r"^$",
        "home",
        name="partners"
    ),
    url(
        r"^list$",
        "list",
        name="partner_list"
    ),
    url(
        r"^search$",
        "search",
        name="partner_search"
    ),
    url(
        r"^add/(?P<target_slug>.+)/$",
        "add_partner",
        name="partner_add"
    ),
    url(
        r"^remove$",
        "remove_partner",
        name="partner_remove"
    ),
    url(
        r"^invitations$",
        ReceivedInvitationsView.as_view(),
        name="partner_received_invitations"
    ),
    url(
        r"^profiles/(?P<target_slug>.+)",
        "partner_profile",
        name="partner_profile"
    ),
    url(
        r"^assessment/(?P<assessment_pk>.+)",
        AssessmentDetailView.as_view(),
        name="partner_assessment_detail"
    ),
)
