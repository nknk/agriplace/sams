from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _

from organization.models import Organization


class UserRegistrationForm(forms.Form):
    email = forms.EmailField(
        label=_('Email address'),
        required=True,
        )
    password = forms.CharField(
        label=_('Password'),
        required=True,
        widget=forms.PasswordInput
    )
    password_repeat = forms.CharField(
        label=_('Password, one more time'),
        required=True,
        widget=forms.PasswordInput
    )
    name = forms.CharField(
        label=_('Your name'),
        required=True
    )

    # Step 2
    REGISTER_ORGANIZATION_CHOICES = (
    ("true", _("I want to also register my organization.")),
    ("false", _("I am only creating an account for myself."))
    )
    register_organization = forms.ChoiceField(widget=forms.widgets.RadioSelect, choices=REGISTER_ORGANIZATION_CHOICES, initial="true")

    organization_name = forms.CharField(
        label=_("Your organization's name"),
        required=False
    )

    #	ORGANIZATION_TYPE_CHOICES = (
    #		("spo", _("Single Producer Organization")),
    #		("retailer", _("Retailer")),
    #		("coop", _("Cooperative")),
    #		("other", _("Other"))
    #	)
    #	organization_type = forms.ChoiceField(choices=ORGANIZATION_TYPE_CHOICES)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            existing_user = User.objects.get(email=email)
            raise ValidationError(_("A user already exists using this email address."))
        except User.DoesNotExist:
            return email

    def clean_password_repeat(self):
        if self.cleaned_data["password"] != self.cleaned_data["password_repeat"]:
            raise ValidationError(_("Passwords don't match."))

    def clean_organization_name(self):
        if self.cleaned_data["register_organization"] == "true":
            organization_name = self.cleaned_data["organization_name"].strip()
            if not len(organization_name):
                raise ValidationError(_("This field is required."))
            other_organizations = Organization.objects.filter(name=organization_name)
            if other_organizations.count():
                raise ValidationError(_("Another organization with this name already exists. To join that organization, first create an account then contact its administrator."))
            return organization_name

class ForgotPasswordForm(forms.Form):
    email = forms.EmailField()
	
	
