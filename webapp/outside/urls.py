from django.conf.urls import \
    patterns, \
    include, \
    url
from django.contrib.auth.views import \
    login, \
    logout_then_login
from django.views.generic.base import TemplateView


urlpatterns = patterns(
    "outside.views",
)

