/***
 * register.js
 * 
 * JavaScript for outside/register template
 * 
 */
function toggleOrganizationDetails() {
	var visible = $("input[name='register_organization']:checked").val();
	console.log(visible);
	if(visible == "false") {
		$("#organizationDetails").fadeOut("slow");
	}
	else {
		$("#organizationDetails").fadeIn();
	}	
}

$(document).ready(function() {
	toggleOrganizationDetails();
	$("input:radio").change(function() {
		toggleOrganizationDetails();
	});
});
