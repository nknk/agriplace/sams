from django.contrib import admin

from core.admin import BaseAdmin, BaseTabularInline
from reporting.models import SqlReport, SqlReportQuery


class SqlReportInline(BaseTabularInline):
    model = SqlReport
    readonly_fields = ('uuid', 'report_document', 'remarks', 'modified_time', 'created_time', 'is_test')
    ordering = ('-created_time',)


class SqlReportAdmin(BaseAdmin):
    model = SqlReport
    list_display = ('uuid', 'report_document', 'query', 'created_time')
    ordering = ('-created_time',)


class SqlReportQueryAdmin(BaseAdmin):
    model = SqlReportQuery
    list_display = ('uuid', 'query', 'created_time')
    ordering = ('-created_time',)

    inlines = [
        SqlReportInline
    ]


admin.site.register(SqlReport, SqlReportAdmin)
admin.site.register(SqlReportQuery, SqlReportQueryAdmin)
