from datetime import datetime

from celery.task import task
from celery.utils.log import get_task_logger
from django.db import connection
from django.core.files import File
import unicodecsv as csv

from reporting.models import SqlReport, SQL_QUERY, SqlReportQuery

logger = get_task_logger(__name__)


def dictfetchall(cursor):
    # Return all rows from a cursor as a dict
    columns = [col[0] for col in cursor.description]
    data = [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
    return columns, data


@task()
def sql_report_task():
    today_datetime = datetime.now()
    file_name = "{}--sql_report.csv".format(today_datetime.strftime("%Y-%m-%d--%H:%M:%S"))

    with open("/tmp/{}".format(file_name), 'wb+') as f:
        cursor = connection.cursor()
        cursor.execute(SQL_QUERY)
        keys, data = dictfetchall(cursor)
        dict_writer = csv.DictWriter(f, keys)
        dict_writer.writeheader()
        dict_writer.writerows(data)

        sql_report = SqlReport()
        sql_report.report_document.save(file_name, File(f))


@task()
def custom_sql_report_task(uuid):
    sql_query = SqlReportQuery.objects.get(uuid=uuid)
    today_datetime = datetime.now()
    file_name = "{}--sql_report.csv".format(today_datetime.strftime("%Y-%m-%d--%H:%M:%S"))

    with open("/tmp/{}".format(file_name), 'wb+') as f:
        cursor = connection.cursor()
        cursor.execute(sql_query.query)
        keys, data = dictfetchall(cursor)
        dict_writer = csv.DictWriter(f, keys)
        dict_writer.writeheader()
        dict_writer.writerows(data)

        sql_report = SqlReport(query=sql_query)
        sql_report.report_document.save(file_name, File(f))
