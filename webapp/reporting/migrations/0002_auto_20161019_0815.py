# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('reporting', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SqlReportQuery',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('query', models.TextField(verbose_name='Query')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'SqlReportQuery',
            },
        ),
        migrations.AddField(
            model_name='sqlreport',
            name='query',
            field=models.ForeignKey(related_name='reports', to='reporting.SqlReportQuery', null=True),
        ),
    ]
