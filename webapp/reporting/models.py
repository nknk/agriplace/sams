from django.db import models

from core.models import UuidModel
from django.utils.translation import ugettext_lazy as _

SQL_QUERY = """
select
o.producer_number as "TELER# HZPC",
o.ggn_number as "GGN#",
o.name as "BEDRIJFSNAAM" ,
pt.name as "PRODUCT",
p.area as "HA GECERT. PRODUCT",
ppo.name as "AFNEMER",
pp.contract_number as "TELER# AFNEMER",
wc.audit_date_planned as "AUDIT DATUM",
date(wcf.created_time) as "CERT. DATUM", 
wc.assessment_status as "CERT. STATUS",
o.mailing_address1 as "ADRES(LIJN 1)",
o.mailing_address2 as "ADRES(LIJN 2)",
o.mailing_postal_code as "POSTCODE",
o.mailing_city as "PLAATS",
o.mailing_country as "LAND",
o.phone as "TELEFOON",
o.email as "EMAIL" , 
auditor_user.username as "AUDITOR"
from "Organization" o
inner join "Assessment" a on a.organization_id = o.uuid
inner join "Assessment_products" ap on ap.assessment_id = a.uuid
inner join "Product" p on p.uuid = ap.product_id
inner join "ProductType" pt on pt.uuid = p.product_type_id
left join "ProductPartner" pp on pp.product_id = p.uuid
left join "Organization" ppo on pp.organization_uuid_id = ppo.uuid
inner join "HzpcWorkflowContext" wc on wc.assessment_id = a.uuid
inner join "HzpcWorkflowContextFiles" wcf on (wcf."attachment_type" = 'proof_of_participation' AND wcf.hzpc_workflow_id = wc.uuid )
inner join "auth_user" auditor_user on wc.auditor_user_id = auditor_user.id
"""


class SqlReportQuery(UuidModel):
    query = models.TextField(_("Query"))

    class Meta(UuidModel.Meta):
        db_table = 'SqlReportQuery'


class SqlReport(UuidModel):
    report_document = models.FileField(max_length=255, upload_to="reports")
    query = models.ForeignKey("SqlReportQuery", related_name="reports", null=True)

    class Meta(UuidModel.Meta):
        db_table = 'SqlReport'
