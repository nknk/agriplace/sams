#!/bin/sh

watchify frontend/bundle.js -t node-lessify -t coffeeify -t brfs -v --full-path=false -o frontend/static/frontend/agriplace-bundle.js
