from django.contrib import admin

from core.admin import BaseAdmin
from user_import.forms import UserImportForm
from user_import.helpers import handle_data_row
from user_import.models import UserImport


class ImportUsersAdmin(BaseAdmin):
    model = UserImport
    form = UserImportForm

    list_display = ('modified_time', 'status', 'file', 'language', 'farm_group')
    list_filter = ('modified_time', 'status')
    search_fields = ('import_date',)
    raw_id_fields = ('farm_group',)
    ordering = ('-modified_time',)

    actions = ['import_users']

    def import_users(self, request, queryset):
        for import_data in queryset:
            handle_data_row(import_data)

        self.message_user(request, "Command has been completed. Kindly check status and report.")

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


admin.site.register(UserImport, ImportUsersAdmin)
