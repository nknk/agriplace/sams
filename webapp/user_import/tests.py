# coding=utf-8
from django.contrib.auth import get_user_model
from django.test import TestCase

import factory

from organization.models import OrganizationMembership, OrganizationType
from registration.models import RegistrationProfile
from user_import.factories import generate_user_import_data_file, UserImportFactory
from user_import.helpers import handle_data_row
from user_import.models import UserImport


class UserImportTests(TestCase):
    """
    Unit tests for user_import app
    """
    def setUp(self):
        self.data = generate_user_import_data_file(5)
        self.user_import_xls = UserImportFactory(
            file=factory.django.FileField(filename='user_data.xls', data=self.data.xls)
        )
        self.user_import_json = UserImportFactory(
            file=factory.django.FileField(filename='user_data.json', data=self.data.json)
        )

    def testUserImportCreation(self):
        self.assertEqual(UserImport.objects.count(), 2)

    def testWrongFileFormat(self):
        handle_data_row(self.user_import_json)
        self.assertEqual(self.user_import_json.status, 'failed')
        self.assertEqual(self.user_import_json.description, 'wrong file format (xls expected)')

    def testUploadFromXls(self):
        handle_data_row(self.user_import_xls)
        self.assertEqual(self.user_import_xls.status, 'success')
        self.assertEqual(self.user_import_xls.description, '')

        for item in self.data.dict:
            # user created
            self.assertTrue(get_user_model().objects.filter(username=item['EMAIL']).exists())
            user_instance = get_user_model().objects.get(username=item['EMAIL'])

            # user organization created and attached to user
            self.assertEqual(user_instance.organizations.all().count(), 1)
            user_organization_instance = user_instance.organizations.first()

            # registration profile created
            self.assertTrue(RegistrationProfile.objects.filter(user=user_instance).exists())

            # organization membership created
            self.assertTrue(OrganizationMembership.objects.filter(
                primary_organization=self.user_import_xls.farm_group,
                secondary_organization=user_organization_instance,
                organization_type=OrganizationType.objects.get(name='producer', slug='producer'),
                membership_number=item['MEMBERSHIP_NO'],
                role=None
            ).exists())


