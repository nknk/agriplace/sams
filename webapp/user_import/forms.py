from django import forms

from user_import.models import UserImport


class UserImportForm(forms.ModelForm):

    class Meta:
        model = UserImport
        fields = [
            'file',
            'language',
            'farm_group',
            'role',
            'status',
            'description',
            'report'
        ]
