# -*- encoding: utf-8 -*-
import factory
import tablib
from factory.django import DjangoModelFactory
from faker import Faker

from organization.tests.factories import OrganizationFactory, UserFactory


USER_IMPORT_HEADERS = [
        'MEMBERSHIP_NO',
        'ORGANIZATION_NAME',
        'ADDRESS_LINE_1',
        'ADDRESS_LINE2',
        'POSTAL_CODE',
        'CITY',
        'COUNTRY',
        'PHONE',
        'MOBILE',
        'WEBSITE',
        'GGN_NO',
        'CHAMBER_OF_COMMERCE',
        'GLN_NO',
        'EMAIL',
        'FIRST_NAME',
        'LAST_NAME',
        'GENDER',
        'DATE_OF_BIRTH',
        'PHONE_NO'
    ]


def generate_user_import_data_file(amount=1):
    fake = Faker()
    data = tablib.Dataset()
    data.headers = USER_IMPORT_HEADERS
    for _ in range(amount):
        profile = fake.simple_profile(sex=None)
        data.append([
            fake.random_int(min=0, max=9999),
            fake.company(),
            fake.secondary_address(),
            '', '',
            fake.city(),
            fake.random_sample(elements=('Netherlands', 'Spain', 'South Africa'))[0],
            '', '', '', '', '', '',
            fake.safe_email(),
            profile['name'].split()[0],
            profile['name'].split()[1],
            profile['sex'].lower(),
            fake.date(pattern="%d/%m/%Y"),
            fake.phone_number()
        ])
    return data


class UserImportFactory(DjangoModelFactory):
    """
    Blank UserImport model factory
    """
    class Meta:
        model = 'user_import.UserImport'

    farm_group = factory.SubFactory(OrganizationFactory)
    file = factory.django.FileField(filename='user_data.xls')
    user = factory.SubFactory(UserFactory)
