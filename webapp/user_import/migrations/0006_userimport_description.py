# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0005_auto_20161227_1304'),
    ]

    operations = [
        migrations.AddField(
            model_name='userimport',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]
