# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def truncate_user_import(apps, schema_editor):
    """
    Removes all instances of UserImport before schema changes will be applied
    """
    apps.get_model("user_import", "UserImport").objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0002_auto_20161107_0750'),
    ]

    operations = [
        migrations.RunPython(truncate_user_import),
    ]
