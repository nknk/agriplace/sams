# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0003_auto_20161222_1145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userimport',
            name='platform_context',
        ),
    ]
