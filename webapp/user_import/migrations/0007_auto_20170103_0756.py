# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0006_userimport_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userimport',
            name='file',
            field=models.FileField(upload_to=b'users/import/', max_length=255, verbose_name='File to import'),
        ),
        migrations.AlterField(
            model_name='userimport',
            name='report',
            field=models.FileField(max_length=255, upload_to=b'users/import/reports/', null=True, verbose_name='Report', blank=True),
        ),
    ]
