# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0004_remove_userimport_platform_context'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userimport',
            name='import_date',
        ),
        migrations.RemoveField(
            model_name='userimport',
            name='import_result',
        ),
    ]
