# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userimport',
            name='format',
        ),
        migrations.AlterField(
            model_name='userimport',
            name='file',
            field=models.FileField(upload_to=b'users/import/', max_length=255, verbose_name='Import xls file'),
        ),
    ]
