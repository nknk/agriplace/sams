# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('organization', '0005_auto_20161103_1130'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserImport',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('language', models.CharField(default=b'nl', max_length=10, verbose_name='Language of users', choices=[(b'en', b'English'), (b'nl', b'Nederlands'), (b'es', 'Espa\xf1ol')])),
                ('platform_context', models.CharField(default=b'agriplace', max_length=50, verbose_name='Platform context', choices=[(b'agriplace', b'Agriplace'), (b'hzpc', b'Hzpc'), (b'farm_group', b'FarmGroup')])),
                ('import_date', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(max_length=20, null=True, blank=True)),
                ('import_result', models.TextField()),
                ('file', models.FileField(upload_to=b'users/import/', max_length=255, verbose_name='Import file')),
                ('report', models.FileField(max_length=255, upload_to=b'users/import/reports/', null=True, verbose_name='Report file', blank=True)),
                ('format', models.IntegerField(default=0, verbose_name='Format', choices=[(b'', b'---'), (0, b'csv'), (1, b'xls'), (2, b'xlsx'), (3, b'tsv'), (4, b'ods'), (5, b'json'), (6, b'yaml'), (7, b'html')])),
                ('farm_group', models.ForeignKey(related_name='user_imports', verbose_name='Farm group', blank=True, to='organization.Organization', null=True)),
                ('user', models.ForeignKey(related_name='user_imports', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'UserImport',
                'permissions': (('view_importusers', 'Can view import users'),),
            },
        ),
    ]
