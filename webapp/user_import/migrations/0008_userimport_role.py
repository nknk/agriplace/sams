# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_import', '0007_auto_20170103_0756'),
    ]

    operations = [
        migrations.AddField(
            model_name='userimport',
            name='role',
            field=models.ForeignKey(related_name='role_user_import', verbose_name='role', blank=True, to='auth.Group', null=True),
        ),
    ]
