import uuid
from django.core.files import File
from django.utils.six import moves
import tablib
import xlrd
from tablib.formats._xls import detect

from organization.admin import OrganizationUserResource
from organization.utils import ORGANIZATION_USER_DATE_FIELDS


def create_xls_dataset(in_stream):
    """
    Create dataset from first sheet.
    """
    xls_book = xlrd.open_workbook(file_contents=in_stream)
    dataset = tablib.Dataset()
    sheet = xls_book.sheets()[0]

    dataset.headers = sheet.row_values(0)
    date_field_indices = [i for i, x in enumerate(dataset.headers) if x in ORGANIZATION_USER_DATE_FIELDS]
    for i in moves.range(1, sheet.nrows):
        row_values = sheet.row_values(i)
        for index in date_field_indices:
            date_value = row_values[index]
            if date_value and isinstance(date_value, float):
                date_values = xlrd.xldate_as_tuple(row_values[index], xls_book.datemode)
                row_values[index] = u'{}/{}/{}'.format(date_values[1], date_values[2], date_values[0])

        dataset.append(row_values)
    return dataset


def handle_data_row(row):
    file_content = row.file.read()

    if detect(file_content):
        dataset = create_xls_dataset(file_content)
        resource = OrganizationUserResource(
            row.language, row.farm_group, row.role
        )
        results = resource.import_data(dataset, dry_run=False, raise_errors=False, collect_failed_rows=True,
                                       use_transactions=False)
        errors_count = results.totals.get('error', 0)
        dataset_count = len(dataset.dict)
        if errors_count:
            file_name = "{}.xls".format(str(uuid.uuid4()))
            with open(file_name, "w+") as f:
                f.write(results.failed_dataset.xls)
                f.seek(0)
                row.report.save(file_name, File(f))
                row.status = 'failed' if dataset_count == errors_count else 'partial success'
        else:
            row.report.delete(False)
            row.status = 'success'
    else:
        row.status = 'failed'
        row.description = 'wrong file format (xls expected)'

    row.save()
