from django.conf import settings
from django.contrib.auth.models import Group
from django.db import models
from django.utils.translation import ugettext as _

from core.models import UuidModel
from organization.models import Organization


class UserImport(UuidModel):
    language = models.CharField(_("Language of users"), max_length=10, choices=settings.LANGUAGES, default="nl")
    farm_group = models.ForeignKey(Organization, related_name="user_imports", verbose_name=_("Farm group"),
                                   blank=True, null=True)
    role = models.ForeignKey(Group, verbose_name=_("role"), related_name='role_user_import',
                             null=True, blank=True)
    status = models.CharField(max_length=20, null=True, blank=True)
    description = models.TextField(blank=True)
    file = models.FileField(max_length=255, verbose_name=_('File to import'), upload_to="users/import/")
    report = models.FileField(max_length=255, verbose_name=_('Report'), upload_to="users/import/reports/",
                              blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_imports')

    class Meta:
        db_table = 'UserImport'
        permissions = (
            ('view_importusers', 'Can view import users'),
        )
