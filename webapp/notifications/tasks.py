from __future__ import absolute_import

from datetime import datetime, timedelta
import uuid

from celery import shared_task
from celery.utils.log import get_task_logger
from celery.task import task
from django.contrib.auth.models import User
from django.db.models.loading import get_model
from django.test.client import RequestFactory

from assessment_workflow.managers import INTERNAL_AUDIT_REQUEST
from notifications.models import ACTION_REMINDER, SUBSCRIPTION, REMINDER

logger = get_task_logger(__name__)


@shared_task
def send_email_with_attachment(notification, workflow_id, pdf_responses):
    # because of circular dependency we put it here
    try:
        from notifications.utils import send_email_notification
        assessment_workflow = get_model('assessment_workflow', 'AssessmentWorkflow').objects.get(uuid=workflow_id)

        for key, response in pdf_responses.iteritems():
            file_name = "{}.pdf".format(str(uuid.uuid4()))
            file_path = "/tmp/{}".format(file_name)
            logger.info("saved file {}".format(file_path))
            target = open(file_path.format(), 'w')
            target.write(response.decode('base64'))
            target.close()
            assessment_workflow.save_workflow_file(notification, file_path, file_name, key)
        send_email_notification(notification, assessment_workflow)
    except BaseException as ex:
        logger.info(ex)


@task()
def action_required_notification_task():
    date_threshold = datetime.now() - timedelta(days=20)
    assessment_workflows = get_model('assessment_workflow', 'AssessmentWorkflow').objects.filter(
        assessment_status='action_required',
        action_required_date__lte=date_threshold.date(),
        action_required_notification_status=False
    )
    for assessment_workflow in assessment_workflows:
        try:
            from notifications.utils import send_email_for_notification_type
            request = RequestFactory().get('/')
            request.user = User.objects.get(id=assessment_workflow.author_id)
            request.session = {}
            send_email_for_notification_type(ACTION_REMINDER, assessment_workflow.uuid, request)
            assessment_workflow.action_required_notification_status = True
            assessment_workflow.save()
        except BaseException as ex:
            logger.info(ex)


@task()
def subscription_notification_task():
    today_date = datetime.now().date()
    workflow_model = get_model('assessment_workflow', 'AssessmentWorkflow')
    notification_model = get_model('notifications', 'NotificationSettings')
    active_notification_settings = notification_model.objects.get_active()
    if active_notification_settings and active_notification_settings.gg_subscription_notification_date == today_date:
        assessment_workflows = workflow_model.objects.filter(
            harvest_year=today_date.year,
            assessment_status=INTERNAL_AUDIT_REQUEST,
            selected_workflow='hzpc',
            assessment_type__code='globalgap_ifa_v5'
        )
        for assessment_workflow in assessment_workflows:
            try:
                from notifications.utils import send_email_notification
                send_email_notification(SUBSCRIPTION, assessment_workflow)
            except BaseException as ex:
                logger.info(ex)


@task()
def reminder_notification_task():
    today_date = datetime.now().date()
    workflow_model = get_model('assessment_workflow', 'AssessmentWorkflow')
    notification_model = get_model('notifications', 'NotificationSettings')
    active_notification_settings = notification_model.objects.get_active()
    if active_notification_settings and active_notification_settings.gg_subscription_reminder_date == today_date:
        assessment_workflows = workflow_model.objects.filter(
            harvest_year=today_date.year,
            assessment_status=INTERNAL_AUDIT_REQUEST,
            selected_workflow='hzpc',
            assessment_type__code='globalgap_ifa_v5'
        )
        for assessment_workflow in assessment_workflows:
            try:
                from notifications.utils import send_email_notification
                send_email_notification(REMINDER, assessment_workflow)
            except BaseException as ex:
                logger.info(ex)
