from django.utils.translation import ugettext_lazy as _
from notifications.models import NOTIFICATION_TYPES, NOTIFICATION_CHANNELS


def send_notification(notification_type, channel, target, context):
    status = description = ""

    if notification_type in NOTIFICATION_TYPES and channel in NOTIFICATION_CHANNELS:
        if channel == 'email':
            pass
        else:
            status = 'error'
            description = _("Sending notifications using %s not implemented yet") % channel
    else:
        status = 'error'
        description = _("Unknown notification type and/or channel")

    return status, description
