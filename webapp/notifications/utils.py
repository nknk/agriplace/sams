from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.mail import send_mail, get_connection
from django.db.models.loading import get_model
from datetime import timedelta, datetime
from notifications.tasks import send_email_with_attachment
from wkhtmltopdf.views import PDFTemplateResponse

from notifications.models import (
    NotificationJournal, Notification, NotificationSettings, EMAIL_CHANNEL, STATUS_SUCCESS, GROWER_NOTIFICATIONS,
    AUDIT_RESULT, ACTION_REMINDER, OBTAINING_CERTIFICATE, AUDIT_SUMMARY, PROOF_OF_PARTICIPATION
)


def get_target_for_notification(notification_type, assessment_workflow):
    if not settings.NOTIFICATIONS_EMAIL_TEST_MODE:
        if notification_type in GROWER_NOTIFICATIONS:
            author = assessment_workflow.author
            if author:
                return "%s (%s)" % (author.get_full_name(), author.username)
        return ""
    else:
        return settings.NOTIFICATIONS_EMAIL_TEST_ADDRESS


def send_email_notification(notification_type, assessment_workflow):
    if assessment_workflow.selected_workflow != 'hzpc':
        return False

    with get_connection(
            backend='django.core.mail.backends.smtp.EmailBackend',
            host=settings.HZPC_EMAIL_HOST,
            port=settings.HZPC_EMAIL_PORT,
            username=settings.HZPC_EMAIL_HOST_USER,
            password=settings.HZPC_EMAIL_HOST_PASSWORD,
    ) as connection:
        notification = Notification(
            channel=EMAIL_CHANNEL,
            notification_type=notification_type,
            workflow_instance=assessment_workflow,
            connection=connection
        )
        status, description = notification.send()
        NotificationJournal.objects.create(
            notification_type=notification_type,
            notification_channel=EMAIL_CHANNEL,
            target=get_target_for_notification(notification_type, assessment_workflow),
            status=status,
            description=description
        )
        return True if status == STATUS_SUCCESS else False


def get_attachment_templates(notification_type):
    files = {}
    if notification_type == AUDIT_RESULT or notification_type == ACTION_REMINDER:
        files[AUDIT_SUMMARY] = 'notifications/attachments/summary.html'
    elif notification_type == OBTAINING_CERTIFICATE:
        files[AUDIT_SUMMARY] = 'notifications/attachments/summary.html'
        files[PROOF_OF_PARTICIPATION] = 'notifications/attachments/statement.html'
    return files


def create_pdf_context(attachment_type, workflow_id, notification_settings):
    try:
        assessment_workflow = get_model('assessment_workflow', 'AssessmentWorkflow').objects.get(uuid=workflow_id)
    except:
        return {}
    context_dict = {}
    if notification_settings:
        context_dict.update(notification_settings.__dict__)
        context_dict.update({
            'coordinator_signature': notification_settings.coordinator_signature.url,
            'farm_group_logo': notification_settings.farm_group_logo.url
        })

    audit_date_planned = assessment_workflow.audit_date_planned
    audit_date_actual_start = assessment_workflow.audit_date_actual_start
    audit_date = audit_date_planned if audit_date_planned else audit_date_actual_start
    valid_date = audit_date_actual_start + relativedelta(years=1)
    context_dict.update({
        'assessment_type': assessment_workflow.assessment_type.name,
        'audit_date': audit_date.strftime('%d.%m.%y'),
        'actual_audit_start_date': assessment_workflow.audit_date_actual_start.strftime('%d.%m.%y'),
        'valid_date': valid_date.strftime('%d.%m.%y'),
        'auditor': u"{} {}".format(assessment_workflow.auditor_user.first_name,
                                   assessment_workflow.auditor_user.last_name),
        'products': assessment_workflow.assessment.products.all(),
    })
    organization = assessment_workflow.author_organization
    membership = assessment_workflow.membership
    if organization:
        context_dict.update({
            'grower_number': membership.membership_number,
            'country': organization.mailing_country,
            'mailing_address1': organization.mailing_address1,
            'mailing_address2': organization.mailing_address2,
            'post_code': organization.mailing_postal_code,
            'city': organization.mailing_city,
            'ggn_number': organization.ggn_number,
            'org_name': organization.name
        })
    if attachment_type == AUDIT_SUMMARY:
        context_dict.update({
            'workflow_judgement': assessment_workflow.judgement,
            'judgements': assessment_workflow.assessment.judgements.filter(value='no'),
        })
    elif attachment_type == PROOF_OF_PARTICIPATION:
        today_date = datetime.now().date()
        context_dict.update({
            'status_date': today_date.strftime('%d.%m.%y')
        })
    return context_dict


def get_pdf_responses(files, request, workflow_id):
    responses = {}
    active_notification_settings = NotificationSettings.objects.get_active()
    for key, file in files.iteritems():
        context = create_pdf_context(key, workflow_id, active_notification_settings)
        response = PDFTemplateResponse(
            request=request, context=context, template=file, cmd_options={'encoding': 'utf8', 'quiet': True}
        )
        pdf = response.rendered_content
        responses[key] = pdf.encode('base64')

    return responses


def send_email_for_notification_type(notification, workflow_id, request):
    files = get_attachment_templates(notification)
    pdf_responses = get_pdf_responses(files, request, workflow_id)
    send_email_with_attachment.delay(notification, workflow_id, pdf_responses)
