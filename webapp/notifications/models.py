import copy
from datetime import datetime
import os
from smtplib import SMTPException
import uuid
from PyPDF2.merger import PdfFileMerger
from dateutil import parser

from django.conf import settings
from django.core.mail import EmailMessage
from django.db import models
from django.template import Template, TemplateDoesNotExist, Context
from django.template.loader import get_template
from django.utils.translation import ugettext as _

from core.models import UuidModel

from django.core.files import File
from StringIO import StringIO
from dateutil.relativedelta import relativedelta
from organization.models import (Certification, CertificationCrop,
                                 CertificationFile, CertificationType, ProductType)

SUBSCRIPTION = "subscription"
REMINDER = "reminder"
AUDIT_DATE_PLANNED = "audit_date_planned"
AUDIT_RESULT = "audit_result"
ACTION_REMINDER = "acton_reminder"
OBTAINING_CERTIFICATE = "obtaining_certificate"

AUDIT_SUMMARY = 'audit_summary'
PROOF_OF_PARTICIPATION= 'proof_of_participation'

GROWER_NOTIFICATIONS = [SUBSCRIPTION, REMINDER, AUDIT_DATE_PLANNED, AUDIT_RESULT, ACTION_REMINDER, OBTAINING_CERTIFICATE]

NOTIFICATION_TYPES = [
    (SUBSCRIPTION, _("Subscription to use HZPC audit tool for GGAP certification in running year")),
    (REMINDER, _("Reminder to subscribe to use HZPC audit tool")),
    (AUDIT_DATE_PLANNED, _("Audit date planned")),
    (AUDIT_RESULT, _("Audit result")),
    (ACTION_REMINDER, _("Corrective acton reminder")),
    (OBTAINING_CERTIFICATE, _("Obtaining certificate"))
]

ATTACHMENT_TYPES = [
    (AUDIT_SUMMARY, _("Audit summary")),
    (PROOF_OF_PARTICIPATION, _("Proof of participation")),
]

# Now just email. can be extended with sms for example
EMAIL_CHANNEL = "email"
NOTIFICATION_CHANNELS = [
    (EMAIL_CHANNEL, _("Email"))
]

STATUS_IN_PROGRESS = "in_progress"
STATUS_SUCCESS = "success"
STATUS_ERROR = "error"
DELIVERY_STATUSES = [
    (STATUS_IN_PROGRESS, _("In progress")),
    (STATUS_SUCCESS, _("Success")),
    (STATUS_ERROR, _("Error"))
]


def get_notification_template(notification_type):
    template_prefix = "notifications/email/"
    try:
        template = get_template(template_prefix+notification_type+'.txt')
    except TemplateDoesNotExist:
        template = Template("")
    return template


class NotificationJournal(UuidModel):
    created_date_time = models.DateTimeField(default=datetime.now)
    notification_type = models.CharField(choices=NOTIFICATION_TYPES, max_length=50)
    notification_channel = models.CharField(choices=NOTIFICATION_CHANNELS, max_length=50)
    target = models.CharField(max_length=200)
    status = models.CharField(choices=DELIVERY_STATUSES, max_length=50)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'NotificationJournal'
        verbose_name = 'Notification'
        verbose_name_plural = 'Journal'
        permissions = (
            ('view_notification_journal', 'Can view notification journal'),
        )


class ActiveManager(models.Manager):

    """ get_active() trying to get "freshest" settings and returns None if there are no settings at all """
    def get_active(self):
        settings_list = self.filter(active=True).order_by("-modified_time")
        if settings_list.count():
            return settings_list[0]
        else:
            return None


class NotificationSettings(UuidModel):
    active = models.BooleanField("Current settings", default=False)
    farm_group_portal_url = models.URLField("URL farm group portal")
    no_reply_address = models.EmailField("No-reply email address")
    gg_subscription_notification_date = models.DateField("Subscription notification date (GLOBALG.A.P.)")
    gg_subscription_reminder_date = models.DateField("Reminder subscription date (GLOBALG.A.P.)")
    farm_group_phone = models.CharField("Phone# farm group", max_length=20)
    coordinator_name = models.CharField("Coordinator name", max_length=200)
    coordinator_email = models.EmailField("Coordinator email")
    coordinator_signature = models.ImageField("Coordinator signature (image)",
                                              max_length=255, upload_to="farm_group/images")
    farm_group_logo = models.ImageField("Logo farm group", max_length=255, upload_to="farm_group/images")
    certificate = models.FileField("Certificate File", max_length=255, upload_to="farm_group/reports")

    objects = ActiveManager()

    class Meta:
        db_table = 'NotificationSettings'
        verbose_name = 'Record'
        verbose_name_plural = 'Settings'
        permissions = (
            ('view_notification_settings', 'Can view notification settings'),
        )


class Notification(object):
    """
    A container for notifications. Made by analogue of Django EmailMessage class
    """

    def __init__(self, channel, notification_type, workflow_instance, notification_settings=None, connection=None):
        if not channel == EMAIL_CHANNEL or not notification_type in dict(NOTIFICATION_TYPES).keys():
            raise NotImplementedError
        else:
            self.active_notification_settings = notification_settings or NotificationSettings.objects.get_active()
            self.assessment_workflow_dict = self._get_assessment_workflow_dict(workflow_instance)
            self.to = self._get_email_to(notification_type, workflow_instance)
            self.subject = self._get_email_subject(notification_type)
            self.body = self._get_email_body(notification_type)
            self.from_email = self._get_from_email()
            self.attachments = self._get_email_attachments(notification_type, workflow_instance)
            self.reply_to = self._get_email_reply_to(notification_type, workflow_instance)
            self.connection = connection

            self.copy_assessment_into_user_certificate(workflow_instance, notification_type)


    def copy_assessment_into_user_certificate(self, assessment_workflow, notification_type):
        """
        Copy assessment information into user organization certificates
        """
        if notification_type != OBTAINING_CERTIFICATE:
            return None

        # Get Assessment Instance
        assessment = assessment_workflow.assessment

        # Getting all the assessment crops with area
        crops = assessment.products.select_related('product_type').all()

        cert_file = False
        for file in self.attachments:
            if(file[0] == 'certificate.pdf'):
                # File content
                cert_file = StringIO(file[1])
                break


        # Hard coded cert type as per talks with Maarten
        cert_type = CertificationType.objects.get(name='GLOBALG.A.P.')
        issue_date = assessment_workflow.audit_date_actual_start
        expiry_date = assessment_workflow.audit_date_actual_start + relativedelta(years=1)

        # Copying assessment information into user organization certificate
        certificate = Certification.objects.create(
            organization=assessment.organization,
            certification_type=cert_type,
            certificate_number='',
            issue_date=issue_date,
            expiry_date=expiry_date,
            issuer='MPS-ECAS B.V.'
        )

        if crops:
            # Copy assisment crops into certificate croup
            crops_obj = [
                CertificationCrop(
                    product_type=crop.product_type,
                    area=crop.area,
                    certification=certificate
                ) for crop in crops
            ]

            CertificationCrop.objects.bulk_create(crops_obj)


        # Copying the certificate file into
        if cert_file:
            cert_name = 'certificate.pdf'
            certificate_file = CertificationFile(
                original_file_name=cert_name,
                file_type='certificate',
                certification=certificate
            )
            file_name = "{}-{}".format(str(uuid.uuid4()), cert_name)
            certificate_file.file.save(file_name, File(cert_file))


    def send(self):
        # get setting from django (do we really sending emails?)
        if settings.NOTIFICATIONS_EMAIL_STATUS:
            email = EmailMessage(
                subject=self.subject,
                body=self.body,
                from_email=self.from_email,
                to=self.to,
                attachments=self.attachments,
                connection=self.connection
            )
            try:
                email.send()
                status = STATUS_SUCCESS
                description = ""
            except SMTPException as ex:
                status = STATUS_ERROR
                description = ex
            return status, description
        else:
            return STATUS_SUCCESS, "Test mode: only records in Notification journal"

    def _get_email_to(self, notification_type, workflow_instance):
        to = []
        # check setting TEST_MODE. If so send to demo email address
        if not settings.NOTIFICATIONS_EMAIL_TEST_MODE:
            if notification_type in GROWER_NOTIFICATIONS:
                author = workflow_instance.author
                if author:
                    to.append(author.username)
        else:
            to.append(settings.NOTIFICATIONS_EMAIL_TEST_ADDRESS)
        return to

    def _get_email_subject(self, notification_type):
        workflow_dict = self.assessment_workflow_dict
        harvest_year = workflow_dict.get('harvest_year', '')
        planned_audit_date = workflow_dict.get('audit_date_planned', '')
        if planned_audit_date:
            if isinstance(planned_audit_date, basestring):
                pa_date = parser.parse(planned_audit_date)
                planned_audit_date = pa_date.strftime('%d.%m.%y %H:%M')
            else:
                planned_audit_date = planned_audit_date.strftime('%d.%m.%y  %H:%M')

        assessment_type = workflow_dict.get('assessment_type', '')

        return {
            SUBSCRIPTION: "Aanmelding GLOBALG.A.P. %s" % (harvest_year,),
            REMINDER: "Herinnering aanmelding GLOBALG.A.P. %s" % (harvest_year,),
            AUDIT_DATE_PLANNED: "Geplande audit datum %s - %s" % (
                assessment_type,
                planned_audit_date
            ),
            AUDIT_RESULT: "Geconstateerde afwijkingen %s" % (assessment_type,),
            ACTION_REMINDER: "Geconstateerde afwijkingen %s" % (assessment_type,),
            OBTAINING_CERTIFICATE: "Uw %s certificaat" % (assessment_type,)
        }.get(notification_type, "")

    def _get_email_body(self, notification_type):
        assessment_workflow_dict = self.assessment_workflow_dict
        context = Context(assessment_workflow_dict)
        return get_notification_template(notification_type).render(context)

    def _get_from_email(self):
        if self.active_notification_settings:
            return u"{} <{}>".format(
                self.active_notification_settings.coordinator_name,
                self.active_notification_settings.no_reply_address
            )
        return ""

    def _get_email_attachments(self, notification_type, workflow_instance):
        attachment_types = []
        attachments = []
        active_notification_settings = None
        if notification_type in [AUDIT_RESULT, ACTION_REMINDER]:
            attachment_types.append(AUDIT_SUMMARY)
        elif notification_type == OBTAINING_CERTIFICATE:
            attachment_types.append(AUDIT_SUMMARY)
            attachment_types.append(PROOF_OF_PARTICIPATION)
            active_notification_settings = NotificationSettings.objects.get_active()
            if active_notification_settings:
                content = active_notification_settings.certificate.file.read()
                attachments.append(("{}.pdf".format("certificate"), content, 'application/pdf'))
        else:
            return []

        files = workflow_instance.get_email_attachments(attachment_types)
        if notification_type == OBTAINING_CERTIFICATE and workflow_instance.selected_workflow == 'hzpc':
            if attachments and active_notification_settings:
                # For HZPC merge the proof pf participation and certificate file.
                proof_of_participation_file = files[PROOF_OF_PARTICIPATION]
                certificate_file = active_notification_settings.certificate.file
                merged_file = self._merge_files([certificate_file, proof_of_participation_file])
                attachments[0] = ("{}.pdf".format("certificate"), merged_file, 'application/pdf')
                # Delete from the files list.
                del files[PROOF_OF_PARTICIPATION]
        for key, path in files.iteritems():
            content = path.file.read()
            attachments.append(("{}.pdf".format(key), content, 'application/pdf'))

        return attachments

    def _merge_files(self, pdf_files=()):
        merger = PdfFileMerger()
        for pdf in pdf_files:
            merger.append(pdf, import_bookmarks=False)
        file_path = "/tmp/{}".format(str(uuid.uuid4()))
        with open(file_path, 'wb') as fout:
            merger.write(fout)
        with open(file_path, 'rb') as fin:
            merged_file = fin.read()
        os.remove(file_path)
        return merged_file

    def _get_email_reply_to(self, notification_type, workflow_instance):
        """ this method will be used only in Django 1.9 """
        reply_to = []
        if notification_type in [AUDIT_DATE_PLANNED]:
            reply_to.append(workflow_instance.auditor_user.username)
        elif notification_type in [AUDIT_RESULT, ACTION_REMINDER]:
            if self.active_notification_settings:
                reply_to.append(self.active_notification_settings.coordinator_email)
        return reply_to

    def _get_assessment_workflow_dict(self, workflow_instance):
        assessment_workflow_dict = copy.deepcopy(workflow_instance.__dict__)
        try:
            assessment_type = workflow_instance.assessment_type.name
            assessment_workflow_dict.update({'assessment_type': assessment_type})
        except AttributeError:
            pass

        try:
            auditor_full_name = workflow_instance.auditor_user.get_full_name()
            assessment_workflow_dict.update({'auditor_full_name': auditor_full_name})
        except AttributeError:
            pass

        try:
            auditor_phone_number = workflow_instance.auditor_user.profile.phone
            assessment_workflow_dict.update({'auditor_phone_number': auditor_phone_number})
        except AttributeError:
            pass

        if self.active_notification_settings:
            assessment_workflow_dict.update(self.active_notification_settings.__dict__)
        return assessment_workflow_dict
