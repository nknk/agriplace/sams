# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationJournal',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_date_time', models.DateTimeField(default=datetime.datetime.now)),
                ('notification_type', models.CharField(max_length=50, choices=[(b'subscription', 'Subscription to use HZPC audit tool for GGAP certification in running year'), (b'reminder', 'Reminder to subscribe to use HZPC audit tool'), (b'audit_date_planned', 'Audit date planned'), (b'audit_result', 'Audit result'), (b'acton_reminder', 'Corrective acton reminder'), (b'obtaining_certificate', 'Obtaining certificate')])),
                ('notification_channel', models.CharField(max_length=50, choices=[(b'email', 'Email')])),
                ('target', models.CharField(max_length=200)),
                ('status', models.CharField(max_length=50, choices=[(b'in_progress', 'In progress'), (b'success', 'Success'), (b'error', 'Error')])),
                ('description', models.TextField(null=True, blank=True)),
            ],
            options={
                'db_table': 'NotificationJournal',
                'verbose_name': 'Notification',
                'verbose_name_plural': 'Journal',
                'permissions': (('view_notification_journal', 'Can view notification journal'),),
            },
        ),
        migrations.CreateModel(
            name='NotificationSettings',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('active', models.BooleanField(default=False, verbose_name=b'Current settings')),
                ('farm_group_portal_url', models.URLField(verbose_name=b'URL farm group portal')),
                ('no_reply_address', models.EmailField(max_length=254, verbose_name=b'No-reply email address')),
                ('gg_subscription_notification_date', models.DateField(verbose_name=b'Subscription notification date (GLOBALG.A.P.)')),
                ('gg_subscription_reminder_date', models.DateField(verbose_name=b'Reminder subscription date (GLOBALG.A.P.)')),
                ('farm_group_phone', models.CharField(max_length=20, verbose_name=b'Phone# farm group')),
                ('coordinator_name', models.CharField(max_length=200, verbose_name=b'Coordinator name')),
                ('coordinator_email', models.EmailField(max_length=254, verbose_name=b'Coordinator email')),
                ('coordinator_signature', models.ImageField(upload_to=b'farm_group/images', max_length=255, verbose_name=b'Coordinator signature (image)')),
                ('farm_group_logo', models.ImageField(upload_to=b'farm_group/images', max_length=255, verbose_name=b'Logo farm group')),
                ('certificate', models.FileField(upload_to=b'farm_group/reports', max_length=255, verbose_name=b'Certificate File')),
            ],
            options={
                'db_table': 'NotificationSettings',
                'verbose_name': 'Record',
                'verbose_name_plural': 'Settings',
                'permissions': (('view_notification_settings', 'Can view notification settings'),),
            },
        ),
    ]
