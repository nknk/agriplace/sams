from django.contrib import admin

from core.admin import BaseAdmin
from notifications.models import NotificationSettings, NotificationJournal


@admin.register(NotificationSettings)
class NotificationSettingsAdmin(BaseAdmin):
    model = NotificationSettings
    list_display = (
        'coordinator_name',
        'gg_subscription_notification_date',
        'gg_subscription_reminder_date',
        'no_reply_address',
        'active')
    list_filter = ('active', 'coordinator_name')
    search_fields = ('coordinator_name',)
    exclude = ('remarks', 'modified_time', 'created_time', 'is_test')


@admin.register(NotificationJournal)
class NotificationJournalAdmin(BaseAdmin):
    model = NotificationJournal
    list_display = (
        'created_date_time',
        'notification_type',
        'notification_channel',
        'target',
        'status')
    list_filter = ('notification_type', 'notification_channel', 'status')
    date_hierarchy = 'created_date_time'
    exclude = ('remarks', 'modified_time', 'created_time', 'is_test')
