from django.http import HttpResponseRedirect
from django.contrib import auth
from django.conf import settings

from .models import Key


def login(request, key):
    """
    Log in using a key.

    When a visitor opens this view with a valid key, the visitor will be logged
    in using the user associated with the key.

    A successful request will redirect the visitor to the URL associated with
    the key. If the URL is ``None``, a ``next`` parameter in the query string
    will be used. If it also does not exist, the default
    ``settings.LOGIN_REDIRECT_URL`` will be used.

    Visitor with an invalid key will be redirected to the default log in page
    specified in ``settings.LOGIN_REDIRECT_URL``. Any value in the ``next``
    parameter in the query string will be also forwarded.
    """
    next = request.GET.get('next', None)
    if next is None:
        next = settings.LOGIN_REDIRECT_URL

    # Validate the key through the standard Django's authentication mechanism.
    # It also means that the authentication backend of this
    # application has to be added to the authentication backends configuration.
    user = auth.authenticate(key=key)
    if user is None:
        url = settings.LOGIN_URL
        if next is not None:
            url = '%s?next=%s' % (url, next)
        return HttpResponseRedirect(url)

    # The key is valid, then now log the user in.
    auth.login(request, user)

    # Update key usage.
    data = Key.objects.get(key=key)
    data.update_usage()

    if data.next is not None:
        next = data.next
    
    return HttpResponseRedirect(next)

