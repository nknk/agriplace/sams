import hashlib
from datetime import datetime

from django.db.models import Q
from django.conf import settings

try:
    from django.apps import apps
    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model


def create_key(pk):
    token = hashlib.sha256()
    token.update(u"%s%s" % (settings.URL_LOGIN_PRIVATE_KEY, pk))
    return token.hexdigest()


def create(user, pk, usage_left=None, expires=None, next=None):
    """
    Create a secret login key for a user.

    A special value ``None`` can be used to disable one or both properties. If
    ``usage_left`` is ``None`` then the key can be used multiple times and
    ``None`` in ``expires`` property means the key will not expire.

    ``next`` paramenter
        A path or URL where the user using this key should be redirected to.
        If this parameter is None, then the default ``settings.LOGIN_URL`` will
        be used.
    """

    key = create_key(pk)

    data = get_model("urllogin", "Key")()
    data.user = user
    data.key = key
    data.usage_left = usage_left
    data.expires = expires
    data.next = next
    data.save()

    return data


def cleanup():
    """
    Remove expired keys.
    Keys that are no longer valid will be moved by calling this method.

    A scheduled calls should be made to this method to make the database clean.
    This can be done by running ``loginurl_cleanup`` command from the Django's
    management script.
    """

    data = get_model("urllogin", "Key").objects.filter(Q(usage_left__lte=0) |
                              Q(expires__lt=datetime.now()))
    if data is not None:
        data.delete()

