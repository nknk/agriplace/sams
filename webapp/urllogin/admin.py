from django.contrib import admin

from core.admin import BaseAdmin
from .models import Key


class KeyAdmin(BaseAdmin):
    list_display = ('user', 'key', 'created', 'expires')
    search_fields = ('user__username', 'key')
    date_hierarchy = 'created'
admin.site.register(Key, KeyAdmin)

