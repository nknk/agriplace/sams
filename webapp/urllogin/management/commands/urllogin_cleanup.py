from django.core.management.base import NoArgsCommand

from urllogin import utils


class Command(NoArgsCommand):
    help = "Clean up expired one time keys."

    def handle_noargs(self, **options):
        utils.cleanup()

