# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('urllogin', '0002_auto_20160816_1445'),
    ]

    operations = [
        migrations.AlterField(
            model_name='key',
            name='user',
            field=models.ForeignKey(related_name='sso_key', to=settings.AUTH_USER_MODEL),
        ),
    ]
