# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('urllogin', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='key',
            options={'permissions': (('view_key', 'Can view key'),)},
        ),
    ]
