from django.conf.urls import patterns, url
from django.views.generic import RedirectView
from django.conf import settings

from .views import login

urlpatterns = patterns('',
    (r'^(?P<key>[0-9A-Za-z]+)/$', login),
    url(r'^$', RedirectView.as_view(url=settings.LOGIN_URL), name='urllogin_redirect'),
)
