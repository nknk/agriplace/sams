from django.db import models
from datetime import datetime


class AssessmentManager(models.Manager):
    def get_organization_assessments(self, organization, assessment_type=None, order_by_column='-created_by'):
        if assessment_type is None:
            return self.filter(organization=organization).order_by(order_by_column)

        qs = self.filter(organization=organization).order_by(order_by_column)

        if isinstance(assessment_type, list):
            return qs.filter(assessment_type__in=assessment_type)
        else:
            return qs.filter(assessment_type=assessment_type)


    def get_open_assessments_of_type(self, organization, assessment_type):
        return self.filter(
            assessment_type=assessment_type,
            organization=organization,
            state__in=['open', 'initial']
        ).distinct()


class AssessmentTypeManager(models.Manager):
    def open_assessment_types(self, membership):
        queryset = self.filter(
            assessments__organization=membership.secondary_organization,
            kind='assessment',
            assessments__state__in=['open', 'initial']
        ).distinct()
        return queryset


class AssessmentDocumentLinkManager(models.Manager):
    def create_attachment_link_copy(self, attachment_link, new_attachment, author, target_assessment, organization):
        new_attachment_link = self.create(
            assessment=target_assessment,
            attachment=new_attachment,
            document_type_attachment=attachment_link.document_type_attachment,
            author=author,
            organization=organization,
            timestamp=datetime.today()
        )
        new_attachment_link.is_done = self.get_is_done(new_attachment_link, attachment_link.is_done)
        new_attachment_link.save()
        return new_attachment_link

    def get_is_done(self, attachment_link, is_done=None):
        """
        :param attachment_link:
        :param is_done: None or Boolean. If None just copy, elif Boolean value set value from param
        :return:
        """
        if attachment_link.attachment.get_attachment_type().lower() in ['agriformattachment', 'formattachment']:
            return False
        elif is_done is None:
            return attachment_link.is_done
        else:
            return is_done

    def get_by_natural_key(self, assessment, document_type_attachment, attachment):
        return self.get(assessment=assessment, document_type_attachment=document_type_attachment, attachment=attachment)


class QuestionnaireManager(models.Manager):
    use_for_related_fields = True

    def published(self, **kwargs):
        return self.filter(unpublished=False, **kwargs)
