# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0027_question_mapping_configuration'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='questionmappings',
            unique_together=set([('configuration', 'target_question'), ('configuration', 'source_question')]),
        ),
    ]
