# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def get_organization(assessmentdocumentlink):
    if assessmentdocumentlink.assessment:
        return assessmentdocumentlink.assessment.organization
    if assessmentdocumentlink.attachment:
        return assessmentdocumentlink.attachment.organization


def migrate_organization(apps, schema_editor):
    i = 0
    AssessmentDocumentLink = apps.get_model('assessments', 'AssessmentDocumentLink')
    for link in AssessmentDocumentLink.objects.select_related('assessment', 'attachment').all().iterator():
        i += 1
        if i % 1000 == 0:
            print("{} records processed.".format(i))
        org = get_organization(link)
        if org:
            link.organization = org
            link.save()
        else:
            print("Organization could not be found for link: {}".format(link))


def revert_organization(apps, schema_editor):
    print("No backward migration is needed.")


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0025_merge'),
        ('assessments', '0019_auto_20170413_1500'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmentdocumentlink',
            name='organization',
            field=models.ForeignKey(related_name='assessment_document_links', blank=True, to='organization.Organization', null=True),
        ),
        migrations.RunPython(migrate_organization, revert_organization)
    ]
