# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0013_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assessmentdocumentlink',
            name='assessment',
            field=models.ForeignKey(related_name='document_links', to='assessments.Assessment', null=True),
        ),
    ]
