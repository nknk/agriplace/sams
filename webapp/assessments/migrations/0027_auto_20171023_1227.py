# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0026_auto_20171003_0946'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assessmenttype',
            name='generic_form',
        ),
        migrations.AlterField(
            model_name='question',
            name='level_object',
            field=models.ForeignKey(related_name='questions', verbose_name=b'Level object', blank=True, to='assessments.QuestionLevel', null=True),
        ),
    ]
