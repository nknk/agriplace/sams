# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0001_initial'),
        ('organization', '0001_initial'),
        ('assessments', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('form_builder', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='document_types_attachments',
            field=models.ManyToManyField(related_name='questions_attachments', to='attachments.DocumentType', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='level_object',
            field=models.ForeignKey(verbose_name=b'Level object', blank=True, to='assessments.QuestionLevel', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='possible_answer_set',
            field=models.ForeignKey(related_name='questions', blank=True, to='assessments.PossibleAnswerSet', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='possibleanswerset',
            name='assessment_type',
            field=models.ForeignKey(related_name='possible_answer_sets', blank=True, to='assessments.AssessmentType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='possibleanswer',
            name='content_package',
            field=models.ForeignKey(related_name='possible_answers', blank=True, to='assessments.ContentPackage', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='possibleanswer',
            name='possible_answer_set',
            field=models.ForeignKey(related_name='possible_answers', blank=True, to='assessments.PossibleAnswerSet', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='possibleanswer',
            name='question',
            field=models.ForeignKey(related_name='possible_answers', blank=True, to='assessments.Question', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='possibleanswer',
            name='triggered_questions',
            field=models.ManyToManyField(related_name='trigger_answers', to='assessments.Question', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='judgement',
            name='assessment',
            field=models.ForeignKey(related_name='judgements', blank=True, to='assessments.Assessment', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='judgement',
            name='author',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='judgement',
            name='question',
            field=models.ForeignKey(related_name='judgements', to='assessments.Question'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='judgement',
            unique_together=set([('assessment', 'question')]),
        ),
        migrations.AddField(
            model_name='category',
            name='parent_category',
            field=models.ForeignKey(related_name='children_categories', to='assessments.Category', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmenttype',
            name='generic_form',
            field=models.ForeignKey(related_name='generic_form', blank=True, to='form_builder.GenericForm', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmenttype',
            name='parent_assessment_type',
            field=models.ForeignKey(related_name='child_assessment_types', blank=True, to='assessments.AssessmentType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentsharepermission',
            name='assessment_share',
            field=models.ForeignKey(related_name='share_permissions', to='assessments.AssessmentShare'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentsharepermission',
            name='author',
            field=models.ForeignKey(related_name='created_share_permissions', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentshare',
            name='assessment',
            field=models.ForeignKey(related_name='shares', to='assessments.Assessment'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentshare',
            name='partner',
            field=models.ForeignKey(related_name='incoming_assessment_shares', to='organization.Organization'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentresult',
            name='assessment',
            field=models.ForeignKey(related_name='results', to='assessments.Assessment'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentdocumentlink',
            name='assessment',
            field=models.ForeignKey(related_name='document_links', to='assessments.Assessment'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentdocumentlink',
            name='attachment',
            field=models.ForeignKey(related_name='assessment_links_attachments', blank=True, to='attachments.Attachment', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentdocumentlink',
            name='author',
            field=models.ForeignKey(related_name='assessment_document_links', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessmentdocumentlink',
            name='document_type_attachment',
            field=models.ForeignKey(related_name='assessment_document_links_attachments', blank=True, to='attachments.DocumentType', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessment',
            name='assessment_type',
            field=models.ForeignKey(related_name='assessments', to='assessments.AssessmentType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessment',
            name='attachment_list',
            field=models.ManyToManyField(to='attachments.Attachment', through='assessments.AssessmentDocumentLink', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessment',
            name='created_by',
            field=models.ForeignKey(related_name='assessments_started', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessment',
            name='last_modified_by',
            field=models.ForeignKey(related_name='assessments_last_modified', blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessment',
            name='organization',
            field=models.ForeignKey(related_name='assessments', blank=True, to='organization.Organization', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='assessment',
            name='products',
            field=models.ManyToManyField(related_name='assessments', to='organization.Product', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='assessment',
            field=models.ForeignKey(related_name='answers', blank=True, to='assessments.Assessment', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='author',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='organization',
            field=models.ForeignKey(related_name='answers', blank=True, to='organization.Organization', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='organization_unit',
            field=models.ForeignKey(related_name='answers', blank=True, to='organization.OrganizationUnit', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='product',
            field=models.ForeignKey(related_name='answers', blank=True, to='organization.Product', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(related_name='answers', to='assessments.Question'),
            preserve_default=True,
        ),
    ]
