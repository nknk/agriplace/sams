# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import assessments.models
import django_extensions.db.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('assessments', '0021_auto_20170825_1211'),
        ('assessments', '0022_dec_migration'),
    ]

    operations = [
        migrations.CreateModel(
            name='DynamicEvidenceImport',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('import_date', models.DateTimeField(null=True, blank=True)),
                ('status', models.CharField(max_length=20)),
                ('file', models.FileField(upload_to=assessments.models.upload_dynamic_evidence_to_s3, max_length=255, verbose_name=b'CSV File')),
                ('import_log', models.TextField(null=True, blank=True)),
                ('configuration', models.ForeignKey(related_name='import_configurations', to='assessments.DynamicEvidenceConfiguration')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'DynamicEvidenceImport',
            },
        ),
    ]
