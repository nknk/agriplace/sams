# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0018_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='assessmenttype',
            name='version',
        ),
        migrations.AddField(
            model_name='assessmenttype',
            name='edition',
            field=models.IntegerField(default=1),
        ),
    ]
