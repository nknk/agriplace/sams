# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('assessments', '0030_assessmentpdfexport'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackgroundEvidenceCopy',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('status', models.CharField(default=b'pending', max_length=99, choices=[(b'pending', 'Pending'), (b'done', 'Done'), (b'error', 'Error')])),
                ('log', models.TextField(null=True, blank=True)),
                ('source_assessment', models.ForeignKey(related_name='copyevidence_source_assessment', to='assessments.Assessment')),
                ('target_assessment', models.OneToOneField(related_name='copyevidence_target_assessment', to='assessments.Assessment')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'backgroundevidencecopy',
                'verbose_name_plural': 'background evidence copy',
                'permissions': (('view_backgroundevidencecopy', 'Can view background evidence copy status'),),
            },
        ),
    ]
