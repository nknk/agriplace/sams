# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0002_auto_20160614_1449'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssessmentTypeSection',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('platform', models.CharField(default=b'agriplace', max_length=50, choices=[(b'agriplace', b'Agriplace'), (b'hzpc', b'Hzpc')])),
                ('sections_list', models.TextField(default=b'overview,evidence,questionnaires,sharing', blank=True)),
                ('assessment_type', models.ForeignKey(related_name='assessment_type_section_list', to='assessments.AssessmentType')),
            ],
            options={
                'ordering': ('uuid',),
                'abstract': False,
                'db_table': 'AssessmentTypeSection',
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='assessmenttypesection',
            unique_together=set([('platform', 'assessment_type')]),
        ),
    ]
