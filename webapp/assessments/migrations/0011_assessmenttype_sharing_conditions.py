# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0010_auto_20161018_1310'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmenttype',
            name='sharing_conditions',
            field=models.TextField(default="", blank=True),
        ),
    ]
