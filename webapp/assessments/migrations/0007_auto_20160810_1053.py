# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0006_auto_20160802_1313'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='assessmenttype',
            options={'ordering': ('code',), 'permissions': (('view_assessmenttype', 'Can view assessment type'),)},
        ),
    ]
