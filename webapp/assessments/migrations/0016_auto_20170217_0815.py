# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import assessments.models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0015_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='assessmentexport',
            old_name='import_outcome',
            new_name='export_detail',
        ),
        migrations.RemoveField(
            model_name='assessmentexport',
            name='import_date',
        ),
        migrations.AlterField(
            model_name='assessmentexport',
            name='file',
            field=models.FileField(upload_to=assessments.models.upload_path, max_length=255, verbose_name=b'json filename'),
        ),
    ]
