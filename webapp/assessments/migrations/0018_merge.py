# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0017_remove_question_compliance_criteria'),
        ('assessments', '0016_auto_20170217_0815'),
    ]

    operations = [
    ]
