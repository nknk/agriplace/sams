# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0015_auto_20170215_0755'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='criteria',
            field=models.TextField(default=b'', verbose_name=b'Criteria', blank=True),
        ),
        migrations.AlterField(
            model_name='questionnaireelement',
            name='guidance',
            field=ckeditor.fields.RichTextField(default=b'', verbose_name=b'Guidance', blank=True),
        ),
    ]
