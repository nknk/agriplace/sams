# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def make_name(DynamicEvidenceConfiguration, assessment_name):
    name = u"%(type)s Configuration" % {"type": assessment_name}
    existing_configs_of_type = DynamicEvidenceConfiguration.objects.filter(name__istartswith=name).count()
    if existing_configs_of_type:
        name = u'{} - {}'.format(name, existing_configs_of_type)
    return name


def migrate_dec(apps, schema_editor):
    i = 0

    AssessmentType = apps.get_model('assessments', 'AssessmentType')
    DynamicEvidenceConfiguration = apps.get_model('assessments', 'DynamicEvidenceConfiguration')
    QuestionEvidenceConfigurations = apps.get_model('assessments', 'QuestionEvidenceConfigurations')
    Question = apps.get_model('assessments', 'Question')
    DocumentType = apps.get_model('attachments', 'DocumentType')

    for assessment_type_x in AssessmentType.objects.all().iterator():
        i += 1
        if i % 10 == 0:
            print("{} assessment type records processed.".format(i))

        dec_name = make_name(DynamicEvidenceConfiguration, assessment_type_x.name)
        dec = DynamicEvidenceConfiguration.objects.create(assessment_type=assessment_type_x, name=dec_name)
        questions = Question.objects.filter(questionnaire__assessment_type=assessment_type_x).distinct()
        for q in questions:
            docs = DocumentType.objects.filter(questions_attachments=q.pk).distinct()
            if docs:
                question_evidence_obj = [
                    QuestionEvidenceConfigurations(
                        configuration=dec,
                        question=q,
                        document_type=doc
                    ) for doc in docs
                ]
                QuestionEvidenceConfigurations.objects.bulk_create(question_evidence_obj)

    print("All assessnents ({}) as processed successfully.".format(i))




def revert_dec(apps, schema_editor):
    print("No backward migration is needed.")


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0021_auto_20170825_1211'),
    ]

    operations = [
        migrations.RunPython(migrate_dec, revert_dec)
    ]
