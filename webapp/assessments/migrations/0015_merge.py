# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0014_assessmentexport'),
        ('assessments', '0014_auto_20170125_0624'),
    ]

    operations = [
    ]
