# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0028_auto_20171023_1057'),
        ('assessments', '0027_auto_20171023_1227'),
    ]

    operations = [
    ]
