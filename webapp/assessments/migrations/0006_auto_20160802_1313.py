# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0005_auto_20160726_0802'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assessmenttypesection',
            name='platform',
            field=models.CharField(default=b'agriplace', max_length=50, choices=[(b'agriplace', b'Agriplace'), (b'hzpc', b'Hzpc'), (b'farm_group', b'FarmGroup')]),
        ),
    ]
