# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0025_dec_setdefaultflag'),
    ]

    operations = [
        migrations.CreateModel(
            name='DynamicLabelConfiguration',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=550)),
            ],
            options={
                'db_table': 'DynamicLabelConfiguration',
                'permissions': (('view_dynamiclabelconfiguration', 'Can view dynamic label configuration permission'),),
            },
        ),
        migrations.CreateModel(
            name='QuestionLabel',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('slug', models.CharField(unique=True, max_length=255)),
                ('name', models.CharField(max_length=550)),
                ('icon_class', models.CharField(max_length=255, blank=True)),
            ],
            options={
                'db_table': 'QuestionLabel',
                'permissions': (('view_questionlabel', 'Can view question labels permission'),),
            },
        ),
        migrations.CreateModel(
            name='QuestionLabelConfigurations',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('configuration', models.ForeignKey(related_name='question_label_configurations', to='assessments.DynamicLabelConfiguration')),
                ('label', models.ForeignKey(related_name='label_configurations', to='assessments.QuestionLabel')),
                ('question', models.ForeignKey(related_name='label_configurations', to='assessments.Question')),
            ],
            options={
                'db_table': 'QuestionLabelConfigurations',
                'permissions': (('view_questionlabelconfigurations', 'Can view question label configuration permission'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='questionlabelconfigurations',
            unique_together=set([('configuration', 'question', 'label')]),
        ),
    ]
