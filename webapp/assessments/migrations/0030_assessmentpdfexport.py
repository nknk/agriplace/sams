# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import assessments.models
import django_extensions.db.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('assessments', '0029_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='AssessmentPDFExport',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('export_date', models.DateTimeField(auto_now_add=True)),
                ('status', models.CharField(max_length=255)),
                ('file', models.FileField(upload_to=assessments.models.upload_path, max_length=255, verbose_name=b'pdf filename')),
                ('export_detail', models.TextField(max_length=1000, null=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'db_table': 'AssessmentPDFExport',
            },
        ),
    ]
