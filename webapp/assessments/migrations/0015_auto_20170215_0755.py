# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields


class Migration(migrations.Migration):
    dependencies = [
        ('assessments', '0014_auto_20170125_0624'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='help_text',
            new_name='criteria',
        ),

        migrations.RenameField(
            model_name='questionnaireelement',
            old_name='extended_text',
            new_name='guidance',
        ),
    ]
