# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0011_questionnaire_requires_reference'),
        ('assessments', '0011_assessmenttype_sharing_conditions'),
    ]

    operations = [
    ]
