# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0023_dynamicevidenceimport'),
    ]

    operations = [
        migrations.AddField(
            model_name='dynamicevidenceconfiguration',
            name='is_default',
            field=models.BooleanField(default=False),
        ),
    ]
