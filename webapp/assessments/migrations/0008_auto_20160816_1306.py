# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0007_auto_20160810_1053'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='assessment',
            options={'ordering': ('created_time',), 'permissions': (('view_assessment', 'Can view assessment'),)},
        ),
        migrations.AlterModelOptions(
            name='assessmentdocumentlink',
            options={'permissions': (('view_assessmentdocumentlink', 'Can view assessment document link'),)},
        ),
        migrations.AlterModelOptions(
            name='assessmentshare',
            options={'ordering': ('uuid',), 'permissions': (('view_assessmentshare', 'Can view assessment share'),)},
        ),
        migrations.AlterModelOptions(
            name='assessmentsharepermission',
            options={'ordering': ('uuid',), 'permissions': (('view_assessmentsharepermission', 'Can view assessment share permission'),)},
        ),
        migrations.AlterModelOptions(
            name='assessmenttypesection',
            options={'ordering': ('uuid',), 'permissions': (('view_assessmenttypesection', 'Can view assessment type section'),)},
        ),
        migrations.AlterModelOptions(
            name='judgement',
            options={'ordering': ('uuid',), 'permissions': (('view_judgement', 'Can view judgement'),)},
        ),
        migrations.AlterModelOptions(
            name='possibleanswer',
            options={'ordering': ['possible_answer_set', 'order_index'], 'permissions': (('view_possibleanswer', 'Can view possible answer'),)},
        ),
        migrations.AlterModelOptions(
            name='possibleanswerset',
            options={'permissions': (('view_possibleanswerset', 'Can view possible answer set'),)},
        ),
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ['questionnaire', 'order_index', 'code'], 'permissions': (('view_question', 'Can view question'),)},
        ),
        migrations.AlterModelOptions(
            name='questionlevel',
            options={'ordering': ('order_index', 'uuid'), 'permissions': (('view_questionlevel', 'Can view question level'),)},
        ),
        migrations.AlterModelOptions(
            name='questionnaire',
            options={'ordering': ('assessment_type', 'order_index'), 'permissions': (('view_questionnaire', 'Can view questionnaire'),)},
        ),
        migrations.AlterModelOptions(
            name='questionnaireheading',
            options={'permissions': (('view_questionnaireheading', 'Can view questionnaire heading'),)},
        ),
        migrations.AlterModelOptions(
            name='questionnaireparagraph',
            options={'permissions': (('view_questionnaireparagraph', 'Can view questionnaire paragraph'),)},
        ),
        migrations.AlterModelOptions(
            name='questionnairestate',
            options={'ordering': ('uuid',), 'permissions': (('view_questionnairestate', 'Can view questionnaire state'),)},
        ),
        migrations.AlterModelOptions(
            name='questiontrigger',
            options={'ordering': ('uuid',), 'permissions': (('view_questiontrigger', 'Can view question trigger'),)},
        ),
        migrations.AlterModelOptions(
            name='questiontriggercondition',
            options={'ordering': ('source_question', 'uuid'), 'permissions': (('view_questiontriggercondition', 'Can view question trigger condition'),)},
        ),
    ]
