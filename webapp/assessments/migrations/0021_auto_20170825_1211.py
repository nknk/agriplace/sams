# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('attachments', '0016_auto_20170117_0958'),
        ('assessments', '0020_assessmentdocumentlink_organization'),
    ]

    operations = [
        migrations.CreateModel(
            name='DynamicEvidenceConfiguration',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=550, blank=True)),
                ('assessment_type', models.ForeignKey(related_name='dynamic_configurations', to='assessments.AssessmentType')),
            ],
            options={
                'db_table': 'DynamicEvidenceConfiguration',
                'permissions': (('view_dynamicevidenceconfiguration', 'Can view dynamic evidence configuration permission'),),
            },
        ),
        migrations.CreateModel(
            name='QuestionEvidenceConfigurations',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('configuration', models.ForeignKey(related_name='question_configurations', to='assessments.DynamicEvidenceConfiguration')),
                ('document_type', models.ForeignKey(related_name='configurations', to='attachments.DocumentType')),
                ('question', models.ForeignKey(related_name='configurations', to='assessments.Question')),
            ],
            options={
                'db_table': 'QuestionEvidenceConfigurations',
                'permissions': (('view_questionevidenceconfigurations', 'Can view question evidence configuration permission'),),
            },
        ),
        migrations.AlterUniqueTogether(
            name='questionevidenceconfigurations',
            unique_together=set([('configuration', 'question', 'document_type')]),
        ),
    ]
