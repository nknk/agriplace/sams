# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0009_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='assessmenttypestandard',
            name='import_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='assessmenttypestandard',
            name='import_outcome',
            field=models.TextField(max_length=1000, null=True),
        ),
    ]
