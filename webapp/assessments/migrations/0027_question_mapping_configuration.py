# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0026_auto_20171003_0946'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuestionMappings',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
            ],
            options={
                'db_table': 'QuestionMappings',
                'verbose_name_plural': 'Question Mappings',
                'permissions': (('view_questionmappings', 'Can view question mappings permission'),),
            },
        ),
        migrations.CreateModel(
            name='QuestionsMappingConfiguration',
            fields=[
                ('uuid', django_extensions.db.fields.ShortUUIDField(primary_key=True, serialize=False, editable=False, max_length=36, blank=True, name=b'uuid')),
                ('remarks', models.TextField(default=b'', verbose_name='Remarks', blank=True)),
                ('is_test', models.BooleanField(default=False)),
                ('modified_time', models.DateTimeField(default=datetime.datetime.today)),
                ('created_time', models.DateTimeField(default=datetime.datetime.today)),
                ('name', models.CharField(max_length=550, blank=True)),
                ('source_assessment_type', models.ForeignKey(related_name='source_questions_mapping', to='assessments.AssessmentType')),
                ('target_assessment_type', models.ForeignKey(related_name='target_questions_mapping', to='assessments.AssessmentType')),
            ],
            options={
                'db_table': 'QuestionsMappingConfiguration',
                'permissions': (('view_questionsmappingconfiguration', 'Can view questions mapping configuration permission'),),
            },
        ),
        migrations.AddField(
            model_name='questionmappings',
            name='configuration',
            field=models.ForeignKey(related_name='question_mapping_configurations', to='assessments.QuestionsMappingConfiguration'),
        ),
        migrations.AddField(
            model_name='questionmappings',
            name='source_question',
            field=models.ForeignKey(related_name='source_question_mapping', to='assessments.Question'),
        ),
        migrations.AddField(
            model_name='questionmappings',
            name='target_question',
            field=models.ForeignKey(related_name='target_question_mapping', to='assessments.Question'),
        ),
        migrations.AlterUniqueTogether(
            name='questionmappings',
            unique_together=set([('configuration', 'source_question')]),
        ),
    ]
