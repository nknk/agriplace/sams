# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0016_auto_20170215_0801'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='compliance_criteria',
        ),
    ]
