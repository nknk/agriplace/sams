# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def migrate_dec(apps, schema_editor):
    i = 0
    DynamicEvidenceConfiguration = apps.get_model('assessments', 'DynamicEvidenceConfiguration')

    for dec in DynamicEvidenceConfiguration.objects.all().iterator():
        i += 1
        if i % 10 == 0:
            print("\n {} DEC updated.".format(i))

        if not DynamicEvidenceConfiguration.objects.filter(
                assessment_type=dec.assessment_type, is_default=True).exists():
            dec.is_default = True
            dec.save()

    print("All DEC ({}) are processed successfully.".format(i))



def revert_dec(apps, schema_editor):
    i = 0
    DynamicEvidenceConfiguration = apps.get_model('assessments', 'DynamicEvidenceConfiguration')

    for dec in DynamicEvidenceConfiguration.objects.all().iterator():
        i += 1
        if i % 10 == 0:
            print("\n {} DEC reverted.".format(i))

        dec.is_default = False
        dec.save()

    print("All DEC revert update ({}) are processed successfully.".format(i))


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0021_auto_20170825_1211'),
        ('assessments', '0022_dec_migration'),
        ('assessments', '0024_dynamicevidenceconfiguration_is_default'),
    ]

    operations = [
        migrations.RunPython(migrate_dec, revert_dec)
    ]
