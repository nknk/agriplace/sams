# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0012_merge'),
        ('assessments', '0011_auto_20161117_0759'),
    ]

    operations = [
    ]
