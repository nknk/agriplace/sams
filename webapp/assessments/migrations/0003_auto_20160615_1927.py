# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0002_auto_20160614_1449'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='info1',
        ),
        migrations.RemoveField(
            model_name='question',
            name='info2',
        ),
        migrations.RemoveField(
            model_name='question',
            name='info3',
        ),
        migrations.RemoveField(
            model_name='question',
            name='info4',
        ),
        migrations.RemoveField(
            model_name='question',
            name='info5',
        ),
        migrations.RemoveField(
            model_name='question',
            name='info6',
        ),
    ]
