# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0003_auto_20160615_1927'),
        ('assessments', '0003_auto_20160630_1436'),
    ]

    operations = [
    ]
