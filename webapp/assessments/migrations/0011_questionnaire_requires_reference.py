# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0010_auto_20161018_1310'),
    ]

    operations = [
        migrations.AddField(
            model_name='questionnaire',
            name='requires_reference',
            field=models.BooleanField(default=False, verbose_name=b'Requires reference?'),
        ),
    ]
