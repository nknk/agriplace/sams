# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def remove_duplicate_questionairestates(apps, schema_editor):
    unique_fields = ['assessment', 'questionnaire']

    QuestionnaireState = apps.get_model('assessments', 'QuestionnaireState')

    duplicates = (QuestionnaireState.objects.values(*unique_fields)
                  .order_by()
                  .annotate(min_created_time=models.Min('created_time'),
                            count_id=models.Count('uuid'))
                  .filter(count_id__gt=1))

    for duplicate in duplicates:
        (QuestionnaireState.objects.filter(**{x: duplicate[x] for x in unique_fields})
         .exclude(created_time=duplicate['min_created_time'])
         .delete()
         )


def backward_migration(apps, schema_editor):
    """No backward migration needed."""
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0010_auto_20161018_1310'),
    ]

    operations = [
        migrations.RunPython(remove_duplicate_questionairestates, backward_migration),
        migrations.AlterUniqueTogether(
            name='questionnairestate',
            unique_together=set([('assessment', 'questionnaire')]),
        ),
    ]
