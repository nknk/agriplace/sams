# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('assessments', '0007_assessmenttypestandard'),
        ('assessments', '0008_auto_20160816_1306'),
    ]

    operations = [
    ]
