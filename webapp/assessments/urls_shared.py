from django.conf.urls import patterns, url
from . import views_shared, views

urlpatterns = patterns(
    'assessments.views_shared',
    url(
        r"^list$",
        views_shared.assessment_list,
        name="shared_assessment_list"
    ),
    url(
        r"^(?P<assessment_pk>.*)/detail$",
        views_shared.assessment_detail,
        name="shared_assessment_detail"
    ),
    url(
        r"^(?P<assessment_pk>.*)/documents$",
        views_shared.assessment_documents,
        name="shared_assessment_documents"
    ),
    url(
        r"^(?P<assessment_pk>.*)/questionnaires$",
        views_shared.assessment_questionnaires,
        name="shared_assessment_questionnaires"
    ),
    url(
        r'^(?P<assessment_pk>.+)/questionnaires/(?P<questionnaire_pk>.+)/edit$',
        views_shared.questionnaire_view,
        name='shared_questionnaire_view'
    ),
    url(
        r'^(?P<assessment_pk>.\w+)/questionnaires/(?P<questionnaire_pk>.+)$',
        views_shared.questionnaire_view,
        name='shared_questionnaire_view'
    ),
    url(
        r"^(?P<assessment_pk>.*)/results$",
        views_shared.assessment_results,
        name="shared_assessment_results",
    ),
    url(
        r"^(?P<assessment_pk>.*)/report$",
        views.assessment_report_view,
        name="my_assessment_report"
    ),
)
