from django import forms
from django.conf import settings

from django.utils.translation import ugettext_lazy as _
from .models import Assessment, AssessmentType, Questionnaire, AssessmentTypeStandard, DynamicEvidenceImport, \
    DynamicLabelConfiguration, QuestionLabelConfigurations, QuestionMappings
from organization.models import ProductType


class AssessmentCreateStep1Form(forms.Form):
    """
    Assessment create: step 1, choose assessment type and name
    """
    assessment_type = forms.ModelChoiceField(queryset=AssessmentType.objects.filter(is_public=True))
    name = forms.CharField()


class AssessmentCreateForm(forms.Form):
    """
    Assessment create: step 2, choose products
    Includes multiple choice product selection using all organization products.
    """
    products = forms.MultipleChoiceField(required=False)

    def __init__(self, organization, *args, **kwargs):
        self.organization = organization
        super(AssessmentCreateForm, self).__init__(*args, **kwargs)
        # Add all organization products as choices for products field
        # If filtering is needed, subclass this form and override field choices
        products = self.organization.products.all()
        self.fields['products'].choices = [(product.pk, product.name) for product in products]


class AssessmentForm(forms.ModelForm):
    class Meta:
        model = Assessment
        fields = [
            'name',
            'assessment_type',
            'organization',
            'products',
            'attachment_list',
            'created_by',
            'state'
        ]


class QuestionnaireForm(forms.ModelForm):

    product_types = forms.ModelMultipleChoiceField(
        queryset=ProductType.objects.all(),
        required=False,
    )

    class Meta:
        model = Questionnaire
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(QuestionnaireForm, self).__init__(*args, **kwargs)

        if self.instance and self.instance.pk:
            self.fields['product_types'].initial = self.instance.product_types.all()

    def save(self, *args, **kwargs):
        instance = super(QuestionnaireForm, self).save(*args, **kwargs)
        if instance.pk:
            for product_type in instance.product_types.all():
                if product_type not in self.cleaned_data['product_types']:
                # we remove product type which have been unselected
                    instance.product_types.remove(product_type)
            for product_type in self.cleaned_data['product_types']:
                if product_type not in instance.product_types.all():
                # we add newly selected product type
                    instance.product_types.add(product_type)
        return instance


class AssessmentTypeStandardForm(forms.ModelForm):

    class Meta:
        model = AssessmentTypeStandard
        fields = [
            'file',
        ]


class DynamicEvidenceImportForm(forms.ModelForm):

    class Meta:
        model = DynamicEvidenceImport
        fields = [
            'file',
            'configuration',
        ]
        help_texts = {'file':'"document_code" & "question_code" {}.'.format(_('are the required fields for import'))}


class QuestionLabelAdminForm(forms.ModelForm):

    class Meta:
        model = DynamicLabelConfiguration
        fields = '__all__'
        help_texts = {
            'icon_class' : 'CSS class name of the icon.',
        }

        
class QuestionLabelConfigurationInlineForm(forms.ModelForm):
    class Meta:
        model = QuestionLabelConfigurations
        fields = [
            'question',
            'label',
        ]


class QuestionMappingsInlineForm(forms.ModelForm):
    class Meta:
        model = QuestionMappings
        fields = [
            'source_question',
            'target_question',
        ]


class AssessmentPdfExportForm(forms.Form):
    LANGUAGES = settings.LANGUAGES
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput, required=True)
    language = forms.ChoiceField(LANGUAGES, initial='en')
