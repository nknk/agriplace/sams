"""
    Management command to copy assessment to one or many secondary_organizations
    input: uuid of source_assessment as first argument and blank separated list of membership uuids
    example: python manage.py assessment_copy -a hvciQEUBj55ESrPqDr7rKm -m UKwMaP7333xS2zWo84NqMU 9zrfotntyr5LYFL3Q8Fbhf
"""
from django.core.management import BaseCommand
from django.db import transaction

from assessments.helpers import assessment_copy
from assessments.models import Assessment
from organization.models import OrganizationMembership


@transaction.atomic
class Command(BaseCommand):
    help = 'Command to copy assessment to one or many secondary_organizations'

    def add_arguments(self, parser):
        parser.add_argument('-a', '--assessment', help='source assessment uuid', type=str, required=True)

        parser.add_argument('-m', '--membership', nargs='+', help='list of target memberships(uuid)',
                            type=str, required=True)

    def handle(self, *args, **options):
        assessment_pk = options['assessment']
        for membership_pk in options['membership']:
            assessment_copy(
                Assessment.objects.get(pk=assessment_pk),
                OrganizationMembership.objects.get(pk=membership_pk)
            )
        self.stdout.write("Script has been run successfully")