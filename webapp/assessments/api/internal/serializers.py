from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
import math
from rest_framework import serializers
from rest_framework_bulk import BulkSerializerMixin
from django.conf import settings

from api.serializers import UuidModelSerializer
from assessments.models import (
    QuestionLabelConfigurations,
    Answer,
    Assessment,
    AssessmentDocumentLink,
    AssessmentShare,
    AssessmentType,
    Questionnaire,
    QuestionnaireElement,
    QuestionnaireElementState,
    QuestionnaireHeading,
    Question,
    QuestionTrigger,
    QuestionTriggerCondition,
    QuestionLevel,
    PossibleAnswer,
    PossibleAnswerSet,
    SharePermission,
    Judgement,
    QuestionnaireState, TODAY, BackgroundEvidenceCopy)
from attachments.api.internal.serializers import AttachmentSerializer, DocumentTypeSerializer, \
    AssessmentDataAttachmentSerializer
from attachments.models import DocumentType
from farm_group.models import AssessmentAgreement
from assessment_workflow.models import AssessmentWorkflow
from organization.models import OrganizationMembership, AGRIPLACE, Product
from organization.utils import HZPC
from sams.utils import get_layout_context
from translations.fields import TranslatableTextField
from translations.helpers import tolerant_ugettext

try:
    from django.apps import apps

    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model


# ---------- BEGIN questionnaire -------------

QUESTIONNAIRE_ELEMENT_FIELDS = [
    'uuid',
    'code',
    'element_type',
    'guidance',
    'is_initially_visible',
    'order_index',
    'parent_element',
    'questionnaire',
    'text',
]


class QuestionLevelSerializer(UuidModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.QuestionnaireFullSerializer
        )
    VERIFIED: ajk 14.09.2016
    """
    title = TranslatableTextField()

    class Meta:
        model = QuestionLevel
        fields = (
            'uuid',
            'code',
            'title',
            'order_index',
            'color',
            'requires_justification',
            'requires_attachment',
            'assessment_type'
        )


class QuestionnaireElementSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.QuestionnaireFullSerializer
        )
    VERIFIED: ajk 14.09.2016
    """
    guidance = TranslatableTextField()
    name = TranslatableTextField()
    text = TranslatableTextField()

    class Meta:
        model = QuestionnaireElement
        fields = QUESTIONNAIRE_ELEMENT_FIELDS


class QuestionnaireHeadingSerializer(QuestionnaireElementSerializer):
    """
    USED AT: (
        assessments.api.serializers.QuestionnaireFullSerializer
        )
    VERIFIED: ajk 14.09.2016
    """

    class Meta(QuestionnaireElementSerializer.Meta):
        model = QuestionnaireHeading

        fields = QUESTIONNAIRE_ELEMENT_FIELDS + [
            'level'
        ]


class QuestionnaireParagraphSerializer(QuestionnaireElementSerializer):
    class Meta(QuestionnaireElementSerializer.Meta):
        model = QuestionnaireHeading


class QuestionSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.QuestionnaireFullSerializer
        )
    VERIFIED: ajk 14.09.2016
    """
    guidance = TranslatableTextField()
    criteria = TranslatableTextField()
    justification_placeholder = TranslatableTextField()
    text = TranslatableTextField()
    document_types_attachments = serializers.SerializerMethodField()

    def get_document_types_attachments(self, obj):
        dec = self.context.get('dec', None)
        if obj and dec:
            docs = DocumentType.objects.filter(
                Q(configurations__configuration=dec,
                  configurations__question=obj.question),
            ).values_list('uuid',flat=True)
            return docs
        else:
            return []

    class Meta(QuestionnaireElementSerializer.Meta):
        fields = QUESTIONNAIRE_ELEMENT_FIELDS + [
            'answer_type',
            'answer_type_data',
            'document_types_attachments',
            'evidence_default_answer',
            'evidence_default_justification',
            'criteria',
            'initially_visible',
            'is_statistics_valuable',
            'is_justification_visible',
            'justification_placeholder',
            'level_object',
            'order_index',
            'possible_answer_set'
        ]
        model = Question


class QuestionnaireElementStateSerializer(UuidModelSerializer):
    def get_identity(self, data):
        # Use assessment uuid + question uuid as composite key
        # to ensure uniqueness.
        try:
            id = u"{0}-{1}".format(
                # we ignore any incoming assessment field
                # because we're already in a (validated) assessment
                self.context['assessment'],
                data.get('question', '')
            )
            return id
        except AttributeError:
            return None

    class Meta:
        model = QuestionnaireElementState
        fields = [
            'assessment',
            'questionnaire_element',
            'is_visible'
        ]


class QuestionnaireFullSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.AssessmentTypeSerializerFull
        )
    VERIFIED: ajk 14.09.2016
    """
    name = TranslatableTextField()
    assessment_type_code = serializers.CharField(source='assessment_type.code')
    question_levels = serializers.SerializerMethodField()
    help_document = serializers.CharField(source='get_help_document_url')
    elements = serializers.SerializerMethodField()

    def get_question_levels(self, obj):
        question_levels = obj.assessment_type.question_levels.all()

        if settings.HZPC_FIFTH_QUESTIONNAIRE_CODE in [questionnaire.code for questionnaire in self.instance]:
            pass
        elif settings.HZPC_FIFTH_QUESTIONNAIRE_CODE_5_1_NL in [questionnaire.code for questionnaire in self.instance]:
            pass
        else:
            question_levels = question_levels.exclude(
                code__in=settings.HZPC_FIFTH_QUESTIONNAIRE_QUESTION_LEVELS
            )

        question_levels_data = QuestionLevelSerializer(question_levels, many=True).data
        return {t['uuid']: t for t in question_levels_data}

    def get_elements(self, obj):
        dec = self.context.get('dec',None)

        questions = obj.questions.all()

        questions_data = QuestionSerializer(questions, many=True, context={'dec':dec}).data
        headings_data = QuestionnaireHeadingSerializer(obj.headings, many=True).data
        paragraphs_data = QuestionnaireParagraphSerializer(obj.paragraphs, many=True).data
        elements = sorted(headings_data + questions_data + paragraphs_data, key=lambda x: x['order_index'])
        return elements

    class Meta:
        model = Questionnaire
        fields = (
            'uuid',
            'code',
            'order_index',
            'name',
            'requires_signature',
            'requires_reference',
            'completeness_check',
            'available_languages',
            'assessment_type_code',
            'question_levels',
            'help_document',
            'elements'
        )


class QuestionnaireSerializer(UuidModelSerializer):
    """
    USED AT: ()
    VERIFIED: ajk 14.09.2016
    """
    name = TranslatableTextField()

    class Meta:
        model = Questionnaire
        fields = (
            'uuid',
            'code',
            'order_index',
            'name',
            'requires_signature',
            'completeness_check',
            'available_languages'
        )


class QuestionnaireStateSerializer(UuidModelSerializer):
    class Meta:
        model = QuestionnaireState
        fields = (
            'is_signed',
            'is_complete'
        )


# ----------- END questionnaire --------------

# ------ BEGIN possible_answers/sets ---------

class PossibleAnswerSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.PossibleAnswerSetSerializer
        )
    VERIFIED: ajk 14.09.2016
    """
    text = TranslatableTextField()
    value = TranslatableTextField()

    class Meta:
        model = PossibleAnswer
        fields = (
            'uuid',
            'order_index',
            'text',
            'value',
            'requires_justification'
        )


class PossibleAnswerSetSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.AssessmentTypeFullSerializer
        )
    VERIFIED: ajk 14.09.2016
    """
    possible_answers = PossibleAnswerSerializer(many=True)

    class Meta:
        model = PossibleAnswerSet
        fields = (
            'uuid',
            'code',
            'name',
            'assessment_type',
            'possible_answers'
        )


# -------- END possible_answers/sets -----------


# --------------- BEGIN triggers ---------------

class QuestionTriggerConditionSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.QuestionTriggerSerializer
        )
    VERIFIED: ajk 14.09.2016
    """

    class Meta:
        model = QuestionTriggerCondition
        fields = (
            'uuid',
            'source_property_name',
            'source_value',
            'trigger',
            'source_question'
        )


class QuestionTriggerSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.serializers.AssessmentTypeFullSerializer
        )
    VERIFIED: ajk 14.09.2016
    """
    conditions = QuestionTriggerConditionSerializer(many=True)
    target_justification = TranslatableTextField()

    class Meta:
        model = QuestionTrigger
        fields = (
            'uuid',
            'target_element',
            'target_question',
            'target_justification',
            'target_value',
            'target_visibility',
            'conditions'
        )


# ---------------- END triggers ----------------


# ---------- BEGIN assessment_type -------------

class AssessmentTypeSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.views.AssessmentTypeList
        assessments.api.views.AssessmentTypeDetail
        )
    VERIFIED: ajk 13.09.2016
    """
    name = TranslatableTextField()
    description = TranslatableTextField()
    sharing_conditions = TranslatableTextField()

    class Meta:
        model = AssessmentType
        fields = (
            'uuid', 'code', 'name', 'description', 'kind', 'has_logo', 'has_product_selection',
            'sharing_conditions', 'edition', 'is_public', 'parent_assessment_type', 'assessment_sections',
            'available_languages', 'logo_small', 'logo_large'
        )


class AssessmentTypeWorkflowCountSerializer(serializers.ModelSerializer):
    work_flow_count = serializers.IntegerField(source='wrokflow_count')
    name = TranslatableTextField()
    description = TranslatableTextField()
    sharing_conditions = TranslatableTextField()

    class Meta:
        model = AssessmentType
        fields = (
            'uuid', 'code', 'name', 'description', 'kind', 'has_logo', 'has_product_selection',
            'sharing_conditions', 'edition', 'is_public', 'parent_assessment_type', 'assessment_sections',
            'available_languages', 'logo_small', 'logo_large', 'work_flow_count'
        )

# Todo: To be removed
class AssessmentTypeFullSerializer(serializers.ModelSerializer):
    """
    USED AT: (
        assessments.api.views.AssessmentTypeFullList
        assessments.api.views.AssessmentTypeFullDetail
        )
    VERIFIED: ajk 13.09.2016
    """
    name = TranslatableTextField()
    description = TranslatableTextField()
    questionnaires = serializers.SerializerMethodField()
    possible_answer_sets = serializers.SerializerMethodField()
    document_types = serializers.SerializerMethodField()
    triggers = serializers.SerializerMethodField()
    sections = serializers.SerializerMethodField()
    sharing_conditions = TranslatableTextField()

    class Meta:
        model = AssessmentType
        fields = (
            'uuid', 'code', 'name', 'description', 'kind', 'has_logo', 'has_product_selection',
            'edition', 'is_public', 'parent_assessment_type', 'available_languages', 'logo_small', 'logo_large',
            'questionnaires', 'possible_answer_sets', 'document_types', 'triggers', 'sections', 'sharing_conditions'
        )

    def get_visible_questionnaires(self):
        workflow = self.context.get('workflow', None)

        if workflow:
            product_types = [product.product_type for product in workflow.assessment.products.select_related(
                'product_type'
            ).all()]
            questionnaires = self.instance.questionnaires.filter(
                Q(unpublished=False) & (Q(product_types__isnull=True) | Q(product_types__in=product_types))
            )
            if workflow.selected_workflow != HZPC:
                questionnaires = questionnaires.exclude(code__in=settings.HZPC_FIFTH_QUESTIONNAIRE_CODES)
        else:
            questionnaires = self.instance.questionnaires.published()

        return questionnaires

    def get_questionnaires(self, obj):
        questionnaires = self.get_visible_questionnaires()
        questionnaires_data = QuestionnaireFullSerializer(questionnaires, many=True).data

        return {t['uuid']: t for t in questionnaires_data}

    def get_possible_answer_sets(self, obj):
        possible_answer_sets = obj.possible_answer_sets.prefetch_related('possible_answers').all()
        possible_answer_sets_data = PossibleAnswerSetSerializer(possible_answer_sets, many=True).data
        return {t['uuid']: t for t in possible_answer_sets_data}

    def get_document_types(self, obj):
        document_types = DocumentType.objects.filter(
            Q(questions_attachments__questionnaire__in=self.get_visible_questionnaires()) &
            Q(questions_attachments__questionnaire__assessment_type__pk=self.instance.pk)
        ).select_related('assessment_type').prefetch_related(
            'help_documents', 'assessment_types_where_optional').distinct()

        document_types_data = DocumentTypeSerializer(document_types, many=True).data

        language = self.context['request'].user.profile.language
        document_types_indexed = dict()
        for dt in document_types_data:
            templates = dt['template']
            if not templates:  # no help document at all
                dt['template'] = ""
            else:
                if language in templates.keys():  # taking first HelpDocument in list
                    dt['template'] = templates[language][0]
                elif "*" in templates.keys():  # no language specific help documents
                    dt['template'] = templates["*"]
                else:
                    dt['template'] = ""
            document_types_indexed[dt['uuid']] = dt

        return document_types_indexed

    def get_triggers(self, obj):
        triggers = obj.get_triggers()
        triggers_data = QuestionTriggerSerializer(triggers, many=True).data
        return {t['uuid']: t for t in triggers_data}

    def get_sections(self, obj):
        user_role = self.context.get('role')
        if not user_role:
            user_role = self.context['request'].membership.role.name
        return obj.get_assessment_type_section_list(get_layout_context(self.context['request']), user_role)



class QuestionLabelConfigurationSerializer(serializers.ModelSerializer):
    label_name = serializers.SerializerMethodField()
    label_icon_class = serializers.SerializerMethodField()
    label_slug = serializers.SerializerMethodField()

    def get_label_name(self, obj):
        return obj.label.name

    def get_label_slug(self, obj):
        return obj.label.slug

    def get_label_icon_class(self, obj):
        return obj.label.icon_class

    class Meta:
        model = QuestionLabelConfigurations
        fields = (
            'uuid',             'label',
            'question',         'label_name',
            'label_icon_class', 'label_slug',
        )


class AssessmentTypeFullSerializer_V2(serializers.ModelSerializer):
    """
    Copy of AssessmentTypeFullSerializer
    Getting document type from QuestionEvidenceConfiguration
    USED AT: (
        assessments.api.views.AssessmentTypeFullList
        assessments.api.views.AssessmentTypeFullDetail
        )
    """
    name = TranslatableTextField()
    description = TranslatableTextField()
    questionnaires = serializers.SerializerMethodField()
    possible_answer_sets = serializers.SerializerMethodField()
    document_types = serializers.SerializerMethodField()
    triggers = serializers.SerializerMethodField()
    sections = serializers.SerializerMethodField()
    sharing_conditions = TranslatableTextField()
    question_label_configuration = serializers.SerializerMethodField()

    class Meta:
        model = AssessmentType
        fields = (
            'uuid', 'code', 'name', 'description', 'kind', 'has_logo', 'has_product_selection',
            'edition', 'is_public', 'parent_assessment_type', 'available_languages', 'logo_small', 'logo_large',
            'questionnaires', 'possible_answer_sets', 'document_types', 'triggers', 'sections', 'sharing_conditions',
            'question_label_configuration'
        )

    def get_question_label_configuration(self, obj):
        workflow = self.context.get('workflow', None)

        if workflow and workflow.label_configuration:

            label_configurations = QuestionLabelConfigurations.objects.filter(
                configuration=workflow.label_configuration,
                question__questionnaire__in=self.get_visible_questionnaires()
            ).select_related('label')

            label_configurations_data = QuestionLabelConfigurationSerializer(label_configurations, many=True).data
            label_configurations_group = dict()
            for x in label_configurations_data:
                label_configurations_group.setdefault(x['question'],[]).append(x)

            return label_configurations_group

        return None

    def get_visible_questionnaires(self):
        workflow = self.context.get('workflow', None)

        if workflow:
            product_types = [product.product_type for product in workflow.assessment.products.all()]
            questionnaires = self.instance.questionnaires.filter(
                Q(unpublished=False) & (Q(product_types__isnull=True) | Q(product_types__in=product_types))
            )

            if workflow.selected_workflow != HZPC:
                questionnaires = questionnaires.exclude(code__in=settings.HZPC_FIFTH_QUESTIONNAIRE_CODES)
        else:
            questionnaires = self.instance.questionnaires.published()

        return questionnaires

    def get_questionnaires(self, obj):
        questionnaires = self.get_visible_questionnaires()
        questionnaires_data = QuestionnaireFullSerializer(questionnaires, many=True,
                                                          context={'dec':self.context.get('dec', None)}).data

        return {t['uuid']: t for t in questionnaires_data}

    def get_possible_answer_sets(self, obj):
        possible_answer_sets = obj.possible_answer_sets.prefetch_related('possible_answers').all()
        possible_answer_sets_data = PossibleAnswerSetSerializer(possible_answer_sets, many=True).data
        return {t['uuid']: t for t in possible_answer_sets_data}

    def get_document_types(self, obj):
        dec = self.context.get('dec', None)

        document_types = DocumentType.objects.filter(
            Q(configurations__configuration=dec,
              configurations__question__questionnaire__in=self.get_visible_questionnaires()),
        ).distinct()

        document_types_data = DocumentTypeSerializer(document_types, many=True).data

        language = self.context['request'].user.profile.language
        document_types_indexed = dict()
        for dt in document_types_data:
            templates = dt['template']
            if not templates:  # no help document at all
                dt['template'] = ""
            else:
                if language in templates.keys():  # taking first HelpDocument in list
                    dt['template'] = templates[language][0]
                elif "*" in templates.keys():  # no language specific help documents
                    dt['template'] = templates["*"]
                else:
                    dt['template'] = ""
            document_types_indexed[dt['uuid']] = dt

        return document_types_indexed

    def get_triggers(self, obj):
        triggers = obj.get_triggers()
        triggers_data = QuestionTriggerSerializer(triggers, many=True).data
        return {t['uuid']: t for t in triggers_data}

    def get_sections(self, obj):
        user_role = self.context.get('role')
        if not user_role:
            user_role = self.context['request'].membership.role.name
        return obj.get_assessment_type_section_list(get_layout_context(self.context['request']), user_role)


class AssessmentTypeWithAssessmentFullSerializer(AssessmentTypeFullSerializer):
    def get_questionnaires(self, obj):
        selected_workflow = self.context.get('selected_workflow', None)
        questionnaires = obj.questionnaires.published()
        if selected_workflow != HZPC:
            questionnaires = questionnaires.exclude(code__in=settings.HZPC_FIFTH_QUESTIONNAIRE_CODES)
        questionnaires_data = QuestionnaireFullSerializer(questionnaires, many=True).data

        return {t['uuid']: t for t in questionnaires_data}


# ----------- END assessment_type --------------

# ---------- BEGIN assessments -------------

class AssessmentShareSerializer(UuidModelSerializer):
    class Meta:
        model = AssessmentShare
        fields = [
            'uuid',
            'assessment',
            'partner',
        ]


class AssessmentSerializer(UuidModelSerializer):
    shares = AssessmentShareSerializer(many=True, required=False)
    state = serializers.CharField(source="assessment_state", required=False)
    state_code = serializers.SerializerMethodField(required=False)

    class Meta:
        fields = [
            'uuid',
            'name',
            'assessment_type',
            'organization',
            'created_by',
            'created_time',
            'state',
            'state_code',
            'products',
            'shares'
        ]
        model = Assessment

    def create(self, validated_data):
        shares_data = validated_data.pop('shares') if 'shares' in validated_data else []
        products_data = validated_data.pop('products') if 'products' in validated_data else []
        assessment = Assessment.objects.create(**validated_data)
        if assessment.assessment_type.kind == 'assessment':
            selected_organization = assessment.organization
            membership = OrganizationMembership.objects.filter(secondary_organization=selected_organization).first()
            assessment_workflow = AssessmentWorkflow.objects.initialize_workflow(
                self.context['request'].user, assessment.assessment_type, TODAY.year, membership, AGRIPLACE
            )
            assessment_workflow.assign_assessment(assessment)

        for share in shares_data:
            AssessmentShare.objects.create(assessment=assessment, **share)

        return assessment

    def update(self, instance, validated_data):
        assessment = instance
        products_data = validated_data.pop('products') if 'products' in validated_data else []

        if 'shares' in validated_data:
            assessment.shares.all().delete()
            shares_data = validated_data.pop('shares')

            for share in shares_data:
                AssessmentShare.objects.create(assessment=assessment, **share)

        for attr, value in validated_data.items():
            setattr(assessment, attr, value)

        assessment.save()
        return assessment

    def get_state_code(self, obj):
        try:
            workflow_context_model = get_model('assessment_workflow.AssessmentWorkflow')
            workflow_context_instance = workflow_context_model.objects.get(assessment=obj)
            if workflow_context_instance.assessment_status \
                    and workflow_context_instance.selected_workflow != 'agriplace':
                return workflow_context_instance.assessment_status
            else:
                return obj.state
        except ObjectDoesNotExist:
            return obj.state


# ---------- END assessments -------------


class AnswerSerializer(UuidModelSerializer):
    class Meta:
        model = Answer


class AnswerNeatSerializer(UuidModelSerializer):
    class Meta:
        model = Answer
        fields = [
            'uuid',
            'justification',
            'question',
            'value',
            'author'
        ]


class SharePermissionSerializer(UuidModelSerializer):
    name = serializers.CharField(source='permission_name')
    data = serializers.CharField(source='permission_data')

    class Meta:
        model = SharePermission
        fields = [
            'name',
            'data'
        ]


class AssessmentWithAttachmentLinksCountSerializer(UuidModelSerializer):
    shares = AssessmentShareSerializer(many=True, required=False)  # allow_add_remove=True
    attachment_link_count = serializers.IntegerField(source='get_attachment_link_count')
    justification_count = serializers.IntegerField(source='get_justification_count')

    class Meta:
        fields = [
            'uuid',
            'name',
            'assessment_type',
            'organization',
            'created_by',
            'state',
            'products',
            'shares',
            'attachment_link_count',
            'justification_count'
        ]
        model = Assessment


class AssessmentSimpleSerializer(UuidModelSerializer):
    created_by = serializers.SerializerMethodField()
    created_by_organization_name = serializers.SerializerMethodField()
    modified_by = serializers.SerializerMethodField()
    shared_time = serializers.SerializerMethodField()
    preferred_month = serializers.SerializerMethodField()
    audit_date = serializers.SerializerMethodField()
    document_review_date = serializers.SerializerMethodField()
    auditor = serializers.SerializerMethodField()
    date_shared = serializers.SerializerMethodField()

    class Meta:
        fields = [
            'uuid',
            'created_time',
            'created_by',
            'created_by_organization_name',
            'modified_time',
            'modified_by',
            'shared_time',
            'preferred_month',
            'audit_date',
            'document_review_date',
            'auditor',
            'date_shared'
        ]
        model = Assessment

    def get_created_by(self, obj):
        if obj and obj.created_by:
            return obj.created_by.get_full_name()
        return None

    def get_created_by_organization_name(self, obj):
        if obj and obj.organization:
            return obj.organization.name
        return None

    def get_modified_by(self, obj):
        if obj and obj.last_modified_by:
            return obj.last_modified_by.get_full_name()
        return None

    def get_shared_time(self, obj):
        return None

    def get_preferred_month(self, obj):
        if obj:
            try:
                workflow_context_instance = AssessmentWorkflow.objects.get(assessment=obj)
                return workflow_context_instance.audit_preferred_month
            except ObjectDoesNotExist:
                return None
        else:
            return None

    def get_audit_date(self, obj):
        if obj:
            try:
                workflow_context_instance = AssessmentWorkflow.objects.get(assessment=obj)
                return workflow_context_instance.audit_date_planned
            except ObjectDoesNotExist:
                return None
        else:
            return None

    def get_date_shared(self, obj):
        if obj:
            try:
                workflow_context_instance = AssessmentWorkflow.objects.get(assessment=obj)
                return workflow_context_instance.date_shared
            except ObjectDoesNotExist:
                return None
        else:
            return None

    def get_auditor(self, obj):
        if obj:
            try:
                workflow_context_instance = AssessmentWorkflow.objects.select_related(
                    'auditor_user').get(assessment=obj)
                if workflow_context_instance.auditor_user:
                    return workflow_context_instance.auditor_user.get_full_name()
                else:
                    return None
            except ObjectDoesNotExist:
                return None
        else:
            return None

    def get_document_review_date(self, obj):
        if obj:
            try:
                workflow_context_instance = AssessmentWorkflow.objects.get(assessment=obj)
                return workflow_context_instance.document_review_date
            except ObjectDoesNotExist:
                return None
        else:
            return None


class AssessmentDocumentLinkSerializer(UuidModelSerializer):
    attachment = AttachmentSerializer()

    class Meta:
        model = AssessmentDocumentLink
        read_only_fields = ('timestamp',)

    def create(self, validated_data):
        instance=AssessmentDocumentLink(**validated_data)
        self.create_or_update(instance)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        self.create_or_update(instance)
        return super(AssessmentDocumentLinkSerializer, self).update(instance, validated_data)

    def create_or_update(self, instance):
        if getattr(self.context['request'], 'membership', None):
            instance.modified_by_user = self.context['request'].user
            instance.modified_by_role = self.context['request'].membership.role


class AssessmentDataDocumentLinkSerializer(UuidModelSerializer):
    attachment = AssessmentDataAttachmentSerializer()

    class Meta:
        model = AssessmentDocumentLink
        read_only_fields = ('timestamp',)

    def create(self, validated_data):
        instance=AssessmentDocumentLink(**validated_data)
        self.create_or_update(instance)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        self.create_or_update(instance)
        return super(AssessmentDataDocumentLinkSerializer, self).update(instance, validated_data)

    def create_or_update(self, instance):
        if getattr(self.context['request'], 'membership', None):
            instance.modified_by_user = self.context['request'].user
            instance.modified_by_role = self.context['request'].membership.role


class AnswerSimpleSerializer(UuidModelSerializer):
    """
    Answer serializer
    """

    class Meta:
        model = Answer
        fields = ('uuid', 'question', 'value', 'justification', 'author')
        read_only_fields = ('uuid',)

    def create(self, validated_data):
        answer = Answer(**validated_data)
        answer.assessment = self.context['assessment']
        if getattr(self.context['request'], 'membership', None):
            answer.modified_by_user = self.context['request'].user
            answer.modified_by_role = self.context['request'].membership.role

        answer.save()
        return answer

    def update(self, instance, validated_data):
        question = Question.objects.get(pk=instance.question)

        answer = instance
        answer.value = validated_data.get('value', instance.value)
        answer.justification = validated_data.get('justification', instance.justification)
        answer.question = question
        if getattr(self.context['request'], 'membership', None):
            answer.modified_by_user = self.context['request'].user
            answer.modified_by_role = self.context['request'].membership.role

        answer.save()
        return answer


class AssessmentCropSerializer(serializers.ModelSerializer):
    area = serializers.FloatField()
    name = serializers.CharField(source="product_type.name")

    class Meta:
        model = Product
        fields = (
            'uuid',
            'product_type',
            'name',
            'area'
        )
        read_only_fields = ('uuid', 'name')


class AssessmentReportSerializer(UuidModelSerializer):
    general_info = serializers.SerializerMethodField()
    evaluation_results = serializers.SerializerMethodField()
    shortcoming_summary = serializers.SerializerMethodField()

    class Meta:
        model = Assessment
        fields = (
            'general_info', 'evaluation_results', 'shortcoming_summary', 'name'
        )

    def get_crops(self, obj):
        return AssessmentCropSerializer(obj.products.all(), many=True).data

    def get_user_name(self, user):
        return u"{} {}".format(user.first_name, user.last_name)

    def get_general_info(self, obj):
        workflow = self.context.get('workflow')
        return {
            "organization": workflow.author_organization.name,
            "farm_group": workflow.selected_workflow,
            "membership_number": workflow.membership.membership_number,
            "ggn_number": workflow.author_organization.ggn_number,
            "date_created": obj.created_time.strftime("%d-%m-%Y") if obj.created_time else "",
            "date_created_by": self.get_user_name(obj.created_by) if obj.created_by else "",
            "date_shared": workflow.date_shared.strftime("%d-%m-%Y") if workflow.date_shared else "",
            "date_shared_by": self.get_user_name(obj.created_by) if obj.created_by and workflow.date_shared else "",
            "crops": self.get_crops(obj)
        }
    
    def get_answers(self, assessment, questions):
        answers = Answer.objects.filter(
            question=questions, assessment=assessment).exclude(Q(value__exact='') | Q(value__isnull=True))
        return answers

    def get_evaluation_results(self, obj):
        major_labels = ["Major", "GG-V5_ES-QL-major-title", "Mayor", "GG-V5.1_ES-QL-major"]
        minor_labels = ["Minor", "GG-V5_ES-QL-minor-title", "Menor", "GG-V5.1_ES-QL-minor"]
        recommended_labels = [
            "Recommended", "GG-V5_ES-QL-recommended-title", "Recom.", "GG-V5.1_ES-QL-recommended",
            "Aanbeveling", "GG-V5.1_NL-QL-recommended"
        ]
        questionnaires = obj.get_visible_questionnaires()

        major_questions = Question.objects.filter(
            questionnaire=questionnaires,
            level_object__title__in=major_labels,
            is_statistics_valuable=True
        )
        minor_questions = Question.objects.filter(
            questionnaire=questionnaires,
            level_object__title__in=minor_labels,
            is_statistics_valuable=True
        )
        recommended_questions = Question.objects.filter(
            questionnaire=questionnaires,
            level_object__title__in=recommended_labels,
            is_statistics_valuable=True
        )
        major_answers = self.get_answers(obj, major_questions)
        minor_answers = self.get_answers(obj, minor_questions)
        recommended_answers = self.get_answers(obj, recommended_questions)

        major_not_applicable_answers = major_answers.filter(value='not_applicable')
        minor_not_applicable_answers = minor_answers.filter(value='not_applicable')
        recommended_not_applicable_answers = recommended_answers.filter(value='not_applicable')

        major_shortcoming_allowed = 0
        minor_shortcoming_allowed = math.floor((minor_questions.count() - minor_not_applicable_answers.count()) * 0.05)
        recommended_shortcoming_allowed = recommended_questions.count()

        major_shortcomings = major_answers.filter(value='no')
        minor_shortcomings = minor_answers.filter(value='no')
        recommended_shortcomings = recommended_answers.filter(value='no')

        major_compliance_percentage = ""
        minor_compliance_percentage = ""
        recommended_compliance_percentage = ""

        if major_questions and major_questions.count() == major_answers.count():
            major_compliance_percentage = round(100 - (float(major_shortcomings.count()) /
                                                       (major_questions.count() - major_not_applicable_answers.count()
                                                        ))*100, 2)

        if minor_questions and minor_questions.count() == minor_answers.count():
            minor_compliance_percentage = round(100 - (float(minor_shortcomings.count()) /
                                                       (minor_questions.count() - minor_not_applicable_answers.count()
                                                        ))*100, 2)

        if recommended_questions and recommended_questions.count() == recommended_answers.count():
            recommended_compliance_percentage = round(100 - (float(recommended_shortcomings.count()) /
                                                             (recommended_questions.count() -
                                                              recommended_not_applicable_answers.count()))*100, 2)
        return {
            "total_control_points": {
                "major": major_questions.count(),
                "minor": minor_questions.count(),
                "recommended": recommended_questions.count()
            },
            "evaluated_control_points": {
                "major": major_answers.count(),
                "minor": minor_answers.count(),
                "recommended": recommended_answers.count()
            },
            "not_applicable": {
                "major": major_not_applicable_answers.count(),
                "minor": minor_not_applicable_answers.count(),
                "recommended": recommended_not_applicable_answers.count()
            },
            "short_comings_allowed": {
                "major": major_shortcoming_allowed,
                "minor": minor_shortcoming_allowed,
                "recommended": recommended_shortcoming_allowed
            },
            "short_comings": {
                "major": major_shortcomings.count(),
                "minor": minor_shortcomings.count(),
                "recommended": recommended_shortcomings.count()
            },
            "compliance_percentage": {
                "major": {
                    "value": major_compliance_percentage,
                    "is_positive": True if major_shortcomings.count() <= major_shortcoming_allowed else False
                },
                "minor": {
                    "value": minor_compliance_percentage,
                    "is_positive": True if minor_shortcomings.count() <= minor_shortcoming_allowed else False
                },
                "recommended": {
                    "value": recommended_compliance_percentage,
                    "is_positive": True if recommended_shortcomings.count() <= recommended_shortcoming_allowed else False
                }
            },
        }

    def get_shortcoming_summary(self, obj):
        summary = []
        questionnaires = obj.get_visible_questionnaires()
        all_question = Question.objects.filter(
            questionnaire=questionnaires,
            is_statistics_valuable=True
        )
        all_answer = self.get_answers(obj, all_question)
        no_answers = all_answer.filter(value='no')
        for answer in no_answers:
            summary.append({
                "code": answer.question.code,
                "level": tolerant_ugettext(answer.question.level_object.title),
                "description": answer.justification
            })

        return summary


class JudgementSerializer(BulkSerializerMixin, UuidModelSerializer):
    class Meta:
        model = Judgement
        fields = ('uuid', 'question', 'value', 'justification', 'evaluation_is_solved', 'evaluation_remarks')
        read_only_fields = ('uuid',)

    def create(self, validated_data):
        judgement = Judgement(**validated_data)
        if getattr(self.context['request'], 'membership', None):
            judgement.modified_by_user = self.context['request'].user
            judgement.modified_by_role = self.context['request'].membership.role

        judgement.save()
        return judgement

    def update(self, instance, validated_data):
        question = Question.objects.get(pk=instance.question)
        judgement = instance
        judgement.value = validated_data.get('value', instance.value)
        judgement.justification = validated_data.get('justification', instance.justification)
        judgement.evaluation_is_solved = validated_data.get('evaluation_is_solved', instance.evaluation_is_solved)
        judgement.question = question
        if getattr(self.context['request'], 'membership', None):
            judgement.modified_by_user = self.context['request'].user
            judgement.modified_by_role = self.context['request'].membership.role

        judgement.save()
        return judgement


class HzpcAssessmentAgreementSerializer(serializers.ModelSerializer):
    form_data = serializers.ReadOnlyField()

    class Meta:
        model = AssessmentAgreement
        fields = ('form_data',)


class UsedInAssessmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assessment
        fields = (
            'uuid',
            'name'
        )


def get_assessment_answers(assessment, format='simple'):
    """
    List of serialized answers for assessment
    """
    serializer = AnswerSerializer if format == 'detailed' else AnswerNeatSerializer if format == 'neat' else AnswerSimpleSerializer
    answers_json = {}
    for answer in assessment.answers.all():
        answers_json[answer.question_id] = serializer(
            answer,
            context={'assessment': assessment}
        ).data
        if format == 'neat':
            answers_json[answer.question_id]['dirtyCounter'] = 1
    return answers_json


class AssessmentDocumentLinkReadonlySerializer(UuidModelSerializer):
    attachment = AttachmentSerializer(read_only=True)
    document_links = serializers.ListField(child=serializers.IntegerField(), write_only=True)

    class Meta:
        model = AssessmentDocumentLink
        read_only_fields = ('assessment', 'attachment', 'document_type_attachment', 'is_done', 'author', 'timestamp')



class BackgroundEvidenceCopySerializer(UuidModelSerializer):
    class Meta:
        model = BackgroundEvidenceCopy
        fields = (
            'source_assessment',
            'target_assessment',
            'status',
        )
