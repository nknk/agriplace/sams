from django.conf.urls import patterns, url

from assessments.api.internal.membership_views import (
    ProductDependentQuestionnaireView,
    AssessmentRetrieveUpdateView, QuestionnaireStateListCreateView,
    AssessmentProductListCreateView, OrganizationAssessmentsByTypeView,
    AssessmentAttachmentRetrieveUpdateDestroyView, AssessmentAttachmentsListCreateView, AnswerRetrieveUpdateDestroyView,
    AnswersListCreateView, JudgementRetrieveUpdateDestroyView, JudgementBulkCreateView, AuditorOverviewRetrieveView,
    ExternalAuditorListView, AssessmentListCreateView, AssessmentProductDestroyView,
    AssessmentStatusesView,
    QuestionnaireConfirmAllJudgementsCreateView, AssessmentReportView, AnswerCreateView, AssessmentAttachmentsListView,
    BackgroundAssessmentEvidenceCopyView, BackgroundAssessmentEvidenceCopyStatusView)

urlpatterns = patterns(
    'assessments.api.internal.views',
    url(
        r'(?P<assessment_pk>.+)/products/questionnaires/$',
        ProductDependentQuestionnaireView.as_view(),
        name='membership-assessment-product-dependent-questionnaires'
    ),
    url(
        r'(?P<source_assessment_pk>.+)/target/(?P<target_assessment_pk>.+)/$',
        BackgroundAssessmentEvidenceCopyView.as_view(),
        name='membership-assessment-evidence-copy-background'
    ),
    url(
        r'(?P<assessment_pk>.+)/questionnaires/(?P<questionnaire_pk>.+)/state/$',
        QuestionnaireStateListCreateView.as_view(),
        name='membership-assessment-questionnaire-state'
    ),
    url(
        r'(?P<assessment_pk>.+)/questionnaires/(?P<questionnaire_pk>.+)/confirm-all-judgements/$',
        QuestionnaireConfirmAllJudgementsCreateView.as_view(),
        name='assessment-questionnaire-state'
    ),
    url(
        r'(?P<assessment_pk>.+)/products/(?P<product_pk>.+)/$',
        AssessmentProductDestroyView.as_view(),
        name='membership-assessment-product'
    ),
    url(
        r'(?P<assessment_pk>.+)/products/$',
        AssessmentProductListCreateView.as_view(),
        name='membership-assessment-products'
    ),
    url(
        r'types/(?P<assessment_type_pk>.+)/$',
        OrganizationAssessmentsByTypeView.as_view(),
        name='membership-assessments-by-type'
    ),
    url(
        r'(?P<assessment_pk>.+)/attachments/(?P<attachment_link_pk>.+)/$',
        AssessmentAttachmentRetrieveUpdateDestroyView.as_view(),
        name='membership-assessment-attachment-link'
    ),
    url(
        r'(?P<assessment_pk>.+)/attachments/$',
        AssessmentAttachmentsListCreateView.as_view(),
        name='membership-assessment-attachment-links'
    ),
    url(
        r'(?P<assessment_pk>.+)/answers/(?P<answer_pk>.+)/$',
        AnswerRetrieveUpdateDestroyView.as_view(),
        name='membership-assessment-answer-details'
    ),
    url(
        r'(?P<assessment_pk>.+)/answer/$',
        AnswerCreateView.as_view(),
        name='membership-assessment-answer'
    ),
    url(
        r'(?P<assessment_pk>.+)/answers/$',
        AnswersListCreateView.as_view(),
        name='membership-assessment-answers'
    ),
    url(
        r'(?P<assessment_pk>.+)/judgements/(?P<pk>.+)/$',
        JudgementRetrieveUpdateDestroyView.as_view(),
        name='membership-assessment-judgement'
    ),
    url(
        r'(?P<assessment_pk>.+)/judgements/$',
        JudgementBulkCreateView.as_view(),
        name='membership-assessment-judgements'
    ),
    url(
        r'(?P<assessment_pk>.+)/auditors/$',
        ExternalAuditorListView.as_view(),
        name='membership-assessment-auditors'
    ),
    url(
        r'(?P<assessment_pk>.+)/auditor_overview/$',
        AuditorOverviewRetrieveView.as_view(),
        name='membership-assessment-auditor-overview'
    ),
    url(
        r'(?P<assessment_pk>.+)/report/$',
        AssessmentReportView.as_view(),
        name='membership-assessment-report'
    ),
    url(
        r'(?P<assessment_pk>.+)/assessment-reuse-status/$',
        BackgroundAssessmentEvidenceCopyStatusView.as_view(),
        name='membership-assessment-background-coping-status'
    ),
    url(
        r'^statuses/$',
        AssessmentStatusesView.as_view(),
        name='membership-assessment-statuses'
    ),
    url(
        r'(?P<pk>.+)/$',
        AssessmentRetrieveUpdateView.as_view(),
        name='membership-assessment'
    ),
    url(
        r'^$',
        AssessmentListCreateView.as_view(),
        name='assessments'
    ),
)
