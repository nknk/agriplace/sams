import datetime

from django.utils import six
from rest_framework import ISO_8601
from rest_framework import serializers
from rest_framework.settings import api_settings


class DateField(serializers.DateField):
    def to_representation(self, value):
        if not value:
            return None

        output_format = getattr(self, 'format', api_settings.DATE_FORMAT)

        if output_format is None or isinstance(value, six.string_types):
            return value

        if isinstance(value, datetime.datetime):
            return value.date()

        if output_format.lower() == ISO_8601:
            return value.isoformat()

        return value.strftime(output_format)
