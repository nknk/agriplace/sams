from django.conf.urls import patterns, url

from assessments.api.internal.views import (
    AssessmentEvidenceCopyView, AssessmentListCreateView, AssessmentRetrieveUpdateView,
    AuditorOverviewRetrieveView, AssessmentProductListCreateView, AssessmentProductDestroyView,
    JudgementRetrieveUpdateDestroyView, JudgementBulkCreateView, AssessmentAttachmentRetrieveUpdateDestroyView,
    AssessmentAttachmentsListCreateView, AssessmentDocumentLinkView, AnswerRetrieveUpdateDestroyView,
    AnswersListCreateView, QuestionnaireStateListCreateView,
    OrganizationAssessmentsByTypeView
)
from organization.api.internal.views import ExternalAuditorListView

urlpatterns = patterns(
    'assessments.api.internal.views',

    url(
        r'(?P<source_assessment_pk>.+)/target/(?P<target_assessment_pk>.+)/$',
        AssessmentEvidenceCopyView.as_view(),
        name='assessment-evidence-copy'
    ),
    url(
        r'(?P<assessment_pk>.+)/products/(?P<product_pk>.+)/$',
        AssessmentProductDestroyView.as_view(),
        name='assessment-product'
    ),
    url(
        r'(?P<assessment_pk>.+)/products/$',
        AssessmentProductListCreateView.as_view(),
        name='assessment-products'
    ),
    url(
        r'(?P<assessment_pk>.+)/judgements/(?P<pk>.+)/$',
        JudgementRetrieveUpdateDestroyView.as_view(),
        name='assessment-judgement'
    ),
    url(
        r'(?P<assessment_pk>.+)/judgements/$',
        JudgementBulkCreateView.as_view(),
        name='assessment-judgements'
    ),
    url(
        r'(?P<assessment_pk>.+)/questionnaires/(?P<questionnaire_pk>.+)/state/$',
        QuestionnaireStateListCreateView.as_view(),
        name='assessment-questionnaire-state'
    ),
    url(
        r'(?P<assessment_pk>.+)/document-types/(?P<document_type_pk>.+)/$',
        AssessmentDocumentLinkView.as_view(),
        name='assessment-document-types'
    ),
    url(
        r'(?P<assessment_pk>.+)/attachments/(?P<attachment_link_pk>.+)/$',
        AssessmentAttachmentRetrieveUpdateDestroyView.as_view(),
        name='assessment-attachment-link'
    ),
    url(
        r'(?P<assessment_pk>.+)/attachments/$',
        AssessmentAttachmentsListCreateView.as_view(),
        name='assessment-attachment-links'
    ),
    url(
        r'(?P<assessment_pk>.+)/answers/(?P<answer_pk>.+)/$',
        AnswerRetrieveUpdateDestroyView.as_view(),
        name='assessment-answer-details'
    ),
    url(
        r'(?P<assessment_pk>.+)/answers/$',
        AnswersListCreateView.as_view(),
        name='assessment-answers'
    ),
    url(
        r'(?P<assessment_pk>.+)/auditors/$',
        ExternalAuditorListView.as_view(),
        name='assessment-auditors'
    ),
    url(
        r'(?P<assessment_pk>.+)/auditor_overview/$',
        AuditorOverviewRetrieveView.as_view(),
        name='assessment-auditor-overview'
    ),
    url(
        r'types/(?P<assessment_type_pk>.+)/$',
        OrganizationAssessmentsByTypeView.as_view(),
        name='assessments-by-type'
    ),
    url(
        r'(?P<pk>.+)/$',
        AssessmentRetrieveUpdateView.as_view(),
        name='assessment'
    ),
    url(
        r'^$',
        AssessmentListCreateView.as_view(),
        name='assessments'
    ),
    url(
        r'^api-root/$',
        'internal_api_assessments_root',
        name='assessments-app-api-list'
    ),
)
