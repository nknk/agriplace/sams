from django.conf import settings

from assessment_workflow.models import ASSESSMENT_WORKFLOW_JUDGEMENTS, AssessmentWorkflow
from translations.helpers import tolerant_ugettext


def is_assessment_report_visible(assessment):
    workflow_instance = AssessmentWorkflow.objects.get(assessment=assessment)
    visible_report_assessments = settings.VISIBLE_ASSESSMENT_REPORT_ASSESSMENT_CODES
    visible_report_assessments = visible_report_assessments.replace(' ', '').split(',')
    if workflow_instance.selected_workflow == "agriplace" \
            and workflow_instance.assessment_type.code in visible_report_assessments:
        return True
    return False


def prepare_auditor_overview_response(w_ctx, judgements):
    result = {"auditor_organization": {
        "name": "",
        "profile_url": "/"
    }, "auditor_user": {
        "name": "",
        "profile_url": "/"
    }, "audit_date_planned": w_ctx.audit_date_planned,
        "audit_date_actual_start": w_ctx.audit_date_actual_start,
        "audit_date_actual_end": w_ctx.audit_date_actual_end,
        "audit_results": {
            "decision": w_ctx.judgement,
            "reaudit": w_ctx.re_audit
        }, "workflow_uuid": w_ctx.uuid, "grower_organization_name": "",
        "possible_decision_list": [[k, tolerant_ugettext(v)] for (k, v) in ASSESSMENT_WORKFLOW_JUDGEMENTS],
        "overview_description": w_ctx.overview_description, "internal_description": w_ctx.internal_description,
        "judgements": judgements
    }
    if w_ctx.author_organization:
        result["grower_organization_name"] = w_ctx.author_organization.name
    if w_ctx.auditor_organization:
        result["auditor_organization"] = {
            "name": w_ctx.auditor_organization.name,
            "profile_url": "/"
        }
    if w_ctx.auditor_user:
        result["auditor_user"] = {
            "name": "%s" % w_ctx.auditor_user.get_full_name(),
            "profile_url": "/"
        }
    if w_ctx.assessment_status == 'action_done':
        result["possible_decision_list"] = result["possible_decision_list"][:-1]
    return result
