from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from api.internal.tests.mixins import AuthAPIMixin
from assessments.api.internal.tests.mixins import TestAnswersMixin
from organization.models import Organization
from organization.tests.factories import OrganizationMembershipFactory


class TestAssessmentAnswers(APITestCase, TestAnswersMixin, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'single_assessment.json'
    ]

    def setUp(self):
        self.test_organization_id = 'DfppWeRpz9GvWZ742yYwfb'
        self.test_assessment_id = 'RrNLcARQ2JafmJBVYNi3E6'
        self.test_question_id1 = 'SWJqrkFwtRLcuFWKELrmwc'
        self.test_question_id2 = '7FczxxeOIkZkUwh5Q6NSpk'

        secondary_organization = Organization.objects.get(pk=self.test_organization_id)

        self.membership = OrganizationMembershipFactory(secondary_organization=secondary_organization)

        self.token = 'Token {0}'.format(self.get_api_token())
        self.assessment_url = reverse('assessment', kwargs={
            'organization_pk': self.test_organization_id,
            'pk': self.test_assessment_id
        })

    def test_save_multiple_answers(self):
        # Ask for target assessment
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        # Save a list of answers
        answers = [
            {
                "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
                "justification": "test",
                "question": self.test_question_id1
            },
            {
                "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
                "justification": "test",
                "question": self.test_question_id2
            },
            {
                "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
                "justification": "test",
                "question": "In valid question id"
            }
        ]

        response = self.client.post(
            reverse('membership-assessment-answers', kwargs={
                'membership_pk': self.membership.pk,
                'assessment_pk': self.test_assessment_id
            }),
            data=answers,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('data' in response.data, msg='The response must a \'data\' field')
        self.assertTrue(isinstance(response.data['data'], dict), msg='The \'data\' field must contain an array')
        self.assertTrue(self.test_question_id1 in response.data['data'], 'First answer must be in the saved list')
        self.assertTrue(self.test_question_id2 in response.data['data'], 'Second answer must be in the saved list')
        self.assertEqual(len(response.data['data'].keys()), 2, 'Only 2 answers must be saved')

        # Ask for the assessment again
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        # This time answers must not be empty
        self.assertNotEqual(response.data['answers'], {}, msg='Answers must not be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')
        self.assertEqual(len(response.data['answers'].keys()), 2, msg='There must be more than 1 answer')
