class TestAnswersMixin(object):
    def assertLooksLikeAnswer(self, something):
        self.assertTrue('uuid' in something, msg='An answer must have a \'uuid\' field')
        self.assertTrue('value' in something, msg='An answer must have a \'value\' field')
        self.assertTrue('question' in something, msg='An answer must have a \'question\' field')
        self.assertTrue('justification' in something, msg='An answer must have a \'justification\' field')
        self.assertTrue('assessment' in something, msg='An answer must have an \'assessment\' field')
        self.assertNotEqual(something['assessment'], None,
                            msg='The \'assessment\' field of an answer must not be empty')
