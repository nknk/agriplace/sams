import json
from unittest import skip
from datetime import datetime

import faker
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
import random
from rest_framework import status
from rest_framework.reverse import reverse

from rest_framework.test import APITestCase

from api.internal.tests.mixins import AuthAPIMixin, APITestSetupMixin
from assessment_workflow.models import ASSESSMENT_STATUSES, AssessmentWorkflow
from assessments.api.internal.tests.mixins import TestAnswersMixin
from assessments.models import Assessment, AssessmentDocumentLink, Question, Judgement, QuestionEvidenceConfigurations, \
    Answer
from assessments.tests.factories import (
    AssessmentFactory, QuestionLevelFactory, QuestionnaireFactory, PossibleAnswerSetFactory, QuestionFactory,
    AnswerFactory, DynamicEvidenceConfigurationFactory, QuestionEvidenceConfigurationsFactory,
    QuestionsMappingConfigurationFactory, QuestionMappingsFactory)
from assessment_types.tests.factories import AssessmentTypeFactory
from attachments.models import TextReferenceAttachment, DocumentType
from attachments.tests.factories import DocumentTypeFactory
from core.constants import AGRIPLACE_GROWER, EXTERNAL_AUDITOR, GROWER, INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from farm_group.tests.factories import AssessmentWorkflowFactory
from organization.models import Organization, AGRIPLACE
from organization.tests.factories import (
    OrganizationFactory, ProductFactory, OrganizationMembershipFactory, OrganizationTypeFactory,
    ProductTypeFactory)

faker = faker.Factory.create()


class TestSetupAssessments(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.size = 3
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)

        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_assessment_list(self):
        """
        Test that a list of assessments is returned against the organization

        """
        self.create_assessments(
            self.organization, self.user, self.assessment_type, self.product, size=self.size
        )

        self.url = reverse('assessments', kwargs={
            'organization_pk': self.organization.pk
        })

        response = self.client.get(self.url, HTTP_AUTHORIZATION=self.token)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
            msg=(
                u"Unexpected status code when loading '{url}': "
                u"expected {expected} but got {actual}"
            ).format(
                url=self.url,
                expected=status.HTTP_200_OK,
                actual=response.status_code
            )
        )

        self.assertNotEqual(response.data, [], msg='An assessment list must be non-empty')
        assessments = response.data

        self.assertIsInstance(assessments, list)
        self.assertEqual(len(assessments), self.size,
                         'A response must have 3 assessments belonging to the organization')

    def test_assessment_detail(self):
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.assessment_type, assessment=self.assessment,
        )
        self.url = reverse('assessment', kwargs={
            'organization_pk': self.organization.pk,
            'pk': self.assessment.pk
        })

        response = self.client.get(self.url, HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('questionnaire_element_states' in response.data,
                        msg='The assessment must have an \'questionnaire element states\' field')
        self.assertTrue('shares' in response.data,
                        msg='The assessment must have an \'shares\' field')

        self.assertEqual(
            response.data['questionnaire_element_states'], {},
            msg='Questionnaire element states must be empty'
        )
        self.assertEqual(response.data['shares'], [], msg='Shares must empty')

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')

        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        self.assertTrue('assessment' in response.data, msg='The assessment detail must have an \'assessment\' field')
        self.assertNotEqual(response.data['assessment'], {}, msg='Assessment must not be empty')


class TestAssessmentAnswers(APITestCase, TestAnswersMixin, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'single_assessment.json'
    ]

    def setUp(self):
        self.test_organization_id = 'DfppWeRpz9GvWZ742yYwfb'
        self.test_assessment_id = 'RrNLcARQ2JafmJBVYNi3E6'
        self.test_question_id1 = 'SWJqrkFwtRLcuFWKELrmwc'
        self.test_question_id2 = '7FczxxeOIkZkUwh5Q6NSpk'

        self.token = 'Token {0}'.format(self.get_api_token())
        self.assessment_url = reverse('assessment', kwargs={
            'organization_pk': self.test_organization_id,
            'pk': self.test_assessment_id
        })

    def test_wrong_format(self):
        """
        Check for an unsupported format
        """
        response = self.client.post(
            reverse('assessment-answers', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id
            }),
            data={'test': 'test'}, format='multipart',
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE, msg=response.content)

    def test_save_single_answer(self):
        # Ask for target assessment
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        # Save an answer
        answers = {
            "answers": {},
            "attachmentLinks": {}
        }

        answers['answers'][self.test_question_id1] = {
            "uuid": "",
            "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
            "justification": "test",
            "question": self.test_question_id1
        }

        response = self.client.post(
            reverse('assessment-answers', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id
            }),
            data=answers,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        # Ask for the assessment again
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must an \'answers\' field')
        self.assertTrue('attachment_links' in response.data, msg='The assessment must an \'attachment_links\' field')

        # This time answers must not be empty
        self.assertNotEqual(response.data['answers'], {}, msg='Answers must not be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

    def test_save_multiple_answers(self):
        # Ask for target assessment
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        # Save a list of answers
        answers = [
            {
                "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
                "justification": "test",
                "question": self.test_question_id1
            },
            {
                "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
                "justification": "test",
                "question": self.test_question_id2
            }
        ]

        response = self.client.post(
            reverse('assessment-answers', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id
            }),
            data=answers,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('data' in response.data, msg='The response must a \'data\' field')
        self.assertTrue(isinstance(response.data['data'], dict), msg='The \'data\' field must contain an array')
        self.assertTrue(self.test_question_id1 in response.data['data'], 'First answer must be in the saved list')
        self.assertTrue(self.test_question_id2 in response.data['data'], 'Second answer must be in the saved list')
        self.assertEqual(len(response.data['data'].keys()), 2, 'Only 2 answers must be saved')

        # Ask for the assessment again
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        # This time answers must not be empty
        self.assertNotEqual(response.data['answers'], {}, msg='Answers must not be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')
        self.assertEqual(len(response.data['answers'].keys()), 2, msg='There must be more than 1 answer')

    def test_detail(self):
        # Ask for target assessment
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        # Save an answer
        answers = {
            "answers": {},
            "attachmentLinks": {}
        }

        answers['answers'][self.test_question_id1] = {
            "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
            "justification": "test",
            "question": self.test_question_id1
        }

        response = self.client.post(
            reverse('assessment-answers', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id
            }),
            data=answers,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('data' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertNotEqual(response.data['data'], {}, msg='Data must not be empty')

        # Retrieve an answer using detail URL
        test_answer_id = response.data['data'][self.test_question_id1]['uuid']

        response = self.client.get(
            reverse('assessment-answer-details', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id,
                'answer_pk': test_answer_id
            }),
            format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertLooksLikeAnswer(response.data)

    def test_delete(self):
        # Ask for target assessment
        self.assessment_url = reverse('assessment', kwargs={
            'organization_pk': self.test_organization_id,
            'pk': self.test_assessment_id
        })
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        # Save an answer
        answers = {
            "answers": {},
            "attachmentLinks": {}
        }

        answers['answers'][self.test_question_id1] = {
            "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
            "justification": "test",
            "question": self.test_question_id1
        }

        response = self.client.post(
            reverse('assessment-answers', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id
            }),
            data=answers,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('data' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertNotEqual(response.data['data'], {}, msg='Data must not be empty')

        test_answer_id = response.data['data'][self.test_question_id1]['uuid']

        response = self.client.delete(
            reverse('assessment-answer-details', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id,
                'answer_pk': test_answer_id
            }),
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, msg=response.content)

        # Ask for the assessment again
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        # The answers must be gone now
        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

    @skip('APP-2077')
    def test_update(self):
        # Ask for target assessment
        response = self.client.get(
            self.assessment_url,
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('answers' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertTrue('attachment_links' in response.data,
                        msg='The assessment must have an \'attachment_links\' field')

        self.assertEqual(response.data['answers'], {}, msg='Answers must be empty')
        self.assertEqual(response.data['attachment_links'], {}, msg='Attachment links must empty')

        # Save an answer
        answers = {
            "answers": {},
            "attachmentLinks": {}
        }

        answers['answers'][self.test_question_id1] = {
            "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
            "justification": "test",
            "question": self.test_question_id1
        }

        response = self.client.post(
            reverse('assessment-answers', kwargs={
                'organization_pk': self.test_organization_id,
                'assessment_pk': self.test_assessment_id
            }),
            data=answers,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertTrue('data' in response.data, msg='The assessment must have an \'answers\' field')
        self.assertNotEqual(response.data['data'], {}, msg='Data must not be empty')

        test_answer_id = response.data['data'][self.test_question_id1]['uuid']
        test_detail_answers_url = reverse('assessment-answer-details', kwargs={
            'organization_pk': self.test_organization_id,
            'assessment_pk': self.test_assessment_id,
            'answer_pk': test_answer_id
        })

        # Save a single answer using PUT
        answer = {
            "value": "^Niet van toepassing, geen opslag GLOBAL G.A.P.-product.",
            "justification": "updated",
            "question": self.test_question_id1,
            "timestamp": "2015-08-06 11:23:00"
        }

        response = self.client.put(
            test_detail_answers_url, data=answer, format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        # Retrieve an answer using detail URL
        response = self.client.get(
            test_detail_answers_url, format='json',
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertLooksLikeAnswer(response.data)


class TestAssessmentAttachmentLinks(APITestCase, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'single_assessment.json',
        'test_document_type_ev188.json'
    ]

    def setUp(self):
        self.organization_id = 'DfppWeRpz9GvWZ742yYwfb'
        self.assessment_id = 'RrNLcARQ2JafmJBVYNi3E6'
        self.token = 'Token {0}'.format(self.get_api_token())

        self.attachments_url = reverse('assessment-attachment-links', kwargs={
            'organization_pk': self.organization_id,
            'assessment_pk': self.assessment_id
        })

    def test_list_attachment_links(self):
        parent_org = OrganizationFactory()

        org = Organization.objects.get(pk=self.organization_id)

        membership = OrganizationMembershipFactory(primary_organization=parent_org, secondary_organization=org)
        assessment = Assessment.objects.get(pk=self.assessment_id)
        workflow = AssessmentWorkflow.objects.get(pk="AmirARQ2JafmJBVYNi3E6")
        workflow.membership = membership
        workflow.assessment = assessment
        workflow.save()
        response = self.client.get(
            self.attachments_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

    def test_add_attachment_links(self):
        parent_org = OrganizationFactory()

        org = Organization.objects.get(pk=self.organization_id)

        membership = OrganizationMembershipFactory(primary_organization=parent_org, secondary_organization=org)
        assessment = Assessment.objects.get(pk=self.assessment_id)
        workflow = AssessmentWorkflow.objects.get(pk="AmirARQ2JafmJBVYNi3E6")
        workflow.membership = membership
        workflow.assessment = assessment
        workflow.save()
        response = self.client.get(
            self.attachments_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        attachment_links = [
            {
                "assessment": self.assessment_id,
                "document_type_attachment": "XW6gVefYRqLfk7xXCCmNG8",
                "attachment": {
                    "title": "123",
                    "attachment_type": "TextReferenceAttachment",
                    "modified_time": "",
                    "created_time": "",
                    "organization": "",
                    "description": "123"
                },
                "is_done": True
            }
        ]

        response = self.client.post(
            self.attachments_url,
            data=attachment_links,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.content)
        self.assertNotEqual(response.data, [], msg='An attachment links list must be non-empty')

    def test_update_attachment_links(self):
        user = User.objects.get(id=15)
        assessment = Assessment.objects.get(pk=self.assessment_id)
        parent_org = OrganizationFactory()
        org = Organization.objects.get(pk=self.organization_id)
        membership = OrganizationMembershipFactory(primary_organization=parent_org, secondary_organization=org)
        workflow = AssessmentWorkflow.objects.get(pk="AmirARQ2JafmJBVYNi3E6")
        workflow.membership = membership
        workflow.assessment = assessment
        workflow.save()

        attachment = TextReferenceAttachment.objects.create(
            title="123",
            description="123"
        )

        attachment_link = AssessmentDocumentLink.objects.create(
            assessment=assessment,
            attachment=attachment,
            author=user,
            is_done=True
        )
        attachment_link_update = {
            "assessment": self.assessment_id,
            "document_type_attachment": "XW6gVefYRqLfk7xXCCmNG8",
            "attachment": {
                "uuid": attachment.uuid,
                "title": "12345",
                "attachment_type": "TextReferenceAttachment",
                "modified_time": "",
                "created_time": "",
                "organization": self.organization_id,
                "description": "12345"
            },
            "is_done": True
        }

        self.attachment_url = reverse('assessment-attachment-link', kwargs={
            'organization_pk': self.organization_id,
            'assessment_pk': self.assessment_id,
            'attachment_link_pk': attachment_link.id
        })
        response = self.client.put(
            self.attachment_url,
            data=attachment_link_update,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        data = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictContainsSubset({
            'title': "12345"
        }, data['attachment'])

        attachment.refresh_from_db()
        self.assertEqual(attachment.title, "12345")

    def test_delete_attachment_link(self):
        user = User.objects.get(id=15)

        org = Organization.objects.get(pk=self.organization_id)
        parent_org = OrganizationFactory()
        membership = OrganizationMembershipFactory(primary_organization=parent_org, secondary_organization=org)
        assessment = Assessment.objects.get(pk=self.assessment_id)
        workflow = AssessmentWorkflow.objects.get(pk="AmirARQ2JafmJBVYNi3E6")
        workflow.membership = membership
        workflow.assessment = assessment
        workflow.save()

        attachment = TextReferenceAttachment.objects.create(
            title="123",
            description="123"
        )

        attachment_link = AssessmentDocumentLink.objects.create(
            assessment=assessment,
            attachment=attachment,
            author=user,
            is_done=True
        )
        self.attachment_url = reverse('assessment-attachment-link', kwargs={
            'organization_pk': self.organization_id,
            'assessment_pk': self.assessment_id,
            'attachment_link_pk': attachment_link.id
        })

        response = self.client.delete(
            self.attachment_url,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        self.assertDictContainsSubset({
            'title': "123",
            'description': "123"
        }, data['attachment'])

    def test_membership_update_attachment_links(self):

        secondary_organization = Organization.objects.get(pk=self.organization_id)

        membership = OrganizationMembershipFactory(secondary_organization=secondary_organization)

        user = User.objects.get(id=15)

        assessment = Assessment.objects.get(pk=self.assessment_id)
        workflow = AssessmentWorkflow.objects.get(pk="AmirARQ2JafmJBVYNi3E6")
        workflow.membership = membership
        workflow.save()

        attachment = TextReferenceAttachment.objects.create(
            title="123",
            description="123"
        )

        attachment_link = AssessmentDocumentLink.objects.create(
            assessment=assessment,
            attachment=attachment,
            author=user,
            is_done=True
        )
        attachment_link_update = {
            "assessment": self.assessment_id,
            "document_type_attachment": "XW6gVefYRqLfk7xXCCmNG8",
            "attachment": {
                "uuid": attachment.uuid,
                "title": "12345",
                "attachment_type": "TextReferenceAttachment",
                "modified_time": "",
                "created_time": "",
                "organization": self.organization_id,
                "description": "12345"
            },
            "is_done": True
        }

        self.attachment_url = reverse('membership-assessment-attachment-link', kwargs={
            'membership_pk': membership.pk,
            'assessment_pk': self.assessment_id,
            'attachment_link_pk': attachment_link.id
        })
        response = self.client.put(
            self.attachment_url,
            data=attachment_link_update,
            format='json',
            HTTP_AUTHORIZATION=self.token
        )

        data = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictContainsSubset({
            'title': "12345"
        }, data['attachment'])

        attachment.refresh_from_db()
        self.assertEqual(attachment.title, "12345")


class TestAssessmentDateShared(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()
    AUDITOR_USERNAME = faker.user_name()

    def setUp(self):
        Group.objects.get_or_create(name=AGRIPLACE_GROWER)
        Group.objects.get_or_create(name=EXTERNAL_AUDITOR)
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.external_auditor = self.create_user(username=self.AUDITOR_USERNAME, password=self.PASSWORD)
        self.primary_organization = OrganizationFactory.create()
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.external_auditor_organization = OrganizationFactory.create(users=(self.external_auditor,))
        organization_type = OrganizationTypeFactory.create()
        self.membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.organization,
            organization_type=organization_type,
            role=Group.objects.get(name=AGRIPLACE_GROWER)
        )
        self.external_auditor_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.external_auditor_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=EXTERNAL_AUDITOR)
        )
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        self.workflow_context = AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.assessment_type, assessment=self.assessment,
            membership=self.membership
        )
        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_date_shared_availbility(self):
        self.url = reverse('membership-assessment', kwargs={
            'membership_pk': self.membership.pk,
            'pk': self.assessment.pk
        })
        response = self.client.get(self.url, format='json', HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        assessment_type = response.data
        date_shared = assessment_type.get('detail').get('date_shared')
        self.assertIsNone(date_shared)
        share_date = {
            "shares":
                [
                    {"partner": self.external_auditor_organization.pk}
                ],
            "state": "closed"
        }
        self.client.patch(path=self.url, data=share_date, format='json', HTTP_AUTHORIZATION=self.token)

        response = self.client.get(self.url, format='json', HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        assessment_type = response.data
        date_shared = assessment_type.get('detail').get('date_shared')
        self.assertIsNotNone(date_shared)
        self.assertEquals(date_shared, datetime.today().date())




class TestSetupAssessmentAuditor(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.auditor = self.create_user(
            username=faker.user_name(), password=faker.word()
        )
        self.auditor_organization = OrganizationFactory.create(users=(self.auditor,))
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        self.workflow_context = AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            auditor_user=self.auditor, auditor_organization=self.auditor_organization,
            assessment_type=self.assessment_type, assessment=self.assessment
        )
        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_auditor_overview(self):
        """
        Test that the assessment auditor overview is returned as json object
        having test organization and test auditor

        """
        self.url = reverse('assessment-auditor-overview', kwargs={
            'organization_pk': self.organization.pk,
            'assessment_pk': self.assessment.pk
        })
        response = self.client.get(self.url, format='json', HTTP_AUTHORIZATION=self.token)

        data = json.loads(response.content)

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertDictContainsSubset({'name': self.auditor_organization.name}, data['auditor_organization'])
        self.assertDictContainsSubset(
            {'name': '{} {}'.format(self.auditor.first_name, self.auditor.last_name)}, data['auditor_user']
        )
        self.assertDictContainsSubset({'decision': '', 'reaudit': False}, data['audit_results'])
        self.assertDictContainsSubset({
            'audit_date_planned': None, 'audit_date_actual_start': None, 'audit_date_actual_end': None
        }, data)


class TestAssessmentJudgements(APITestCase, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'single_assessment.json'
    ]

    def setUp(self):
        self.assessment_id = 'RrNLcARQ2JafmJBVYNi3E6'
        self.judgement_id = '2f74yR2uQAxBC7ekojkkYS'
        self.question_id = '7FczxxeOIkZkUwh5Q6NSpk'
        self.organization_id = 'DfppWeRpz9GvWZ742yYwfb'

        self.judgements_url = reverse('assessment-judgements', kwargs={
            'organization_pk': self.organization_id,
            'assessment_pk': self.assessment_id
        })

        self.judgement_url = reverse('assessment-judgement', kwargs={
            'organization_pk': self.organization_id,
            'assessment_pk': self.assessment_id,
            'pk': self.judgement_id
        })

        self.token = self.get_api_token()

    def test_list_judgements(self):
        """
        Test that the assessment judgements are returned as a list of objects
        """
        response = self.client.get(
            self.judgements_url,
            HTTP_AUTHORIZATION='Token ' + self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

    def test_get_single_judgement(self):
        response = self.client.get(
            self.judgement_url,
            HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('uuid' in response.data, msg='A response to a judgement query must have a uuid')
        self.assertEqual(response.data['uuid'], self.judgement_id, msg=response.content)
        self.assertTrue('question' in response.data, msg='A response to a judgement query must have a question id')
        self.assertEqual(response.data['question'], self.question_id, msg=response.content)
        self.assertNotEqual(response.data, {}, msg='A judgement against uuid must be non-empty')

    def test_update_judgement(self):
        judgement = Judgement.objects.get(pk=self.judgement_id)

        data = {
            'uuid': judgement.pk,
            'question': judgement.question.pk,
            'value': 'updated value',
            'justification': 'updated justification'
        }

        response = self.client.put(
            self.judgement_url,
            data=data,
            format='json',
            HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        judgement = Judgement.objects.get(pk=self.judgement_id)

        self.assertEqual(judgement.value, data['value'], msg='Must have updated judgement value')
        self.assertEqual(judgement.justification, data['justification'], msg='Must have update judgement justification')

    def test_delete_judgement(self):
        judgement = Judgement.objects.get(pk=self.judgement_id)

        self.assertNotEqual(judgement, {}, msg='A judgement should exist before delete')

        response = self.client.delete(
            self.judgement_url,
            HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT, msg=response.content)

        response = self.client.get(
            self.judgement_url,
            HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )
        self.assertEqual(response.data, {'detail': 'Not found.'}, msg='Judgement must be empty')

    def test_add_judgement(self):
        Judgement.objects.get(question=self.question_id).delete()
        question = Question.objects.get(pk=self.question_id)

        judgement = {
            "question": question.pk,
            "value": "new judgement",
            "justification": "this is a new judgement"
        }

        response = self.client.post(
            self.judgements_url,
            data=judgement,
            format='json',
            HTTP_AUTHORIZATION='Token {0}'.format(self.token)
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response.content)

        self.assertTrue('question' in response.data, msg='A response to a judgment query must have \'uuid\' field')
        self.assertEqual(response.data['question'], question.pk, msg='question foreign key must match')
        self.assertNotEqual(response.data, {}, msg='A judgements list must be non-empty')


class TestAssessmentTypes(APITestCase, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'test_doctype.json',
        'test_doctype_attach.json',
        'single_assessment.json'
    ]

    def setUp(self):
        self.test_assessment_type_id = 'uaZHkcWBdNp0tW0S_IpzJ'

        self.token = self.get_api_token()

    @skip('APP-1628')
    def test_assessment_type_detail_full(self):
        response = self.client.get(
            reverse('assessment-type', kwargs={
                'pk': self.test_assessment_type_id
            }),

            HTTP_AUTHORIZATION='Token ' + self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        assessment_type = response.data

        self.assertNotEqual(assessment_type, {}, msg='Assessment type must not be empty')
        self.assertEqual(
            assessment_type['sections'],
            ["overview", "evidence", "questionnaires", "results", "sharing"]
        )

        self.assertTrue('possible_answer_sets' in assessment_type,
                        msg='The assessment must have an \'possible answer sets\' field')
        self.assertNotEqual(assessment_type['possible_answer_sets'], {}, msg='Possible answer sets must not be empty')

        possible_answer_sets = assessment_type['possible_answer_sets']
        self.assertEqual(len(possible_answer_sets.keys()), 2, msg='Possible answer sets must have two instances')

        self.assertTrue('document_types' in assessment_type,
                        msg='The assessment must have an \'document types\' field')
        self.assertNotEqual(assessment_type['document_types'], {}, msg='Document types must not be empty')

        document_types = assessment_type['document_types']
        self.assertEqual(len(document_types.keys()), 2, msg='Document types must have two instances')

        self.assertTrue('triggers' in assessment_type,
                        msg='The assessment must have an \'triggers\' field')
        self.assertEqual(assessment_type['triggers'], {}, msg='Triggers must be empty')


class TestAssessmentTypesQuestionnaires(APITestCase, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'single_assessment.json'
    ]

    def setUp(self):
        self.test_assessment_type_id = 'uaZHkcWBdNp0tW0S_IpzJ'
        self.token = self.get_api_token()

    @skip('APP-1628')
    def test_assessment_type_detail_full_questionnaires(self):
        response = self.client.get(
            reverse('assessment-type', kwargs={
                'pk': self.test_assessment_type_id
            }),

            HTTP_AUTHORIZATION='Token ' + self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        assessment_type = response.data

        self.assertTrue('questionnaires' in assessment_type,
                        msg='The assessment must have an \'questionnaires\' field')

        self.assertNotEqual(assessment_type['questionnaires'], {}, msg='Questionnaires must not be empty')

        questionnaires = assessment_type['questionnaires']

        self.assertEqual(len(questionnaires.keys()), 1, msg='Only one published Questionnaire')


class TestAssessmentQuestionnaireState(APITestCase, AuthAPIMixin):
    fixtures = [
        'test_user.json',
        'test_producer.json',
        'single_assessment.json'
    ]

    def setUp(self):
        self.test_organization_id = 'DfppWeRpz9GvWZ742yYwfb'
        self.test_assessment_id = 'RrNLcARQ2JafmJBVYNi3E6'
        self.test_questionnaire_id = '2ZIZ5SZ4BrdHMxFlxLWn2c'

        self.test_questionnaire_state_detail_url = reverse('assessment-questionnaire-state', kwargs={
            'organization_pk': self.test_organization_id,
            'assessment_pk': self.test_assessment_id,
            'questionnaire_pk': self.test_questionnaire_id
        })

        self.token = 'Token {}'.format(self.get_api_token())

    def test_get_questionnaire_state_detail(self):
        response = self.client.get(
            self.test_questionnaire_state_detail_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('is_complete' in response.data, msg='Must have \'is complete\' field')
        self.assertTrue('is_signed' in response.data, msg='Must have \'is signed\' field')

        self.assertEqual(response.data['is_complete'], False, msg='Is complete must be false')
        self.assertEqual(response.data['is_signed'], False, msg='Is signed must be false')

    def test_update_questionnaire_state_detail(self):
        response = self.client.get(
            self.test_questionnaire_state_detail_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('is_complete' in response.data, msg='Must have \'is complete\' field')
        self.assertTrue('is_signed' in response.data, msg='Must have \'is signed\' field')

        self.assertEqual(response.data['is_complete'], False, msg='Is complete must be false')
        self.assertEqual(response.data['is_signed'], False, msg='Is signed must be false')

        data = {
            "is_complete": True,
            "is_signed": True
        }
        response = self.client.post(
            self.test_questionnaire_state_detail_url,
            data=data,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)

        response = self.client.get(
            self.test_questionnaire_state_detail_url,
            HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        self.assertTrue('is_complete' in response.data, msg='Must have \'is complete\' field')
        self.assertTrue('is_signed' in response.data, msg='Must have \'is signed\' field')

        self.assertEqual(response.data['is_complete'], True, msg='Is complete must be true')
        self.assertEqual(response.data['is_signed'], True, msg='Is signed must be true')


class TestOrganizationAssessmentsAPISetup(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa_v5'

    def setUp(self):
        self.user = self.create_user()
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.assessmentType = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)
        self.token = 'Token {0}'.format(self.get_api_token())

        self.url = reverse('assessments-by-type', kwargs={
            'organization_pk': self.organization.pk, 'assessment_type_pk': self.assessmentType.pk
        })

    def test_organization_assessment_get(self):
        """
        Test that organization assessment of provided type are returned
        """
        self.assessments = self.create_assessments(
            organization=self.organization, user=self.user, assessment_type=self.assessmentType,
            product=self.product, size=3
        )

        response = self.client.get(
            self.url, HTTP_AUTHORIZATION=self.token
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
            msg=(
                u"Unexpected status code when loading '{url}': "
                u"expected {expected} but got {actual}"
            ).format(
                url=self.url,
                expected=status.HTTP_200_OK,
                actual=response.status_code
            )
        )

        assessments = response.data

        self.assertIsInstance(assessments, list)
        self.assertEqual(len(assessments), 3, 'A response must have 3 assessments belonging to the organization')




class TestAssessments(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)

        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_assessment_close(self):
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.assessment_type, assessment=self.assessment,
        )
        self.url = reverse('assessment', kwargs={
            'organization_pk': self.organization.pk,
            'pk': self.assessment.pk
        })

        response = self.client.get(self.url, HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        content = json.loads(response.content)
        self.assertContains(response, 'open')
        data = {"state": "closed"}
        response = self.client.patch(self.url, HTTP_AUTHORIZATION=self.token, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.content)
        content = json.loads(response.content)
        self.assertEqual(content['state'], 'closed')


class TestAssessmentDocumentLinkApi(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.parent_org = OrganizationFactory()
        self.organization = OrganizationFactory.create(users=(self.user,))
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        self.product = ProductFactory.create(organization=self.organization)
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        membership = OrganizationMembershipFactory(primary_organization=self.parent_org, secondary_organization=self.organization)
        AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.assessment_type, assessment=self.assessment, membership=membership
        )

        self.document_type = self.create_document_type(assessment_type=self.assessment_type)
        self.attachment = self.create_text_reference_attachment(organization=self.organization)

        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def test_assessment_document_links(self):
        self.target_assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        membership = OrganizationMembershipFactory(primary_organization=self.parent_org,
                                                   secondary_organization=self.organization)
        AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.assessment_type, assessment=self.target_assessment,membership=membership
        )

        self.document_link = self.create_assessment_document_link(
            user=self.user, assessment=self.assessment, attachment=self.attachment, document_type=self.document_type
        )

        self.post_url = reverse('assessment-document-types', kwargs={
            'organization_pk': self.organization.pk,
            'assessment_pk': self.assessment.pk,
            'document_type_pk': self.document_type.pk,
        })

        post_response = self.client.post(
            self.post_url, HTTP_AUTHORIZATION=self.token,
            data={'document_links': [self.document_link.pk]},
            format='json'
        )

        self.assertEqual(post_response.status_code, status.HTTP_201_CREATED, msg=post_response.content)

        self.assertEquals(
            len(post_response.data), 1, msg="Api response data doesn't contain the document links of length 1"
        )

        self.get_url = reverse('assessment-document-types', kwargs={
            'organization_pk': self.organization.pk,
            'assessment_pk': self.target_assessment.pk,
            'document_type_pk': self.document_type.pk,
        })

        get_response = self.client.get(self.get_url, HTTP_AUTHORIZATION=self.token)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK, msg=get_response.content)


class TestAssessmentStatuses(APITestCase, APITestSetupMixin, AuthAPIMixin):

    GG_CODE = 'globalgap_ifa'
    TN_CODE = 'Tesco_nurture'

    GROWER_USERNAME = faker.user_name()
    INSPECTOR_USERNAME = faker.user_name()
    AUDITOR_USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.create_farm_groups()
        self.grower = self.create_user(username=self.GROWER_USERNAME, password=self.PASSWORD)
        self.internal_inspector = self.create_user(username=self.INSPECTOR_USERNAME, password=self.PASSWORD)
        self.internal_auditor = self.create_user(username=self.AUDITOR_USERNAME, password=self.PASSWORD)

        self.primary_organization = OrganizationFactory.create()
        self.grower_organization = OrganizationFactory.create(
            users=(self.grower,)
        )
        self.internal_inspector_organization = OrganizationFactory.create(
            users=(self.internal_inspector,)
        )
        self.internal_auditor_organization = OrganizationFactory.create(
            users=(self.internal_auditor,)
        )
        organization_type = OrganizationTypeFactory.create()
        self.grower_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.grower_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=GROWER)
        )
        self.internal_inspector_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_inspector_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_INSPECTOR)
        )
        self.internal_auditor_membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.internal_auditor_organization,
            organization_type=organization_type,
            role=Group.objects.get(name=INTERNAL_AUDITOR)
        )
        self.product = ProductFactory.create(organization=self.grower_organization)
        self.GG_assessment_type = AssessmentTypeFactory.create(code=self.GG_CODE)
        self.TN_assessment_type = AssessmentTypeFactory.create(code=self.TN_CODE)
        self.create_workflows()

    def create_workflows(self):
        for i in range(2):
            gg_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.GG_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.GG_assessment_type, assessment=gg_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership, assessment_status='internal_audit_request'
            )

            tn_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.TN_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.TN_assessment_type, assessment=tn_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership, assessment_status='internal_audit_request'
            )

        for i in range(4):
            gg_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.GG_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.GG_assessment_type, assessment=gg_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership, assessment_status='internal_audit_done'
            )

            tn_assessment = self.gg_assessment = self.create_assessment(
                self.grower_organization, self.grower, self.TN_assessment_type, self.product
            )
            AssessmentWorkflowFactory.create(
                author=self.grower, author_organization=self.grower_organization,
                assessment_type=self.TN_assessment_type, assessment=tn_assessment,
                auditor_user=self.internal_inspector, auditor_organization=self.internal_inspector_organization,
                membership=self.grower_membership, assessment_status='internal_audit_done'
            )

    def test_internal_inspector_statuses(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.INSPECTOR_USERNAME, password=self.PASSWORD))
        response = self.client.get(
            reverse('membership-assessment-statuses', kwargs={
                'membership_pk': self.internal_inspector_membership.pk
            }),
            HTTP_AUTHORIZATION=token
        )
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), len(ASSESSMENT_STATUSES))

        for result in response_content:
            if result.get('code') == 'internal_audit_done':
                self.assertEqual(result.get('work_flow_count'), 8)
            elif result.get('code') == 'internal_audit_request':
                self.assertEqual(result.get('work_flow_count'), 4)
            else:
                self.assertEqual(result.get('work_flow_count'), 0)

    def test_internal_auditor_statuses(self):
        token = 'Token {0}'.format(self.get_api_token(username=self.AUDITOR_USERNAME, password=self.PASSWORD))
        response = self.client.get(
            reverse('membership-assessment-statuses', kwargs={
                'membership_pk': self.internal_auditor_membership.pk
            }),
            HTTP_AUTHORIZATION=token
        )
        response_content = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response.data)

        self.assertEqual(len(response_content), len(ASSESSMENT_STATUSES))

        for result in response_content:
            if result.get('code') == 'internal_audit_done':
                self.assertEqual(result.get('work_flow_count'), 8)
            elif result.get('code') == 'internal_audit_request':
                self.assertEqual(result.get('work_flow_count'), 4)
            else:
                self.assertEqual(result.get('work_flow_count'), 0)


class AssessmentReportTest(APITestCase, APITestSetupMixin):
    CODE = 'globalgap_ifa'
    USERNAME = faker.user_name()
    PASSWORD = faker.word()

    def setUp(self):
        self.setup_workflow()
        self.setup_questions()
        self.token = 'Token {0}'.format(self.get_api_token(username=self.USERNAME, password=self.PASSWORD))

    def setup_workflow(self):
        Group.objects.get_or_create(name=AGRIPLACE_GROWER)
        self.user = self.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.primary_organization = OrganizationFactory.create()
        self.organization = OrganizationFactory.create(
            users=(self.user,),
            name="Grower Reporting Organization",
            ggn_number="88664422"
        )
        organization_type = OrganizationTypeFactory.create(name=AGRIPLACE_GROWER)
        self.membership = OrganizationMembershipFactory.create(
            primary_organization=self.primary_organization,
            secondary_organization=self.organization,
            organization_type=organization_type,
            role=Group.objects.get(name=AGRIPLACE_GROWER),
            membership_number="9977553311"
        )
        self.assessment_type = AssessmentTypeFactory.create(code=self.CODE)
        product_type = ProductTypeFactory(name="Reporting product")
        self.product = ProductFactory.create(
            organization=self.organization,
            product_type=product_type,
            area=10
        )
        self.assessment = self.create_assessment(
            self.organization, self.user, self.assessment_type, self.product
        )
        self.workflow_context = AssessmentWorkflowFactory.create(
            author=self.user, author_organization=self.organization,
            assessment_type=self.assessment_type, assessment=self.assessment,
            membership=self.membership, selected_workflow=AGRIPLACE
        )

    def setup_questions(self):
        question_levels = []
        question_levels.append(QuestionLevelFactory.create(assessment_type=self.assessment_type, title="Major"))
        question_levels.append(QuestionLevelFactory.create(assessment_type=self.assessment_type, title="Minor"))
        question_levels.append(QuestionLevelFactory.create(assessment_type=self.assessment_type, title="Recommended"))
        questionnaire1 = QuestionnaireFactory.create(
            code='AF', assessment_type=self.assessment_type, order_index=1, name='All Farm Base Module'
        )
        questionnaire2 = QuestionnaireFactory.create(
            code='FV', assessment_type=self.assessment_type, order_index=3, name='Module Fruit & Vegetables'
        )
        possible_answer_set = PossibleAnswerSetFactory.create(assessment_type=self.assessment_type)
        for question_level in question_levels:
            if question_level.title == "Recommended":
                questionnaire = questionnaire2
                counter = 25
            else:
                questionnaire = questionnaire1
                counter = 100

            for i in range(counter):
                if i <= 97:
                    question = QuestionFactory.create(
                        level_object=question_level, possible_answer_set=possible_answer_set,
                        questionnaire=questionnaire
                    )
                else:
                    question = QuestionFactory.create(
                        level_object=question_level, possible_answer_set=possible_answer_set,
                        questionnaire=questionnaire, code="{}-{}".format(question_level.title, i)
                    )

                if i < 95:
                    AnswerFactory.create(
                        question=question, assessment=self.assessment, value="yes"
                    )
                elif i <= 97:
                    AnswerFactory.create(
                        question=question, assessment=self.assessment, value="not_applicable"
                    )
                else:
                    AnswerFactory.create(
                        question=question, assessment=self.assessment, value="no",
                        justification="{} {} justification".format(question_level.title, i)
                    )

    def test_report(self):
        response = self.client.get(
            reverse('membership-assessment-report', kwargs={
                'membership_pk': self.membership.pk,
                'assessment_pk': self.assessment.pk
            }),
            HTTP_AUTHORIZATION=self.token
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        general_info = response.data.get('general_info')
        evaluation_results = response.data.get('evaluation_results')
        shortcoming_summary = response.data.get('shortcoming_summary')

        self.assertEqual(general_info.get('farm_group'), 'agriplace')
        self.assertEqual(general_info.get('ggn_number'), '88664422')
        self.assertEqual(general_info.get('membership_number'), '9977553311')
        self.assertEqual(general_info.get('organization'), 'Grower Reporting Organization')
        self.assertEqual(len(general_info.get("crops")), 1)
        self.assertEqual(general_info.get("crops")[0].get('area'), 10.0)
        self.assertEqual(general_info.get("crops")[0].get('name'), 'Reporting product')

        self.assertEqual(evaluation_results.get("total_control_points").get("major"), 100)
        self.assertEqual(evaluation_results.get("total_control_points").get("minor"), 100)
        self.assertEqual(evaluation_results.get("total_control_points").get("recommended"), 25)

        self.assertEqual(evaluation_results.get("evaluated_control_points").get("major"), 100)
        self.assertEqual(evaluation_results.get("evaluated_control_points").get("minor"), 100)
        self.assertEqual(evaluation_results.get("evaluated_control_points").get("recommended"), 25)

        self.assertEqual(evaluation_results.get("not_applicable").get("major"), 3)
        self.assertEqual(evaluation_results.get("not_applicable").get("minor"), 3)
        self.assertEqual(evaluation_results.get("not_applicable").get("recommended"), 0)

        self.assertEqual(evaluation_results.get("short_comings_allowed").get("major"), 0)
        self.assertEqual(evaluation_results.get("short_comings_allowed").get("minor"), 4)
        self.assertEqual(evaluation_results.get("short_comings_allowed").get("recommended"), 25)

        self.assertEqual(evaluation_results.get("short_comings").get("major"), 2)
        self.assertEqual(evaluation_results.get("short_comings").get("minor"), 2)
        self.assertEqual(evaluation_results.get("short_comings").get("recommended"), 0)

        self.assertEqual(evaluation_results.get("compliance_percentage").get("major").get("value"), 97.94)
        self.assertEqual(evaluation_results.get("compliance_percentage").get("major").get("is_positive"), False)
        self.assertEqual(evaluation_results.get("compliance_percentage").get("minor").get("value"), 97.94)
        self.assertEqual(evaluation_results.get("compliance_percentage").get("minor").get("is_positive"), True)
        self.assertEqual(evaluation_results.get("compliance_percentage").get("recommended").get("value"), 100)
        self.assertEqual(evaluation_results.get("compliance_percentage").get("recommended").get("is_positive"), True)

        shortcoming_expected = [
            {
                "code": "Minor-98",
                "description": "Minor 98 justification",
                "level": "Minor"
            }, {
                "code": "Minor-99",
                "description": "Minor 99 justification",
                "level": "Minor"
            }, {
                "code": "Major-98",
                "description": "Major 98 justification",
                "level": "Major"
            }, {
                "code": "Major-99",
                "description": "Major 99 justification",
                "level": "Major"
            }
        ]
        for row in shortcoming_expected:
            self.assertIn(row, shortcoming_summary)
