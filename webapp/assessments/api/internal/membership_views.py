from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist
from django.db.models.aggregates import Count
from django.http import Http404
from rest_framework.response import Response
from raven.contrib.django.raven_compat.models import client
from rest_framework.generics import (
    RetrieveUpdateAPIView, ListCreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, CreateAPIView,
    DestroyAPIView,
    get_object_or_404)
from rest_framework.permissions import IsAuthenticated
from rest_framework import status
from rest_framework.views import APIView
from rest_framework_bulk import ListBulkCreateAPIView
from rest_framework import generics, permissions

from assessment_workflow.models import AssessmentWorkflow, ASSESSMENT_STATUSES
from assessments.api.internal.serializers import (
    AssessmentSerializer, AssessmentSimpleSerializer, QuestionnaireStateSerializer,
    AssessmentWithAttachmentLinksCountSerializer, AssessmentDocumentLinkSerializer, AnswerSimpleSerializer,
    AnswerNeatSerializer, AnswerSerializer, get_assessment_answers, JudgementSerializer,
    QuestionnaireSerializer, AssessmentReportSerializer, AssessmentDataDocumentLinkSerializer,
    BackgroundEvidenceCopySerializer)
from assessments.api.internal.utils import prepare_auditor_overview_response, is_assessment_report_visible
from assessments.api.internal.views import get_assessment_data, save_assessment_answers
from assessments.models import (
    Assessment, AssessmentDocumentLink, Questionnaire, QuestionnaireState, AssessmentType, Answer, Judgement,
    QuestionsMappingConfiguration)
from attachments.models import Attachment, NotApplicableAttachment, FileAttachment
from core.constants import INTERNAL_INSPECTOR, INTERNAL_AUDITOR
from form_builder.api.internal.serializers import GenericFormDataSerializer
from form_builder.models import GenericFormData
from organization.api.internal.serializers import ProductSerializer, AssessmentProductLinkSerializer, \
    OrganizationSerializer
from organization.models import Organization, Product, OrganizationMembership
from sams.communication.auditor import assessment_shared
from sams.decorators import block_for_background_copying
from sams.view_mixins import MembershipRequiredPermission
from django.conf import settings

from django.db.models import Q

from assessments.tasks import assessment_evidence_copy_task
from assessments.models import BackgroundEvidenceCopy
from assessments.utils import AssessmentEvidenceCopyUtility


class BaseOrganizationView(APIView):
    """
    Organization base view

    """
    serializer_class = OrganizationSerializer
    permission_classes = (permissions.IsAuthenticated,)


class AssessmentRetrieveUpdateView(RetrieveUpdateAPIView):
    """
    Assessment detail
    :param request:
    :return: assessment for current organization
    """
    serializer_class = AssessmentSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.select_related(
            'organization', 'assessment_type', 'created_by', 'last_modified_by'
        ).prefetch_related('shares', 'products', 'answers').get(pk=self.kwargs.get('pk', ''))

    def get(self, request, *args, **kwargs):
        result = {}
        assessment = self.get_queryset()

        assessment_data = self.get_serializer(assessment).data
        questionnaires_with_completeness_check, available_questionnaires = assessment.get_questionnaires_detail()

        assessment_data.update({"available_questionnaries": available_questionnaires})
        assessment_data.update({"completeness_required_for": questionnaires_with_completeness_check})

        result['assessment'] = assessment_data
        result['detail'] = AssessmentSimpleSerializer(assessment).data
        result['products'] = ProductSerializer(assessment.products, many=True).data
        if assessment.assessment_type.kind == 'assessment':
            result['is_report_visible'] = is_assessment_report_visible(assessment)
        data = get_assessment_data(request, assessment)
        result.update(data)
        if assessment.state == 'closed':
            result['document_type_reuse_attachments'] = []
        else:
            self.get_document_types(request.user, result, assessment)

        result['assessment_reuse_status'] = ""
        bgcopy = BackgroundEvidenceCopy.objects.filter(target_assessment=assessment).values('status').first()
        if bgcopy:
            result['assessment_reuse_status'] = bgcopy['status']

        return Response(result)


    @staticmethod
    def get_attachment_count(current_date, document_link, attachment=None):
        return 1 if not AssessmentRetrieveUpdateView.is_attachment_expired(
            current_date, document_link, attachment=attachment) else 0

    @staticmethod
    def is_attachment_expired(current_date, document_link, attachment=None):
        if not attachment:
            child_object = Attachment.objects.get_subclass(pk=document_link.attachment.pk)
        else:
            child_object = attachment
        # The NotApplicableAttachment attachments should not be counted
        if isinstance(child_object, NotApplicableAttachment):
            return True
        elif isinstance(child_object, FileAttachment):
            expiration_date = child_object.expiration_date
            expiration_date = datetime.strptime(expiration_date, '%Y-%m-%d').date() if isinstance(
                expiration_date, basestring) else expiration_date
            return expiration_date and expiration_date < current_date
        else:
            return False

    def get_document_types(self, user, assessment_data, assessment):
        document_type_reuse_attachments = []
        today = datetime.today().date()
        counts = {}
        document_links = AssessmentDocumentLink.objects.filter(
            assessment__organization=assessment.organization
        ).exclude(assessment=assessment).distinct()

        attachment_uuids = set()
        for document_link in document_links:
            attachment_uuids.add(document_link.attachment_id)
        attachments_query = Attachment.objects.filter(pk__in=attachment_uuids).select_subclasses()

        attachments = {}
        for at in attachments_query:
            attachments[at.pk] = at

        for document_link in document_links:
            document_type_uuid = document_link.document_type_attachment_id
            attachment = attachments[document_link.attachment_id]
            attachment_count = self.get_attachment_count(today, document_link, attachment=attachment)
            if document_type_uuid in counts:
                counts[document_type_uuid] += attachment_count
            else:
                counts[document_type_uuid] = attachment_count

        for document_type_uuid, count in counts.items():
            document_type_reuse_attachments.append({
                "document_type_uuid": document_type_uuid,
                "attachment_count": count
            })
        assessment_data['document_type_reuse_attachments'] = document_type_reuse_attachments

    def get_object(self):
        return self.get_queryset()

    def partial_update(self, request, *args, **kwargs):
        old_partner_set = set()

        if 'pk' in kwargs:
            try:
                old_object = Assessment.objects.get(pk=kwargs.get('pk', ''))
                old_partner_set = set(map(lambda share: share.partner.uuid, old_object.shares.all()))
            except Assessment.DoesNotExist:
                pass

        result = super(generics.RetrieveUpdateAPIView, self).partial_update(request, args, kwargs)

        if old_object.assessment_state == 'open' and self.get_object().state == 'closed':
            workflow = self.get_object().assessmentworkflow_set.first()
            workflow.date_shared = datetime.today().date()
            workflow.save()

        if self.get_object().state == 'closed':
            new_partner_set = set(map(lambda share: share.partner.uuid, self.get_object().shares.all()))
            notify_set = new_partner_set - old_partner_set
            for partner_uuid in notify_set:
                try:
                    partner = Organization.objects.get(pk=partner_uuid)
                    assessment_shared(request, self.get_object(), partner)
                except Organization.DoesNotExist:
                    client.captureException()

        return result



SEE_ATTACHMENT = ('see attachment', 'ver archivo adjunto', 'zie bijlage')


class BackgroundAssessmentEvidenceCopyView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)
    serializer_class = BackgroundEvidenceCopySerializer

    def post(self, request, *args, **kwargs):
        try:
            source_assessment = Assessment.objects.get(pk=self.kwargs.get('source_assessment_pk', ''))
            target_assessment = Assessment.objects.get(pk=self.kwargs.get('target_assessment_pk', ''))

            instance, created = BackgroundEvidenceCopy.objects.get_or_create(
                target_assessment=target_assessment,
                defaults={'source_assessment':source_assessment,'user':request.user}
            )
            if created:
                assessment_evidence_copy_task.delay(self.kwargs.get('source_assessment_pk', ''), kwargs.get('target_assessment_pk', ''), request.user.pk)

            result = self.get_serializer(instance).data
            return Response(result, status.HTTP_201_CREATED)
        except ObjectDoesNotExist:
            return Response([], status.HTTP_404_NOT_FOUND)


class BackgroundAssessmentEvidenceCopyStatusView(generics.GenericAPIView):
    serializer_class = BackgroundEvidenceCopySerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        try:
            background_copying = BackgroundEvidenceCopy.objects.get(target_assessment__uuid=self.kwargs.get('assessment_pk', ''))
            result = self.get_serializer(background_copying).data
            return Response(result, status=status.HTTP_200_OK)
        except ObjectDoesNotExist:
            return Response({}, status.HTTP_200_OK)


class QuestionnaireStateListCreateView(ListCreateAPIView):
    serializer_class = QuestionnaireStateSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        questionnaire = Questionnaire.objects.get(pk=self.kwargs.get('questionnaire_pk', ''))
        state, _ = QuestionnaireState.objects.get_or_create(assessment=assessment, questionnaire=questionnaire)

        return state, assessment

    def get(self, request, *args, **kwargs):
        try:
            questionnaire_state, _ = self.get_queryset()
            return Response(self.get_serializer(questionnaire_state).data, status.HTTP_200_OK)
        except (Assessment.DoesNotExist, Questionnaire.DoesNotExist):
            return Response({}, status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        questionnaire_state, assessment = self.get_queryset()
        questionnaire_state.is_complete = request.data.get('is_complete', questionnaire_state.is_complete)

        if not questionnaire_state.is_signed and request.data.get('is_signed', ''):
            assessment.modified_time = datetime.now()
            assessment.save()

        questionnaire_state.is_signed = request.data.get('is_signed', questionnaire_state.is_signed)
        questionnaire_state.save()

        return Response(self.get_serializer(questionnaire_state).data, status.HTTP_200_OK)


class QuestionnaireConfirmAllJudgementsCreateView(CreateAPIView):
    serializer_class = JudgementSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        assessment = get_object_or_404(Assessment, pk=self.kwargs.get('assessment_pk', ''))
        questionnaire = get_object_or_404(Questionnaire, pk=self.kwargs.get('questionnaire_pk', ''))
        return assessment, questionnaire

    def post(self, request, *args, **kwargs):
        assessment, questionnaire = self.get_queryset()
        judgements = Judgement.objects.filter(assessment=assessment, question__questionnaire=questionnaire)
        empty_judgments = Judgement.objects.filter(value="", assessment=assessment,
                                                   question__questionnaire=questionnaire)
        unanswered_judgment_questions = questionnaire.questions.exclude(judgements=judgements)
        new_judgments = []
        for question in unanswered_judgment_questions:
            judgement = Judgement(
                question=question,
                value="yes",
                justification="",
                assessment=assessment
            )
            judgement.modified_by_user = request.user
            judgement.modified_by_role = request.membership.role
            judgement.save()
            new_judgments.append(judgement)

        for judgement in empty_judgments:
            judgement.value = "yes"
            judgement.modified_by_user = request.user
            judgement.modified_by_role = request.membership.role
            judgement.save()

        if new_judgments or empty_judgments:
            judgements = Judgement.objects.filter(assessment=assessment, question__questionnaire=questionnaire)
        results = self.get_serializer(judgements, many=True).data
        return Response(data=results, status=status.HTTP_200_OK)


class AssessmentProductDestroyView(DestroyAPIView):

    @block_for_background_copying()
    def delete(self, request, *args, **kwargs):
        result = []
        assessment_pk = self.kwargs.get('assessment_pk', '')
        product_pk = self.kwargs.get('product_pk', '')
        try:
            assessment = Assessment.objects.get(pk=assessment_pk)
            if product_pk in assessment.products.all().values_list('uuid', flat=True):
                product_instance = Product.objects.get(uuid=product_pk)
                assessment.products.remove(product_instance)
                result.append(product_pk)
                return Response(result, status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)
        return Response(result)


class AssessmentProductListCreateView(ListCreateAPIView):
    serializer_class = AssessmentProductLinkSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        result = []
        try:
            assessment = self.get_queryset()
            result = [product_pk for product_pk in assessment.products.all().values_list('uuid', flat=True)]
            return Response(result, status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)

    @block_for_background_copying()
    def post(self, request, *args, **kwargs):
        result = []
        data = request.data
        organization = request.membership.secondary_organization
        assessment = self.get_queryset()
        organization_products = organization.products.all().values_list('uuid', flat=True)
        if all(product_pk in organization_products for product_pk in data):
            for product_pk in data:
                product_instance = Product.objects.get(uuid=product_pk)
                assessment.products.add(product_instance)
                result.append(product_pk)
            return Response(result, status.HTTP_201_CREATED)
        else:
            return Response(result, status.HTTP_400_BAD_REQUEST)


class OrganizationAssessmentsByTypeView(ListAPIView):
    serializer_class = AssessmentWithAttachmentLinksCountSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        assessment_type = AssessmentType.objects.get(pk=self.kwargs.get('assessment_type_pk', ''))

        mapping_assessments = []
        mapping_assessments.append(assessment_type)

        # Check Mappings for exists Orginations
        mappings = QuestionsMappingConfiguration.objects.filter(
            Q(source_assessment_type=assessment_type) | Q(target_assessment_type=assessment_type)
        ).select_related('source_assessment_type', 'target_assessment_type')

        for x in mappings:
            if assessment_type == x.source_assessment_type:
                mapping_assessments.append(x.target_assessment_type)
            else:
                mapping_assessments.append(x.source_assessment_type)

        return Assessment.objects.get_organization_assessments(
                organization=self.request.membership.secondary_organization, assessment_type=mapping_assessments,
            )

    def get(self, request, *args, **kwargs):
        assessments = [self.get_serializer(assessment).data for assessment in self.get_queryset()]

        return Response(assessments)


class AssessmentAttachmentRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    serializer_class = AssessmentDocumentLinkSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    @block_for_background_copying()
    def put(self, request, *args, **kwargs):
        attachment_link_pk = self.kwargs.get('attachment_link_pk', '')
        organization = self.request.membership.secondary_organization
        attachment_link = AssessmentDocumentLink.objects.get(pk=attachment_link_pk)
        data = request.data

        if isinstance(data, dict):  # because validation goes later and we need to be sure that this is dict
            data['attachment']['organization'] = organization.uuid  # rights for assessment already checked
            data['author'] = request.user.pk

        serializer = self.get_serializer(
            instance=attachment_link,
            data=data,
            many=False,
            partial=True
        )
        if serializer.is_valid():
            updated_attachment_link = serializer.save()

            attachment_data = data['attachment']
            attachment = updated_attachment_link.attachment
            if attachment:
                attachment = Attachment.objects.get_subclass(pk=attachment.pk)
                attachment.agriform_reference = attachment_data.get('agriform_reference')
                if attachment_data.get('title', None):
                    updated_attachment_link.attachment.title = attachment_data['title']
                    attachment.title = attachment_data['title']
                if attachment_data.get('description', None):
                    updated_attachment_link.attachment.description = attachment_data['description']
                    attachment.description = attachment_data['description']
                attachment.save()
            if attachment_data.get('generic_form_data', None):
                try:
                    generic_form_data_serializer = GenericFormDataSerializer(
                        instance=GenericFormData.objects.get(uuid=attachment_data['generic_form_data']['uuid']),
                        data=attachment_data['generic_form_data'],
                        many=False,
                        partial=True
                    )
                    if generic_form_data_serializer.is_valid():
                        updated_generic_form_data = generic_form_data_serializer.save()
                        updated_attachment_link.attachment.generic_form_data = updated_generic_form_data
                except GenericFormData.DoesNotExist:
                    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
            result = self.get_serializer(updated_attachment_link).data
            return Response(result, status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    @block_for_background_copying()
    def delete(self, request, *args, **kwargs):
        result = []
        try:
            attachment_link_pk = self.kwargs.get('attachment_link_pk', '')
            assessment = self.get_queryset()
            if assessment:
                assessment_document_link = AssessmentDocumentLink.objects.get(pk=attachment_link_pk)
                result = self.get_serializer(assessment_document_link).data
                assessment_document_link.modified_by_user = request.user
                assessment_document_link.modified_by_role = request.membership.role
                assessment_document_link.delete()
                return Response(result, status.HTTP_200_OK)
            else:
                return Response(result, status.HTTP_403_FORBIDDEN)
        except Assessment.DoesNotExist, AssessmentDocumentLink.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)


class AssessmentAttachmentsListCreateView(ListCreateAPIView):
    serializer_class = AssessmentDocumentLinkSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        try:
            assessment = self.get_queryset()
            assessment_document_links = AssessmentDocumentLink.objects.filter(
                assessment=assessment
            )
            result = self.get_serializer(assessment_document_links, many=True).data
            return Response(result, status=status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response([], status.HTTP_403_FORBIDDEN)

    @block_for_background_copying()
    def post(self, request, *args, **kwargs):

        organization = self.request.membership.secondary_organization
        data = request.data

        if isinstance(data, list):
            for idx, item in enumerate(data):  # TODO: fined a better way of manipulation using DRF 3
                item['attachment']['organization'] = organization.uuid
                item['author'] = request.user.pk
                data[idx] = item
        serializer = self.get_serializer(
            data=data,
            many=True
        )
        if serializer.is_valid():
            assessment_document_links = serializer.save()
            return Response(self.get_serializer(assessment_document_links, many=True).data, status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class AssessmentAttachmentsListView(ListAPIView):
    serializer_class = AssessmentDataDocumentLinkSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        try:
            assessment = self.get_queryset()

            attachments = AssessmentDocumentLink.objects.filter(
                assessment=assessment
            ).select_related('attachment')

            attachments_data_list = AssessmentDataDocumentLinkSerializer(attachments, many=True).data
            attachment_links = {}
            attachment_uuids = set()
            file_attachment_uuids = set()
            for attachment_data in attachments_data_list:
                if attachment_data['attachment']['attachment_type'] == 'FileAttachment':
                    file_attachment_uuids.add(attachment_data['attachment']['uuid'])
                attachment_uuids.add(attachment_data['attachment']['uuid'])

            used_in_assessments = Attachment.get_used_in_assessments(attachment_uuids=attachment_uuids)
            used_in_document_types = Attachment.get_used_in_document_types(attachment_uuids=file_attachment_uuids)

            # group attachments by document type
            for attachment_data in attachments_data_list:
                # attachments = {a['document_type']: a for a in attachments_data_list}
                attachment_data['attachment']['used_in_assessments'] = used_in_assessments[
                    attachment_data['attachment']['uuid']]
                if attachment_data['attachment']['attachment_type'] == 'FileAttachment':
                    attachment_data['attachment']['used_in'] = used_in_document_types[attachment_data['attachment']['uuid']]

                attachment_links.setdefault(attachment_data['document_type_attachment'], []).append(attachment_data)
            return Response(attachment_links, status=status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response([], status.HTTP_403_FORBIDDEN)


class AnswerRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_object(self):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk'))
        answer = self.queryset.get(pk=self.kwargs.get('answer_pk'))

        if answer.assessment.uuid != assessment.uuid:
            raise Http404('No %s matches the given query.' % Answer.model._meta.object_name)

        self.check_object_permissions(self.request, answer)
        return answer


    @block_for_background_copying()
    def delete(self, request, *args, **kwargs):
        try:
            answer = self.queryset.get(pk=self.kwargs.get('answer_pk'))
            answer.modified_by_user = request.user
            answer.modified_by_role = request.membership.role
            answer.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Answer.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


    @block_for_background_copying()
    def update(self, request, *args, **kwargs):
        try:
            partial = kwargs.pop('partial', False)
            answer = self.get_object()

            data = request.data
            data["author"] = request.user.pk

            serializer = AnswerSimpleSerializer(answer, data=data,
                                                partial=partial, context=self.get_serializer_context())
            if not serializer.is_valid():
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            if answer is None:
                answer = serializer.save(force_insert=True)
                self.perform_update(answer)
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            answer = serializer.save(force_update=False)
            return Response(AnswerNeatSerializer(answer).data, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response({"errors": ex.message}, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get_serializer_context(self):
        assessment_pk = getattr(self.kwargs, 'assessment_pk', '')

        return {
            'assessment': Assessment.objects.get(pk=assessment_pk) if assessment_pk else None,
            'request': self.request
        }


class AnswerCreateView(CreateAPIView):
    serializer_class = AnswerSimpleSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    @block_for_background_copying()
    def post(self, request, *args, **kwargs):
        if not request.META.get('CONTENT_TYPE', 'none/none').startswith('application/json'):
            return Response({}, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        assessment = self.get_queryset()
        serializer = self.serializer_class(
            data=request.data,
            context={
                'request': request,
                'assessment': assessment,
            }
        )
        if serializer.is_valid():
            try:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            except Exception as ex:
                return Response({"error": ex.message}, status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class AnswersListCreateView(ListCreateAPIView):
    serializer_class = AnswerSimpleSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        assessment = self.get_queryset()
        answers_json = {}

        for answer in Answer.objects.filter(assessment=assessment).all():
            answers_json[answer.question_id] = self.get_serializer(
                answer,
                context={'assessment': assessment}
            ).data

        return Response(answers_json, status.HTTP_200_OK)

    @block_for_background_copying()
    def post(self, request, *args, **kwargs):
        if not request.META.get('CONTENT_TYPE', 'none/none').startswith('application/json'):
            return Response({}, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

        try:
            data = request.data if type(request.data) is list else request.data.get('answers', '')
            assessment = self.get_queryset()
            failed_answers = save_assessment_answers(
                request,
                assessment,
                data
            )
            result = {'success': False if failed_answers else True}
            response_status = status.HTTP_200_OK
            received_answers = data if type(data) is list else data.values()
            retrieved_answers = get_assessment_answers(assessment, format='neat')
            saved_answers = {}

            for answer in received_answers:
                if answer['question'] in retrieved_answers:
                    saved_answers[answer['question']] = retrieved_answers[answer['question']]

            result['data'] = saved_answers

            return Response(result, response_status)
        except Exception as ex:
            return Response({"error": ex.message}, status.HTTP_500_INTERNAL_SERVER_ERROR)


class JudgementBaseView(APIView):
    serializer_class = JudgementSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        return Judgement.objects.filter(assessment=assessment)

    def update_serializer(self, serializer):
        serializer.save(
            author=self.request.user,
            assessment=Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        )


class JudgementRetrieveUpdateDestroyView(JudgementBaseView, RetrieveUpdateDestroyAPIView):
    def perform_update(self, serializer):
        self.update_serializer(serializer=serializer)

    def delete(self, request, *args, **kwargs):
        try:
            judgment = get_object_or_404(Judgement, pk=self.kwargs.get('pk'))
            judgment.modified_by_user = request.user
            judgment.modified_by_role = request.membership.role
            judgment.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Answer.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)


class JudgementBulkCreateView(JudgementBaseView, ListBulkCreateAPIView):
    def perform_create(self, serializer):
        self.update_serializer(serializer=serializer)


class AuditorOverviewRetrieveView(RetrieveAPIView):
    lookup_url_kwarg = 'assessment_pk'
    serializer_class = JudgementSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        assessment_pk = self.kwargs.get(self.lookup_url_kwarg, '')
        return Assessment.objects.get(pk=assessment_pk)

    def get(self, request, *args, **kwargs):
        try:
            w_ctx = AssessmentWorkflow.objects.get(assessment=self.get_queryset())
            judgements = self.get_serializer(
                Judgement.objects.filter(assessment=self.get_queryset(), value="no"),
                many=True
            ).data

            result = prepare_auditor_overview_response(
                w_ctx=w_ctx, judgements=judgements
            )
            return Response(result)
        except ObjectDoesNotExist:
            return Response(status.HTTP_404_NOT_FOUND)


class AssessmentReportView(RetrieveAPIView):
    lookup_url_kwarg = 'assessment_pk'
    serializer_class = AssessmentReportSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        assessment_pk = self.kwargs.get(self.lookup_url_kwarg, '')
        assessment = get_object_or_404(Assessment, pk=assessment_pk)
        workflow = get_object_or_404(AssessmentWorkflow, assessment=assessment)
        return assessment, workflow

    def get(self, request, *args, **kwargs):
        assessment, workflow = self.get_queryset()
        data = self.get_serializer(assessment).data
        return Response(data)

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        __, workflow = self.get_queryset()
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'workflow': workflow
        }


class ExternalAuditorListView(BaseOrganizationView, ListAPIView):
    """
    List of all organization auditors in the current assessment

    """
    lookup_url_kwarg = 'assessment_pk'
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return OrganizationMembership.objects.get_external_auditors(
            membership=self.request.membership
        )


class AssessmentListCreateView(generics.ListCreateAPIView):
    """
    Assessment list
    :param request:
    :return: list of assessments for current organization
    """
    serializer_class = AssessmentSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        return Assessment.objects.filter(organization=self.request.organization)


class ProductDependentQuestionnaireView(generics.CreateAPIView):
    serializer_class = QuestionnaireSerializer
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def post(self, request, *args, **kwargs):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        product_types = Product.objects.filter(pk__in=request.data).values_list('product_type', flat=True)

        assessment_product_types = assessment.products.all().exclude(
            pk__in=request.data
        ).values_list('product_type', flat=True)

        questionnaires = Questionnaire.objects.filter(
            product_types__in=product_types, assessment_type=assessment.assessment_type
        ).exclude(product_types__in=assessment_product_types)

        selected_workflow = assessment.get_workflow_context().selected_workflow
        if selected_workflow != 'hzpc':
            questionnaires = questionnaires.exclude(code__in=settings.HZPC_FIFTH_QUESTIONNAIRE_CODES)

        return Response(self.get_serializer(questionnaires, many=True).data, status.HTTP_200_OK)


class AssessmentStatusesView(generics.GenericAPIView):
    """
    Assessment list
    :param request:
    :return: list of assessments statuses
    """
    permission_classes = (IsAuthenticated, MembershipRequiredPermission)

    def get_queryset(self):
        # return ASSESSMENT_STATUSES
        membership = self.request.membership
        organization_role = membership.role
        is_auditor = INTERNAL_INSPECTOR == organization_role.name
        is_coordinator = INTERNAL_AUDITOR == organization_role.name
        harvest_year = self.request.query_params.get("year")

        queryset = AssessmentWorkflow.objects
        if is_auditor:
            queryset = self.auditor_workflows(queryset, membership)
        if is_coordinator:
            queryset = self.coordinator_workflows(queryset, membership)
        if harvest_year and harvest_year != "All":
            queryset = queryset.filter(harvest_year=harvest_year)

        return queryset.values("assessment_status").annotate(
            workflow_count=Count('assessment_status')
        ).order_by()

    def auditor_workflows(self, queryset, membership):
        return queryset.filter(
            auditor_organization=membership.secondary_organization
        ).exclude(
            workflow_status='error'
        )

    def coordinator_workflows(self, queryset, membership):
        ids = OrganizationMembership.objects.filter(
            primary_organization=membership.primary_organization).distinct().values_list(
            'secondary_organization', flat=True
        )

        return queryset.exclude(
            assessment_status=""
        ).exclude(
            workflow_status='error'
        ).filter(
            author_organization__uuid__in=ids
        )

    def get(self, request, *args, **kwargs):
        data = []
        assessment_statuses_dict = dict()
        for statuses in self.get_queryset():
            assessment_statuses_dict[statuses["assessment_status"]] = statuses["workflow_count"]
        for statuses in ASSESSMENT_STATUSES:
            data.append({
                "code": statuses[0],
                "value": statuses[1],
                "work_flow_count": assessment_statuses_dict.get(statuses[0], 0)
            })

        return Response(data)
