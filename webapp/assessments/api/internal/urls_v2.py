from django.conf.urls import patterns, url

from assessments.api.internal.membership_views import AssessmentAttachmentsListView

urlpatterns = patterns(
    'assessments.api.internal.views',
    url(
        r'(?P<assessment_pk>.+)/attachments/$',
        AssessmentAttachmentsListView.as_view(),
        name='membership-assessment-attachment-links-v2'
    ),
)
