from datetime import datetime
from django.conf import settings

from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from raven.contrib.django.raven_compat.models import client
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import (
    ListAPIView, ListCreateAPIView, DestroyAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView, CreateAPIView,
    RetrieveUpdateAPIView)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework_bulk import ListBulkCreateAPIView

from assessments.api.internal.serializers import (
    AssessmentShareSerializer, JudgementSerializer, get_assessment_answers, AssessmentDocumentLinkSerializer,
    QuestionnaireElementStateSerializer, AnswerSimpleSerializer, AssessmentSerializer,
    AssessmentSimpleSerializer, AnswerSerializer, AnswerNeatSerializer, AssessmentWithAttachmentLinksCountSerializer,
    QuestionnaireStateSerializer, AssessmentDocumentLinkReadonlySerializer,
    AssessmentDataDocumentLinkSerializer)
from assessments.api.internal.utils import prepare_auditor_overview_response
from assessments.models import (
    Answer, Judgement, Assessment, AssessmentType, QuestionnaireState, Questionnaire, QuestionnaireElementState,
    AssessmentDocumentLink
)
from assessments.utils import make_attachment_link_replicator, get_bucket
from attachments.models import FileAttachment, Attachment, NotApplicableAttachment
from form_builder.api.internal.serializers import GenericFormDataSerializer
from form_builder.models import GenericFormData
from farm_group.utils import get_all_data_objects_by_state, get_user_info
from assessment_workflow.models import AssessmentWorkflow
from organization.api.internal.serializers import ProductSerializer, AssessmentProductLinkSerializer
from organization.mixins import OrganizationMixin
from organization.models import Organization, Product
from rbac.models import GroupRole
from sams.communication.auditor import assessment_shared


@api_view(['GET'])
def internal_api_assessments_root(request, format=None):
    """
    Root for Assessments Internal API
    :param request:
    :param format:
    :return:
    """
    return Response({
        'assessment-types': reverse('assessment-types', request=request, format=format),
        'assessments': reverse('assessments', request=request, format=format),
    })


# ---------- BEGIN assessments -------------

class AssessmentListCreateView(OrganizationMixin, generics.ListCreateAPIView):
    """
    Assessment list
    :param request:
    :return: list of assessments for current organization
    """
    serializer_class = AssessmentSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = Assessment.objects.filter(organization=self.get_organization())
        state = self.request.query_params.get("state")
        if state:
            queryset = queryset.filter(state=state)
        return queryset


class AssessmentRetrieveUpdateView(OrganizationMixin, RetrieveUpdateAPIView):
    """
    Assessment detail
    :param request:
    :return: assessment for current organization
    """
    serializer_class = AssessmentSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('pk', ''))

    def get(self, request, *args, **kwargs):
        result = {}
        assessment = self.get_queryset()

        assessment_data = self.get_serializer(assessment).data
        questionnaires_with_completeness_check, available_questionnaires = assessment.get_questionnaires_detail()

        assessment_data.update({"available_questionnaries": available_questionnaires})
        assessment_data.update({"completeness_required_for": questionnaires_with_completeness_check})

        result['assessment'] = assessment_data
        result['detail'] = AssessmentSimpleSerializer(assessment).data
        result['products'] = ProductSerializer(assessment.products.all(), many=True).data
        data = get_assessment_data(request, assessment)
        result.update(data)
        self.get_document_types(request.user, result, assessment)

        return Response(result)

    @staticmethod
    def get_attachment_count(current_date, document_link):
        return 1 if not AssessmentRetrieveUpdateView.is_attachment_expired(current_date, document_link) else 0

    @staticmethod
    def is_attachment_expired(current_date, document_link):
        child_object = Attachment.objects.get_subclass(pk=document_link.attachment.pk)
        # The NotApplicableAttachment attachments should not be counted
        if isinstance(child_object, NotApplicableAttachment):
            return True
        elif isinstance(child_object, FileAttachment):
            expiration_date = child_object.expiration_date
            expiration_date = datetime.strptime(expiration_date, '%Y-%m-%d').date() if isinstance(
                expiration_date, basestring) else expiration_date
            return expiration_date and expiration_date < current_date
        else:
            return False

    def get_document_types(self, user, assessment_data, assessment):
        document_type_reuse_attachments = []
        today = datetime.today().date()
        counts = {}
        document_links = AssessmentDocumentLink.objects.filter(
            assessment__organization=assessment.organization
        ).exclude(assessment=assessment).distinct()
        for document_link in document_links:
            document_type_uuid = document_link.document_type_attachment.uuid
            attachment_count = self.get_attachment_count(today, document_link)
            if document_type_uuid in counts:
                counts[document_type_uuid] += attachment_count
            else:
                counts[document_type_uuid] = attachment_count

        for document_type_uuid, count in counts.items():
            document_type_reuse_attachments.append({
                "document_type_uuid": document_type_uuid,
                "attachment_count": count
            })
        assessment_data['document_type_reuse_attachments'] = document_type_reuse_attachments

    def get_object(self):
        return self.get_queryset()

    def partial_update(self, request, *args, **kwargs):
        old_partner_set = set()

        if 'pk' in kwargs:
            try:
                old_object = Assessment.objects.get(pk=kwargs.get('pk', ''))
                old_partner_set = set(map(lambda share: share.partner.uuid, old_object.shares.all()))
            except Assessment.DoesNotExist:
                pass

        result = super(generics.RetrieveUpdateAPIView, self).partial_update(request, args, kwargs)

        if self.get_object().state == 'closed':
            new_partner_set = set(map(lambda share: share.partner.uuid, self.get_object().shares.all()))
            notify_set = new_partner_set - old_partner_set
            for partner_uuid in notify_set:
                try:
                    partner = Organization.objects.get(pk=partner_uuid)
                    assessment_shared(request, self.get_object(), partner)
                except Organization.DoesNotExist:
                    client.captureException()

        return result


# ---------- END assessments -------------

class AuditorOverviewRetrieveView(OrganizationMixin, RetrieveAPIView):
    lookup_url_kwarg = 'assessment_pk'
    serializer_class = JudgementSerializer

    def get_queryset(self):
        assessment_pk = self.kwargs.get(self.lookup_url_kwarg, '')
        return Assessment.objects.get(pk=assessment_pk)

    def get(self, request, *args, **kwargs):
        try:
            w_ctx = AssessmentWorkflow.objects.get(assessment=self.get_queryset())
            judgements = self.get_serializer(
                Judgement.objects.filter(assessment=self.get_queryset(), value="no"),
                many=True
            ).data

            result = prepare_auditor_overview_response(
                w_ctx=w_ctx, judgements=judgements
            )
            return Response(result)
        except ObjectDoesNotExist:
            return Response(status.HTTP_404_NOT_FOUND)


class OrganizationAssessmentsByTypeView(OrganizationMixin, ListAPIView):
    serializer_class = AssessmentWithAttachmentLinksCountSerializer

    def get_queryset(self):
        assessment_type = AssessmentType.objects.get(pk=self.kwargs.get('assessment_type_pk', ''))

        return Assessment.objects.get_organization_assessments(
            organization=self.get_organization(), assessment_type=assessment_type,
        )

    def get(self, request, *args, **kwargs):
        assessments = [self.get_serializer(assessment).data for assessment in self.get_queryset()]

        return Response(assessments)


class AssessmentEvidenceCopyView(OrganizationMixin, CreateAPIView):
    serializer_class = AssessmentDocumentLinkSerializer

    def post(self, request, *args, **kwargs):
        result = []
        try:
            source_assessment = Assessment.objects.get(pk=self.kwargs.get('source_assessment_pk', ''))
            target_assessment = Assessment.objects.get(pk=self.kwargs.get('target_assessment_pk', ''))

            if source_assessment and target_assessment:
                source_attachment_links = source_assessment.document_links.all()
                bucket = get_bucket()
                replicator = make_attachment_link_replicator(request.user, target_assessment, bucket=bucket)
                map(replicator, source_attachment_links)
                attachment_links = AssessmentDocumentLink.objects.filter(
                    assessment=target_assessment
                )
                return Response(self.get_serializer(attachment_links, many=True).data, status.HTTP_201_CREATED)
            else:
                return Response(result, status.HTTP_403_FORBIDDEN)
        except Assessment.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)


class QuestionnaireStateListCreateView(OrganizationMixin, ListCreateAPIView):
    serializer_class = QuestionnaireStateSerializer

    def get_queryset(self):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        questionnaire = Questionnaire.objects.get(pk=self.kwargs.get('questionnaire_pk', ''))
        state, _ = QuestionnaireState.objects.get_or_create(assessment=assessment, questionnaire=questionnaire)

        return state, assessment

    def get(self, request, *args, **kwargs):
        try:
            questionnaire_state, _ = self.get_queryset()
            return Response(self.get_serializer(questionnaire_state).data, status.HTTP_200_OK)
        except (Assessment.DoesNotExist, Questionnaire.DoesNotExist):
            return Response({}, status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        questionnaire_state, assessment = self.get_queryset()
        questionnaire_state.is_complete = request.data.get('is_complete', questionnaire_state.is_complete)

        if not questionnaire_state.is_signed and request.data.get('is_signed', ''):
            assessment.modified_time = datetime.now()
            assessment.save()

        questionnaire_state.is_signed = request.data.get('is_signed', questionnaire_state.is_signed)
        questionnaire_state.save()

        return Response(self.get_serializer(questionnaire_state).data, status.HTTP_200_OK)


class AnswersListCreateView(OrganizationMixin, ListCreateAPIView):
    serializer_class = AnswerSimpleSerializer

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        assessment = self.get_queryset()
        answers_json = {}

        for answer in Answer.objects.filter(assessment=assessment).all():
            answers_json[answer.question_id] = self.get_serializer(
                answer,
                context={'assessment': assessment}
            ).data

        return Response(answers_json, status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        if not request.META.get('CONTENT_TYPE', 'none/none').startswith('application/json'):
            return Response({}, status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)

        try:
            data = request.data if type(request.data) is list else request.data.get('answers', '')
            assessment = self.get_queryset()
            failed_answers = save_assessment_answers(
                request,
                assessment,
                data
            )
            result = {'success': bool(failed_answers)}
            response_status = status.HTTP_200_OK if not failed_answers else status.HTTP_400_BAD_REQUEST
            received_answers = data if type(data) is list else data.values()
            retrieved_answers = get_assessment_answers(assessment, format='neat')
            saved_answers = {}

            for answer in received_answers:
                if answer['question'] in retrieved_answers:
                    saved_answers[answer['question']] = retrieved_answers[answer['question']]

            result['data'] = saved_answers
            result['failed_answers'] = failed_answers
            return Response(result, response_status)
        except Exception as ex:
            return Response({"error": ex.message}, status.HTTP_500_INTERNAL_SERVER_ERROR)


class AnswerRetrieveUpdateDestroyView(OrganizationMixin, generics.RetrieveUpdateDestroyAPIView):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

    def get_object(self):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk'))
        answer = self.queryset.get(pk=self.kwargs.get('answer_pk'))

        if answer.assessment.uuid != assessment.uuid:
            raise Http404('No %s matches the given query.' % Answer.model._meta.object_name)

        self.check_object_permissions(self.request, answer)
        return answer

    def update(self, request, *args, **kwargs):
        try:
            partial = kwargs.pop('partial', False)
            answer = self.get_object()

            data = request.data
            data["author"] = request.user.pk

            serializer = AnswerSimpleSerializer(answer, data=data,
                                                partial=partial, context=self.get_serializer_context())
            if not serializer.is_valid():
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            if answer is None:
                answer = serializer.save(force_insert=True)
                self.perform_update(answer)
                return Response(serializer.data, status=status.HTTP_201_CREATED)

            answer = serializer.save(force_update=False)
            self.perform_update(answer)
            return Response(AnswerNeatSerializer(answer).data, status=status.HTTP_200_OK)
        except Exception as ex:
            return Response({"errors": ex.message}, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get_serializer_context(self):
        assessment_pk = getattr(self.kwargs, 'assessment_pk', '')

        return {
            'assessment': Assessment.objects.get(pk=assessment_pk) if assessment_pk else None,
            'request': self.request
        }


class JudgementBaseView(OrganizationMixin, APIView):
    serializer_class = JudgementSerializer

    def get_queryset(self):
        assessment = Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        return Judgement.objects.filter(assessment=assessment)

    def update_serializer(self, serializer):
        serializer.save(
            author=self.request.user,
            assessment=Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))
        )


class JudgementBulkCreateView(JudgementBaseView, ListBulkCreateAPIView):
    def perform_create(self, serializer):
        self.update_serializer(serializer=serializer)


class JudgementRetrieveUpdateDestroyView(JudgementBaseView, RetrieveUpdateDestroyAPIView):
    def perform_update(self, serializer):
        self.update_serializer(serializer=serializer)


class AssessmentAttachmentsListCreateView(OrganizationMixin, ListCreateAPIView):
    serializer_class = AssessmentDocumentLinkSerializer

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        try:
            assessment = self.get_queryset()
            assessment_document_links = AssessmentDocumentLink.objects.filter(
                assessment=assessment
            )
            result = self.get_serializer(assessment_document_links, many=True).data
            return Response(result, status=status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response([], status.HTTP_403_FORBIDDEN)

    def post(self, request, *args, **kwargs):

        organization = self.get_organization()
        data = request.data

        if isinstance(data, list):
            for idx, item in enumerate(data):  # TODO: fined a better way of manipulation using DRF 3
                item['attachment']['organization'] = organization.uuid
                item['author'] = request.user.pk
                data[idx] = item
        serializer = self.get_serializer(
            data=data,
            many=True
        )
        if serializer.is_valid():
            assessment_document_links = serializer.save()
            return Response(self.get_serializer(assessment_document_links, many=True).data, status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class AssessmentAttachmentRetrieveUpdateDestroyView(OrganizationMixin, RetrieveUpdateDestroyAPIView):
    serializer_class = AssessmentDocumentLinkSerializer

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def put(self, request, *args, **kwargs):
        attachment_link_pk = self.kwargs.get('attachment_link_pk', '')
        organization = self.get_organization()
        attachment_link = AssessmentDocumentLink.objects.get(pk=attachment_link_pk)
        data = request.data

        if isinstance(data, dict):  # because validation goes later and we need to be sure that this is dict
            data['attachment']['organization'] = organization.uuid  # rights for assessment already checked
            data['author'] = request.user.pk

        serializer = self.get_serializer(
            instance=attachment_link,
            data=data,
            many=False,
            partial=True
        )
        if serializer.is_valid():
            updated_attachment_link = serializer.save()

            attachment_data = data['attachment']
            if attachment_data.get('title', None):
                updated_attachment_link.attachment.title = attachment_data['title']
            if attachment_data.get('description', None):
                updated_attachment_link.attachment.description = attachment_data['description']
            updated_attachment_link.attachment.agriform_reference = attachment_data.get('agriform_reference')
            updated_attachment_link.attachment.save()
            if attachment_data.get('generic_form_data', None):
                try:
                    generic_form_data_serializer = GenericFormDataSerializer(
                        instance=GenericFormData.objects.get(uuid=attachment_data['generic_form_data']['uuid']),
                        data=attachment_data['generic_form_data'],
                        many=False,
                        partial=True
                    )
                    if generic_form_data_serializer.is_valid():
                        updated_generic_form_data = generic_form_data_serializer.save()
                        updated_attachment_link.attachment.generic_form_data = updated_generic_form_data
                except GenericFormData.DoesNotExist:
                    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
            result = self.get_serializer(updated_attachment_link).data
            return Response(result, status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        result = []
        try:
            attachment_link_pk = self.kwargs.get('attachment_link_pk', '')
            assessment = self.get_queryset()
            if assessment:
                assessment_document_link = AssessmentDocumentLink.objects.get(pk=attachment_link_pk)
                result = self.get_serializer(assessment_document_link).data
                assessment_document_link.delete()
                return Response(result, status.HTTP_200_OK)
            else:
                return Response(result, status.HTTP_403_FORBIDDEN)
        except Assessment.DoesNotExist, AssessmentDocumentLink.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)


class AssessmentProductListCreateView(OrganizationMixin, ListCreateAPIView):
    serializer_class = AssessmentProductLinkSerializer

    def get_queryset(self):
        return Assessment.objects.get(pk=self.kwargs.get('assessment_pk', ''))

    def get(self, request, *args, **kwargs):
        result = []
        try:
            assessment = self.get_queryset()
            result = [product_pk for product_pk in assessment.products.all().values_list('uuid', flat=True)]
            return Response(result, status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        result = []
        data = request.data
        organization = self.get_organization()
        assessment = self.get_queryset()
        organization_products = organization.products.all().values_list('uuid', flat=True)
        if all(product_pk in organization_products for product_pk in data):
            for product_pk in data:
                product_instance = Product.objects.get(uuid=product_pk)
                assessment.products.add(product_instance)
                result.append(product_pk)
            return Response(result, status.HTTP_201_CREATED)
        else:
            return Response(result, status.HTTP_400_BAD_REQUEST)


class AssessmentProductDestroyView(OrganizationMixin, DestroyAPIView):
    def delete(self, request, *args, **kwargs):
        result = []
        assessment_pk = self.kwargs.get('assessment_pk', '')
        product_pk = self.kwargs.get('product_pk', '')
        try:
            assessment = Assessment.objects.get(pk=assessment_pk)
            if product_pk in assessment.products.all().values_list('uuid', flat=True):
                product_instance = Product.objects.get(uuid=product_pk)
                assessment.products.remove(product_instance)
                result.append(product_pk)
                return Response(result, status.HTTP_200_OK)
        except Assessment.DoesNotExist:
            return Response(result, status.HTTP_404_NOT_FOUND)
        return Response(result)


def save_assessment_answers(request, assessment, data):
    """
    Saves assessment answers from data object.

    :param request:
    :param data: answers array
    :param assessment:
    :return: tuple (success?, errors list)
    """
    # Using the serializer with many=True will bomb if anything goes wrong with any one object in the request.
    # TODO Maybe deserialize answers one by one to ignore errors?
    # OTOH if the request has errors maybe we don't want to save anything after all.
    answers = data if type(data) is list else data.values()
    [answer.update({u'author': request.user.pk}) for answer in answers]
    failed_answers = []
    for answer in answers:
        serializer = AnswerSimpleSerializer(
            data=answer,
            context={
                'request': request,
                'assessment': assessment,
            }
        )
        if serializer.is_valid():
            try:
                serializer.save()
            except:
                failed_answers.append({
                    'answer': answer,
                    'errors': serializer.errors
                })
        else:
            failed_answers.append({
                    'answer': answer,
                    'errors': serializer.errors
                })
    return failed_answers


def get_assessment_data(request, assessment, questionnaire=None):
    """
    Builds composite data dict: assessment answers+attachments
    :param request: Request, for things like user and current organization
    :param assessment: Current assessment
    :param questionnaire: (optional) Questionnaire to get data for
    :return: Assessment data dict
    """

    answers_data = get_assessment_answers(assessment)
    attachments = AssessmentDocumentLink.objects.filter(
        assessment=assessment
    ).select_related('attachment')

    if questionnaire:
        attachments = attachments.filter(
            document_type_attachment__questions__questionnaire=questionnaire
        )

    attachments_data_list = AssessmentDataDocumentLinkSerializer(attachments, many=True).data
    attachment_links = {}
    attachment_uuids = set()
    file_attachment_uuids = set()
    for attachment_data in attachments_data_list:
        if attachment_data['attachment']['attachment_type'] == 'FileAttachment':
            file_attachment_uuids.add(attachment_data['attachment']['uuid'])
        attachment_uuids.add(attachment_data['attachment']['uuid'])

    used_in_assessments = Attachment.get_used_in_assessments(attachment_uuids=attachment_uuids)
    used_in_document_types = Attachment.get_used_in_document_types(attachment_uuids=file_attachment_uuids)

    # group attachments by document type
    for attachment_data in attachments_data_list:
        # attachments = {a['document_type']: a for a in attachments_data_list}
        attachment_data['attachment']['used_in_assessments'] = used_in_assessments[
            attachment_data['attachment']['uuid']]
        if attachment_data['attachment']['attachment_type'] == 'FileAttachment':
            attachment_data['attachment']['used_in'] = used_in_document_types[attachment_data['attachment']['uuid']]

        attachment_links.setdefault(attachment_data['document_type_attachment'], []).append(attachment_data)

    # Question states
    questionnaire_element_states = get_assessment_questionnaire_element_states(assessment, questionnaire)

    shares = AssessmentShareSerializer(assessment.shares, many=True).data

    # RBAC permissions + workflow instance uuid
    permissions = []
    workflow_uuid = ""
    layout_name = get_user_info(request)["layout_name"]
    if layout_name == 'hzpc' or layout_name == 'farm_group':
        try:
            workflow_instance = AssessmentWorkflow.objects.get(assessment=assessment)
            workflow_uuid = workflow_instance.uuid
            user = request.user
            if getattr(request, 'membership', None):
                group = request.membership.role
            else:
                group = user.groups.all()

            user_role_ids = GroupRole.objects.filter(
                group=group).values_list("role__id", flat=True)
            permissions = get_all_data_objects_by_state(workflow_instance.assessment_status, user_role_ids)
        except ObjectDoesNotExist:
            permissions = []

    result = {
        'answers': answers_data,
        'attachment_links': attachment_links,
        'questionnaire_element_states': questionnaire_element_states,
        'shares': shares,
        'permissions': permissions,
        'workflow_uuid': workflow_uuid
    }

    return result


def get_assessment_questionnaire_element_states(assessment, questionnaire=None):
    """
    Gets assessment question states
    :param request:
    :param assessment_pk:
    :return: list of serialized attachments
    """
    questionnaire_element_states = QuestionnaireElementState.objects.filter(
        assessment=assessment
    )
    if questionnaire:
        questionnaire_element_states = questionnaire_element_states.filter(
            questionnaire_element__questionnaire=questionnaire
        )
    questionnaire_element_states_data = QuestionnaireElementStateSerializer(
        questionnaire_element_states,
        many=True,
        context={
            'assessment': assessment
        }
    ).data
    result = {}
    for item in questionnaire_element_states_data:
        result[item['questionnaire_element']] = item
    return result


class AssessmentDocumentLinkView(OrganizationMixin, ListCreateAPIView):
    serializer_class = AssessmentDocumentLinkReadonlySerializer
    queryset = AssessmentDocumentLink.objects.all()

    @staticmethod
    def is_attachment_expired(current_date, attachment):
        if attachment['attachment_type'] == 'NotApplicableAttachment':
            return True
        expiration_date = attachment.get('expiration_date')
        expiration_date = datetime.strptime(expiration_date, '%Y-%m-%d').date() if isinstance(
            expiration_date, basestring) else expiration_date
        return expiration_date and expiration_date < current_date

    def get(self, request, *args, **kwargs):
        assessment_pk = kwargs.get('assessment_pk', '')
        document_type_pk = kwargs.get('document_type_pk', '')
        try:
            assessment = Assessment.objects.get(pk=assessment_pk)
        except ObjectDoesNotExist:
            return Response("Assessment does not exists.", status=status.HTTP_404_NOT_FOUND)

        queryset = self.queryset.filter(
            document_type_attachment=document_type_pk, assessment__organization=self.get_organization()
        ).exclude(assessment=assessment).order_by('-timestamp').distinct()

        document_links = self.serializer_class(queryset, many=True).data
        today = datetime.today().date()
        un_expired_links = [
            link for link in document_links
            if not AssessmentDocumentLinkView.is_attachment_expired(today, link['attachment'])
            ]
        return Response(un_expired_links)

    def post(self, request, *args, **kwargs):
        assessment_pk = kwargs.get('assessment_pk', '')
        new_document_links = []

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            document_link_ids = serializer.validated_data.get('document_links')
            try:
                target_assessment = Assessment.objects.get(pk=assessment_pk)
                source_attachment_links = AssessmentDocumentLink.objects.filter(id__in=document_link_ids)
                bucket = get_bucket()
                replicator = make_attachment_link_replicator(request.user, target_assessment, bucket=bucket)
                new_document_links = map(replicator, source_attachment_links)

                new_document_links = self.get_serializer(new_document_links, many=True).data
            except Assessment.DoesNotExist:
                return Response(new_document_links, status.HTTP_404_NOT_FOUND)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(new_document_links, status.HTTP_201_CREATED)
