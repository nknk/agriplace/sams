from rest_framework import permissions
from assessments.models import Assessment


class UserAndAssessmentOrganizationMatch(permissions.BasePermission):
    """ """
    def has_object_permission(self, request, view, obj):
        return obj.check_permissions(request.user, 'write')
