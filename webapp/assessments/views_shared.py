from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext as _

from ap_extras.item_list import ItemListModel, ItemListColumn
from assessments.models import Assessment, Questionnaire
from assessments.views import get_assessments_context
from attachments.models import DocumentType
from organization.views import get_organization_context
from sams.constants import ICONS
from sams.decorators import membership_required
from sams.helpers import get_object_for_user


def get_shared_assessments_context(request, kwargs, page_name):
    """
    """
    context, organization = get_organization_context(request, kwargs, page_name)
    context.update({
        'page_icon': ICONS.get('assessment'),
        'page_triggers': ['shared_assessments', page_name],
    })
    context["nav_links"] = [
        {  # category
            "text": '',
            "links": [
                {  # link
                    'name': 'shared_assessment_list',
                    'text': _("My assessments"),
                    'icon': ICONS['list'],
                    'url': reverse('shared_assessment_list', args=[request.membership.pk])
                },
            ]
        }
    ]
    return context, organization


def get_shared_assessment_context(request, kwargs, page_name=""):
    context, organization = get_shared_assessments_context(request, kwargs, page_name)
    view_layout = context['layout_name']
    assessment = get_object_for_user(request.user, Assessment, 'read', pk=kwargs["assessment_pk"])
    assessment_type = assessment.assessment_type
    user_role = request.membership.role.name
    context.update({
        "assessment": assessment,
        "organization": organization,
        "page_title": assessment.name,
        "page_title_secondary": _("assessment"),
        "page_title_url": reverse("shared_assessment_detail", args=[request.membership.pk, assessment.pk]),
        "page_triggers": ["shared_assessments", page_name],
        "page_icon": ICONS["assessment"],
        "breadcrumbs": [
            {
                "text": _("Shared assessments"),
                "url": reverse("shared_assessment_list", args=[request.membership.pk]),
                "icon": None
            },
        ],
        'assessment_sections': assessment_type.get_assessment_type_section_list(view_layout, user_role),
        'read_only': True
    })

    return context, organization, assessment


@login_required
@membership_required
def assessment_list(request, *args, **kwargs):
    """
    Assessments list page
    """
    context, organization = get_assessments_context(request, kwargs, page_name='shared_assessments_list')

    context.update({
        'page_title': _("Assessments shared by others"),
        'page_title_url': reverse("shared_assessment_list", args=[request.membership.pk]),
        'page_icon': ICONS['share'],
        'page_triggers': 'shared_assessments'
    })

    context['breadcrumbs'] += [
        {
            'text': _("Assessments")
        },
    ]

    context['assessment_icon_class'] = ICONS.get("assessment")

    # Shared assessments item list model
    shared_assessments_model = ItemListModel()
    shared_assessments_model.id = "shared_assessments"
    shared_assessments_model.hide_toolbar = True
    shared_assessments_model.items = Assessment.objects.filter(assessment_type__kind="assessment",
                                                               shares__partner=organization).order_by('-created_time')
    shared_assessments_model.columns = [
        ItemListColumn(
            header=_("Organization"),
            text=lambda assessment: assessment.organization.name,
        ),
        ItemListColumn(
            header=_("Assessment"),
            text=lambda assessment: assessment.__unicode__,
            url=lambda assessment: reverse("shared_assessment_detail", args=[request.membership.pk, assessment.pk]),
            icon_class=lambda assessment: ICONS['assessment']
        ),
        ItemListColumn(
            header=_("Type"),
            text=lambda assessment: assessment.assessment_type.name,
        ),
        ItemListColumn(
            header=_("Crops"),
            text=lambda assessment: assessment.get_product_list
        )
    ]

    shared_assessments_model.no_items_text = _("No shared assessments available.")
    shared_assessments_model.refresh()
    context['shared_assessments_model'] = shared_assessments_model

    return render(request, "assessments/assessment_list_shared.html", context)


@login_required
@membership_required
def assessment_detail(request, *args, **kwargs):
    """
    Assessment detail page
    """
    context, _, assessment = get_shared_assessment_context(request, kwargs, 'assessment_detail')
    context['page_triggers'] += ['shared_assessment_detail']
    result_query = assessment.results
    if result_query.count():
        context['result'] = assessment.results.latest('created_time')
    # sections
    if assessment.organization not in request.user.organizations.all():
        while 'sharing' in context['assessment_sections']:
            context['assessment_sections'].remove('sharing')
        while 'products' in context['assessment_sections']:
            context['assessment_sections'].remove('products')
    # readonly
    context["read_only"] = assessment.state == 'closed'
    return render(request, 'assessments/assessment_detail_shared.html', context)


@login_required
@membership_required
def assessment_documents(request, *args, **kwargs):
    context, organization, assessment = get_shared_assessment_context(request, kwargs, 'shared_assessment_documents')
    # sections
    if organization not in request.user.organizations.all():
        while 'sharing' in context['assessment_sections']:
            context['assessment_sections'].remove('sharing')
        while 'products' in context['assessment_sections']:
            context['assessment_sections'].remove('products')
    context["read_only"] = assessment.state == 'closed'
    return render(request, 'assessments/assessment_documents_shared.html', context)


@login_required
@membership_required
def assessment_questionnaires(request, *args, **kwargs):
    context, organization, assessment = get_shared_assessment_context(
        request, kwargs, page_name='shared_assessment_questionnaires')
    context["page_triggers"] += ["shared_assessment_questionnaires"]
    context['read_only'] = True
    result_query = assessment.results
    if result_query.count():
        context["result"] = assessment.results.latest("created_time")

    questionnaires_query = assessment.assessment_type.questionnaires.all().order_by('order_index')
    # Tree list
    context["list"] = {
        "headers": [_("Questionnaire"), _("Progress")],
        "items": questionnaires_query,
        "rows": [],
        "no_items_text": _("No questionnaires available."),
    }

    for questionnaire in questionnaires_query:
        context["list"]["rows"].append({
            "cells": [
                {
                    "icon": ICONS['list'],
                    "text": questionnaire.name,
                    "url": reverse(
                        "shared_questionnaire_view",
                        kwargs={
                            "membership_pk": request.membership.pk,
                            "assessment_pk": assessment.pk,
                            "questionnaire_pk": questionnaire.pk
                        }
                    )
                },
                {
                    'text': "{progress}".format(progress=questionnaire.get_progress(assessment)),
                }
            ]
        })

    if questionnaires_query.count() == 1:
        return redirect(reverse(
            "shared_questionnaire_view", kwargs={
                "membership_pk": request.membership.pk,
                "assessment_pk": assessment.pk,
                "questionnaire_pk": questionnaire.pk
            })
        )
    else:
        return render(request, "assessments/assessment_questionnaires_shared.html", context)


@login_required
@membership_required
def questionnaire_view(request, *args, **kwargs):
    context, organization, assessment = get_shared_assessment_context(request, kwargs, 'shared_questionnaire_view')
    questionnaire = get_object_or_404(Questionnaire, pk=kwargs['questionnaire_pk'])
    context.update({
        'page_title': questionnaire.name,
        'page_title_secondary': _("questionnaire"),
        'breadcrumbs': [
            {
                'text': _("Assessments"),
                'url': reverse('shared_assessment_list', args=[request.membership.pk])

            },
            {
                'text': _("Assessment %s") % assessment.name,
                'url': reverse('shared_assessment_detail', args=[request.membership.pk, assessment.pk]),
            }
        ],
        'back_url': reverse('shared_assessment_detail', args=[request.membership.pk, assessment.pk]),
        'questionnaire': questionnaire,
    })
    context["page_triggers"] += ["shared_assessment_questionnaires"]
    return render(request, 'assessments/questionnaire_view_shared.html', context)


@login_required
@membership_required
def assessment_results(request, *args, **kwargs):
    """
    Results page
    """
    context, organization, assessment = get_shared_assessment_context(
        request, kwargs, page_name='my_assessment_results')

    context["page_triggers"] += ["shared_assessment_results"]
    # sections
    if organization not in request.user.organizations.all():
        while 'sharing' in context['assessment_sections']:
            context['assessment_sections'].remove('sharing')
        while 'products' in context['assessment_sections']:
            context['assessment_sections'].remove('products')
    context["read_only"] = assessment.state == 'closed'
    return render(request, 'assessments/assessment_results_shared.html', context)
