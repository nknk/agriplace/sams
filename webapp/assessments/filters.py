from django.contrib import admin


class AssessmentTypeKindFilter(admin.SimpleListFilter):
    title = 'Assessment kind'
    parameter_name = 'assessment_kind'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """

        return (
            ("assessment", "Assessment"),
            ("agriform", "Agriform"),
            ("agriform2", "Agriform 2")
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """

        if self.value():
            return queryset.filter(assessment_type__kind=self.value())
        else:
            return queryset
