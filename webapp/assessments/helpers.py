import datetime
from django.http import HttpResponse
from xlwt import Workbook

from assessment_workflow.models import AssessmentWorkflow
from assessments.models import Assessment
from assessments.utils import get_bucket, make_attachment_link_replicator
from organization.models import OrganizationMembership
from translations.helpers import _get_language_pofile, _check_identifier_existance
from django.conf import settings


def create_pending_translation_report(fields, data):

    translation_data = create_sheet_header_and_translation_info(fields)
    create_data_pending_translations(data, translation_data[1], fields, translation_data[2])
    return translation_data[0]


def create_sheet_header_and_translation_info(fields):
    report_book = Workbook()
    translation_info = []
    row_counters = []
    base_language = settings.BASE_LANGUAGE
    supported_langauges = settings.LANGUAGES
    for language in supported_langauges:
        if language[0] not in base_language:
            pofile = _get_language_pofile(language[0])
            report_sheet = report_book.add_sheet(language[1])
            report_header_row = report_sheet.row(0)
            for counter in range(len(fields)):
                excel_column = counter*2
                report_header_row.write(excel_column, fields[counter])
                report_header_row.write(excel_column+1, "target {}".format(fields[counter]))
            translation_info.append((language[1], pofile, report_sheet))
            row_counters.append(0)

    return report_book, translation_info, row_counters


def create_data_pending_translations(data, translation_info, fields, row_counters):
    for row in data:
        for trans_counter in range(len(translation_info)):
            info = translation_info[trans_counter]
            pofile = info[1]
            sheet = info[2]
            found = False
            for counter in range(len(fields)):
                value = getattr(row, fields[counter]).strip()
                if value and not _check_identifier_existance(value, pofile):
                    if not found:
                        found = True
                        row_counters[trans_counter] += 1
                        report_row = sheet.row(row_counters[trans_counter])
                    excel_column = counter*2
                    report_row.write(excel_column, value)
                    report_row.write(excel_column+1, "")


def create_excel_response(report_book, model_name):
    now = datetime.datetime.now()
    file_name = "pending-translation-{}-{}.xls".format(model_name, now.strftime("%d.%m.%y-%H:%M:%S"))
    response = HttpResponse(content_type="application/ms-excel")
    report_book.save(response)
    response['Content-Disposition'] = 'attachment; filename={}'.format(file_name)
    return response


#
# Assesment copy functions. See assessment_copy() for details.
#

def error_log_add(error_list, message, description=""):
    error_list.append({
        "message": message,
        "description": description
    })


def attachment_copy(source_attachment, target_author, target_assessment, target_organization, error_list):
    bucket = get_bucket()  # if S3 there will be link to bucket, else local media folder
    # reusing of existing functionality for evidence reuse
    replicator = make_attachment_link_replicator(
        target_author,
        target_assessment,
        organization=target_organization,
        bucket=bucket
    )
    try:
        replicator(source_attachment)
    except IOError:
        # oops make_attachment_link_replicator() can't find source attachment
        # sad, let's inform user
        error_log_add(error_list, 'Attachement not found', description='{}'.format(source_attachment))


def answer_copy(source_answer, target_assessment):
    target_answer = source_answer
    target_answer.uuid = None
    target_answer.assessment = target_assessment
    target_answer.save()


def workflow_copy(source_workflow, target_organization, target_author, target_membership):
    # It is easy to create new one then reinit of existing workflow instance
    workflow = AssessmentWorkflow.objects.create(
        author_organization=target_organization,
        author=target_author,
        membership=target_membership,
        assessment_type=source_workflow.assessment_type,
        assessment_status="internal_audit_open",
        selected_workflow=source_workflow.selected_workflow,
        harvest_year=source_workflow.harvest_year,
        created_time=datetime.datetime.now(),
        modified_time=datetime.datetime.now()
    )
    return workflow


def assessment_copy(source_assessment, target_membership):
    # Function to copy Assessment(with evidences of all types, answers, and workflow)
    # Before run this function make sure that source_aasessment and target_membership are exists(internal
    # checking excluded because of perfomance issues)
    # Returns blank list if everything copied well or list of errors.
    # Function stops only on critical errors.

    error_list = []  # list of errors fired during copy

    # secondary_organization required field, so if target_membership exist no need to check
    target_organization = target_membership.secondary_organization

    # We still have user as required field for workflows and document_links
    target_organization_user_list = target_organization.users.all()
    if not target_organization_user_list:
        error_log_add(error_list, 'No users attached to target organization', '{}'.format(target_organization.name))
        return error_list
    # all fine here, let's do some ... trick
    target_author = target_organization_user_list.first()

    # collecting data from source assessment
    source_answer_list = source_assessment.answers.all()
    source_workflow_list = source_assessment.assessmentworkflow_set.all()
    source_attachment_list = source_assessment.document_links.all()

    # time to create copy
    target_assessment = source_assessment

    # first new assessment itself
    target_assessment.uuid = None
    target_assessment.name = None
    target_assessment.created_time = datetime.datetime.now()
    target_assessment.modified_time = datetime.datetime.now()
    target_assessment.organization = target_organization
    target_assessment.created_by = None
    target_assessment.save()


    # then workflow copy
    # first check if workflow attached and it only one
    if not source_workflow_list or len(source_workflow_list) > 1:
        # error but not critical we can continue to copy data, just informing about an issue
        error_log_add(error_list, 'No workflow atttached to source assessment or there are too many',  '')
    else:
        workflow_instance = workflow_copy(
            source_workflow_list[0],
            target_organization,
            target_author,
            target_membership
        )
        workflow_instance.assessment = target_assessment  # update of FK
        workflow_instance.save()  # save

    # second step, answers copy
    for source_answer in source_answer_list:
        answer_copy(source_answer, target_assessment)

    # ... and last one, attachment copy
    for source_attachment in source_attachment_list:
        attachment_copy(source_attachment, target_author, target_assessment, target_organization, error_list)

#
# end of assessment copy functions
#