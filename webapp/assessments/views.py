import csv
import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils.translation import ugettext as _

from app_settings.utils import get_text_blocks
from assessment_workflow.models import AssessmentWorkflow
from assessments.forms import AssessmentCreateForm
from assessments.models import (
    Assessment,
    AssessmentType,
    ASSESSMENT_STATE_CHOICES,
    AssessmentSharePermission,
    AssessmentShare
)
from farm_group.models import AssessmentMutationLog
from organization.models import Organization
from organization.views import get_organization_context
from sams.constants import ICONS, PAGINATION_ITEMS_PER_PAGE
from sams.decorators import membership_required
from sams.helpers import get_object_for_user, get_object_for_organization
from sams.views import SamsView

logger = logging.getLogger(__name__)


def get_assessments_context(request, kwargs, page_name):
    """
    """
    context, organization = get_organization_context(request, kwargs, page_name)
    context.update({
        'page_icon': ICONS.get('assessment'),
        'page_triggers': ['my_assessments', page_name],
    })
    context["nav_links"] = [
        {  # category
            "text": '',
            "links": [
                {  # link
                    'name': 'my_assessment_list',
                    'text': _("My assessments"),
                    'icon': ICONS['list'],
                    'url': reverse('my_assessment_list', args=[request.membership.pk])
                }
            ]
        }
    ]

    return context, organization


def get_navigation_links(page_name, reverse_kwargs):
    from collections import OrderedDict

    try:
        assessment = Assessment.objects.get(pk=reverse_kwargs['assessment_pk'])
    except Assessment.DoesNotExist:
        assessment = None

    assessment_admin_sections = assessment.assessment_type.assessment_sections if assessment else ''

    assessment_sections = OrderedDict([
        (
            "assessment_detail",
            dict(
                text=_("Start"),
                url=reverse("my_assessment_detail", kwargs=reverse_kwargs),
                description=_("Continue to"),
                admin_name="overview"
            )
        ),
        (
            "my_assessment_documents",
            dict(
                text=_("Evidence"),
                url=reverse("my_assessment_documents", kwargs=reverse_kwargs),
                description=_("Continue to"),
                admin_name="evidence"
            )
        ),
        (
            "my_assessment_questionnaires",
            dict(
                text=_("Answer questionnaires"),
                url=reverse(
                    "my_assessment_questionnaires", kwargs=reverse_kwargs
                ),
                description=_("Continue to"),
                admin_name="questionnaires"
            )
        ),
        (
            "my_assessment_results",
            dict(
                text=_("Results"),
                url=reverse("my_assessment_results", kwargs=reverse_kwargs),
                description=_("Continue to"),
                admin_name="results"
            )
        ),
        (
            "my_assessment_sharing",
            dict(
                text=_("Closing"),
                url=reverse("my_assessment_sharing", kwargs=reverse_kwargs),
                description=_("Continue to"),
                admin_name="sharing"
            )
        ),
        # (
        #     "finish",
        #     dict(
        #         text=_("Finish assessment"),
        #         url=reverse("home", args=[reverse_kwargs['organization_slug']]),
        #         description=_("Done") + "?",
        #         admin_name="finish"
        #     )
        # )
    ])

    if not page_name in assessment_sections:
        return dict()
    page_index = assessment_sections.keys().index(page_name)

    # prev section
    prev = None
    for item in reversed(assessment_sections.keys()[:page_index]):
        if assessment_sections[item]['admin_name'] in assessment_admin_sections.split(','):
            prev = (assessment_sections.values()[assessment_sections.keys().index(item)])
            break

    # next section
    next = None
    for item in assessment_sections.keys()[page_index + 1:]:
        if assessment_sections[item]['admin_name'] in assessment_admin_sections.split(',') + ['finish']:
            next = (assessment_sections.values()[assessment_sections.keys().index(item)])
            break

    return dict(prev_section=prev, next_section=next)

    # return dict(
    #     prev_section=(
    #         assessment_sections.values()[page_index - 1] if page_index > 0 else None
    #     ),
    #     next_section=(
    #         assessment_sections.values()[page_index + 1] if
    #         page_index < len(assessment_sections.keys()) - 1 else None
    #     )
    # )


def get_navigation_links_hzpc(page_name, reverse_kwargs):
    from collections import OrderedDict

    try:
        assessment = Assessment.objects.get(pk=reverse_kwargs['assessment_pk'])
    except Assessment.DoesNotExist:
        assessment = None

    assessment_sections = OrderedDict([
        (
            "assessment_detail",
            dict(
                text=_("Start"),
                url=reverse("my_assessment_detail", kwargs=reverse_kwargs),
                description=_("Continue to"),
                admin_name="overview"
            )
        ),
        (
            "my_assessment_questionnaires",
            dict(
                text=_("Answer questionnaires"),
                url=reverse(
                    "my_assessment_questionnaires", kwargs=reverse_kwargs
                ),
                description=_("Continue to"),
                admin_name="questionnaires"
            )
        ),
        (
            "auditor_review",
            dict(
                text=_("Auditor review"),
                url=reverse(
                    "my_assessment_review", kwargs=reverse_kwargs
                ),
                description=_("Continue to"),
                admin_name="review"
            )
        ),
    ])

    if not page_name in assessment_sections:
        return dict()
    page_index = assessment_sections.keys().index(page_name)

    # prev section
    prev = None
    for item in reversed(assessment_sections.keys()[:page_index]):
        prev = (assessment_sections.values()[assessment_sections.keys().index(item)])
        break

    # next section
    next = None
    for item in assessment_sections.keys()[page_index + 1:]:
        next = (assessment_sections.values()[assessment_sections.keys().index(item)])
        break

    return dict(prev_section=prev, next_section=next)


def get_assessment_context(request, kwargs, page_name=""):
    context, organization = get_assessments_context(request, kwargs, page_name)

    view_layout = context['layout_name']
    assessment = get_object_for_organization(organization, Assessment, pk=kwargs["assessment_pk"])
    assessment_type = assessment.assessment_type
    context.update({
        'membership': request.membership,
        "assessment": assessment,
        "organization": organization,
        "page_title": assessment.name,
        "page_title_secondary": _("assessment"),
        "page_title_url": reverse("my_assessment_detail", args=[request.membership.pk, assessment.pk]),
        "page_triggers": ["my_assessments", page_name],
        "page_icon": ICONS["assessment"],
        "breadcrumbs": [
            {
                "text": _("Assessments"),
                "url": reverse("my_assessment_list", args=[request.membership.pk]),
                "icon": None
            },
        ],
        'assessment_sections': assessment_type.get_assessment_type_section_list(
            view_layout, request.membership.role.name
        )
    })
    if view_layout == 'hzpc':
        context.update(
            get_navigation_links_hzpc(
                page_name,
                dict(
                    membership_pk=request.membership.pk,
                    assessment_pk=assessment.pk
                )
            )
        )
    else:
        context.update(
            get_navigation_links(
                page_name,
                dict(
                    membership_pk=request.membership.pk,
                    assessment_pk=assessment.pk
                )
            )
        )

    # Nav links
    # Structure:
    # [
    # {
    # text: category label
    #                 links: {
    #                     name: link name (used for highlighting current page
    #                     text: link text
    #                     url: link url
    #                 }
    #             }
    #         ]
    context["nav3_links"] = [
        {  # link
            'name': 'my_assessment_documents',
            'text': _("Evidence"),
            'url': reverse('my_assessment_documents', args=[request.membership.pk, assessment.pk])
        },
        {  # link
            "name": "my_assessment_detail",
            "text": _("Assessment"),
            "url": reverse("my_assessment_detail", args=[request.membership.pk, assessment.pk])
        },
        {  # link
            "name": "my_assessment_check",
            "text": _("Check"),
            "url": reverse("my_assessment_check", args=[request.membership.pk, assessment.pk]),
        },
        {  # link
            "name": "my_assessment_sharing",
            "text": _("Sharing"),
            "url": reverse("my_assessment_sharing", args=[request.membership.pk, assessment.pk]),
            "html_class": ""
        },
        {  # link
            "name": "home",
            "text": _("Finish assessment"),
            "url": reverse("my_assessment_sharing", args=[request.membership.pk, assessment.pk]),
            "html_class": ""
        },
    ]
    return context, organization, assessment


class AssessmentView(SamsView):
    @property
    def assessment_kwargs(self):
        return {
            "assessment_pk": self.assessment.pk
        }

    def load_url_objects(self, kwargs):
        """
        Gets objects based on URL parameters
        """
        self.assessment = get_object_for_user(self.request.user, Assessment,
                                              pk=kwargs["assessment_pk"])
        self.organization = self.assessment.organization

    def get_context_data(self, **kwargs):
        context = {
            "page_name": self.page_name,
            "page_title": _("Assessment"),
            "page_icon": ICONS["assessment"],
            "page_triggers": ["my_assessments", self.page_name] + getattr(self, "page_triggers",
                                                                          []),

            "organization": self.organization
        }
        return context


@login_required
@membership_required
def home(request, *args, **kwargs):
    return redirect('my_assessment_list', request.membership.pk)


@login_required
@membership_required
def assessment_create_start(request, *args, **kwargs):
    """
    Assessment create page
    """
    context, organization = get_assessments_context(request, kwargs, page_name='my_assessment_create_start')
    assessment_types = AssessmentType.objects.filter(kind='assessment', is_public=True).all()
    # Get user language
    user_lang = request.user.profile.language
    # Filter assessments that contain the language of the user
    context['assessment_types'] = [at for at in assessment_types if user_lang in at.available_languages.split(",")]
    return render(request, "assessments/assessment_create_start.html", context)


@login_required
@membership_required
def assessment_create_oftype(request, *args, **kwargs):
    """
    Assessment create type, choose type and name
    """
    context, organization = get_assessments_context(request, kwargs, page_name='my_assessment_create_oftype')
    try:
        assessment_type_code = kwargs['assessment_type_code']
        assessment_type = AssessmentType.objects.get(code=assessment_type_code)
        context['assessment_type'] = assessment_type
    except AssessmentType.DoesNotExist:
        raise Http404('Bad assessment type code')

    if request.method == 'POST':
        form = AssessmentCreateForm(organization, request.POST)
        if form.is_valid():
            assessment, _ = AssessmentWorkflow.objects.initialize_assessment_and_workflow(
                user=request.user, membership=request.membership, organization=organization,
                assessment_type_code=assessment_type_code
            )

            return redirect('my_assessment_detail', request.membership.pk, assessment.pk)
    else:
        return redirect('my_assessment_list', request.membership.pk)


@login_required
@membership_required
def assessment_products(request, *args, **kwargs):
    """
    Assessment products page
    """
    context, _, assessment = get_assessment_context(request, kwargs, 'assessment_products')
    # readonly
    context["read_only"] = assessment.state == 'closed'
    return render(request, 'assessments/assessment_products.html', context)


@login_required
@membership_required
def assessment_state(request, *args, **kwargs):
    """
    Change assessment state
    """
    context, organization, assessment = get_assessment_context(request, kwargs, 'assessment_detail')

    if request.method == 'POST':

        state = request.POST.get('state')
        default_url = reverse('my_assessment_detail', args=(request.membership.pk, assessment.pk))
        if state == 'closed':
            default_url = reverse('my_assessment_finished', args=(request.membership.pk, assessment.pk))
        url = request.POST.get('redirect_url', default_url)

        if state in (choice[0] for choice in ASSESSMENT_STATE_CHOICES):
            assessment.state = state
            assessment.save()
            messages.success(request, _("Saved"))
            return redirect(url)
        else:
            raise Exception("Unrecognized state.")


@login_required
def assessment_target(request, *args, **kwargs):
    """
    Assessment target selection page
    Choose which products this assessment is about.
    """
    context, organization = get_assessments_context(request, kwargs,
                                                    page_name='my_assessment_target')
    context['products'] = organization.products.all()
    return render(request, "assessments/assessment_target.html", context)


@login_required
@membership_required
def assessment_delete(request, *args, **kwargs):
    """
    Assessment delete
    """
    # AJK fix "tuple indices must be integers, not str" error on delete
    context, organization, assessment = get_assessment_context(request, kwargs, "my_assessment_delete")

    # Add variables for generic confirmation template
    context.update({
        "body_dark": True,
        "title": _("Delete %s") % assessment,
        "page_icon": ICONS["assessment"],
        "message": _("Are you sure you want to delete the assessment %s?") % assessment,
        "yes_text": _("Yes, delete %s") % assessment,
        "no_text": _("Cancel"),
        "no_url": reverse("my_assessment_detail", args=[request.membership.pk, assessment.pk])
    })
    if request.method == "POST":
        assessment.delete()
        return redirect("my_assessment_list", membership_pk=request.membership.pk)
    else:
        # GET
        return render(request, "core/confirmation.html", context)


@login_required
@membership_required
def assessment_switch(request, *args, **kwargs):
    """
    Assessment action switch
    """
    context, organization, assessment = get_assessment_context(request, kwargs)
    url_name = '{assessment_type}:{page_url}'.format(
        assessment_type=assessment.assessment_type.code,
        page_url=kwargs['page']
    )
    return redirect(
        url_name,
        membership_pk=request.membership.pk,
        assessment_pk=assessment.pk
    )


@login_required
@membership_required
def assessment_documents(request, *args, **kwargs):
    """
    Assessment required documents
    """
    context, _, assessment = get_assessment_context(request, kwargs, 'my_assessment_documents')
    # readonly
    context["read_only"] = assessment.state == 'closed'
    context['text_blocks'] = get_text_blocks(('assessment_evidence_list_intro',))
    return render(request, 'assessments/assessment_documents.html', context)


@login_required
@membership_required
def assessment_results(request, *args, **kwargs):
    """
    Results page
    """
    context, _, assessment = get_assessment_context(request, kwargs, page_name='my_assessment_results')

    # readonly
    context["read_only"] = assessment.state == 'closed'
    context["page_triggers"] += ["my_assessment_results"]

    return render(request, 'assessments/assessment_results.html', context)


@login_required
@membership_required
def assessment_check(request, *args, **kwargs):
    """
    Assessment required documents
    """
    context, _, _ = get_assessment_context(request, kwargs, 'my_assessment_check')
    return render(request, 'assessments/assessment_check.html', context)


@login_required
@membership_required
def assessment_sharing(request, *args, **kwargs):
    """
    Assessment sharing page
    """
    context, organization, assessment = get_assessment_context(request, kwargs, 'my_assessment_sharing')
    AUDITOR_SLUGS = (
        'cuc',
        'isacert',
        'nsf',
        'sgs-nederland',
    )
    # All and only auditors considered partners
    partners = Organization.objects.filter(slug__in=AUDITOR_SLUGS)
    partner_uuids = partners.values_list('uuid', flat=True)
    # excluding partners that already have an access permission to this assessment
    context['new_partners'] = partners.exclude(incoming_assessment_shares__assessment=assessment)
    context["sharing_choices"] = [
        # ("results", _("Only results")),
        ("answers", _("All answers and results")),
        ("NONE", _("No access"))
    ]
    context["assessment_share_infos"] = []
    shares = assessment.shares.all()
    context['shares_count'] = assessment.shares.count()
    if shares.exists():
        context['sharing'] = True
    for share in shares:
        assessment_share_info = {
            "share": share
        }
        share_permissions = share.share_permissions.values_list("permission_name", flat=True)
        if "answers" in share_permissions:
            assessment_share_info["access_level"] = "answers"
        elif "results" in share_permissions:
            assessment_share_info["access_level"] = "results"
        context["assessment_share_infos"].append(assessment_share_info)

    context['text_blocks'] = get_text_blocks(('assessment_sharing_intro',))
    # readonly
    context["read_only"] = assessment.state == 'closed'

    if request.method == "POST":
        # The form contains two things:
        # - for each existing partners a field access_level-UUID
        #       with the new permission for that partner org
        # - for a new partner, new_partner_pk and new_access_level,
        #       considered only if new_partner_pk is not NONE or empty
        #
        form_dict = request.POST.dict()

        # Dict like {partner_pk: access_level} that represents the new permissions for this assessment
        access_levels = {}

        # Adds the given org and access level to the access levels dict
        # only if the organization is a partner
        def add_partner(partner_pk, access_level):
            try:
                partner = partners.get(pk=partner_pk)
                access_levels[partner.uuid] = access_level
            except Organization.DoesNotExist:
                print "WARNING: Trying to grant access to unknown organization."

        # Existing partners, only change access level
        for key, value in form_dict.iteritems():
            key_parts = key.split('-')
            if key_parts[0] == 'access_level':
                partner_pk = key_parts[1]
                add_partner(partner_pk, value)

        # New partner (last row)
        partner_pk = form_dict['new_partner_pk']
        if partner_pk and partner_pk != 'NONE':
            add_partner(form_dict['new_partner_pk'], 'answers')

        update_assessment_sharing(assessment, access_levels)
        assessment.state = 'closed'
        assessment.save()
        messages.success(request, _("Saved."))
        return redirect("my_assessment_finished", request.membership.pk, assessment.pk)
    else:
        # GET
        return render(request, "assessments/assessment_sharing.html", context)


@login_required
@membership_required
def assessment_report_view(request, *args, **kwargs):
    context, organization, assessment = get_assessment_context(request, kwargs, 'my_assessment_report')
    return render(request, "assessments/assessment_report.html", context)


@login_required
@membership_required
def assessment_reopen(request, *args, **kwargs):
    """
    Reopen assessment
    """
    context, _, _ = get_assessment_context(request, kwargs, 'my_assessment_sharing')
    context['intro_text_block'] = get_text_blocks(['assessment reopen']).get('assessment reopen', '')
    return render(request, "assessments/assessment_reopen.html", context)


@login_required
@membership_required
def assessment_finished(request, *args, **kwargs):
    """
    Last assessment page
    """
    context, _, _ = get_assessment_context(request, kwargs, 'my_assessment_sharing')
    return render(request, "assessments/assessment_finished.html", context)


def update_assessment_sharing(assessment, access_levels):
    """
    Deletes all shares of assessment and recreates them based on access_levels dict
    Expected access_levels: {partner_pk: access_level [results|answers]}
    """
    # Delete all shares of this assessment
    for share in assessment.shares.all():
        share.delete()
    # Create shares based on access_levels dict
    for (partner_pk, access_level) in access_levels.items():
        if access_level not in (x[0] for x in AssessmentSharePermission.PERMISSION_NAME_CHOICES):
            break
        # Find partner by PK
        try:
            # Accepting any organization, even non-partners
            # For initial version only auditors are available, and partners module is not in use.
            partner = Organization.objects.get(pk=partner_pk)
            # partner = assessment.organization.partners.get(pk=partner_pk)
            share = AssessmentShare(
                assessment=assessment,
                partner=partner
            )
            share.save()
            if access_level == "answers":
                share.share_permissions.add(AssessmentSharePermission(
                    permission_name="answers"
                ))
                share.share_permissions.add(AssessmentSharePermission(
                    permission_name="results"
                ))
            elif access_level == "results":
                share.share_permissions.add(AssessmentSharePermission(
                    permission_name="results"
                ))
            share.save()
        except Exception as ex:  # TODO reduce to Django not found exception
            logger.warning(ex)


def csv_export_answers(assessment, outfile):
    writer = csv.DictWriter(outfile, ['question_code', 'heading',
                                      'question', 'answer'])
    writer.writeheader()
    for questionnaire in assessment.assessment_type.questionnaires.all():
        data = questionnaire.get_detail(assessment)
        for heading in data['headings'].itervalues():
            for question in heading['questions']:
                writer.writerow({
                    'heading': (heading['heading_name'] or u'').encode('utf8'),
                    'question': (question['question_text'] or u'').encode('utf8'),
                    'question_code': (question['question_code'] or u'').encode('utf8'),
                    'answer': (question['answer_value'] or u'').encode('utf8'),
                })


@login_required
@membership_required
def assessment_logs(request, *args, **kwargs):
    """
    Api view for retrieving list of assessment logs against the provided workflow context
    """
    try:
        context, __ = get_assessments_context(request, kwargs, page_name='assessment_logs')
        wf_ctx = AssessmentWorkflow.objects.get(assessment=kwargs["assessment_pk"])
    except AssessmentWorkflow.DoesNotExist:
        raise Http404()

    else:
        logs_type = kwargs.get("logs_type", "assessment")
        if logs_type == "assessment":
            logs = AssessmentMutationLog.objects.filter(
                assessment_workflow=wf_ctx.pk,
                mutation_type=AssessmentMutationLog.ASSESSMENT_MUTATION
            ).order_by('question__questionnaire__order_index', 'question__order_index', '-mutation_date', 'priority')
        else:
            logs = AssessmentMutationLog.objects.filter(
                assessment_workflow=wf_ctx.pk,
                mutation_type=AssessmentMutationLog.JUDGEMENT_MUTATION
            ).order_by('question__questionnaire__order_index', 'question__order_index', '-mutation_date', 'priority')

        paginator = Paginator(logs, PAGINATION_ITEMS_PER_PAGE)  # Show 20 contacts per page

        page = request.GET.get('page')
        try:
            assessment_logs = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            assessment_logs = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            assessment_logs = paginator.page(paginator.num_pages)

        context['assessment_logs'] = assessment_logs
        context['question_header'] = _("Question / Section") if logs_type == "assessment" else _("Question")
        context['mutation_header'] = _("Mutation") if logs_type == "assessment" else _("Change")
        context['mutation_by_header'] = _("Mutation by") if logs_type == "assessment" else _("Modified by")
        context['table_header'] = _("Assessment logs") if logs_type == "assessment" else _("Judgement logs")

        return render(request, "assessments/assessment_logs.html", context)
