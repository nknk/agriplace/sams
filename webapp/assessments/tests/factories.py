from datetime import datetime

import factory
from factory import DjangoModelFactory, SubFactory
from factory import Iterator

from assessment_types.tests.factories import AssessmentTypeFactory
from assessments.models import (
    Assessment, Judgement, Question, QuestionLevel, PossibleAnswerSet, Questionnaire,
    QuestionnaireHeading, AssessmentDocumentLink, Answer, DynamicLabelConfiguration, QuestionLabelConfigurations,
    QuestionLabel, QuestionEvidenceConfigurations, DynamicEvidenceConfiguration, QuestionsMappingConfiguration,
    QuestionMappings)
from attachments.tests.factories import AttachmentFactory, DocumentTypeFactory
from organization.tests.factories import OrganizationFactory, UserFactory


class AssessmentFactory(DjangoModelFactory):
    organization = SubFactory(OrganizationFactory)
    name = factory.sequence(lambda n: "Assessment {}".format(n))

    assessment_type = SubFactory(AssessmentTypeFactory)
    created_by = SubFactory(UserFactory)
    modified_time = datetime(2016, 1, 1)
    is_test = False
    created_time = datetime(2015, 1, 1)
    state = "open"
    remarks = ""
    last_modified_by = SubFactory(UserFactory)

    class Meta:
        model = Assessment

    @factory.post_generation
    def products(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for product in extracted:
                self.products.add(product)

    @factory.post_generation
    def documents(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for document in extracted:
                self.documents.add(document)


class PossibleAnswerSetFactory(DjangoModelFactory):
    code = factory.sequence(lambda n: "EV{}-PAS".format(n))
    name = factory.sequence(lambda n: "Name {}".format(n))
    assessment_type = SubFactory(AssessmentTypeFactory)
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    remarks = factory.sequence(lambda n: "Remarks {}".format(n))

    class Meta:
        model = PossibleAnswerSet


class QuestionLevelFactory(DjangoModelFactory):
    code = factory.sequence(lambda n: "EV{}".format(n))
    color = factory.sequence(lambda n: "#FFFFF{}".format(n))
    assessment_type = SubFactory(AssessmentTypeFactory)
    title = factory.sequence(lambda n: "Title {}".format(n))
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    order_index = factory.sequence(lambda n: n)
    requires_attachment = False
    remarks = factory.sequence(lambda n: "Remarks {}".format(n))
    requires_justification = False

    class Meta:
        model = QuestionLevel


class QuestionnaireFactory(DjangoModelFactory):
    code = factory.sequence(lambda n: "AF{}-QF".format(n))
    assessment_type = SubFactory(AssessmentTypeFactory)
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    order_index = factory.sequence(lambda n: n)
    available_languages = Iterator(["en,nl"])
    remarks = factory.sequence(lambda n: "Remarks {}".format(n))
    requires_signature = False
    name = factory.sequence(lambda n: "Title {}".format(n))
    intro_text = factory.sequence(lambda n: "Intro text {}".format(n))
    unpublished = False

    class Meta:
        model = Questionnaire


class QuestionnaireHeadingFactory(DjangoModelFactory):
    code = factory.sequence(lambda n: "AF{}-QH".format(n))
    guidance = factory.sequence(lambda n: "Guidance {}".format(n))
    element_type = Iterator(['heading', 'paragraph', 'question'])
    parent_element = SubFactory('assessments.tests.factories.QuestionnaireHeadingFactory')
    modified_time = datetime.now()
    is_test = False
    created_time = datetime.now()
    order_index = factory.sequence(lambda n: n)
    is_initially_visible = True
    text = factory.sequence(lambda n: "Text {}".format(n))
    remarks = factory.sequence(lambda n: "Remarks {}".format(n))
    questionnaire = SubFactory(QuestionnaireFactory)

    class Meta:
        model = QuestionnaireHeading


class QuestionFactory(DjangoModelFactory):
    criteria = factory.sequence(lambda n: "Criteria {}".format(n))
    covered_by_evidence = False
    target_type = None
    initially_visible = True
    answer_type_data = factory.sequence(lambda n: "Answer type data {}".format(n))
    hide_when = ""
    answer_type = Iterator(["radio_buttons"])
    scope = None
    level_object = SubFactory(QuestionLevelFactory)
    justification_placeholder = factory.sequence(lambda n: "Justification placeholder {}".format(n))
    is_justification_visible = True
    is_statistics_valuable = True
    answer_when_attachments = Iterator(["yes", "no"])
    possible_answer_set = SubFactory(PossibleAnswerSetFactory)
    level = factory.sequence(lambda n: "Level {}".format(n))
    framework_p4e = factory.sequence(lambda n: "p4e {}".format(n))
    show_when = ""
    evidence_default_justification = factory.sequence(lambda n: "Default justification {}".format(n))
    evidence_default_answer = Iterator(["yes", "no"])

    class Meta:
        model = Question

    @factory.post_generation
    def document_types_attachments(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for document_types_attachment in extracted:
                self.document_types_attachments.add(document_types_attachment)


class AnswerFactory(DjangoModelFactory):
    justification = factory.sequence(lambda n: "Judgement {}".format(n))
    author = SubFactory(UserFactory)
    question = SubFactory(QuestionFactory)
    value = factory.sequence(lambda n: "Test value {}".format(n))
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()
    remarks = factory.sequence(lambda n: "Remarks {}".format(n))
    assessment = SubFactory(AssessmentFactory)

    class Meta:
        model = Answer


class JudgementFactory(DjangoModelFactory):
    justification = factory.sequence(lambda n: "Judgement {}".format(n))
    author = SubFactory(UserFactory)
    question = SubFactory(QuestionFactory)
    value = factory.sequence(lambda n: "Test value {}".format(n))
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()
    remarks = factory.sequence(lambda n: "Remarks {}".format(n))
    assessment = SubFactory(AssessmentFactory)

    class Meta:
        model = Judgement


class AssessmentDocumentLinkFactory(DjangoModelFactory):
    assessment = SubFactory(AssessmentFactory)
    attachment = SubFactory(AttachmentFactory)
    document_type_attachment = SubFactory(DocumentTypeFactory)
    is_done = False
    author = SubFactory(UserFactory)
    timestamp = datetime.now()

    class Meta:
        model = AssessmentDocumentLink


class QuestionLabelFactory(DjangoModelFactory):
    slug = factory.sequence(lambda n: "L-{}".format(n))
    name = factory.sequence(lambda n: "Test Label {}".format(n))
    icon_class = 'fa-test'
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = QuestionLabel


class DynamicLabelConfigurationFactory(DjangoModelFactory):
    name = 'Test DLC'
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = DynamicLabelConfiguration


class QuestionLabelConfigurationsFactory(DjangoModelFactory):
    configuration = SubFactory(DynamicLabelConfiguration)
    question = SubFactory(QuestionFactory)
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = QuestionLabelConfigurations




class DynamicEvidenceConfigurationFactory(DjangoModelFactory):
    is_test = False
    is_default = False
    name = None
    assessment_type = SubFactory(AssessmentTypeFactory)
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = DynamicEvidenceConfiguration


class QuestionEvidenceConfigurationsFactory(DjangoModelFactory):
    configuration = SubFactory(DynamicEvidenceConfigurationFactory)
    question = SubFactory(QuestionFactory)
    document_type = SubFactory(DocumentTypeFactory)
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = QuestionEvidenceConfigurations


class QuestionsMappingConfigurationFactory(DjangoModelFactory):
    is_test = False
    name = None
    source_assessment_type = SubFactory(AssessmentTypeFactory)
    target_assessment_type = SubFactory(AssessmentTypeFactory)
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = QuestionsMappingConfiguration


class QuestionMappingsFactory(DjangoModelFactory):
    configuration = SubFactory(QuestionsMappingConfigurationFactory)
    source_question = SubFactory(QuestionFactory)
    target_question = SubFactory(QuestionFactory)
    is_test = False
    created_time = datetime.now()
    modified_time = datetime.now()

    class Meta:
        model = QuestionMappings


