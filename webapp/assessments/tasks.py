from __future__ import absolute_import
import json
import uuid
import time
from django.core.files import File
import traceback

from celery.task import task
from django.contrib.auth import get_user_model
from celery.utils.log import get_task_logger
from datetime import datetime
from django.core.files.base import ContentFile
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from django.utils import translation

from wkhtmltopdf.utils import render_pdf_from_template

from assessments.models import DynamicEvidenceImport, AssessmentTypeStandard, AssessmentType, \
    Assessment, AssessmentExport as ExportModel, QuestionEvidenceConfigurations, Question, AssessmentPDFExport, \
    BackgroundEvidenceCopy
from assessments.utils import StandardImport, StandardExport, AssessmentExport, DynamicEvidenceConfigurationImport, \
    AssessmentEvidenceCopyUtility
from translations.helpers import tolerant_ugettext

logger = get_task_logger(__name__)


def get_assessment_export_filename(assessment_count):
    now = datetime.now()
    file_name = 'Assessment_{}_{}.json'.format(assessment_count, now.strftime("%d.%m.%y-%H:%M:%S"))
    return file_name


@task()
def standard_import_task(ids, user_id):
    standards = AssessmentTypeStandard.objects.filter(uuid__in=ids)
    standard_import = StandardImport(standards=standards, user_id=user_id)
    standard_import.import_standards()


@task()
def standard_export_task(codes, user_id):
    standards = AssessmentType.objects.filter(code__in=codes)
    standard_export = StandardExport(standards=standards, user_id=user_id)
    standard_export.export_standards()


@task()
def assessment_export_task(ids, assessment_export_id):
    assessment_jsons = []
    standard = ExportModel.objects.get(uuid=assessment_export_id)
    queryset = Assessment.objects.filter(uuid__in=ids)
    try:
        for assessment in queryset.all():
            assessment_json = AssessmentExport(assessment).export_assessments()
            assessment_jsons.append(assessment_json)

        file_name = get_assessment_export_filename(len(ids))
        standard.file.save(file_name, ContentFile(json.dumps(assessment_jsons)))
        standard.status = _("Success")

    except Exception as exp:
        standard.status = _("Error")
        standard.export_detail = str(exp)

    standard.save()


@task()
def assessment_pdf_export_task(assessment_id, assessment_pdf_export_id, language):
    assessment = Assessment.objects.get(pk=assessment_id)
    question_evidence_map = {}

    if assessment.assessment_type.kind == 'assessment':
        assessment_evidences_map = {}

        for evidence_link in assessment.document_links.select_related(
                'attachment', 'document_type_attachment').all():
            assessment_evidences_map.setdefault(evidence_link.document_type_attachment.pk, []).append(
                evidence_link
            )

        workflow_context = assessment.get_workflow_context()
        if workflow_context:
            configuration = workflow_context.get_configuration()
            question_evidence_configurations = QuestionEvidenceConfigurations.objects.filter(
                configuration=configuration
            ).select_related('document_type')

            qec_map = {}
            for qec in question_evidence_configurations:
                qec_map.setdefault(qec.question_id, []).append(qec.document_type)

            for question_id, document_types in qec_map.items():
                for document_type in document_types:
                    question_evidence_map.setdefault(question_id, []).extend(assessment_evidences_map.get(
                        document_type.pk, []
                    ))

    questions = Question.objects.filter(
        questionnaire__assessment_type=assessment.assessment_type
    )
    assessment_answers = assessment.answers.all()
    question_answer_map = {}
    for answer in assessment_answers:
        question_answer_map[answer.question_id] = answer
    for question in questions:
        evidences = {}
        question_evidences = question_evidence_map.get(question.pk, [])
        for question_evidence in question_evidences:
            trans_name = tolerant_ugettext(question_evidence.document_type_attachment.name, given_lang=language)
            name = trans_name or question_evidence.document_type_attachment.name
            code = question_evidence.document_type_attachment.code
            key = u'{} ({})'.format(name, code)
            evidences.setdefault(key, []).append(
                question_evidence.attachment.get_pdf_export_text())
        question.evidences = evidences
        question.answer = question_answer_map.get(question.pk)

    context = {'questions': questions, 'assessment': assessment, 'report_date': datetime.now(), 'language': language}
    cur_language = translation.get_language()
    try:
        translation.activate(language)
        context['question_code'] = translation.ugettext('Question Code')
        context['question_text'] = translation.ugettext('Question Text')
        context['answer'] = translation.ugettext('Answer')
        context['justification'] = translation.ugettext('Justification')
        context['evidences'] = translation.ugettext('Evidences')

        template = get_template('assessments/assessment_pdf.html')
        pdf_return = render_pdf_from_template(
            template, None, None, context
        )
    finally:
        translation.activate(cur_language)

    file_name = "{}.pdf".format(str(uuid.uuid4()))
    file_path = "/tmp/{}".format(file_name)
    target = open(file_path.format(), 'w')
    target.write(pdf_return)
    target.close()

    assessment_pdf_export = AssessmentPDFExport.objects.get(pk=assessment_pdf_export_id)

    with open(file_path) as f:
        assessment_pdf_export.file.save(file_name, File(f))
        assessment_pdf_export.status = _("Success")
        assessment_pdf_export.save()


@task()
def dynamic_evidence_import_task(ids, user_id):
    configuration_files = DynamicEvidenceImport.objects.filter(uuid__in=ids).select_related('configuration')
    dec_import = DynamicEvidenceConfigurationImport(dynamic_configurations_import=configuration_files, user_id=user_id)
    dec_import.import_data()


@task()
def assessment_evidence_copy_task(source_assessment_id, target_assessment_id, user_id):
    startTime = time.time()
    try:
        source_assessment = Assessment.objects.get(pk=source_assessment_id)
        target_assessment = Assessment.objects.get(pk=target_assessment_id)
        user = get_user_model().objects.get(pk=user_id)
        copying = AssessmentEvidenceCopyUtility(source_assessment=source_assessment,
                                                target_assessment=target_assessment,
                                                user=user)
        copying.start()
        BackgroundEvidenceCopy.objects.filter(
            source_assessment=source_assessment, target_assessment=target_assessment, user=user
        ).update(status="done",log="Took {} seconds".format(round(time.time() - startTime)))

    except Exception as e:
        BackgroundEvidenceCopy.objects.filter(
            source_assessment__uuid=source_assessment_id, target_assessment__uuid=target_assessment_id,
            user__pk=user_id
        ).update(
            status="error",
            log=u"{} \n\n{} \n\n---------------------- \nTook {} seconds.".format(
                e.message,
                traceback.format_exc(),
                round(time.time() - startTime)
            )
        )
