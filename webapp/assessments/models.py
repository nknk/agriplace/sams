import logging
from collections import OrderedDict
from uuid import uuid4

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

from assessments.managers import AssessmentManager, AssessmentTypeManager, AssessmentDocumentLinkManager, \
    QuestionnaireManager
from core.constants import EXTERNAL_AUDITOR
from farm_group.mixins import AssessmentMutationLogModelMixin
from lib.colorfield.fields import ColorField
from django.conf import settings
from translations.helpers import tolerant_ugettext

logger = logging.getLogger(__name__)
from datetime import datetime

from itertools import chain
from operator import attrgetter

from ckeditor.fields import RichTextField
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from core.models import UuidModel
from form_builder.models import GenericForm

try:
    from django.apps import apps

    get_model = apps.get_model
except ImportError:
    from django.db.models.loading import get_model

TODAY = datetime.now()

HARVEST_YEAR_CHOICES = []
for y in range(2015, (TODAY.year + 2)):
    HARVEST_YEAR_CHOICES.append((y, y))

TODAY = datetime.now()

ASSESSMENT_STATE_CHOICES = (
    ("initial", _("Initial")),
    ("open", _("Open")),
    ("closed", _("Closed"))
)

PLATFORM_CHOICES = (
    ('agriplace', "Agriplace"),
    ('hzpc', "Hzpc"),
    ('farm_group', "FarmGroup")
)


BACKGROUND_EVIDENCE_COPY_STATUS = (
    ("pending", _("Pending")),
    ("done", _("Done")),
    ("error", _("Error")),
)


class ContentPackage(UuidModel):
    """
    ! NOT USED: Needed for future content versioning

    Content package (data import)
    A content package is an import event consisting of questionnaires, headings, questions, possible answers, etc.
    They are meant to separate content revisions.

    Content packages with the same major version but different minor versions are compatible (in question codes).
    Content packages with different major versions are not 1:1 compatible (in question codes).
    """
    code = models.CharField(max_length=100)  #:
    name = models.CharField(max_length=1000)  #:
    major_version = models.IntegerField(default=1)  #:
    minor_version = models.IntegerField(default=0)  #:

    class Meta:
        db_table = 'ContentPackage'
        ordering = ['code', 'major_version', 'minor_version']


class Category(UuidModel):
    """
    A category in the People 4 Earth framework (e.g. environment, social, etc.)

    ! NOT USED
    """
    name = models.CharField(max_length=1000, default="New category")  #:
    parent_category = models.ForeignKey('self', related_name='children_categories', null=True)  #:
    CATEGORY_CHOICES = (
        ("domain", "Domain"),
        ("subdomain", "Subdomain"),
        ("category1", "Category 1"),
        ("category2", "Category 2")
    )
    category_type = models.CharField(max_length=1000, null=True, choices=CATEGORY_CHOICES)  #:

    class Meta(UuidModel.Meta):
        db_table = 'Category'
        verbose_name_plural = "P4E Categories"
        ordering = ['name']


class QuestionnaireElement(UuidModel):
    """
    Base abstract class for questionnaire elements.

    Questionnaires are made up of multiple Questionnaire Elements: headings, paragraphs, questions, images, etc.

    Elements are displayed in their questionnaire in order, based on their `order_index` field. This does not take
    into account any parent/child relationships.

    Elements should have an unique code to allow easy reference to them for development and support. This code is set and managed manually.

    Parent elements
    ----------------
    An element can have another element as parent. This field is only taken into account when determining whether
    an element is visible in its questionnaire or not. If an element's parent is hidden, then the element will also
    be hidden, regardless of any other factors affecting its visiblity.

    Visibility triggering
    ----------------------
    Not all elements are initially visible. If an element's `initially_visible` field is `False`, then the element will
    only appear if triggered by a question, through a :class:`QuestionnaireTrigger`. Elements that are not visible are
    not taken into account, as if they didn't exist in the questionnaire (their answers, attachments, etc. are ignored,
    and they are not counted towards the questionnaire's totals and status).



    Extended by :class:`QuestionnaireHeading`, :class:`QuestionnaireParagraph`, :class:`Question`, etc.
    """

    content_package = models.ForeignKey(
        'ContentPackage',
        related_name='questionnaire_elements',
        blank=True, null=True)  #:
    is_initially_visible = models.BooleanField("Is initially visible?", default=True)  #:
    parent_element = models.ForeignKey(
        'QuestionnaireElement',
        related_name='child_elements',
        blank=True, null=True)  #:
    questionnaire = models.ForeignKey(
        'Questionnaire', related_name='%(class)ss', blank=True, null=True)  #:
    order_index = models.PositiveSmallIntegerField(default=0)  #:
    code = models.CharField(max_length=100, blank=True)  # ;
    element_type = models.CharField(max_length=100, blank=True)  #:
    text = RichTextField("Text", default='', blank=True)  #:
    guidance = RichTextField("Guidance", default='', blank=True)  #:

    class Meta:
        db_table = 'QuestionnaireElement'
        ordering = ('questionnaire__assessment_type', 'questionnaire', 'order_index')

    def __unicode__(self):
        return u"{} {}".format(self.element_type, self.code)

    def get_simple_code(self):
        assessment_code = self.questionnaire.assessment_type.code
        return self.code.replace(assessment_code, '')


class QuestionnaireHeading(QuestionnaireElement):
    """
    A questionnaire heading element. Corresponds to a HTML <h1>, <h2>, etc. tag.
    """
    level = models.IntegerField(default=1)

    class Meta:
        db_table = 'QuestionnaireHeading'
        permissions = (
            ('view_questionnaireheading', 'Can view questionnaire heading'),
        )

    def __init__(self, *args, **kwargs):
        super(QuestionnaireHeading, self).__init__(*args, **kwargs)
        self.element_type = 'heading'


class QuestionnaireParagraph(QuestionnaireElement):
    """
    A questionnaire paragraph element. Contains rich text.
    """

    class Meta:
        db_table = 'QuestionnaireParagraph'
        permissions = (
            ('view_questionnaireparagraph', 'Can view questionnaire paragraph'),
        )

    def __init__(self, *args, **kwargs):
        super(QuestionnaireParagraph, self).__init__(*args, **kwargs)
        self.element_type = 'paragraph'


class QuestionnaireImage(QuestionnaireElement):
    """
    A questionnaire image element. Styled as a separate-paragraph figure.
    """

    image = models.ImageField(blank=True, null=True)

    class Meta:
        db_table = 'QuestionnaireImage'

    def __init__(self, *args, **kwargs):
        super(QuestionnaireImage, self).__init__(*args, **kwargs)
        self.element_type = 'image'


class Question(QuestionnaireElement):
    """
    A question in a questionnaire.

    Questions can have different answer types. Each corresponds to a differnet editor shown in the questionnaire.

    Default answers from attachments
    ---------------
    Questions can be associated with document types (:class:`DocumentType`) if they can receive an attached document of
    that type. If a question can be answerd simply by attaching a document, without further input, then it will have
    values in the `evidence_default_answer` and/or `evidence_default_justification` fields. These values for answer and
    justification will be automatically set if any active ('done') attachment is added to any of the accepted document types
    of this question.
    """
    criteria = models.TextField(
        verbose_name="Criteria",
        default='',
        blank=True
    )
    ANSWER_TYPE_CHOICES = (
        ('dropdown', "Single choice dropdown"),
        ('list_single', "Single choice list"),
        ('memo', "Memo field"),
        ('checkboxes', "Multiple choice checkboxes"),
        ('numeric', "Numeric field"),
        ('radio', "Single choice radio paragraphs"),
        ('single_choice_buttons', "Single choice buttons"),
        ('scale', "Scale field"),
        ('text', "Text field"),
        ('context', "Context question"),
        ('yesno', "Yes/No question"),
        ('yesnona', "Yes/No/NA question")
    )
    answer_type = models.CharField(
        max_length=1004, choices=ANSWER_TYPE_CHOICES, default='text'
    )
    answer_type_data = models.CharField(max_length=1004, blank=True, null=True)
    level = models.CharField(
        max_length=255, verbose_name="Level", blank=True
    )
    level_object = models.ForeignKey(
        'QuestionLevel', verbose_name='Level object',
        null=True, blank=True, related_name='questions'
    )

    SCOPE_CHOICES = (
        ('organization', "Organization"),
        ('organization_unit', "Organization unit"),
        ('product', "Product"),
        ('user', "User")
    )
    target_type = models.CharField(
        max_length=1000, choices=SCOPE_CHOICES, blank=True, null=True
    )
    initially_visible = models.BooleanField("Is initially visible?", default=True)
    is_statistics_valuable = models.BooleanField("Is statistics valuable", default=True)

    # next 3 properties - scope, show_when, hide_when - are not using any more
    scope = models.CharField(
        max_length=1000, choices=SCOPE_CHOICES, blank=True, null=True
    )
    show_when = models.CharField("Show conditions", max_length=1000, default='', blank=True)
    hide_when = models.CharField("Hide conditions", max_length=1000, default='', blank=True)

    possible_answer_set = models.ForeignKey('PossibleAnswerSet', related_name='questions', blank=True, null=True)

    document_types_attachments = models.ManyToManyField(
        'attachments.DocumentType', related_name='questions_attachments', blank=True)

    covered_by_evidence = models.BooleanField(default=False)  # TODO delete later. see AP-890
    evidence_default_answer = models.CharField(
        max_length=1000, default='', blank=True)
    evidence_default_justification = models.TextField(
        default='', blank=True)
    framework_p4e = models.CharField(max_length=1000, default='', blank=True)
    is_justification_visible = models.BooleanField(default=False)
    justification_placeholder = models.CharField("Justification placeholder", max_length=1000, default='', blank=True)
    # Answer value when any documents are attached
    # (question is answered by attachment alone)
    answer_when_attachments = models.CharField(
        max_length=1000, blank=True, null=True)

    class Meta:
        db_table = 'Question'
        ordering = ['questionnaire', 'order_index', 'code']
        permissions = (
            ('view_question', 'Can view question'),
        )

    def __init__(self, *args, **kwargs):
        super(Question, self).__init__(*args, **kwargs)
        self.element_type = 'question'

    def get_code(self):
        if self.code:
            return self.code
        else:
            return "#%s" % self.order_index

    def get_data(self):
        return {
            'code': self.code,
            'text': self.text,
            'help_text': self.criteria,
        }

    def __unicode__(self):
        # return "{} {}".format(self.uuid, self.get_code())
        # return "{}".format(self.get_code())
        return self.get_code()

    # For grappelli-filters
    @staticmethod
    def autocomplete_search_fields():
        return ('code__iexact',)

    def save(self, *args, **kwargs):
        original_answer_type = self.answer_type
        # Always save first to commit question object
        obj = super(Question, self).save(*args, **kwargs)

        # A few special question kinds check and create associated PossibleAnswer instances
        # possible_answers = self.possible_answers.all()
        if original_answer_type in ('yesno', 'yesnona'):
            # Check or create Yes possible answer
            pa, created = PossibleAnswer.objects.get_or_create(question=self, value='yes', text="Yes")
            pa, created = PossibleAnswer.objects.get_or_create(question=self, value='no', text="No")
            self.answer_type = 'radio'
            if original_answer_type == 'yesnona':
                pa, created = PossibleAnswer.objects.get_or_create(question=self, value='na', text="Not Applicable")
            # Save again to change answer_type
            obj = super(Question, self).save(*args, **kwargs)

        return obj


# TODO looks like this model not used anymore
class QuestionnaireElementState(UuidModel):
    """
    QuestionnaireElement state in assessment.
    """
    questionnaire_element = models.ForeignKey('QuestionnaireElement', verbose_name="Element",
                                              related_name='questionnaire_element_states')
    assessment = models.ForeignKey('Assessment', verbose_name="Assessment", related_name='questionnaire_element_states')
    is_visible = models.BooleanField("Is visible?", default=True)

    class Meta:
        db_table = 'QuestionnaireElementState'

    def __unicode__(self):
        return u"assessment [{self.assessment}] element [{self.questionnaire_element}] is [{self.is_visible}]".format(
            self=self)


class QuestionTrigger(UuidModel):
    # when these conditions are met
    # conditions: FK from QuestionTriggerCondition
    # set destination question
    target_question = models.ForeignKey('Question', related_name='incoming_triggers_question',
                                        blank=True, null=True)
    target_element = models.ForeignKey('QuestionnaireElement', related_name='incoming_triggers',
                                       blank=True, null=True)
    # to this value
    target_value = models.CharField("Target value", max_length=1000, default='', blank=True, null=True)
    # and this justification
    target_justification = models.TextField("Target justification", blank=True, null=True)
    # and this visibility status
    target_visibility = models.NullBooleanField("Target visibility", default=None, blank=True, null=True)

    class Meta:
        db_table = 'QuestionTrigger'
        ordering = ('uuid',)
        permissions = (
            ('view_questiontrigger', 'Can view question trigger'),
        )

    def __unicode__(self):
        return u'{} trigger'.format(getattr(self, 'target_question', ""))


class QuestionTriggerCondition(UuidModel):
    # parent trigger
    trigger = models.ForeignKey('QuestionTrigger', related_name='conditions')

    # source question
    source_question = models.ForeignKey('Question', related_name='outgoing_triggers')
    # or source property (in active property collection)
    SOURCE_PROPERTY_CHOICES = (
        ('value', "Value"),
        ('justification', "Justification"),
        ('visible', "Visibility")
    )
    source_property_name = models.CharField(max_length=1000, default='value', choices=SOURCE_PROPERTY_CHOICES)
    # when it is equal to this
    source_value = models.CharField(max_length=100, default='', blank=True)

    class Meta:
        db_table = 'QuestionTriggerCondition'
        ordering = ('source_question', 'uuid',)
        permissions = (
            ('view_questiontriggercondition', 'Can view question trigger condition'),
        )

    def __unicode__(self):
        return u"{t} v={value}".format(t=getattr(self, 'trigger', ""), value=self.source_value)

    @staticmethod
    def autocomplete_search_fields():
        return ("uuid__iexact",)


class QuestionLevel(UuidModel):
    assessment_type = models.ForeignKey('assessments.AssessmentType', related_name='question_levels', null=True,
                                        blank=True)
    order_index = models.PositiveSmallIntegerField(default=0)
    code = models.CharField(max_length=100, default='')
    title = models.CharField("Title", max_length=100, default='')
    # color = models.CharField(max_length=20, default='#ffffff')
    color = ColorField(default='#ffffff')
    # Do questions of this level require a justification to be complete?
    requires_justification = models.BooleanField("Requires justification?", default=False, blank=False)
    # Do questions of this level require an attachment to be complete?
    requires_attachment = models.BooleanField("Requires attachment", default=False, blank=False)

    class Meta:
        db_table = 'QuestionLevel'
        ordering = ('order_index', 'uuid')
        permissions = (
            ('view_questionlevel', 'Can view question level'),
        )

    def __unicode__(self):
        return u"{} {} {}".format(self.order_index, self.assessment_type, self.code)


class PossibleAnswer(UuidModel):
    """
    Possible answer for multiple-choice questions
    """
    content_package = models.ForeignKey('ContentPackage', related_name='possible_answers', blank=True, null=True)
    possible_answer_set = models.ForeignKey('PossibleAnswerSet', related_name='possible_answers', blank=True, null=True)
    question = models.ForeignKey('Question', related_name="possible_answers", blank=True, null=True)
    text = models.CharField("Label", max_length=1000, blank=True, null=True)
    value = models.CharField("Value", max_length=1000, blank=True)
    code = models.CharField("Code", max_length=255, blank=True, null=True)
    ANSWER_TYPE_CHOICES = (
        ('text', "Text"),
        ('integer', "Integer"),
        ('decimal', "Decimal"),
        ('other', "Other")
    )
    answer_type = models.CharField("Answer type", max_length=1000, choices=ANSWER_TYPE_CHOICES, default='text',
                                   blank=True)
    default = models.BooleanField(default=False)
    triggered_questions = models.ManyToManyField('Question', related_name='trigger_answers', blank=True)
    order_index = models.PositiveSmallIntegerField(default=0)
    background_color = models.CharField("Background colour", max_length=10, default='', blank=True)
    requires_justification = models.BooleanField("Requires justification?", default=False, blank=False)
    requires_attachment = models.BooleanField("Requires attachment", default=False, blank=False)

    class Meta:
        db_table = 'PossibleAnswer'
        ordering = ['possible_answer_set', 'order_index']
        permissions = (
            ('view_possibleanswer', 'Can view possible answer'),
        )

    def __unicode__(self):
        if self.possible_answer_set:
            return u"%s / %s" % (self.possible_answer_set.name, self.text)
        else:
            return u"%s / %s" % ("Not connected to PAS PA", self.text)


class PossibleAnswerSet(UuidModel):
    """
    Set of possible answers
    """
    # possible_answers: fk PossibleAnswer
    # question: fk Question
    assessment_type = models.ForeignKey('AssessmentType', related_name='possible_answer_sets', blank=True, null=True)
    code = models.CharField(max_length=255, default='', blank=True)
    name = models.CharField(max_length=1000, default="New possible answer set")

    class Meta:
        db_table = 'PossibleAnswerSet'
        permissions = (
            ('view_possibleanswerset', 'Can view possible answer set'),
        )


class Questionnaire(UuidModel):
    """
    A Questionnaire is an ordered list of QuestionnaireElements.


    """
    content_package = models.ForeignKey('ContentPackage', related_name='questionnaires', blank=True, null=True)
    assessment_type = models.ForeignKey('AssessmentType', related_name='questionnaires', blank=True, null=True)
    order_index = models.PositiveSmallIntegerField("Order index", default=10)
    name = models.CharField("Name", max_length=1004, blank=True)
    code = models.CharField("Code", max_length=255, blank=True)
    intro_text = models.TextField("Intro text", blank=True)
    available_languages = models.TextField("Available languages", blank=True, null=False, default='en')
    requires_signature = models.BooleanField('Requires signature?', default=False)
    requires_reference = models.BooleanField('Requires reference?', default=False)
    completeness_check = models.BooleanField('Is required for completeness check?', default=False)
    help_document = models.FileField(max_length=255, upload_to="help_documents", blank=True, null=True)
    unpublished = models.BooleanField('Unpublished', default=False)

    # fk: elements: QuestionnaireElement

    objects = QuestionnaireManager()

    class Meta(UuidModel.Meta):
        db_table = 'Questionnaire'
        ordering = ('assessment_type', 'order_index')
        permissions = (
            ('view_questionnaire', 'Can view questionnaire'),
        )

    def __unicode__(self):
        return u"%s" % (self.name or self.code or self.uuid,)

    def get_help_document_url(self):
        return self.help_document.url if self.help_document else ""

    @property
    def questions(self):
        return Question.objects.filter(questionnaire=self)

    @property
    def headings(self):
        return QuestionnaireHeading.objects.filter(questionnaire=self)

    @property
    def paragraphs(self):
        return QuestionnaireParagraph.objects.filter(questionnaire=self)

    @property
    def element_objects(self):
        """
        Gathers elements as subclass instances (actual Question, QuestionnaireHeading, etc.)
        in a list indexed by order_index
        """
        headings = QuestionnaireHeading.objects.filter(questionnaire=self)
        questions = Question.objects.filter(questionnaire=self).prefetch_related('possible_answers')
        # put QuerySets together and render them to a list
        result = list(chain(headings, questions))
        result.sort(key=attrgetter('order_index'))
        return result

    @property
    def specific_questions(self):
        # subclasses should override this property to return a queryset
        # that fetches models of their specific Question subclass
        return self.questions

    def check_permissions(self, user, action):
        return user in self.organization.users.all()

    def get_progress(self, assessment):
        """
        Returns progress indication for given assessment
        """
        answered = len(set(
            r['question']
            for r in assessment.answers
                .filter(question__questionnaire=self)
                .values('question')))
        total = self.questions.count()
        return "{answered}/{total}".format(answered=answered, total=total)

    def get_state(self, assessment):
        """
        Returns current state of questionnaire (done/not done)
        """
        try:
            state_obj = self.questionnaire_states.get(assessment=assessment)
            if state_obj.is_complete:
                state = 'complete'
            else:
                state = 'incomplete'
        except QuestionnaireState.DoesNotExist:
            state = 'incomplete'
        return state

    @property
    def answers(self):
        """
        Retrieves all answers ever to questions in this questionnaire
        """
        query = Answer.objects.filter(question__questionnaire=self)
        return query

    def get_answers(self, assessment=None):
        """
        Returns all answers to this questionnaire, cross filtered by assessment if provided
        :return: List of Answer objects
        """
        query = Answer.objects.filter(question__questionnaire=self)
        if assessment is not None:
            query = query.filter(assessment=assessment)
        return query.all()

    def get_detail(self, assessment=None):
        """
        Generate questionnaire question/answer detail dict.
        The detail provides two dicts:

        `questions`
            A dict from question_code to question+answer data. If
            `assessment` is provided, question-data includes answer
            data.

        `headings`
            A list of headings, each with a list of question codes
        """
        result = {
            "headings": OrderedDict(),
            "questions": OrderedDict()
        }

        # Default heading holds questions without an actual heading
        result["headings"]["default"] = {
            "code": None,
            "heading_object": None,
            "heading_name": None,
            "questions": [],
            "question_codes": [],
        }
        for heading in self.headings.all():
            result["headings"][heading.pk] = {
                "code": heading.code,
                "heading_object": heading,
                "heading_name": heading.code,
                "questions": [],
                "question_codes": [],
            }

        for question in (self.specific_questions
                                 .prefetch_related('possible_answers')
                                 .order_by("order_index")):
            question_item = {
                "question_object": question,
                "answer_object": None,
                "answer_value": None
            }
            question_data = question.get_data()
            question_item.update({'question_' + k: question_data[k]
                                  for k in question_data})

            # Add question to the heading item's question collection
            # TODO: This causes one query / question. Rewrite to avoid that.
            heading = question.parent_element
            heading_id = 'default' if heading is None else heading.pk
            heading_data = result['headings'][heading_id]
            heading_data["questions"].append(question_item)
            heading_data["question_codes"].append(question.code)

            # Add question to the mixed question dict
            result["questions"][question.code] = question_item

        return result


class AssessmentType(UuidModel):
    """
    Assessment type
    """

    objects = AssessmentTypeManager()

    kind = models.CharField(max_length=10,
                            choices=(
                                ('assessment', 'assessment'),
                                ('agriform', 'agriform'),
                                ('agriform2', 'agriform2')),
                            default="assessment"
                            )
    name = models.CharField("Name", max_length=1000, default="New assessment type")
    description = RichTextField("Description", default='', blank=True)
    code = models.CharField(max_length=1000, default='', blank=True)
    edition = models.IntegerField(default=1)
    has_product_selection = models.BooleanField(default=True)
    is_public = models.BooleanField(default=True)
    parent_assessment_type = models.ForeignKey('self', related_name='child_assessment_types',
                                               blank=True, null=True)
    has_logo = models.BooleanField(default=True)
    ASSESSMENT_SECTIONS_CHOICES = (
        ('overview', "Overview"),
        ('evidence', "Evidence"),
        ('questionnaires', "Questionnaires"),
        ('results', "Results"),
        ('sharing', "Sharing")
    )
    assessment_sections = models.TextField(default='overview,evidence,questionnaires,results,sharing', blank=True)
    available_languages = models.CharField(default=",".join((x[0] for x in settings.LANGUAGES)), max_length=255)
    logo_small = models.ImageField(_("logo small"), upload_to='assessment-type-logos', blank=True, null=True)
    logo_large = models.ImageField(_("logo large"), upload_to='assessment-type-logos', blank=True, null=True)
    sharing_conditions = models.TextField(default="", blank=True)

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentType'
        ordering = ('code',)
        permissions = (
            ('view_assessmenttype', 'Can view assessment type'),
        )

    def __unicode__(self):
        return self.code

    def get_document_types(self):
        return get_model('attachments', 'DocumentType').objects.filter(
            questions_attachments__questionnaire__assessment_type__pk=self.uuid).distinct()

    def get_triggers(self):
        return QuestionTrigger.objects.filter(
            target_element__questionnaire__assessment_type__pk=self.uuid).prefetch_related('conditions')

    def get_assessment_type_section_list(self, platform_name, role):
        try:
            section_list_object = AssessmentTypeSection.objects.get(assessment_type=self, platform=platform_name)
            section_list = section_list_object.sections_list
        except ObjectDoesNotExist:
            section_list = self.assessment_sections if self.assessment_sections else ""
        section_list = section_list.replace(' ', '').encode('utf8').split(',')
        if role == EXTERNAL_AUDITOR and 'sharing' in section_list:
            section_list.remove('sharing')
        return section_list


class AssessmentTypeSection(UuidModel):
    platform = models.CharField(choices=PLATFORM_CHOICES, max_length=50,
                                default='agriplace')
    assessment_type = models.ForeignKey(
        AssessmentType,
        related_name="assessment_type_section_list",
        limit_choices_to={'kind': 'assessment'}
    )
    sections_list = models.TextField(default='overview,evidence,questionnaires,sharing', blank=True)

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentTypeSection'
        unique_together = ("platform", "assessment_type")
        permissions = (
            ('view_assessmenttypesection', 'Can view assessment type section'),
        )

    def __unicode__(self):
        return u"{} ({})".format(getattr(self, 'assessment_type', ""), self.platform)


class Answer(UuidModel, AssessmentMutationLogModelMixin):
    """
    Answer to Question in Questionnaire
    """
    question = models.ForeignKey('Question', related_name="answers")
    value = models.CharField(max_length=1024, blank=True, null=True)
    value_uuids = models.CharField(max_length=1024, blank=True, null=True)
    value_old = models.CharField(max_length=1024, blank=True, null=True)
    justification = models.TextField(verbose_name="Justification", blank=True)
    assessment = models.ForeignKey("Assessment", related_name="answers", blank=True, null=True)
    target_uuid = models.CharField(max_length=36, blank=True, null=True)

    # Possible targets
    organization = models.ForeignKey(
        'organization.Organization',
        related_name='answers',
        blank=True, null=True
    )
    organization_unit = models.ForeignKey(
        'organization.OrganizationUnit',
        related_name='answers',
        blank=True, null=True
    )
    product = models.ForeignKey(
        'organization.Product',
        related_name='answers',
        blank=True, null=True
    )

    # Meta
    timestamp = models.DateTimeField(null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)

    def __init__(self, *args, **kwargs):
        super(Answer, self).__init__(*args, **kwargs)
        self.modified_by_user = None
        self.modified_by_role = None

    def __unicode__(self):
        # Careful: Generates one query per object!
        return u"Q:<{question}> Asm:<{assessment}> = {value}".decode().format(
            question=(self.question.code or '?'),
            assessment=self.assessment,
            value=(self.value or '?'),
        )

    @property
    def target(self):
        """
        Target object of answer, depending on which field is filled in
        Can be Organization, Organization Unit, Product

        Returns:
            (type, object)
        """
        if self.organization:
            return self.organization
        if self.organization_unit:
            return self.organization_unit
        if self.product:
            return self.product

    def save(self, *args, **kwargs):
        # Ensure unique answer for question+assessment combination
        # by deleting other instances
        queryset = Answer.objects.filter(question=self.question, assessment=self.assessment)
        for answer in queryset:
            answer.modified_by_user = self.modified_by_user
            answer.modified_by_role = self.modified_by_role
            answer.new_answer = self
            answer.delete()
        self.prev_answers = queryset
        super(Answer, self).save(*args, **kwargs)

    class Meta(UuidModel.Meta):
        db_table = 'Answer'

    # Add attachment: check that document belongs to same organization as answer
    def add_attachment(self, document):
        """
        Adds document to attachments checking that it belongs to same organization.
        Does not save instance.
        """
        if document.organization != self.organization:
            raise Exception("Cannot add document that belongs to another organization.")
        self.attachments.add(document)


class Judgement(UuidModel, AssessmentMutationLogModelMixin):
    """
    Judgement to Question in Questionnaire
    """
    question = models.ForeignKey('Question', related_name="judgements")
    value = models.CharField(max_length=1024, blank=True, null=True)
    justification = models.TextField(verbose_name="Justification", blank=True)

    evaluation_is_solved = models.BooleanField(default=False)
    evaluation_remarks = models.TextField(blank=True, null=True)

    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    assessment = models.ForeignKey("Assessment", related_name="judgements", blank=True, null=True)

    def __unicode__(self):
        return u"Q:<{question}> Asm:<{assessment}> = {value}".decode().format(
            question=(self.question.code or '?'),
            assessment=self.assessment,
            value=(self.value or '?'),
        )

    def __init__(self, *args, **kwargs):
        super(Judgement, self).__init__(*args, **kwargs)
        self.modified_by_user = None
        self.modified_by_role = None

    class Meta(UuidModel.Meta):
        db_table = 'Judgement'
        unique_together = ('assessment', 'question')
        permissions = (
            ('view_judgement', 'Can view judgement'),
        )


class Assessment(UuidModel):
    """
    Assessment instance
    """
    name = models.CharField(max_length=1024, blank=True, null=True)
    assessment_type = models.ForeignKey('AssessmentType', related_name="assessments")
    organization = models.ForeignKey('organization.Organization',
                                     related_name="assessments",
                                     blank=True, null=True)
    products = models.ManyToManyField('organization.Product',
                                      related_name='assessments',
                                      blank=True)
    attachment_list = models.ManyToManyField('attachments.Attachment', through='AssessmentDocumentLink',
                                             blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='assessments_started',
                                   blank=True, null=True)
    last_modified_by = models.ForeignKey(settings.AUTH_USER_MODEL,
                                         related_name='assessments_last_modified',
                                         blank=True, null=True)
    state = models.CharField(max_length=10, choices=ASSESSMENT_STATE_CHOICES, default='open')

    objects = AssessmentManager()

    class Meta(UuidModel.Meta):
        db_table = 'Assessment'
        ordering = ('created_time',)
        permissions = (
            ('view_assessment', 'Can view assessment'),
        )

    @property
    def target(self):
        return self.product or self.organization_unit or self.organization

    @property
    def assessment_state(self):
        try:
            workflow_context_model = get_model('assessment_workflow.AssessmentWorkflow')
            workflow_context_instance = workflow_context_model.objects.get(assessment=self)
            if workflow_context_instance.assessment_status \
                    and workflow_context_instance.selected_workflow != 'agriplace':
                return workflow_context_instance.get_assessment_status_display()
            else:
                return self.state
        except ObjectDoesNotExist:
            return self.state

    @assessment_state.setter
    def assessment_state(self, value):
        self.state = value

    def make_name(self):
        name = u"%(type)s / %(created_time)s" % {
            "type": tolerant_ugettext(self.assessment_type.name),
            "created_time": self.created_time.strftime("%Y-%m-%d")
        }
        # Add unique index if name already in use
        other_names = Assessment.objects.filter(name__startswith=name).values_list("name", flat=True)
        if name in other_names:
            # Name already in use, append first available index
            done = False
            index = 1
            while not done:
                name_try = "%s-%s" % (name, index)
                if name_try not in other_names:
                    name = name_try
                    done = True
                else:
                    index += 1
        return name

    def save(self, *args, **kwargs):
        """
        On create, create unique name.
        """
        # Generate nice name
        super(Assessment, self).save()
        if not self.name:
            self.name = self.make_name()
            self.save()

    def check_permissions(self, user, action):
        if user in self.organization.users.all():
            return True

        if action == 'read':
            for share in self.shares.all():
                if user in share.partner.users.all():
                    return True

        # TODO use RBAC
        try:
            workflow_context_model = get_model('assessment_workflow', 'AssessmentWorkflow')
            if workflow_context_model and workflow_context_model.objects.get(assessment=self.uuid):
                return True
        except ObjectDoesNotExist:
            return False

        return False

    def check_permissions_organization(self, organization, action):
        workflow_context_model = get_model('assessment_workflow', 'AssessmentWorkflow')

        if self.assessment_type.kind == 'agriform':
            agriform_attachment_model = get_model('attachments.AgriformAttachment')
            try:
                agriform_attachments = agriform_attachment_model.objects.filter(assessment=self)
                document_links = AssessmentDocumentLink.objects.filter(attachment__in=agriform_attachments)
                assessment_to_check_list = [document_link.assessment for document_link in document_links]
            except agriform_attachment_model.DoesNotExist:
                return False
        else:
            assessment_to_check_list = [self]
        try:
            if workflow_context_model:
                for assessment_to_check in assessment_to_check_list:
                    if workflow_context_model.objects.get(assessment=assessment_to_check):
                        return True

        except ObjectDoesNotExist:
            if organization == self.organization:
                return True
            if action == 'read':
                for assessment_to_check in assessment_to_check_list:
                    for share in assessment_to_check.shares.all():
                        if organization == share.partner:
                            return True
        return False

    def is_eligible_for_mutation_log(self):
        workflow_context_model = get_model('assessment_workflow', 'AssessmentWorkflow')
        wf_context = workflow_context_model.objects.get(assessment=self)

        if wf_context and wf_context.assessment_status in [
            'internal_audit_done',
            'document_review_done',
            'audit_done',
            'action_required',
            'action_done',
            'reaudit_required',
        ]:
            return True
        else:
            return False

    def get_workflow_context(self):
        workflow_context_model = get_model('assessment_workflow', 'AssessmentWorkflow')

        try:
            return workflow_context_model.objects.select_related('membership').get(assessment=self)
        except ObjectDoesNotExist:
            return None

    def get_all_document_types(self):
        # TODO very bad - rewrite
        question_list = []
        questionnaire_list = self.assessment_type.questionnaires.all()
        for questionnaire in questionnaire_list:
            question_list += questionnaire.questions

        type_list = []
        for q in question_list:
            tlist = q.document_types.all()
            for t in tlist:
                if t not in type_list:
                    type_list.append(t)
        return type_list

    def get_attachment_link_count(self):
        return self.document_links.all().count()

    def get_justification_count(self):
        return self.answers.exclude(Q(justification="") | Q(justification__isnull=True)).distinct().count()

    def get_absolute_url(self):
        wf_context = self.get_workflow_context()
        # This might be due to some data discrepancies because
        # now each assessment should have attached workflow.
        if not wf_context:
            return '#'
        return reverse('my_assessment_detail', args=[wf_context.membership.pk, self.pk])

    def get_membership_absolute_url(self, membership):
        return reverse('my_assessment_detail', args=[membership.pk, self.pk])

    def get_product_list(self):
        result = ', '.join([tolerant_ugettext(p.__unicode__()) for p in self.products.all()])
        return result

    def get_status(self):
        return self.state

    def get_progress(self):
        questions = self.get_all_questions()
        answers = self.get_answers(questions)
        answer_count = answers.exclude(Q(value__exact='') | Q(value__isnull=True)).count()

        return int(float(answer_count) / questions.count() * 100)

    def get_all_questions(self, with_trigger=True):
        questions = Question.objects.filter(
            questionnaire=self.get_visible_questionnaires()
        )
        if not with_trigger:
            questions = questions.filter(
                incoming_triggers_question__isnull=True
            )
        return questions

    def get_question_covered_by_doc_type(self, document_type):
        questions = Question.objects.filter(
            questionnaire=self.assessment_type.questionnaires.all(),
            document_types_attachments=document_type
        ).distinct()
        return questions

    def get_answers(self, questions=None, with_trigger=True):
        if not questions:
            questions = self.get_all_questions(with_trigger)
        answers = Answer.objects.filter(
            question=questions, assessment=self)
        return answers

    def get_questionnaires_detail(self):
        questionnaires_with_completeness_check = set()
        available_questionnaires = set()

        for questionnaire in self.assessment_type.questionnaires.all():
            if hasattr(questionnaire, 'completeness_check') and questionnaire.completeness_check:
                questionnaires_with_completeness_check.add(questionnaire.uuid)

        for product in self.products.all():
            for questionnaire_uuid in product.product_type.questionnaires.published().values_list('uuid', flat=True):
                available_questionnaires.add(questionnaire_uuid)

        return questionnaires_with_completeness_check, available_questionnaires

    def get_visible_questionnaires(self):
        workflow = self.get_workflow_context()

        if workflow:
            product_types = [product.product_type for product in workflow.assessment.products.all()]
            questionnaires = self.assessment_type.questionnaires.filter(
                Q(unpublished=False) & (Q(product_types__isnull=True) | Q(product_types__in=product_types))
            )
            if workflow.selected_workflow != 'hzpc':
                questionnaires = questionnaires.exclude(code__in=settings.HZPC_FIFTH_QUESTIONNAIRE_CODES)
        else:
            questionnaires = self.assessment_type.questionnaires.published()

        return questionnaires


class QuestionnaireState(UuidModel):
    assessment = models.ForeignKey('Assessment', related_name='questionnaire_states')
    questionnaire = models.ForeignKey('Questionnaire', related_name='questionnaire_states')
    is_complete = models.BooleanField('Is complete?', default=False)
    is_signed = models.BooleanField('Is signed?', default=False)

    class Meta(UuidModel.Meta):
        db_table = 'QuestionnaireState'
        permissions = (
            ('view_questionnairestate', 'Can view questionnaire state'),
        )
        unique_together = ('assessment', 'questionnaire')


class AssessmentDocumentLink(models.Model, AssessmentMutationLogModelMixin):
    """
    M2M link model between Assessment and Document.
    Represents document attached to entire assessment.
    """
    assessment = models.ForeignKey('Assessment', related_name='document_links', null=True)
    attachment = models.ForeignKey('attachments.Attachment', related_name='assessment_links_attachments', blank=True,
                                   null=True)
    document_type_attachment = models.ForeignKey('attachments.DocumentType',
                                                 related_name='assessment_document_links_attachments', blank=True,
                                                 null=True)

    is_done = models.BooleanField(_("Is done?"), default=False)
    organization = models.ForeignKey(
        'organization.Organization', related_name='assessment_document_links', blank=True, null=True
    )
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='assessment_document_links',
                               blank=True, null=True)
    timestamp = models.DateTimeField()

    objects = AssessmentDocumentLinkManager()

    def natural_key(self):
        return self.assessment, self.document_type_attachment, self.attachment

    def __unicode__(self):
        return u"asm=<{self.assessment}> dt=<{self.document_type_attachment}> doc=<{self.attachment}>".format(self=self)

    def save(self, *args, **kwargs):
        # Update timestamp
        if not self.pk:
            self.timestamp = datetime.today()
        return super(AssessmentDocumentLink, self).save(*args, **kwargs)

    class Meta:
        db_table = 'AssessmentDocumentLink'
        permissions = (
            ('view_assessmentdocumentlink', 'Can view assessment document link'),
        )

    def __init__(self, *args, **kwargs):
        super(AssessmentDocumentLink, self).__init__(*args, **kwargs)
        self.modified_by_user = None
        self.modified_by_role = None


class AssessmentResult(UuidModel):
    """
    Assessment result

    To be subclassed by assessment apps to provide specific fields.
    """
    assessment = models.ForeignKey("Assessment", related_name="results")
    text = models.TextField(blank=True, null=True)

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentResult'

    def __unicode__(self):
        return u"Asm:<%s> Text:<%s>" % (self.assessment, self.text)


class AssessmentShare(UuidModel):
    """
    Assessment share: instance of assessment shared with one partner
    """
    assessment = models.ForeignKey('Assessment', related_name='shares')
    partner = models.ForeignKey('organization.Organization', related_name='incoming_assessment_shares')

    # fk: share_permissions: SharePermission

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentShare'
        permissions = (
            ('view_assessmentshare', 'Can view assessment share'),
        )

    def __unicode__(self):
        return u"<%s/%s> shared with <%s>" % (self.assessment.organization, self.assessment, self.partner)


class SharePermission(UuidModel):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="created_share_permissions", blank=True,
                               null=True)
    PERMISSION_NAME_CHOICES = []  # to be overridden in derived models
    permission_name = models.CharField(max_length=1000, choices=PERMISSION_NAME_CHOICES)
    permission_data = models.TextField(max_length=10000, blank=True)

    class Meta(UuidModel.Meta):
        abstract = True


class AssessmentSharePermission(SharePermission):
    assessment_share = models.ForeignKey("AssessmentShare", related_name="share_permissions")
    PERMISSION_NAME_CHOICES = [
        ("results", "Results"),
        ("answers", "Answers")
    ]

    class Meta(UuidModel.Meta):
        db_table = 'AssessmentSharePermission'
        permissions = (
            ('view_assessmentsharepermission', 'Can view assessment share permission'),
        )

    def __unicode__(self):
        return u"<%s> on <%s> shared with <%s>" % \
               (self.permission_name,
                self.assessment_share.assessment,
                self.assessment_share.partner)


def upload_standard_to_s3(instance, filename):
    return u'assessments/standards/{file}'.format(
        uuid=str(uuid4()),
        file=filename
    )


def upload_path(instance, filename):
    return u'assessments/assessmentExports/{file}'.format(
        uuid=str(uuid4()),
        file=filename
    )


class AssessmentTypeStandard(UuidModel):
    export_date = models.DateTimeField(auto_now_add=True)
    import_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=10)
    file = models.FileField(max_length=255, verbose_name='json filename', upload_to=upload_standard_to_s3)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    import_outcome = models.TextField(max_length=1000, null=True)

    class Meta:
        db_table = 'AssessmentTypeStandard'


class AssessmentExport(UuidModel):
    export_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=255)
    file = models.FileField(max_length=255, verbose_name='json filename', upload_to=upload_path)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    export_detail = models.TextField(max_length=1000, null=True)

    class Meta:
        db_table = 'AssessmentExport'


class AssessmentPDFExport(UuidModel):
    export_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=255)
    file = models.FileField(max_length=255, verbose_name='pdf filename', upload_to=upload_path)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    export_detail = models.TextField(max_length=1000, null=True)

    class Meta:
        db_table = 'AssessmentPDFExport'


class DynamicEvidenceConfiguration(UuidModel):
    assessment_type = models.ForeignKey('AssessmentType', related_name='dynamic_configurations')
    name = models.CharField(max_length=550, blank=True)
    is_default = models.BooleanField(default=False)

    DEFAULT_DEC_ERROR = 'This assessment type already has default configuration.'

    class Meta:
        db_table = "DynamicEvidenceConfiguration"
        permissions = (
            ('view_dynamicevidenceconfiguration', 'Can view dynamic evidence configuration permission'),
        )

    def __unicode__(self):
        return u'{}'.format(self.name)

    def make_name(self):
        name = u"%(type)s Configuration" % {"type": tolerant_ugettext(self.assessment_type.name)}
        existing_configs_of_type = DynamicEvidenceConfiguration.objects.filter(name__istartswith=name).count()
        if existing_configs_of_type:
            name = u'{} - {}'.format(name, existing_configs_of_type)
        return name

    def clean(self):
        if self.is_default and DynamicEvidenceConfiguration.objects.filter(
                assessment_type=self.assessment_type,
                is_default=True).exclude(pk=self.pk).exists():
            raise ValidationError({'is_default':_(self.DEFAULT_DEC_ERROR)})

    def save(self, *args, **kwargs):
        if not self.name:
            self.name = self.make_name()

        if self.is_default and DynamicEvidenceConfiguration.objects.filter(
                assessment_type=self.assessment_type,
                is_default=True).exclude(pk=self.pk).exists():
            raise ValidationError(self.DEFAULT_DEC_ERROR)

        super(DynamicEvidenceConfiguration, self).save(*args, **kwargs)


class QuestionEvidenceConfigurations(UuidModel):
    configuration = models.ForeignKey('DynamicEvidenceConfiguration', related_name='question_configurations')
    question = models.ForeignKey('Question', related_name='configurations')
    document_type = models.ForeignKey('attachments.DocumentType', related_name='configurations')

    class Meta:
        db_table = "QuestionEvidenceConfigurations"
        unique_together = ('configuration', 'question', 'document_type')
        permissions = (
            ('view_questionevidenceconfigurations', 'Can view question evidence configuration permission'),
        )

    def __unicode__(self):
        return u'{}({})({})'.format(self.configuration, self.question.code, self.document_type.code)


def upload_dynamic_evidence_to_s3(instance, filename):
    return u'assessments/dynamic_evidence/{uuid}-{file}'.format(
        uuid=str(uuid4()),
        file=filename
    )


class DynamicEvidenceImport(UuidModel):
    configuration = models.ForeignKey('DynamicEvidenceConfiguration', related_name='import_configurations')
    import_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=20)
    file = models.FileField(max_length=255, verbose_name='CSV File', upload_to=upload_dynamic_evidence_to_s3)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    import_log = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'DynamicEvidenceImport'

    def __unicode__(self):
        return u'{} - {}'.format(self.configuration, self.created_time)


class QuestionLabel(UuidModel):
    slug = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=550)
    icon_class = models.CharField(max_length=255, blank=True)

    class Meta:
        db_table = "QuestionLabel"
        permissions = (
            ('view_questionlabel', 'Can view question labels permission'),
        )

    def __unicode__(self):
        return u'{} - {}'.format(self.name, self.slug)


class DynamicLabelConfiguration(UuidModel):
    name = models.CharField(max_length=550)
    
    class Meta:
        db_table = "DynamicLabelConfiguration"
        permissions = (
            ('view_dynamiclabelconfiguration', 'Can view dynamic label configuration permission'),
        )

    def __unicode__(self):
        return u'{}'.format(self.name)


class QuestionLabelConfigurations(UuidModel):
    configuration = models.ForeignKey('DynamicLabelConfiguration', related_name='question_label_configurations')
    question = models.ForeignKey('Question', related_name='label_configurations')
    label = models.ForeignKey('QuestionLabel', related_name='label_configurations')

    class Meta:
        db_table = "QuestionLabelConfigurations"
        unique_together = ('configuration', 'question', 'label')
        permissions = (
            ('view_questionlabelconfigurations', 'Can view question label configuration permission'),
        )

    def __unicode__(self):
        return u'{}({})({})'.format(self.configuration, self.question.code, self.label.slug)


class QuestionsMappingConfiguration(UuidModel):
    source_assessment_type = models.ForeignKey('AssessmentType', related_name='source_questions_mapping')
    target_assessment_type = models.ForeignKey('AssessmentType', related_name='target_questions_mapping')
    name = models.CharField(max_length=550, blank=True)

    SAME_ASSESSMENTS_ERROR = "Source and the target assessment types should not be the same."
    RELATION_ALREADY_EXIST_ERROR = "Relationship between source and target is already exists."


    class Meta:
        db_table = "QuestionsMappingConfiguration"
        permissions = (
            ('view_questionsmappingconfiguration', 'Can view questions mapping configuration permission'),
        )


    def __unicode__(self):
        return u'{}'.format(self.name)


    def make_name(self):
        name = u"{a} - {b} Mapping".format(
            a=tolerant_ugettext(self.source_assessment_type.name),
            b=tolerant_ugettext(self.target_assessment_type.name)
        )
        existing_configs_of_type = QuestionsMappingConfiguration.objects.filter(name__istartswith=name).count()
        if existing_configs_of_type:
            name = u'{} - {}'.format(name, existing_configs_of_type)
        return name


    def check_relation_count(self):
        return self.__class__.objects.filter(
            (Q(source_assessment_type=self.source_assessment_type) & Q(target_assessment_type=self.target_assessment_type)) |
            (Q(source_assessment_type=self.target_assessment_type) & Q(target_assessment_type=self.source_assessment_type))
        ).count()


    def clean(self):
        if self.source_assessment_type_id and self.target_assessment_type_id and \
                        self.source_assessment_type_id == self.target_assessment_type_id:
            raise ValidationError({'target_assessment_type': _(self.SAME_ASSESSMENTS_ERROR)})

        # Only check on create
        if not self.pk and self.source_assessment_type_id and self.target_assessment_type_id and self.check_relation_count():
            raise ValidationError({
                'source_assessment_type':_(self.RELATION_ALREADY_EXIST_ERROR),
                'target_assessment_type':_(self.RELATION_ALREADY_EXIST_ERROR)
            })


    def save(self, *args, **kwargs):
        if self.source_assessment_type == self.target_assessment_type:
            raise ValidationError({'target_assessment_type': _(self.SAME_ASSESSMENTS_ERROR)})

        # Only check on create
        if not self.pk and self.check_relation_count():
            raise ValidationError(self.RELATION_ALREADY_EXIST_ERROR)

        if not self.name:
            self.name = self.make_name()

        super(QuestionsMappingConfiguration, self).save(*args, **kwargs)


class QuestionMappings(UuidModel):
    configuration = models.ForeignKey('QuestionsMappingConfiguration', related_name='question_mapping_configurations')
    source_question = models.ForeignKey('Question', related_name='source_question_mapping')
    target_question = models.ForeignKey('Question', related_name='target_question_mapping')

    class Meta:
        db_table = "QuestionMappings"
        verbose_name_plural = "Question Mappings"
        unique_together = (('configuration', 'source_question',),
                           ('configuration', 'target_question',))
        permissions = (
            ('view_questionmappings', 'Can view question mappings permission'),
        )

    def __unicode__(self):
        return u'{} - ({} - {})'.format(self.configuration, self.source_question.code, self.target_question.code)



class BackgroundEvidenceCopy(UuidModel):
    source_assessment = models.ForeignKey(Assessment, related_name='copyevidence_source_assessment')
    target_assessment = models.OneToOneField(Assessment, related_name='copyevidence_target_assessment')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
    status = models.CharField(max_length=99, choices=BACKGROUND_EVIDENCE_COPY_STATUS, default='pending')
    log = models.TextField(blank=True,null=True)

    class Meta:
        db_table = "backgroundevidencecopy"
        verbose_name_plural = "background evidence copy"
        permissions = (
            ('view_backgroundevidencecopy', 'Can view background evidence copy status'),
        )

    def __unicode__(self):
        return u'{} - {} ({})'.format(self.source_assessment, self.target_assessment, self.status)

