from datetime import datetime
import json

from django import forms
from django.conf import settings
from django.contrib import admin
from django.db import transaction
from django.forms import ModelForm
from django.http.response import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.utils.translation import ugettext_lazy as _

from django.http import HttpResponse
from django.conf.urls import url
from django.core.urlresolvers import reverse

from import_export import fields
from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin
from import_export.widgets import ForeignKeyWidget, ManyToManyWidget
from assessments.filters import AssessmentTypeKindFilter
from assessments.forms import AssessmentPdfExportForm
from assessments.models import QuestionnaireElementState, AssessmentResult, AssessmentExport, \
    DynamicEvidenceConfiguration, QuestionEvidenceConfigurations, QuestionLabel, \
    QuestionLabelConfigurations, DynamicLabelConfiguration, QuestionsMappingConfiguration, QuestionMappings, \
    AssessmentPDFExport, BackgroundEvidenceCopy

from assessments.tasks import standard_import_task, standard_export_task, assessment_export_task, \
    dynamic_evidence_import_task, assessment_pdf_export_task

from attachments.models import DocumentType, Attachment, TextReferenceAttachment, NotApplicableAttachment, \
    AgriformAttachment, FormAttachment, FileAttachment
from core.admin import BaseAdmin, BaseTabularInline, TranslationMixin
from form_builder.models import GenericForm, GenericFormData
from .forms import QuestionnaireForm, AssessmentTypeStandardForm, DynamicEvidenceImportForm, QuestionLabelAdminForm, \
    QuestionLabelConfigurationInlineForm, QuestionMappingsInlineForm
from .models import (
    Answer,
    Judgement,
    Assessment,
    AssessmentDocumentLink,
    AssessmentShare,
    AssessmentSharePermission,
    AssessmentType,
    AssessmentTypeSection,
    PossibleAnswer,
    PossibleAnswerSet,
    Question,
    QuestionLevel,
    Questionnaire,
    QuestionnaireElement,
    QuestionnaireHeading,
    QuestionnaireParagraph,
    QuestionnaireState,
    QuestionTrigger,
    QuestionTriggerCondition,
    AssessmentTypeStandard,
    DynamicEvidenceImport)
from organization.models import Organization, OrganizationUnit, Product, ProductType, ProductPartner, ProductIdentifier

from django.core.paginator import EmptyPage, InvalidPage, Paginator
from django.contrib.admin.views.main import ChangeList

from assessments.utils import  DynamicEvidenceConfigurationExport


ASSESSMENT_TYPE_RESOURCE_FIELDS = ('uuid', 'code', 'kind', 'name', 'description', 'is_public',
                                   'has_product_selection', 'has_logo', 'available_languages', 'remarks',
                                   'sharing_conditions', 'edition')

QUESTIONNAIRE_RESOURCE_FIELDS = ('uuid', 'code', 'order_index', 'name', 'assessment_type', 'intro_text',
                                 'available_languages', 'requires_signature', 'completeness_check', 'unpublished',
                                 'remarks', 'requires_reference', 'product_types')

QUESTION_LEVEL_RESOURCE_FIELDS = ('uuid', 'code', 'assessment_type', 'order_index', 'title', 'color',
                                  'requires_justification', 'requires_attachment', 'remarks')

ASSESSMENT_TYPE_SECTION_RESOURCE_FIELDS = ('uuid', 'assessment_type', 'platform', 'sections_list')

POSSIBLE_ANSWER_SET_RESOURCE_FIELDS = ('uuid', 'code', 'assessment_type', 'name')

POSSIBLE_ANSWER_RESOURCE_FIELDS = ('uuid', 'code', 'possible_answer_set', 'text', 'value', 'answer_type',
                                   'order_index', 'requires_justification', 'requires_attachment')

HEADING_RESOURCE_FIELDS = ('uuid', 'code', 'element_type', 'order_index', 'questionnaire',
                           'text', 'guidance', 'level', 'is_initially_visible', 'parent_element')

PARAGRAPH_RESOURCE_FIELDS = ('uuid', 'code', 'element_type', 'order_index', 'questionnaire',
                             'text', 'guidance', 'is_initially_visible', 'parent_element')

QUESTION_RESOURCE_FIELDS = ('uuid', 'code', 'element_type', 'order_index', 'questionnaire',
                            'text', 'guidance', 'criteria', 'answer_type', 'possible_answer_set',
                            'level_object', 'is_statistics_valuable', 'document_types_attachments',
                            'evidence_default_answer', 'evidence_default_justification',
                            'is_justification_visible', 'justification_placeholder', 'initially_visible',
                            'is_initially_visible', 'parent_element',)

QUESTION_TRIGGER_RESOURCE_FIELDS = ('uuid', 'target_question', 'target_element', 'target_value',
                                    'target_justification', 'target_visibility')

QUESTION_TRIGGER_CONDITION_RESOURCE_FIELDS = ('uuid', 'trigger', 'source_question', 'source_property_name',
                                              'source_value')

# Assessment Export resource fields
UUID_MODEL_FIELDS = (
    'uuid', 'remarks', 'is_test', 'modified_time', 'created_time'
)

ASSESSMENT_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('name', 'state', 'products')

ASSESSMENT_DOCUMENT_LINKS_RESOURCE_FIELDS = ('id', 'assessment', 'attachment', 'is_done', 'timestamp')

QUESTIONNAIRESTATE_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'assessment', 'is_complete', 'is_signed'
)

QUESTIONNAIREELEMENTSTATE_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'assessment', 'is_visible'
)

ANSWER_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'value', 'value_uuids', 'value_old', 'justification', 'assessment', 'target_uuid', 'timestamp'
)

JUDGEMENT_RESOURCE_FIELDS = UUID_MODEL_FIELDS + (
    'value', 'justification', 'evaluation_is_solved', 'evaluation_remarks', 'assessment'
)

ASSESSMENTRESULT_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('assessment', 'text')

ASSESSMENTSHARE_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('assessment',)

ASSESSMENTSHAREPERMISSION_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('permission_name', 'permission_data',
                                                                 'assessment_share')
PRODUCT_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('name', 'slug', 'harvest_year', 'area', 'location', 'comment')

PRODUCTPARTNER_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('product', 'contract_number')

PRODUCTIDENTIFIER_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('product', 'identifier_type', 'identifier_type_name', 'value')

GENERICFORM_RESOURCE = UUID_MODEL_FIELDS + ('name', 'SLUG')

GENERICFORMDATA_RESOURCE = UUID_MODEL_FIELDS + ('generic_form', 'json_data')

ATTACHMENT_RESOURCE_FIELDS = UUID_MODEL_FIELDS + ('title', 'for_deletion_mark')

FILEATTACHMENT_RESOURCE_FIELDS = ('file', 'original_file_name', 'expiration_date', 'file_type')

TEXT_REFERENCE_ATTACHMENT_RESOURCE_FIELDS = ('description',)

NOTAPPLICABLE_ATTACHMENT_RESOURCE_FIELDS = ()

AGRIFORM_ATTACHMENT_RESOURCE_FIELDS = ('assessment', 'agriform_reference')

FORM_ATTACHMENT_RESOURCE_FIELDS = ('generic_form_data',)


class StandardExportMixin(object):
    actions = ['export_selected_assessment_types_as_standard']

    def export_selected_assessment_types_as_standard(self, request, queryset):
        ids = list(queryset.values_list('code', flat=True))
        standard_export_task.delay(ids, request.user.pk)
        self.message_user(request, 'Export started in background. Please check later')


class AssessmentExportMixin(object):
    actions = ['export_whole_assessment']

    def export_whole_assessment(self, request, queryset):
        ids = list(queryset.order_by('created_time').values_list('uuid', flat=True))

        assessment_export = AssessmentExport()
        assessment_export.status = _("In progress")
        assessment_export.user = request.user
        assessment_export.save()

        assessment_export_task.delay(ids, assessment_export.uuid)
        self.message_user(request, 'Export started in background. Please check later')


class StandardImportMixin(object):
    actions = ['import_selected_standard']

    def import_selected_standard(self, request, queryset):
        ids = list(queryset.values_list('uuid', flat=True))
        standard_import_task.delay(ids, request.user.pk)
        self.message_user(request, 'Import started in background. Please check later')


# Import/Export resources
class AssessmentResource(resources.ModelResource):
    created_by = fields.Field(
        column_name='created_by',
        attribute='created_by',
        widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    last_modified_by = fields.Field(
        column_name='last_modified_by',
        attribute='last_modified_by',
        widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    organization = fields.Field(
        column_name='organization',
        attribute='organization',
        widget=ForeignKeyWidget(Organization, 'slug'))

    assessment_type = fields.Field(
        column_name='assessment_type',
        attribute='assessment_type',
        widget=ForeignKeyWidget(AssessmentType, 'code'))

    class Meta:
        fields = ASSESSMENT_RESOURCE_FIELDS + (
            'created_by', 'last_modified_by', 'organization', 'assessment_type'
        )
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = Assessment


class AssessmentDocumentLinkResource(resources.ModelResource):
    author = fields.Field(
        column_name='author',
        attribute='author',
        widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    document_type_attachment = fields.Field(
        column_name='document_type_attachment',
        attribute='document_type_attachment',
        widget=ForeignKeyWidget(DocumentType, 'code'))

    class Meta:
        fields = ASSESSMENT_DOCUMENT_LINKS_RESOURCE_FIELDS + ('author', 'document_type_attachment')
        import_id_fields = ('id',)
        skip_unchanged = True
        model = AssessmentDocumentLink


class QuestionnaireStateResource(resources.ModelResource):
    questionnaire = fields.Field(
        column_name='questionnaire',
        attribute='questionnaire',
        widget=ForeignKeyWidget(Questionnaire, 'code'))

    class Meta:
        fields = QUESTIONNAIRESTATE_RESOURCE_FIELDS + ('questionnaire',)
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = QuestionnaireState


class QuestionnaireElementStateResource(resources.ModelResource):
    questionnaire_element = fields.Field(
        column_name='questionnaire_element',
        attribute='questionnaire_element',
        widget=ForeignKeyWidget(QuestionnaireElement, 'code'))

    class Meta:
        fields = QUESTIONNAIREELEMENTSTATE_RESOURCE_FIELDS + ('questionnaire_element',)
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = QuestionnaireElementState


class AnswerResource(resources.ModelResource):
    question = fields.Field(
        column_name='question',
        attribute='question',
        widget=ForeignKeyWidget(Question, 'code'))

    organization = fields.Field(
        column_name='organization',
        attribute='organization',
        widget=ForeignKeyWidget(Organization, 'slug'))

    organization_unit = fields.Field(
        column_name='organization_unit',
        attribute='organization_unit',
        widget=ForeignKeyWidget(OrganizationUnit, 'slug'))

    author = fields.Field(
        column_name='author',
        attribute='author',
        widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    class Meta:
        fields = ANSWER_RESOURCE_FIELDS + (
            'question', 'organization', 'organization_unit', 'product', 'author'
        )
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = Answer


class JudgementResource(resources.ModelResource):
    question = fields.Field(
        column_name='question',
        attribute='question',
        widget=ForeignKeyWidget(Question, 'code'))

    author = fields.Field(
        column_name='author',
        attribute='author',
        widget=ForeignKeyWidget(settings.AUTH_USER_MODEL, 'username'))

    class Meta:
        fields = JUDGEMENT_RESOURCE_FIELDS + ('question', 'author')
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = Judgement


class AssessmentResultResource(resources.ModelResource):
    partner = fields.Field(
        column_name='partner',
        attribute='partner',
        widget=ForeignKeyWidget(Organization, 'slug'))

    class Meta:
        fields = ASSESSMENTRESULT_RESOURCE_FIELDS + ('partner',)
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentResult


class AssessmentShareResource(resources.ModelResource):
    class Meta:
        fields = ASSESSMENTSHARE_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentShare


class AssessmentSharePermissionResource(resources.ModelResource):
    class Meta:
        fields = ASSESSMENTSHAREPERMISSION_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentSharePermission


class ProductResource(resources.ModelResource):
    organization = fields.Field(
        column_name='organization',
        attribute='organization',
        widget=ForeignKeyWidget(Organization, 'slug'))
    organization_unit = fields.Field(
        column_name='organization_unit',
        attribute='organization_unit',
        widget=ForeignKeyWidget(OrganizationUnit, 'slug'))
    product_type = fields.Field(
        column_name='product_type',
        attribute='product_type',
        widget=ForeignKeyWidget(ProductType, 'code'))

    class Meta:
        fields = PRODUCT_RESOURCE_FIELDS + ('organization', 'organization_unit', 'product_type')
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = Product


class ProductPartnerResource(resources.ModelResource):
    organization_uuid = fields.Field(
        column_name='organization_uuid',
        attribute='organization_uuid',
        widget=ForeignKeyWidget(Organization, 'slug'))

    class Meta:
        fields = PRODUCTPARTNER_RESOURCE_FIELDS + ('organization',)
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = ProductPartner


class ProductIdentifierResource(resources.ModelResource):
    class Meta:
        fields = PRODUCTIDENTIFIER_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = ProductIdentifier


class GenericFormResource(resources.ModelResource):
    class Meta:
        fields = GENERICFORM_RESOURCE
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = GenericForm


class GenericFormDataResource(resources.ModelResource):
    class Meta:
        fields = GENERICFORMDATA_RESOURCE
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = GenericFormData


class AttachmentResource(resources.ModelResource):
    organization = fields.Field(
        column_name='organization',
        attribute='organization',
        widget=ForeignKeyWidget(Organization, 'slug'))

    class Meta:
        fields = ATTACHMENT_RESOURCE_FIELDS + ('organization',)
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = Attachment


class FileAttachmentResource(resources.ModelResource):
    organization = fields.Field(
        column_name='organization',
        attribute='organization',
        widget=ForeignKeyWidget(Organization, 'slug'))

    class Meta:
        fields = FILEATTACHMENT_RESOURCE_FIELDS + ('organization',)
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = FileAttachment


class TextReferenceAttachmentResource(resources.ModelResource):
    class Meta:
        fields = TEXT_REFERENCE_ATTACHMENT_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = TextReferenceAttachment


class NotApplicableAttachmentResource(resources.ModelResource):
    class Meta:
        fields = NOTAPPLICABLE_ATTACHMENT_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = NotApplicableAttachment


class AgriformAttachmentResource(resources.ModelResource):
    class Meta:
        fields = AGRIFORM_ATTACHMENT_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AgriformAttachment


class FormAttachmentResource(resources.ModelResource):
    class Meta:
        fields = FORM_ATTACHMENT_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = FormAttachment


# End of assessment export resources


class AssessmentTypeResource(resources.ModelResource):
    class Meta:
        # fields = ASSESSMENT_TYPE_RESOURCE_FIELDS
        # export_order = ASSESSMENT_TYPE_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentType


class QuestionLevelResource(resources.ModelResource):
    assessment_type = fields.Field(
        column_name='assessment_type',
        attribute='assessment_type',
        widget=ForeignKeyWidget(AssessmentType, 'code'))

    class Meta:
        fields = QUESTION_LEVEL_RESOURCE_FIELDS
        export_order = QUESTION_LEVEL_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = QuestionLevel


class QuestionnaireResource(resources.ModelResource):
    def __init__(self):
        self.product_type_names = []
        super(QuestionnaireResource, self).__init__()

    assessment_type = fields.Field(
        column_name='assessment_type',
        attribute='assessment_type',
        widget=ForeignKeyWidget(AssessmentType, 'code'))

    product_types = fields.Field(
        column_name='product_types',
        attribute='product_types',
        widget=ManyToManyWidget(model=ProductType, field='name'))

    def dehydrate_product_types(self, obj):
        if obj.pk:
            product_types = obj.product_types.all()

            return ",".join([product_type.name for product_type in product_types])
        else:
            return ""

    def before_import_row(self, row, **kwargs):
        product_type_names = row['product_types']
        self.product_type_names = product_type_names.split(',') if product_type_names else []

    def after_save_instance(self, instance, using_transactions, dry_run):
        if dry_run:
            return

        if not self.product_type_names:
            return

        questionnaire = Questionnaire.objects.get(uuid=instance.pk)
        product_types = ProductType.objects.filter(name__in=self.product_type_names)

        [product_type.questionnaires.add(questionnaire) for product_type in product_types]

    class Meta:
        fields = QUESTIONNAIRE_RESOURCE_FIELDS
        export_order = QUESTIONNAIRE_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = False
        model = Questionnaire


class HeadingResource(resources.ModelResource):
    questionnaire = fields.Field(
        column_name='questionnaire',
        attribute='questionnaire',
        widget=ForeignKeyWidget(Questionnaire, 'code'))

    parent_element = fields.Field(
        column_name='parent_element',
        attribute='parent_element',
        widget=ForeignKeyWidget(QuestionnaireElement, 'code'))

    class Meta:
        fields = HEADING_RESOURCE_FIELDS
        export_order = HEADING_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = QuestionnaireHeading


class ParagraphResource(resources.ModelResource):
    questionnaire = fields.Field(
        column_name='questionnaire',
        attribute='questionnaire',
        widget=ForeignKeyWidget(Questionnaire, 'code'))

    parent_element = fields.Field(
        column_name='parent_element',
        attribute='parent_element',
        widget=ForeignKeyWidget(QuestionnaireElement, 'code'))

    class Meta:
        fields = PARAGRAPH_RESOURCE_FIELDS
        export_order = PARAGRAPH_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = QuestionnaireParagraph


class QuestionResource(resources.ModelResource):
    questionnaire = fields.Field(
        column_name='questionnaire',
        attribute='questionnaire',
        widget=ForeignKeyWidget(Questionnaire, 'code'))

    possible_answer_set = fields.Field(
        column_name='possible_answer_set',
        attribute='possible_answer_set',
        widget=ForeignKeyWidget(PossibleAnswerSet, 'code'))

    document_types_attachments = fields.Field(
        column_name='document_types_attachments',
        attribute='document_types_attachments',
        widget=ManyToManyWidget(model=DocumentType, field='code'))

    parent_element = fields.Field(
        column_name='parent_element',
        attribute='parent_element',
        widget=ForeignKeyWidget(QuestionnaireElement, 'code'))

    level_object = fields.Field(
        column_name='level_object',
        attribute='level_object',
        widget=ForeignKeyWidget(QuestionLevel, 'code')
    )

    class Meta:
        fields = QUESTION_RESOURCE_FIELDS
        export_order = QUESTION_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = False
        model = Question


class AssessmentTypeSectionResource(resources.ModelResource):
    assessment_type = fields.Field(
        column_name='assessment_type',
        attribute='assessment_type',
        widget=ForeignKeyWidget(AssessmentType, 'code'))

    class Meta:
        fields = ASSESSMENT_TYPE_SECTION_RESOURCE_FIELDS
        export_order = ASSESSMENT_TYPE_SECTION_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = AssessmentTypeSection


class PossibleAnswerResource(resources.ModelResource):
    triggered_questions = fields.Field(
        column_name='triggered_questions',
        attribute='triggered_questions',
        widget=ManyToManyWidget(model=Question, field='uuid'))

    class Meta:
        fields = POSSIBLE_ANSWER_RESOURCE_FIELDS
        export_order = POSSIBLE_ANSWER_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = PossibleAnswer


class PossibleAnswerSetResource(resources.ModelResource):
    assessment_type = fields.Field(
        column_name='assessment_type',
        attribute='assessment_type',
        widget=ForeignKeyWidget(AssessmentType, 'code'))

    class Meta:
        fields = POSSIBLE_ANSWER_SET_RESOURCE_FIELDS
        export_order = POSSIBLE_ANSWER_SET_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = PossibleAnswerSet


class QuestionTriggerResource(resources.ModelResource):
    target_question = fields.Field(
        column_name='target_question',
        attribute='target_question',
        widget=ForeignKeyWidget(Question, 'code'))

    target_element = fields.Field(
        column_name='target_element',
        attribute='target_element',
        widget=ForeignKeyWidget(QuestionnaireElement, 'code'))

    class Meta:
        fields = QUESTION_TRIGGER_RESOURCE_FIELDS
        export_order = QUESTION_TRIGGER_RESOURCE_FIELDS
        import_id_fields = ('uuid',)
        skip_unchanged = True
        model = QuestionTrigger


class QuestionTriggerConditionResource(resources.ModelResource):
    trigger = fields.Field(
        column_name='trigger',
        attribute='trigger',
        widget=ForeignKeyWidget(QuestionTrigger, 'uuid'))

    source_question = fields.Field(
        column_name='source_question',
        attribute='source_question',
        widget=ForeignKeyWidget(Question, 'code'))

    class Meta:
        import_id_fields = ('uuid',)
        fields = QUESTION_TRIGGER_CONDITION_RESOURCE_FIELDS
        export_order = QUESTION_TRIGGER_CONDITION_RESOURCE_FIELDS
        model = QuestionTriggerCondition
        skip_unchanged = True


# Admin classes

@admin.register(Answer)
class AnswerAdmin(BaseAdmin):
    """
    Answer
    """
    model = Answer
    list_display = ('uuid', 'organization_name', 'assessment', 'question', 'value', 'created_time', 'modified_time')
    search_fields = ('uuid', 'assessment__organization__name', 'question__code')
    list_filter = ('assessment__assessment_type',)

    def organization_name(self, obj):
        return obj.assessment.organization


@admin.register(Judgement)
class JudgementAdmin(BaseAdmin):
    """
    Judgement
    """
    model = Judgement
    list_display = ('uuid', 'assessment', 'question', 'value', 'created_time', 'modified_time')
    search_fields = ('uuid', 'assessment__name', 'question__code')


class AssessmentShareInline(BaseTabularInline):
    model = AssessmentShare


@admin.register(Assessment)
class AssessmentAdmin(BaseAdmin, AssessmentExportMixin):
    model = Assessment
    list_display = ('name', 'organization', 'assessment_type', 'state')
    search_fields = ('uuid',)
    list_filter = ('organization__name', 'assessment_type', AssessmentTypeKindFilter)
    filter_horizontal = ('products',)

    inlines = [
        AssessmentShareInline
    ]

    actions = ['export_as_pdf']

    def export_as_pdf(self, request, queryset):
        form = None

        if 'apply' in request.POST:
            form = AssessmentPdfExportForm(request.POST)
            if form.is_valid():
                language = form.cleaned_data['language']
                assessments = queryset.all()
                for assessment in assessments:
                    assessment_pf_export = AssessmentPDFExport()
                    assessment_pf_export.status = _("In progress")
                    assessment_pf_export.user = request.user
                    assessment_pf_export.save()
                    assessment_pdf_export_task.delay(assessment.pk, assessment_pf_export.pk, language)

                self.message_user(request, 'Export started in background. Please check later')

                return HttpResponseRedirect(
                    reverse("admin:%s_%s_changelist" % (self.model._meta.app_label, self.model._meta.model_name),))

        if not form:
            form = AssessmentPdfExportForm(
                initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)}
            )

        return render_to_response(
            'assessments/admin/export_assessment_as_pdf.html',
            {'form': form, 'opts': self.model._meta, 'title': _("Export assessment as pdf"),},
            context_instance=RequestContext(request)
        )

    export_as_pdf.short_description = "Export as PDF"


@admin.register(QuestionnaireState)
class QuestionnaireStateAdmin(BaseAdmin):
    model = QuestionnaireState
    list_display = ('assessment', 'questionnaire', 'is_complete', 'is_signed')
    search_fields = ('assessment', 'questionnaire')


@admin.register(AssessmentDocumentLink)
class AssessmentDocumentLinkAdmin(BaseAdmin):
    model = AssessmentDocumentLink
    list_display = ('get_document', 'get_document_type', 'is_done')
    search_fields = ('attachment__title', 'document_type_attachment__name')
    list_filter = ('attachment', 'is_done')
    raw_id_fields = ('assessment', 'attachment', 'document_type_attachment', 'author')
    autocomplete_lookup_fields = {
        'fk': ['assessment', 'attachment', 'document_type_attachment', 'author'],
    }

    def get_document(self, obj):
        return obj.attachment.title

    def get_document_type(self, obj):
        return obj.document_type_attachment.name


@admin.register(AssessmentShare)
class AssessmentShareAdmin(BaseAdmin):
    model = AssessmentShare
    use_fields = ('assessment', 'partner')


@admin.register(AssessmentSharePermission)
class AssessmentSharePermissionAdmin(BaseAdmin):
    model = AssessmentSharePermission
    use_fields = ('assessment_share', 'permission_name', 'permission_data')


class AssessmentTypeAdminForm(forms.ModelForm):
    available_languages = forms.MultipleChoiceField(choices=settings.LANGUAGES)

    def __init__(self, *args, **kwargs):
        print kwargs
        instance = kwargs.get('instance')
        if instance:
            instance.available_languages = instance.available_languages.split(",")
        super(AssessmentTypeAdminForm, self).__init__(*args, **kwargs)
        if not instance:
            self.fields['available_languages'].initial = [x[0] for x in settings.LANGUAGES]

    def clean_available_languages(self):
        langs = self.cleaned_data['available_languages']
        concatenated = ",".join(langs)
        return concatenated


class AssessmentTypeAdmin(ImportExportActionModelAdmin, BaseAdmin, TranslationMixin, StandardExportMixin):
    model = AssessmentType
    resource_class = AssessmentTypeResource
    list_display = ('name', 'uuid', 'code', 'kind')
    list_filter = ('kind',)
    search_fields = ('name', 'uuid', 'code')
    form = AssessmentTypeAdminForm

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'name',
            'description'
        ]
        return super(AssessmentTypeAdmin, self).__init__(*args, **kwargs)


@admin.register(AssessmentTypeSection)
class AssessmentTypeSectionsAdmin(ImportExportActionModelAdmin, BaseAdmin):
    model = AssessmentTypeSection
    resource_class = AssessmentTypeSectionResource
    list_display = ('assessment_type', 'sections_list', 'platform')
    list_filter = ('assessment_type', 'platform')
    search_fields = ('uuid',)
    exclude = ('is_test', 'created_time', 'modified_time', 'remarks')


class PossibleAnswerInline(BaseTabularInline):
    model = PossibleAnswer
    fields = ('value', 'text', 'order_index', 'requires_justification')
    sortable_field_name = 'order_index'


class PossibleAnswerAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    model = PossibleAnswer
    resource_class = PossibleAnswerResource
    list_display = ('uuid', 'code', 'order_index', 'value', 'requires_justification')
    list_filter = ('possible_answer_set',)
    search_fields = ('code', 'value',)

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'text',
        ]
        return super(PossibleAnswerAdmin, self).__init__(*args, **kwargs)


@admin.register(PossibleAnswerSet)
class PossibleAnswerSetAdmin(ImportExportActionModelAdmin, BaseAdmin):
    model = PossibleAnswerSet
    resource_class = PossibleAnswerSetResource

    # List
    list_display = ('name', 'assessment_type')
    list_filter = ('assessment_type',)

    # Detail
    inlines = [
        PossibleAnswerInline,
    ]


class QuestionAssessmentFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('assessment type')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'assessment_type'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        assessment_types = AssessmentType.objects.all()
        lookups = [(at.code, at.name) for at in assessment_types]
        return lookups

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            result = queryset.filter(questionnaire__assessment_type__code=self.value())
        else:
            result = queryset
        return result


class QuestionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)

        # If we have a questionnaire (this means we are editing an existing question)
        # then only show its level objects in the dropdown.
        # When creating a new question, we don't have a questionnaire or anything so just show all levels.

        # TODO: why next line removed
        # if self.instance.questionnaire and 'level_object' in self.fields and self.instance.questionnaire.assessment_type:
        if self.instance.questionnaire:
            self.fields['level_object'].queryset = QuestionLevel.objects.filter(
                assessment_type=self.instance.questionnaire.assessment_type)


class QuestionnaireHeadingAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    resource_class = HeadingResource
    model = QuestionnaireHeading
    list_display = ('questionnaire', '__str__', 'order_index', 'code', 'text')
    search_fields = ('code', 'uuid', 'text')
    list_filter = ('questionnaire__assessment_type', 'questionnaire')
    order_by = ['questionnaire', 'order_index']

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'text',
            'guidance',
        ]
        return super(QuestionnaireHeadingAdmin, self).__init__(*args, **kwargs)


class QuestionnaireParagraphAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    resource_class = ParagraphResource
    model = QuestionnaireParagraph
    list_display = ('questionnaire', '__str__', 'order_index', 'code', 'text')
    search_fields = ('code', 'uuid', 'text')
    list_filter = ('questionnaire__assessment_type', 'questionnaire')
    order_by = ['questionnaire', 'order_index']

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'text',
            'guidance',
        ]
        return super(QuestionnaireParagraphAdmin, self).__init__(*args, **kwargs)


class QuestionAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    resource_class = QuestionResource
    model = Question
    form = QuestionForm
    fieldsets = (
        (None, {
            'fields': (
                'questionnaire',
                'order_index',
                'code',
                'answer_type',
                'level_object',
                'possible_answer_set',
                'text',
                'guidance',
                'is_justification_visible',
                'justification_placeholder',
                'is_statistics_valuable'
            )
        }),
        (_("Help text"), {
            'fields': (
                'criteria',
            )
        }),
        (_("Evidence"), {
            'fields': (
                'document_types_attachments',
                'covered_by_evidence',
                'evidence_default_answer',
                'answer_when_attachments'
            )
        }),
        (_("Triggering"), {
            'fields': (
                'is_initially_visible',
                'parent_element'
            )
        })
    )
    list_display = ('questionnaire', '__str__', 'order_index', 'code', 'text')
    search_fields = ('code', 'uuid', 'text')
    list_filter = ('questionnaire__assessment_type', 'questionnaire')
    raw_id_fields = ('questionnaire', 'parent_element', 'level_object', 'possible_answer_set')
    autocomplete_lookup_fields = {
        'fk': ('questionnaire', 'parent_element', 'level_object', 'possible_answer_set')
    }
    filter_horizontal = ('document_types_attachments',)
    order_by = ['questionnaire', 'order_index']

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'criteria', 'justification_placeholder',
            'text', 'guidance',
        ]
        return super(QuestionAdmin, self).__init__(*args, **kwargs)

    def get_assessment_type(self, obj):
        if obj.questionnaire:
            return obj.questionnaire.assessment_type


class QuestionLevelAdmin(ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    model = QuestionLevel
    resource_class = QuestionLevelResource
    list_display = ('__unicode__', 'assessment_type', 'order_index', 'code', 'color')
    list_filter = ('assessment_type',)
    ordering = ('assessment_type', 'order_index')

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'title',
        ]
        return super(QuestionLevelAdmin, self).__init__(*args, **kwargs)


# Questionnaires


class QuestionInline(BaseTabularInline):
    model = Question
    fields = ('code', 'order_index')
    ordering = ('order_index',)
    sortable_field_name = 'order_index'


class QuestionnaireElementInline(BaseTabularInline):
    model = QuestionnaireElement
    fields = ('element_type', 'code', 'text', 'order_index')
    readonly_fields = ('element_type', 'code', 'text')
    ordering = ('order_index',)
    sortable_field_name = 'order_index'

    def truncated_text(self, obj):
        return obj.text_nl[0:20]


class QuestionnaireAdmin(ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    resource_class = QuestionnaireResource
    form = QuestionnaireForm
    list_display = ('__unicode__', 'code', 'uuid', 'name', 'get_num_questions', 'assessment_type', 'unpublished')
    list_filter = ('assessment_type', 'unpublished')
    inlines = (
        QuestionnaireElementInline,
    )

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'name',
        ]
        return super(QuestionnaireAdmin, self).__init__(*args, **kwargs)

    def get_num_questions(self, obj):
        return obj.questions.count()

    get_num_questions.short_description = "Question count"


@admin.register(QuestionnaireElement)
class QuestionnaireElementAdmin(BaseAdmin):
    model = QuestionnaireElement
    list_display = ('__unicode__', 'code', 'text', 'uuid', 'get_assessment_type', 'questionnaire')
    list_filter = ('questionnaire__assessment_type', 'questionnaire')
    search_fields = ('code', 'text')
    raw_id_fields = ('parent_element',)
    autocomplete_lookup_fields = {
        'fk': ('parent_element',)
    }

    def get_assessment_type(self, obj):
        if obj.questionnaire:
            if obj.questionnaire.assessment_type:
                return obj.questionnaire.assessment_type.name
            else:
                return ''

    get_assessment_type.short_description = "Assessment type"


class QuestionTriggerConditionInline(BaseTabularInline):
    model = QuestionTriggerCondition
    fields = ('trigger', 'source_question', 'source_property_name', 'source_value')
    raw_id_fields = ('source_question',)
    autocomplete_lookup_fields = {
        'fk': ('source_question',)
    }


class QuestionTriggerAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin, BaseAdmin, TranslationMixin):
    model = QuestionTrigger
    resource_class = QuestionTriggerResource
    list_display = ('uuid', 'target_question', 'target_element', 'target_value', 'all_conditions')
    list_filter = ('target_element__questionnaire__assessment_type', 'target_element__questionnaire')
    search_fields = ('target_element', 'target_value')
    inlines = (QuestionTriggerConditionInline,)
    order_by = ['target_element__questionnaire']

    def all_conditions(self, obj):
        return list("{c.source_question}={c.source_value}".format(c=c) for c in obj.conditions.all())

    def __init__(self, *args, **kwargs):
        self.trans_fields = [
            'target_justification',
        ]
        return super(QuestionTriggerAdmin, self).__init__(*args, **kwargs)


@admin.register(QuestionTriggerCondition)
class QuestionTriggerConditionAdmin(ImportExportModelAdmin, ImportExportActionModelAdmin, BaseAdmin):
    list_display = ('uuid', 'trigger', 'source_question', 'source_property_name', 'source_value')
    search_fields = ('trigger', 'source_question')
    list_filter = ('source_question__questionnaire__assessment_type', 'source_question__questionnaire')
    order_by = ['source_question__target_element__questionnaire']
    model = QuestionTriggerCondition
    resource_class = QuestionTriggerConditionResource


@admin.register(AssessmentTypeStandard)
class AssessmentTypeStandardAdmin(BaseAdmin, StandardImportMixin):
    model = AssessmentTypeStandard
    form = AssessmentTypeStandardForm

    list_display = ('file', 'export_date', 'import_date', 'import_outcome', 'status', 'user')
    list_filter = ('export_date', 'status')
    search_fields = ('export_date',)
    ordering = ('-export_date',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.status = _("Success")
        obj.save()

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(AssessmentTypeStandardAdmin, self).get_actions(request)

        if 'delete_selected' in actions:
            del actions['delete_selected']

        return actions


@admin.register(AssessmentExport)
class AssessmentExportAdmin(BaseAdmin):
    model = AssessmentExport

    list_display = ('file', 'export_date', 'export_detail', 'status', 'user')
    list_filter = ('export_date', 'status')
    search_fields = ('export_date',)
    ordering = ('-export_date',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.status = _("Success")
        obj.save()

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(AssessmentExportAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


@admin.register(AssessmentPDFExport)
class AssessmentPDFExportAdmin(BaseAdmin):
    model = AssessmentPDFExport

    list_display = ('file', 'export_date', 'export_detail', 'status', 'user')
    list_filter = ('export_date', 'status')
    search_fields = ('export_date',)
    ordering = ('-export_date',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.status = _("Success")
        obj.save()

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(AssessmentPDFExportAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


class QuestionEvidenceConfigurationInlineForm(ModelForm):

    class Meta:
        model = QuestionEvidenceConfigurations
        fields = [
            'question',
            'document_type',
            'modified_time',
            'created_time',
        ]


class InlineChangeList(object):
    can_show_all = True
    multi_page = True
    get_query_string = ChangeList.__dict__['get_query_string']

    def __init__(self, request, page_num, paginator):
        self.show_all = 'all' in request.GET
        self.page_num = page_num
        self.paginator = paginator
        self.result_count = paginator.count
        self.params = dict(request.GET.items())


class QuestionEvidenceConfigurationInline(BaseTabularInline):
    model = QuestionEvidenceConfigurations
    max_num = None
    form = QuestionEvidenceConfigurationInlineForm
    raw_id_fields = ("document_type",)
    ordering = ('question',)
    per_page = 50
    template = 'admin/edit_inline/list.html'


    def get_formset(self, request, obj=None, **kwargs):
        formset_class = super(QuestionEvidenceConfigurationInline, self).get_formset(
            request, obj, **kwargs)

        class PaginationFormSet(formset_class):
            def __init__(self, *args, **kwargs):
                super(PaginationFormSet, self).__init__(*args, **kwargs)

                qs = self.queryset
                paginator = Paginator(qs, self.per_page)
                try:
                    page_num = int(request.GET.get('page', '1'))
                except ValueError:
                    page_num = 1

                try:
                    page = paginator.page(page_num)
                except (EmptyPage, InvalidPage):
                    page = paginator.page(paginator.num_pages)

                self.cl = InlineChangeList(request, page_num, paginator)

                self.paginator = paginator
                self.my_page = page
                self.my_max_num = len(page.object_list)
                if self.cl.show_all:
                    self._queryset = qs
                else:
                    self._queryset = page.object_list

        PaginationFormSet.per_page = self.per_page
        return PaginationFormSet


    def formfield_for_dbfield(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        form_field = super(QuestionEvidenceConfigurationInline, self).formfield_for_dbfield(db_field, request=request, **kwargs)
        if db_field.name in ('question', 'document_type'):
            form_field.widget.can_add_related = False
            form_field.widget.can_change_related = False
            form_field.widget.can_delete_related = False

        return form_field

    def get_max_num(self, request, obj=None, **kwargs):
        """Don't allow the addition of inline configuration on adding the parent model."""
        return 0 if not obj else self.max_num


class DynamicEvidenceConfigurationAdmin(BaseAdmin):
    model = DynamicEvidenceConfiguration
    inlines = [QuestionEvidenceConfigurationInline]
    list_display = ('name', 'assessment_type', 'is_default', 'import_and_export')
    ordering = ('name',)
    search_fields = ['uuid', 'name', 'assessment_type__code']


    def get_urls(self):
        urls = super(DynamicEvidenceConfigurationAdmin, self).get_urls()
        custom_urls = [
            url(
                r'^(?P<uuid>.+)/export/$',
                self.admin_site.admin_view(self.process_export),
                name='dec-export',
            ),
        ]
        return custom_urls + urls


    def process_export(self, request, uuid, *args, **kwargs):
        dec_name = request.GET.get('name','')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="Dynamic Evidence Export {} {}.csv"'.format(
            dec_name, datetime.now().strftime('%Y-%m-%d'))
        file = DynamicEvidenceConfigurationExport([uuid]).export_data().getvalue()
        response.write(file)
        return response



    def import_and_export(self, obj):
        return "<a class='button' href='{export_url}?name={dec_name}'>Export</a> " \
               "<a class='button' href='{import_url}?action_type=import&configuration={dec_uuid}'>Import</a> ".format(
            export_url= reverse('admin:dec-export', args=[obj.pk]),
            import_url = reverse('admin:assessments_dynamicevidenceimport_add'),
            dec_uuid=obj.pk, dec_name=obj
        )

    import_and_export.allow_tags = True
    import_and_export.short_description = _(u'Import & Export')


    def get_readonly_fields(self, request, obj=None):
        if obj and obj.pk:
            return self.readonly_fields + ('assessment_type', )
        else:
            return self.readonly_fields

    def formfield_for_dbfield(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        form_field = super(DynamicEvidenceConfigurationAdmin, self).formfield_for_dbfield(db_field, request=request, **kwargs)

        if db_field.name == 'assessment_type':
            form_field.widget.can_add_related = False
            form_field.widget.can_change_related = False
            form_field.widget.can_delete_related = False
            view_name = request.resolver_match.view_name
            if view_name == 'admin:assessments_dynamicevidenceconfiguration_change':
                form_field.widget.attrs['disabled'] = 'disabled'

        return form_field

    def change_view(self, request, object_id, form_url='', extra_context=None):
          def formfield_for_foreignkey(self, db_field, request, **kwargs):
              if db_field.name == 'question':
                  try:
                      dynamic_evidence_config = DynamicEvidenceConfiguration.objects.get(pk=object_id)
                      kwargs['queryset'] = Question.objects.filter(
                          questionnaire__assessment_type=dynamic_evidence_config.assessment_type
                      ).distinct()
                  except DynamicEvidenceConfiguration.DoesNotExist:
                      pass
              return super(QuestionEvidenceConfigurationInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

          QuestionEvidenceConfigurationInline.formfield_for_foreignkey = formfield_for_foreignkey

          self.inline_instances = [QuestionEvidenceConfigurationInline(self.model, self.admin_site)]

          return super(DynamicEvidenceConfigurationAdmin, self).change_view(
              request, object_id, form_url=form_url, extra_context=extra_context
          )


@admin.register(DynamicEvidenceImport)
class DynamicEvidenceImportAdmin(BaseAdmin):
    model = DynamicEvidenceImport
    form = DynamicEvidenceImportForm
    list_display = ('file', 'configuration', 'import_date', 'status', 'user', 'import_log', 'created_time')
    ordering = ('-created_time',)
    actions = ['import_selected_files']


    def import_selected_files(self, request, qs):
        for q in qs:
            q.status =_('Importing')
            q.import_log = ''
            q.save()

        ids = list(qs.values_list('uuid', flat=True))
        dynamic_evidence_import_task.delay(ids, request.user.pk)
        self.message_user(request, 'Import started in background. Please check later')


    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


    def has_delete_permission(self, request, obj=None):
        return False


    def get_actions(self, request):
        actions = super(DynamicEvidenceImportAdmin, self).get_actions(request)

        if 'delete_selected' in actions:
            del actions['delete_selected']

        return actions


@admin.register(QuestionLabel)
class QuestionLabelAdmin(BaseAdmin):
    model = QuestionLabel
    form = QuestionLabelAdminForm
    list_display = ('name', 'slug', 'created_time', 'modified_time')
    search_fields = ('uuid', 'name', 'slug')
    ordering = ('name',)


class QuestionLabelConfigurationInline(BaseTabularInline):
    model = QuestionLabelConfigurations
    max_num = None
    form = QuestionLabelConfigurationInlineForm
    raw_id_fields = ('question', 'label')
    ordering = ('question__code', 'label__name')
    per_page = 25
    template = 'admin/edit_inline/list.html'


    def get_formset(self, request, obj=None, **kwargs):
        formset_class = super(QuestionLabelConfigurationInline, self).get_formset(
            request, obj, **kwargs)

        class PaginationFormSet(formset_class):
            def __init__(self, *args, **kwargs):
                super(PaginationFormSet, self).__init__(*args, **kwargs)

                qs = self.queryset
                paginator = Paginator(qs, self.per_page)
                try:
                    page_num = int(request.GET.get('page', '1'))
                except ValueError:
                    page_num = 1

                try:
                    page = paginator.page(page_num)
                except (EmptyPage, InvalidPage):
                    page = paginator.page(paginator.num_pages)

                self.cl = InlineChangeList(request, page_num, paginator)

                self.paginator = paginator
                self.my_page = page
                self.my_max_num = len(page.object_list)
                if self.cl.show_all:
                    self._queryset = qs
                else:
                    self._queryset = page.object_list

        PaginationFormSet.per_page = self.per_page
        return PaginationFormSet


    def formfield_for_dbfield(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        form_field = super(QuestionLabelConfigurationInline, self).formfield_for_dbfield(db_field, request=request, **kwargs)
        if db_field.name in ('question', 'label'):
            form_field.widget.can_add_related = False
            form_field.widget.can_change_related = False
            form_field.widget.can_delete_related = False

        return form_field

    def get_max_num(self, request, obj=None, **kwargs):
        """Don't allow the addition of inline configuration on adding the parent model."""
        return 0 if not obj else self.max_num


@admin.register(DynamicLabelConfiguration)
class DynamicLabelConfigurationAdmin(BaseAdmin):
    model = DynamicLabelConfiguration
    list_display = ('name', )
    ordering = ('name',)
    search_fields = ['uuid', 'name']

    def change_view(self, request, object_id, form_url='', extra_context=None):
        # Show inline form on update screen
        self.inlines = [QuestionLabelConfigurationInline]
        return super(self.__class__, self).change_view(request, object_id)


    def add_view(self, request, form_url='', extra_context=None):
        # Hide inline form on add screen
        self.inlines = []
        return super(self.__class__, self).add_view(request)



class QuestionMappingsInline(BaseTabularInline):
    model = QuestionMappings
    max_num = None
    form = QuestionMappingsInlineForm
    ordering = ('source_question__code', 'target_question__code')
    per_page = 25
    template = 'admin/edit_inline/list.html'


    def get_formset(self, request, obj=None, **kwargs):
        formset_class = super(QuestionMappingsInline, self).get_formset(
            request, obj, **kwargs)

        class PaginationFormSet(formset_class):
            def __init__(self, *args, **kwargs):
                super(PaginationFormSet, self).__init__(*args, **kwargs)

                qs = self.queryset
                paginator = Paginator(qs, self.per_page)
                try:
                    page_num = int(request.GET.get('page', '1'))
                except ValueError:
                    page_num = 1

                try:
                    page = paginator.page(page_num)
                except (EmptyPage, InvalidPage):
                    page = paginator.page(paginator.num_pages)

                self.cl = InlineChangeList(request, page_num, paginator)

                self.paginator = paginator
                self.my_page = page
                self.my_max_num = len(page.object_list)
                if self.cl.show_all:
                    self._queryset = qs
                else:
                    self._queryset = page.object_list

        PaginationFormSet.per_page = self.per_page
        return PaginationFormSet


    def formfield_for_dbfield(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        form_field = super(QuestionMappingsInline, self).formfield_for_dbfield(db_field, request=request, **kwargs)
        if db_field.name in ('source_question', 'target_question'):
            form_field.widget.can_add_related = False
            form_field.widget.can_change_related = False
            form_field.widget.can_delete_related = False

        return form_field



@admin.register(QuestionsMappingConfiguration)
class QuestionsMappingConfigurationAdmin(BaseAdmin):
    model = QuestionsMappingConfiguration
    list_display = ('name', 'created_time', )
    ordering = ('name',)
    search_fields = ['uuid', 'name']


    def add_view(self, request, form_url='', extra_context=None):
        # Hide inline form on add screen
        self.inlines = []
        return super(self.__class__, self).add_view(request)


    def get_readonly_fields(self, request, obj=None):
        if obj and obj.pk:
            return self.readonly_fields + ('source_assessment_type', 'target_assessment_type', )
        else:
            return self.readonly_fields


    def formfield_for_dbfield(self, db_field, request=None, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        form_field = super(QuestionsMappingConfigurationAdmin, self).formfield_for_dbfield(db_field, request=request, **kwargs)

        if db_field.name in ('source_assessment_type','target_assessment_type'):
            form_field.widget.can_add_related = False
            form_field.widget.can_change_related = False
            form_field.widget.can_delete_related = False
            view_name = request.resolver_match.view_name
            if view_name == 'admin:assessments_questionsmappingconfigurationadmin_change':
                form_field.widget.attrs['disabled'] = 'disabled'

        return form_field



    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.inlines = [QuestionMappingsInline]

        def formfield_for_foreignkey(self, db_field, request, **kwargs):
            if db_field.name == 'source_question':
                try:
                    evidence_config = QuestionsMappingConfiguration.objects.get(pk=object_id)
                    kwargs['queryset'] = Question.objects.filter(
                        questionnaire__assessment_type=evidence_config.source_assessment_type
                    ).distinct()
                except QuestionsMappingConfiguration.DoesNotExist:
                    pass

            if db_field.name == 'target_question':
                try:
                    evidence_config = QuestionsMappingConfiguration.objects.get(pk=object_id)
                    kwargs['queryset'] = Question.objects.filter(
                        questionnaire__assessment_type=evidence_config.target_assessment_type
                    ).distinct()
                except QuestionsMappingConfiguration.DoesNotExist:
                    pass

            return super(QuestionMappingsInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

        QuestionMappingsInline.formfield_for_foreignkey = formfield_for_foreignkey
        self.inline_instances = [QuestionMappingsInline(self.model, self.admin_site)]

        return super(QuestionsMappingConfigurationAdmin, self).change_view(
            request, object_id, form_url=form_url, extra_context=extra_context
        )


@admin.register(BackgroundEvidenceCopy)
class BackgroundEvidenceCopyAdmin(BaseAdmin):
    model = BackgroundEvidenceCopy
    list_display = ('source_assessment', 'target_assessment', 'user', 'status', 'created_time', )
    raw_id_fields = ('source_assessment', 'target_assessment', 'user')
    search_fields = [
        'uuid', 'source_assessment__name', 'target_assessment__name', 'user__username',
        'status', 'source_assessment__uuid', 'target_assessment__uuid',
    ]
    ordering = ('-created_time',)
    readonly_fields = ['log',]



admin.site.register(AssessmentType, AssessmentTypeAdmin)
admin.site.register(PossibleAnswer, PossibleAnswerAdmin)
admin.site.register(QuestionnaireHeading, QuestionnaireHeadingAdmin)
admin.site.register(QuestionnaireParagraph, QuestionnaireParagraphAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(QuestionLevel, QuestionLevelAdmin)
admin.site.register(Questionnaire, QuestionnaireAdmin)
admin.site.register(QuestionTrigger, QuestionTriggerAdmin)
admin.site.register(DynamicEvidenceConfiguration, DynamicEvidenceConfigurationAdmin)
