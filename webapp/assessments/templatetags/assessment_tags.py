from django import template
from django.utils import dateformat, dateparse
from translations.templatetags.translation_filters import ugettext_filter_with_en
import HTMLParser

register = template.Library()


@register.filter
def get_mutation_role(mutation_log):
    return ugettext_filter_with_en(mutation_log.role.name) if mutation_log.role else ''


@register.filter
def datetime_with_timezone(value):
    frmt = "d/m/y H:i O"

    if isinstance(value, basestring):
        value = dateparse.parse_datetime(value)

    time_str = dateformat.format(value, frmt)
    return ' ' + time_str[:-2] + ':' + time_str[-2:]


import re
cleanr = re.compile('<.*?>')


@register.filter
def clean_html(raw_html):
    clean_text = re.sub(cleanr, '', raw_html)
    return clean_text


@register.filter
def decode(value):
    """HTML decodes a string """
    h = HTMLParser.HTMLParser()
    return h.unescape(value)
