import json
from django.conf import settings
import tablib
from copy import deepcopy
from datetime import datetime

from django.utils.text import slugify

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

from django.contrib.auth import get_user_model
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist, PermissionDenied
from django.core.files.base import ContentFile
from django.db import IntegrityError
from django.db import transaction
from django.utils.translation import ugettext_lazy as _

import csv
from operator import or_
from django.db.models import Q

from assessments.models import QuestionEvidenceConfigurations, QuestionsMappingConfiguration, QuestionMappings, \
    BackgroundEvidenceCopy
from attachments.models import DocumentType

from assessments.models import (
    AssessmentDocumentLink, Answer, QuestionnaireState,
    AssessmentType, Questionnaire, QuestionLevel, AssessmentTypeSection, PossibleAnswerSet, PossibleAnswer,
    QuestionnaireHeading, QuestionnaireParagraph, Question, QuestionTrigger, QuestionTriggerCondition,
    AssessmentTypeStandard, QuestionnaireElement, Assessment, AssessmentSharePermission)

from attachments.models import Attachment, FileAttachment, TextReferenceAttachment, AgriformAttachment, \
    NotApplicableAttachment, FormAttachment, upload_to_organization_folder
from organization.models import Product, ProductPartner, ProductIdentifier


from boto.s3.connection import S3Connection


def get_bucket():
    bucket = None
    if settings.DEFAULT_FILE_STORAGE == 'storages.backends.s3boto.S3BotoStorage':
        conn = S3Connection(
            settings.AWS_ACCESS_KEY_ID,
            settings.AWS_SECRET_ACCESS_KEY
        )

        bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
    return bucket


def get_new_filename(old_attachment):
    url = old_attachment.file.name.split('.')
    ext = ""
    if len(url) > 1:
        ext = url.pop(-1)
    name = url.pop(-1).split('/')[-1]
    name = '.'.join([name, ext]) if ext else name
    return name


def create_object_copy(object):
    """
    :param object: Object
    :return: New instance with same values for fields
    """
    new_object = deepcopy(object)
    new_object.pk = None
    new_object.id = None
    new_object.uuid = None
    new_object.save()
    return new_object


def create_answer_copy(answer, target_assessment, new_organization=None):
    new_answer = Answer.objects.create(
        question=answer.question,
        value=answer.value,
        value_uuids=answer.value_uuids,
        value_old=answer.value_old,
        justification=answer.justification,
        assessment=target_assessment,
        target_uuid=answer.target_uuid,
        organization=new_organization if new_organization else answer.organization,
        organization_unit=answer.organization_unit,
        product=answer.product,
        timestamp=answer.timestamp,
        author=answer.author,
    )
    new_answer.save()
    return new_answer


def create_questionnaire_state_copy(questionnaire_state, target_assessment):
    new_questionnaire_state = QuestionnaireState.objects.create(
        assessment=target_assessment,
        questionnaire=questionnaire_state.questionnaire,
        is_complete=questionnaire_state.is_complete,
        is_signed=False
    )
    return new_questionnaire_state


def create_agriform_copy(agriform, new_organization=None):
    """
    Making a copy of agriform with new answers(with same values)
    :param agriform: Object
    :return: new agriform(Assessment) instance
    """

    new_agriform = create_object_copy(agriform)

    answers_to_copy = Answer.objects.filter(assessment=agriform)
    if answers_to_copy:
        # make copy of all connected answers
        for answer in answers_to_copy:
            new_answer = create_answer_copy(answer, new_agriform, new_organization=new_organization)
            new_answer.save()

    questionnaire_states_to_copy = QuestionnaireState.objects.filter(assessment=agriform)
    if questionnaire_states_to_copy:
        # make copy of all QuestionnaireState instances
        for questionnaire_state in questionnaire_states_to_copy:
            create_questionnaire_state_copy(questionnaire_state, new_agriform)

    if new_organization:
        new_agriform.organization = new_organization
    new_agriform.save()
    return new_agriform


def create_attachment_copy(attachment, new_organization=None, bucket=None):
    """
    :param attachment: Object
    :return: new attachment instance
    """
    old_attachment = Attachment.objects.get_subclass(pk=attachment.uuid)
    new_attachment = create_object_copy(old_attachment)
    attachment_type_lower = new_attachment.get_attachment_type().lower()
    if attachment_type_lower == 'agriformattachment':
        new_attachment.assessment = create_agriform_copy(old_attachment.assessment, new_organization=new_organization)
    elif attachment_type_lower == 'formattachment':
        new_attachment.generic_form_data = create_object_copy(old_attachment.generic_form_data)
    elif attachment_type_lower == 'fileattachment':
        if settings.DEFAULT_FILE_STORAGE == 'storages.backends.s3boto.S3BotoStorage':
            # Copy the attachment to the new organization folder.
            if new_organization:
                new_attachment.organization = new_organization
            new_filename = get_new_filename(old_attachment)
            new_key_name = upload_to_organization_folder(new_attachment, new_filename)
            bucket = get_bucket() if not bucket else bucket
            new_key = bucket.copy_key(new_key_name, bucket.name, old_attachment.file.name)
            new_attachment.file.name = new_key.name
        else:
            # Make a copy of the physical file.
            tmp_file = StringIO(old_attachment.file.read())
            tmp_file = ContentFile(tmp_file.getvalue())
            tmp_file.name = get_new_filename(old_attachment)
            new_attachment.file = tmp_file
            tmp_file.close()
    # Copy the attachment to the new organization folder.
    if new_organization:
        new_attachment.organization = new_organization
    new_attachment.save()
    return new_attachment


def get_is_done(attachment_link, is_done=None):
    """
    :param attachment_link:
    :param is_done: None or Boolean. If None just copy, elif Boolean value set value from param
    :return:
    """
    if attachment_link.attachment.get_attachment_type().lower() in ['agriformattachment', 'formattachment']:
        return False
    elif is_done is None:
        return attachment_link.is_done
    else:
        return is_done


def create_shared_attachment_copy(attachment, curr_organization, user, target_assessment=None, bucket=None):
    attachment_copy = create_attachment_copy(
        attachment,
        new_organization=curr_organization,
        bucket=bucket
    )
    for attachment_link in attachment.assessment_links_attachments.all():
        AssessmentDocumentLink.objects.create_attachment_link_copy(
            attachment_link, attachment_copy, user, target_assessment, curr_organization
        )

    return attachment_copy


def make_attachment_link_replicator(author, target_assessment, organization=None, bucket=None):
    """
    :param author: Object
    :param target_assessment: Object
    :return: replicator function which generates a copy of attachment_link in target_assessment.
    author and is_done properties will be assigned to the copy
    """
    @transaction.atomic
    def replicator(attachment_link):
        new_attachment_link = AssessmentDocumentLink.objects.create(
            assessment=target_assessment,
            attachment=create_attachment_copy(attachment_link.attachment, new_organization=organization, bucket=bucket),
            document_type_attachment=attachment_link.document_type_attachment,
            author=author,
            organization=organization,
            timestamp=datetime.today()
        )
        new_attachment_link.is_done = get_is_done(new_attachment_link, attachment_link.is_done)
        new_attachment_link.save()
        return new_attachment_link

    return replicator


class StandardExport(object):
    def __init__(self, standards, user_id):
        self.standards = standards
        self.user_id = user_id

    def export_standards(self):
        for assessment_type in self.standards:
            now = datetime.now()
            file_name = u'{}-standard-{}.json'.format(slugify(assessment_type.name), now.strftime("%d.%m.%y-%H:%M:%S"))
            standard_json = self.get_standard_json(assessment_type.code)

            standard = AssessmentTypeStandard()
            standard.file.save(file_name, ContentFile(standard_json))
            standard.status = _("Success")
            standard.user = get_user_model().objects.get(pk=self.user_id)
            standard.save()

    @staticmethod
    def get_standard_json(assessment_code):
        from assessments.admin import (
            AssessmentTypeResource, QuestionnaireResource, QuestionLevelResource,
            AssessmentTypeSectionResource, PossibleAnswerSetResource, PossibleAnswerResource, HeadingResource,
            ParagraphResource, QuestionResource, QuestionTriggerResource, QuestionTriggerConditionResource,
        )
        from attachments.admin import DocumentTypeResource, DocumentType

        code = assessment_code

        order_by_column = 'uuid'

        q1 = AssessmentType.objects.filter(code=code).order_by(order_by_column)
        q2 = Questionnaire.objects.filter(assessment_type__code=code).order_by(order_by_column)
        q3 = QuestionLevel.objects.filter(assessment_type__code=code).order_by(order_by_column)
        q4 = AssessmentTypeSection.objects.filter(assessment_type__code=code).order_by(order_by_column)
        q5 = DocumentType.objects.filter(questions_attachments__questionnaire=q2).order_by(order_by_column)
        q6 = PossibleAnswerSet.objects.filter(assessment_type__code=code).order_by(order_by_column)
        q7 = PossibleAnswer.objects.filter(possible_answer_set=q6).order_by(order_by_column)
        q8 = QuestionnaireHeading.objects.filter(questionnaire__assessment_type__code=code).order_by(order_by_column)
        q9 = QuestionnaireParagraph.objects.filter(questionnaire__assessment_type__code=code).order_by(order_by_column)
        q10 = Question.objects.filter(questionnaire=q2).order_by(order_by_column)
        questionnaire_elements = QuestionnaireElement.objects.filter(questionnaire=q2).order_by(order_by_column)
        q11 = QuestionTrigger.objects.filter(target_element=questionnaire_elements).order_by(order_by_column)
        q12 = QuestionTriggerCondition.objects.filter(trigger=q11).order_by(order_by_column)

        r1 = AssessmentTypeResource().export(q1)
        r2 = QuestionnaireResource().export(q2)
        r3 = QuestionLevelResource().export(q3)
        r4 = AssessmentTypeSectionResource().export(q4)
        r5 = DocumentTypeResource().export(q5)
        r6 = PossibleAnswerSetResource().export(q6)
        r7 = PossibleAnswerResource().export(q7)
        r8 = HeadingResource().export(q8)
        r9 = ParagraphResource().export(q9)
        r10 = QuestionResource().export(q10)
        r11 = QuestionTriggerResource().export(q11)
        r12 = QuestionTriggerConditionResource().export(q12)

        standard_dict = '{{"assessment_type": {assessment_type},"questionnaires": {questionnaires},' \
                        '"question_levels": {question_levels},"assessment_type_section": {assessment_type_section},' \
                        '"document_types": {document_types},' \
                        '"possible_answer_sets": {possible_answers_set},"possible_answers": {possible_answers},' \
                        '"questionnaire_headings": {questionnaire_headings},' \
                        '"questionnaire_paragraphs": {questionnaire_paragraphs},"questions": {questions},' \
                        '"question_triggers": {question_triggers},' \
                        '"question_trigger_conditions": {question_triggers_condition}}}'

        return standard_dict.format(
            assessment_type=r1.json,
            questionnaires=r2.json,
            question_levels=r3.json,
            assessment_type_section=r4.json,
            document_types=r5.json,
            possible_answers_set=r6.json,
            possible_answers=r7.json,
            questionnaire_headings=r8.json,
            questionnaire_paragraphs=r9.json,
            questions=r10.json,
            question_triggers=r11.json,
            question_triggers_condition=r12.json
        )


class StandardImport(object):
    def __init__(self, standards, user_id):
        self.standards = standards
        self.user_id = user_id

    def import_standards(self):
        for standard in self.standards:
            try:
                self.import_standard(standard=standard)
                outcome = u"Imported '{0}'. Success".format(standard.file.name)
                status = _('Success')
            except IntegrityError as ie:
                outcome = u"Error importing '{0}'. Error: '{1}'".format(standard.file.name, ie.message)
                status = _('Error')
            except MultipleObjectsReturned as mor:
                outcome = u"Error importing '{0}'. Error: '{1}'".format(standard.file.name, mor.message)
                status = _('Error')
            except UnicodeDecodeError as ude:
                outcome = u"Error importing '{0}'. Error: '{1}'".format(standard.file.name, ude)
                status = _('Error')
            except Exception as e:
                outcome = u"Error importing '{0}'. Error: '{1}'".format(standard.file.name, e.message)
                status = _('Error')

            finally:
                standard.status = status
                standard.import_outcome = outcome
                standard.import_date = datetime.now()
                standard.user = get_user_model().objects.get(pk=self.user_id)
                standard.save()

    @transaction.atomic
    def import_standard(self, standard):

        from assessments.admin import (
            AssessmentTypeResource, QuestionnaireResource, QuestionLevelResource,
            AssessmentTypeSectionResource, PossibleAnswerSetResource, PossibleAnswerResource, HeadingResource,
            ParagraphResource, QuestionResource, QuestionTriggerResource, QuestionTriggerConditionResource,
            ASSESSMENT_TYPE_RESOURCE_FIELDS, QUESTIONNAIRE_RESOURCE_FIELDS, QUESTION_LEVEL_RESOURCE_FIELDS,
            ASSESSMENT_TYPE_SECTION_RESOURCE_FIELDS, POSSIBLE_ANSWER_SET_RESOURCE_FIELDS,
            POSSIBLE_ANSWER_RESOURCE_FIELDS, HEADING_RESOURCE_FIELDS, PARAGRAPH_RESOURCE_FIELDS,
            QUESTION_RESOURCE_FIELDS,
            QUESTION_TRIGGER_RESOURCE_FIELDS, QUESTION_TRIGGER_CONDITION_RESOURCE_FIELDS
        )
        from attachments.admin import DocumentTypeResource, DOCUMENT_TYPE_RESOURCE_FIELDS

        file_json = self._load_json(standard)
        assessment_type_data_set = self._get_data_set(
            json_items=file_json.get('assessment_type'), headers=ASSESSMENT_TYPE_RESOURCE_FIELDS, validate_edition=True
        )
        questionnaires_data_set = self._get_data_set(
            json_items=file_json.get('questionnaires'), headers=QUESTIONNAIRE_RESOURCE_FIELDS
        )
        question_levels_data_set = self._get_data_set(
            json_items=file_json.get('question_levels'), headers=QUESTION_LEVEL_RESOURCE_FIELDS
        )
        assessment_type_section_data_set = self._get_data_set(
            json_items=file_json.get('assessment_type_section'), headers=ASSESSMENT_TYPE_SECTION_RESOURCE_FIELDS
        )
        document_types_data_set = self._get_data_set(
            json_items=file_json.get('document_types'), headers=DOCUMENT_TYPE_RESOURCE_FIELDS
        )
        possible_answer_sets_data_set = self._get_data_set(
            json_items=file_json.get('possible_answer_sets'), headers=POSSIBLE_ANSWER_SET_RESOURCE_FIELDS
        )
        possible_answer_data_set = self._get_data_set(
            json_items=file_json.get('possible_answers'), headers=POSSIBLE_ANSWER_RESOURCE_FIELDS
        )
        headings_data_set = self._get_data_set(
            json_items=file_json.get('questionnaire_headings'), headers=HEADING_RESOURCE_FIELDS,
            sort_by_column='parent_element'
        )
        paragraphs_data_set = self._get_data_set(
            json_items=file_json.get('questionnaire_paragraphs'), headers=PARAGRAPH_RESOURCE_FIELDS,
            sort_by_column='parent_element'
        )
        questions_data_set = self._get_data_set(
            json_items=file_json.get('questions'), headers=QUESTION_RESOURCE_FIELDS,
            sort_by_column='parent_element'
        )
        question_triggers_data_set = self._get_data_set(
            json_items=file_json.get('question_triggers'), headers=QUESTION_TRIGGER_RESOURCE_FIELDS
        )
        question_trigger_conditions_data_set = self._get_data_set(
            json_items=file_json.get('question_trigger_conditions'),
            headers=QUESTION_TRIGGER_CONDITION_RESOURCE_FIELDS
        )

        self._import_data(
            resource_model=AssessmentTypeResource(), data_set=assessment_type_data_set
        )
        self._import_data(
            resource_model=QuestionnaireResource(), data_set=questionnaires_data_set
        )
        self._import_data(
            resource_model=QuestionLevelResource(), data_set=question_levels_data_set
        )
        self._import_data(
            resource_model=AssessmentTypeSectionResource(), data_set=assessment_type_section_data_set
        )
        self._import_data(
            resource_model=DocumentTypeResource(), data_set=document_types_data_set
        )
        self._import_data(
            resource_model=PossibleAnswerSetResource(), data_set=possible_answer_sets_data_set
        )
        self._import_data(
            resource_model=PossibleAnswerResource(), data_set=possible_answer_data_set
        )
        self._import_data(
            resource_model=HeadingResource(), data_set=headings_data_set
        )
        self._import_data(
            resource_model=ParagraphResource(), data_set=paragraphs_data_set
        )
        self._import_data(
            resource_model=QuestionResource(), data_set=questions_data_set
        )
        self._import_data(
            resource_model=QuestionTriggerResource(), data_set=question_triggers_data_set
        )
        self._import_data(
            resource_model=QuestionTriggerConditionResource(), data_set=question_trigger_conditions_data_set
        )

    @classmethod
    def _load_json(cls, standard):
        standard_file = standard.file
        standard_file.open(mode='rb')
        file_contents = standard_file.read()
        standard_file.close()

        return json.loads(file_contents)

    @classmethod
    def _get_data_set(cls, json_items, headers, sort_by_column=None, reverse=False, validate_edition=False):

        data_set = tablib.Dataset()
        data_set.headers = headers

        for item in json_items:
            data = []
            for header in headers:
                try:
                    data.append(item[header])
                except KeyError as exp:
                    if header == 'edition'and validate_edition:
                        raise Exception(
                            "\"Version\" attribute is obsolete. Now \"edition\" attribute is required in standard."
                        )
                    else:
                        raise exp
            data_set.append(data)

        return data_set if sort_by_column is None else data_set.sort(col=sort_by_column, reverse=reverse)

    def _import_data(self, resource_model, data_set):
        print("Import started for: {}".format(resource_model.__class__.__name__))
        resource_model.import_data(data_set, dry_run=False, raise_errors=True)
        print("Import finished for: {}".format(resource_model.__class__.__name__))


class AssessmentExport(object):
    def __init__(self, assessment):
        self.assessment = assessment

    def export_assessments(self):
        assessment_shares = self.get_assessment_shares()
        assessment_share_uuids = self.get_assessment_share_uuids(assessment_shares)

        standard_json = {
            "assessment": self.get_assessment_json(),
            "document_links": self.get_document_links_json(),
            "questionnaire_states": self.get_questionnaire_state(),
            "questionnaire_element_states": self.get_questionnaire_element_states(),
            "answers": self.get_all_answers(),
            "judgements": self.get_judgement(),
            "assessment_results": self.get_assessment_results(),
            "assessment_shares": assessment_shares,
            "assessment_share_permissions": self.get_assessment_share_permissions(assessment_share_uuids),
            "products": self.get_assessment_products(),
            "product_partners": self.get_product_partners(),
            "product_identifiers": self.get_product_identifiers(),
            "attachments": self.get_attachments()
        }

        return standard_json

    def get_assessment_json(self):
        from assessments.admin import (
            AssessmentResource
        )

        assessments = [self.assessment]
        assessment_resources = AssessmentResource().export(assessments)
        return json.loads(assessment_resources.json)

    def get_document_links_json(self):
        from assessments.admin import AssessmentDocumentLinkResource

        document_links = self.assessment.document_links.order_by('id').select_related().all()
        return json.loads(AssessmentDocumentLinkResource().export(document_links).json)

    def get_questionnaire_state(self):
        from assessments.admin import QuestionnaireStateResource

        questionnaire_states = self.assessment.questionnaire_states.order_by('created_time').all()
        return json.loads(QuestionnaireStateResource().export(questionnaire_states).json)

    def get_questionnaire_element_states(self):
        from assessments.admin import QuestionnaireElementStateResource

        questionnaire_element_states = self.assessment.questionnaire_element_states.order_by('created_time').all()
        return json.loads(QuestionnaireElementStateResource().export(questionnaire_element_states).json)

    def get_all_answers(self):
        from assessments.admin import AnswerResource
        answers = self.assessment.answers.order_by('created_time').all()
        return json.loads(AnswerResource().export(answers).json)

    def get_judgement(self):
        from assessments.admin import JudgementResource
        judgements = self.assessment.judgements.order_by('created_time').all()
        return json.loads(JudgementResource().export(judgements).json)

    def get_assessment_results(self):
        from assessments.admin import AssessmentResultResource

        results = self.assessment.results.order_by('created_time').all()
        return json.loads(AssessmentResultResource().export(results).json)

    def get_assessment_shares(self):
        from assessments.admin import AssessmentShareResource
        shares = self.assessment.shares.order_by('created_time').all()
        shares_json = json.loads(AssessmentShareResource().export(shares).json)
        return shares_json

    def get_assessment_share_permissions(self, assessment_share_uuids):
        from assessments.admin import AssessmentSharePermissionResource
        share_permissions = AssessmentSharePermission.objects.filter(
            assessment_share__uuid__in=assessment_share_uuids
        ).order_by('created_time')
        share_permissions_json = json.loads(AssessmentSharePermissionResource().export(share_permissions).json)
        return share_permissions_json

    def get_assessment_products(self):
        from assessments.admin import ProductResource
        products = Product.objects.filter(assessments=self.assessment).order_by('created_time')
        return json.loads(ProductResource().export(products).json)

    def get_product_partners(self):
        from assessments.admin import ProductPartnerResource
        product_partners = ProductPartner.objects.filter(product__assessments=self.assessment).order_by('created_time')
        return json.loads(ProductPartnerResource().export(product_partners).json)

    def get_product_identifiers(self):
        from assessments.admin import ProductIdentifierResource
        product_identifiers = ProductIdentifier.objects.filter(
            product__assessments=self.assessment).order_by('id')
        return json.loads(ProductIdentifierResource().export(product_identifiers).json)

    def get_attachments(self):
        from assessments.admin import (
            AttachmentResource, FileAttachmentResource, TextReferenceAttachmentResource, AgriformAttachmentResource,
            FormAttachmentResource, GenericFormDataResource, GenericFormResource
        )

        attachments_data = []
        attachments = Attachment.objects.filter(
            assessment_links_attachments__assessment=self.assessment).order_by('created_time')

        for attachment in attachments:
            data = AttachmentResource().export([attachment]).json
            data = json.loads(data)[0]
            child_object = Attachment.objects.get_subclass(pk=attachment.pk)
            if isinstance(child_object, FileAttachment):
                subclass_data = FileAttachmentResource().export([child_object]).json
                subclass_data = json.loads(subclass_data)[0]
                subclass_data['attachment_type'] = 'FileAttachment'
                data['subclass_data'] = subclass_data
            if isinstance(child_object, TextReferenceAttachment):
                subclass_data = TextReferenceAttachmentResource().export([child_object]).json
                subclass_data = json.loads(subclass_data)[0]
                subclass_data['attachment_type'] = 'TextReferenceAttachment'
                data['subclass_data'] = subclass_data
            if isinstance(child_object, AgriformAttachment):
                subclass_data = AgriformAttachmentResource().export([child_object]).json
                subclass_data = json.loads(subclass_data)[0]
                subclass_data['attachment_type'] = 'AgriformAttachment'
                data['subclass_data'] = subclass_data
            if isinstance(child_object, FormAttachment):
                subclass_data = FormAttachmentResource().export([child_object]).json
                subclass_data = json.loads(subclass_data)[0]
                subclass_data['attachment_type'] = 'FormAttachment'

                generic_form_data = GenericFormDataResource().export([child_object.generic_form_data]).json
                generic_form_data = json.loads(generic_form_data)[0]
                subclass_data['GenericFormData'] = generic_form_data

                generic_form = GenericFormResource().export([child_object.generic_form_data.generic_form]).json
                generic_form = json.loads(generic_form)[0]
                subclass_data['GenericForm'] = generic_form

                data['subclass_data'] = subclass_data
            if isinstance(child_object, NotApplicableAttachment):
                subclass_data = {"attachment_type": "NotApplicableAttachment"}
                data['subclass_data'] = subclass_data

            attachments_data.append(data)
        return attachments_data

    @staticmethod
    def get_assessment_share_uuids(assessment_shares):
        return [assessment_share["uuid"] for assessment_share in assessment_shares]



class DynamicEvidenceConfigurationImport(object):

    def __init__(self, dynamic_configurations_import, user_id):
        self.dynamic_configurations_import = dynamic_configurations_import
        self.user_id = user_id


    def import_data(self):
        for dec_import in self.dynamic_configurations_import:
            status = _('Error')
            log_text = _('Unable to perform import')
            try:
                self.import_configuration(dec_import)
                status = _('Success')
                log_text = _('File imported successfully')
            except ValueError as e:
                status = _('Error')
                log_text = _(e.message)
            finally:
                dec_import.status = status
                dec_import.user = get_user_model().objects.get(pk=self.user_id)
                dec_import.import_date = datetime.now()
                dec_import.import_log = log_text
                dec_import.save()


    @transaction.atomic
    def import_configuration(self, dec_import):

        csv_file = csv.DictReader(dec_import.file)

        # Check the required keys in the file
        if 'document_code' and 'question_code' not in csv_file.fieldnames:
            raise ValueError('Required fields are not available in the file.')

        # Converting to list so it can be iterable
        csv_file = list(csv_file )

        # Striping list for extra spaces end removing rows with empty document & question code
        csv_file =  [ {k:str(v).strip() for k,v in x.iteritems()}
                      for x in csv_file
                      if x.get('document_code').strip() != '' and x.get('question_code').strip() != '']

        # Select all question evidences provided by user
        user_question_evidences = QuestionEvidenceConfigurations.objects.only('uuid')\
            .filter(configuration=dec_import.configuration.uuid)\
            .filter(
            reduce(or_, [
                (Q(question__code=x['question_code']) & Q(document_type__code=x['document_code'])) for x in csv_file
                ])).values_list('uuid', flat=True).all()

        # Delete all question evidences excepts user provided data
        QuestionEvidenceConfigurations.objects.filter(configuration=dec_import.configuration.uuid)\
            .exclude(pk__in=user_question_evidences)\
            .delete()

        for line in csv_file:
            try:
                ev_question = Question.objects.only('pk').get(code=line['question_code'])
                ev_document = DocumentType.objects.only('pk').get(code=line['document_code'])

                QuestionEvidenceConfigurations.objects.update_or_create(
                    configuration=dec_import.configuration,
                    question=ev_question,
                    document_type=ev_document
                )
            except Question.DoesNotExist:
                raise ValueError('Invalid question {}.'.format(line['question_code']))
            except DocumentType.DoesNotExist:
                raise ValueError('Invalid document type code {}.'.format(line['document_code']))



class DynamicEvidenceConfigurationExport(object):

    def __init__(self, ids):
        self.ids = ids

    def export_data(self):
        ids = self.ids
        data = QuestionEvidenceConfigurations.objects \
            .only('document_type__code', 'question__code').filter(configuration__uuid__in=ids) \
            .select_related('question') \
            .select_related('document_type').order_by('question__code',)

        file = StringIO()
        writer = csv.writer(file, dialect='excel')
        writer.writerow(['question_code', 'document_code'])

        writer.writerows([[x.question.code, x.document_type.code] for x in data])

        return file



SEE_ATTACHMENT = ('see attachment', 'ver archivo adjunto', 'zie bijlage')

class AssessmentEvidenceCopyUtility(object):

    def __init__(self, source_assessment, target_assessment, user):
        self.source_assessment = source_assessment
        self.target_assessment = target_assessment
        self.user = user

    def start(self):
        result = []
        # copy from
        source_assessment = self.source_assessment

        # copy to
        target_assessment = self.target_assessment

        if source_assessment and target_assessment:
            if source_assessment.assessment_type_id == target_assessment.assessment_type_id:
                source_attachment_links = source_assessment.document_links.all()
                bucket = get_bucket()
                replicator = make_attachment_link_replicator(self.user, target_assessment, bucket=bucket)
                map(replicator, source_attachment_links)

                self.autoanswering_for_documents(target_assessment)
                self.copy_answer_justifications(source_assessment, target_assessment)
                return True
            else:
                # Cross assessment copying
                return self.copy_documents_from_mapping()
        else:
            raise Exception('No source and target assessment set')




    def copy_documents_from_mapping(self):
        source_assessment = self.source_assessment
        target_assessment = self.target_assessment

        try:
            mapping_conf = QuestionsMappingConfiguration.objects.filter(
                (Q(source_assessment_type=source_assessment.assessment_type) & Q(target_assessment_type=target_assessment.assessment_type)) |
                (Q(source_assessment_type=target_assessment.assessment_type) & Q(target_assessment_type=source_assessment.assessment_type))
            ).select_related('source_assessment_type', 'target_assessment_type').get()
        except Exception:
            raise Exception('No question mapping found for these assessments')

        dec_req_source = source_assessment.get_workflow_context().get_configuration()
        dec_req_target = target_assessment.get_workflow_context().get_configuration()

        question_mappings = QuestionMappings.objects.filter(
            configuration=mapping_conf
        ).select_related('source_question', 'target_question').all()

        copy_case = "target_to_source" if mapping_conf.source_assessment_type == source_assessment.assessment_type \
            else "source_to_target"

        assessment_from = source_assessment
        assessment_to = target_assessment

        dec_from = dec_req_source
        dec_to = dec_req_target

        dec_questions_evidence_from = QuestionEvidenceConfigurations.objects.filter(
            configuration=dec_from).select_related('document_type')
        dec_questions_evidence_map_from = {x.question_id: x for x in dec_questions_evidence_from}

        dec_questions_evidence_to = QuestionEvidenceConfigurations.objects.filter(
            configuration=dec_to).select_related('document_type','question')
        dec_questions_evidence_map_to = {x.question_id: x for x in dec_questions_evidence_to}

        dec_questions_evidence_map_to_by_document = {}
        for x in dec_questions_evidence_to:
            dec_questions_evidence_map_to_by_document.setdefault(x.document_type.uuid,[]).append(x.question)


        documents_source_assesment = AssessmentDocumentLink.objects.filter(
            assessment=assessment_from
        ).select_related('document_type_attachment').all()


        documents_source_assesment_map = {}
        for x in documents_source_assesment:
            documents_source_assesment_map.setdefault(x.document_type_attachment.uuid,[]).append(x)

        bucket = get_bucket()
        replicator = make_attachment_link_replicator(self.user, assessment_to, bucket=bucket)

        agriforms = AgriformAttachment.objects.filter(
            assessment_links_attachments=documents_source_assesment
        ).values_list('uuid', flat=True)

        # Holds uuid of the evidences which are already replicated.
        copied_evidence_docs = []
        question_ignore_for_agriform = {}
        bulk_answers = []

        for mapping in question_mappings:
            if copy_case == "target_to_source":
                map_question_to = mapping.target_question
                map_question_from = mapping.source_question

            elif copy_case == "source_to_target":
                map_question_to = mapping.source_question
                map_question_from = mapping.target_question


            # flag to check evidence had document
            had_doc = False
            had_agriform_only = False

            if map_question_from.uuid in dec_questions_evidence_map_from and map_question_to.uuid in dec_questions_evidence_map_to:
                docs_dec_questions_evidence_from = dec_questions_evidence_map_from[map_question_from.uuid]
                docs_dec_questions_evidence_to = dec_questions_evidence_map_to[map_question_to.uuid]

                # Check the both document types through DEC and also checking the evidence is available
                # in previous assessment
                if docs_dec_questions_evidence_from.document_type == docs_dec_questions_evidence_to.document_type \
                        and docs_dec_questions_evidence_from.document_type.uuid in documents_source_assesment_map:

                    # To stop duplication of evidence copying
                    if docs_dec_questions_evidence_from.document_type.uuid not in copied_evidence_docs:
                        attachments = []
                        for x in documents_source_assesment_map[docs_dec_questions_evidence_from.document_type.uuid]:
                            # COPYING THE DOCUMENT!!!
                            replicator(x)
                            attachments.append(x.attachment_id)

                        copied_evidence_docs.append(docs_dec_questions_evidence_from.document_type.uuid)

                        attach_len = len(set(attachments) & set(agriforms))
                        if any(x in agriforms for x in attachments) and attach_len == len(attachments):
                            had_agriform_only = True
                            question_ignore_for_agriform.setdefault(map_question_to.uuid,[]).extend(attachments)

                    had_doc = True

            try:
                source_answer = Answer.objects.get(assessment=assessment_from, question=map_question_from)

                if had_doc is True:
                    # Copy justification text as it is if evidence had documents
                    justification_text = source_answer.justification
                else:
                    # Copying justification is text is not equal to See attachment
                    justification_text = source_answer.justification
                    if justification_text.lower() in SEE_ATTACHMENT:
                        continue

                had_already_answered = Answer.objects.filter(question=map_question_to,assessment=assessment_to).count()
                if had_already_answered < 1:
                    target_answer = Answer(
                        question=map_question_to,
                        assessment=assessment_to,
                        justification=justification_text
                    )
                    if had_doc is True and had_agriform_only is not True:
                        target_answer.value = map_question_to.evidence_default_answer

                    bulk_answers.append(target_answer)
                else:
                    to_save = {}
                    to_save['justification'] = justification_text
                    if had_doc is True and had_agriform_only is not True:
                        to_save['value'] = map_question_to.evidence_default_answer
                    Answer.objects.filter(question=map_question_to, assessment=assessment_to).update(**to_save)

            except ObjectDoesNotExist:
                pass

        Answer.objects.bulk_create(bulk_answers)
        self.autoanswering_for_documents(assessment_to, dec_to)

        # return Response({'status':'ok'}, status.HTTP_201_CREATED)
        return True


    def autoanswering_for_documents(self, target_assessment, dec = None):
        if not dec:
            dec = target_assessment.get_workflow_context().get_configuration()

        docs = DocumentType.objects.filter(
            assessment_document_links_attachments__assessment=target_assessment,
            is_optional=False).distinct()

        doc_questions = Question.objects.filter(
            configurations__document_type=docs,
            configurations__configuration=dec
        )

        current_ans = Question.objects.filter(answers__assessment=target_assessment)

        document_links = AssessmentDocumentLink.objects.filter(
            assessment=target_assessment
        ).select_related('document_type_attachment').all()
        document_links_map = {}
        for x in document_links:
            document_links_map.setdefault(x.document_type_attachment_id, []).append(x)

        agriforms = AgriformAttachment.objects.filter(assessment_links_attachments=document_links).values_list(
            'uuid', flat=True)

        dec_questions = QuestionEvidenceConfigurations.objects.filter(configuration=dec)
        dec_question_to_doc_link_map = {}
        for x in dec_questions:
            if x.document_type_id in document_links_map:
                dec_question_to_doc_link_map.setdefault(x.question_id, []).append(document_links_map[x.document_type_id])

        blk_ans = []
        for q in doc_questions:
            if q not in current_ans:
                a = Answer(
                    assessment=target_assessment,
                    question=q,
                    justification=_('See attachment'),
                )
                attachments = sum([[y.attachment_id for y in x] for x in dec_question_to_doc_link_map[q.uuid] ],[])
                if any(x in agriforms for x in attachments):
                    if len(set(attachments) & set(agriforms)) != len(attachments):
                        a.value = q.evidence_default_answer
                else:
                    a.value = q.evidence_default_answer

                blk_ans.append(a)

        Answer.objects.bulk_create(blk_ans)


    @transaction.atomic
    def copy_answer_justifications(self, source_assessment, target_assessment):
        source_answer = Answer.objects.filter(assessment=source_assessment).select_related('question')
        current_ans = Question.objects.filter(answers__assessment=target_assessment)

        if source_answer:
            blk_create = []
            for x in source_answer:
                if x.question not in current_ans:
                    a = Answer(
                        assessment=target_assessment,
                        question=x.question,
                        justification=x.justification
                    )
                    blk_create.append(a)
                else:
                    Answer.objects.filter(
                        assessment=target_assessment,
                        question=x.question
                    ).update(justification=x.justification)


            Answer.objects.bulk_create(blk_create)

