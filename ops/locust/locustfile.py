from locust import HttpLocust, TaskSet, task


def login(l):
    l.client.post('/login', {'username': 'admin', 'password': 'q'})
    foo = l.client.post('/internal-api/api-token-auth/', {'username': 'admin', 'password': 'q'})


class BaseTaskSet(TaskSet):
    pass


class LoadQuestionnaire(TaskSet):
    def on_start(self):
        login(self)

    @task(1)
    def root(self):
        self.client.get("/")

    # @task
    # def questionnaire_page(self):
    #     self.client.get('/test-organization/our/assessments/6tpWdaGeHjIhZWZdqkKZ94/questionnaires/3qveuv8LTo2P5qtR5lEQAY/edit')

    # @task
    # def questionnaire_detail(self):
    #     print "cookies"
    #     for cookie in self.client.cookies:
    #         print cookie
    #     self.client.get('/internal-api/assessments/questionnaires/3qveuv8LTo2P5qtR5lEQAY?format=json')

    # @task(4)
    # def questionnaire_data(self):
    #     self.client.get('/internal-api/assessments/6tpWdaGeHjIhZWZdqkKZ94/data/?format=json&questionnaireUuid=3qveuv8LTo2P5qtR5lEQAY')


# class WebsiteUser(HttpLocust):
#     task_set = UserBehavior
#     min_wait = 5000
#     max_wait = 9000

class BaseLocust(HttpLocust):
    min_wait = 5000
    max_wait = 9000


class LoadQuestionnaireUser(BaseLocust):
    task_set = LoadQuestionnaire


