BIRT reports for SAMS
=====================
The SAMS application can use BIRT to generate PDF reports of
assessments. This readme explains how to set up BIRT.


Service
~~~~~~~
We run BIRT as a standalone service that knows nothing about the
main application. All it needs is the BIRT runtime and the files in this
folder. Setting up the service:

* Download and unpack birt-runtime from
  http://download.eclipse.org/birt/downloads/; rename the unpacked
  folder to ``birt-runtime``.
* ``pip install requirements.txt``
* To run the service, execute ``./birt-wrapper.py``. It will listen on
  ``localhost:5000`` for HTTP requests.
* Configure the main application to send requests to this service. Set
  the Django settings variable ``BIRT_SERVICE_URL`` to
  ``http://localhost:5000/birt/sai_assessment_results``.
