#!/usr/bin/env python

import os
import subprocess
import csv
import tempfile
from contextlib import contextmanager
import flask
from path import path

APP_DIR = path(__file__).abspath().parent
BIRT_HOME = path(os.environ.get('BIRT_HOME', APP_DIR / 'birt-runtime'))

DESIGNS = {
    'test': {
        'template': BIRT_HOME / 'WebViewerExample' / 'test.rptdesign',
    },
    'sai_assessment_results': {
        'template': APP_DIR / 'sai_results.rptdesign',
        'data_filename': 'results.csv',
    },
}

app = flask.Flask(__name__)
logger = app.logger


@app.route('/')
def index():
    return flask.render_template('index.html', designs=DESIGNS)


@contextmanager
def tmp_dir():
    tmp = path(tempfile.mkdtemp())
    try:
        yield tmp
    finally:
        tmp.rmtree()


@app.route('/birt/<name>', methods=['GET', 'POST'])
def run_birt(name):
    design = DESIGNS.get(name)
    if design is None:
        flask.abort(404)

    with tmp_dir() as tmp:
        logger.info("preparing report environment in %s", tmp)
        template_path = tmp / design['template'].name
        logger.info("copying template to %s", template_path)
        design['template'].copy(template_path)
        if 'data_filename' in design:
            def make_row(record, prefix=''):
                out = {}
                for key, value in record.items():
                    if isinstance(value, dict):
                        out.update(make_row(value, prefix + key + '-'))
                    else:
                        out[prefix + key] = value
                return out
            rows = [make_row(record) for record in flask.request.json]
            data_path = tmp / design['data_filename']
            logger.info("writing input data to %s", data_path)
            with data_path.open('wb') as f:
                writer = csv.DictWriter(f, sorted(rows[0]))
                writer.writeheader()
                for row in rows:
                    writer.writerow(row)
        gen_report_path = BIRT_HOME / 'ReportEngine' / 'genReport.sh'
        command = ['bash', str(gen_report_path),
                   '--format', 'pdf',
                   '--output', 'report.pdf',
                   str(template_path)]
        env = dict(os.environ, BIRT_HOME=BIRT_HOME)
        logger.info("invoking %r ...", command)
        res = subprocess.check_output(command, cwd=tmp, env=env)

        if res:
            logger.info("output: %s", res)
            return str(res)

        else:
            logger.info("success! returing the result")
            out_path = tmp / 'report.pdf'
            return flask.Response(out_path.bytes(),
                                  content_type='application/pdf')


if __name__ == '__main__':
    host = os.environ.get('APP_HOST', '127.0.0.1')
    app.run(debug=True, host=host)
