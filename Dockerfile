FROM heroku/cedar:14

RUN apt-get update
RUN apt-get -y install python
RUN apt-get -y install python-pip
RUN apt-get -y install npm
RUN apt-get -y install nodejs-legacy
RUN apt-get -y install gettext

RUN pip install virtualenv

RUN mkdir -p /app
RUN mkdir -p /app/.heroku
RUN mkdir -p /app/.heroku/python

RUN virtualenv /app/.heroku/python

ADD . /app

WORKDIR /app/webapp

RUN wget -q http://builds.agriplace.com/dav/npm-cache.tar.gz

RUN tar xzf npm-cache.tar.gz

RUN npm install

RUN rm -f npm-cache.tar.gz

RUN ./node_modules/.bin/gulp production

RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

RUN tar xf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

RUN mv wkhtmltox /app/wkhtmltox

RUN rm -f wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

#
# Clean up development dependencies
#
RUN rm -rf node_modules

WORKDIR /app

RUN /bin/bash -c "source /app/.heroku/python/bin/activate && pip install -r webapp/requirements/production.txt && pip install waitress"

WORKDIR /app/webapp

RUN mkdir -p locale/es/LC_MESSAGES

RUN mkdir -p locale/nl/LC_MESSAGES

# RUN wget -q -O locale/es/LC_MESSAGES/content.po https://builds.agriplace.com/dav/translations/es/content.po

# RUN wget -q -O locale/nl/LC_MESSAGES/content.po https://builds.agriplace.com/dav/translations/nl/content.po

RUN /bin/bash -c "source /app/.heroku/python/bin/activate && AGRIPLACE_AUTOBUILD=on python manage.py compilemessages && AGRIPLACE_AUTOBUILD=on python manage.py compress --no-color && AGRIPLACE_AUTOBUILD=on python manage.py collectstatic --noinput"

RUN rm -rf frontend/components frontend/demo frontend/javascript frontend/less frontend/lib frontend/locale frontend/pages frontend/react frontend/static/frontend/css frontend/static/frontend/fonts frontend/static/frontend/img frontend/static/frontend/javascript frontend/static/frontend/jquery.js frontend/static/frontend/leaflet frontend/static/frontend/lib frontend/static/frontend/readmore.min.js frontend/static/frontend/select2.png db datasync/datastore/content

ARG BUILD_NUMBER

RUN echo "BUILD_NUMBER = $BUILD_NUMBER\nBUILD_TIME = \"$(date +"%m-%d-%Y %H:%M %z")\"" > ./sams_project/settings/build_stats.py

WORKDIR /

RUN tar czf slug.tar.gz ./app --exclude="npm-cache*"

WORKDIR /app

RUN /bin/bash -c "source /app/.heroku/python/bin/activate && pip install -r webapp/requirements/autobuild.txt"
